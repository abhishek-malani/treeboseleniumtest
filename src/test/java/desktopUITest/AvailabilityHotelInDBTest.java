package desktopUITest;

import java.io.IOException;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentSkipListMap;

import javax.xml.parsers.ParserConfigurationException;

import org.joda.time.DateTime;
import org.joda.time.Days;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;
import org.xml.sax.SAXException;

import base.HotelLogixAPI;
import utils.DBUtils;

public class AvailabilityHotelInDBTest {

	/**
	 * Test to check that DB availability table has the entry (0 or more) for
	 * every hotel for 6months window
	 * 
	 * @throws SQLException
	 * @throws IOException
	 * @throws SAXException
	 * @throws ParserConfigurationException
	 */
	@Test(groups = { "AvailabilityHotel" })
	public void testHotelAvailabilityInDB()
			throws SQLException, ParserConfigurationException, SAXException, IOException {
		System.setProperty("webdriver.chrome.driver", "/usr/local/share/chromedriver");
		HotelLogixAPI api = new HotelLogixAPI();
		DBUtils db = new DBUtils();
		SoftAssert verify = new SoftAssert();
		int loopCnt = Integer.parseInt(System.getProperty("numberOfDays"));
		String hotelIdList = null; // will contain hotelIds which are not in sync as comma separated list

		String checkin = null, checkout = null;

		Map<String, List<String>> soldOutHotelsOnWeb = new ConcurrentSkipListMap<String, List<String>>();

		DateTime dt = new DateTime();
		DateTimeFormatter dtf = DateTimeFormat.forPattern("yyyy-MM-dd");

		for (int i = 0; i < loopCnt; i++) {
			checkin = dtf.print(dt.plusDays(i));
			checkout = dtf.print(dt.plusDays(i + 1));

			Map<String, Integer> hxAvailability = api.hxRoomAvailability(checkin, checkout);
			Map<String, Integer> dbAvailaiblity = db.getRoomAvailability(checkin);

			// remove same entry i.e. if Hotel name and room type availability
			// is same in both DB and HX
			for (String key : hxAvailability.keySet()) {
				if (hxAvailability.containsKey(key) && dbAvailaiblity.containsKey(key)) {
					if (hxAvailability.get(key) == dbAvailaiblity.get(key)) {
						hxAvailability.remove(key);
						dbAvailaiblity.remove(key);
					}
				}
			}

			// remove if value is zero in dbAvailability and its not present in
			// hxAvailability, which means that it was sold out
			for (String key : dbAvailaiblity.keySet()) {
				if (dbAvailaiblity.get(key) == 0) {
					if (!hxAvailability.containsKey(key)) {
						dbAvailaiblity.remove(key);
					}
				}
			}

			// Third case where, availability in HX is there but there is no
			// entry in DB for hotel room type

			for (String key : hxAvailability.keySet()) {
				if (!dbAvailaiblity.containsKey(key)) {
					int hxcount = hxAvailability.get(key);
					String keyHotelRoomHXCount = key ; //+ " HX count " + "(" + hxcount + ")";

					if (!soldOutHotelsOnWeb.containsKey(keyHotelRoomHXCount)) {
						System.out.println("key is not present " + keyHotelRoomHXCount + " for checkin : " + checkin);
						soldOutHotelsOnWeb.put(keyHotelRoomHXCount, new ArrayList<String>());
						soldOutHotelsOnWeb.get(keyHotelRoomHXCount).add(new String(checkin));
					} else {
						System.out.println("key is present " + keyHotelRoomHXCount + " for checkin : " + checkin);
						soldOutHotelsOnWeb.get(keyHotelRoomHXCount).add(new String(checkin));
					}
				}
			}
		}

		List<String> logMessage = new ArrayList<String>();

		for (String key : soldOutHotelsOnWeb.keySet()) {
			System.out.println(key + " for dates : " + Arrays.toString(soldOutHotelsOnWeb.get(key).toArray()));
			String id = key.split("_")[0].split("\\(")[1].split("\\)")[0];
			
			if (hotelIdList == null){
				hotelIdList = id;
			}else{
				if(!(hotelIdList.contains(id))){
					hotelIdList = hotelIdList + "," + id;
				}
			}
			logMessage.add("\n" + key + " for dates : "
					+ Arrays.toString(convertToDateArrange(soldOutHotelsOnWeb.get(key)).toArray()));
		}

		verify.assertTrue(soldOutHotelsOnWeb.isEmpty(),
				"Issues with Hotels Room shown sold out on Website/MSite (No entry in DB) although HX has full inventory of room type available: \n"
						+ Arrays.toString(logMessage.toArray()));
		System.out.println("Issue exists with the following hotel ids: "+ hotelIdList);
		
		try{
			Date date = new Date();
			String currDate = new SimpleDateFormat("dd-MM-yyyy").format(date);
			String url = String.format("http://biagent:3af5d148205aafe719594decf7ff5b4e@jenkins.treebohotels.com/buildByToken/buildWithParameters?job=DBSyncJobBI&token=asweasfsaf1255we&HOTELID=%s&STARTDATE=%s&DELTA=365", hotelIdList,currDate);
			WebDriver driver = new ChromeDriver();
			driver.get(url);
		}catch(Exception e){
			// Do nothing
		}

		verify.assertAll();
		
//		Debug
//		String[] a = new String[] {"2016-09-01", "2016-09-03","2016-09-04","2016-09-05","2016-09-06","2016-09-07",  "2016-09-08", "2016-09-09", "2016-09-10", "2016-09-11","2016-09-12","2016-09-13","2016-10-30","2016-10-31"};// 2016-09-09, 2016-09-10, 2016-09-11, 2016-09-12, 2016-09-13, 2016-09-14, 2016-09-15, 2016-09-16, 2016-09-17, 2016-09-18, 2016-09-19, 2016-09-20, 2016-09-21, 2016-09-22, 2016-09-23, 2016-09-24, 2016-09-25, 2016-09-26, 2016-09-27, 2016-09-28, 2016-09-29, 2016-09-30, 2016-10-01, 2016-10-02, 2016-10-03, 2016-10-04, 2016-10-05, 2016-10-06, 2016-10-07, 2016-10-08, 2016-10-09, 2016-10-10, 2016-10-11, 2016-10-12, 2016-10-13, 2016-10-14, 2016-10-15, 2016-10-16, 2016-10-17, 2016-10-18, 2016-10-19, 2016-10-20, 2016-10-21, 2016-10-22, 2016-10-23, 2016-10-24, 2016-10-25, 2016-10-26, 2016-10-27, 2016-10-28, 2016-10-29, 2016-10-30, 2016-10-31, 2016-11-01, 2016-11-02, 2016-11-03, 2016-11-04, 2016-11-05, 2016-11-06, 2016-11-07, 2016-11-08, 2016-11-09, 2016-11-10, 2016-11-11, 2016-11-12, 2016-11-13, 2016-11-14, 2016-11-15, 2016-11-16, 2016-11-17, 2016-11-18, 2016-11-19, 2016-11-20, 2016-11-21, 2016-11-22, 2016-11-23, 2016-11-24, 2016-11-25, 2016-11-26, 2016-11-27, 2016-11-28, 2016-11-29, 2016-11-30, 2016-12-01, 2016-12-02, 2016-12-03, 2016-12-04, 2016-12-05, 2016-12-06, 2016-12-07, 2016-12-08, 2016-12-09, 2016-12-10, 2016-12-11, 2016-12-12, 2016-12-13, 2016-12-14, 2016-12-15, 2016-12-16, 2016-12-17, 2016-12-18, 2016-12-19, 2016-12-20, 2016-12-21, 2016-12-22, 2016-12-24, 2016-12-25, 2016-12-26, 2016-12-27, 2016-12-28, 2016-12-29, 2016-12-30, 2016-12-31, 2017-01-01, 2017-01-02, 2017-01-03, 2017-01-04, 2017-01-05, 2017-01-06, 2017-01-07, 2017-01-08, 2017-01-09, 2017-01-10, 2017-01-11, 2017-01-12, 2017-01-13, 2017-01-14, 2017-01-15, 2017-01-16, 2017-01-17, 2017-01-18, 2017-01-19, 2017-01-20, 2017-01-21, 2017-01-22, 2017-01-23, 2017-01-24, 2017-01-25, 2017-01-26, 2017-01-27, 2017-01-28, 2017-01-29, 2017-01-30, 2017-01-31, 2017-02-01, 2017-02-02, 2017-02-03, 2017-02-04, 2017-02-05, 2017-02-06, 2017-02-07, 2017-02-08, 2017-02-09, 2017-02-10, 2017-02-11, 2017-02-12, 2017-02-13, 2017-02-14, 2017-02-15, 2017-02-16, 2017-02-17, 2017-02-18, 2017-02-19, 2017-02-20};
//		List<String> list = new ArrayList<String>(Arrays.asList(a));
//		System.out.println(convertToDateArrange(list));
	}

	// convert the date array into date range
	public List<String> convertToDateArrange(List<String> date) {
		DateTime dt = new DateTime();
		DateTimeFormatter dtf = DateTimeFormat.forPattern("yyyy-MM-dd");

		List<String> formattedDate = new ArrayList<String>();

		// if there is only one item in list, there is no range, so just add to
		// the list
		System.out.println("input list is :" + Arrays.toString(date.toArray()));

		if (date.size() == 1) {
			System.out.println("size of date list is 1");
			formattedDate.add(date.get(0));
		} else {
			// Put all consecutive dates as range in format, as example
			// 2016-09-01 to 2016-10-31 (61 days)
			// If there are multiple date ranges, identify first date range and
			// remove first date range Strings from list.
			// Repeat above till date list is empty
			int key = 0; // to stop to become infinite loop

			while ((key < 100) && (!date.isEmpty())) {
				
				key += 1;
				System.out.println("Current iteration :" + key);
				int num = date.size();
				System.out.println("Current list size :" + num);
				int numOfDays = 1;
				String initialDate = date.get(0);
				String endDateRange = null;
				System.out.println("Current  :" + num);
				
				if (date.size() == 1) {
					System.out.println("size of date list is 1");
					formattedDate.add(date.get(0));
					date.remove(0);
					break;
				}
				for (int i = 0; i < (num - 1); i++) {
					// check if consecutive dates are there
					System.out.println(" first date " + dtf.parseDateTime(date.get(i)));
					System.out.println(" second date " + dtf.parseDateTime(date.get(i + 1)));
					System.out.println("Difference in days " + Days
							.daysBetween(dtf.parseDateTime(date.get(i)), dtf.parseDateTime(date.get(i + 1))).getDays());
					if (Days.daysBetween(dtf.parseDateTime(date.get(i)), dtf.parseDateTime(date.get(i + 1)))
							.getDays() == 1) {
						System.out.println("Days are in consecutive order");
						numOfDays += 1;

						if (num == numOfDays) {
							// If all dates were in sequence, then it need not
							// go in else block
							// at all
							System.out.println("All Days are in consecutive order with range length " + numOfDays);
							endDateRange = date.get(num - 1);
							String newDateRangesWithDays = initialDate + " to " + endDateRange + " (" + numOfDays
									+ " days" + ") ";
							formattedDate.add(newDateRangesWithDays);
							// Empty list

							Iterator<String> itr = date.iterator();
							while (itr.hasNext()) {
								String s = itr.next();
								itr.remove();
							}
							break;
						}
					} else {
						// If the days are Zero means no range all individual
						// days, just add in the list
						if (numOfDays == 1) {
							System.out.println("Days are not in consecutive order");
							formattedDate.add(date.get(i));
							date.remove(i);
							break;
							// If there are date ranges, format it and then add
						} else if (numOfDays > 1) {
							System.out.println("Days are in consecutive order with range length " + numOfDays);
							endDateRange = date.get(i);
							String newDateRangesWithDays = initialDate + " to " + endDateRange + " (" + numOfDays
									+ " days" + ") ";
							formattedDate.add(newDateRangesWithDays);
							System.out.println("value of i " + i);
							
							Iterator<String> itr = date.iterator();
							while ( numOfDays > 0 && itr.hasNext()) {
								String s = itr.next();
								itr.remove();
								numOfDays = numOfDays - 1;
							}
							break;
						}
					}
				}
			}
		}

		return formattedDate;
	}

}
