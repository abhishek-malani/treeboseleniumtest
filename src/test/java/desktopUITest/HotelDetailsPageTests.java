package desktopUITest;

import java.util.Queue;

import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import base.BrowserHelper;
import base.TestBaseSetUp;
import desktopUIPageObjects.BookingHistory;
import desktopUIPageObjects.ConfirmationPage;
import desktopUIPageObjects.HomePage;
import desktopUIPageObjects.HotelDetailsPage;
import desktopUIPageObjects.HotelResultsPage;
import desktopUIPageObjects.ItineraryPage;
import utils.CommonUtils;

public class HotelDetailsPageTests extends TestBaseSetUp {
	private HomePage homePage;
	private HotelDetailsPage hotelDetailsPage;
	private BrowserHelper browserHelper;
	private HotelResultsPage hotelResultsPage;

	//
	// open homepage then serach for random city and for date
	// after search we get hotel search result page
	// then pick any random hotel from the resultpage
	// then check the hotel details
	// Verify About Hotel Title is displayed
	// Verify About Hotel Content is displayed
	// Verify About Hotel Content More link is displayed
	// Click Content More link 
	// veryfy the content is displayed under more link
	// does Treebo Trilights  Title is displayed
	// verify About Treebo Trilights Wrapper is displayed
	// Verify Assured Quality Image is displayed
	// Verify Hotel Amenities is displayed
	// Verify Hotel Accessbilities is displayed
	// Rules and Policies Title is displayed
	// click on Know More link in Policies Content
	// Open respective page and verified the page
	//

	@Test(groups = { "HDPage" })
	public void testHotelDetailsCheck() {
		homePage = new HomePage();
		hotelResultsPage = new HotelResultsPage();
		hotelDetailsPage = new HotelDetailsPage();
		browserHelper = new BrowserHelper();

		CommonUtils utils = new CommonUtils();
		SoftAssert verify = new SoftAssert();

		String location = utils.getProperty("Country"); // "India"; // "India";
		String testHotel = utils.getProperty("DummyHotel"); // "Dummy";
		String testUrl = utils.getProperty("TestUrl");
		int hotelIndex = 0;
		int checkInFromCurrentDate = utils.getRandomNumber(30, 45);
		int checkOutFromCurrentDate = checkInFromCurrentDate + 1;
		int roomIndex = 1;
		String[] checkInCheckOutDates = new String[2];

		// Home Page is Displayed
		homePage.verifyHomePagePresence();
		
		// Do search and get checkin checkout dates
		checkInCheckOutDates = homePage.doSearch(location, checkInFromCurrentDate, checkOutFromCurrentDate, roomIndex);
		hotelResultsPage.verifyHotelResultsPagePresence();

		if (browserHelper.getCurrentUrl().contains(testUrl)) {
			hotelIndex = hotelResultsPage.getHotelIndexByHotelName(testHotel);
		} else {
			hotelIndex = hotelResultsPage.getHotelIndexByHotelAvailable();
		}

		String hotelName = hotelResultsPage.getHotelNameByIndex(hotelIndex);
		String hotelId = hotelResultsPage.getHotelIdByHotelName(hotelName);
		System.out.println("Hotel Name: " + hotelName + " ,Hotel id : " + hotelId);
		
		//click on hotel by its name
		hotelDetailsPage.clickonhotel(hotelName);
		//hotel title verified
		verify.assertTrue(hotelDetailsPage.doesHotelTitleDisplayed());
		//about hotel content verified 
		verify.assertTrue(hotelDetailsPage.doesAboutHotelContentDisplayed());
		//click more link and verified the more link content 
		hotelDetailsPage.clickMoreLinkandOpenedTextVerified();
		
		// check Treebo Trillight Title 
		verify.assertTrue(hotelDetailsPage.doesTreeboTrilightsTitleDisplayed());
		//Treebo Trillight Wrapper verified
		verify.assertTrue(hotelDetailsPage.doesTreeboTrilightWrapperDisplayed());
		// Assure quality image displayed
		verify.assertTrue(hotelDetailsPage.doesDisplayAssuredQualityImageDisplayed());
		//Amenities Wrapper verified
		verify.assertTrue(hotelDetailsPage.doesHotelAmenitiesWrapperDisplayed());
		//Hotel Accessbilities title verified
		verify.assertTrue(hotelDetailsPage.doesHotelAccessbilitiesTitleDisplayed());
		//hotel accessbilities wrapper verified
		verify.assertTrue(hotelDetailsPage.doesHotelAccessbilitiesWrapperDisplayed());
		//Rules and Policy title varified
		verify.assertTrue(hotelDetailsPage.doesRulesandPoliciesTitleDisplayed());
		for (int i = 1; i <= 4; i++) {
			//click on KnowMore Links
			hotelDetailsPage.clickKnowMoreLink(i);
			//check opened correct page
			verify.assertTrue(hotelDetailsPage.doesKnowMoreLinkOpenedCorrectPage(i));
		}
		verify.assertAll();
	}
	
	
	/***
	 * search the hotel by location,chekIn,chekOut
	 * click on hotel by its name on hotel result page
	 * verify the normal image
	 * then click on the expand 
	 * verify the gallary image
	 * hten click on the next button 
	 * verify the next gallary image
	 * then click on the cross to close gallary view
	 * 	 
	 */
	@Test(groups = { "HDPage" })
	public void testHotelDetailsImageGalleryView(){
		homePage = new HomePage();
		hotelResultsPage = new HotelResultsPage();
		hotelDetailsPage = new HotelDetailsPage();
		browserHelper = new BrowserHelper();

		CommonUtils utils = new CommonUtils();
		SoftAssert verify = new SoftAssert();

		String location = utils.getProperty("city"); // "India"; // "India";
		String testHotel = utils.getProperty("DummyHotel"); // "Dummy";
		String testUrl = utils.getProperty("TestUrl");
		int hotelIndex = 0;
		int checkInFromCurrentDate = utils.getRandomNumber(30, 45);
		int checkOutFromCurrentDate = checkInFromCurrentDate + 1;
		int roomIndex = 1;
		String[] checkInCheckOutDates = new String[2];

		// Home Page is Displayed
		homePage.verifyHomePagePresence();
		// Do search and get checkin checkout dates
		checkInCheckOutDates = homePage.doSearch(location, checkInFromCurrentDate, checkOutFromCurrentDate, roomIndex);
		hotelResultsPage.verifyHotelResultsPagePresence();

		if (browserHelper.getCurrentUrl().contains(testUrl)) {
			hotelIndex = hotelResultsPage.getHotelIndexByHotelName(testHotel);
		} else {
			hotelIndex = hotelResultsPage.getHotelIndexByHotelAvailable();
		}

		String hotelName = hotelResultsPage.getHotelNameByIndex(hotelIndex);
		String hotelId = hotelResultsPage.getHotelIdByHotelName(hotelName);
		System.out.println("Hotel Name: " + hotelName + " ,Hotel id : " + hotelId);
		//click on the hotel by  hotel name
		hotelDetailsPage.clickonhotel(hotelName);
		//Normal Image opened
		verify.assertTrue(hotelDetailsPage.isNormalImageDisplayed(),"Gallary imgae is displayed");
		//click on zoomOut
		hotelDetailsPage.clickOnZoomInButton();
		//Gallary view opened
		verify.assertTrue(hotelDetailsPage.doesGallaryImageDisplayed(),"Expand image is not displayed");
		//click on next button
		hotelDetailsPage.clickOnNextButton();
		//next image displayed
		verify.assertTrue(hotelDetailsPage.isNextGallaryImageDisplayed(),"Next image is not displayed");
		//click on zoomOut button
		hotelDetailsPage.clickOnZoomOutButton();
		verify.assertAll();
		}
	
	/***
     * 
     * Click on homePage and search for perticular hotel for current date
     * then click on hotel by its name
     * check the all available roomtype
     * verify the first available room auto select
     * also verify the hotel's status with drop down hotel status
     * 
     */
	@Test(groups = { "HDPage" })
    public void testHDRoomDetailsCheck(){
    	homePage = new HomePage();
		hotelResultsPage = new HotelResultsPage();
		browserHelper = new BrowserHelper();
		hotelDetailsPage = new HotelDetailsPage();
		CommonUtils utils = new CommonUtils();
		SoftAssert verify = new SoftAssert();
		
		String location = utils.getProperty("city"); // "India";
		String testHotel = utils.getProperty("DummyHotel"); // "Dummy";
		String testUrl = utils.getProperty("TestUrl");
		int hotelIndex = 0;
		int checkInFromCurrentDate =utils.getRandomNumber(30, 45);;
		int checkOutFromCurrentDate = checkInFromCurrentDate + 1;
		int roomIndex = 1;
		String[] checkInCheckOutDates = new String[2];
		
		
		// Home Page is Displayed
	   homePage.verifyHomePagePresence();
	   // Do search and get checkIn checkout dates
	   checkInCheckOutDates = homePage.doSearch(location, checkInFromCurrentDate, checkOutFromCurrentDate, roomIndex);
	   
	   //verify hotels  result page
	   hotelResultsPage.verifyHotelResultsPagePresence();

	   if (browserHelper.getCurrentUrl().contains(testUrl)) {
					hotelIndex = hotelResultsPage.getHotelIndexByHotelName(testHotel);
				} 
	   else {
					hotelIndex = hotelResultsPage.getHotelIndexByHotelAvailable();
				}

		String hotelName = utils.getProperty("HotelNameAllRoomType");//Treebo Akshaya Mayflower
		
		//click on the hotel Treebo Akshaya Mayflower
		hotelResultsPage.clickOnHotelByName(hotelName);
		
		//Extracting the defualt roomType from HD page
		String defaultRoom = hotelDetailsPage.extactDafaultRoomTypeWithPrice().trim();
		System.out.println("Default room type that appeared "+defaultRoom);
		 
		// Store all roomtype into String array
		String[] roomType = hotelDetailsPage.getAvailableRoomTypes();
		//extracting the first available room
        String firstAvailableRoomType = roomType[0];
        
        //Store all soldOut room type
        String[] soldOutRoomType= hotelDetailsPage.getSoldOutRoom();
        
        //verified the roomtype appear on HD Page
        verify.assertTrue(hotelDetailsPage.doesRoomTypeAppearCorrect(defaultRoom, firstAvailableRoomType),"Room Type that appear on HD page is incorrect");

		
        //click on drop down
        hotelDetailsPage.clickOnDropdown();
        
        //get all roomtype dropdown
        String[] roomTypeDropDown=hotelDetailsPage.getAllRoomTypesDropDown();
        
        //get selected room
        String selectRoom = hotelDetailsPage.getSelectedRoomTypeDropDown();
        
        //get Not selected roomType
        String notSelected[]=hotelDetailsPage.getAvailableRoomTypesDropDownNotSelected();
        
        //get total roomtype that are available
        String totalRoomAvailable[]=hotelDetailsPage.getTotalAvailableRoomTypeDropDown();
        
        //verify the availablity status on both HD page and drop down
        verify.assertTrue(hotelDetailsPage.doesRoomStatusSame(roomType, totalRoomAvailable),"Availability Room Status is not same");
        
        //get roomtype which is soldOut
        String soldOutRoomDropDown[] =hotelDetailsPage.getSoldOutRoomDropDown();
        
        //verify the soldout status on both HD page and drop down
        verify.assertTrue(hotelDetailsPage.doesRoomStatusSame(soldOutRoomType, soldOutRoomDropDown),"Sold Out status is not same ");
        
        verify.assertAll();
    	
    }
	

}
