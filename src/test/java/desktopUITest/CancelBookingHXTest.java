package desktopUITest;

import java.io.IOException;
import java.sql.SQLException;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import javax.xml.parsers.ParserConfigurationException;

import org.apache.http.client.ClientProtocolException;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.testng.annotations.Test;
import org.xml.sax.SAXException;

import base.HotelLogixAPI;
import utils.DBUtils;

public class CancelBookingHXTest {
	
	@Test(groups={"CancelBooking"})
	public void testCancelBookingThroughHXAPI() throws SQLException, ClientProtocolException, IOException, ParserConfigurationException, SAXException{
		DBUtils db = new DBUtils();
		HotelLogixAPI api = new HotelLogixAPI();
		
		DateTime dt = new DateTime();
		DateTimeFormatter dtf = DateTimeFormat.forPattern("yyyy-MM-dd");
		String date = dtf.print(dt.minusDays(1));
		
		ConcurrentHashMap<String,String> openBookingInfo = db.getOrderIdAndReservationId("",date);
		
		openBookingInfo.keySet().parallelStream().forEach((key) -> {
			try {
				System.out.println("Canceling reservation with order id " + openBookingInfo.get(key) + " and reservation id " + key);
				api.cancelBooking(openBookingInfo.get(key),key);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		});
		
		
//		for (String key : openBookingInfo.keySet()){
//			System.out.println("Canceling reservation with order id " + openBookingInfo.get(key) + " and reservation id " + key);
//			api.cancelBooking(openBookingInfo.get(key),key);
//		}
	}
}