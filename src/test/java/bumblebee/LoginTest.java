package bumblebee;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.ParseException;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.entity.EntityBuilder;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;
import org.testng.Assert;
import org.testng.annotations.Test;

import utils.BumblebeeUtils;

public class LoginTest {
  
  @Test(groups={"login", "bumblebee"})
	public void RunLoginBasicTest() throws IOException{
		CloseableHttpClient httpClient = HttpClients.createDefault();
		HttpPost httpPost = new HttpPost(BumblebeeUtils.makeString("login"));

		try{

			EntityBuilder builder = EntityBuilder.create();
			List<NameValuePair> data = new ArrayList<NameValuePair>();
			data.add(new BasicNameValuePair("username", BumblebeeUtils.getFromProperty("email")));
			data.add(new BasicNameValuePair("password", BumblebeeUtils.getFromProperty("password")));
			if(BumblebeeUtils.makeString("register").contains("staging")){
				data.add(new BasicNameValuePair("hotel_id", BumblebeeUtils.getFromProperty("hotel_id")));
			}
			else{
				data.add(new BasicNameValuePair("hotel_id", BumblebeeUtils.getFromProperty("prod_hotel_id")));
			}
			builder.setParameters(data);
			HttpEntity entity = builder.build();
			httpPost.setEntity(entity);

			HttpResponse response = httpClient.execute(httpPost);
			HttpEntity responseEntity = response.getEntity();

			Assert.assertTrue(response.getStatusLine().getStatusCode() == 200,"Response code is "+response.getStatusLine().getStatusCode());
			JSONObject jsonObject = new JSONObject(EntityUtils.toString(responseEntity));

			Assert.assertTrue(jsonObject.getString("status").equals("success"),"Response is "+jsonObject.getString("status"));
			Assert.assertTrue(BumblebeeUtils.getToken() != null, "Response is null");

			for(int i = 0; i<jsonObject.getJSONObject("data").getJSONArray("rooms").length(); i++){
				Assert.assertTrue(jsonObject.getJSONObject("data").getJSONArray("rooms").getJSONObject(i).getInt("total_rooms")>0 );
			}


		}
		catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ClientProtocolException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		finally{
			httpClient.close();

		}


	}
	@Test(groups={"login", "bumblebee"})
	public void nullUserNameTest() throws IOException, ParseException, JSONException{
		CloseableHttpClient httpClient = HttpClients.createDefault();
		HttpPost httpPost = new HttpPost(BumblebeeUtils.makeString("login"));

		try{

			EntityBuilder builder = EntityBuilder.create();
			List<NameValuePair> data = new ArrayList<NameValuePair>();

			data.add(new BasicNameValuePair("password", BumblebeeUtils.getFromProperty("password")));
			if(BumblebeeUtils.makeString("register").contains("staging")){
				data.add(new BasicNameValuePair("hotel_id", BumblebeeUtils.getFromProperty("hotel_id")));
			}
			else{
				data.add(new BasicNameValuePair("hotel_id", BumblebeeUtils.getFromProperty("prod_hotel_id")));
			}
			builder.setParameters(data);
			HttpEntity entity = builder.build();
			httpPost.setEntity(entity);

			HttpResponse response = httpClient.execute(httpPost);
			HttpEntity responseEntity = response.getEntity();

			Assert.assertTrue(response.getStatusLine().getStatusCode() == 200,"Response code is "+response.getStatusLine().getStatusCode());
			JSONObject jsonObject = new JSONObject(EntityUtils.toString(responseEntity));

			Assert.assertTrue(jsonObject.getString("status").equals("error"), "error, You have logged in");
			Assert.assertTrue(jsonObject.getString("msg").equals("Invalid email"), "error, You have logged in");
			Assert.assertTrue(jsonObject.getString("code").equals("108"), "error, You have logged in");


		}
		catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ClientProtocolException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		finally{
			httpClient.close();

		}


	}
	@Test(groups={"login", "bumblebee"})
	public void invalidEmailTest() throws IOException, ParseException, JSONException{
		CloseableHttpClient httpClient = HttpClients.createDefault();
		HttpPost httpPost = new HttpPost(BumblebeeUtils.makeString("login"));

		try{

			EntityBuilder builder = EntityBuilder.create();
			List<NameValuePair> data = new ArrayList<NameValuePair>();
			data.add(new BasicNameValuePair("username", BumblebeeUtils.getFromProperty("invalidEmail")));
			data.add(new BasicNameValuePair("password", BumblebeeUtils.getFromProperty("password")));
			if(BumblebeeUtils.makeString("register").contains("staging")){
				data.add(new BasicNameValuePair("hotel_id", BumblebeeUtils.getFromProperty("hotel_id")));
			}
			else{
				data.add(new BasicNameValuePair("hotel_id", BumblebeeUtils.getFromProperty("prod_hotel_id")));
			}
			builder.setParameters(data);
			HttpEntity entity = builder.build();
			httpPost.setEntity(entity);

			HttpResponse response = httpClient.execute(httpPost);
			HttpEntity responseEntity = response.getEntity();

			Assert.assertTrue(response.getStatusLine().getStatusCode() == 200,"Response code is "+response.getStatusLine().getStatusCode());
			JSONObject jsonObject = new JSONObject(EntityUtils.toString(responseEntity));

			Assert.assertTrue(jsonObject.getString("status").equals("error"), "error, You have logged in");
			Assert.assertTrue(jsonObject.getString("msg").equals("Enter a valid email address."), "error, You have logged in");
			Assert.assertTrue(jsonObject.getString("code").equals("101"), "error, You have logged in");


		}
		finally{
			httpClient.close();

		}
	}

	@Test(groups={"login", "bumblebee"})
	public void wrongEmailTest() throws IOException{
		CloseableHttpClient httpClient = HttpClients.createDefault();
		HttpPost httpPost = new HttpPost(BumblebeeUtils.makeString("login"));

		try{

			EntityBuilder builder = EntityBuilder.create();
			List<NameValuePair> data = new ArrayList<NameValuePair>();
			data.add(new BasicNameValuePair("username", BumblebeeUtils.getFromProperty("wrongEmail")));
			data.add(new BasicNameValuePair("password", BumblebeeUtils.getFromProperty("password")));
			if(BumblebeeUtils.makeString("register").contains("staging")){
				data.add(new BasicNameValuePair("hotel_id", BumblebeeUtils.getFromProperty("hotel_id")));
			}
			else{
				data.add(new BasicNameValuePair("hotel_id", BumblebeeUtils.getFromProperty("prod_hotel_id")));
			}
			builder.setParameters(data);
			HttpEntity entity = builder.build();
			httpPost.setEntity(entity);

			HttpResponse response = httpClient.execute(httpPost);
			HttpEntity responseEntity = response.getEntity();

			Assert.assertTrue(response.getStatusLine().getStatusCode() == 200,"Response code is "+response.getStatusLine().getStatusCode());
			JSONObject jsonObject = new JSONObject(EntityUtils.toString(responseEntity));

			Assert.assertTrue(jsonObject.getString("status").equals("error"), "error, You have logged in");
			Assert.assertTrue(jsonObject.getString("msg").equals("Invalid Username or Password"), "error, You have logged in");
			Assert.assertTrue(jsonObject.getString("code").equals("101"), "error, You have logged in");


		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		finally{
			httpClient.close();

		}
	}

	@Test(groups={"login", "bumblebee"})
	public void invalidPasswordTest() throws IOException{
		CloseableHttpClient httpClient = HttpClients.createDefault();
		HttpPost httpPost = new HttpPost(BumblebeeUtils.makeString("login"));

		try{

			EntityBuilder builder = EntityBuilder.create();
			List<NameValuePair> data = new ArrayList<NameValuePair>();
			data.add(new BasicNameValuePair("username", BumblebeeUtils.getFromProperty("email")));
			data.add(new BasicNameValuePair("password", BumblebeeUtils.getFromProperty("invalidPassword")));
			if(BumblebeeUtils.makeString("register").contains("staging")){
				data.add(new BasicNameValuePair("hotel_id", BumblebeeUtils.getFromProperty("hotel_id")));
			}
			else{
				data.add(new BasicNameValuePair("hotel_id", BumblebeeUtils.getFromProperty("prod_hotel_id")));
			}
			builder.setParameters(data);
			HttpEntity entity = builder.build();
			httpPost.setEntity(entity);

			HttpResponse response = httpClient.execute(httpPost);
			HttpEntity responseEntity = response.getEntity();

			Assert.assertTrue(response.getStatusLine().getStatusCode() == 200,"Response code is "+response.getStatusLine().getStatusCode());
			JSONObject jsonObject = new JSONObject(EntityUtils.toString(responseEntity));

			Assert.assertTrue(jsonObject.getString("status").equals("error"), "error, You have logged in");
			Assert.assertTrue(jsonObject.getString("msg").equals("A valid integer is required."), "error, You have logged in");
			Assert.assertTrue(jsonObject.getString("code").equals("101"), "error, You have logged in");


		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		finally{
			httpClient.close();

		}
	}
	@Test(groups={"login", "bumblebee"})
	public void wrongPasswordTest() throws IOException{
		CloseableHttpClient httpClient = HttpClients.createDefault();
		HttpPost httpPost = new HttpPost(BumblebeeUtils.makeString("login"));

		try{

			EntityBuilder builder = EntityBuilder.create();
			List<NameValuePair> data = new ArrayList<NameValuePair>();
			data.add(new BasicNameValuePair("username", BumblebeeUtils.getFromProperty("email")));
			data.add(new BasicNameValuePair("password", BumblebeeUtils.getFromProperty("wrongPassword")));
			if(BumblebeeUtils.makeString("register").contains("staging")){
				data.add(new BasicNameValuePair("hotel_id", BumblebeeUtils.getFromProperty("hotel_id")));
			}
			else{
				data.add(new BasicNameValuePair("hotel_id", BumblebeeUtils.getFromProperty("prod_hotel_id")));
			}
			builder.setParameters(data);
			HttpEntity entity = builder.build();
			httpPost.setEntity(entity);

			HttpResponse response = httpClient.execute(httpPost);
			HttpEntity responseEntity = response.getEntity();

			Assert.assertTrue(response.getStatusLine().getStatusCode() == 200,"Response code is "+response.getStatusLine().getStatusCode());
			JSONObject jsonObject = new JSONObject(EntityUtils.toString(responseEntity));

			Assert.assertTrue(jsonObject.getString("status").equals("error"), "error, You have logged in");
			Assert.assertTrue(jsonObject.getString("msg").equals("Invalid Username or Password"), "error, "+jsonObject.getString("msg"));
			Assert.assertTrue(jsonObject.getString("code").equals("101"), "error, You have logged in");


		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		finally{
			httpClient.close();

		}
	}

	@Test(groups={"login", "bumblebee"})
	public void invalidHotelIDLoginTest() throws IOException{
		CloseableHttpClient httpClient = HttpClients.createDefault();
		HttpPost httpPost = new HttpPost(BumblebeeUtils.makeString("login"));

		try{

			EntityBuilder builder = EntityBuilder.create();
			List<NameValuePair> data = new ArrayList<NameValuePair>();
			data.add(new BasicNameValuePair("username", BumblebeeUtils.getFromProperty("email")));
			data.add(new BasicNameValuePair("password", BumblebeeUtils.getFromProperty("password")));
			data.add(new BasicNameValuePair("hotel_id", BumblebeeUtils.getFromProperty("invalidHotel_id")));	
			builder.setParameters(data);
			HttpEntity entity = builder.build();
			httpPost.setEntity(entity);

			HttpResponse response = httpClient.execute(httpPost);
			HttpEntity responseEntity = response.getEntity();

			Assert.assertTrue(response.getStatusLine().getStatusCode() == 200,"Response code is ");
			JSONObject jsonObject = new JSONObject(EntityUtils.toString(responseEntity));

			Assert.assertTrue(jsonObject.getString("status").equals("error"), "error, You have logged in");
			Assert.assertTrue(jsonObject.getString("msg").equals("A valid integer is required."), "error, You have logged in");
			Assert.assertTrue(jsonObject.getString("code").equals("101"), "error, You have logged in");


		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		finally{
			httpClient.close();

		}
	}

	@Test(groups={"login", "bumblebee"})
	public void wrongHotelIDLoginTest() throws IOException{
		CloseableHttpClient httpClient = HttpClients.createDefault();
		HttpPost httpPost = new HttpPost(BumblebeeUtils.makeString("login"));

		try{

			EntityBuilder builder = EntityBuilder.create();
			List<NameValuePair> data = new ArrayList<NameValuePair>();
			data.add(new BasicNameValuePair("username", BumblebeeUtils.getFromProperty("email")));
			data.add(new BasicNameValuePair("password", BumblebeeUtils.getFromProperty("password")));
			data.add(new BasicNameValuePair("hotel_id", BumblebeeUtils.getFromProperty("wrongHotelID")));
			builder.setParameters(data);
			HttpEntity entity = builder.build();
			httpPost.setEntity(entity);

			HttpResponse response = httpClient.execute(httpPost);
			HttpEntity responseEntity = response.getEntity();

			Assert.assertTrue(response.getStatusLine().getStatusCode() == 200,"Response code is "+response.getStatusLine().getStatusCode());
			JSONObject jsonObject = new JSONObject(EntityUtils.toString(responseEntity));

			Assert.assertTrue(jsonObject.getString("status").equals("error"), "error, You have logged in");
			Assert.assertTrue(jsonObject.getString("msg").equals("Hotel matching query does not exist."), "error, You have logged in");
			Assert.assertTrue(jsonObject.getString("code").equals("101"), "error, You have logged in");


		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		finally{
			httpClient.close();

		}
	}
}
