package desktopUITest;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Arrays;
import java.util.Set;

import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import base.BrowserHelper;
import base.TestBaseSetUp;
import desktopUIPageObjects.HomePage;
import desktopUIPageObjects.HotelDetailsPage;
import desktopUIPageObjects.HotelResultsPage;
import org.testng.Assert;
import org.testng.asserts.SoftAssert;
import utils.CommonUtils;

public class HotelSearchTest extends TestBaseSetUp {

	private HomePage homePage;
	private BrowserHelper browserHelper;
	private HotelResultsPage hotelResultsPage;
	private HotelDetailsPage hotelDetailsPage;

	/**
	 * This test verifies that basic hotel search from home page works Checks
	 * for Hotel search from home with Country - India State - Karnataka City -
	 * Bangalore POI - Marathahalli Hotel Name - Treebo Cartier Invalid name -
	 * abcd
	 * 
	 * @throws IOException
	 * @throws FileNotFoundException
	 * 
	 */

	@Test(groups = { "HotelSearch","ProdSanity" })
	public void testHotelBasicSearchFromHomePage() throws FileNotFoundException, IOException {
		homePage = new HomePage();
		hotelResultsPage = new HotelResultsPage();
		CommonUtils utils = new CommonUtils();

		String[] locations = { utils.getProperty("Country"), utils.getProperty("State"), utils.getProperty("Location"),
				utils.getProperty("Locality"), utils.getProperty("HotelName") };// {"India","Karnataka","Bangalore","Marathahalli","Treebo
																				// Cartier"}
		String invalidLocation = utils.getProperty("InvalidLocation"); // "abcd"

		for (String location : locations) {
			homePage.doSearch(location);
			hotelResultsPage.verifyHotelResultsPagePresence();
			hotelResultsPage.verifyAtleastOneHotelResultIsDisplayed();
			homePage.goToHomePage();
			homePage.verifyHomePagePresence();
		}

		homePage.doSearch(invalidLocation);
		hotelResultsPage.verifyNoResults();
	}

	/**
	 * This test verifies that basic hotel search from hotel search page works
	 * with Country - India State - Karnataka City - Bangalore POI -
	 * Marathahalli Hotel Name - Treebo Cartier Invalid name - abcd
	 * 
	 * @throws IOException
	 * @throws FileNotFoundException
	 * 
	 */

	@Test(groups = { "HotelSearch" })
	public void testHotelBasicSearchFromHotelSearchPage() throws FileNotFoundException, IOException {
		homePage = new HomePage();
		hotelResultsPage = new HotelResultsPage();
		CommonUtils utils = new CommonUtils();

		String[] locations = { utils.getProperty("Country"), utils.getProperty("State"), utils.getProperty("Location"),
				utils.getProperty("Locality"), utils.getProperty("HotelName") };
		String invalidLocation = utils.getProperty("InvalidLocation"); // "abcd"
		homePage.doSearch(locations[0]);
		hotelResultsPage.verifyHotelResultsPagePresence();
		
		for (String location : locations) {
			hotelResultsPage.doSearch(location);
			hotelResultsPage.verifyHotelResultsPagePresence();
			hotelResultsPage.verifyAtleastOneHotelResultIsDisplayed();
		}

		hotelResultsPage.doSearch(invalidLocation);
		hotelResultsPage.verifyNoResults();
	}

	/**
	 * This test verifies that show only available filter works
	 * 
	 * @throws IOException
	 * @throws FileNotFoundException
	 * 
	 */

	@Test(groups = { "HotelSearch" })
	public void testHotelVerifyShowOnlyAvailableFilter() throws FileNotFoundException, IOException {
		homePage = new HomePage();
		hotelResultsPage = new HotelResultsPage();
		CommonUtils utils = new CommonUtils();

		String location = utils.getProperty("Location"); // "Bangalore";

		System.out.println("Do Hotel Search for todays checkin since it will most likely return sold out rates");
		homePage.doSearch(location);
		hotelResultsPage.verifyHotelResultsPagePresence();
		// Get all hotel displayed, hotel with Quick book and hotel as sold out
		int totalDisplayedResult = hotelResultsPage.getTotalDisplayedHotelResults();
		int availableHotelResult = hotelResultsPage.getTotalAvailableHotelResults();
		int soldOutResults = hotelResultsPage.getTotalSoldOutHotelResults();
		hotelResultsPage.showOnlyAvailableHotels();
		int latestTotalDisplayedResult = hotelResultsPage.getTotalDisplayedHotelResults();
		// Verify only available hotel results are displayed after show only
		// available
		Assert.assertEquals(availableHotelResult, latestTotalDisplayedResult);
		Assert.assertTrue(totalDisplayedResult == (availableHotelResult + soldOutResults));
	}

	/**
	 * This test verifies that once price filter is applied as 1500-3000 hotel
	 * priced within this range only displayed
	 * 
	 * @throws IOException
	 * @throws FileNotFoundException
	 * 
	 */

	@Test(groups = { "HotelSearch" })
	public void testHotelPriceFilter() throws FileNotFoundException, IOException {
		homePage = new HomePage();
		hotelResultsPage = new HotelResultsPage();

		homePage.doSearch();
		hotelResultsPage.verifyHotelResultsPagePresence();
		hotelResultsPage.applyPriceFilter();// 1500-3000
		for (int x : hotelResultsPage.getAllHotelRoomRates()) {
			Assert.assertTrue(1500 < x && x < 3000);
		}
	}

	/**
	 * This test verifies that once Location filter is applied as Electronic
	 * city hotel within this locality only displayed
	 * 
	 * @throws IOException
	 * @throws FileNotFoundException
	 * 
	 */

	@Test(groups = { "HotelSearch" })
	public void testHotelLocationFilter() throws FileNotFoundException, IOException {
		homePage = new HomePage();
		hotelResultsPage = new HotelResultsPage();

		homePage.doSearch();
		hotelResultsPage.verifyHotelResultsPagePresence();
		String locationSelected = hotelResultsPage.applyLocationFilter().toLowerCase();// Electronic
																						// City
		for (String x : hotelResultsPage.getAddressForDisplayedHotels()) {
			System.out.println(x);
			Assert.assertTrue(x.toLowerCase().contains(locationSelected), "Failed for address: " + x);
		}
	}

	/**
	 * This test verifies that once Amenity filter is applied hotel with amenity
	 * only displayed
	 * 
	 * @throws IOException
	 * @throws FileNotFoundException
	 * 
	 */

	@Test(groups = { "HotelSearch" })
	public void testHotelAmenityFilter() throws FileNotFoundException, IOException {
		homePage = new HomePage();
		hotelResultsPage = new HotelResultsPage();

		homePage.doSearch();
		hotelResultsPage.verifyHotelResultsPagePresence();
		String[] initialHotelDisplayed = hotelResultsPage.getDisplayedHotelNames();
		hotelResultsPage.applyAmenityFilter();// GYM
		String[] hotelDisplayedAfterAmenityFilter = hotelResultsPage.getDisplayedHotelNames();
		Assert.assertTrue(initialHotelDisplayed.length > hotelDisplayedAfterAmenityFilter.length);
	}

	/**
	 * This test verifies that Default sort for hotel search is sorted by price
	 * 
	 * @throws IOException
	 * @throws FileNotFoundException
	 * 
	 */

	@Test(groups = { "HotelSearch","ProdSanity" })
	public void testDefaultSearchIsSortedByPrice() throws FileNotFoundException, IOException {
		homePage = new HomePage();
		hotelResultsPage = new HotelResultsPage();
		CommonUtils utils = new CommonUtils();
		SoftAssert verify = new SoftAssert();
		browserHelper = new BrowserHelper();
		String expectedSort = utils.getProperty("SortByPriceText"); // price
		String location = utils.getProperty("Country"); // "India";

		homePage.doSearch(location);
		hotelResultsPage.verifyHotelResultsPagePresence();
		//browserHelper.waitTime(10000);
		Assert.assertEquals(hotelResultsPage.getSortName(), expectedSort);
		
//		if (browserHelper.getCurrentUrl().contains("preprod")){
			int[] roomRatesAvailable = hotelResultsPage.getAllHotelRoomRatesForAvailableHotel();
			for (int i = 0; i < roomRatesAvailable.length - 1; i++) {
				verify.assertTrue(roomRatesAvailable[i] > 500, "Room rate is less than 500 from Available Hotels");
				verify.assertTrue(roomRatesAvailable[i + 1] >= roomRatesAvailable[i], "Available Hotels are not displayed with proper room rate."
						+ roomRatesAvailable[i + 1] + " should have been greater or equal than " + roomRatesAvailable[i]);
			}
			int[] roomRatesSoldOut = hotelResultsPage.getAllHotelRoomRatesForSoldOutHotel();
			for (int i = 0; i < roomRatesSoldOut.length - 1; i++) {
				verify.assertTrue(roomRatesSoldOut[i] > 500, "Room rate is less than 500 from Sold out hotels");
				verify.assertTrue(roomRatesSoldOut[i + 1] >= roomRatesSoldOut[i], "Sold out Hotels are not displayed with proper room rate."
						+ roomRatesSoldOut[i + 1] + " should have been greater or equal than " + roomRatesSoldOut[i]);
			}
//		}else {
//			int[] roomRates = hotelResultsPage.getAllHotelRoomRates();
//			for (int i = 0; i < roomRates.length - 1; i++) {
//				verify.assertTrue(roomRates[i] > 500, "Room rate is less than 500");
//				verify.assertTrue(roomRates[i + 1] >= roomRates[i], "Hotels are not displayed with proper room rate."
//						+ roomRates[i + 1] + " should have been greater or equal than " + roomRates[i]);
//			}
//		}
		
		verify.assertAll();
	}

	/**
	 * This test verifies that hotel search is sorted by distance if searched by
	 * locality
	 * 
	 * @throws IOException
	 * @throws FileNotFoundException
	 * 
	 */

	@Test(groups = { "HotelSearch" })
	public void testDefaultSearchIsSortedByDistanceIfSearchedByLocality() throws FileNotFoundException, IOException {
		homePage = new HomePage();
		hotelResultsPage = new HotelResultsPage();
		CommonUtils utils = new CommonUtils();
		SoftAssert verify = new SoftAssert();
		browserHelper = new BrowserHelper();

		String location = utils.getProperty("Locality"); // "Marathahalli"
		String expectedSort = utils.getProperty("SortByDistanceText"); // "distance"

		homePage.doSearch(location);
		hotelResultsPage.verifyHotelResultsPagePresence();
		Assert.assertEquals(hotelResultsPage.getSortName(), expectedSort);
		// Get all Hotel distance
//		if (browserHelper.getCurrentUrl().contains("preprod")){
			double[] distanceFromAvailable = hotelResultsPage.getAllHotelDistanceFromForAvailableHotel();
			for (int i = 0; i < distanceFromAvailable.length - 1; i++) {
				verify.assertTrue(distanceFromAvailable[i + 1] >= distanceFromAvailable[i],
						"AVailable Hotels are not displayed with proper distance sort." + distanceFromAvailable[i + 1]
								+ " should have been greated than " + distanceFromAvailable[i]);
			}
			
			double[] distanceFromSold = hotelResultsPage.getAllHotelDistanceFromForSoldOutHotel();
			for (int i = 0; i < distanceFromSold.length - 1; i++) {
				verify.assertTrue(distanceFromSold[i + 1] >= distanceFromSold[i],
						"Hotels are not displayed with proper distance sort." + distanceFromSold[i + 1]
								+ " should have been greated than " + distanceFromSold[i]);
			}
			
//		}else{
//			double[] distanceFrom = hotelResultsPage.getAllHotelDistanceFrom();
//			for (int i = 0; i < distanceFrom.length - 1; i++) {
//				verify.assertTrue(distanceFrom[i + 1] >= distanceFrom[i],
//						"Hotels are not displayed with proper distance sort." + distanceFrom[i + 1]
//								+ " should have been greated than " + distanceFrom[i]);
//			}
//		}
		
		verify.assertAll();
	}

	/**
	 * 1.Verify typing partial text of destination and hit on Search button,
	 * gives correct result (ex: BENG > Hit Search button) 2.Verify searching
	 * for Bangalore also displays treebo hotels available in Bangalore 3.Verify
	 * typing partial text of destination where treebo hotels are not available
	 * and hitting search button, displays no results found pop up 4.Verify
	 * searching by localities like Kormanagala is displays the results of
	 * hotels that are available in Kormangala along with near by treebos
	 * 5.Verify Breadcrumb in Search page is correct 6.Verify clicking on 'Home'
	 * takes user to landing page while clicking on 'Mumbai' takes user to
	 * Search result page of Mumbai city 7.Verify if NO destination is entered
	 * in the text field, clicking on Search button displays drop down cities
	 * 8.Verify user can select cities from the drop down and can click on
	 * 'Search Button' 9.Verify past dates in the calendar of CheckIn and
	 * Checkout are disabled 10.Verify users can search hotels for period of
	 * months (ex: Checkin - 1/3/16 to Checkout - 31/4/16)
	 */

	@Test(groups = { "HotelSearch" })
	public void testSearchDifferentScenarios() {
		homePage = new HomePage();
		hotelResultsPage = new HotelResultsPage();
		browserHelper = new BrowserHelper();
		CommonUtils utils = new CommonUtils();

		String partialLocation = utils.getProperty("partialLocation");// "Beng";
		String city = utils.getProperty("city");// "Bengaluru";
		String cityBangalore = utils.getProperty("Location");// "Bangalore";
		String locality = utils.getProperty("locality1");// "koramangala";
		String partialLocationNoTreebo = utils.getProperty("partialLocationNoTreebo");// "Mizo";
		int checkInFromCurrentDate = utils.getRandomNumber(60, 90);
		int checkOutFromCurrentDate = checkInFromCurrentDate + 35;
		int roomIndex = 1;

		homePage.doSearch(partialLocation);
		hotelResultsPage.verifyHotelResultsPagePresence();
		Assert.assertEquals(hotelResultsPage.getCitySearched().toLowerCase(), city.toLowerCase());
		homePage.goToHomePage();

		homePage.doSearch(cityBangalore);
		hotelResultsPage.verifyHotelResultsPagePresence();
		Assert.assertEquals(hotelResultsPage.getCitySearched().toLowerCase(), city.toLowerCase());
		homePage.goToHomePage();

		homePage.doSearch(partialLocationNoTreebo);
		hotelResultsPage.verifyNoResults();
		homePage.goToHomePage();

		homePage.doSearch(locality);
		hotelResultsPage.verifyHotelResultsPagePresence();
		Assert.assertEquals(hotelResultsPage.getLocalitySearched().toLowerCase(), locality.toLowerCase());
		hotelResultsPage.clickOnHomeInBreadcrumb();
		homePage.verifyHomePagePresence();
		homePage.doSearch(locality);
		hotelResultsPage.verifyHotelResultsPagePresence();
		String cityNameDisplayed = hotelResultsPage.getCitySearched();
		hotelResultsPage.clickOnCityInBreadcrumb();
		hotelResultsPage.verifyHotelResultsPagePresence();
		Assert.assertEquals(hotelResultsPage.getCitySearched().toLowerCase(), cityNameDisplayed.toLowerCase());
		homePage.goToHomePage();

		homePage.doSearch("");
		homePage.verifyCityDropDownDisplayed();
		homePage.selectCityFromDropDown();
		homePage.clickSearchButton();
		hotelResultsPage.verifyHotelResultsPagePresence();
		homePage.goToHomePage();

		// past dates are disabled in calendar
		homePage.verifyPastDatesAreDisabled();

		// more than 30 days search
		browserHelper.browserRefresh();
		homePage.doSearch(city, checkInFromCurrentDate, checkOutFromCurrentDate, roomIndex);
		hotelResultsPage.verifyHotelResultsPagePresence();
	}

	/**
	 * This test clicks on cities carousel for each city and checks that it
	 * loads search page - Click on City link - verify City name in search page
	 * - Click on any hotel link - verify HD page displayed
	 */

	@Test(groups = { "HotelSearch","ProdSanity" })
	public void testCityLinkCarousel() throws FileNotFoundException, IOException {
		homePage = new HomePage();
		hotelResultsPage = new HotelResultsPage();
		browserHelper = new BrowserHelper();
		hotelDetailsPage = new HotelDetailsPage();
		SoftAssert verify = new SoftAssert();

		homePage.verifyHomePagePresence();
		Set<String> cityLinksInCarousel = homePage.getAllCityLinkInCarousel();
		for (String city : cityLinksInCarousel) {
			try {
				homePage.goToHomePage();
				homePage.clickOnCityLinkInCarousel(city);
				hotelResultsPage.verifyHotelResultsPagePresence();
				hotelResultsPage.clickHotelNameByIndex(0); // click on first
				hotelDetailsPage.verifyHotelDetailsPagePresence();
			} catch (Exception e) {
				e.printStackTrace();
				verify.assertTrue(false, "failed for :" + city);
			}
		}
		verify.assertAll();
	}

	/**
	 * This test clicks on city link in SEO for each city and checks that it
	 * loads search page
	 */
	@Test(groups = { "HotelSearch","ProdSanity" })
	public void testSeoCityLink() throws FileNotFoundException, IOException {
		homePage = new HomePage();
		hotelResultsPage = new HotelResultsPage();
		browserHelper = new BrowserHelper();
		hotelDetailsPage = new HotelDetailsPage();
		SoftAssert verify = new SoftAssert();

		homePage.verifyHomePagePresence();
		Set<String> cityLinksInSEO = homePage.getAllCityLinkInSEO();
		for (String seoLink : cityLinksInSEO) {
			try {
				homePage.goToHomePage();
				homePage.clickOnSeoLink(seoLink);
				hotelResultsPage.verifyHotelResultsPagePresence();
				hotelResultsPage.clickHotelNameByIndex(0); // click on first
				hotelDetailsPage.verifyHotelDetailsPagePresence();
			} catch (Exception e) {
				e.printStackTrace();
				verify.assertTrue(false, "failed for :" + seoLink);
			}
		}
		verify.assertAll();
	}

	/**
	 * This test checks pricing remains same either Search done through city
	 * page, SEO Page or direct search
	 */
	@Test(groups = { "HotelSearch" })
	public void testPricingSameCitySEOKeyword() throws FileNotFoundException, IOException {
		homePage = new HomePage();
		hotelResultsPage = new HotelResultsPage();
		browserHelper = new BrowserHelper();
		SoftAssert verify = new SoftAssert();

		homePage.verifyHomePagePresence();
		Set<String> cityLinksInSEO = homePage.getAllCityLinkInSEO();
		Set<String> cityLinksInCarousel = homePage.getAllCityLinkInCarousel();
		for (String cityLink : cityLinksInCarousel) {
			// first Search through city link
			homePage.clickOnCityLinkInCarousel(cityLink);
			hotelResultsPage.verifyHotelResultsPagePresence();
			int[] roomRatesCity = hotelResultsPage.getAllHotelRoomRates();
			homePage.goToHomePage();

			// Find SEO link corresponding to city
			int[] roomRatesSeo = new int[roomRatesCity.length];
			try {
				String citySeoLink = getCitySEOLinkForCity(cityLinksInSEO, cityLink);
				homePage.clickOnSeoLink(citySeoLink);
				hotelResultsPage.verifyHotelResultsPagePresence();
				roomRatesSeo = hotelResultsPage.getAllHotelRoomRates();

			} catch (NoSuchElementException e) {
				e.printStackTrace();
				verify.assertTrue(false, "SEO link not found for : " + cityLink);
			}

			homePage.goToHomePage();

			// Keyword search for city
			homePage.doSearch(cityLink);
			hotelResultsPage.verifyHotelResultsPagePresence();
			int[] roomRatesKeyword = hotelResultsPage.getAllHotelRoomRates();
			homePage.goToHomePage();

			// the rates should be same
			verify.assertEquals(roomRatesCity, roomRatesKeyword);
			verify.assertEquals(roomRatesCity, roomRatesSeo);
		}
		verify.assertAll();
	}

	/**
	 * This test checks pricing remains same either Search done through city
	 * page, SEO Page or direct search
	 */
	@Test(groups = { "HotelSearch" })
	public void testSimilarHotelsNearBy() throws FileNotFoundException, IOException {
		homePage = new HomePage();
		hotelResultsPage = new HotelResultsPage();
		hotelDetailsPage = new HotelDetailsPage();
		SoftAssert verify = new SoftAssert();

		Set<String> cityLinksInCarousel = homePage.getAllCityLinkInCarousel();
		for (String city : cityLinksInCarousel) {
			try {
				homePage.goToHomePage();
				homePage.clickOnCityLinkInCarousel(city);
				hotelResultsPage.verifyHotelResultsPagePresence();
				int hotelCount = hotelResultsPage.getTotalDisplayedHotelResults();
				hotelResultsPage.clickHotelNameByIndex(0); // click on first
				hotelDetailsPage.verifyHotelDetailsPagePresence();
				if (hotelCount > 1) {
					verify.assertTrue(hotelDetailsPage.isSimilarTreebosNearbyShown(),
							"Similar treebos nearby Not shown");
					// Similar hotels should be sorted in distance order

				} else {
					verify.assertFalse(hotelDetailsPage.isSimilarTreebosNearbyShown(),
							"Similar treebos nearby shown though only one hotel in city");
				}
			} catch (Exception e) {
				e.printStackTrace();
				verify.assertTrue(false, "failed for :" + city);
			}
		}
		verify.assertAll();
	}

	private String getCitySEOLinkForCity(Set<String> seoCity, String city) {
		String strToReturn = null;
		for (String cityLink : seoCity) {
			if (cityLink.toLowerCase().contains(city.toLowerCase())) {
				strToReturn = cityLink;
				break;
			}
		}
		return strToReturn;
	}

}
