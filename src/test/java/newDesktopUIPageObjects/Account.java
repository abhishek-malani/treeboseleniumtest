package newDesktopUIPageObjects;

import java.util.HashMap;
import java.util.Map;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import desktopUIPageObjects.HomePage;

import base.BrowserHelper;
import base.DriverManager;

public class Account {
	private By accountName = By.xpath("//li[@id='user']/div/button");
	private By myAccount = By.xpath("//li[@id='user']//a[@href='/account/']");
	private By settingsLink = By.cssSelector(".settings");
	private By profileLink = By.cssSelector(".profile");
	private By oldPassword = By.cssSelector("#oldPwd");
	private By newPassword = By.cssSelector("#newPwd");
	private By confirmPassword = By.cssSelector("#confirmPwd");
	private By savePassword = By.cssSelector("#submitPasswordChange");
	private By signoutLink = By.xpath("//li[@id='user']//a[@href='/logout/']");
	private By signedInUser = By.cssSelector(".user__name.dropbtn");
	
	private By profileDataStored = By.cssSelector("#profileDetailData");
	private By firstNameLocator = By.cssSelector("#firstName");
	private By lastNameLocator = By.cssSelector("#lastName");
	private By mobileLocator = By.cssSelector("#mobile");
	private By emailLocator = By.xpath("//input[@id='email'][@readonly]");
	private By saveProfileButton = By.cssSelector("#submitProfileDetail");
	
	private By myReferrals = By.cssSelector(".account__link.tab__link.referrals");
	private By facebookLink = By.cssSelector(".btn.js-fbshare.btn--facebook");
	private By gmailLink = By.cssSelector(".btn.js-email-share.btn--google");
	
	public void goToAccountPage() {
		WebDriver driver = DriverManager.getInstance().getDriver();
		WebDriverWait wait = new WebDriverWait(driver, 120);
		wait.until(ExpectedConditions.presenceOfElementLocated(accountName));
		driver.findElement(accountName).click();
		driver.findElement(myAccount).click();
	}
	
	public void goToSettingsPage() {
		WebDriver driver = DriverManager.getInstance().getDriver();
		WebDriverWait wait = new WebDriverWait(driver, 120);
		wait.until(ExpectedConditions.presenceOfElementLocated(accountName));
		driver.findElement(accountName).click();
		driver.findElement(myAccount).click();
		driver.findElement(settingsLink).click();
	}
	
	public void goToProfilePage() {
		WebDriver driver = DriverManager.getInstance().getDriver();
		WebDriverWait wait = new WebDriverWait(driver, 120);
		wait.until(ExpectedConditions.presenceOfElementLocated(accountName));
		driver.findElement(accountName).click();
		driver.findElement(myAccount).click();
		driver.findElement(profileLink).click();
	}
	
	public void changePassword(String strOldPassword,String strNewPassword){
		WebDriver driver = DriverManager.getInstance().getDriver();
		driver.findElement(oldPassword).sendKeys(strOldPassword);
		driver.findElement(newPassword).sendKeys(strNewPassword);
		driver.findElement(confirmPassword).sendKeys(strNewPassword);
		driver.findElement(savePassword).click();
	}
	
	public void doSignOut() {
		WebDriver driver = DriverManager.getInstance().getDriver();
		System.out.println("Click on Signout ");
		driver.findElement(signedInUser).click();
		driver.findElement(signoutLink).click();
		try {
			Thread.sleep(3000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void updateNameAndMobile(String strFirstName, String strLastName, String strMobile){
		WebDriver driver = DriverManager.getInstance().getDriver();
		driver.findElement(firstNameLocator).clear();
		driver.findElement(firstNameLocator).sendKeys(strFirstName);;
		driver.findElement(lastNameLocator).clear();
		driver.findElement(lastNameLocator).sendKeys(strLastName);;
		driver.findElement(mobileLocator).clear();
		driver.findElement(mobileLocator).sendKeys(strMobile);;

		// verify that email is read only and cannot be edited/updated
		Assert.assertTrue(driver.findElement(emailLocator).isDisplayed());
		driver.findElement(saveProfileButton).click();
		try {
			Thread.sleep(3000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	// Keys are
	// [work_email, gender, phone, city, dob, last_name, first_name, anniversary, email]
	public String getProfileFieldValue(String key){
		Map<String,String> mapProfileData = returnProfileData();
		String profileFieldValue = mapProfileData.get(key);
		System.out.println("Value of profile field " + key + " is : " + profileFieldValue);
		return profileFieldValue;
	}
	
	public Map<String,String> returnProfileData(){
		WebDriver driver = DriverManager.getInstance().getDriver();
		String profielData = driver.findElement(profileDataStored).getAttribute("value");
		return convert(profielData);
	}
	
	// {"phone": "2000000000", "first_name": "Test", "last_name": "Test", "city": "India", "dob": "1989-01-05", 
	// "gender": "M", "work_email": "treebotestaccount@gmail.com", "anniversary": "2001-01-31", 
	 // "email": "treebotestaccount@gmail.com"} 
	
	public static Map<String, String> convert(String str) {
	    String[] tokens = str.replaceAll("\\{|\\}|\"|\"", "").split(":|,");
	    Map<String, String> map = new HashMap<String, String>();
	    for (int i=0; i<tokens.length-1; ) map.put(tokens[i++].trim(), tokens[i++].trim());
	    return map;
	}
	//click on account name and then click on my account link
	public void goToMyAccountPage() {
		WebDriver driver = DriverManager.getInstance().getDriver();
		WebDriverWait wait = new WebDriverWait(driver, 120);
		wait.until(ExpectedConditions.presenceOfElementLocated(accountName));
		driver.findElement(accountName).click();
		driver.findElement(myAccount).click();
		//driver.findElement(profileLink).click();
	}
	//click on my referrals
	public void clickOnMyReferrals(){
		WebDriver driver = DriverManager.getInstance().getDriver();
		WebDriverWait wait = new WebDriverWait(driver, 120);
		wait.until(ExpectedConditions.presenceOfElementLocated(myReferrals));
		driver.findElement(myReferrals).click();
		
	}
	//verified MyReferrals Title is verified
   public boolean isMyReferralsDisplayed(){
	   WebDriver driver = DriverManager.getInstance().getDriver();
	   return driver.findElement(By.cssSelector(".pages__heading.referral__heading")).isDisplayed();
	}
   // placeholder text displayed
   public boolean isTransparentTextDisplayed(){
	   WebDriver driver = DriverManager.getInstance().getDriver();
	   return driver.findElement(By.xpath(".//*[@id='referral']//span[@class='referral__code']")).isDisplayed();	   
   }
   //copy button displayed
  public boolean doesCopyButtonDisplayed(){
	  WebDriver driver = DriverManager.getInstance().getDriver();
	  return driver.findElement(By.cssSelector(".btn.referral__copy")).isDisplayed();
  }
  //facebook login link displayed
  public boolean doesFacebookLinkDisplayed(){
	  WebDriver driver = DriverManager.getInstance().getDriver();
	  return driver.findElement(facebookLink).isDisplayed();
  }
  //gmail login link displayed
  public boolean doesGmailLinkDisplayed(){
	  WebDriver driver = DriverManager.getInstance().getDriver();
	  return driver.findElement(gmailLink).isDisplayed();
  }
  //the SignedUp and Converted Text displayed
  public boolean doesSignedUpandConverteDisplayed(){
	  WebDriver driver = DriverManager.getInstance().getDriver();
	  return driver.findElement(By.cssSelector(".referral__total")).isDisplayed();
  }
  
  
}
