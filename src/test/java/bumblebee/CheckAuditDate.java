package bumblebee;

import java.io.IOException;

import org.apache.http.HttpEntity;
import org.apache.http.ParseException;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;
import org.testng.Assert;
import org.testng.annotations.Test;

import utils.BumblebeeUtils;

public class CheckAuditDate {
  @Test
  public void basicCheck() throws ClientProtocolException, IOException {
	  CloseableHttpClient httpClient = HttpClients.createDefault(); 
		HttpGet httpGet = new HttpGet(BumblebeeUtils.makeString("check_audit"));
		try{
			httpGet.addHeader("Authorization","Bearer "+ BumblebeeUtils.getToken());
			CloseableHttpResponse response = httpClient.execute(httpGet);
			Assert.assertTrue(response.getStatusLine().getStatusCode() == 200,"Response code is "+response.getStatusLine().getStatusCode());
			HttpEntity entity = response.getEntity();

			JSONObject jsonObject = new JSONObject(EntityUtils.toString(entity));
			Assert.assertTrue(jsonObject.getString("status").equals("success"));
			Assert.assertTrue(jsonObject.getJSONObject("data").getJSONObject("response").getJSONObject("status").getString("message").contains("Night Audit")); 
		
		}
		catch (JSONException e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		finally{
			httpClient.close();
		}
  }
}
