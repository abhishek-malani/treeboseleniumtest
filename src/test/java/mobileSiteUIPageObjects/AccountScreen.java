package mobileSiteUIPageObjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import base.BrowserHelper;
import base.DriverManager;

public class AccountScreen {
	private BrowserHelper browserHelper;
	
	private By bookingsLink = By.cssSelector("a[href='/account/bookings']");
	
		
	/**
	 * Click and bookings link and navigate to booking history
	 * @return true if home screen else false
	 */
	
	public void clickBookingsLink(){
		WebDriver driver = DriverManager.getInstance().getDriver();
		browserHelper = new BrowserHelper();
		browserHelper.jsclick(driver.findElement(bookingsLink));
	}
	
	public boolean isBookingHistoryScreenOpened(){
		WebDriver driver = DriverManager.getInstance().getDriver();
		return (driver.findElements(By.xpath("//div[contains(@class,'account-page__header')]")).size() == 1);
	}
	
	public boolean isUpcomingStaysHeaderDisplayed(){
		WebDriver driver = DriverManager.getInstance().getDriver();
		return (driver.findElements(By.xpath("//div[@id='upcomingStay']/div[@class='bookings__item__header'][text()='Upcoming Stays']")).size() == 1);
	}
	
	public String getHotelName(){
		WebDriver driver = DriverManager.getInstance().getDriver();
		return driver.findElement(By.cssSelector(".booking-info__hotel-name")).getText();
	}
	
	public String getCheckInDate(){
		WebDriver driver = DriverManager.getInstance().getDriver();
		return driver.findElement(By.cssSelector(".col-2>span:nth-of-type(1)")).getText();
	}
	
	public String getCheckOutDate(){
		WebDriver driver = DriverManager.getInstance().getDriver();
		return driver.findElement(By.cssSelector(".col-2>span:nth-of-type(2)")).getText();
	}
	
	public int getAdultCount(){
		WebDriver driver = DriverManager.getInstance().getDriver();
		return Integer.parseInt(driver.findElement(By.cssSelector(".rooms")).getText().split(" ")[0]);
	}
	
	public int getRoomCount(){
		WebDriver driver = DriverManager.getInstance().getDriver();
		return Integer.parseInt(driver.findElement(By.cssSelector(".guests")).getText().split(" ")[0]);
	}
	
	public String getBookingStatus(){
		WebDriver driver = DriverManager.getInstance().getDriver();
		return driver.findElement(By.cssSelector(".booking-info__status")).getText();
	}
	
	public void openBookingDetailsByBookingId(String bookingId){
		WebDriver driver = DriverManager.getInstance().getDriver();
		String locator = String.format("//a[contains(@href,'%s')]", bookingId);
		driver.findElement(By.xpath(locator)).click();
	}
	
	public boolean isBookingDetailsScreenOpened(){
		WebDriver driver = DriverManager.getInstance().getDriver();
		return (driver.findElements(By.xpath("//div[contains(@class,'account-page__header')]/span[text()='BOOKING DETAILS']")).size() == 1);
	}
	
	public String getGuestName(){
		WebDriver driver = DriverManager.getInstance().getDriver();
		return driver.findElement(By.cssSelector(".booking-details__item__traveller-name")).getText();
	}
	
	public String getGuestEmail(){
		WebDriver driver = DriverManager.getInstance().getDriver();
		return driver.findElement(By.cssSelector(".booking-details__item__traveller-email")).getText();
	}
	
	public String getGuestMobile(){
		WebDriver driver = DriverManager.getInstance().getDriver();
		return driver.findElement(By.cssSelector(".booking-details__item__traveller-number")).getText();
	}
	
	public Double getTotalAmount(){
		WebDriver driver = DriverManager.getInstance().getDriver();
		return Double.parseDouble(driver.findElement(By.cssSelector(".payment-info__total .col-2 span")).getText());
	}
	
	public void cancelBooking(){
		WebDriver driver = DriverManager.getInstance().getDriver();
		browserHelper = new BrowserHelper();
		driver.findElement(By.cssSelector(".booking-cancel")).click();
		browserHelper.waitTime(1000);
		driver.findElement(By.cssSelector(".col.prompt__action__item.prompt__action__item--ok")).click();
		browserHelper.waitTime(3000);
	}
	
	public void goBackToBookingHistory(){
		WebDriver driver = DriverManager.getInstance().getDriver();
		driver.findElement(By.cssSelector(".booking-info__back-link i")).click();
	}
}
