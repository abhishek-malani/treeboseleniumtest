package bumblebee;

import java.io.IOException;

import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.ParseException;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.entity.EntityBuilder;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.testng.Assert;
import org.testng.annotations.Test;

import utils.BumblebeeUtils;

import org.json.*;

public class RegisterTest {

	@Test(groups={"register", "bumblebee"})
	public void registerBasicTest() throws ClientProtocolException, IOException{
		CloseableHttpClient httpClient = HttpClients.createDefault();
		HttpPost httpPost = new HttpPost(BumblebeeUtils.makeString("register"));

		try{

			EntityBuilder builder = EntityBuilder.create();
			List<NameValuePair> data = new ArrayList<NameValuePair>();
			data.add(new BasicNameValuePair("email", BumblebeeUtils.getFromProperty("email")));
			data.add(new BasicNameValuePair("first_name", BumblebeeUtils.getFromProperty("firstName")));
			data.add(new BasicNameValuePair("last_name", BumblebeeUtils.getFromProperty("lastName")));
			data.add(new BasicNameValuePair("phone_number", BumblebeeUtils.getFromProperty("phone")));
			if(BumblebeeUtils.makeString("register").contains("staging")){
				data.add(new BasicNameValuePair("hotel_id", BumblebeeUtils.getFromProperty("hotel_id")));
			}
			else{
				data.add(new BasicNameValuePair("hotel_id", BumblebeeUtils.getFromProperty("prod_hotel_id")));
			}
			data.add(new BasicNameValuePair("title", BumblebeeUtils.getFromProperty("title")));
			builder.setParameters(data);
			HttpEntity entity = builder.build();
			httpPost.setEntity(entity);

			HttpResponse response = httpClient.execute(httpPost);
			HttpEntity responseEntity = response.getEntity();

			Assert.assertTrue(response.getStatusLine().getStatusCode() == 200,"Response code is "+response.getStatusLine().getStatusCode());
			JSONObject jsonObject = new JSONObject(EntityUtils.toString(responseEntity));

			Assert.assertTrue(jsonObject.getString("status").equals("error"), "Already exist");
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		finally{
			httpClient.close();
		}
	}

	@Test(groups={"register", "bumblebee"})
	public void nullEmailRegisterTest() throws ClientProtocolException, IOException, ParseException, JSONException{
		CloseableHttpClient httpClient = HttpClients.createDefault();
		HttpPost httpPost = new HttpPost(BumblebeeUtils.makeString("register"));

		try{

			EntityBuilder builder = EntityBuilder.create();
			List<NameValuePair> data = new ArrayList<NameValuePair>();
			data.add(new BasicNameValuePair("email", ""));
			data.add(new BasicNameValuePair("first_name", BumblebeeUtils.getFromProperty("firstName")));
			data.add(new BasicNameValuePair("last_name", BumblebeeUtils.getFromProperty("lastName")));
			data.add(new BasicNameValuePair("phone_number", BumblebeeUtils.getFromProperty("phone")));
			if(BumblebeeUtils.makeString("register").contains("staging")){
				data.add(new BasicNameValuePair("hotel_id", BumblebeeUtils.getFromProperty("hotel_id")));
			}
			else{
				data.add(new BasicNameValuePair("hotel_id", BumblebeeUtils.getFromProperty("prod_hotel_id")));
			}
			data.add(new BasicNameValuePair("title", BumblebeeUtils.getFromProperty("title")));
			builder.setParameters(data);
			HttpEntity entity = builder.build();
			httpPost.setEntity(entity);

			HttpResponse response = httpClient.execute(httpPost);
			HttpEntity responseEntity = response.getEntity();

			Assert.assertTrue(response.getStatusLine().getStatusCode() == 200,"Response code is "+response.getStatusLine().getStatusCode());
			JSONObject jsonObject = new JSONObject(EntityUtils.toString(responseEntity));
			Assert.assertTrue(jsonObject.getString("status").equals("error"), "error, You have logged in");
			Assert.assertTrue(jsonObject.getString("msg").equals("Invalid email"), "error, You have logged in");
			Assert.assertTrue(jsonObject.getString("code").equals("108"), "error, You have logged in");
		}
		finally{
			httpClient.close();
		}
	}

	@Test(groups={"register", "bumblebee"})
	public void invalidEmailRegisterTest() throws ClientProtocolException, IOException{
		CloseableHttpClient httpClient = HttpClients.createDefault();
		HttpPost httpPost = new HttpPost(BumblebeeUtils.makeString("register"));

		try{

			EntityBuilder builder = EntityBuilder.create();
			List<NameValuePair> data = new ArrayList<NameValuePair>();
			data.add(new BasicNameValuePair("email", BumblebeeUtils.getFromProperty("invalidEmail")));
			data.add(new BasicNameValuePair("first_name", BumblebeeUtils.getFromProperty("firstName")));
			data.add(new BasicNameValuePair("last_name", BumblebeeUtils.getFromProperty("lastName")));
			data.add(new BasicNameValuePair("phone_number", BumblebeeUtils.getFromProperty("phone")));
			if(BumblebeeUtils.makeString("register").contains("staging")){
				data.add(new BasicNameValuePair("hotel_id", BumblebeeUtils.getFromProperty("hotel_id")));
			}
			else{
				data.add(new BasicNameValuePair("hotel_id", BumblebeeUtils.getFromProperty("prod_hotel_id")));
			}
			data.add(new BasicNameValuePair("title", BumblebeeUtils.getFromProperty("title")));
			builder.setParameters(data);
			HttpEntity entity = builder.build();
			httpPost.setEntity(entity);

			HttpResponse response = httpClient.execute(httpPost);
			HttpEntity responseEntity = response.getEntity();

			Assert.assertTrue(response.getStatusLine().getStatusCode() == 200,"Response code is "+response.getStatusLine().getStatusCode());
			JSONObject jsonObject = new JSONObject(EntityUtils.toString(responseEntity));
			Assert.assertTrue(jsonObject.getString("status").equals("error"), "error, You have logged in");
			Assert.assertTrue(jsonObject.getString("msg").equals("Enter a valid email address."), "error, You have logged in");
			Assert.assertTrue(jsonObject.getString("code").equals("101"), "error, You have logged in");
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		finally{
			httpClient.close();
		}
	}

	@Test(groups={"register", "bumblebee"})
	public void repeatEmailRegisterTest() throws ClientProtocolException, IOException{
		CloseableHttpClient httpClient = HttpClients.createDefault();
		HttpPost httpPost = new HttpPost(BumblebeeUtils.makeString("register"));

		try{

			EntityBuilder builder = EntityBuilder.create();
			List<NameValuePair> data = new ArrayList<NameValuePair>();
			data.add(new BasicNameValuePair("email", BumblebeeUtils.getFromProperty("email")));
			data.add(new BasicNameValuePair("first_name", BumblebeeUtils.getFromProperty("firstName")));
			data.add(new BasicNameValuePair("last_name", BumblebeeUtils.getFromProperty("lastName")));
			data.add(new BasicNameValuePair("phone_number", BumblebeeUtils.getFromProperty("phone")));
			if(BumblebeeUtils.makeString("register").contains("staging")){
				data.add(new BasicNameValuePair("hotel_id", BumblebeeUtils.getFromProperty("hotel_id")));
			}
			else{
				data.add(new BasicNameValuePair("hotel_id", BumblebeeUtils.getFromProperty("prod_hotel_id")));
			}
			data.add(new BasicNameValuePair("title", BumblebeeUtils.getFromProperty("title")));
			builder.setParameters(data);
			HttpEntity entity = builder.build();
			httpPost.setEntity(entity);

			HttpResponse response = httpClient.execute(httpPost);
			HttpEntity responseEntity = response.getEntity();

			Assert.assertTrue(response.getStatusLine().getStatusCode() == 200,"Response code is "+response.getStatusLine().getStatusCode());
			JSONObject jsonObject = new JSONObject(EntityUtils.toString(responseEntity));
			Assert.assertTrue(jsonObject.getString("status").equals("error"), "error, You have logged in");
			Assert.assertTrue(jsonObject.getString("msg").equals("User already exists"), "error, You have logged in");
			Assert.assertTrue(jsonObject.getString("code").equals("107"), "error, You have logged in");
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		finally{
			httpClient.close();
		}
	}

	@Test(groups={"register", "bumblebee"})
	public void nullFirstNameRegisterTest() throws ClientProtocolException, IOException{
		CloseableHttpClient httpClient = HttpClients.createDefault();
		HttpPost httpPost = new HttpPost(BumblebeeUtils.makeString("register"));

		try{

			EntityBuilder builder = EntityBuilder.create();
			List<NameValuePair> data = new ArrayList<NameValuePair>();
			data.add(new BasicNameValuePair("email", BumblebeeUtils.getFromProperty("email")));

			data.add(new BasicNameValuePair("last_name", BumblebeeUtils.getFromProperty("lastName")));
			data.add(new BasicNameValuePair("phone_number", BumblebeeUtils.getFromProperty("phone")));
			if(BumblebeeUtils.makeString("register").contains("staging")){
				data.add(new BasicNameValuePair("hotel_id", BumblebeeUtils.getFromProperty("hotel_id")));
			}
			else{
				data.add(new BasicNameValuePair("hotel_id", BumblebeeUtils.getFromProperty("prod_hotel_id")));
			}
			data.add(new BasicNameValuePair("title", BumblebeeUtils.getFromProperty("title")));
			builder.setParameters(data);
			HttpEntity entity = builder.build();
			httpPost.setEntity(entity);

			HttpResponse response = httpClient.execute(httpPost);
			HttpEntity responseEntity = response.getEntity();

			Assert.assertTrue(response.getStatusLine().getStatusCode() == 200,"Response code is "+response.getStatusLine().getStatusCode());
			JSONObject jsonObject = new JSONObject(EntityUtils.toString(responseEntity));
			Assert.assertTrue(jsonObject.getString("status").equals("error"), "error, You have logged in");
			Assert.assertTrue(jsonObject.getString("msg").equals("Unauthorized Access. Please login"), "error, You have logged in");
			Assert.assertTrue(jsonObject.getString("code").equals("118"), "error, You have logged in");
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		finally{
			httpClient.close();
		}
	}

	@Test(groups={"register", "bumblebee"})
	public void nullLastNameRegisterTest() throws ClientProtocolException, IOException{
		CloseableHttpClient httpClient = HttpClients.createDefault();
		HttpPost httpPost = new HttpPost(BumblebeeUtils.makeString("register"));

		try{

			EntityBuilder builder = EntityBuilder.create();
			List<NameValuePair> data = new ArrayList<NameValuePair>();
			data.add(new BasicNameValuePair("email", BumblebeeUtils.getFromProperty("email")));
			data.add(new BasicNameValuePair("first_name", BumblebeeUtils.getFromProperty("firstName")));

			data.add(new BasicNameValuePair("phone_number", BumblebeeUtils.getFromProperty("phone")));
			if(BumblebeeUtils.makeString("register").contains("staging")){
				data.add(new BasicNameValuePair("hotel_id", BumblebeeUtils.getFromProperty("hotel_id")));
			}
			else{
				data.add(new BasicNameValuePair("hotel_id", BumblebeeUtils.getFromProperty("prod_hotel_id")));
			}
			data.add(new BasicNameValuePair("title", BumblebeeUtils.getFromProperty("title")));
			builder.setParameters(data);
			HttpEntity entity = builder.build();
			httpPost.setEntity(entity);

			HttpResponse response = httpClient.execute(httpPost);
			HttpEntity responseEntity = response.getEntity();

			Assert.assertTrue(response.getStatusLine().getStatusCode() == 200,"Response code is "+response.getStatusLine().getStatusCode());
			JSONObject jsonObject = new JSONObject(EntityUtils.toString(responseEntity));
			Assert.assertTrue(jsonObject.getString("status").equals("error"), "error, You have logged in");
			Assert.assertTrue(jsonObject.getString("msg").equals("Last name cannot be Empty"), "error, You have logged in");
			Assert.assertTrue(jsonObject.getString("code").equals("117"), "error, You have logged in");
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		finally{
			httpClient.close();
		}
	}

	@Test(groups={"register", "bumblebee"})
	public void nullPhoneNumberRegisterTest() throws ClientProtocolException, IOException{
		CloseableHttpClient httpClient = HttpClients.createDefault();
		HttpPost httpPost = new HttpPost(BumblebeeUtils.makeString("register"));

		try{

			EntityBuilder builder = EntityBuilder.create();
			List<NameValuePair> data = new ArrayList<NameValuePair>();
			data.add(new BasicNameValuePair("email", BumblebeeUtils.getFromProperty("email")));
			data.add(new BasicNameValuePair("first_name", BumblebeeUtils.getFromProperty("firstName")));
			data.add(new BasicNameValuePair("last_name", BumblebeeUtils.getFromProperty("lastName")));

			if(BumblebeeUtils.makeString("register").contains("staging")){
				data.add(new BasicNameValuePair("hotel_id", BumblebeeUtils.getFromProperty("hotel_id")));
			}
			else{
				data.add(new BasicNameValuePair("hotel_id", BumblebeeUtils.getFromProperty("prod_hotel_id")));
			}
			data.add(new BasicNameValuePair("title", BumblebeeUtils.getFromProperty("title")));
			builder.setParameters(data);
			HttpEntity entity = builder.build();
			httpPost.setEntity(entity);

			HttpResponse response = httpClient.execute(httpPost);
			HttpEntity responseEntity = response.getEntity();

			Assert.assertTrue(response.getStatusLine().getStatusCode() == 200,"Response code is "+response.getStatusLine().getStatusCode());
			JSONObject jsonObject = new JSONObject(EntityUtils.toString(responseEntity));
			Assert.assertTrue(jsonObject.getString("status").equals("error"), "error, You have logged in");
			Assert.assertTrue(jsonObject.getString("msg").equals("Invalid phone number"), "error, You have logged in");
			Assert.assertTrue(jsonObject.getString("code").equals("121"), "error, You have logged in");
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		finally{
			httpClient.close();
		}
	}

	@Test(groups={"register", "bumblebee"})
	public void wrongPhoneNumberRegisterTest() throws ClientProtocolException, IOException{
		CloseableHttpClient httpClient = HttpClients.createDefault();
		HttpPost httpPost = new HttpPost(BumblebeeUtils.makeString("register"));

		try{

			EntityBuilder builder = EntityBuilder.create();
			List<NameValuePair> data = new ArrayList<NameValuePair>();
			data.add(new BasicNameValuePair("email", BumblebeeUtils.getFromProperty("email")));
			data.add(new BasicNameValuePair("first_name", BumblebeeUtils.getFromProperty("firstName")));
			data.add(new BasicNameValuePair("last_name", BumblebeeUtils.getFromProperty("lastName")));
			data.add(new BasicNameValuePair("phone_number", BumblebeeUtils.getFromProperty("wrongPhone")));
			if(BumblebeeUtils.makeString("register").contains("staging")){
				data.add(new BasicNameValuePair("hotel_id", BumblebeeUtils.getFromProperty("hotel_id")));
			}
			else{
				data.add(new BasicNameValuePair("hotel_id", BumblebeeUtils.getFromProperty("prod_hotel_id")));
			}
			data.add(new BasicNameValuePair("title", BumblebeeUtils.getFromProperty("title")));
			builder.setParameters(data);
			HttpEntity entity = builder.build();
			httpPost.setEntity(entity);

			HttpResponse response = httpClient.execute(httpPost);
			HttpEntity responseEntity = response.getEntity();

			Assert.assertTrue(response.getStatusLine().getStatusCode() == 200,"Response code is "+response.getStatusLine().getStatusCode());
			JSONObject jsonObject = new JSONObject(EntityUtils.toString(responseEntity));
			Assert.assertTrue(jsonObject.getString("status").equals("error"), "error, You have logged in");
			Assert.assertTrue(jsonObject.getString("msg").equals("Ensure this field has no more than 12 characters."), "error, You have logged in");
			Assert.assertTrue(jsonObject.getString("code").equals("101"), "error, You have logged in");
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		finally{
			httpClient.close();
		}
	}

	@Test(groups={"register", "bumblebee"})
	public void invalidHotelIDRegisterTest() throws IOException{
		CloseableHttpClient httpClient = HttpClients.createDefault();
		HttpPost httpPost = new HttpPost(BumblebeeUtils.makeString("register"));

		try{

			EntityBuilder builder = EntityBuilder.create();
			List<NameValuePair> data = new ArrayList<NameValuePair>();
			data.add(new BasicNameValuePair("email", BumblebeeUtils.getFromProperty("email")));
			data.add(new BasicNameValuePair("first_name", BumblebeeUtils.getFromProperty("firstName")));
			data.add(new BasicNameValuePair("last_name", BumblebeeUtils.getFromProperty("lastName")));
			data.add(new BasicNameValuePair("phone_number", BumblebeeUtils.getFromProperty("phone")));
			data.add(new BasicNameValuePair("hotel_id", ""));
			data.add(new BasicNameValuePair("title", BumblebeeUtils.getFromProperty("title")));
			builder.setParameters(data);
			HttpEntity entity = builder.build();
			httpPost.setEntity(entity);
			HttpResponse response = httpClient.execute(httpPost);
			HttpEntity responseEntity = response.getEntity();

			Assert.assertTrue(response.getStatusLine().getStatusCode() == 200,"Response code is ");
			JSONObject jsonObject = new JSONObject(EntityUtils.toString(responseEntity));

			Assert.assertTrue(jsonObject.getString("status").equals("error"), "error, You have logged in "+jsonObject.getString("status")+".");
			Assert.assertTrue(jsonObject.getString("msg").equals("A valid integer is required."), "error,"+jsonObject.getString("msg") );
			Assert.assertTrue(jsonObject.getString("code").equals("101"), "error, You have logged in");


		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		finally{
			httpClient.close();

		}
	}

	@Test(groups={"register", "bumblebee"})
	public void wrongHotelIDRegisterTest() throws IOException{
		CloseableHttpClient httpClient = HttpClients.createDefault();
		HttpPost httpPost = new HttpPost(BumblebeeUtils.makeString("register"));

		try{

			EntityBuilder builder = EntityBuilder.create();
			List<NameValuePair> data = new ArrayList<NameValuePair>();
			data.add(new BasicNameValuePair("email", BumblebeeUtils.getFromProperty("email")));
			data.add(new BasicNameValuePair("first_name", BumblebeeUtils.getFromProperty("firstName")));
			data.add(new BasicNameValuePair("last_name", BumblebeeUtils.getFromProperty("lastName")));
			data.add(new BasicNameValuePair("phone_number", BumblebeeUtils.getFromProperty("phone")));
			data.add(new BasicNameValuePair("hotel_id", BumblebeeUtils.getFromProperty("wrongHotelID")));
			data.add(new BasicNameValuePair("title", BumblebeeUtils.getFromProperty("title")));
			builder.setParameters(data);
			HttpEntity entity = builder.build();
			httpPost.setEntity(entity);

			HttpResponse response = httpClient.execute(httpPost);
			HttpEntity responseEntity = response.getEntity();

			Assert.assertTrue(response.getStatusLine().getStatusCode() == 200,"Response code is ");
			JSONObject jsonObject = new JSONObject(EntityUtils.toString(responseEntity));

			Assert.assertTrue(jsonObject.getString("status").equals("error"), "error, You have logged in");
			Assert.assertTrue(jsonObject.getString("msg").equals("Hotel matching query does not exist."), "error, You have logged in");
			Assert.assertTrue(jsonObject.getString("code").equals("101"), "error, You have logged in");


		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		finally{
			httpClient.close();

		}
	}

	@Test(groups={"register", "bumblebee"})
	public void nullTitleRegisterTest() throws IOException{
		CloseableHttpClient httpClient = HttpClients.createDefault();
		HttpPost httpPost = new HttpPost(BumblebeeUtils.makeString("register"));

		try{

			EntityBuilder builder = EntityBuilder.create();
			List<NameValuePair> data = new ArrayList<NameValuePair>();
			data.add(new BasicNameValuePair("email", BumblebeeUtils.getFromProperty("email")));
			data.add(new BasicNameValuePair("first_name", BumblebeeUtils.getFromProperty("firstName")));
			data.add(new BasicNameValuePair("last_name", BumblebeeUtils.getFromProperty("lastName")));
			data.add(new BasicNameValuePair("phone_number", BumblebeeUtils.getFromProperty("phone")));
			if(BumblebeeUtils.makeString("register").contains("staging")){
				data.add(new BasicNameValuePair("hotel_id", BumblebeeUtils.getFromProperty("hotel_id")));
			}
			else{
				data.add(new BasicNameValuePair("hotel_id", BumblebeeUtils.getFromProperty("prod_hotel_id")));
			}

			builder.setParameters(data);
			HttpEntity entity = builder.build();
			httpPost.setEntity(entity);

			HttpResponse response = httpClient.execute(httpPost);
			HttpEntity responseEntity = response.getEntity();

			Assert.assertTrue(response.getStatusLine().getStatusCode() == 200,"Response code is ");
			JSONObject jsonObject = new JSONObject(EntityUtils.toString(responseEntity));

			Assert.assertTrue(jsonObject.getString("status").equals("error"), "error, You have logged in");
			Assert.assertTrue(jsonObject.getString("msg").equals("Invalid Title Type"), "error, You have logged in");
			Assert.assertTrue(jsonObject.getString("code").equals("129"), "error, You have logged in");


		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		finally{
			httpClient.close();

		}
	}

	@Test(groups={"register", "bumblebee"})
	public void wrongTitleRegisterTest() throws IOException{
		CloseableHttpClient httpClient = HttpClients.createDefault();
		HttpPost httpPost = new HttpPost(BumblebeeUtils.makeString("register"));

		try{
			// 			create a random string here
			EntityBuilder builder = EntityBuilder.create();
			List<NameValuePair> data = new ArrayList<NameValuePair>();
			data.add(new BasicNameValuePair("email", BumblebeeUtils.getFromProperty("email")));
			data.add(new BasicNameValuePair("first_name", BumblebeeUtils.getFromProperty("firstName")));
			data.add(new BasicNameValuePair("last_name", BumblebeeUtils.getFromProperty("lastName")));
			data.add(new BasicNameValuePair("phone_number", BumblebeeUtils.getFromProperty("phone")));
			if(BumblebeeUtils.makeString("register").contains("staging")){
				data.add(new BasicNameValuePair("hotel_id", BumblebeeUtils.getFromProperty("hotel_id")));
			}
			else{
				data.add(new BasicNameValuePair("hotel_id", BumblebeeUtils.getFromProperty("prod_hotel_id")));
			}
			data.add(new BasicNameValuePair("title", BumblebeeUtils.getFromProperty("wrongTitle")));
			builder.setParameters(data);
			HttpEntity entity = builder.build();
			httpPost.setEntity(entity);

			HttpResponse response = httpClient.execute(httpPost);
			HttpEntity responseEntity = response.getEntity();

			Assert.assertTrue(response.getStatusLine().getStatusCode() == 200,"Response code is ");
			JSONObject jsonObject = new JSONObject(EntityUtils.toString(responseEntity));

			Assert.assertTrue(jsonObject.getString("status").equals("error"), "error, You have logged in");
			Assert.assertTrue(jsonObject.getString("msg").equals("list index out of range"), "error, "	+jsonObject.getString("msg"));
			Assert.assertTrue(jsonObject.getString("code").equals("101"), "error, You have logged in");


		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		finally{
			httpClient.close();

		}
	}
}
