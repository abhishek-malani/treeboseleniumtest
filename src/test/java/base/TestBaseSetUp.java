package base;

import java.io.IOException;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.WebDriver;
import org.apache.commons.exec.CommandLine;
import org.apache.commons.exec.DefaultExecuteResultHandler;
import org.apache.commons.exec.DefaultExecutor;
import org.openqa.selenium.Cookie;
import org.openqa.selenium.Dimension;
import org.testng.Assert;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;

public class TestBaseSetUp {
	private BrowserHelper browserHelper;
	Process p;

	@BeforeMethod(alwaysRun = true)
	public void initTest() {
		try {
			WebDriver driver;
			String device = System.getProperty("device");
            System.out.println("device value is "+device);
            //hello ashok
			if (device.equals("mobile")) {
				System.out.println("Starting mobile test");
				startServer();
				startEmulator();
				driver = DriverManager.getInstance().getDriver();
			}else{
				System.out.println("Starting desktop or mobile chrome test");
				
				try{
					driver = DriverManager.getInstance().getDriver();
				}catch(Exception SessionNotCreatedException){
					// in case issue with session, try once more
					driver = DriverManager.getInstance().getDriver();
				}
				

				String host = System.getProperty("host");
				System.out.println("Starting test ...");

				// get thread safe driver from driver Manager for specific browser
				// etc
				// if Desktop/Mobile browser maximize it or set the dimension
				// Get the test URL and open it

				String url = System.getProperty("url");
				if (url == null || url.isEmpty()) {
					url = "https://treebohotels.com";
					System.out.println("Url not provided! Default url is : " + url);
				} else {
					System.out.println("Launching website : " + url);
				}

				long start = System.currentTimeMillis();
				driver.get(url);
				
				// Handling cookie for analytics
				String tracking = System.getProperty("tracking");
				browserHelper = new BrowserHelper();
				if (tracking == null || tracking.isEmpty()){
					browserHelper.addCookie("ignore_analytics", "true");
				}else if(tracking.equals("true")){
					browserHelper.addCookie("ignore_analytics", "false");
				}
				
				long finish = System.currentTimeMillis();
				long totalTime = (finish - start);
				System.out.println("Total Time for open site in mili second - " + totalTime);
				if (host == null || host.isEmpty()) {
					host = "grid";
				}
				if (host.equals("localhost")) {
					System.out.println("Maximizing the browser window for localhost");
					driver.manage().window().maximize();
				} else if (host.equals("grid")) {
					System.out.println("Maximizing the browser window with dimension for remote machine, grid");
					Dimension dimension = new Dimension(1920, 1080);
					driver.manage().window().setSize(dimension);
				}
				// driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
				driver.manage().timeouts().pageLoadTimeout(240, TimeUnit.SECONDS);
			}


		} catch (Exception e) {
			e.printStackTrace();
			Assert.assertTrue(false, "Error in setting up driver!");
		}

	}

	@AfterMethod
	public void afterMethod(ITestResult result) throws IOException {
		System.out.println("Test completed is: " + result.getMethod().getMethodName());
		System.out.println("Test Passed? " + result.isSuccess());
	}

	@AfterMethod(alwaysRun = true)
	public void endTest() {
		String device = System.getProperty("device");
		// for Desktop Browser and Mobile Browser(Through Chrome options)
		// quit the threadlocal driver and remove it
		if (!(System.getProperty("debug").equals("true"))) {
			DriverManager.getInstance().removeDriver();
			if (device.equals("mobile")) {
				stopServer();
				stopEmulator();
			}
		}
	}

	public void startEmulator() throws IOException, InterruptedException {
		// Start the emulator
		System.out.println("starting the genymotion");
		String cmd = "/Applications/Genymotion.app/Contents/MacOS/player.app/Contents/MacOS/player --vm-name nexus9";
		p = Runtime.getRuntime().exec(cmd);
		Thread.sleep(10000);
		System.out.println("started the genymotion");
	}

	public void stopEmulator() {
		p.destroy();
	}

	public void startServer() {
		CommandLine command = new CommandLine("/Applications/Appium.app/Contents/Resources/node/bin/node");
		command.addArgument("/Applications/Appium.app/Contents/Resources/node_modules/appium/bin/appium.js", false);
		command.addArgument("--address", false);
		command.addArgument("127.0.0.1");
		command.addArgument("--port", false);
		command.addArgument("4723");
		command.addArgument("--full-reset", false);

		DefaultExecuteResultHandler resultHandler = new DefaultExecuteResultHandler();
		DefaultExecutor executor = new DefaultExecutor();
		executor.setExitValue(1);
		try {
			executor.execute(command, resultHandler);
			Thread.sleep(5000);
			System.out.println("Appium server started.");
		} catch (IOException e) {
			e.printStackTrace();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	public void stopServer() {
		String[] command = { "/usr/bin/killall", "-KILL", "node" };
		try {
			Runtime.getRuntime().exec(command);
			System.out.println("Appium server stopped.");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}