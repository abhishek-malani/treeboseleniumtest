package mobileSiteUITest;

import java.text.ParseException;

import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import base.BrowserHelper;
import base.TestBaseSetUp;
import mobileSiteUIPageObjects.AccountScreen;
import mobileSiteUIPageObjects.ConfirmationScreen;
import mobileSiteUIPageObjects.HomeScreen;
import mobileSiteUIPageObjects.HotelSearchResultsScreen;
import mobileSiteUIPageObjects.ItineraryScreen;
import utils.CommonUtils;

public class MSiteItineraryPageTest extends TestBaseSetUp {
	private HomeScreen homeScreen;
	private AccountScreen accountScreen;
	private HotelSearchResultsScreen hotelSearchResultScreen;
	private ItineraryScreen itineraryScreen;
	private ConfirmationScreen confirmationScreen;
	private BrowserHelper browserHelper;
	
	/***
	 * open home page search hotel 
	 * click on quick book 
	 * extract date amount discount
	 * open complete view price break up popup
	 * verify details
	 * @throws ParseException 
	 * 
	 */
	@Test(groups = { "MSite","MSiteItinerary" })
	public void testPriceBreakUpDetails() throws ParseException  {
		
		homeScreen = new HomeScreen();
		hotelSearchResultScreen = new HotelSearchResultsScreen();
		itineraryScreen = new ItineraryScreen(); 
		confirmationScreen = new ConfirmationScreen();
		accountScreen = new AccountScreen();
		browserHelper = new BrowserHelper();
		CommonUtils utils = new CommonUtils();
		SoftAssert verify = new SoftAssert();
		
		
		String location = utils.getProperty("city"); // "Bengaluru";
		String testHotel = utils.getProperty("DummyHotel"); // "Elmas";
		String testUrl = utils.getProperty("TestUrl");
		int hotelIndex = 0;
		int checkInFromCurrentDate = utils.getRandomNumber(30, 45);
		int checkOutFromCurrentDate = checkInFromCurrentDate + 2;
		String[] checkInCheckOutDates = new String[2];
		
		
		// Check if home screen is displayed
		verify.assertTrue(homeScreen.isHomeScreenDisplayed(),"Home Screen is not displayed");
		//Do Search
		checkInCheckOutDates = homeScreen.doSearch(location, checkInFromCurrentDate, checkOutFromCurrentDate);
		
		// check if search results displayed
		verify.assertTrue(hotelSearchResultScreen.isHotelSearchScreenDisplayed(),"search result screen not displayed");
		//Select a random hotel or test hotel
		if (browserHelper.getCurrentUrl().contains(testUrl)) {
			hotelIndex = hotelSearchResultScreen.getAllAvailableHotelNames().indexOf(testHotel);
		}else{
			hotelIndex = hotelSearchResultScreen.getRandomIndexOfAvailableHotel();
		}
		
		String hotelName = hotelSearchResultScreen.getHotelNameByIndex(hotelIndex);
		int hotelRoomRate = hotelSearchResultScreen.getRoomRateByIndex(hotelIndex);
		System.out.println("Hotel Name: " + hotelName + " Hotel room rate : " + hotelRoomRate );

		
		//Click quick book 
		hotelSearchResultScreen.clickQuickBookByIndex(hotelIndex);
		
		// check if itinerary page displayed
		verify.assertTrue(itineraryScreen.isItineraryScreenDisplayed(),"Itinerary page is not displayed");
		
		double basePrice = itineraryScreen.getBasePrice();
		double taxPrice =itineraryScreen.getTax();
		String checkIn = utils.formatDateWithoutSuffix(checkInCheckOutDates[0]);
		String checkOut = utils.formatDateWithoutSuffix(checkInCheckOutDates[1]);

		double discount=itineraryScreen.getDiscount();  
		double totalCost=itineraryScreen.getTotalCost();
		System.out.println("Base Price before tax calculate " + basePrice);
		System.out.println("Tax "+ taxPrice);
		System.out.println("checkIn date at itinerary Page" + checkIn);
		System.out.println("checkOut date at itinerary Page "+ checkOut);
		System.out.println("discount at itinerary Page "+ discount);
		System.out.println("totalCost at itinerary Page "+ totalCost);
		
		//click on view complete break up
		itineraryScreen.clickOnViewCompleteBreakUp();
		//verify pop up
		verify.assertTrue(itineraryScreen.doesBreakUpPopUpOpened(),"View Complete break up is not opened");
		//verify checkIn checkout date same on complete view breakup
		verify.assertTrue(itineraryScreen.doesDatesSame(checkIn.trim(), checkOut.trim()),"checkIn checkOut date not match");
		
		verify.assertTrue(itineraryScreen.checkStayNightDateWithCheckInDate(checkIn.trim()),"Stay night date is not match with checkIn date");
	    
		//compare basePrice and total of break up base price
		verify.assertTrue(itineraryScreen.checkBasePriceEqualWithBreakPrice(),"break price calculate is not matching with base price on popup");
		//compare total price and total of break up final price
		verify.assertTrue(itineraryScreen.checkTotalPriceEqualWithBreakFinalPrice(),"final price calculated from break up is not match with total price on pop up");
		//check  total price is sum of base price and tax
		verify.assertTrue(itineraryScreen.checkTotalPrice(),"calculate total price is not equal sum of base price and tax");
		//check inclusive all tax text displayed
		verify.assertTrue(itineraryScreen.isInclusiveTaxTextDisplayed(),"Inclusive all taxes text is not displayed");
		verify.assertTrue(itineraryScreen.isDoneButtonDisplayed(),"Done button is not displayed");
		itineraryScreen.clickOnDoneButton();
		//Complete view price break up closed 
		verify.assertFalse(itineraryScreen.doesBreakUpPopUpOpened(),"View Complete Break Up is still opened");
		verify.assertAll();
	}

}
