package utils;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.ParseException;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.entity.EntityBuilder;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.joda.time.DateTime;
import org.joda.time.Period;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.json.JSONException;
import org.json.JSONObject;
import org.testng.Assert;



public class BumblebeeUtils {

	public static String makeString(String api){
		Properties prop = new Properties();
		InputStream input = null;

		try {

			input = Thread.currentThread().getContextClassLoader().getResourceAsStream("bumblebeeTest.properties");

			// load a properties file
			prop.load(input);

			// get the property value and print it out




		} catch (IOException ex) {
			ex.printStackTrace();
		} finally {
			if (input != null) {
				try {
					input.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		// Get the url from POM, url, if its present if not set the default
		//
		String SysURL = System.getProperty("environment");
		if(SysURL == null){
			String url = prop.getProperty("staging");
			return url+prop.getProperty(api);
		}
		else{
			return prop.getProperty("prod")+prop.getProperty(api);
		}
	}

	public static String getFromProperty(String property){
		Properties prop = new Properties();
		InputStream input = null;

		try {

			input = Thread.currentThread().getContextClassLoader().getResourceAsStream("bumblebeeTest.properties");

			// load a properties file
			prop.load(input);

			// get the property value and print it out




		} catch (IOException ex) {
			ex.printStackTrace();
		} finally {
			if (input != null) {
				try {
					input.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		return prop.getProperty(property);
	}

	public static String getToken() throws ClientProtocolException, IOException, ParseException, JSONException {
		CloseableHttpClient httpClient = HttpClients.createDefault();
		HttpPost httpPost = new HttpPost(BumblebeeUtils.makeString("login"));

		try{

			EntityBuilder builder = EntityBuilder.create();
			List<NameValuePair> data = new ArrayList<NameValuePair>();
			data.add(new BasicNameValuePair("username", getFromProperty("email")));
			data.add(new BasicNameValuePair("password", getFromProperty("password")));
			if(BumblebeeUtils.makeString("register").contains("staging")){
				data.add(new BasicNameValuePair("hotel_id", BumblebeeUtils.getFromProperty("hotel_id")));
			}
			else{
				data.add(new BasicNameValuePair("hotel_id", BumblebeeUtils.getFromProperty("prod_hotel_id")));
			}
			builder.setParameters(data);
			HttpEntity entity = builder.build();
			httpPost.setEntity(entity);

			HttpResponse response = httpClient.execute(httpPost);
			HttpEntity responseEntity = response.getEntity();
			JSONObject jsonObject = new JSONObject(EntityUtils.toString(responseEntity));
			return  jsonObject.getJSONObject("data").getString("access_token");
		}
		finally{
			httpClient.close();
		}

	}

	public static String getCheckinDate() throws IOException{
		CloseableHttpClient httpClient = HttpClients.createDefault(); 
		HttpGet httpGet = new HttpGet(makeString("check_audit"));
		String message="";
		String auditDate= "";
		try{
			httpGet.addHeader("Authorization","Bearer "+ getToken());
			CloseableHttpResponse response = httpClient.execute(httpGet);
			Assert.assertTrue(response.getStatusLine().getStatusCode() == 200,"Response code is "+response.getStatusLine().getStatusCode());
			HttpEntity entity = response.getEntity();

			JSONObject jsonObject = new JSONObject(EntityUtils.toString(entity));
			message = jsonObject.getJSONObject("data").getJSONObject("response").getJSONObject("status").getString("message");
			String[] split = message.split(" ");
			auditDate = split[split.length-1];



		}
		catch (JSONException e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		finally{
			httpClient.close();
		}

		return auditDate;
	}	

	public static String getCheckoutDate(String night) throws IOException{

		String checkin = getCheckinDate();
		DateTimeFormatter dtf = DateTimeFormat.forPattern("YYYY-MM-dd" );
		DateTime jodaTime = dtf.parseDateTime(checkin);
		DateTime checkout = jodaTime.plus(Period.days(Integer.parseInt(night)));
		DateTimeFormatter dtfOut = DateTimeFormat.forPattern("YYYY-MM-dd");
		return dtfOut.print(checkout);
	}

	public static String getMultipleParams(String... params ) throws IOException{
		Properties prop = new Properties();
		InputStream input = null;

		try {

			input = Thread.currentThread().getContextClassLoader().getResourceAsStream("bumblebeeTest.properties");

			// load a properties file
			prop.load(input);

			// get the property value and print it out
		} catch (IOException ex) {	
			ex.printStackTrace();
		} finally {
			if (input != null) {
				try {
					input.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}

		String url = makeString("availability");
		for(String iter: params){

			if(iter.contains("checkin")){
				if(iter.contains("=")){
					String[] tokens = iter.split(" |=");
					Map<String, String> map = new HashMap<String, String>();
					for(int i=0; i<tokens.length-1; )map.put(tokens[i++], tokens[i++]);
					for(String key : map.keySet()){
						url+="&"+key+"="+map.get(key);
					}
				}
					else {
				url += "checkin_date="+getCheckinDate();
					}
			}
			else if(iter.contains("nights")){
				String[] tokens = iter.split(" |=");
				Map<String, String> map = new HashMap<String, String>();
				for(int i=0; i<tokens.length-1; )map.put(tokens[i++], tokens[i++]);
				for(String key : map.keySet()){
					url+="&checkout_date="+getCheckoutDate(map.get(key));
				}
			}
			else if(iter.contains("hotel_id")){
				if(iter.contains("=")){
					String[] tokens = iter.split(" |=");
					Map<String, String> map = new HashMap<String, String>();
					for(int i=0; i<tokens.length-1; )map.put(tokens[i++], tokens[i++]);
					for(String key : map.keySet()){
						url+="&"+key+"="+map.get(key);
					}
				}
					else if(makeString("availability").contains("staging")){
					url+="&hotel_id="+prop.getProperty("hotel_id");
				}
				else{
					url+="&hotel_id="+prop.getProperty("prod_hotel_id");
				}
			}
			else if(iter.contains("=")){
				String[] tokens = iter.split(" |=");
				Map<String, String> map = new HashMap<String, String>();
				for(int i=0; i<tokens.length-1; )map.put(tokens[i++], tokens[i++]);
				for(String key : map.keySet()){
					url+="&"+key+"="+map.get(key);
				}

			}	

			else{
				url += "&"+iter+"="+prop.getProperty(iter);
			}

		}

		return url;
	}




	public static void main(String[] args) throws ClientProtocolException, IOException{

					System.out.println(getMultipleParams("checkin_date","nights=4","adult=4", "child=6", "no_of_rooms=2","hotel_id","user_id"));
//		System.out.println(getCheckoutDate("2"));


	}
}


