package mobileSiteUITest;

import java.util.List;

import org.testng.Assert;
import org.testng.asserts.SoftAssert;
import org.testng.annotations.Test;

import base.BrowserHelper;
import base.TestBaseSetUp;
import desktopUIPageObjects.HomePage;
import mobileSiteUIPageObjects.HomeScreen;
import mobileSiteUIPageObjects.HotelSearchResultsScreen;
import mobileSiteUIPageObjects.HotelDetailsScreen;

public class MSiteHomeTest extends TestBaseSetUp {

	private HomeScreen homeScreen;
	private HotelSearchResultsScreen hotelSearchResultScreen;
	private HotelDetailsScreen hotelDetailsScreen;
	private BrowserHelper browserHelper;

	/**
	 * This test verifies that spotlight section is displayed
	 */

	@Test(groups = { "MSite", "MSiteHome" })
	public void testMobileHomeSpotLightDisplayed() {
		homeScreen = new HomeScreen();
		Assert.assertTrue(homeScreen.isSpotLightDisplayed());
	}

	/**
	 * This test verifies that city sections is displayed Click on each city
	 * link and verifies it opens search page
	 */

	@Test(groups = { "MSite", "MSiteHome" })
	public void testMobileCityLinks() {
		homeScreen = new HomeScreen();
		hotelSearchResultScreen = new HotelSearchResultsScreen();
		SoftAssert verify = new SoftAssert();

		// Get the city links
		List<String> arrCity = homeScreen.getCityLinks();
		for (String city : arrCity) {
			homeScreen.clickOnCityLink(city);
			verify.assertTrue(hotelSearchResultScreen.isHotelSearchScreenDisplayed(),
					"Hotel Search Screen is not displayed on clicking city link " + city);
			verify.assertTrue(hotelSearchResultScreen.isHotelResultDisplayed(), "Results not displayed!!!");
			homeScreen.goToHomeScreen();
		}
		verify.assertAll();
	}

	/**
	 * This test verifies that every hotel HD screen loads properly
	 */

	@Test(groups = { "MSite", "MSiteHome" })
	public void testMobileHotelHDLoadsBengaluru() {
		homeScreen = new HomeScreen();
		hotelSearchResultScreen = new HotelSearchResultsScreen();
		hotelDetailsScreen = new HotelDetailsScreen();

		SoftAssert verify = new SoftAssert();
		String city = "Bengaluru";

		homeScreen.clickOnCityLink(city);
		verify.assertTrue(hotelSearchResultScreen.isHotelSearchScreenDisplayed(),
				"Hotel Search Screen is not displayed on clicking city link " + city);
		verify.assertTrue(hotelSearchResultScreen.isHotelResultDisplayed(), "Results not displayed!!!");
		verify.assertTrue(hotelSearchResultScreen.isSeoCityContentDisplayed(), "SEO Content not displayed!!!");
		int countOfHotels = hotelSearchResultScreen.getCountOfTotalHotelDisplayed();
		
        for (int i = 1; i <= countOfHotels ; i++){
        	String hotelName = hotelSearchResultScreen.getHotelNameByIndex(i);
        	hotelSearchResultScreen.clickHotelByIndex(i);
    		verify.assertTrue(hotelDetailsScreen.isHDScreenDisplayed(),"HD Screen is not displayed");
    		verify.assertEquals(hotelDetailsScreen.getHotelNameInHDScreen(), hotelName,"Hotel Name is wrong in HD");
    		homeScreen.goToHomeScreen();
    		homeScreen.clickOnCityLink(city);
        }
		verify.assertAll();
	}
	
	/**
	 * This test verifies that every hotel HD screen loads properly
	 */

	@Test(groups = { "MSite", "MSiteHome" })
	public void testMobileHotelHDLoadsHyderabad() {
		homeScreen = new HomeScreen();
		hotelSearchResultScreen = new HotelSearchResultsScreen();
		hotelDetailsScreen = new HotelDetailsScreen();

		SoftAssert verify = new SoftAssert();
		String city = "Hyderabad";

		homeScreen.clickOnCityLink(city);
		verify.assertTrue(hotelSearchResultScreen.isHotelSearchScreenDisplayed(),
				"Hotel Search Screen is not displayed on clicking city link " + city);
		verify.assertTrue(hotelSearchResultScreen.isHotelResultDisplayed(), "Results not displayed!!!");
		verify.assertTrue(hotelSearchResultScreen.isSeoCityContentDisplayed(), "SEO Content not displayed!!!");
		int countOfHotels = hotelSearchResultScreen.getCountOfTotalHotelDisplayed();
		
        for (int i = 1; i <= countOfHotels ; i++){
        	String hotelName = hotelSearchResultScreen.getHotelNameByIndex(i);
        	hotelSearchResultScreen.clickHotelByIndex(i);
    		verify.assertTrue(hotelDetailsScreen.isHDScreenDisplayed(),"HD Screen is not displayed");
    		verify.assertEquals(hotelDetailsScreen.getHotelNameInHDScreen(), hotelName,"Hotel Name is wrong in HD");
    		homeScreen.goToHomeScreen();
    		homeScreen.clickOnCityLink(city);
        }
		verify.assertAll();
	}
	
	/**
	 * This test verifies that every hotel HD screen loads properly
	 */

	@Test(groups = { "MSite", "MSiteHome" })
	public void testMobileHotelHDLoadsMumbai() {
		homeScreen = new HomeScreen();
		hotelSearchResultScreen = new HotelSearchResultsScreen();
		hotelDetailsScreen = new HotelDetailsScreen();

		SoftAssert verify = new SoftAssert();
		String city = "Mumbai";

		homeScreen.clickOnCityLink(city);
		verify.assertTrue(hotelSearchResultScreen.isHotelSearchScreenDisplayed(),
				"Hotel Search Screen is not displayed on clicking city link " + city);
		verify.assertTrue(hotelSearchResultScreen.isHotelResultDisplayed(), "Results not displayed!!!");
		verify.assertTrue(hotelSearchResultScreen.isSeoCityContentDisplayed(), "SEO Content not displayed!!!");
		int countOfHotels = hotelSearchResultScreen.getCountOfTotalHotelDisplayed();
		
        for (int i = 1; i <= countOfHotels ; i++){
        	String hotelName = hotelSearchResultScreen.getHotelNameByIndex(i);
        	hotelSearchResultScreen.clickHotelByIndex(i);
    		verify.assertTrue(hotelDetailsScreen.isHDScreenDisplayed(),"HD Screen is not displayed");
    		verify.assertEquals(hotelDetailsScreen.getHotelNameInHDScreen(), hotelName,"Hotel Name is wrong in HD");
    		homeScreen.goToHomeScreen();
    		homeScreen.clickOnCityLink(city);
        }
		verify.assertAll();
	}
	
	/**
	 * This test verifies that every hotel HD screen loads properly
	 */

	@Test(groups = { "MSite", "MSiteHome" })
	public void testMobileHotelHDLoadsChennai() {
		homeScreen = new HomeScreen();
		hotelSearchResultScreen = new HotelSearchResultsScreen();
		hotelDetailsScreen = new HotelDetailsScreen();

		SoftAssert verify = new SoftAssert();
		String city = "Chennai";

		homeScreen.clickOnCityLink(city);
		verify.assertTrue(hotelSearchResultScreen.isHotelSearchScreenDisplayed(),
				"Hotel Search Screen is not displayed on clicking city link " + city);
		verify.assertTrue(hotelSearchResultScreen.isHotelResultDisplayed(), "Results not displayed!!!");
		verify.assertTrue(hotelSearchResultScreen.isSeoCityContentDisplayed(), "SEO Content not displayed!!!");
		int countOfHotels = hotelSearchResultScreen.getCountOfTotalHotelDisplayed();
		
        for (int i = 1; i <= countOfHotels ; i++){
        	String hotelName = hotelSearchResultScreen.getHotelNameByIndex(i);
        	hotelSearchResultScreen.clickHotelByIndex(i);
    		verify.assertTrue(hotelDetailsScreen.isHDScreenDisplayed(),"HD Screen is not displayed");
    		verify.assertEquals(hotelDetailsScreen.getHotelNameInHDScreen(), hotelName,"Hotel Name is wrong in HD");
    		homeScreen.goToHomeScreen();
    		homeScreen.clickOnCityLink(city);
        }
		verify.assertAll();
	}
	
	/**
	 * This test verifies that every hotel HD screen loads properly
	 */

	@Test(groups = { "MSite", "MSiteHome" })
	public void testMobileHotelHDLoadsJaipur() {
		homeScreen = new HomeScreen();
		hotelSearchResultScreen = new HotelSearchResultsScreen();
		hotelDetailsScreen = new HotelDetailsScreen();

		SoftAssert verify = new SoftAssert();
		String city = "Jaipur";

		homeScreen.clickOnCityLink(city);
		verify.assertTrue(hotelSearchResultScreen.isHotelSearchScreenDisplayed(),
				"Hotel Search Screen is not displayed on clicking city link " + city);
		verify.assertTrue(hotelSearchResultScreen.isHotelResultDisplayed(), "Results not displayed!!!");
		verify.assertTrue(hotelSearchResultScreen.isSeoCityContentDisplayed(), "SEO Content not displayed!!!");
		int countOfHotels = hotelSearchResultScreen.getCountOfTotalHotelDisplayed();
		
        for (int i = 1; i <= countOfHotels ; i++){
        	String hotelName = hotelSearchResultScreen.getHotelNameByIndex(i);
        	hotelSearchResultScreen.clickHotelByIndex(i);
    		verify.assertTrue(hotelDetailsScreen.isHDScreenDisplayed(),"HD Screen is not displayed");
    		verify.assertEquals(hotelDetailsScreen.getHotelNameInHDScreen(), hotelName,"Hotel Name is wrong in HD");
    		homeScreen.goToHomeScreen();
    		homeScreen.clickOnCityLink(city);
        }
		verify.assertAll();
	}
	
	/**
	 * This test verifies that every hotel HD screen loads properly
	 */

	@Test(groups = { "MSite", "MSiteHome" })
	public void testMobileHotelHDLoadsNewDelhi() {
		homeScreen = new HomeScreen();
		hotelSearchResultScreen = new HotelSearchResultsScreen();
		hotelDetailsScreen = new HotelDetailsScreen();

		SoftAssert verify = new SoftAssert();
		String city = "New Delhi";

		homeScreen.clickOnCityLink(city);
		verify.assertTrue(hotelSearchResultScreen.isHotelSearchScreenDisplayed(),
				"Hotel Search Screen is not displayed on clicking city link " + city);
		verify.assertTrue(hotelSearchResultScreen.isHotelResultDisplayed(), "Results not displayed!!!");
		verify.assertTrue(hotelSearchResultScreen.isSeoCityContentDisplayed(), "SEO Content not displayed!!!");
		int countOfHotels = hotelSearchResultScreen.getCountOfTotalHotelDisplayed();
		
        for (int i = 1; i <= countOfHotels ; i++){
        	String hotelName = hotelSearchResultScreen.getHotelNameByIndex(i);
        	hotelSearchResultScreen.clickHotelByIndex(i);
    		verify.assertTrue(hotelDetailsScreen.isHDScreenDisplayed(),"HD Screen is not displayed");
    		verify.assertEquals(hotelDetailsScreen.getHotelNameInHDScreen(), hotelName,"Hotel Name is wrong in HD");
    		homeScreen.goToHomeScreen();
    		homeScreen.clickOnCityLink(city);
        }
		verify.assertAll();
	}
	
	/**
	 * This test verifies that every hotel HD screen loads properly
	 */

	@Test(groups = { "MSite", "MSiteHome" })
	public void testMobileHotelHDLoadsPune() {
		homeScreen = new HomeScreen();
		hotelSearchResultScreen = new HotelSearchResultsScreen();
		hotelDetailsScreen = new HotelDetailsScreen();

		SoftAssert verify = new SoftAssert();
		String city = "Pune";

		homeScreen.clickOnCityLink(city);
		verify.assertTrue(hotelSearchResultScreen.isHotelSearchScreenDisplayed(),
				"Hotel Search Screen is not displayed on clicking city link " + city);
		verify.assertTrue(hotelSearchResultScreen.isHotelResultDisplayed(), "Results not displayed!!!");
		verify.assertTrue(hotelSearchResultScreen.isSeoCityContentDisplayed(), "SEO Content not displayed!!!");
		int countOfHotels = hotelSearchResultScreen.getCountOfTotalHotelDisplayed();
		
        for (int i = 1; i <= countOfHotels ; i++){
        	String hotelName = hotelSearchResultScreen.getHotelNameByIndex(i);
        	hotelSearchResultScreen.clickHotelByIndex(i);
    		verify.assertTrue(hotelDetailsScreen.isHDScreenDisplayed(),"HD Screen is not displayed");
    		verify.assertEquals(hotelDetailsScreen.getHotelNameInHDScreen(), hotelName,"Hotel Name is wrong in HD");
    		homeScreen.goToHomeScreen();
    		homeScreen.clickOnCityLink(city);
        }
		verify.assertAll();
	}
	
	/**
	 * This test verifies that every hotel HD screen loads properly
	 */

	@Test(groups = { "MSite", "MSiteHome" })
	public void testMobileHotelHDLoadsGoa() {
		homeScreen = new HomeScreen();
		hotelSearchResultScreen = new HotelSearchResultsScreen();
		hotelDetailsScreen = new HotelDetailsScreen();

		SoftAssert verify = new SoftAssert();
		String city = "Goa";

		homeScreen.clickOnCityLink(city);
		verify.assertTrue(hotelSearchResultScreen.isHotelSearchScreenDisplayed(),
				"Hotel Search Screen is not displayed on clicking city link " + city);
		verify.assertTrue(hotelSearchResultScreen.isHotelResultDisplayed(), "Results not displayed!!!");
		verify.assertTrue(hotelSearchResultScreen.isSeoCityContentDisplayed(), "SEO Content not displayed!!!");
		int countOfHotels = hotelSearchResultScreen.getCountOfTotalHotelDisplayed();
		
        for (int i = 1; i <= countOfHotels ; i++){
        	String hotelName = hotelSearchResultScreen.getHotelNameByIndex(i);
        	hotelSearchResultScreen.clickHotelByIndex(i);
    		verify.assertTrue(hotelDetailsScreen.isHDScreenDisplayed(),"HD Screen is not displayed");
    		verify.assertEquals(hotelDetailsScreen.getHotelNameInHDScreen(), hotelName,"Hotel Name is wrong in HD");
    		homeScreen.goToHomeScreen();
    		homeScreen.clickOnCityLink(city);
        }
		verify.assertAll();
	}
	
	/**
	 * This test verifies that every hotel HD screen loads properly
	 */

	@Test(groups = { "MSite", "MSiteHome" })
	public void testMobileHotelHDLoadsKolkata() {
		homeScreen = new HomeScreen();
		hotelSearchResultScreen = new HotelSearchResultsScreen();
		hotelDetailsScreen = new HotelDetailsScreen();

		SoftAssert verify = new SoftAssert();
		String city = "Kolkata";

		homeScreen.clickOnCityLink(city);
		verify.assertTrue(hotelSearchResultScreen.isHotelSearchScreenDisplayed(),
				"Hotel Search Screen is not displayed on clicking city link " + city);
		verify.assertTrue(hotelSearchResultScreen.isHotelResultDisplayed(), "Results not displayed!!!");
		verify.assertTrue(hotelSearchResultScreen.isSeoCityContentDisplayed(), "SEO Content not displayed!!!");
		int countOfHotels = hotelSearchResultScreen.getCountOfTotalHotelDisplayed();
		
        for (int i = 1; i <= countOfHotels ; i++){
        	String hotelName = hotelSearchResultScreen.getHotelNameByIndex(i);
        	hotelSearchResultScreen.clickHotelByIndex(i);
    		verify.assertTrue(hotelDetailsScreen.isHDScreenDisplayed(),"HD Screen is not displayed");
    		verify.assertEquals(hotelDetailsScreen.getHotelNameInHDScreen(), hotelName,"Hotel Name is wrong in HD");
    		homeScreen.goToHomeScreen();
    		homeScreen.clickOnCityLink(city);
        }
		verify.assertAll();
	}
	
	/**
	 * This test verifies that every hotel HD screen loads properly
	 */

	@Test(groups = { "MSite", "MSiteHome" })
	public void testMobileHotelHDLoadsMysore() {
		homeScreen = new HomeScreen();
		hotelSearchResultScreen = new HotelSearchResultsScreen();
		hotelDetailsScreen = new HotelDetailsScreen();

		SoftAssert verify = new SoftAssert();
		String city = "Mysore";

		homeScreen.clickOnCityLink(city);
		verify.assertTrue(hotelSearchResultScreen.isHotelSearchScreenDisplayed(),
				"Hotel Search Screen is not displayed on clicking city link " + city);
		verify.assertTrue(hotelSearchResultScreen.isHotelResultDisplayed(), "Results not displayed!!!");
		verify.assertTrue(hotelSearchResultScreen.isSeoCityContentDisplayed(), "SEO Content not displayed!!!");
		int countOfHotels = hotelSearchResultScreen.getCountOfTotalHotelDisplayed();
		
        for (int i = 1; i <= countOfHotels ; i++){
        	String hotelName = hotelSearchResultScreen.getHotelNameByIndex(i);
        	hotelSearchResultScreen.clickHotelByIndex(i);
    		verify.assertTrue(hotelDetailsScreen.isHDScreenDisplayed(),"HD Screen is not displayed");
    		verify.assertEquals(hotelDetailsScreen.getHotelNameInHDScreen(), hotelName,"Hotel Name is wrong in HD");
    		homeScreen.goToHomeScreen();
    		homeScreen.clickOnCityLink(city);
        }
		verify.assertAll();
	}
	
	/**
	 * This test verifies that every hotel HD screen loads properly
	 */

	@Test(groups = { "MSite", "MSiteHome" })
	public void testMobileHotelHDLoadsGurgaon() {
		homeScreen = new HomeScreen();
		hotelSearchResultScreen = new HotelSearchResultsScreen();
		hotelDetailsScreen = new HotelDetailsScreen();

		SoftAssert verify = new SoftAssert();
		String city = "Gurgaon";

		homeScreen.clickOnCityLink(city);
		verify.assertTrue(hotelSearchResultScreen.isHotelSearchScreenDisplayed(),
				"Hotel Search Screen is not displayed on clicking city link " + city);
		verify.assertTrue(hotelSearchResultScreen.isHotelResultDisplayed(), "Results not displayed!!!");
		verify.assertTrue(hotelSearchResultScreen.isSeoCityContentDisplayed(), "SEO Content not displayed!!!");
		int countOfHotels = hotelSearchResultScreen.getCountOfTotalHotelDisplayed();
		
        for (int i = 1; i <= countOfHotels ; i++){
        	String hotelName = hotelSearchResultScreen.getHotelNameByIndex(i);
        	hotelSearchResultScreen.clickHotelByIndex(i);
    		verify.assertTrue(hotelDetailsScreen.isHDScreenDisplayed(),"HD Screen is not displayed");
    		verify.assertEquals(hotelDetailsScreen.getHotelNameInHDScreen(), hotelName,"Hotel Name is wrong in HD");
    		homeScreen.goToHomeScreen();
    		homeScreen.clickOnCityLink(city);
        }
		verify.assertAll();
	}
	
	/**
	 * This test verifies that every hotel HD screen loads properly
	 */

	@Test(groups = { "MSite", "MSiteHome" })
	public void testMobileHotelHDLoadsOoty() {
		homeScreen = new HomeScreen();
		hotelSearchResultScreen = new HotelSearchResultsScreen();
		hotelDetailsScreen = new HotelDetailsScreen();

		SoftAssert verify = new SoftAssert();
		String city = "Ooty";

		homeScreen.clickOnCityLink(city);
		verify.assertTrue(hotelSearchResultScreen.isHotelSearchScreenDisplayed(),
				"Hotel Search Screen is not displayed on clicking city link " + city);
		verify.assertTrue(hotelSearchResultScreen.isHotelResultDisplayed(), "Results not displayed!!!");
		//verify.assertTrue(hotelSearchResultScreen.isSeoCityContentDisplayed(), "SEO Content not displayed!!!");
		int countOfHotels = hotelSearchResultScreen.getCountOfTotalHotelDisplayed();
		
        for (int i = 1; i <= countOfHotels ; i++){
        	String hotelName = hotelSearchResultScreen.getHotelNameByIndex(i);
        	hotelSearchResultScreen.clickHotelByIndex(i);
    		verify.assertTrue(hotelDetailsScreen.isHDScreenDisplayed(),"HD Screen is not displayed");
    		verify.assertEquals(hotelDetailsScreen.getHotelNameInHDScreen(), hotelName,"Hotel Name is wrong in HD");
    		homeScreen.goToHomeScreen();
    		homeScreen.clickOnCityLink(city);
        }
		verify.assertAll();
	}
	
	/**
	 * This test verifies that every hotel HD screen loads properly
	 */

	@Test(groups = { "MSite", "MSiteHome" })
	public void testMobileHotelHDLoadsManali() {
		homeScreen = new HomeScreen();
		hotelSearchResultScreen = new HotelSearchResultsScreen();
		hotelDetailsScreen = new HotelDetailsScreen();

		SoftAssert verify = new SoftAssert();
		String city = "Manali";

		homeScreen.clickOnCityLink(city);
		verify.assertTrue(hotelSearchResultScreen.isHotelSearchScreenDisplayed(),
				"Hotel Search Screen is not displayed on clicking city link " + city);
		verify.assertTrue(hotelSearchResultScreen.isHotelResultDisplayed(), "Results not displayed!!!");
		//verify.assertTrue(hotelSearchResultScreen.isSeoCityContentDisplayed(), "SEO Content not displayed!!!");
		int countOfHotels = hotelSearchResultScreen.getCountOfTotalHotelDisplayed();
		
        for (int i = 1; i <= countOfHotels ; i++){
        	String hotelName = hotelSearchResultScreen.getHotelNameByIndex(i);
        	hotelSearchResultScreen.clickHotelByIndex(i);
    		verify.assertTrue(hotelDetailsScreen.isHDScreenDisplayed(),"HD Screen is not displayed");
    		verify.assertEquals(hotelDetailsScreen.getHotelNameInHDScreen(), hotelName,"Hotel Name is wrong in HD");
    		homeScreen.goToHomeScreen();
    		homeScreen.clickOnCityLink(city);
        }
		verify.assertAll();
	}
	
	/**
	 * This test verifies that every hotel HD screen loads properly
	 */

	@Test(groups = { "MSite", "MSiteHome" })
	public void testMobileHotelHDLoadsAgra() {
		homeScreen = new HomeScreen();
		hotelSearchResultScreen = new HotelSearchResultsScreen();
		hotelDetailsScreen = new HotelDetailsScreen();

		SoftAssert verify = new SoftAssert();
		String city = "Agra";

		homeScreen.clickOnCityLink(city);
		verify.assertTrue(hotelSearchResultScreen.isHotelSearchScreenDisplayed(),
				"Hotel Search Screen is not displayed on clicking city link " + city);
		verify.assertTrue(hotelSearchResultScreen.isHotelResultDisplayed(), "Results not displayed!!!");
		verify.assertTrue(hotelSearchResultScreen.isSeoCityContentDisplayed(), "SEO Content not displayed!!!");
		int countOfHotels = hotelSearchResultScreen.getCountOfTotalHotelDisplayed();
		
        for (int i = 1; i <= countOfHotels ; i++){
        	String hotelName = hotelSearchResultScreen.getHotelNameByIndex(i);
        	hotelSearchResultScreen.clickHotelByIndex(i);
    		verify.assertTrue(hotelDetailsScreen.isHDScreenDisplayed(),"HD Screen is not displayed");
    		verify.assertEquals(hotelDetailsScreen.getHotelNameInHDScreen(), hotelName,"Hotel Name is wrong in HD");
    		homeScreen.goToHomeScreen();
    		homeScreen.clickOnCityLink(city);
        }
		verify.assertAll();
	}
	
	/**
	 * This test verifies that every hotel HD screen loads properly
	 */

	@Test(groups = { "MSite", "MSiteHome" })
	public void testMobileHotelHDLoadsUdaipur() {
		homeScreen = new HomeScreen();
		hotelSearchResultScreen = new HotelSearchResultsScreen();
		hotelDetailsScreen = new HotelDetailsScreen();

		SoftAssert verify = new SoftAssert();
		String city = "Udaipur";

		homeScreen.clickOnCityLink(city);
		verify.assertTrue(hotelSearchResultScreen.isHotelSearchScreenDisplayed(),
				"Hotel Search Screen is not displayed on clicking city link " + city);
		verify.assertTrue(hotelSearchResultScreen.isHotelResultDisplayed(), "Results not displayed!!!");
		verify.assertTrue(hotelSearchResultScreen.isSeoCityContentDisplayed(), "SEO Content not displayed!!!");
		int countOfHotels = hotelSearchResultScreen.getCountOfTotalHotelDisplayed();
		
        for (int i = 1; i <= countOfHotels ; i++){
        	String hotelName = hotelSearchResultScreen.getHotelNameByIndex(i);
        	hotelSearchResultScreen.clickHotelByIndex(i);
    		verify.assertTrue(hotelDetailsScreen.isHDScreenDisplayed(),"HD Screen is not displayed");
    		verify.assertEquals(hotelDetailsScreen.getHotelNameInHDScreen(), hotelName,"Hotel Name is wrong in HD");
    		homeScreen.goToHomeScreen();
    		homeScreen.clickOnCityLink(city);
        }
		verify.assertAll();
	}
	
	/**
	 * This test verifies that every hotel HD screen loads properly
	 */

	@Test(groups = { "MSite", "MSiteHome" })
	public void testMobileHotelHDLoadsKodaikanal() {
		homeScreen = new HomeScreen();
		hotelSearchResultScreen = new HotelSearchResultsScreen();
		hotelDetailsScreen = new HotelDetailsScreen();

		SoftAssert verify = new SoftAssert();
		String city = "Kodaikanal";

		homeScreen.clickOnCityLink(city);
		verify.assertTrue(hotelSearchResultScreen.isHotelSearchScreenDisplayed(),
				"Hotel Search Screen is not displayed on clicking city link " + city);
		verify.assertTrue(hotelSearchResultScreen.isHotelResultDisplayed(), "Results not displayed!!!");
		verify.assertTrue(hotelSearchResultScreen.isSeoCityContentDisplayed(), "SEO Content not displayed!!!");
		int countOfHotels = hotelSearchResultScreen.getCountOfTotalHotelDisplayed();
		
        for (int i = 1; i <= countOfHotels ; i++){
        	String hotelName = hotelSearchResultScreen.getHotelNameByIndex(i);
        	hotelSearchResultScreen.clickHotelByIndex(i);
    		verify.assertTrue(hotelDetailsScreen.isHDScreenDisplayed(),"HD Screen is not displayed");
    		verify.assertEquals(hotelDetailsScreen.getHotelNameInHDScreen(), hotelName,"Hotel Name is wrong in HD");
    		homeScreen.goToHomeScreen();
    		homeScreen.clickOnCityLink(city);
        }
		verify.assertAll();
	}
	
	/**
	 * This test verifies that every hotel HD screen loads properly
	 */

	@Test(groups = { "MSite", "MSiteHome" })
	public void testMobileHotelHDLoadsAurangabad() {
		homeScreen = new HomeScreen();
		hotelSearchResultScreen = new HotelSearchResultsScreen();
		hotelDetailsScreen = new HotelDetailsScreen();

		SoftAssert verify = new SoftAssert();
		String city = "Aurangabad";

		homeScreen.clickOnCityLink(city);
		verify.assertTrue(hotelSearchResultScreen.isHotelSearchScreenDisplayed(),
				"Hotel Search Screen is not displayed on clicking city link " + city);
		verify.assertTrue(hotelSearchResultScreen.isHotelResultDisplayed(), "Results not displayed!!!");
		//verify.assertTrue(hotelSearchResultScreen.isSeoCityContentDisplayed(), "SEO Content not displayed!!!");
		int countOfHotels = hotelSearchResultScreen.getCountOfTotalHotelDisplayed();
		
        for (int i = 1; i <= countOfHotels ; i++){
        	String hotelName = hotelSearchResultScreen.getHotelNameByIndex(i);
        	hotelSearchResultScreen.clickHotelByIndex(i);
    		verify.assertTrue(hotelDetailsScreen.isHDScreenDisplayed(),"HD Screen is not displayed");
    		verify.assertEquals(hotelDetailsScreen.getHotelNameInHDScreen(), hotelName,"Hotel Name is wrong in HD");
    		homeScreen.goToHomeScreen();
    		homeScreen.clickOnCityLink(city);
        }
		verify.assertAll();
	}
	
	/**
	 * This test verifies that every hotel HD screen loads properly
	 */

	@Test(groups = { "MSite", "MSiteHome" })
	public void testMobileHotelHDLoadsNoida() {
		homeScreen = new HomeScreen();
		hotelSearchResultScreen = new HotelSearchResultsScreen();
		hotelDetailsScreen = new HotelDetailsScreen();

		SoftAssert verify = new SoftAssert();
		String city = "Noida";

		homeScreen.clickOnCityLink(city);
		verify.assertTrue(hotelSearchResultScreen.isHotelSearchScreenDisplayed(),
				"Hotel Search Screen is not displayed on clicking city link " + city);
		verify.assertTrue(hotelSearchResultScreen.isHotelResultDisplayed(), "Results not displayed!!!");
		//verify.assertTrue(hotelSearchResultScreen.isSeoCityContentDisplayed(), "SEO Content not displayed!!!");
		int countOfHotels = hotelSearchResultScreen.getCountOfTotalHotelDisplayed();
		
        for (int i = 1; i <= countOfHotels ; i++){
        	String hotelName = hotelSearchResultScreen.getHotelNameByIndex(i);
        	hotelSearchResultScreen.clickHotelByIndex(i);
    		verify.assertTrue(hotelDetailsScreen.isHDScreenDisplayed(),"HD Screen is not displayed");
    		verify.assertEquals(hotelDetailsScreen.getHotelNameInHDScreen(), hotelName,"Hotel Name is wrong in HD");
    		homeScreen.goToHomeScreen();
    		homeScreen.clickOnCityLink(city);
        }
		verify.assertAll();
	}
	
	/**
	 * This test verifies that every hotel HD screen loads properly
	 */

	@Test(groups = { "MSite", "MSiteHome" })
	public void testMobileHotelHDLoadsAhmedabad() {
		homeScreen = new HomeScreen();
		hotelSearchResultScreen = new HotelSearchResultsScreen();
		hotelDetailsScreen = new HotelDetailsScreen();

		SoftAssert verify = new SoftAssert();
		String city = "Ahmedabad";

		homeScreen.clickOnCityLink(city);
		verify.assertTrue(hotelSearchResultScreen.isHotelSearchScreenDisplayed(),
				"Hotel Search Screen is not displayed on clicking city link " + city);
		verify.assertTrue(hotelSearchResultScreen.isHotelResultDisplayed(), "Results not displayed!!!");
		//verify.assertTrue(hotelSearchResultScreen.isSeoCityContentDisplayed(), "SEO Content not displayed!!!");
		int countOfHotels = hotelSearchResultScreen.getCountOfTotalHotelDisplayed();
		
        for (int i = 1; i <= countOfHotels ; i++){
        	String hotelName = hotelSearchResultScreen.getHotelNameByIndex(i);
        	hotelSearchResultScreen.clickHotelByIndex(i);
    		verify.assertTrue(hotelDetailsScreen.isHDScreenDisplayed(),"HD Screen is not displayed");
    		verify.assertEquals(hotelDetailsScreen.getHotelNameInHDScreen(), hotelName,"Hotel Name is wrong in HD");
    		homeScreen.goToHomeScreen();
    		homeScreen.clickOnCityLink(city);
        }
		verify.assertAll();
	}
	
	/**
	 * This test verifies that every hotel HD screen loads properly
	 */

	@Test(groups = { "MSite", "MSiteHome" })
	public void testMobileHotelHDLoadsChandigarh() {
		homeScreen = new HomeScreen();
		hotelSearchResultScreen = new HotelSearchResultsScreen();
		hotelDetailsScreen = new HotelDetailsScreen();

		SoftAssert verify = new SoftAssert();
		String city = "Chandigarh";

		homeScreen.clickOnCityLink(city);
		verify.assertTrue(hotelSearchResultScreen.isHotelSearchScreenDisplayed(),
				"Hotel Search Screen is not displayed on clicking city link " + city);
		verify.assertTrue(hotelSearchResultScreen.isHotelResultDisplayed(), "Results not displayed!!!");
		//verify.assertTrue(hotelSearchResultScreen.isSeoCityContentDisplayed(), "SEO Content not displayed!!!");
		int countOfHotels = hotelSearchResultScreen.getCountOfTotalHotelDisplayed();
		
        for (int i = 1; i <= countOfHotels ; i++){
        	String hotelName = hotelSearchResultScreen.getHotelNameByIndex(i);
        	hotelSearchResultScreen.clickHotelByIndex(i);
    		verify.assertTrue(hotelDetailsScreen.isHDScreenDisplayed(),"HD Screen is not displayed");
    		verify.assertEquals(hotelDetailsScreen.getHotelNameInHDScreen(), hotelName,"Hotel Name is wrong in HD");
    		homeScreen.goToHomeScreen();
    		homeScreen.clickOnCityLink(city);
        }
		verify.assertAll();
	}
	
	/**
	 * This test verifies that every hotel HD screen loads properly
	 */

	@Test(groups = { "MSite", "MSiteHome" })
	public void testMobileHotelHDLoadsIndore() {
		homeScreen = new HomeScreen();
		hotelSearchResultScreen = new HotelSearchResultsScreen();
		hotelDetailsScreen = new HotelDetailsScreen();

		SoftAssert verify = new SoftAssert();
		String city = "Indore";

		homeScreen.clickOnCityLink(city);
		verify.assertTrue(hotelSearchResultScreen.isHotelSearchScreenDisplayed(),
				"Hotel Search Screen is not displayed on clicking city link " + city);
		verify.assertTrue(hotelSearchResultScreen.isHotelResultDisplayed(), "Results not displayed!!!");
		//verify.assertTrue(hotelSearchResultScreen.isSeoCityContentDisplayed(), "SEO Content not displayed!!!");
		int countOfHotels = hotelSearchResultScreen.getCountOfTotalHotelDisplayed();
		
        for (int i = 1; i <= countOfHotels ; i++){
        	String hotelName = hotelSearchResultScreen.getHotelNameByIndex(i);
        	hotelSearchResultScreen.clickHotelByIndex(i);
    		verify.assertTrue(hotelDetailsScreen.isHDScreenDisplayed(),"HD Screen is not displayed");
    		verify.assertEquals(hotelDetailsScreen.getHotelNameInHDScreen(), hotelName,"Hotel Name is wrong in HD");
    		homeScreen.goToHomeScreen();
    		homeScreen.clickOnCityLink(city);
        }
		verify.assertAll();
	}
	
	/**
	 * This test verifies that every hotel HD screen loads properly
	 */

	@Test(groups = { "MSite", "MSiteHome" })
	public void testMobileHotelHDLoadsPondicherry() {
		homeScreen = new HomeScreen();
		hotelSearchResultScreen = new HotelSearchResultsScreen();
		hotelDetailsScreen = new HotelDetailsScreen();

		SoftAssert verify = new SoftAssert();
		String city = "Pondicherry";

		homeScreen.clickOnCityLink(city);
		verify.assertTrue(hotelSearchResultScreen.isHotelSearchScreenDisplayed(),
				"Hotel Search Screen is not displayed on clicking city link " + city);
		verify.assertTrue(hotelSearchResultScreen.isHotelResultDisplayed(), "Results not displayed!!!");
		//verify.assertTrue(hotelSearchResultScreen.isSeoCityContentDisplayed(), "SEO Content not displayed!!!");
		int countOfHotels = hotelSearchResultScreen.getCountOfTotalHotelDisplayed();
		
        for (int i = 1; i <= countOfHotels ; i++){
        	String hotelName = hotelSearchResultScreen.getHotelNameByIndex(i);
        	hotelSearchResultScreen.clickHotelByIndex(i);
    		verify.assertTrue(hotelDetailsScreen.isHDScreenDisplayed(),"HD Screen is not displayed");
    		verify.assertEquals(hotelDetailsScreen.getHotelNameInHDScreen(), hotelName,"Hotel Name is wrong in HD");
    		homeScreen.goToHomeScreen();
    		homeScreen.clickOnCityLink(city);
        }
		verify.assertAll();
	}
	
	/**
	 * This test verifies that every hotel HD screen loads properly
	 */

	@Test(groups = { "MSite", "MSiteHome" })
	public void testMobileHotelHDLoadsNainital() {
		homeScreen = new HomeScreen();
		hotelSearchResultScreen = new HotelSearchResultsScreen();
		hotelDetailsScreen = new HotelDetailsScreen();

		SoftAssert verify = new SoftAssert();
		String city = "Nainital";

		homeScreen.clickOnCityLink(city);
		verify.assertTrue(hotelSearchResultScreen.isHotelSearchScreenDisplayed(),
				"Hotel Search Screen is not displayed on clicking city link " + city);
		verify.assertTrue(hotelSearchResultScreen.isHotelResultDisplayed(), "Results not displayed!!!");
		//verify.assertTrue(hotelSearchResultScreen.isSeoCityContentDisplayed(), "SEO Content not displayed!!!");
		int countOfHotels = hotelSearchResultScreen.getCountOfTotalHotelDisplayed();
		
        for (int i = 1; i <= countOfHotels ; i++){
        	String hotelName = hotelSearchResultScreen.getHotelNameByIndex(i);
        	hotelSearchResultScreen.clickHotelByIndex(i);
    		verify.assertTrue(hotelDetailsScreen.isHDScreenDisplayed(),"HD Screen is not displayed");
    		verify.assertEquals(hotelDetailsScreen.getHotelNameInHDScreen(), hotelName,"Hotel Name is wrong in HD");
    		homeScreen.goToHomeScreen();
    		homeScreen.clickOnCityLink(city);
        }
		verify.assertAll();
	}
	
	/**
	 * This test verifies that drawer can be opened and closed
	 */
	
	@Test(groups = { "MSite", "MSiteHome" })
	public void testMobileDrawerFunctionality() {
		homeScreen = new HomeScreen();
		SoftAssert verify = new SoftAssert();
		
		homeScreen.openDrawer();
		verify.assertTrue(homeScreen.isDrawerOpen(), " Drawer is not open");
		homeScreen.closeDrawer();
		verify.assertFalse(homeScreen.isDrawerOpen(), " Drawer is open");
		verify.assertAll();
	}
	
	
	/**
	 * This test verifies that links in drawer works fine
	 */
	@Test(groups = { "MSite", "MSiteHome" })
	public void testMobileDrawerHomeNavigation() {
		homeScreen = new HomeScreen();
		hotelSearchResultScreen = new HotelSearchResultsScreen();
		browserHelper = new BrowserHelper();
		SoftAssert verify = new SoftAssert();
		
		verify.assertTrue(homeScreen.isHomeScreenDisplayed(),"Home page not displayed");
		
		homeScreen.doSearch();
		verify.assertTrue(hotelSearchResultScreen.isHotelSearchScreenDisplayed(),
				"Hotel Search Screen is not displayed!!!");
		verify.assertTrue(hotelSearchResultScreen.isHotelResultDisplayed(), "Results not displayed!!!");
		
		homeScreen.openDrawer();
		verify.assertTrue(homeScreen.isDrawerOpen(), " Drawer is not open");
		
		browserHelper.waitTime(2000);
        homeScreen.clickOnHomeLink();
        
        verify.assertTrue(homeScreen.isHomeScreenDisplayed(),"Home page not displayed");
        
		verify.assertAll();
	}
	/**
	 * open home page
	 * click on checkIn
	 * verify the left right arrow
	 */
	@Test(groups = { "MSite", "MSiteHome" })
	public void testMobileCalendarNavigation(){
		homeScreen = new HomeScreen();
		browserHelper = new BrowserHelper();
		SoftAssert verify = new SoftAssert();
		//home screen displayed
		verify.assertTrue(homeScreen.isHomeScreenDisplayed(),"Home page not displayed");
		verify.assertTrue(homeScreen.doescheckIndisplayed());
		
		homeScreen.clickOnCheckIn();
		//extract month
		String month1 = homeScreen.extractMonthNameFromCalandar();
				
		verify.assertFalse(homeScreen.doesLeftArrowDisplayed(),"Left Arrow is displayed");
		verify.assertTrue(homeScreen.doesRightArrowDisplayed(),"Right Arrow is not displayed");
		
		homeScreen.clickOnRightArrow();
		String month2=homeScreen.extractMonthNameFromCalandar();
		
		verify.assertTrue(homeScreen.doesLeftArrowDisplayed(),"Left Arrow is not displayed");
		homeScreen.clickOnLeftArrow();
		String month3=homeScreen.extractMonthNameFromCalandar();
		
		verify.assertFalse(homeScreen.isEqualMonthName(month1, month2),"Month1 and Month2  have equal value");
		verify.assertTrue(homeScreen.isEqualMonthName(month1, month3),"Month1 and Month3 don't have equal value");
		verify.assertAll();
		
	}
	
}
