package mobileSiteUITest;

import org.apache.commons.lang3.RandomStringUtils;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import base.BrowserHelper;
import base.TestBaseSetUp;
import mobileSiteUIPageObjects.HomeScreen;
import mobileSiteUIPageObjects.HotelSearchResultsScreen;
import mobileSiteUIPageObjects.ItineraryScreen;
import utils.CommonUtils;

public class MSiteLoginTest extends TestBaseSetUp{
	
	private HomeScreen homeScreen;
	private HotelSearchResultsScreen hotelSearchResultScreen;
	private ItineraryScreen itineraryScreen;
	private BrowserHelper browserHelper;
	
	/**
	 * This test checks Login at home page
	 */
	
	@Test(groups={"MSite" , "MSiteLogin"})
	public void testMobileLoginAtHomePage(){
		homeScreen = new HomeScreen();
		SoftAssert verify = new SoftAssert();
		CommonUtils utils = new CommonUtils();
		
		String guestEmail = utils.getProperty("TestBookingAccount");
		String guestPassword = utils.getProperty("TestLoginPassword");
		String guestName = utils.getProperty("GuestName");
		
		// verify home screen is displayed
		verify.assertTrue(homeScreen.isHomeScreenDisplayed(),"Home screen is not displayed");
		
		//Login
		homeScreen.openDrawer();
		homeScreen.clickOnLogin();
		homeScreen.loginAsTreeboMember(guestEmail, guestPassword);
		
		homeScreen.openDrawer();
		verify.assertTrue(homeScreen.isUserSignedIn(),"Not signed in!");
		verify.assertEquals(homeScreen.getLoggedInUserText(), "Hi " + guestName, "User name is not matching");
		
		verify.assertAll();
	}
	
	/**
	 * This test checks Logout
	 */
	
	@Test(groups={"MSite" , "MSiteLogin"})
	public void testMobileLogout(){
		homeScreen = new HomeScreen();
		SoftAssert verify = new SoftAssert();
		CommonUtils utils = new CommonUtils();
		
		String guestEmail = utils.getProperty("TestBookingAccount");
		String guestPassword = utils.getProperty("TestLoginPassword");
		String guestName = utils.getProperty("GuestName");
		
		// verify home screen is displayed
		verify.assertTrue(homeScreen.isHomeScreenDisplayed(),"Home screen is not displayed");
		
		//Login
		homeScreen.openDrawer();
		homeScreen.clickOnLogin();
		homeScreen.loginAsTreeboMember(guestEmail, guestPassword);
		
		homeScreen.openDrawer();
		verify.assertTrue(homeScreen.isUserSignedIn(),"Not signed in!");
		verify.assertEquals(homeScreen.getLoggedInUserText(), "Hi " + guestName, "User name is not matching");
		
		homeScreen.clickOnSignOutLink();
		
		// Verify User gets signed out
		homeScreen.openDrawer();
		verify.assertFalse(homeScreen.isUserSignedIn(), "User is still signed-in after doing sign out");
		
		verify.assertAll();
		
	}
	
	/**
	 * This test checks Registration
	 */
	
	@Test(groups={"MSite" , "MSiteLogin"})
	public void testMobileRegistration(){
		homeScreen = new HomeScreen();
		SoftAssert verify = new SoftAssert();
		CommonUtils utils = new CommonUtils();
		
		String guestName = utils.getProperty("GuestName");
		
		// verify home screen is displayed
		verify.assertTrue(homeScreen.isHomeScreenDisplayed(),"Home screen is not displayed");
		
		//Register
		homeScreen.openDrawer();
		homeScreen.clickOnSignUp();
		homeScreen.registerOnHomePage();
		
		homeScreen.openDrawer();
		verify.assertTrue(homeScreen.isUserSignedIn(),"Not signed in!");
		verify.assertEquals(homeScreen.getLoggedInUserText(), "Hi " + guestName, "User name is not matching");
		
		homeScreen.clickOnSignOutLink();
		
		// Verify User gets signed out
		homeScreen.openDrawer();
		verify.assertFalse(homeScreen.isUserSignedIn(), "User is still signed-in after doing sign out");
		
		verify.assertAll();
	}
	
	/**
	 * This test checks Google Sign-in at home page
	 */
	
//	@Test(groups={"MSite , MSiteLogin"})
//	public void testMobileGoogleSignIn(){
//		
//	}
//	
//	/**
//	 * This test checks FB Sign-in at home page
//	 */
//	
//	@Test(groups={"MSite , MSiteLogin"})
//	public void testMobileFBSignIn(){
//		
//	}
	
	
	/**
	 * This test checks Registration/Logout in Itinerary Screen
	 */
	
	@Test(groups={"MSite" , "MSiteLogin"})
	public void testMobileRegistrationAtItineraryScreen(){
		homeScreen = new HomeScreen();
		hotelSearchResultScreen = new HotelSearchResultsScreen();
		itineraryScreen = new ItineraryScreen();
		browserHelper = new BrowserHelper();
		CommonUtils utils = new CommonUtils();
		SoftAssert verify = new SoftAssert();

		String location = utils.getProperty("city"); // "Bengaluru";
		String testHotel = utils.getProperty("DummyHotel"); // "Elmas";
		String testUrl = utils.getProperty("TestUrl");
		int hotelIndex = 0;
		int checkInFromCurrentDate = utils.getRandomNumber(60, 90);
		int checkOutFromCurrentDate = checkInFromCurrentDate + 1;

		// Check if home screen is displayed
		verify.assertTrue(homeScreen.isHomeScreenDisplayed(), "Home Screen is not displayed");
		// Do Search
		homeScreen.doSearch(location, checkInFromCurrentDate, checkOutFromCurrentDate);
		// check if search results displayed
		verify.assertTrue(hotelSearchResultScreen.isHotelSearchScreenDisplayed(), "search result screen not displayed");
		// Select a random hotel or test hotel
		if (browserHelper.getCurrentUrl().contains(testUrl)) {
			hotelIndex = hotelSearchResultScreen.getAllAvailableHotelNames().indexOf(testHotel);
		} else {
			hotelIndex = hotelSearchResultScreen.getRandomIndexOfAvailableHotel();
		}

		String hotelName = hotelSearchResultScreen.getHotelNameByIndex(hotelIndex);
		int hotelRoomRate = hotelSearchResultScreen.getRoomRateByIndex(hotelIndex);
		System.out.println("Hotel Name: " + hotelName + " Hotel room rate : " + hotelRoomRate);

		// Click quick book
		hotelSearchResultScreen.clickQuickBookByIndex(hotelIndex);
		// Check If Itinerary screen
		verify.assertTrue(itineraryScreen.isItineraryScreenDisplayed());
		itineraryScreen.clickOnNextStepOne();
		
		// Do registration on itinerary screen
		itineraryScreen.clickOnRegister();
		String[] regInfo = itineraryScreen.registerOnItineraryScreen();
		
        verify.assertEquals(itineraryScreen.getGuestName(),regInfo[0],"Prefilled name on itinerary is not correct");
        verify.assertEquals(itineraryScreen.getGuestMobile(),regInfo[2],"Prefilled mobile on itinerary is not correct");
        verify.assertEquals(itineraryScreen.getGuestEmail(),regInfo[1],"Prefilled email on itinerary is not correct");
        
        while(itineraryScreen.isBackButtonThere()){
        	itineraryScreen.clickOnBackOnItineraryScreen();
        }
        
        verify.assertTrue(hotelSearchResultScreen.isHotelSearchScreenDisplayed(), "search result screen not displayed");
		
		homeScreen.openDrawer();
		verify.assertTrue(homeScreen.isUserSignedIn(),"Not signed in!");
		verify.assertEquals(homeScreen.getLoggedInUserText(), "Hi " + regInfo[0], "User name is not matching");
		
		homeScreen.clickOnSignOutLink();
		
		// Verify User gets signed out
		homeScreen.openDrawer();
		verify.assertFalse(homeScreen.isUserSignedIn(), "User is still signed-in after doing sign out");
        
		verify.assertAll();
	}
	
//	/**
//	 * This test checks Google Sign-in/Logout at Itinerary Screen
//	 */
//	
//	@Test(groups={"MSite , MSiteLogin"})
//	public void testMobileGoogleSignInItineraryScreen(){
//		
//	}
//	
//	/**
//	 * This test checks FB Sign-in/Logout Itinerary Screen
//	 */
//	
//	@Test(groups={"MSite , MSiteLogin"})
//	public void testMobileFBSignInItineraryScreen(){
//		
//	}
//	
//	/**
//	 * This test checks forgot password
//	 */
//	
//	@Test(groups={"MSite , MSiteLogin"})
//	public void testMobilForgotPasswordFlow(){
//		
//	}
	
	/**
	 * open home page
	 * click on login
	 * verify gmail login and 
	 */
	@Test(groups={"MSite" , "MSiteLogin"})
	public void testMobileLoginAtHomePageThroughGmail()
	{
		homeScreen = new HomeScreen();
		SoftAssert verify = new SoftAssert();
		CommonUtils utils = new CommonUtils();
		browserHelper = new BrowserHelper();
		
		String email = utils.getProperty("TestLoginEmail");  //"treebotest@gmail.com"
		String password = utils.getProperty("GmailFbPassword");
		
		// verify home screen is displayed
		verify.assertTrue(homeScreen.isHomeScreenDisplayed(),"Home screen is not displayed");
		
		//login using gmail account
		homeScreen.openDrawer();
		homeScreen.clickOnLogin();
		verify.assertTrue(homeScreen.isGmailLinkDisplayed(),"Gmail link is not displayed");
		homeScreen.clickOnGmailLogin();

		
		homeScreen.loginThroughGmail(email, password);
		verify.assertAll();
	}
	/**
	 * open homepage
	 * reset password
	 */
	@Test(groups={"MSite" , "MSiteLogin"})
	public void testMobileForgotPasswordLink(){
		homeScreen = new HomeScreen();
		SoftAssert verify = new SoftAssert();
		CommonUtils utils = new CommonUtils();
		browserHelper = new BrowserHelper();
		
		// verify home screen is displayed
		verify.assertTrue(homeScreen.isHomeScreenDisplayed(),"Home screen is not displayed");
		
		//login 
		homeScreen.openDrawer();
		homeScreen.clickOnLogin();
		verify.assertTrue(homeScreen.isForgotLinkDisplayed(),"Forgot link is not displayed");
		String strEmail = utils.getProperty("TestLoginEmail"); 
		//forgot pass request sent to email
		homeScreen.requestForForgotPassword(strEmail);
		verify.assertTrue(homeScreen.forgotPasswordSuccessMessageDisplayed(),"Success Message is not displayed");
		verify.assertAll();
	}
	
	
}
