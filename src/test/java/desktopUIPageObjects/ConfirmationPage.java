package desktopUIPageObjects;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.xml.parsers.ParserConfigurationException;

import org.apache.http.client.ClientProtocolException;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.xml.sax.SAXException;

import base.BrowserHelper;
import base.DriverManager;
import base.HotelLogixAPI;
import desktopUIPageObjects.HomePage;

public class ConfirmationPage {
	private HomePage homePage;
	private BrowserHelper browserHelper;

	private By confirmationPage = By.xpath("//div[@class='confirmation-page']");
	private By confirmationPageHeader = By.xpath("//div[@class='confirmation-page__header']/div[@class='cp-head']");// Congratulations!
	private By confirmationPageSubHeader = By.xpath("//div[@class='confirmation-page__header']/div[@class='cp-subhead']");//Your booking is confirmed!
	private By confirmationPageSubHeaderInfo = By.xpath("//div[@class='confirmation-page__header']/div[@class='cp-subhead-info']");//You will receive booking confirmation email shortly. Wish you a happy stay!
    private By bookingId = By.xpath("//div[@class='confirmation-page__bookin-id']/span");
    private By guestNameInConfirmationPage = By.xpath("//div[contains(@class,'confirmation-page__guests__name')]/span[@class='g-name']");
    private By guestEmailInConfirmationPage = By.xpath("//div[contains(@class,'confirmation-page__guests__name')]/span[contains(@class,'g-email')]");
    private By guestMobileInConfirmationpage = By.xpath("//div[@class='confirmation-page__guests__mobile']");
    private By confirmationPageGuestsAction = By.xpath("//div[@class='confirmation-page__guests__action']/span[@class='confirmation-page__guests__action--sent']");
	
    private By hotelNameInConfirmationPage = By.cssSelector(".itinerary-view__hotel-info__name");
    private By hotelAddressInConfirmationPage = By.xpath("//div[@class='itinerary-view__hotel-info__address']");
    private By hotelCheckInDateInConfirmationPage = By.xpath("//div[contains(@class,'analytics-checkin')]");
    private By hotelCheckOutDateInConfirmationPage = By.xpath("//div[contains(@class,'analytics-checkout')]");
    private By guestDetailsInConfimrationPage = By.xpath("//div[@class='itinerary-view__booking']/div[3]//div[@class='itinerary-view__booking-info']");
    private By roomInfoInConfirmationPage = By.xpath("//div[@class='itinerary-view__booking']/div[4]//div[contains(@class,'itinerary-view__booking-info')]/span");
    private By roomTypeInConfirmationPage = By.xpath("//div[@class='itinerary-view__booking']/div[5]//div[@class='itinerary-view__booking-info']");
    
    private By roomPrice = By.xpath("//span[@id='roomPrice']");
    private By totalTax = By.xpath("//span[@id='totalTax']");
    private By discountValue = By.xpath("//span[@id='discountValue']");
    private By grandTotal = By.xpath("//span[@id='grandTotal']");
    
    private By hotelNameInGmail = By.xpath("//div[contains(@id,':')]/div[2]/center/div/table/tbody/tr/td/table[1]/tbody/tr[2]/td/table/tbody/tr/td/center/div[1]/span");
    private By guestNameInGmailSalutation = By.xpath("//div[contains(@id,':')]/div[2]/center/div/table/tbody/tr/td/table[1]/tbody/tr[3]/td/table/tbody/tr/td/p/span[1]");
    private By bookingIdInGmail = By.xpath("//div[contains(@id,':')]/div[2]/center/div/table/tbody/tr/td/table[1]/tbody/tr[4]/td/table/tbody/tr/td/span[1]/span");
    private By bookingInTheNameOfGuest = By.xpath("//div[contains(@id,':')]/div[2]/center/div/table/tbody/tr/td/table[1]/tbody/tr[4]/td/table/tbody/tr/td/span[2]/span");
    private By checkinDateInGmail = By.xpath("//div[contains(@id,':')]/div[2]/center/div/table/tbody/tr/td/table[1]/tbody/tr[5]/td/table/tbody/tr/td[1]/div[2]/span/span");
    private By checkoutDateInGmail = By.xpath("//div[contains(@id,':')]/div[2]/center/div/table/tbody/tr/td/table[1]/tbody/tr[5]/td/table/tbody/tr/td[3]/div[2]/span/span");
    private By numberOfNightsInGmail = By.xpath("//div[contains(@id,':')]/div[2]/center/div/table/tbody/tr/td/table[1]/tbody/tr[5]/td/table/tbody/tr/td[2]/div/div[1]");
    private By roomPriceInGmail = By.xpath("//div[contains(@id,':')]/div[2]/center/div/table/tbody/tr/td/table[1]/tbody/tr[8]/td/table/tbody/tr/td[2]/table/tbody/tr[1]/td");
    private By taxesInGmail = By.xpath("//div[contains(@id,':')]/div[2]/center/div/table/tbody/tr/td/table[1]/tbody/tr[8]/td/table/tbody/tr/td[2]/table/tbody/tr[2]/td");
    private By discountInGmail = By.xpath("//div[contains(@id,':')]/div[2]/center/div/table/tbody/tr/td/table[1]/tbody/tr[8]/td/table/tbody/tr/td[2]/table/tbody/tr[3]/td");
    private By totalInGmail = By.xpath("//div[contains(@id,':')]/div[2]/center/div/table/tbody/tr/td/table[1]/tbody/tr[9]/td/table/tbody/tr/td[2]/table/tbody/tr/td/div[1]");

	public void verifyConfirmationPagePresence(){
		WebDriver driver = DriverManager.getInstance().getDriver();
		homePage = new HomePage();
		browserHelper = new BrowserHelper();
		
		//check if 500 page
		if (homePage.is500Displayed()){
			Assert.fail("500 displayed instead of confirmation page for url " + browserHelper.getCurrentUrl());
		}
		
		try{
		    WebDriverWait wait = new WebDriverWait(driver, 120);
		    wait.until(ExpectedConditions.presenceOfElementLocated(confirmationPage));
		}catch (Exception e){
			// do nothing
		}

	    Assert.assertTrue((driver.findElements(confirmationPage).size() > 0),"Confirmation page is not displayed. Url is " + browserHelper.getCurrentUrl());
	    Assert.assertTrue(driver.findElement(confirmationPage).isDisplayed(),"Confirmation page is not displayed. Url is " + browserHelper.getCurrentUrl());
	    System.out.println("Confimration Page is displayed");
	}
	
	public String getBookingId(){
		WebDriver driver = DriverManager.getInstance().getDriver();
		String id = driver.findElement(bookingId).getText();
		System.out.println("Booking id is: " + id);

		// if booking is done through async 
		// Finally verify that booking created in HX
        if (id.contains("TRB")){
    		HotelLogixAPI api = new HotelLogixAPI();
    		try {
				api.getOrderDetailsFromHX(id);
			} catch (ClientProtocolException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (ParserConfigurationException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (SAXException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
        }
		
		return id;
	}
	
	public String getGuestNameInConfPage(){
		WebDriver driver = DriverManager.getInstance().getDriver();
		return driver.findElement(guestNameInConfirmationPage).getText();
	}
	
	public String getGuestEmailInConfPage(){
		WebDriver driver = DriverManager.getInstance().getDriver();
		return driver.findElement(guestEmailInConfirmationPage).getText();
	}
	
	public String getGuestMobileInConfPage(){
		WebDriver driver = DriverManager.getInstance().getDriver();
		return driver.findElement(guestMobileInConfirmationpage).getText().replaceAll("[^0-9]", "");
	}
	
	public String getHotelNameInConfirmationPage(){
		WebDriver driver = DriverManager.getInstance().getDriver();
		return driver.findElement(hotelNameInConfirmationPage).getText();
	}
	
	public String getHotelAddressInConfirmationPage(){
		WebDriver driver = DriverManager.getInstance().getDriver();
		return driver.findElement(hotelAddressInConfirmationPage).getText();
	}
	
	public String getHotelCheckInDateInConfirmationPage(){
		WebDriver driver = DriverManager.getInstance().getDriver();
		return driver.findElement(hotelCheckInDateInConfirmationPage).getText();
	}
	
	public String getHotelCheckOutDateInConfirmationPage(){
		WebDriver driver = DriverManager.getInstance().getDriver();
		return driver.findElement(hotelCheckOutDateInConfirmationPage).getText();
	}
	
	public String getGuestDetailsInConfimrationPage(){
		WebDriver driver = DriverManager.getInstance().getDriver();
		return driver.findElement(guestDetailsInConfimrationPage).getText();
	}
	
	public String getRoomInfoInConfirmationPage(){
		WebDriver driver = DriverManager.getInstance().getDriver();
		return driver.findElement(roomInfoInConfirmationPage).getText();
	}
	
	public String getRoomTypeInConfirmationPage(){
		WebDriver driver = DriverManager.getInstance().getDriver();
		return driver.findElement(roomTypeInConfirmationPage).getText();
	}
	
	public String getRoomPrice(){
		WebDriver driver = DriverManager.getInstance().getDriver();
		return driver.findElement(roomPrice).getText();
	}
	
	public String getTotalTax(){
		WebDriver driver = DriverManager.getInstance().getDriver();
		return driver.findElement(totalTax).getText();
	}
	
	public String getDiscountValue(){
		WebDriver driver = DriverManager.getInstance().getDriver();
		return driver.findElement(discountValue).getText();
	}
	
	public String getGrandTotal(){
		WebDriver driver = DriverManager.getInstance().getDriver();
		String grandTotalInConf = driver.findElement(grandTotal).getText();
		System.out.println("Grand Total in Confirmation page:" + grandTotalInConf);
		return grandTotalInConf;
	}
	
	public double[] getHotelPriceInfoInConfirmationPage() {
		WebDriver driver = DriverManager.getInstance().getDriver();
		double[] hotelPriceInfo = new double[4];
		hotelPriceInfo[0] = Double.parseDouble(driver.findElement(roomPrice).getText());
		hotelPriceInfo[1] = Double.parseDouble(driver.findElement(totalTax).getText());
		hotelPriceInfo[2] = Double.parseDouble(driver.findElement(discountValue).getText());
		hotelPriceInfo[3] = Double.parseDouble(driver.findElement(grandTotal).getText());
		System.out.println(" Confirmation Page Price :" + "Room Price : " + hotelPriceInfo[0] + " Total Tax : "
				+ hotelPriceInfo[1] + " Total Discount : " + hotelPriceInfo[2] + " Grand Total :" + hotelPriceInfo[3]);
		return hotelPriceInfo;
	}
	
	public Map<String,String> getConfirmationEmailDetails(String email,String password,String bookingId){
		WebDriver driver = DriverManager.getInstance().getDriver();
		homePage = new HomePage();
		homePage.signInToGmail(email, password);
		homePage.searchGmailWithTextAndOpenTheEmail(bookingId);
		Map<String,String> gmailData = new HashMap<String,String>();
		String strhotelNameInGmail = driver.findElement(hotelNameInGmail).getText();
		String strguestNameInGmailSalutation = driver.findElement(guestNameInGmailSalutation).getText();
		String strbookingIdInGmail = driver.findElement(bookingIdInGmail).getText();
		String strbookingInTheNameOfGuest = driver.findElement(bookingInTheNameOfGuest).getText();
		String strcheckinDateInGmail = driver.findElement(checkinDateInGmail).getText();
		String strcheckoutDateInGmail = driver.findElement(checkoutDateInGmail).getText();
		String strnumberOfNightsInGmail = driver.findElement(numberOfNightsInGmail).getText();
		String strroomPriceInGmail = driver.findElement(roomPriceInGmail).getText();
		String strtaxesInGmail = driver.findElement(discountInGmail).getText();
		String strdiscountInGmail = driver.findElement(discountInGmail).getText();
		String strtotalInGmail = driver.findElement(totalInGmail).getText();
		
		gmailData.put("hotelName", strhotelNameInGmail);
		gmailData.put("guestName", strguestNameInGmailSalutation);
		gmailData.put("bookingId", strbookingIdInGmail);
		gmailData.put("bookingInName", strbookingInTheNameOfGuest);
		gmailData.put("checkInDate", strcheckinDateInGmail);
		gmailData.put("checkOutDate", strcheckoutDateInGmail);
		gmailData.put("numOfNights", strnumberOfNightsInGmail);
		gmailData.put("taxes", strtaxesInGmail);
		gmailData.put("discount", strdiscountInGmail);
		gmailData.put("roomPrice", strroomPriceInGmail);
		gmailData.put("totalPrice", strtotalInGmail);
		
		return gmailData;
	}
}

