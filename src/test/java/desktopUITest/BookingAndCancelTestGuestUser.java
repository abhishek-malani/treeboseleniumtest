package desktopUITest;

import java.sql.SQLException;
import java.text.ParseException;

import org.apache.commons.lang3.RandomStringUtils;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import utils.CommonUtils;
import utils.DBUtils;
import base.TestBaseSetUp;
import desktopUIPageObjects.HomePage;
import desktopUIPageObjects.BookingHistory;
import desktopUIPageObjects.HotelResultsPage;
import desktopUIPageObjects.ItineraryPage;
import desktopUIPageObjects.ConfirmationPage;
import base.BrowserHelper;

public class BookingAndCancelTestGuestUser extends TestBaseSetUp {

	private HomePage homePage;
	private HotelResultsPage hotelResultsPage;
	private ItineraryPage itineraryPage;
	private ConfirmationPage confirmationPage;
	private BookingHistory bookingHistory;
	private BrowserHelper browserHelper;

	/***
	 * Book a treebo hotel as guest
	 * with OTP verification 
	 * Cancel the booking
	 * 
	 * @throws InterruptedException
	 * @throws SQLException 
	 */
	@Test(groups = { "HotelBooking", "Sanity" })
	public void testHotelBookingAndCancellationAsGuest()
			throws ParseException, InterruptedException, SQLException {
		homePage = new HomePage();
		hotelResultsPage = new HotelResultsPage();
		itineraryPage = new ItineraryPage();
		confirmationPage = new ConfirmationPage();
		browserHelper = new BrowserHelper();
		bookingHistory = new BookingHistory();
		CommonUtils utils = new CommonUtils();
		DBUtils dbUtills = new DBUtils();
		SoftAssert verify = new SoftAssert();

		String location = utils.getProperty("Location"); // "India";
		String testHotel = utils.getProperty("DummyHotel"); // "Dummy";
		String testUrl = utils.getProperty("TestUrl");
		int hotelIndex = 0;
		int checkInFromCurrentDate = utils.getRandomNumber(30, 45);
		int checkOutFromCurrentDate = checkInFromCurrentDate + 1;
		int roomIndex = 1;
		String[] checkInCheckOutDates = new String[2];
		String guestName = utils.getProperty("GuestName"); // "Test";
		String guestEmail = browserHelper.getDisposableEmail();
		String guestMobile = Integer.toString(utils.getRandomNumber(2, 6)) + RandomStringUtils.randomNumeric(9);

		// Home Page is Displayed
		homePage.verifyHomePagePresence();
		// Do search and get checkin checkout dates
		checkInCheckOutDates = homePage.doSearch(location, checkInFromCurrentDate, checkOutFromCurrentDate, roomIndex);
		hotelResultsPage.verifyHotelResultsPagePresence();

		if (browserHelper.getCurrentUrl().contains(testUrl)) {
			hotelIndex = hotelResultsPage.getHotelIndexByHotelName(testHotel);
		} else {
			hotelIndex = hotelResultsPage.getHotelIndexByHotelAvailable();
		}

		String hotelName = hotelResultsPage.getHotelNameByIndex(hotelIndex);
		String hotelRoomRate = hotelResultsPage.getHotelRoomRateByIndex(hotelIndex);
		String hotelAddress = hotelResultsPage.getHotelAddressByIndex(hotelIndex);
		System.out.println(
				"Hotel Name:" + hotelName + " Hotel room rate : " + hotelRoomRate + " Hotel Address:" + hotelAddress);

		hotelResultsPage.clickQuickBookByIndex(hotelIndex);
		itineraryPage.verifyItineraryPagePresence();
		itineraryPage.clickContinueAsGuest();
		itineraryPage.bookHotelAsGuestWithPayAtHotel(guestName, guestMobile, guestEmail);
		
		Thread.sleep(3000);
		//verify OTP pop up
		verify.assertTrue(itineraryPage.doesOTPPopupDisplayed(),"OTP pop up is not displayed");
		
		//check OTP pop up title
		verify.assertTrue(itineraryPage.doesOTPPopupTitleDisplayed(),"OTP Title is not displayed");
		
		//check resend sms  clickable
		verify.assertTrue(itineraryPage.doesClickableResendSms(),"Resend sms is not clickable");
		
		//check change number clickable
		verify.assertTrue(itineraryPage.doesClickableChangeNumber(),"change number is not clickable");
		
		//extract mobile number from OTP pop up
		String popupMobile=itineraryPage.extractMobileNumber();
		
		//mobile number verification
		verify.assertTrue(itineraryPage.doesEqualBothString(popupMobile.trim(), guestMobile.trim()),"Mobile number is not equal");
		
		verify.assertTrue(itineraryPage.doesDoneButtonOnOTPPopupDisplayed(), "Done button is not displayed");
		
		Thread.sleep(3000);
		String wrongOTP="111111";
		itineraryPage.enterOTP(wrongOTP.trim());
		itineraryPage.clickOnDoneButtonForOTPVerification();
		Thread.sleep(3000);
		verify.assertTrue(itineraryPage.doesShowErrorMessage(),"Please enter correct OTP message is not displayed");
		
		Thread.sleep(3000);
		String otp = null;

	    otp = dbUtills.getOTP(guestMobile.trim());
	    itineraryPage.enterOTP(otp.trim());
	    
	    
	    itineraryPage.clickOnDoneButtonForOTPVerification();
	    //verify.assertFalse(itineraryPage.doesOTPPopupDisplay
	    
	    Thread.sleep(5000);

		confirmationPage.verifyConfirmationPagePresence();
		String bookingId = confirmationPage.getBookingId();

		verify.assertEquals(guestName, confirmationPage.getGuestNameInConfPage());
		verify.assertEquals(guestEmail, confirmationPage.getGuestEmailInConfPage());
		verify.assertEquals(guestMobile, confirmationPage.getGuestMobileInConfPage());
		verify.assertEquals(hotelName, confirmationPage.getHotelNameInConfirmationPage());

		System.out.println(utils.formatDate(checkInCheckOutDates[0]).toUpperCase());
		System.out.println(confirmationPage.getHotelCheckInDateInConfirmationPage().toUpperCase());
		verify.assertEquals(utils.formatDate(checkInCheckOutDates[0]).toUpperCase(),
				confirmationPage.getHotelCheckInDateInConfirmationPage().toUpperCase());
		verify.assertEquals(utils.formatDate(checkInCheckOutDates[1]).toUpperCase(),
				confirmationPage.getHotelCheckOutDateInConfirmationPage().toUpperCase());

		verify.assertAll();
	}
	/*
	 *Book a treebo hotel as guest
	 * with skip OTP option and skip to Pay now
	 * Cancel the booking 
	 */
	@Test(groups = { "HotelBooking", "Sanity" })
	public void testHotelBookingAndCancellationAsGuestSkipOTP(){
		homePage = new HomePage();
		hotelResultsPage = new HotelResultsPage();
		itineraryPage = new ItineraryPage();
		confirmationPage = new ConfirmationPage();
		browserHelper = new BrowserHelper();
		bookingHistory = new BookingHistory();
		CommonUtils utils = new CommonUtils();
		DBUtils dbUtills = new DBUtils();
		SoftAssert verify = new SoftAssert();

		String location = utils.getProperty("Location"); // "India";
		String testHotel = utils.getProperty("DummyHotel"); // "Dummy";
		String testUrl = utils.getProperty("TestUrl");
		int hotelIndex = 0;
		int checkInFromCurrentDate = utils.getRandomNumber(30, 45);
		int checkOutFromCurrentDate = checkInFromCurrentDate + 1;
		int roomIndex = 1;
		String[] checkInCheckOutDates = new String[2];
		String guestName = utils.getProperty("GuestName"); // "Test";
		String guestEmail = browserHelper.getDisposableEmail();
		String guestMobile = Integer.toString(utils.getRandomNumber(2, 6)) + RandomStringUtils.randomNumeric(9);

		// Home Page is Displayed
		homePage.verifyHomePagePresence();
		// Do search and get checkin checkout dates
		checkInCheckOutDates = homePage.doSearch(location, checkInFromCurrentDate, checkOutFromCurrentDate, roomIndex);
		hotelResultsPage.verifyHotelResultsPagePresence();

		if (browserHelper.getCurrentUrl().contains(testUrl)) {
			hotelIndex = hotelResultsPage.getHotelIndexByHotelName(testHotel);
		} else {
			hotelIndex = hotelResultsPage.getHotelIndexByHotelAvailable();
		}

		String hotelName = hotelResultsPage.getHotelNameByIndex(hotelIndex);
		String hotelRoomRate = hotelResultsPage.getHotelRoomRateByIndex(hotelIndex);
		String hotelAddress = hotelResultsPage.getHotelAddressByIndex(hotelIndex);
		System.out.println(
				"Hotel Name:" + hotelName + " Hotel room rate : " + hotelRoomRate + " Hotel Address:" + hotelAddress);

		hotelResultsPage.clickQuickBookByIndex(hotelIndex);
		itineraryPage.verifyItineraryPagePresence();
		itineraryPage.clickContinueAsGuest();
		itineraryPage.bookHotelAsGuestWithPayAtHotel(guestName, guestMobile, guestEmail);
		
		try {
			Thread.sleep(3000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		//verify OTP pop up
		verify.assertTrue(itineraryPage.doesOTPPopupDisplayed(),"OTP pop up is not displayed");
		
		//check OTP pop up title
		verify.assertTrue(itineraryPage.doesOTPPopupTitleDisplayed(),"OTP Title is not displayed");
		
		//check resend sms  clickable
		verify.assertTrue(itineraryPage.doesClickableResendSms(),"Resend sms is not clickable");
		
		//check change number clickable
		verify.assertTrue(itineraryPage.doesClickableChangeNumber(),"change number is not clickable");
		
		//extract mobile number from OTP pop up
		String popupMobile=itineraryPage.extractMobileNumber();
		
		//mobile number verification
		verify.assertTrue(itineraryPage.doesEqualBothString(popupMobile.trim(), guestMobile.trim()),"Mobile number is not equal");
		
		verify.assertTrue(itineraryPage.doesDoneButtonOnOTPPopupDisplayed(), "Done button is not displayed");
		
		try {
			Thread.sleep(3000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		String wrongOTP="111111";
		itineraryPage.enterOTP(wrongOTP.trim());
		itineraryPage.clickOnDoneButtonForOTPVerification();
		try {
			Thread.sleep(3000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		verify.assertTrue(itineraryPage.doesShowErrorMessage(),"Please enter correct OTP message is not displayed");
		
		try {
			Thread.sleep(3000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		verify.assertTrue(itineraryPage.doesPayNowOptionDisplayedOnOTPPopup(),"Pay Now option is not displayed on OTP popup");
		itineraryPage.clickOnSkippIWillPayNow();
		
		verify.assertTrue(itineraryPage.doesLogoOnTransactionPageDisplayed(),"Logo is not present");
		
      verify.assertAll();
	}
	
	/*
	 * Book a treebo hotel as guest
	 * verified with OTP with change number at OTP pop up
	 *Cancel the booking
	 */
	@Test(groups = { "HotelBooking", "Sanity" })
	public void testHotelBookingAndCancellationAsGuestChangeMobilenNumberForOTP() throws SQLException{
		homePage = new HomePage();
		hotelResultsPage = new HotelResultsPage();
		itineraryPage = new ItineraryPage();
		confirmationPage = new ConfirmationPage();
		browserHelper = new BrowserHelper();
		bookingHistory = new BookingHistory();
		CommonUtils utils = new CommonUtils();
		DBUtils dbUtills = new DBUtils();
		SoftAssert verify = new SoftAssert();

		String location = utils.getProperty("Location"); // "India";
		String testHotel = utils.getProperty("DummyHotel"); // "Dummy";
		String testUrl = utils.getProperty("TestUrl");
		int hotelIndex = 0;
		int checkInFromCurrentDate = utils.getRandomNumber(30, 45);
		int checkOutFromCurrentDate = checkInFromCurrentDate + 1;
		int roomIndex = 1;
		String[] checkInCheckOutDates = new String[2];
		String guestName = utils.getProperty("GuestName"); // "Test";
		String guestEmail = browserHelper.getDisposableEmail();
		String guestMobile = Integer.toString(utils.getRandomNumber(2, 6)) + RandomStringUtils.randomNumeric(9);

		// Home Page is Displayed
		homePage.verifyHomePagePresence();
		// Do search and get checkin checkout dates
		checkInCheckOutDates = homePage.doSearch(location, checkInFromCurrentDate, checkOutFromCurrentDate, roomIndex);
		hotelResultsPage.verifyHotelResultsPagePresence();

		if (browserHelper.getCurrentUrl().contains(testUrl)) {
			hotelIndex = hotelResultsPage.getHotelIndexByHotelName(testHotel);
		} else {
			hotelIndex = hotelResultsPage.getHotelIndexByHotelAvailable();
		}

		String hotelName = hotelResultsPage.getHotelNameByIndex(hotelIndex);
		String hotelRoomRate = hotelResultsPage.getHotelRoomRateByIndex(hotelIndex);
		String hotelAddress = hotelResultsPage.getHotelAddressByIndex(hotelIndex);
		System.out.println(
				"Hotel Name:" + hotelName + " Hotel room rate : " + hotelRoomRate + " Hotel Address:" + hotelAddress);

		hotelResultsPage.clickQuickBookByIndex(hotelIndex);
		itineraryPage.verifyItineraryPagePresence();
		itineraryPage.clickContinueAsGuest();
		itineraryPage.bookHotelAsGuestWithPayAtHotel(guestName, guestMobile, guestEmail);
		
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		//verify OTP pop up
		verify.assertTrue(itineraryPage.doesOTPPopupDisplayed(),"OTP pop up is not displayed");
		
		//check OTP pop up title
		verify.assertTrue(itineraryPage.doesOTPPopupTitleDisplayed(),"OTP Title is not displayed");
		
		//check resend sms  clickable
		verify.assertTrue(itineraryPage.doesClickableResendSms(),"Resend sms is not clickable");
		
		//check change number clickable
		verify.assertTrue(itineraryPage.doesClickableChangeNumber(),"change number is not clickable");
		
		//extract mobile number from OTP pop up
		String popupMobile=itineraryPage.extractMobileNumber();
		
		//mobile number verification
		verify.assertTrue(itineraryPage.doesEqualBothString(popupMobile.trim(), guestMobile.trim()),"Mobile number is not equal");
		
		verify.assertTrue(itineraryPage.doesDoneButtonOnOTPPopupDisplayed(), "Done button is not displayed");
		
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		String wrongOTP="111111";
		itineraryPage.enterOTP(wrongOTP.trim());
		itineraryPage.clickOnDoneButtonForOTPVerification();
		try {
			Thread.sleep(3000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		verify.assertTrue(itineraryPage.doesShowErrorMessage(),"Please enter correct OTP message is not displayed");
		
		try {
			Thread.sleep(3000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		//change number
		itineraryPage.clickOnChangeNumber();
		
		//check title of change number pop up
		verify.assertTrue(itineraryPage.doesChangeMobileNumberTitleDisplayed(),"Enter Mobile Number title is not displayed");
		
		String newNumber=Integer.toString(utils.getRandomNumber(2, 6)) + RandomStringUtils.randomNumeric(9);
		
		try {
			Thread.sleep(3000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		itineraryPage.changeMobileNumber(newNumber.trim());
		
		try {
			Thread.sleep(3000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		itineraryPage.clickOnDoneButtonForOTPVerification();
		
		try {
			Thread.sleep(3000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		String otp = null;
		
       //extract otp from db
	    otp = dbUtills.getOTP(newNumber.trim());
	    itineraryPage.enterOTP(otp.trim());
	    
	    
	    itineraryPage.clickOnDoneButtonForOTPVerification();
	    //verify.assertFalse(itineraryPage.doesOTPPopupDisplay
	    
	    try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		confirmationPage.verifyConfirmationPagePresence();
		String bookingId = confirmationPage.getBookingId();

		verify.assertEquals(guestName, confirmationPage.getGuestNameInConfPage());
		verify.assertEquals(guestEmail, confirmationPage.getGuestEmailInConfPage());
		verify.assertEquals(guestMobile, confirmationPage.getGuestMobileInConfPage());
		verify.assertEquals(hotelName, confirmationPage.getHotelNameInConfirmationPage());

		verify.assertAll();
		}
}