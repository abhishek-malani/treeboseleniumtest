package newDesktopUITest;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.SQLException;
import java.text.ParseException;

import org.apache.commons.lang3.RandomStringUtils;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import base.BrowserHelper;
import base.TestBaseSetUp;
import desktopUIPageObjects.BookingHistory;
import desktopUIPageObjects.ConfirmationPage;
import desktopUIPageObjects.HomePage;
import desktopUIPageObjects.HotelResultsPage;
import desktopUIPageObjects.ItineraryPage;
import utils.CommonUtils;
import utils.DBUtils;

public class ItineraryPageTest extends TestBaseSetUp {
	private HomePage homePage;
	private HotelResultsPage hotelResultsPage;
	private ItineraryPage itineraryPage;
	private ConfirmationPage confirmationPage;
	private BrowserHelper browserHelper;
	
	
	
	
	/***
	 * 
	 * @throws ParseException
	 * @throws FileNotFoundException
	 * @throws IOException
	 * @throws InterruptedException
	 * Open homePage with checkIn Checkout location
	 * then search hotel on hotel results page 
	 * click on random hotel's quick book link 
	 * on next page login with invalid credentials
	 */
	
	    @Test(groups = { "Itinerary" })
		public void testItineraryLoginUsingInvalidUsernameAndPassword()
				throws ParseException, FileNotFoundException, IOException, InterruptedException {
			homePage = new HomePage();
			hotelResultsPage = new HotelResultsPage();
			itineraryPage = new ItineraryPage();
			//confirmationPage = new ConfirmationPage();
			browserHelper = new BrowserHelper();
			//bookingHistory = new BookingHistory();
			CommonUtils utils = new CommonUtils();
			SoftAssert verify = new SoftAssert();

			String location = utils.getProperty("Country"); // "India";
			String testHotel = utils.getProperty("DummyHotel"); // "Dummy";
			String testUrl = utils.getProperty("TestUrl");
			int hotelIndex = 0;
			int checkInFromCurrentDate = utils.getRandomNumber(30, 45);
			int checkOutFromCurrentDate = checkInFromCurrentDate + 1;
			int roomIndex = 1;
			String[] checkInCheckOutDates = new String[2];
			String strGuestName = utils.getProperty("GuestName");
			String strGuestMobile = "8888888888";//utils.getProperty("GuestMobile");// "9000000000";

			// Home Page is Displayed
			homePage.verifyHomePagePresence();
			// Do search and get checkin checkout dates
			checkInCheckOutDates = homePage.doSearch(location, checkInFromCurrentDate, checkOutFromCurrentDate, roomIndex);
			hotelResultsPage.verifyHotelResultsPagePresence();

			if (browserHelper.getCurrentUrl().contains(testUrl)) {
				hotelIndex = hotelResultsPage.getHotelIndexByHotelName(testHotel);
			} else {
				hotelIndex = hotelResultsPage.getHotelIndexByHotelAvailable();
			}

			String hotelName = hotelResultsPage.getHotelNameByIndex(hotelIndex);
			String hotelRoomRate = hotelResultsPage.getHotelRoomRateByIndex(hotelIndex);
			String hotelAddress = hotelResultsPage.getHotelAddressByIndex(hotelIndex);
			System.out.println(
					"Hotel Name:" + hotelName + " Hotel room rate : " + hotelRoomRate + " Hotel Address:" + hotelAddress);
            //click on hotel by its index
			hotelResultsPage.clickQuickBookByIndex(hotelIndex);
			String invalidEmail = utils.getProperty("InvalidEmail"); 
			String invalidPassword = utils.getProperty("InvalidPassword");
			
			//verify the email address title
			verify.assertTrue(itineraryPage.isEmailAddressTitleDisplayed(),"Your Email Address title is not displayed");
			//verify the  email box 
			verify.assertTrue(itineraryPage.doesEmailBoxDisplayed(),"Email Box is not displayed");
			//verify the placeholder text
			verify.assertTrue(itineraryPage.doesEmailPlaceHolderDisplayed(),"Trasparent Email Text is not displayed");
			//login with invalid email
			itineraryPage.enterEmailAddress(invalidEmail.trim());
			//verify the checkBox display
			verify.assertTrue(itineraryPage.doesCheckBoxDisplayed(),"checkBox is not displayed");
			//select the checkBox
			itineraryPage.clickOnCheckBox();
			//verify password box
			verify.assertTrue(itineraryPage.doesPasswordBoxDisplayed(),"Password box is not displayed");
			//login with invalid password
			itineraryPage.enterPassword(invalidPassword.trim());
			//verify continue button
			verify.assertTrue(itineraryPage.doesContinueButtonDisplayed(),"Continue button is not displayed");
			
			itineraryPage.clickOnContinueButton();
			//verify the invalid email , pass message
			verify.assertTrue(itineraryPage.doesInvalidCredentialsMessageDisplayed(),"Invalid Username or Password message is not displayed");
			Thread.sleep(5000);
			verify.assertAll();	
	    }
	    /***
	     * 
	     * @throws ParseException
	     * @throws FileNotFoundException
	     * @throws IOException
	     * @throws InterruptedException
	     * Open homePage with checkIn Checkout location
	     * then search hotel on hotel results page 
	     * then click on random hotel's quick book link
	     * next page login with correct treebo credentials
	     * after click on pay at hotel 
	     * check user mobile number verified or not
	     * if not then otp pop up open
	     * extract otp from db and enter that otp
	     * verify confirm page presence
	     * @throws SQLException 
	     * 
	     */
	 @Test(groups = { "HotelItineraryTest" })
		public void testItineraryLoginUsingValidUsernameAndPasswordWithOTPVerification()
				throws ParseException, FileNotFoundException, IOException, InterruptedException, SQLException {
			homePage = new HomePage();
			hotelResultsPage = new HotelResultsPage();
			itineraryPage = new ItineraryPage();
			confirmationPage = new ConfirmationPage();
			//confirmationPage = new ConfirmationPage();
			browserHelper = new BrowserHelper();
			//bookingHistory = new BookingHistory();
			CommonUtils utils = new CommonUtils();
			DBUtils dbUtills = new DBUtils();
			SoftAssert verify = new SoftAssert();

			String location = utils.getProperty("city");
			String testHotel = utils.getProperty("DummyHotel"); // "Dummy";
			String testUrl = utils.getProperty("TestUrl");
			int hotelIndex = 0;
			int checkInFromCurrentDate = utils.getRandomNumber(30, 45);
			int checkOutFromCurrentDate = checkInFromCurrentDate + 1;
			int roomIndex = 1;
			String[] checkInCheckOutDates = new String[2];
			String strGuestName = utils.getProperty("GuestName");
			String strGuestMobile =utils.getProperty("GuestMobile");// "9000000000";

			// Home Page is Displayed
			homePage.verifyHomePagePresence();
			// Do search and get checkin checkout dates
			checkInCheckOutDates = homePage.doSearch(location, checkInFromCurrentDate, checkOutFromCurrentDate, roomIndex);
			hotelResultsPage.verifyHotelResultsPagePresence();

			if (browserHelper.getCurrentUrl().contains(testUrl)) {
				hotelIndex = hotelResultsPage.getHotelIndexByHotelName(testHotel);
			} else {
				hotelIndex = hotelResultsPage.getHotelIndexByHotelAvailable();
			}

			String hotelName = hotelResultsPage.getHotelNameByIndex(hotelIndex);
			String hotelRoomRate = hotelResultsPage.getHotelRoomRateByIndex(hotelIndex);
			String hotelAddress = hotelResultsPage.getHotelAddressByIndex(hotelIndex);
			System.out.println(
					"Hotel Name:" + hotelName + " Hotel room rate : " + hotelRoomRate + " Hotel Address:" + hotelAddress);

			hotelResultsPage.clickQuickBookByIndex(hotelIndex);
			String emailId = utils.getProperty("TestBookingAccount"); 
			String password = utils.getProperty("TestLoginPassword");  
			
			//verify email address text title
			verify.assertTrue(itineraryPage.isEmailAddressTitleDisplayed(),"Your Email Address title is not displayed");
			//verify email box
			verify.assertTrue(itineraryPage.doesEmailBoxDisplayed(),"Email Box is not displayed");
			//verify email place holder
			verify.assertTrue(itineraryPage.doesEmailPlaceHolderDisplayed(),"Trasparent Email Text is not displayed");
			itineraryPage.enterEmailAddress(emailId);
			//verify check box
			verify.assertTrue(itineraryPage.doesCheckBoxDisplayed(),"checkBox is not displayed");
			//click on that checkBox
			itineraryPage.clickOnCheckBox();
			//verify  password box
			verify.assertTrue(itineraryPage.doesPasswordBoxDisplayed(),"Password box is not displayed");
			itineraryPage.enterPassword(password);
			//verify continue button
			verify.assertTrue(itineraryPage.doesContinueButtonDisplayed(),"Continue button is not displayed");
			//click on continue button
			itineraryPage.clickOnContinueButton();
			//verify title text
			verify.assertTrue(itineraryPage.doesEnterDetailsOfPrimaryTravellerTitleDisplayed(),"Enter Details of Primary traveller title is not displayed");
			//verify email label 
			verify.assertTrue(itineraryPage.doesNameLabelDisplayed(),"Name label is not displayed");
			//itineraryPage.enterName(strGuestName);
			verify.assertTrue(itineraryPage.doesMobileNumberLableDisplayed(),"Mobile Number label is not displayed");
			//itineraryPage.enterMobileNumber(strGuestMobile);
			verify.assertTrue(itineraryPage.doesEmailIdLabelDisplayed(),"Email Label is not displayed");
			//itineraryPage.enterEmailAddress(emailId);
			verify.assertTrue(itineraryPage.doesContinueButtonDisplayed(),"Continue button is not displayed");
			itineraryPage.clickOnContinueButton();
			//verify title text
			verify.assertTrue(itineraryPage.doesMentionSpecialRequestDisplayed(),"Mention Special request text is not displayed");
			//verify checkIn checkOut box Comment box pay now Pay at hotel links
			verify.assertTrue(itineraryPage.doesCheckInBoxDisplayed(),"Checkin box is not displayed");
			verify.assertTrue(itineraryPage.doesCheckOutBoxDisplayed(),"Checkout box is not displayed");
			verify.assertTrue(itineraryPage.doesCommentBoxDisplayed(),"Mention request box is not displayed");
			verify.assertTrue(itineraryPage.doesPayNowButtonDisplayed(),"Pay Now link is not displayed");
			verify.assertTrue(itineraryPage.doesPayAtHotelLinkDisplayed(),"Pay at hotel link is not displayed");
			
			//click on pay at hotel
			itineraryPage.bookWithPayAtHotel();
			
			//extract status
			Boolean status = dbUtills.getOtpVerifiedStatus(emailId);
			System.out.println("**************");
			System.out.println(status);
			System.out.println("**************");
			
			String otp=null;
			String wrongOTP="111111";
			if(status == false){
				verify.assertTrue(itineraryPage.doesOTPPopupDisplayed(),"OTP Pop up is not displayed");
				
				//check OTP pop up title
				verify.assertTrue(itineraryPage.doesOTPPopupTitleDisplayed(),"OTP Title is not displayed");
				
				//check resend sms  clickable
				verify.assertTrue(itineraryPage.doesClickableResendSms(),"Resend sms is not clickable");
				
				//check change number clickable
				verify.assertTrue(itineraryPage.doesClickableChangeNumber(),"change number is not clickable");
				
				//extract mobile number from OTP pop up
				String popupMobile=itineraryPage.extractMobileNumber();
				
				//mobile number verification
				verify.assertTrue(itineraryPage.doesEqualBothString(popupMobile.trim(), strGuestMobile.trim()),"Mobile number is not equal");
				
				verify.assertTrue(itineraryPage.doesDoneButtonOnOTPPopupDisplayed(), "Done button is not displayed");
				itineraryPage.enterOTP(wrongOTP.trim());
				itineraryPage.clickOnDoneButtonForOTPVerification();
				try {
					Thread.sleep(3000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				verify.assertTrue(itineraryPage.doesShowErrorMessage(),"Please enter correct OTP message is not displayed");
				
				try {
					Thread.sleep(3000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				//get otp from db
				otp= dbUtills.getOTP(strGuestMobile.trim());
				itineraryPage.enterOTP(otp.trim());
				
				verify.assertTrue(itineraryPage.doesDoneButtonOnOTPPopupDisplayed(),"Done button is not displayed");
				itineraryPage.clickOnDoneButtonForOTPVerification();
				
				confirmationPage.verifyConfirmationPagePresence();
				
				Boolean newStatus = dbUtills.getOtpVerifiedStatus(emailId);
				System.out.println("**************");
				System.out.println(newStatus);
				System.out.println("**************");
			}
			else
			{
				verify.assertFalse(itineraryPage.doesOTPPopupDisplayed(),"OTP Popup still open");
				//verify confirmation page
				confirmationPage.verifyConfirmationPagePresence();
			}
			boolean updateStatus = dbUtills.getOtpVerifiedStatus(emailId);
			System.out.println("**************");
			System.out.println("**************");
			System.out.println(updateStatus);
			System.out.println("**************");
			System.out.println("**************");
			verify.assertAll();	
	    }
	    
	 
//	 
//	   
//	   
//	    /***
//	    * 
//	    * @throws ParseException
//	    * @throws FileNotFoundException
//	    * @throws IOException
//	    * @throws InterruptedException
//	    * Open homePage with checkIn Checkout location
//	    * then search hotel
//	    * on hotel results page 
//	    * then click on random hotel's quick book link 
//	    * Next page just login as guest 
//	    * enter all required details 
//	     * @throws SQLException 
//	    * 
//	     */
//	    @Test(groups = { "HotelItineraryTest" })
//		public void testItineraryLoginAsGuest()
//				throws ParseException, FileNotFoundException, IOException, InterruptedException, SQLException {
//			homePage = new HomePage();
//			hotelResultsPage = new HotelResultsPage();
//			itineraryPage = new ItineraryPage();
//			confirmationPage = new ConfirmationPage();
//			browserHelper = new BrowserHelper();
//			//bookingHistory = new BookingHistory();
//			CommonUtils utils = new CommonUtils();
//			DBUtils dbUtills = new DBUtils();
//			SoftAssert verify = new SoftAssert();
//			
//
//			String location = utils.getProperty("city");
//			String testHotel = utils.getProperty("DummyHotel"); // "Dummy";
//			String testUrl = utils.getProperty("TestUrl");
//			int hotelIndex = 0;
//			int checkInFromCurrentDate = utils.getRandomNumber(30, 45);
//			int checkOutFromCurrentDate = checkInFromCurrentDate + 1;
//			int roomIndex = 1;
//			String[] checkInCheckOutDates = new String[2];
//			String emailId = browserHelper.getDisposableEmail();
//			String strGuestMobile = Integer.toString(utils.getRandomNumber(2, 6)) + RandomStringUtils.randomNumeric(9);
//			String strGuestName = utils.getProperty("GuestName");
//			//String strGuestMobile = utils.getProperty("GuestMobile");// "9000000000";
//			String hotelName=null;
//			String hotelRoomRate=null;
//			String hotelAddress=null;
//			
//
//			// Home Page is Displayed
//			homePage.verifyHomePagePresence();
//			// Do search and get checkin checkout dates
//			checkInCheckOutDates = homePage.doSearch(location, checkInFromCurrentDate, checkOutFromCurrentDate, roomIndex);
//			hotelResultsPage.verifyHotelResultsPagePresence();
//
//			if (browserHelper.getCurrentUrl().contains(testUrl)) {
//				hotelIndex = hotelResultsPage.getHotelIndexByHotelName(testHotel);
//			} else {
//				hotelIndex = hotelResultsPage.getHotelIndexByHotelAvailable();
//			}
//			
//			hotelName = hotelResultsPage.getHotelNameByIndex(hotelIndex);
//			hotelResultsPage.getHotelRoomRateByIndex(hotelIndex);
//			hotelResultsPage.getHotelAddressByIndex(hotelIndex);
//			
//            //click on quick boox using index
//			hotelResultsPage.clickQuickBookByIndex(hotelIndex);
//			  
//			//verified email address title
//			verify.assertTrue(itineraryPage.isEmailAddressTitleDisplayed(),"Your Email Address text is not displayed");
//			//verified email box
//			verify.assertTrue(itineraryPage.doesEmailBoxDisplayed(),"Email Box is not displayed");
//			//email place holder displayed
//			verify.assertTrue(itineraryPage.doesEmailPlaceHolderDisplayed(),"Trasparent Email Text is not displayed");
//			itineraryPage.enterEmailAddress(emailId);
//			//checkBox displayed
//			verify.assertTrue(itineraryPage.doesCheckBoxDisplayed(),"checkBox is not displayed");
//			//continue button displayed
//			verify.assertTrue(itineraryPage.doesContinueButtonDisplayed(),"Continue button is not displayed");
//			itineraryPage.clickOnContinueButton();
//			//title verified
//			verify.assertTrue(itineraryPage.doesEnterDetailsOfPrimaryTravellerTitleDisplayed(),"Enter Details of Primary traveller text is not displayed");
//			//label verified
//			verify.assertTrue(itineraryPage.doesRegistrationNameLabelDisplayed(),"Name label is not displayed");
//			itineraryPage.enterName(strGuestName);
//			verify.assertTrue(itineraryPage.doesRegistrationMobileNumberLabelDisplayed(),"Mobile Number label is not displayed");
//			itineraryPage.enterMobileNumber(strGuestMobile);
//			verify.assertTrue(itineraryPage.doesEmailIdLabelDisplayed(),"Email Label is not displayed");
//			//itineraryPage.enterEmailAddress(emailId);
//			verify.assertTrue(itineraryPage.doesContinueButtonDisplayed(),"Continue button is not displayed");
//			itineraryPage.clickOnContinueButton();
//			verify.assertTrue(itineraryPage.doesMentionSpecialRequestDisplayed(),"Mention Special request text is not displayed");
//			verify.assertTrue(itineraryPage.doesCheckBoxDisplayed(),"Checkin box is not displayed");
//			verify.assertTrue(itineraryPage.doesCheckOutBoxDisplayed(),"Checkout box is not displayed");
//			verify.assertTrue(itineraryPage.doesCommentBoxDisplayed(),"Mention request box is not displayed");
//			verify.assertTrue(itineraryPage.doesPayNowButtonDisplayed(),"Pay Now link is not displayed");
//			verify.assertTrue(itineraryPage.doesPayAtHotelLinkDisplayed(),"Pay at hotel link is not displayed");
//			
//			//click on pay at hotel
//			itineraryPage.bookWithPayAtHotel();
//			
//			//check otp popup
//			verify.assertTrue(itineraryPage.doesOTPPopupDisplayed(),"OTP Pop up is not displayed");
//			//extarct status
//			//Boolean status = dbUtills.getOtpVerifiedStatus(emailId);
//		    String otp = null;
//
//		    otp = dbUtills.getOTP(strGuestMobile.trim());
//		    itineraryPage.enterOTP(otp.trim());
//		    verify.assertTrue(itineraryPage.doesDoneButtonOnOTPPopupDisplayed(), "Done button is not displayed");
//		    itineraryPage.clickOnDoneButtonForOTPVerification();
//		    verify.assertFalse(itineraryPage.doesOTPPopupDisplayed(), "OTP Popup still open");
//		    Thread.sleep(5000);
//		    confirmationPage.verifyConfirmationPagePresence();
//		    
//
//		    verify.assertAll();
//	    }
//	    @Test(groups = { "HotelItineraryTest" })
//	    public void testItineraryLoginAsGuestChangeMobileNumberOnOTPPopup() throws SQLException, InterruptedException{
//	    	homePage = new HomePage();
//			hotelResultsPage = new HotelResultsPage();
//			itineraryPage = new ItineraryPage();
//			confirmationPage = new ConfirmationPage();
//			//confirmationPage = new ConfirmationPage();
//			browserHelper = new BrowserHelper();
//			//bookingHistory = new BookingHistory();
//			CommonUtils utils = new CommonUtils();
//			DBUtils dbUtills = new DBUtils();
//			SoftAssert verify = new SoftAssert();
//			
//
//			String location = utils.getProperty("city"); // "India";
//			String testHotel = utils.getProperty("DummyHotel"); // "Dummy";
//			String testUrl = utils.getProperty("TestUrl");
//			int hotelIndex = 0;
//			int checkInFromCurrentDate = utils.getRandomNumber(30, 45);
//			int checkOutFromCurrentDate = checkInFromCurrentDate + 1;
//			int roomIndex = 1;
//			String[] checkInCheckOutDates = new String[2];
//			String strGuestName = utils.getProperty("GuestName");
//			//String strGuestMobile = utils.getProperty("GuestMobile");// "9000000000";
//			String hotelName="";
//			String hotelRoomRate="";
//			String hotelAddress="";
//			String emailId = browserHelper.getDisposableEmail();
//			String strGuestMobile = Integer.toString(utils.getRandomNumber(2, 6)) + RandomStringUtils.randomNumeric(9);
////			String emailId = utils.getProperty("TestBookingAccount"); 
////			String password = utils.getProperty("TestLoginPassword"); 
//			 String otp = null;
//
//			// Home Page is Displayed
//			homePage.verifyHomePagePresence();
//			// Do search and get checkin checkout dates
//			checkInCheckOutDates = homePage.doSearch(location, checkInFromCurrentDate, checkOutFromCurrentDate, roomIndex);
//			hotelResultsPage.verifyHotelResultsPagePresence();
//
//			if (browserHelper.getCurrentUrl().contains(testUrl)) {
//				hotelIndex = hotelResultsPage.getHotelIndexByHotelName(testHotel);
//			} else {
//				hotelIndex = hotelResultsPage.getHotelIndexByHotelAvailable();
//			}
//			
//			hotelName = hotelResultsPage.getHotelNameByIndex(hotelIndex);
//			hotelResultsPage.getHotelRoomRateByIndex(hotelIndex);
//			hotelResultsPage.getHotelAddressByIndex(hotelIndex);
//			
//            //click on quick boox using index
//			hotelResultsPage.clickQuickBookByIndex(hotelIndex);
//			  
//			//verified email address title
//			verify.assertTrue(itineraryPage.isEmailAddressTitleDisplayed(),"Your Email Address text is not displayed");
//			//verified email box
//			verify.assertTrue(itineraryPage.doesEmailBoxDisplayed(),"Email Box is not displayed");
//			//email place holder displayed
//			verify.assertTrue(itineraryPage.doesEmailPlaceHolderDisplayed(),"Trasparent Email Text is not displayed");
//			itineraryPage.enterEmailAddress(emailId.trim());
//			//checkBox displayed
//			verify.assertTrue(itineraryPage.doesCheckBoxDisplayed(),"checkBox is not displayed");
//			//continue button displayed
//			verify.assertTrue(itineraryPage.doesContinueButtonDisplayed(),"Continue button is not displayed");
//			itineraryPage.clickOnContinueButton();
//			//title verified
//			verify.assertTrue(itineraryPage.doesEnterDetailsOfPrimaryTravellerTitleDisplayed(),"Enter Details of Primary traveller text is not displayed");
//			//label verified
//			verify.assertTrue(itineraryPage.doesRegistrationNameLabelDisplayed(),"Name label is not displayed");
//			itineraryPage.enterName(strGuestName.trim());
//			verify.assertTrue(itineraryPage.doesRegistrationMobileNumberLabelDisplayed(),"Mobile Number label is not displayed");
//			itineraryPage.enterMobileNumber(strGuestMobile.trim());
//			verify.assertTrue(itineraryPage.doesEmailIdLabelDisplayed(),"Email Label is not displayed");
//			//itineraryPage.enterEmailAddress(emailId);
//			verify.assertTrue(itineraryPage.doesContinueButtonDisplayed(),"Continue button is not displayed");
//			itineraryPage.clickOnContinueButton();
//			verify.assertTrue(itineraryPage.doesMentionSpecialRequestDisplayed(),"Mention Special request text is not displayed");
//			verify.assertTrue(itineraryPage.doesCheckBoxDisplayed(),"Checkin box is not displayed");
//			verify.assertTrue(itineraryPage.doesCheckOutBoxDisplayed(),"Checkout box is not displayed");
//			verify.assertTrue(itineraryPage.doesCommentBoxDisplayed(),"Mention request box is not displayed");
//			verify.assertTrue(itineraryPage.doesPayNowButtonDisplayed(),"Pay Now link is not displayed");
//			verify.assertTrue(itineraryPage.doesPayAtHotelLinkDisplayed(),"Pay at hotel link is not displayed");
//			
//			//click on pay at hotel
//			itineraryPage.bookWithPayAtHotel();
//			//check otp popup
//			//verify.assertTrue(itineraryPage.doesOTPPopupDisplayed(),"OTP Pop up is not displayed");
//			//extarct status
//			//Boolean status = dbUtills.getOtpVerifiedStatus(emailId);
//		   
//            
//		    itineraryPage.clickOnChangeNumber();
//		    
//		    Thread.sleep(2000);
//		    
//		    //update new number
//		    String newGuestMobile=Integer.toString(utils.getRandomNumber(2, 6)) + RandomStringUtils.randomNumeric(9);
//		    
//		    //enter new number 
//		    itineraryPage.changeMobileNumber(newGuestMobile.trim());
//		    
//		    verify.assertTrue(itineraryPage.doesDoneButtonOnOTPPopupDisplayed(), "Done button is not displayed");
//		    Thread.sleep(2000);
//		    itineraryPage.clickOnDoneButtonForOTPVerification();
//		    
//		    otp = dbUtills.getOTP(newGuestMobile.trim());
//		    itineraryPage.enterOTP(otp);
//		    System.out.println("************");
//		    System.out.println(newGuestMobile);
//		    System.out.println(otp);
//		    System.out.println("************");
//		    itineraryPage.clickOnDoneButtonForOTPVerification();
//		    
//		    Thread.sleep(3000);
//		    confirmationPage.verifyConfirmationPagePresence();
//            
//		    
//		    verify.assertAll();
//	    	
//	    }
	    /***
	     * Started with homePage
	     * login with treebo credentials
	     * then search hotel for perticular location with perticular checkIn and checkOut date
	     * come to hotel result page pick random hotel from there and click that hotel's quick book
	     * then opened next page
	     * then click on continue button 
	     * then verified the next page text,title and links
	     * then click on payNow link on that page
	     * then there are three option for payment 
	     * default is credit/debit card option
	     * checking all the fields,links on that page
	     */
	    @Test(groups = { "HotelItineraryTest" })
		public void testItineraryPayNowUsingDebitCreditCard(){
	    	homePage = new HomePage();
			hotelResultsPage = new HotelResultsPage();
			itineraryPage = new ItineraryPage();
			//confirmationPage = new ConfirmationPage();
			browserHelper = new BrowserHelper();
			//bookingHistory = new BookingHistory();
			CommonUtils utils = new CommonUtils();
			SoftAssert verify = new SoftAssert();
			
			
			String location = utils.getProperty("city"); // "Bangalore";
			String testHotel = utils.getProperty("DummyHotel"); // "Dummy";
			String testUrl = utils.getProperty("TestUrl");
			int hotelIndex = 0;
			int checkInFromCurrentDate = utils.getRandomNumber(30, 45);
			int checkOutFromCurrentDate = checkInFromCurrentDate + 1;
			int roomIndex = 1;
			String[] checkInCheckOutDates = new String[2];
			String email = utils.getProperty("TestLoginEmail");  //"treebotest@gmail.com"
			String password = utils.getProperty("TestLoginPassword"); //"password"
			
			
			// Home Page is Displayed
			homePage.verifyHomePagePresence();
			homePage.clickOnLoginLink();
			homePage.enterLoginForm(email, password);
	        homePage.verifyUserIsSignedIn();
			
			checkInCheckOutDates = homePage.doSearch(location, checkInFromCurrentDate, checkOutFromCurrentDate, roomIndex);
			hotelResultsPage.verifyHotelResultsPagePresence();

			if (browserHelper.getCurrentUrl().contains(testUrl)) {
				hotelIndex = hotelResultsPage.getHotelIndexByHotelName(testHotel);
			} else {
				hotelIndex = hotelResultsPage.getHotelIndexByHotelAvailable();
			}
       
			hotelResultsPage.clickQuickBookByIndex(hotelIndex);
			
			//verified label
			verify.assertTrue(itineraryPage.doesRegistrationNameLabelDisplayed(),"Name label is not displayed");
			verify.assertTrue(itineraryPage.doesRegistrationMobileNumberLabelDisplayed(),"Mobile Number label is not displayed");
			verify.assertTrue(itineraryPage.doesEmailIdLabelDisplayed(),"Email Label is not displayed");
			//continue button displayed
			verify.assertTrue(itineraryPage.doesContinueButtonDisplayed(),"Continue button is not displayed");
			itineraryPage.clickOnContinueButton();
			//title text verified
			verify.assertTrue(itineraryPage.doesMentionSpecialRequestDisplayed(),"Mention Special request text is not displayed");
			
			verify.assertTrue(itineraryPage.doesCheckBoxDisplayed(),"Checkin box is not displayed");
			verify.assertTrue(itineraryPage.doesCheckOutBoxDisplayed(),"Checkout box is not displayed");
			verify.assertTrue(itineraryPage.doesCommentBoxDisplayed(),"Mention request box is not displayed");
			verify.assertTrue(itineraryPage.doesPayNowButtonDisplayed(),"Pay Now link is not displayed");
			verify.assertTrue(itineraryPage.doesPayAtHotelLinkDisplayed(),"Pay at hotel link is not displayed");
			verify.assertTrue(itineraryPage.doesPayNowButtonDisplayed(),"Pay Now link is not displayed");
			
			itineraryPage.clickOnPayNow();
			
			//check OTP Popup presence 
		    verify.assertFalse(itineraryPage.doesOTPPopupDisplayed(),"OTP popup displayed");
		    
			//displayed label
			verify.assertTrue(itineraryPage.doesCardNumberLabelDispalyed(),"Card Number label is not displayed");
			verify.assertTrue(itineraryPage.doesExpiryDateLabelDisplayed(),"Expiry Date label is not displayed");
			verify.assertTrue(itineraryPage.doesMonthBoxDisplayed(),"Month box is not displayed");
			verify.assertTrue(itineraryPage.doesYearBoxDisplayed(),"Year box is not displayed");
			verify.assertTrue(itineraryPage.doesCVVLabelDisplayed(),"CVV label is not displayed");
			
     		verify.assertAll();
			}
	    
	    /*** 
	     * Started with homePage
	     * login with treebo credentials
	     * then search hotel for perticular location with perticular checkIn and checkOut date
	     * come to hotel result page pick random hotel from there and click that hotel's quick book
	     * then opened next page
	     * then click on continue button 
	     * then verified the next page text,title and links
	     * then click on payNow link on that page
	     * then there are three option for payment 
	     * click on  Mobikwik payment option on that page
	     * then click on PayNow link
	     * also verify that otp pop is not displayed
	     * then verified the text(Login to Mobikwik Text) that appear on next page
	     */
	 
	    @Test(groups = { "HotelItineraryTest" })
		public void testItineraryPayNowUsingMobikwik() throws InterruptedException{
	    	homePage = new HomePage();
			hotelResultsPage = new HotelResultsPage();
			itineraryPage = new ItineraryPage();
			//confirmationPage = new ConfirmationPage();
			browserHelper = new BrowserHelper();
			//bookingHistory = new BookingHistory();
			CommonUtils utils = new CommonUtils();
			SoftAssert verify = new SoftAssert();
			
			
			String location = utils.getProperty("city"); // "India";
			String testHotel = utils.getProperty("DummyHotel"); // "Dummy";
			String testUrl = utils.getProperty("TestUrl");
			int hotelIndex = 0;
			int checkInFromCurrentDate = utils.getRandomNumber(30, 45);
			int checkOutFromCurrentDate = checkInFromCurrentDate + 1;
			int roomIndex = 1;
			String[] checkInCheckOutDates = new String[2];
			String email = utils.getProperty("TestLoginEmail");  //"treebotest@gmail.com"
			String password = utils.getProperty("TestLoginPassword"); //"password"
			
			
			// Home Page is Displayed
			homePage.verifyHomePagePresence();
			homePage.clickOnLoginLink();
			homePage.enterLoginForm(email, password);
	        homePage.verifyUserIsSignedIn();
			
			checkInCheckOutDates = homePage.doSearch(location, checkInFromCurrentDate, checkOutFromCurrentDate, roomIndex);
			hotelResultsPage.verifyHotelResultsPagePresence();

			if (browserHelper.getCurrentUrl().contains(testUrl)) {
				hotelIndex = hotelResultsPage.getHotelIndexByHotelName(testHotel);
			} else {
				hotelIndex = hotelResultsPage.getHotelIndexByHotelAvailable();
			}

			hotelResultsPage.clickQuickBookByIndex(hotelIndex);
			//verified label
			verify.assertTrue(itineraryPage.doesRegistrationNameLabelDisplayed(),"Name label is not displayed");
			verify.assertTrue(itineraryPage.doesRegistrationMobileNumberLabelDisplayed(),"Mobile Number label is not displayed");
			verify.assertTrue(itineraryPage.doesEmailIdLabelDisplayed(),"Email Label is not displayed");
			//verified continue button
			verify.assertTrue(itineraryPage.doesContinueButtonDisplayed(),"Continue button is not displayed");
			itineraryPage.clickOnContinueButton();
			//verified title
			verify.assertTrue(itineraryPage.doesMentionSpecialRequestDisplayed(),"Mention Special request text is not displayed");
			verify.assertTrue(itineraryPage.doesCheckBoxDisplayed(),"Checkin box is not displayed");
			verify.assertTrue(itineraryPage.doesCheckOutBoxDisplayed(),"Checkout box is not displayed");
			verify.assertTrue(itineraryPage.doesCommentBoxDisplayed(),"Mention request box is not displayed");
			//verify links
			verify.assertTrue(itineraryPage.doesPayNowButtonDisplayed(),"Pay Now link is not displayed");
			verify.assertTrue(itineraryPage.doesPayAtHotelLinkDisplayed(),"Pay at hotel link is not displayed");
			verify.assertTrue(itineraryPage.doesPayNowButtonDisplayed(),"Pay Now link is not displayed");
			itineraryPage.clickOnPayNowButton();
			
			//OTP pop is  displayed
			verify.assertFalse(itineraryPage.doesOTPPopupDisplayed(),"OTP Pop up is displayed");
			
			itineraryPage.clickOnMobikwik();
			//verified mobikwik image
			verify.assertTrue(itineraryPage.doesMobikwikImageDisplayed(),"Mobikwik Image is displayed");
			//verified pay now button
			verify.assertTrue(itineraryPage.doesPayNowButtonForFinalTranscationDisplayed(),"Pay Now button is not displayed");
			//verified mobikwik page with title
			verify.assertTrue(itineraryPage.checkProcessingPaymentForFinalTransactionMobikwik(),"Login to Mobikwik Text is not displayed");
			verify.assertAll();
		    }
	    /***
	     * Started with homePage
	     * login with treebo credentials
	     * then search hotel for perticular location with perticular checkIn and checkOut date
	     * come to hotel result page pick random hotel from there and click that hotel's quick book
	     * verified the pay now option netbanking
	     * 
	     */
	    @Test(groups = { "HotelItineraryTest" })
		public void testItineraryPayNowUsingNetBanking(){
	    	homePage = new HomePage();
			hotelResultsPage = new HotelResultsPage();
			itineraryPage = new ItineraryPage();
			//confirmationPage = new ConfirmationPage();
			browserHelper = new BrowserHelper();
			//bookingHistory = new BookingHistory();
			CommonUtils utils = new CommonUtils();
			SoftAssert verify = new SoftAssert();
			
			
			
			String location = utils.getProperty("city"); // "India";
			String testHotel = utils.getProperty("DummyHotel"); // "Dummy";
			String testUrl = utils.getProperty("TestUrl");
			int hotelIndex = 0;
			int checkInFromCurrentDate = utils.getRandomNumber(30, 45);
			int checkOutFromCurrentDate = checkInFromCurrentDate + 1;
			int roomIndex = 1;
			String[] checkInCheckOutDates = new String[2];
			String email = utils.getProperty("TestLoginEmail");  //"treebotest@gmail.com"
			String password = utils.getProperty("TestLoginPassword");
			
			
			homePage.verifyHomePagePresence();
			homePage.clickOnLoginLink();
			homePage.enterLoginForm(email, password);
	        homePage.verifyUserIsSignedIn();
			
			checkInCheckOutDates = homePage.doSearch(location, checkInFromCurrentDate, checkOutFromCurrentDate, roomIndex);
			hotelResultsPage.verifyHotelResultsPagePresence();

			if (browserHelper.getCurrentUrl().contains(testUrl)) {
				hotelIndex = hotelResultsPage.getHotelIndexByHotelName(testHotel);
			} else {
				hotelIndex = hotelResultsPage.getHotelIndexByHotelAvailable();
			}

			hotelResultsPage.clickQuickBookByIndex(hotelIndex);
			//verified label
			verify.assertTrue(itineraryPage.doesRegistrationNameLabelDisplayed(),"Name label is not displayed");
			verify.assertTrue(itineraryPage.doesRegistrationMobileNumberLabelDisplayed(),"Mobile Number label is not displayed");
			verify.assertTrue(itineraryPage.doesEmailIdLabelDisplayed(),"Email Label is not displayed");
			//verified the continue button
			verify.assertTrue(itineraryPage.doesContinueButtonDisplayed(),"Continue button is not displayed");
			itineraryPage.clickOnContinueButton();
			//verified title,checkbox,checkin,checkOut,Comment box,button links
			verify.assertTrue(itineraryPage.doesMentionSpecialRequestDisplayed(),"Mention Special request text is not displayed");
			verify.assertTrue(itineraryPage.doesCheckInBoxDisplayed(),"Checkin box is not displayed");
			verify.assertTrue(itineraryPage.doesCheckOutBoxDisplayed(),"Checkout box is not displayed");
			verify.assertTrue(itineraryPage.doesCommentBoxDisplayed(),"Mention request box is not displayed");
			verify.assertTrue(itineraryPage.doesPayNowButtonDisplayed(),"Pay Now link is not displayed");
			verify.assertTrue(itineraryPage.doesPayAtHotelLinkDisplayed(),"Pay at hotel link is not displayed");
			verify.assertTrue(itineraryPage.doesPayNowButtonDisplayed(),"Pay Now link is not displayed");
			itineraryPage.clickOnPayNowButton();
			
			//OTP pop is  displayed
			verify.assertFalse(itineraryPage.doesOTPPopupDisplayed(),"OTP Pop up is displayed");
			
			itineraryPage.clickOnNetBankingOption();
			//verified the icon 
			verify.assertTrue(itineraryPage.doesSBIBankIconDisplayed(),"SBI bank Icon is not displayed");
			verify.assertTrue(itineraryPage.doesAxisBankIconDisplayed(),"Axis bank Icon is not displayed");
			verify.assertTrue(itineraryPage.doesHDFCBankIconDisplayed(),"HDFC bank Icon is not displayed");
	    	verify.assertTrue(itineraryPage.doesKotakBankIconDisplayed(),"Kotak bank Icon is not displayed");
			verify.assertTrue(itineraryPage.doesICICIBankIconDisplayed(),"ICICI bank Icon is not displayed");
			verify.assertTrue(itineraryPage.doesYesBankIconDisplayed(),"Yes bank Icon is not displayed");
    		verify.assertTrue(itineraryPage.doesOtherBankBoxDisplayed(),"Other Bank Box is not displayed");
			itineraryPage.clickOnSBI();
		    verify.assertTrue(itineraryPage.doesPayNowButtonDisplayed(),"Pay Now button is not displayed");
		    //clicked pay now verified the netbanking page
		    verify.assertTrue(itineraryPage.checkProcessingPaymentForFinalTransactionNetBanking(),"NetBanking login is not displayed");
		    
		    verify.assertAll();
	    	
	    }
	    @Test(groups = { "HotelItineraryTest" })
	    public void testPriceBreakUpOnItineraryPage() throws ParseException{
	    	homePage = new HomePage();
			hotelResultsPage = new HotelResultsPage();
			itineraryPage = new ItineraryPage();
			browserHelper = new BrowserHelper();
			CommonUtils utils = new CommonUtils();
			SoftAssert verify = new SoftAssert();
			
			String location = utils.getProperty("city"); // "India";
			String testHotel = utils.getProperty("DummyHotel"); // "Dummy";
			String testUrl = utils.getProperty("TestUrl");
			int hotelIndex = 0;
			int checkInFromCurrentDate = utils.getRandomNumber(30, 45);
			int checkOutFromCurrentDate = checkInFromCurrentDate + 2;
			String hotelName="";
			String hotelRoomRate="";
			String hotelAddress="";
			float hotelPrice,tax;
			int roomPrice,taxPrice,totalPrice;
			//int nextDayAfterCheckIn = checkInFromCurrentDate+1;
			int roomIndex = 1;
			String[] checkInCheckOutDates = new String[2];
			// Home Page is Displayed
		   homePage.verifyHomePagePresence();
		   // Do search and get checkin checkout dates
		   checkInCheckOutDates = homePage.doSearch(location, checkInFromCurrentDate, checkOutFromCurrentDate, roomIndex);
		   hotelResultsPage.verifyHotelResultsPagePresence();

		   if (browserHelper.getCurrentUrl().contains(testUrl)) {
						hotelIndex = hotelResultsPage.getHotelIndexByHotelName(testHotel);
					} 
		   else {
						hotelIndex = hotelResultsPage.getHotelIndexByHotelAvailable();
					}

			hotelName = hotelResultsPage.getHotelNameByIndex(hotelIndex);
		    hotelRoomRate = hotelResultsPage.getHotelRoomRateByIndex(hotelIndex);
		    hotelAddress = hotelResultsPage.getHotelAddressByIndex(hotelIndex);
			System.out.println(
					"Hotel Name:" + hotelName + " Hotel room rate : " + hotelRoomRate + " Hotel Address:" + hotelAddress);
			hotelResultsPage.clickQuickBookByIndex(hotelIndex);
			hotelPrice = itineraryPage.extractRoomPrice();
		    roomPrice = (int) hotelPrice;
		    tax = itineraryPage.extractTaxPrice();
			taxPrice =(int)tax;
			totalPrice = itineraryPage.extractTotalPrice();
			System.out.println(hotelPrice +" "+ tax+" " +" "+totalPrice);
			String checkIn = utils.formatDateWithoutSuffix(checkInCheckOutDates[0]);
			String checkOut =utils.formatDateWithoutSuffix(checkInCheckOutDates[1]);
			//verify.assertTrue(itineraryPage.isTotalPriceDisplayCorrect(totalPrice, hotelPrice, tax),"Price is not matching");
			itineraryPage.clickOnPriceBreakUpDetails();
			//verified price breakup window
			verify.assertTrue(hotelResultsPage.isOpenPriceBreakUpWindow(),"Price BreakUp Window is not opened");
			//verified price breakup title
			verify.assertTrue(hotelResultsPage.isPriceBreakTitleDisplayed(),"Price Break Up Title is not displayed");
			//verified checkIn checkOut,Date title,tax title,final price
     		verify.assertTrue(hotelResultsPage.doesCheckInCheckoutCheckedInTitle(checkIn, checkOut),"Not Verified the CheckIn, CheckOut");
			verify.assertTrue(hotelResultsPage.isDateTitleDisplayed(),"Date Title is not displayed");
			verify.assertTrue(hotelResultsPage.isBasePriceTitleDisplayed(),"Base Price Title is not displayed");
			verify.assertTrue(hotelResultsPage.isTaxTitleDisplayed(),"Tax Title is not displayed");	
			verify.assertTrue(hotelResultsPage.isFinalPriceTitle(),"Final Price Title is not displayed");
			//verified checkIn date
			verify.assertTrue(hotelResultsPage.isCheckInDateSame(checkIn),"CheckIn Date is not same");
			//compared roomprice,tax,hotelprice
			verify.assertTrue(hotelResultsPage.compareBasePrice(2, roomPrice),"Hotel Price is not compared");
			verify.assertTrue(hotelResultsPage.compareTaxPrice(2, taxPrice),"Tax Price is not compared");
			verify.assertTrue(hotelResultsPage.comapareActualPrice(2, totalPrice),"Actual Price is not compared");
			//verified room price on footer,tax price,total price,inclusive text
			verify.assertTrue(hotelResultsPage.checkRoomPriceOnFooter(roomPrice),"Room Price not checked");
			verify.assertTrue(hotelResultsPage.checkTaxPriceOnFooter(taxPrice),"Tax price on Footer not checked");
			verify.assertTrue(hotelResultsPage.checkTotalPriceOnFooter(totalPrice),"Total Price not  checked");
			verify.assertTrue(hotelResultsPage.isInclusiveTaxTextDisplayed(),"Inclusive Taxt is not dispalyed");
			verify.assertTrue(hotelResultsPage.isDoneButtonDisplayed(),"Done button is not displayed");
			hotelResultsPage.clickOnDone();
			verify.assertFalse(hotelResultsPage.isOpenPriceBreakUpWindow(),"Price BreakUp Window is opened" );
			verify.assertAll();
			}
	    
	    	    

}
