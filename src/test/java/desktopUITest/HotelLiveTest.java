package desktopUITest;

import java.text.ParseException;
import java.util.Arrays;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import org.testng.Assert;
import org.testng.asserts.SoftAssert;

import utils.CommonUtils;
import base.TestBaseSetUp;
import desktopUIPageObjects.HomePage;
import desktopUIPageObjects.HotelResultsPage;
import desktopUIPageObjects.ItineraryPage;
import desktopUIPageObjects.ConfirmationPage;
import desktopUIPageObjects.HotelDetailsPage;
import desktopUIPageObjects.BookingHistory;
import base.BrowserHelper;
import base.DriverManager;


public class HotelLiveTest extends TestBaseSetUp {
	
	private HomePage homePage;
	private HotelResultsPage hotelResultsPage;
	private ItineraryPage itineraryPage;
	private ConfirmationPage confirmationPage;
	private HotelDetailsPage hotelDetailsPage;
	private BookingHistory bookingHistory;
	private BrowserHelper browserHelper;
	
	
	@Test(groups = { "HotelLive" })
	public void testHotelLiveBooking() throws ParseException {
		WebDriver driver = DriverManager.getInstance().getDriver();
		homePage = new HomePage();
		hotelResultsPage = new HotelResultsPage();
		itineraryPage = new ItineraryPage();
		confirmationPage = new ConfirmationPage();
		hotelDetailsPage = new HotelDetailsPage();
		bookingHistory = new BookingHistory();
		browserHelper = new BrowserHelper();
		CommonUtils utils = new CommonUtils();
		SoftAssert verify = new SoftAssert();

		String hotelNameToGet = System.getProperty("hotelName");
		String cityNameToSearch = System.getProperty("cityName");
		int checkInFromCurrentDate = utils.getRandomNumber(30,45);
		int checkOutFromCurrentDate = checkInFromCurrentDate + 1;
		int roomIndex = 1;
		String guestName = utils.getProperty("GuestName"); // "Test";
		String guestEmail = utils.getProperty("TestBookingAccount"); 
		String guestMobile = utils.getProperty("GuestMobile");// "9000000000";
		String[] checkInCheckOutDates = new String[2];
		
		homePage.verifyHomePagePresence();
		homePage.clickOnCityLinkInCarousel(cityNameToSearch);
		String hotelResultsPageTitle = browserHelper.getBrowserTitle();
		
		boolean citySEODisplayed = true;
		boolean cityNameInSEODescHeader = false;
		boolean cityNameInSEODesc = false;
		try{
		    cityNameInSEODescHeader = hotelResultsPage.isSEOCityDisplayedInDescription(cityNameToSearch);
		    cityNameInSEODesc = hotelResultsPage.isSEODescriptionDisplayed(cityNameToSearch);
		}catch(Exception e){
			citySEODisplayed = false;
		}
		
		boolean distanceDisplayedOnCityPage = hotelResultsPage.isDistanceDisplayed();
		verify.assertFalse(hotelResultsPage.isDistanceDisplayed(),"Distance is displayed in search page");
		List<String> dupeImageInSearch = hotelResultsPage.verifyNoDuplicateImagesInSearchPageForHotel(hotelNameToGet);
		verify.assertTrue(dupeImageInSearch.isEmpty(),"Duplicate images are there in Search Page :" + dupeImageInSearch.toString());
		hotelResultsPage.clickOnTheHotelByName(hotelNameToGet);
		hotelDetailsPage.verifyHotelDetailsPagePresence();
		String hotelDetailsPageTitle = browserHelper.getBrowserTitle();
		boolean metaDesc = true;
		String hotelDetailsPageMetaDescription = "";
		try{
			hotelDetailsPageMetaDescription = driver.findElement(By.cssSelector("meta[name='description']")).getAttribute("content");
		}catch(NoSuchElementException e){
			metaDesc = false;
		}
		
		boolean accessbilityContentDisplayed = hotelDetailsPage.isHotelAccessibilityShown();
		String accessBilityContent = hotelDetailsPage.getHotelAccessibilityContent();
		String trilightContent = hotelDetailsPage.getTrilightsContent();
		System.out.println("trilights:\n" + trilightContent );
		boolean trilightStatus = hotelDetailsPage.areThreeTrilightsItemDisplayed();
		verify.assertTrue(hotelDetailsPage.areThreeTrilightsItemDisplayed(),"trilights are not displayed properly" + hotelDetailsPage.getTrilightsContent());
		List<String> dupeImageInHD = hotelDetailsPage.verifyNoDuplicateImagesInHDPage();
		verify.assertTrue(dupeImageInHD.isEmpty(),"Duplicate images are there in HD page:" + dupeImageInHD.toString());
		
		homePage.goToHomePage();
		homePage.verifyHomePagePresence();
		checkInCheckOutDates = homePage.doSearch(hotelNameToGet, checkInFromCurrentDate, checkOutFromCurrentDate, roomIndex);
		hotelResultsPage.verifyHotelResultsPagePresence();

        int hotelIndex = 0; //hotelResultsPage.getHotelIndexByHotelAvailable();
       
		String hotelName = hotelResultsPage.getHotelNameByIndex(hotelIndex);
		String hotelRoomRate = hotelResultsPage.getHotelRoomRateByIndex(hotelIndex);
		String hotelAddress = hotelResultsPage.getHotelAddressByIndex(hotelIndex);
		System.out.println(
				"Hotel Name:" + hotelName + " Hotel room rate : " + hotelRoomRate + " Hotel Address:" + hotelAddress);

		hotelResultsPage.clickHotelNameByIndex(hotelIndex);
		String urlHDPage = browserHelper.getCurrentUrl();
		String[] roomType = hotelDetailsPage.getAllRoomTypesAvailable();
		String[] roomTypeBooked = new String[roomType.length];
		
        for (int i=0; i < roomType.length ; i++){
            try{
            	hotelDetailsPage.verifyHotelDetailsPagePresence();
            	if (hotelDetailsPage.selectRoomType(roomType[i])){
                	browserHelper.waitTime(5000);
            		hotelDetailsPage.clickBookNowInHotelsDetailsPage();
            		itineraryPage.verifyItineraryPagePresence();
            		itineraryPage.clickContinueAsGuest();
            		itineraryPage.bookHotelAsGuestWithPayAtHotel(guestName, guestMobile, guestEmail);
            		confirmationPage.verifyConfirmationPagePresence();
            		String bookingId = confirmationPage.getBookingId();
            		Assert.assertEquals(guestName, confirmationPage.getGuestNameInConfPage());
            		Assert.assertEquals(guestEmail, confirmationPage.getGuestEmailInConfPage());
            		Assert.assertEquals(guestMobile, confirmationPage.getGuestMobileInConfPage());
            		Assert.assertEquals(hotelName, confirmationPage.getHotelNameInConfirmationPage());

            		System.out.println(utils.formatDate(checkInCheckOutDates[0]).toUpperCase());
            		System.out.println(confirmationPage.getHotelCheckInDateInConfirmationPage().toUpperCase());
            		Assert.assertEquals(utils.formatDate(checkInCheckOutDates[0]).toUpperCase(),
            				confirmationPage.getHotelCheckInDateInConfirmationPage().toUpperCase());
            		Assert.assertEquals(utils.formatDate(checkInCheckOutDates[1]).toUpperCase(),
            				confirmationPage.getHotelCheckOutDateInConfirmationPage().toUpperCase());
            		roomTypeBooked[i] = roomType[i] + " booked for " + checkInCheckOutDates[0] + " and " + checkInCheckOutDates[1];
            		System.out.println(roomTypeBooked[i]);
            	}else{
            		roomTypeBooked[i] = roomType[i] + " not available for " + checkInCheckOutDates[0] + " and " + checkInCheckOutDates[1];
            	}
            		
            }catch (Exception e){
            	roomTypeBooked[i] = roomType[i] + " booking failed for " + checkInCheckOutDates[0] + " and " + checkInCheckOutDates[1];
            	verify.assertTrue(false,"Failed for room Type :" + roomType[i]);
            	e.printStackTrace();
            }
            browserHelper.openUrl(urlHDPage);    
        }
        String currURL = browserHelper.getCurrentUrl();
        browserHelper.openUrl("http://www.treebohotels.com/qa/allHotels");
        String abc = driver.findElement(By.cssSelector("body")).getText().replaceAll("[\\[{}\\],'<>:\"]", "");
        System.out.println(abc);
        boolean hotelAddedInApp = abc.contains(hotelName.replaceAll("'",""));
        browserHelper.openUrl(currURL);
        
        System.out.println("Start Token");
        System.out.println("Hotel Live process Summary for:   " + hotelName + "\n");
        System.out.println("SEO Page Title:  " + hotelResultsPageTitle + "\n");
        System.out.println("HD Page Title:  " + hotelDetailsPageTitle + "\n");
        if(!metaDesc){
        	System.out.println("Meta tag description not added!\n");
        }else{
        	System.out.println("Meta tag description :");
            System.out.println(hotelDetailsPageMetaDescription + "\n");
        }
        
        if (citySEODisplayed){
            if (cityNameInSEODescHeader && cityNameInSEODesc){
            	System.out.println("SEO description is displayed alongwith City Name header " +  "\n");
            }else if(cityNameInSEODescHeader && !cityNameInSEODesc){
            	System.out.println("SEO City Name header is displayed but description is missing " +  "\n");
            }else if(!cityNameInSEODescHeader && !cityNameInSEODesc){
            	System.out.println("SEO description is missing " +  "\n");
            }
        }else{
        	System.out.println("SEO city name/description missing! Need to be fixed" +  "\n");
        }

        
        
        System.out.println("Trilights:\n" + trilightContent );
        if (!trilightStatus){
        	System.out.println("\nAlert!!! Trilights are not displayed properly. Need to be fixed. ");
        }
        
        if (!accessbilityContentDisplayed){
        	System.out.println("\n Hotel Accessibilities content is not displayed");
        }else{
        	System.out.println("\n" + accessBilityContent);
        }
        
        System.out.println("\nImages duplicate (by file name) in Search Page? ");
        if (dupeImageInSearch.isEmpty()){
        	System.out.println("No");
        }else{
        	System.out.println("Alert!!! " + "Duplicate images in Search page found! Need to be fixed:  " + dupeImageInSearch.toString());
        }
        
        System.out.println("\nImages duplicate (by file name) in HD Page? ");
        if (dupeImageInHD.isEmpty()){
        	System.out.println("No");
        }else{
        	System.out.println("Alert!!! " + "Duplicate images in HD page found! Need to be fixed:  " + dupeImageInHD.toString());
        }
        
        if (distanceDisplayedOnCityPage) {
        	System.out.println("\nAlert !!! In SEO Page distance showed : " + distanceDisplayedOnCityPage );
        }else{
        	System.out.println("\nIn SEO Page distance is not shown. Nothing to worry." );
        }
        
        System.out.println("\nBookings done :");
        for (String x : roomTypeBooked){
        	System.out.println(x+"\n");
        }
        
        System.out.println(hotelName + " Added in Feedback and Prowl App?");  
        if(hotelAddedInApp){
        	System.out.println("Yes");
        }else{
        	System.out.println("Alert!!! Need to be added to app");
        }
        System.out.println("");
        
        System.out.println("End Token");
        verify.assertAll();
	}
	
	@AfterMethod(alwaysRun = true)
	public void cancelBooking() {
		try{
			homePage = new HomePage();
			browserHelper = new BrowserHelper();
			bookingHistory = new BookingHistory();
			CommonUtils utils = new CommonUtils();
			browserHelper.openUrl(browserHelper.getCurrentUrl());

			if (!homePage.isUserSignedIn()) {
				System.out.println("User is not signed in! Doing sign-in to cancel booking...");
				homePage.signInAsTreeboMember(utils.getProperty("TestBookingAccount"),
						utils.getProperty("TestLoginPassword"));
			} else {
				System.out.println("User is already signed in!");
			}
			bookingHistory.cancelAllBookings();
		}catch(Exception e){
			e.printStackTrace();
		}
	}
}