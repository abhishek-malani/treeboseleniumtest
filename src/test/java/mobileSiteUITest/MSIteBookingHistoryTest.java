package mobileSiteUITest;

import java.sql.SQLException;
import java.text.ParseException;

import org.apache.commons.lang3.RandomStringUtils;
import org.testng.Assert;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;
import base.BrowserHelper;
import base.TestBaseSetUp;
import mobileSiteUIPageObjects.ConfirmationScreen;
import mobileSiteUIPageObjects.HomeScreen;
import mobileSiteUIPageObjects.AccountScreen;
import mobileSiteUIPageObjects.HotelSearchResultsScreen;
import mobileSiteUIPageObjects.ItineraryScreen;
import utils.CommonUtils;
import utils.DBUtils;

public class MSIteBookingHistoryTest extends TestBaseSetUp {
	
	private HomeScreen homeScreen;
	private AccountScreen accountScreen;
	private HotelSearchResultsScreen hotelSearchResultScreen;
	private ItineraryScreen itineraryScreen;
	private ConfirmationScreen confirmationScreen;
	private BrowserHelper browserHelper;

	/**
	 * Login and click on Bookings link, booking history page should open
	 */
	@Test(groups = { "MSite", "MSiteBookingHistory" })
	public void testMobileBookingHistory() {
		homeScreen = new HomeScreen();
		accountScreen = new AccountScreen();

		CommonUtils utils = new CommonUtils();
		SoftAssert verify = new SoftAssert();

		String guestEmail = utils.getProperty("TestAccountLogin");
		String guestPassword = utils.getProperty("TestLoginPassword");
		String guestName = utils.getProperty("GuestName");

		// verify home screen is displayed
		verify.assertTrue(homeScreen.isHomeScreenDisplayed(), "Home screen is not displayed");

		// Login
		homeScreen.openDrawer();
		homeScreen.clickOnLogin();
		homeScreen.loginAsTreeboMember(guestEmail, guestPassword);
		homeScreen.openDrawer();
		accountScreen.clickBookingsLink();
		Assert.assertTrue(accountScreen.isBookingHistoryScreenOpened(), "Booking History screen not opened!");
		Assert.assertTrue(accountScreen.isUpcomingStaysHeaderDisplayed(), "Upcoming stay header not dispalyed!");
	}
	
	/**
	 * This test do a booking and cancel from booking history
	 * also verify OTP and its popup content
	 * @throws ParseException 
	 * @throws SQLException 
	 * @throws InterruptedException 
	 */
	@Test(groups={"MSite" , "MSiteBookingHistory"})
	public void testMobileBookingAndCancelationThroughBookingHistory() throws ParseException, SQLException, InterruptedException{
		homeScreen = new HomeScreen();
		hotelSearchResultScreen = new HotelSearchResultsScreen();
		itineraryScreen = new ItineraryScreen(); 
		confirmationScreen = new ConfirmationScreen();
		accountScreen = new AccountScreen();
		browserHelper = new BrowserHelper();
		CommonUtils utils = new CommonUtils();
		DBUtils dbUtills = new DBUtils();
		SoftAssert verify = new SoftAssert();
		
		String location = utils.getProperty("city"); // "Bengaluru";
		String testHotel = utils.getProperty("DummyHotel"); // "Elmas";
		String testUrl = utils.getProperty("TestUrl");
		int hotelIndex = 0;
		int checkInFromCurrentDate = utils.getRandomNumber(30, 45);
		int checkOutFromCurrentDate = checkInFromCurrentDate + 1;
		String[] checkInCheckOutDates = new String[2];
		
		// verify home screen is displayed
		verify.assertTrue(homeScreen.isHomeScreenDisplayed(),"Home screen is not displayed");
		
		//Register
		homeScreen.openDrawer();
		homeScreen.clickOnSignUp();
		String[] guestInfo = homeScreen.registerOnHomePage();
		
		homeScreen.openDrawer();
		verify.assertTrue(homeScreen.isUserSignedIn(),"Not signed in!");
		verify.assertEquals(homeScreen.getLoggedInUserText(), "Hi " + guestInfo[0], "User name is not matching");
		
		homeScreen.closeDrawer();
		
		// Check if home screen is displayed
		verify.assertTrue(homeScreen.isHomeScreenDisplayed(),"Home Screen is not displayed");
		//Do Search
		checkInCheckOutDates = homeScreen.doSearch(location, checkInFromCurrentDate, checkOutFromCurrentDate);
		// check if search results displayed
		verify.assertTrue(hotelSearchResultScreen.isHotelSearchScreenDisplayed(),"search result screen not displayed");
		//Select a random hotel or test hotel
		if (browserHelper.getCurrentUrl().contains(testUrl)) {
			hotelIndex = hotelSearchResultScreen.getAllAvailableHotelNames().indexOf(testHotel);
		}else{
			hotelIndex = hotelSearchResultScreen.getRandomIndexOfAvailableHotel();
		}
		
		String hotelName = hotelSearchResultScreen.getHotelNameByIndex(hotelIndex);
		int hotelRoomRate = hotelSearchResultScreen.getRoomRateByIndex(hotelIndex);
		System.out.println("Hotel Name: " + hotelName + " Hotel room rate : " + hotelRoomRate );

		
		//Click quick book 
		hotelSearchResultScreen.clickQuickBookByIndex(hotelIndex);
		//Check If Itinerary screen
		verify.assertTrue(itineraryScreen.isItineraryScreenDisplayed());
		// click on next
		itineraryScreen.clickOnNextStepOne();
        verify.assertEquals(itineraryScreen.getGuestName(),guestInfo[0],"Prefilled name on itinerary is not correct");
        verify.assertEquals(itineraryScreen.getGuestMobile(),guestInfo[1],"Prefilled mobile on itinerary is not correct");
        verify.assertEquals(itineraryScreen.getGuestEmail(),guestInfo[2],"Prefilled email on itinerary is not correct");
		itineraryScreen.clickOnPayAtHotel();
		
		//otp verification
		verify.assertTrue(itineraryScreen.doesOTPPopupDisplayed(),"OTP Pop up is not displayed");
		
		//check otp title
		verify.assertTrue(itineraryScreen.doesOTPPopupTitleDisplayed(),"OTP title is not displayed");
		
		//check change number clickable
		verify.assertTrue(itineraryScreen.doesClickableChangeNumber(),"change number is not clickable");
		
		//check resend sms clickable
		verify.assertTrue(itineraryScreen.doesClickableResendSms(),"Resend sms is not clickable");
		
		String popupMobile = itineraryScreen.extractMobileNumberFromOTPPopup();
		
		
		verify.assertTrue(itineraryScreen.doesBothStringEqual(popupMobile.trim(), itineraryScreen.getGuestMobile().trim()));
		verify.assertTrue(itineraryScreen.doesVerifyButtonDisplayed(),"Verify button is not displayed");
		
		String wrongOTP = "111111";
		itineraryScreen.enterOTP(wrongOTP.trim());
		itineraryScreen.clickOnVerifyButton();
		Thread.sleep(3000);
		verify.assertTrue(itineraryScreen.doesErrorMessageDisplayed(),"Enter Correct OTP message is not displayed");
		
		
		String otp = null;

	    otp = dbUtills.getOTP(itineraryScreen.getGuestMobile().trim());
	    
	    //enter otp
	    itineraryScreen.enterOTP(otp.trim());
	    
	     itineraryScreen.clickOnVerifyButton();
	    
//	    Thread.sleep(5000);
		
		verify.assertTrue(confirmationScreen.isConfirmationScreenDisplayed(),"Confirmation screen not displayed");
		String bookingId = confirmationScreen.getBookingId();
		System.out.println("Booking id : " + bookingId);
		int total = confirmationScreen.getTotalCost();
		verify.assertEquals(guestInfo[0], confirmationScreen.getGuestName());
		verify.assertEquals(hotelName, confirmationScreen.getHotelName());
		verify.assertEquals(1 , confirmationScreen.getGuestCount());
		verify.assertEquals(1 , confirmationScreen.getRoomCount());
		verify.assertEquals(utils.formatDate(checkInCheckOutDates[0]).toUpperCase(),
				confirmationScreen.getCheckInDate().toUpperCase());
		verify.assertEquals(utils.formatDate(checkInCheckOutDates[1]).toUpperCase(),
				confirmationScreen.getCheckOutDate().toUpperCase());
		verify.assertTrue(confirmationScreen.isPayAtHotelImageDisplayed(),"Pay At Hotel image not displayed");
		
		// Now navigate to booking history
		homeScreen.openDrawer();
		accountScreen.clickBookingsLink();
		verify.assertTrue(accountScreen.isBookingHistoryScreenOpened(), "Booking History screen not opened!");
		verify.assertTrue(accountScreen.isUpcomingStaysHeaderDisplayed(), "Upcoming stay header not dispalyed!");
		
		verify.assertEquals(accountScreen.getHotelName(), hotelName,"Hotel Name is not correct");
		verify.assertEquals(accountScreen.getBookingStatus(), "Confirmed","Booking status is not confirmed");
		
		accountScreen.openBookingDetailsByBookingId(bookingId);
		verify.assertTrue(accountScreen.isBookingDetailsScreenOpened(),"Booking details not opened");
		
		verify.assertEquals(accountScreen.getGuestName(), guestInfo[0],"Guest name mismatch");
		verify.assertEquals(accountScreen.getGuestMobile(), guestInfo[1],"Guest name mismatch");
		verify.assertEquals(accountScreen.getGuestEmail(), guestInfo[2],"Guest name mismatch");
		verify.assertEquals((int) Math.round(accountScreen.getTotalAmount()), total,"Total is not matching");
		
		// Now cancel booking
		accountScreen.cancelBooking();
		
		//Now status should change to cancelled
		verify.assertEquals(accountScreen.getBookingStatus(), "Cancelled","Booking status is not cancelled");
		
		verify.assertAll();
	}
	/*
	 * This test do a booking  as guest
	 * also verify OTP and its pop up content
	 * @throws ParseException 
	 * @throws SQLException 
	 * @throws InterruptedException 
	 */
	
	@Test(groups={"MSite" , "MSiteBookingHistory"})
	public void testMobileBookingAsGuestUser() throws SQLException{
		homeScreen = new HomeScreen();
		hotelSearchResultScreen = new HotelSearchResultsScreen();
		itineraryScreen = new ItineraryScreen(); 
		confirmationScreen = new ConfirmationScreen();
		accountScreen = new AccountScreen();
		browserHelper = new BrowserHelper();
		CommonUtils utils = new CommonUtils();
		DBUtils dbUtills = new DBUtils();
		SoftAssert verify = new SoftAssert();
		
		String location = utils.getProperty("city"); // "Bengaluru";
		String testHotel = utils.getProperty("DummyHotel"); // "Elmas";
		String testUrl = utils.getProperty("TestUrl");
		int hotelIndex = 0;
		int checkInFromCurrentDate = utils.getRandomNumber(30, 45);
		int checkOutFromCurrentDate = checkInFromCurrentDate + 1;
		String[] checkInCheckOutDates = new String[2];
		String guestName = utils.getProperty("GuestName"); // "Test";
		String guestEmail = browserHelper.getDisposableEmail();
		String guestMobile = Integer.toString(utils.getRandomNumber(2, 6)) + RandomStringUtils.randomNumeric(9);

		
		// verify home screen is displayed
		// verify.assertTrue(homeScreen.isHomeScreenDisplayed(),"Home screen is
		// not displayed");

		// Check if home screen is displayed
		verify.assertTrue(homeScreen.isHomeScreenDisplayed(), "Home Screen is not displayed");
		// Do Search
		checkInCheckOutDates = homeScreen.doSearch(location, checkInFromCurrentDate, checkOutFromCurrentDate);
		// check if search results displayed
		verify.assertTrue(hotelSearchResultScreen.isHotelSearchScreenDisplayed(), "search result screen not displayed");
		// Select a random hotel or test hotel
		if (browserHelper.getCurrentUrl().contains(testUrl)) {
			hotelIndex = hotelSearchResultScreen.getAllAvailableHotelNames().indexOf(testHotel);
		} else {
			hotelIndex = hotelSearchResultScreen.getRandomIndexOfAvailableHotel();
		}

		String hotelName = hotelSearchResultScreen.getHotelNameByIndex(hotelIndex);
		int hotelRoomRate = hotelSearchResultScreen.getRoomRateByIndex(hotelIndex);
		System.out.println("Hotel Name: " + hotelName + " Hotel room rate : " + hotelRoomRate);

		// Click quick book
		hotelSearchResultScreen.clickQuickBookByIndex(hotelIndex);
		// Check If Itinerary screen
		verify.assertTrue(itineraryScreen.isItineraryScreenDisplayed());
		
		//click on next button
		itineraryScreen.clickOnNextStepOne();
		
		itineraryScreen.continueAsGuest();
		
		itineraryScreen.enterGuestDetails(guestName, guestMobile, guestEmail);
		
        itineraryScreen.clickOnPayAtHotel();
		
		//otp verification
		verify.assertTrue(itineraryScreen.doesOTPPopupDisplayed(),"OTP Pop up is not displayed");
		
		//check otp title
		verify.assertTrue(itineraryScreen.doesOTPPopupTitleDisplayed(),"OTP title is not displayed");
		
		//check change number clickable
		verify.assertTrue(itineraryScreen.doesClickableChangeNumber(),"change number is not clickable");
		
		//check resend sms clickable
		verify.assertTrue(itineraryScreen.doesClickableResendSms(),"Resend sms is not clickable");
		
		String popupMobile = itineraryScreen.extractMobileNumberFromOTPPopup();
		
		
		verify.assertTrue(itineraryScreen.doesBothStringEqual(popupMobile.trim(), itineraryScreen.getGuestMobile().trim()));
		
		String wrongOTP = "111111";
		itineraryScreen.enterOTP(wrongOTP.trim());
		itineraryScreen.clickOnVerifyButton();
		try {
			Thread.sleep(3000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		verify.assertTrue(itineraryScreen.doesErrorMessageDisplayed(),"Enter Correct OTP message is not displayed");
		
		String otp = null;

	    otp = dbUtills.getOTP(itineraryScreen.getGuestMobile().trim());
	    
	    //enter otp
	    itineraryScreen.enterOTP(otp.trim());
	    
	    verify.assertTrue(itineraryScreen.doesVerifyButtonDisplayed(),"Verify button is not displayed");
	    
	    itineraryScreen.clickOnVerifyButton();
	    verify.assertTrue(confirmationScreen.isConfirmationScreenDisplayed(),"Confirmation screen not displayed");
		String bookingId = confirmationScreen.getBookingId();
		System.out.println("Booking id : " + bookingId);
		int total = confirmationScreen.getTotalCost();
		
		verify.assertEquals(hotelName, confirmationScreen.getHotelName());
		verify.assertEquals(1 , confirmationScreen.getGuestCount());
		verify.assertEquals(1 , confirmationScreen.getRoomCount());
		
		verify.assertTrue(confirmationScreen.isPayAtHotelImageDisplayed(),"Pay At Hotel image not displayed");
		
	    verify.assertAll();

	}
	/*
	 * This test do a booking as guest user
	 * also verify OTP  popup content
	 * skip OTP verification and go to transaction page
	 * @throws ParseException 
	 * @throws SQLException 
	 * @throws InterruptedException 
	 */
	@Test(groups={"MSite" , "MSiteBookingHistory"})
	public void testMobileBookingAsGuestUserSkipOTP() throws SQLException, InterruptedException{
		homeScreen = new HomeScreen();
		hotelSearchResultScreen = new HotelSearchResultsScreen();
		itineraryScreen = new ItineraryScreen(); 
		confirmationScreen = new ConfirmationScreen();
		accountScreen = new AccountScreen();
		browserHelper = new BrowserHelper();
		CommonUtils utils = new CommonUtils();
		DBUtils dbUtills = new DBUtils();
		SoftAssert verify = new SoftAssert();
		
		String location = utils.getProperty("city"); // "Bengaluru";
		String testHotel = utils.getProperty("DummyHotel"); // "Elmas";
		String testUrl = utils.getProperty("TestUrl");
		int hotelIndex = 0;
		int checkInFromCurrentDate = utils.getRandomNumber(30, 45);
		int checkOutFromCurrentDate = checkInFromCurrentDate + 1;
		String[] checkInCheckOutDates = new String[2];
		String guestName = utils.getProperty("GuestName"); // "Test";
		String guestEmail = browserHelper.getDisposableEmail();
		String guestMobile = Integer.toString(utils.getRandomNumber(2, 6)) + RandomStringUtils.randomNumeric(9);

		
		// verify home screen is displayed
		// verify.assertTrue(homeScreen.isHomeScreenDisplayed(),"Home screen is
		// not displayed");

		// Check if home screen is displayed
		verify.assertTrue(homeScreen.isHomeScreenDisplayed(), "Home Screen is not displayed");
		// Do Search
		checkInCheckOutDates = homeScreen.doSearch(location, checkInFromCurrentDate, checkOutFromCurrentDate);
		// check if search results displayed
		verify.assertTrue(hotelSearchResultScreen.isHotelSearchScreenDisplayed(), "search result screen not displayed");
		// Select a random hotel or test hotel
		if (browserHelper.getCurrentUrl().contains(testUrl)) {
			hotelIndex = hotelSearchResultScreen.getAllAvailableHotelNames().indexOf(testHotel);
		} else {
			hotelIndex = hotelSearchResultScreen.getRandomIndexOfAvailableHotel();
		}

		String hotelName = hotelSearchResultScreen.getHotelNameByIndex(hotelIndex);
		int hotelRoomRate = hotelSearchResultScreen.getRoomRateByIndex(hotelIndex);
		System.out.println("Hotel Name: " + hotelName + " Hotel room rate : " + hotelRoomRate);

		// Click quick book
		hotelSearchResultScreen.clickQuickBookByIndex(hotelIndex);
		// Check If Itinerary screen
		verify.assertTrue(itineraryScreen.isItineraryScreenDisplayed());
		
		//click on next button
		itineraryScreen.clickOnNextStepOne();
		
		itineraryScreen.continueAsGuest();
		
		itineraryScreen.enterGuestDetails(guestName, guestMobile, guestEmail);
		
        itineraryScreen.clickOnPayAtHotel();
		
		//otp verification
		verify.assertTrue(itineraryScreen.doesOTPPopupDisplayed(),"OTP Pop up is not displayed");
		
		//check otp title
		verify.assertTrue(itineraryScreen.doesOTPPopupTitleDisplayed(),"OTP title is not displayed");
		
		//check change number clickable
		verify.assertTrue(itineraryScreen.doesClickableChangeNumber(),"change number is not clickable");
		
		//check resend sms clickable
		verify.assertTrue(itineraryScreen.doesClickableResendSms(),"Resend sms is not clickable");
		
		String popupMobile = itineraryScreen.extractMobileNumberFromOTPPopup();
		
		
		verify.assertTrue(itineraryScreen.doesBothStringEqual(popupMobile.trim(), itineraryScreen.getGuestMobile().trim()));
		verify.assertTrue(itineraryScreen.doesPayNowOptionDisplayedOnOTPPopup(),"Skip to Pay now option is not displayed ");
		
		itineraryScreen.clickOnSkipIWillPayNow();
	
		verify.assertTrue(itineraryScreen.doesTransactionPageTitleDisplayed(),"Transaction Page Title is not displayed");
		
		String transactionMobile=itineraryScreen.getMobileNumberFromTransactionPage().trim();
		String transactionEmail=itineraryScreen.getEmailAddressFromTransactionPage().trim();
		
		verify.assertTrue(itineraryScreen.doesBothStringEqual(transactionMobile.trim(),guestMobile.trim() ));
		verify.assertTrue(itineraryScreen.doesBothStringEqual(transactionEmail.trim(), guestEmail.trim()));
		
		verify.assertAll();
	}
	
	/*
	 * This test do a booking and cancel from booking history
	 * as guest user
	 * also verify OTP popup content
	 * change mobile number and verify that change mobile number popup
	 * 
	 * @throws ParseException 
	 * @throws SQLException 
	 * @throws InterruptedException 
	 */
	@Test(groups={"MSite" , "MSiteBookingHistory"})
	public void testMobileBookingAsGuestUserChangeMobileNumberAtOTPPopup() throws SQLException, InterruptedException{
		homeScreen = new HomeScreen();
		hotelSearchResultScreen = new HotelSearchResultsScreen();
		itineraryScreen = new ItineraryScreen(); 
		confirmationScreen = new ConfirmationScreen();
		accountScreen = new AccountScreen();
		browserHelper = new BrowserHelper();
		CommonUtils utils = new CommonUtils();
		DBUtils dbUtills = new DBUtils();
		SoftAssert verify = new SoftAssert();
		
		String location = utils.getProperty("city"); // "Bengaluru";
		String testHotel = utils.getProperty("DummyHotel"); // "Elmas";
		String testUrl = utils.getProperty("TestUrl");
		int hotelIndex = 0;
		int checkInFromCurrentDate = utils.getRandomNumber(30, 45);
		int checkOutFromCurrentDate = checkInFromCurrentDate + 1;
		String[] checkInCheckOutDates = new String[2];
		String guestName = utils.getProperty("GuestName"); // "Test";
		String guestEmail = browserHelper.getDisposableEmail();
		String guestMobile = Integer.toString(utils.getRandomNumber(2, 6)) + RandomStringUtils.randomNumeric(9);

		
		// verify home screen is displayed
		// verify.assertTrue(homeScreen.isHomeScreenDisplayed(),"Home screen is
		// not displayed");

		// Check if home screen is displayed
		verify.assertTrue(homeScreen.isHomeScreenDisplayed(), "Home Screen is not displayed");
		// Do Search
		checkInCheckOutDates = homeScreen.doSearch(location, checkInFromCurrentDate, checkOutFromCurrentDate);
		// check if search results displayed
		verify.assertTrue(hotelSearchResultScreen.isHotelSearchScreenDisplayed(), "search result screen not displayed");
		// Select a random hotel or test hotel
		if (browserHelper.getCurrentUrl().contains(testUrl)) {
			hotelIndex = hotelSearchResultScreen.getAllAvailableHotelNames().indexOf(testHotel);
		} else {
			hotelIndex = hotelSearchResultScreen.getRandomIndexOfAvailableHotel();
		}

		String hotelName = hotelSearchResultScreen.getHotelNameByIndex(hotelIndex);
		int hotelRoomRate = hotelSearchResultScreen.getRoomRateByIndex(hotelIndex);
		System.out.println("Hotel Name: " + hotelName + " Hotel room rate : " + hotelRoomRate);

		// Click quick book
		hotelSearchResultScreen.clickQuickBookByIndex(hotelIndex);
		// Check If Itinerary screen
		verify.assertTrue(itineraryScreen.isItineraryScreenDisplayed());
		
		//click on next button
		itineraryScreen.clickOnNextStepOne();
		
		itineraryScreen.continueAsGuest();
		
		itineraryScreen.enterGuestDetails(guestName, guestMobile, guestEmail);
		
        itineraryScreen.clickOnPayAtHotel();
		
		//otp verification
		verify.assertTrue(itineraryScreen.doesOTPPopupDisplayed(),"OTP Pop up is not displayed");
		
		//check otp title
		verify.assertTrue(itineraryScreen.doesOTPPopupTitleDisplayed(),"OTP title is not displayed");
		
		//check change number clickable
		verify.assertTrue(itineraryScreen.doesClickableChangeNumber(),"change number is not clickable");
		
		//check resend sms clickable
		verify.assertTrue(itineraryScreen.doesClickableResendSms(),"Resend sms is not clickable");
		
		String popupMobile = itineraryScreen.extractMobileNumberFromOTPPopup();
	
		
		verify.assertTrue(itineraryScreen.doesBothStringEqual(popupMobile.trim(), itineraryScreen.getGuestMobile().trim()));
		
		Thread.sleep(3000);
        
		itineraryScreen.clickOnChangeNumber();
		
		String updateNumber = Integer.toString(utils.getRandomNumber(2, 6)) + RandomStringUtils.randomNumeric(9);
		
		Thread.sleep(3000);
		
		itineraryScreen.changeNumber(updateNumber.trim());
		
		verify.assertTrue(itineraryScreen.doesSendOTPButtonDisplayed(),"Send OTP button is not displayed");
		
		Thread.sleep(3000);
		
		itineraryScreen.clickOnSendOTP();
		
		Thread.sleep(3000);
		String wrongOTP = "111111";
		itineraryScreen.enterOTP(wrongOTP.trim());
		itineraryScreen.clickOnVerifyButton();
		Thread.sleep(3000);
		verify.assertTrue(itineraryScreen.doesErrorMessageDisplayed(),"Enter Correct OTP message is not displayed");
		
		String otp = null;

	    otp = dbUtills.getOTP(updateNumber.trim());
	    
	    //enter otp
	    itineraryScreen.enterOTP(otp.trim());
	    
	    //check Verify button
        verify.assertTrue(itineraryScreen.doesVerifyButtonDisplayed(),"Verify button is not displayed");
        itineraryScreen.clickOnVerifyButton();
        verify.assertTrue(confirmationScreen.isConfirmationScreenDisplayed(),"Confirmation screen not displayed");
		String bookingId = confirmationScreen.getBookingId();
		System.out.println("Booking id : " + bookingId);
		int total = confirmationScreen.getTotalCost();
		
		verify.assertEquals(hotelName, confirmationScreen.getHotelName());
		verify.assertEquals(1 , confirmationScreen.getGuestCount());
		verify.assertEquals(1 , confirmationScreen.getRoomCount());
		
		verify.assertTrue(confirmationScreen.isPayAtHotelImageDisplayed(),"Pay At Hotel image not displayed");
		
		
	   verify.assertAll();
	}
	
	
	/***
	 * Open homePage
	 * open drawer
	 * click on login link
	 * login with treebo credentials
	 * then click on user account
	 * then click on myreferrals
	 * verify the content on that page
	 */
	@Test(groups={"MSite" , "MSiteBookingHistory"})
	public void testMyAccountMobile() throws InterruptedException {
		homeScreen = new HomeScreen();
		accountScreen = new AccountScreen();

		CommonUtils utils = new CommonUtils();
		SoftAssert verify = new SoftAssert();

		String guestEmail = utils.getProperty("TestAccountLogin");
		String guestPassword = utils.getProperty("TestLoginPassword");

		// verify home screen is displayed
		verify.assertTrue(homeScreen.isHomeScreenDisplayed(), "Home screen is not displayed");

		// Login
		homeScreen.openDrawer();
		homeScreen.clickOnLogin();
		homeScreen.loginAsTreeboMember(guestEmail, guestPassword);
		homeScreen.openDrawer();
		Thread.sleep(5000);
		homeScreen.clickOnReferrals();

		verify.assertTrue(homeScreen.isMainTitleTextDisplayed(),"Main Title text is not displayed");
		verify.assertTrue(homeScreen.isTransparentLinkDisplayed(),"Transparent link is not displayed");
		verify.assertTrue(homeScreen.isCopyButtonDisplayed(),"Copy button is not displayed");
		verify.assertTrue(homeScreen.isFacebookLinkDisplayed(),"Facebook link is not displayed");
		verify.assertTrue(homeScreen.isWhatsAppLinkDisplayed(),"WhatsApp link is not displayed");
		verify.assertTrue(homeScreen.isGmailLinkDisplayed(),"Gmail link is not displayed");
		verify.assertTrue(homeScreen.doesSignedUpTextDisplayed(),"Signed Up is not displayed");
		verify.assertTrue(homeScreen.doesConvertedTextDisplayed(),"Convertedt is not displayed");
		
		verify.assertAll();
	}
	
}