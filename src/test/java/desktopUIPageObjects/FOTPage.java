package desktopUIPageObjects;

import java.util.Arrays;
import java.util.List;

import org.apache.commons.lang3.RandomStringUtils;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import base.BrowserHelper;
import base.DriverManager;
import utils.CommonUtils;
import desktopUIPageObjects.HomePage;
import desktopUIPageObjects.HotelResultsPage;

public class FOTPage {
	private BrowserHelper browserHelper;
	private HomePage homePage;
	private HotelResultsPage hotelResultsPage;

	private By fotPage = By.cssSelector(".fotpage");
	private By joinUsLink = By.xpath("//a[@href='/fot/signup/']/div");
	private By loginLink = By.cssSelector(".js-headerlogin.btn.signup-schedule__login.submit");
	private By signUpPage = By.cssSelector(".signupPage");
	private By fotName = By.cssSelector("#userName");
	private By fotEmail = By.cssSelector("#email");
	private By fotMobile = By.cssSelector("#mobile");
	private By fotPassword = By.cssSelector("#pwd");
	private By confirmfotPassword = By.cssSelector("#confirmPwd");
	private By fotContinue = By.cssSelector("#signupSubmit");
	private By continueToForm = By.xpath(
			"//div[contains(@class,'fot-success-fail')][not(contains(@class,'hide'))]//input[@id='continueToForm']");
	private By regSuccess = By.xpath("//div[@id='testSuccess'][not(contains(@class,'hide'))]");
	private By scheduleFirstAuditLink = By.cssSelector(".schedule-first-audit");
	private By scheduleAuditLink = By.xpath("//a[contains(@href,'/fot/search')]");
	private By bookAnotherAudit = By.cssSelector("#bookAnotherAudit");
	private By fotHomePageDesc = By.cssSelector(".fot-image__desc");

	public void openFOTPage() {
		WebDriver driver = DriverManager.getInstance().getDriver();
		browserHelper = new BrowserHelper();
		homePage = new HomePage();
		// Home Page is Displayed
		homePage.verifyHomePagePresence();

		browserHelper.openUrl(browserHelper.getCurrentUrl() + "fot");
		
		//check if 500 page
		if (homePage.is500Displayed()){
			Assert.fail("500 displayed instead of FOT page for url " + browserHelper.getCurrentUrl());
		}
		
		Assert.assertTrue(driver.findElement(fotPage).isDisplayed(),"FOT Page is not displayed " + browserHelper.getCurrentUrl());
		System.out.println("FOT Page is opened");
	}

	public String[] fotSignUp() {
		WebDriver driver = DriverManager.getInstance().getDriver();
		driver.findElement(joinUsLink).click();
		Assert.assertTrue(isSignUpPageDisplayed(), "Sign up page not displayed");
		String[] guestDetails = enterSignUpUserDetails();
		continueToFOTSignUp();
		fillPersonalInfo();
		selectProblemSolvingAnswers();
		Assert.assertTrue(isRegistrationSuccessful(), "Sign up page not displayed");
		return guestDetails;
	}

	public boolean isSignUpPageDisplayed() {
		WebDriver driver = DriverManager.getInstance().getDriver();
		return (driver.findElements(signUpPage).size() > 0);
	}

	public String[] enterSignUpUserDetails() {
		WebDriver driver = DriverManager.getInstance().getDriver();
		CommonUtils utils = new CommonUtils();
		
		String name = "Test";
//		String uniqueEmail = "test" + RandomStringUtils.randomAlphanumeric(15);
//		String email = uniqueEmail + "@gmail.com";
		browserHelper = new BrowserHelper();
		String email = browserHelper.getDisposableEmail();
		String mobile = Integer.toString(utils.getRandomNumber(2, 6)) + RandomStringUtils.randomNumeric(9);
		String password = "password";

		System.out.println("Default Sign up with following details. Name : " + name + ", mobile :" + mobile
				+ ", email : " + email + ", password:" + password);
		driver.findElement(fotName).sendKeys(name);
		driver.findElement(fotMobile).sendKeys(mobile);
		driver.findElement(fotEmail).sendKeys(email);
		driver.findElement(fotPassword).sendKeys(password);
		driver.findElement(confirmfotPassword).sendKeys(password);
		driver.findElement(fotContinue).click();
		String[] guestDetails = { name, email, mobile, password };
		return guestDetails;
	}

	public void continueToFOTSignUp() {
		WebDriver driver = DriverManager.getInstance().getDriver();
		WebDriverWait wait = new WebDriverWait(driver, 120);
		wait.until(ExpectedConditions.presenceOfElementLocated(continueToForm));
		driver.findElement(continueToForm).click();
	}

	public void fillPersonalInfo() {
		WebDriver driver = DriverManager.getInstance().getDriver();
		CommonUtils utils = new CommonUtils();
		String city = utils.getProperty("city");
		driver.findElement(By.cssSelector("#address")).sendKeys(city);
		driver.findElement(By.cssSelector("input[value='Married']")).click();
		driver.findElement(By.cssSelector("input[value='month']")).click();
		driver.findElement(By.xpath("//div[@id='personal-info']//input[@type='button']")).click();
	}

	public void selectProblemSolvingAnswers() {
		WebDriver driver = DriverManager.getInstance().getDriver();
		// Question 1, answer 1
		driver.findElement(By
				.xpath("//div[contains(@class,'signup-page__enter-detail')][not(contains(@class,'hide'))]/div[@class='fot-form'][1]//*[contains(@class,'fot-form__check')][1]//input"))
				.click();
		// Question 2, answer 1
		driver.findElement(By
				.xpath("//div[contains(@class,'signup-page__enter-detail')][not(contains(@class,'hide'))]/div[@class='fot-form'][2]//*[contains(@class,'fot-form__check')][1]//input"))
				.click();
		// Question 3, answer 1
//		driver.findElement(By
//				.xpath("//div[contains(@class,'signup-page__enter-detail')][not(contains(@class,'hide'))]/div[@class='fot-form'][3]//*[contains(@class,'fot-form__check')][1]//input"))
//				.click();
		// Question 3, answer 4
		driver.findElement(By
				.xpath("//div[contains(@class,'signup-page__enter-detail')][not(contains(@class,'hide'))]/div[@class='fot-form'][3]//*[contains(@class,'fot-form__check')][4]//input"))
				.click();
		// Question 4, answer 4
		driver.findElement(By
				.xpath("//div[contains(@class,'signup-page__enter-detail')][not(contains(@class,'hide'))]/div[@class='fot-form'][4]//*[contains(@class,'fot-form__check')][4]//input"))
				.click();
		// Question 5, answer 4
		driver.findElement(By
				.xpath("//div[contains(@class,'signup-page__enter-detail')][not(contains(@class,'hide'))]/div[@class='fot-form'][5]//*[contains(@class,'fot-form__check')][4]//input"))
				.click();
		// Question 6, answer 2
		driver.findElement(By
				.xpath("//div[contains(@class,'signup-page__enter-detail')][not(contains(@class,'hide'))]/div[@class='fot-form'][6]//*[contains(@class,'fot-form__check')][2]//input"))
				.click();

		// continue
		driver.findElement(By
				.xpath("//div[contains(@class,'signup-page__enter-detail')][not(contains(@class,'hide'))]//input[@type='button']"))
				.click();

		// Question 1, answer 2
		driver.findElement(By
				.xpath("//div[contains(@class,'signup-page__enter-detail')][not(contains(@class,'hide'))]/div[@class='fot-form'][1]/div[3]//input"))
				.click();
		// Question 2, answer 2
		driver.findElement(By
				.xpath("//div[contains(@class,'signup-page__enter-detail')][not(contains(@class,'hide'))]/div[@class='fot-form'][2]/div[3]//input"))
				.click();

		// Age check and Agree to terms and conditions
		driver.findElement(By.cssSelector("#ageCheck")).click();
		driver.findElement(By.cssSelector("#termsCheck")).click();

		// submit
		driver.findElement(By
				.xpath("//div[contains(@class,'signup-page__enter-detail')][not(contains(@class,'hide'))]//input[@type='button']"))
				.click();
	}

	public boolean isRegistrationSuccessful() {
		WebDriver driver = DriverManager.getInstance().getDriver();
		WebDriverWait wait = new WebDriverWait(driver, 120);
		wait.until(ExpectedConditions.presenceOfElementLocated(regSuccess));
		return (driver.findElements(regSuccess).size() > 0);
	}

	public boolean isScheduleFirstAuditDisplayed() {
		WebDriver driver = DriverManager.getInstance().getDriver();
		WebDriverWait wait = new WebDriverWait(driver, 30);
		wait.until(ExpectedConditions.presenceOfElementLocated(scheduleFirstAuditLink));
		return driver.findElement(scheduleFirstAuditLink).isDisplayed();
	}

	public void clickScheduleFirstAudit() {
		WebDriver driver = DriverManager.getInstance().getDriver();
		browserHelper = new BrowserHelper();
		driver.findElement(scheduleFirstAuditLink).click();
		browserHelper.waitTime(3000);
	}

	public boolean isScheduleAuditDisplayed() {
		WebDriver driver = DriverManager.getInstance().getDriver();
		WebDriverWait wait = new WebDriverWait(driver, 30);
		wait.until(ExpectedConditions.presenceOfElementLocated(scheduleAuditLink));
		return driver.findElement(scheduleAuditLink).isDisplayed();
	}

	public void clickScheduleAudit() {
		WebDriver driver = DriverManager.getInstance().getDriver();
		browserHelper = new BrowserHelper();
		driver.findElement(scheduleAuditLink).click();
		browserHelper.waitTime(10000);
	}

	public boolean isFOTSearchPageDisplayed() {
		WebDriver driver = DriverManager.getInstance().getDriver();
		return (driver.findElements(By.xpath("//div[@class='result-page']//select[@id='cityFot']")).size() > 0);
	}

	public boolean isDefaultSearchAsExpected() {
		WebDriver driver = DriverManager.getInstance().getDriver();
		DateTime dt = new DateTime();
		DateTimeFormatter dtf = DateTimeFormat.forPattern("dd/MM/yyyy");
		String date = dtf.print(dt.plusDays(1));
		return (driver.findElement(By.cssSelector(".dr-date")).getText().equals(date));
	}

	public void doFOTSearch(String strCity, int auditDateFromTodaysDate, int guestCount) {
		WebDriver driver = DriverManager.getInstance().getDriver();
		browserHelper = new BrowserHelper();
		Select cityDropdown = new Select(driver.findElement(By.id("cityFot")));
		cityDropdown.selectByValue(strCity);

		// Select Audit date
		driver.findElement(By.cssSelector(".dr-date")).click();
		browserHelper.waitTime(3000);
		// Take the count of available dates in current month
		int numDaysCurMonth = driver
				.findElements(By.xpath("//li[contains(@class,'dr-selected')][not(contains(@class,'dr-fade'))]")).size();
		if (auditDateFromTodaysDate <= numDaysCurMonth) {
			String xpathDays = String.format(
					"//li[contains(@class,'dr-selected')][not(contains(@class,'dr-fade'))][%d]",
					auditDateFromTodaysDate);
			driver.findElement(By.xpath(xpathDays)).click();
		} else {
			// move month to next
			driver.findElement(By.xpath("//i[contains(@class,'icon-right')][not(contains(@class,'dr-disabled'))]"))
					.click();
			browserHelper.waitTime(3000);
			int daysRemaining = auditDateFromTodaysDate - numDaysCurMonth;
			int numDaysNextMonth = driver
					.findElements(By.xpath("//li[contains(@class,'dr-selected')][not(contains(@class,'dr-fade'))]"))
					.size();
			if (daysRemaining <= numDaysNextMonth) {
				String xpathDaysNext = String.format(
						"//li[contains(@class,'dr-selected')][not(contains(@class,'dr-fade'))][%d]", daysRemaining);
				driver.findElement(By.xpath(xpathDaysNext)).click();
			} else {
				int finalDays = daysRemaining - numDaysNextMonth;
				// move month to next
				driver.findElement(By.xpath("//i[contains(@class,'icon-right')][not(contains(@class,'dr-disabled'))]"))
						.click();
				browserHelper.waitTime(3000);
				String xpathDaysFinal = String
						.format("//li[contains(@class,'dr-selected')][not(contains(@class,'dr-fade'))][%d]", finalDays);
				driver.findElement(By.xpath(xpathDaysFinal)).click();
			}
		}

		// Select Guest
		Select guest = new Select(driver.findElement(By.id("fotRoomConfig")));
		guest.selectByValue(Integer.toString(guestCount));
		driver.findElement(By.id("searchFotSubmitBtn")).click();
		browserHelper.waitTime(5000);
	}

	public boolean isJoinUSDisplayedOnFOTHomePage() {
		WebDriver driver = DriverManager.getInstance().getDriver();
		return (driver.findElements(joinUsLink).size() > 0);
	}

	public boolean isLoginDisplayedOnFOTHomePage() {
		WebDriver driver = DriverManager.getInstance().getDriver();
		return (driver.findElements(loginLink).size() > 0);
	}

	public void loginToFOT(String email, String password) {
		WebDriver driver = DriverManager.getInstance().getDriver();
		homePage = new HomePage();
		driver.findElement(loginLink).click();
		homePage.enterLoginForm(email, password);
	}

	public int getAvailableAuditDates() {
		WebDriver driver = DriverManager.getInstance().getDriver();
		browserHelper = new BrowserHelper();
		driver.findElement(By.cssSelector(".dr-date")).click();
		browserHelper.waitTime(3000);
		int auditDays = driver
				.findElements(By.xpath("//li[contains(@class,'dr-selected')][not(contains(@class,'dr-fade'))]")).size();
		System.out.println("Number of Audit days in current month " + auditDays);
		// click next month
		int days = -1;
		while ((driver.findElements(By.xpath("//i[contains(@class,'icon-right')][not(contains(@class,'dr-disabled'))]"))
				.size() > 0)) {
			driver.findElement(By.xpath("//i[contains(@class,'icon-right')][not(contains(@class,'dr-disabled'))]"))
					.click();
			browserHelper.waitTime(3000);
			days = driver
					.findElements(By.xpath("//li[contains(@class,'dr-selected')][not(contains(@class,'dr-fade'))]"))
					.size();
			System.out.println("Number of Audit days in this month " + days);
			auditDays = auditDays + days;
		}
		System.out.println("Number of Audit days " + auditDays);
		return auditDays;
	}

	// Get All hotel index where Audit can be scheduled
	public int[] getAllStyleOrderForAvailableAudits() {
		WebDriver driver = DriverManager.getInstance().getDriver();
		List<WebElement> ele = driver.findElements(
				By.xpath("//div[@class='results']//div[contains(@class,'results__row')][@soldout='scheduleaudit']"));
		int[] hotelIndex = new int[ele.size()];
		int i = 0;
		for (WebElement el : ele) {
			hotelIndex[i] = Integer.parseInt(el.getAttribute("style").replaceAll("[^0-9]", ""));
			i = i + 1;
		}
		Arrays.sort(hotelIndex);
		return hotelIndex;
	}

	// Get random hotel to schedule
	public int getHotelIndexByAvailableForAudit() {
		WebDriver driver = DriverManager.getInstance().getDriver();
		int[] indexList = getAllStyleOrderForAvailableAudits();
		CommonUtils utils = new CommonUtils();
		int indexToReturn = utils.getRandomNumber(0, indexList.length - 1);
		return indexToReturn;
	}

	// Get By Index hotel name
	public String getHotelNameByIndex(int hotelIndex) {
		WebDriver driver = DriverManager.getInstance().getDriver();
		int[] indexList = getAllStyleOrderForAvailableAudits();
		int hotelDisplayIndex = indexList[hotelIndex];
		String xpathHotelName = String.format(
				"//div[@class='results']//div[contains(@class,'results__row')][@style='order: %d;' or @style='order:%d']//div[@class='hotel__name']",
				hotelDisplayIndex, hotelDisplayIndex);
		return driver.findElement(By.xpath(xpathHotelName)).getText();
	}

	// Click on Schedule Audit for a hotel by Name
	public void scheduleHotelAudit(String hotelName) {
		WebDriver driver = DriverManager.getInstance().getDriver();
		String locatorSchAudit = String.format(".results__row[hotel='%s'] .scheduleAudit", hotelName);
		driver.findElement(By.cssSelector(locatorSchAudit)).click();
	}

	public boolean isConfirmAuditDisplayed() {
		WebDriver driver = DriverManager.getInstance().getDriver();
		return (driver.findElements(By.cssSelector("#confirmAudit:not(.hide)")).size() > 0);
	}

	public void confirmAudit() {
		WebDriver driver = DriverManager.getInstance().getDriver();
		driver.findElement(By.cssSelector(".auditPopup .check-term")).click();
		driver.findElement(By.cssSelector("#auditConfirm")).click();
	}

	// Message displayed for successful Audit schedule
	public boolean isScheduleAuditSuccessMessageDisplayed() {
		WebDriver driver = DriverManager.getInstance().getDriver();
		WebDriverWait wait = new WebDriverWait(driver, 120);
		wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector("#auditSuccess:not(.hide)")));
		return (driver.findElements(By.cssSelector("#auditSuccess:not(.hide)")).size() > 0);
	}

	// Book Another Audit is displayed
	public boolean isBookAnotherAuditIsDisplayed() {
		WebDriver driver = DriverManager.getInstance().getDriver();
		return (driver.findElements(By.cssSelector("#auditSuccess:not(.hide) #bookAnotherAudit")).size() > 0);
	}

	public void clickOnBookAnotherAudit() {
		WebDriver driver = DriverManager.getInstance().getDriver();
		driver.findElement(By.cssSelector("#auditSuccess:not(.hide) #bookAnotherAudit")).click();
	}

	// After second audit is scheduled, success message have the message
	public boolean doesSecondAuditSuccessMsgDisplayedWithLimitMentioned() {
		WebDriver driver = DriverManager.getInstance().getDriver();
		browserHelper = new BrowserHelper();
		CommonUtils utils = new CommonUtils();
		WebDriverWait wait = new WebDriverWait(driver, 120);
		wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector("#auditSuccess:not(.hide)")));
		return (driver.findElement(By.xpath("//div[@id='auditSuccess'][not(contains(@class,'hide'))]//div[@class='fot-success-fail__desc'][2]/p")).getText()
				.equals(utils.getProperty("FOTTwoAuditLimitMsg")));
	}

	public boolean doesSecondAuditSucessPopUpDisplaysExpectedLinks() {
		WebDriver driver = DriverManager.getInstance().getDriver();
		return (driver.findElements(By.cssSelector("#auditSuccess:not(.hide) #bookTreebo")).size() > 0
				&& driver.findElements(By.cssSelector("#auditSuccess:not(.hide) #backSearch")).size() > 0);
	}

	public boolean doesClickBookATreeboOpensTreeboLandingPage() {
		WebDriver driver = DriverManager.getInstance().getDriver();
		browserHelper = new BrowserHelper();
		homePage = new HomePage();
		driver.findElement(By.cssSelector("#auditSuccess:not(.hide) #bookTreebo")).click();
		browserHelper.waitTime(3000);
		String parentWindow = browserHelper.switchToNewWindow();
		// Home Page is Displayed
		homePage.verifyHomePagePresence();
		driver.close();
		driver.switchTo().window(parentWindow);
		return true;
	}

	// Back to search should open default search
	public void clickBackToSearch() {
		WebDriver driver = DriverManager.getInstance().getDriver();
		driver.findElement(By.cssSelector("#auditSuccess:not(.hide) #backSearch")).click();
	}

	// After second audit does the limit message display
	public boolean doesLimitMessageDisplayedAfterTwoAuditScheduled() {
		WebDriver driver = DriverManager.getInstance().getDriver();
		CommonUtils utils = new CommonUtils();
		return driver.findElement(By.cssSelector(".result-page__audit-error")).getText().trim()
				.equals(utils.getProperty("FOTErrorTwoAuditsBooked"));
	}

	// For Scheduled Audit it should display Cancel Audit, check with Hotel Name
	public boolean doesCancelAuditForAlreadyScheduleAuditDisplayed(String hotelName) {
		WebDriver driver = DriverManager.getInstance().getDriver();
		String locatorCancelAudit = String.format(".results__row[hotel='%s'] .cancelAudit", hotelName);
		return (driver.findElements(By.cssSelector(locatorCancelAudit)).size() == 1);
	}

	public void clickCancelAuditForAlreadyScheduleAudit(String hotelName) {
		WebDriver driver = DriverManager.getInstance().getDriver();
		String locatorCancelAudit = String.format(".results__row[hotel='%s'] .cancelAudit", hotelName);
		driver.findElement(By.cssSelector(locatorCancelAudit)).click();
		driver.findElement(By.cssSelector("#cancelYesBtn")).click();
	}

	public boolean doesCancelAuditSuccessMessageDisplayed() {
		WebDriver driver = DriverManager.getInstance().getDriver();
		WebDriverWait wait = new WebDriverWait(driver, 30);
		wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector("#auditCancelled:not(.hide) #cancelledYesBtn")));
		return ((driver.findElements(By.cssSelector("#auditCancelled:not(.hide)")).size() > 0)
				&& (driver.findElements(By.cssSelector("#auditCancelled:not(.hide) #cancelledYesBtn")).size() > 0)
				&& (driver.findElements(By.cssSelector("#auditCancelled:not(.hide) #cancelledNoBtn")).size() > 0));
	}

	public void clickYesForBookAnotherAuditInCanceledAuditPopUp() {
		WebDriver driver = DriverManager.getInstance().getDriver();
		WebDriverWait wait = new WebDriverWait(driver, 30);
		wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector("#auditCancelled:not(.hide) #cancelledYesBtn")));
		driver.findElement(By.cssSelector("#auditCancelled:not(.hide) #cancelledYesBtn")).click();
	}
	
	public boolean doesErrorMessageForSameDayAuditDisplayed() {
		WebDriver driver = DriverManager.getInstance().getDriver();
		WebDriverWait wait = new WebDriverWait(driver, 30);
		wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(".result-page__audit-error")));
		CommonUtils utils = new CommonUtils();
		return driver.findElement(By.cssSelector(".result-page__audit-error")).getText()
				.equals(utils.getProperty("FOTErrorForSameDate"));
	}

	// Close Audit displayed for all but 1 in same city
	// Same day cannot schedule another audit
	public boolean doesCloseAllAuditDisplayForAllHotelsOtherThanOne() {
		WebDriver driver = DriverManager.getInstance().getDriver();
		return ((driver.findElements(By.cssSelector(".closeAudit")).size()
				+ driver.findElements(By.cssSelector(".soldOut")).size() + 1) == driver
						.findElements(By.cssSelector(".results__row")).size());
	}

	// Close Audit displayed for all in another city
	// Same day cannot schedule another audit
	public boolean doesCloseAllAuditDisplayForAllHotelsInDifferentCity() {
		WebDriver driver = DriverManager.getInstance().getDriver();
		return (driver.findElements(By.cssSelector(".closeAudit")).size()
				+ +driver.findElements(By.cssSelector(".soldOut")).size() == driver
						.findElements(By.cssSelector(".results__row")).size());
	}

	public boolean doesHereLinkTakesToAccount() {
		WebDriver driver = DriverManager.getInstance().getDriver();
		browserHelper = new BrowserHelper();
		driver.findElement(By.cssSelector(".result-page__audit-error a")).click();
		browserHelper.waitTime(3000);
		String parentWindow = browserHelper.switchToNewWindow();
		boolean flag = (driver.findElements(By.cssSelector(".account")).size() > 0);
		Assert.assertTrue(doesFOTAuditDisplayedInMyAccount());
		driver.close();
		driver.switchTo().window(parentWindow);
		browserHelper.waitTime(2000);
		return flag;
	}

	public boolean doesFOTAuditDisplayedInMyAccount() {
		WebDriver driver = DriverManager.getInstance().getDriver();
		WebDriverWait wait = new WebDriverWait(driver, 30);
		wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//div[text()='FOT Audit']")));
		return (driver
				.findElements(By
						.xpath("//div[@id='confirm']/span/../../..//div[contains(@class,'pages__audit-status')][text()='FOT Audit']"))
				.size() > 0);
	}

	public void goBackToFOTSearchPageFromMyAccount() {
		browserHelper = new BrowserHelper();
		browserHelper.browserGoBack();
	}

	public void closeConfirmAudit() {
		WebDriver driver = DriverManager.getInstance().getDriver();
		driver.findElement(By.cssSelector("#confirmAudit:not(.hide) .icon-cross")).click();
	}

	public boolean isConfirmAuditPopUpClosed() {
		WebDriver driver = DriverManager.getInstance().getDriver();
		return (driver.findElements(By.cssSelector("#confirmAudit:not(.hide)")).size() > 0);
	}

	public boolean doesBookATreeboDisplaysForFailedFOT() {
		WebDriver driver = DriverManager.getInstance().getDriver();
		WebDriverWait wait = new WebDriverWait(driver, 30);
		wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//div[text()='BOOK A TREEBO']")));
		return (driver.findElements(By.xpath("//div[text()='BOOK A TREEBO']")).size() > 0);
	}

	public boolean doesClickBookATreeboForFailedFOTOpensLandingPage() {
		homePage = new HomePage();
		homePage.verifyHomePagePresence();
		return true;
	}
}
