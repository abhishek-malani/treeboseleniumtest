package newDesktopUIPageObjects;

import java.util.ArrayList;
import java.util.Set;

import org.apache.commons.lang3.RandomStringUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import base.BrowserHelper;
import base.DriverManager;

public class Footer {
	private BrowserHelper browserHelper;

	private By facebookLink = By
			.xpath("//div[@class='page__footer']//a[@href='//www.facebook.com/TreeboHotels']/span/i");
	private By twitterLink = By.xpath("//div[@class='page__footer']//a[@href='//twitter.com/TreeboHotels']/span/i");
	private By linkedInLink = By.cssSelector(".icon-linkedin");
	private By googlePlusLink = By
			.xpath("//div[@class='page__footer']//a[@href='//www.google.com/+Treebohotels1']/span/i");

	private By aboutLink = By.xpath("//ul[contains(@class,'footer__column')]//a[text()='About']");
	private By contactLink = By.xpath("//ul[contains(@class,'footer__column')]//a[text()='Contact']");
	private By faqLink = By.xpath("//ul[contains(@class,'footer__column')]//a[text()='FAQs']");
	private By termsOfServiceLink = By.xpath("//ul[contains(@class,'footer__column')]//a[text()='Terms of Service']");
	private By privacyPolicyLink = By.xpath("//ul[contains(@class,'footer__column')]//a[text()='Privacy Policy']");
	private By feedbackLink = By.xpath("//ul[contains(@class,'footer__column')]//a[text()='Feedback']");
	private By blogLink = By.xpath("//ul[contains(@class,'footer__column')]//a[text()='Blog']");

	private By joinOurNetworkLink = By.xpath("//ul[contains(@class,'footer__column')]//a[text()='Join Our Network']");
	private By corporateEnquiryLink = By
			.xpath("//ul[contains(@class,'footer__column')]//a[text()='Corporate Enquiry']");
	private By travelAgentsLink = By.xpath("//ul[contains(@class,'footer__column')]//a[text()='Travel Agents']");

	private By alphaLink = By.xpath("//ul[contains(@class,'footer__column')]//a[text()='Alpha']");
	private By fotLink = By.xpath("//ul[contains(@class,'footer__column')]//a[text()='Friends of Treebo']");
	private By careerLink = By.xpath("//ul[contains(@class,'footer__column')]//a[text()='Careers']");

	private By feedbackLinkInRightCorner = By.cssSelector(".footer__navitem.feedback a");
	private By feedbackCancelLink = By.xpath("//button[@id='feedbackCancel']");
	private By feedbackCrossIcon = By.xpath("//div[@id='feedback']/div/div/i");
	private By nameInFeedbackForm = By.xpath(
			"//div[@id='feedback'][not(contains(@class,'hide'))]//form[@id='feedbackForm']//input[@id='feedbackName']");
	private By emailInFeedbackForm = By.xpath(
			"//div[@id='feedback'][not(contains(@class,'hide'))]//form[@id='feedbackForm']//input[@id='feedbackEmail']");
	private By messageInFeedbackForm = By.xpath(
			"//div[@id='feedback'][not(contains(@class,'hide'))]//form[@id='feedbackForm']//textarea[@id='feedbackcomments']");
	private By submitButtonInFeedbackForm = By.xpath(
			"//div[@id='feedback'][not(contains(@class,'hide'))]//form[@id='feedbackForm']//button[@id='feedbackSubmit']");
	private By feedbackSuccessMessage = By
			.xpath("//div[contains(@class,'feedbackMessage successMessage')][not(contains(@class,'hide'))]");

	private By fotSignUpNow = By.xpath("//a[contains(@class,'top-button')][text()='Sign-Up Now']");
	private By fotRegName = By.cssSelector("#fotRegister #name");
	private By fotRegEmail = By.cssSelector("#fotRegister #email");
	private By fotRegPassword = By.cssSelector("#fotRegister #password");
	private By fotRegMobile = By.cssSelector("#fotRegister #mobile");
	private By fotRegCity = By.cssSelector("#fotRegister #city");
	private By fotRegAge = By.cssSelector("#fotRegister #age");
	private By fotRegCurrAddr = By.xpath("//label[contains(text(),'Current Address')]/following-sibling::input");

	public void verifyFOTLinkAndFeature() {
		WebDriver driver = DriverManager.getInstance().getDriver();
		browserHelper = new BrowserHelper();
		String name = "Test";
		String uniqueEmail = "test" + RandomStringUtils.randomAlphanumeric(15);
		String email = uniqueEmail + "@gmail.com";
		String mobile = "6000000000";
		String password = "password";
		String city = "Bangalore";
		String age = "25";

		driver.findElement(fotLink).click();
		String parentWindow = browserHelper.switchToNewWindow();
		Assert.assertTrue(driver.findElement(By.xpath("//div[@class='fot-info']")).isDisplayed(),
				"FOT page info not displayed");

		Assert.assertTrue(driver.findElement(fotSignUpNow).isDisplayed(),
				"Sign Up Now link in footer page not displayed");
		driver.findElement(fotSignUpNow).click();

		driver.findElement(fotRegName).sendKeys(name);
		driver.findElement(fotRegEmail).sendKeys(email);
		driver.findElement(fotRegMobile).sendKeys(mobile);
		driver.findElement(fotRegPassword).sendKeys(password);
		driver.findElement(fotRegCurrAddr).sendKeys(city);
		driver.findElement(fotRegCity).sendKeys(city);
		driver.findElement(fotRegAge).sendKeys(age);

		driver.findElement(By.xpath("//input[@value='Married']")).click();
		driver.findElement(By.xpath("//input[@value='Smartphone']")).click();
		driver.findElement(By
				.xpath("//label[contains(text(),'Which of the following best describes your occupation?')]/following-sibling::select"))
				.click();
		driver.findElement(By.xpath("//option[@value='Education']")).click();
		driver.findElement(By.xpath("//input[@value='Report to the hotel manager']")).click();
		driver.findElement(By.xpath("//input[contains(@value,'Friendship is most important')]")).click();
		driver.findElement(By.xpath("//input[@value='4']")).click();
		driver.findElement(By.xpath("//input[@value='5-10 days']")).click();
		driver.findElement(By.xpath("//input[contains(@value,'deep in the water')]")).click();
		driver.findElement(By.xpath("//input[contains(@value,'Once a month')]")).click();
		driver.findElement(By.cssSelector("#travelExp")).sendKeys("Test");
		driver.findElement(By.cssSelector("#referer")).sendKeys("Test");
		driver.findElement(By.cssSelector("input[name=tnc]")).click();
		driver.findElement(By.cssSelector("#submitform")).click();

	}

	public void verifyFeedbackForm() {
		WebDriver driver = DriverManager.getInstance().getDriver();
		driver.findElement(feedbackLinkInRightCorner).click();
		Assert.assertTrue(driver.findElement(nameInFeedbackForm).isDisplayed(), "feedback form is not displayed");
		driver.findElement(feedbackCancelLink).click();
		Assert.assertEquals(driver.findElements(nameInFeedbackForm).size(), 0,
				"feedback form is displayed after cancel also");

		driver.findElement(feedbackLinkInRightCorner).click();
		Assert.assertTrue(driver.findElement(nameInFeedbackForm).isDisplayed(), "feedback form is not displayed");
		driver.findElement(feedbackCrossIcon).click();
		Assert.assertEquals(driver.findElements(nameInFeedbackForm).size(), 0,
				"feedback form is displayed after cancel also");

		driver.findElement(feedbackLinkInRightCorner).click();
		Assert.assertTrue(driver.findElement(nameInFeedbackForm).isDisplayed(), "feedback form is not displayed");
		driver.findElement(nameInFeedbackForm).sendKeys("Test");
		driver.findElement(emailInFeedbackForm).sendKeys("treebotest@gmail.com");
		driver.findElement(messageInFeedbackForm).sendKeys("Test");
		driver.findElement(submitButtonInFeedbackForm).click();

		WebDriverWait wait = new WebDriverWait(driver, 120);
		wait.until(ExpectedConditions.presenceOfElementLocated(feedbackSuccessMessage));
	}

	
	
	public void verifyJoinUsLinks() throws InterruptedException {
		WebDriver driver = DriverManager.getInstance().getDriver();
		browserHelper = new BrowserHelper();
		Assert.assertTrue(driver.findElement(facebookLink).isDisplayed());
		Assert.assertTrue(driver.findElement(twitterLink).isDisplayed());
		Assert.assertTrue(driver.findElement(linkedInLink).isDisplayed());
		Assert.assertTrue(driver.findElement(googlePlusLink).isDisplayed());

		// Navigation
		driver.findElement(facebookLink).click();
		Thread.sleep(30000);
		String treeboWindow = browserHelper.switchToNewWindow();
		String fbWindow = browserHelper.getCurrentWindowHandle();
		Assert.assertEquals(browserHelper.getCurrentUrl(), "https://www.facebook.com/TreeboHotels",
				"url does not match exactly");
		// driver.close();
		driver.switchTo().window(treeboWindow);

		// Navigation
		driver.findElement(twitterLink).click();
		Thread.sleep(30000);
		Set<String> allHandles = browserHelper.getAllWindowHandles();
		allHandles.remove(treeboWindow);
		allHandles.remove(fbWindow);
		ArrayList<String> handle1 = new ArrayList<String>(allHandles);
		String twitterWindow = handle1.get(0);
		browserHelper.switchToWindow(twitterWindow);
		Assert.assertEquals(browserHelper.getCurrentUrl(), "https://twitter.com/TreeboHotels",
				"url does not match exactly");
		// driver.close();
		driver.switchTo().window(treeboWindow);

		// Navigation
		driver.findElement(googlePlusLink).click();
		Thread.sleep(30000);
		Set<String> allHandles1 = browserHelper.getAllWindowHandles();
		allHandles1.remove(treeboWindow);
		allHandles1.remove(fbWindow);
		allHandles1.remove(twitterWindow);
		ArrayList<String> handle2 = new ArrayList<String>(allHandles1);
		String gplusWindow = handle2.get(0);
		browserHelper.switchToWindow(gplusWindow);
		Assert.assertEquals(browserHelper.getCurrentUrl(), "https://plus.google.com/+Treebohotels1",
				"url does not match exactly");
		// driver.close();
		driver.switchTo().window(treeboWindow);

		// Navigation
		// driver.findElement(linkedInLink).click();
		// Thread.sleep(30000);
		// String parentWindow2 = browserHelper.switchToNewWindow();
		// Assert.assertTrue(browserHelper.getCurrentUrl().contains("www.linkedin.com/company/treebo-hotels"),
		// "url does not match :" + browserHelper.getCurrentUrl());
		// driver.close();
		// driver.switchTo().window(parentWindow2);
	}

	public void verifyLinksInCompanySection() {
		WebDriver driver = DriverManager.getInstance().getDriver();
		browserHelper = new BrowserHelper();
		driver.findElement(aboutLink).click();
		Assert.assertEquals(driver.findElement(By.xpath("//div[@class='container aboutus']//header")).getText().trim(),
				("OUR STORY"));
		driver.findElement(contactLink).click();
		Assert.assertEquals(driver.findElement(By.xpath("//div[contains(@class,'contactus__query')]/h2")).getText(),
				("WE'D LOVE TO HEAR FROM YOU"));
		driver.findElement(faqLink).click();
		Assert.assertEquals(driver.findElement(By.xpath("//p[@class='faqPage__headline']")).getText(),
				("FREQUENTLY ASKED QUESTIONS"));
		driver.findElement(termsOfServiceLink).click();
		Assert.assertEquals(
				driver.findElement(By.xpath("//div[contains(@class,'termsPage__heading')]/p")).getText().trim().toLowerCase(),
				("Terms of use for use of the Website and the Mobile application").toLowerCase());
		driver.findElement(privacyPolicyLink).click();
		Assert.assertEquals(driver.findElement(By.xpath("//div[contains(@class,'policyPage__heading')]/p")).getText().toLowerCase(),
				("Privacy Policy").toLowerCase());
		driver.findElement(feedbackLink).click();
		Assert.assertTrue(driver
				.findElement(By
						.xpath("//div[@id='feedback'][not(contains(@class,'hide'))]//form[@id='feedbackForm']//input[@id='feedbackName']"))
				.isDisplayed(), "feedback pop up not displayed");
		driver.findElement(By.xpath("//button[@id='feedbackCancel']")).click();
		driver.findElement(blogLink).click();
		browserHelper.switchToNewWindow();
		Assert.assertTrue(driver.findElement(By.cssSelector(".home.blog")).isDisplayed(), "Treebo blog not displayed");
	}

	public void verifyLinksInBusinessAndDiscoverSection() {
		WebDriver driver = DriverManager.getInstance().getDriver();
		browserHelper = new BrowserHelper();
		driver.findElement(joinOurNetworkLink).click();
		Assert.assertEquals(driver
				.findElement(By.xpath("//div[@id='joinournetwork']/div/p[contains(@class,'joinus__network-title')]"))
				.getText(), ("JOIN OUR NETWORK"));

		driver.findElement(alphaLink).click();
		browserHelper.waitTime(3000);
		String treeboWindow = browserHelper.switchToNewWindow();
		String alphaWindow = browserHelper.getCurrentWindowHandle();
		Assert.assertTrue(
				driver.findElement(By.xpath("//h2[text()='All this and more at a price much lower than you think!']"))
						.isDisplayed(),
				"Alpha page not displayed");
		// driver.close();
		driver.switchTo().window(treeboWindow);

		driver.findElement(fotLink).click();
		browserHelper.waitTime(3000);
		Set<String> allHandles = browserHelper.getAllWindowHandles();
		allHandles.remove(alphaWindow);
		allHandles.remove(treeboWindow);
		ArrayList<String> handle1 = new ArrayList<String>(allHandles);
		String fotWindow = handle1.get(0);
		browserHelper.switchToWindow(fotWindow);
		Assert.assertTrue(driver.findElement(By.xpath("//div[@class='fotpage']")).isDisplayed(),
				"FOT page info not displayed");
		// driver.close();
		driver.switchTo().window(treeboWindow);

		// corporate enquiry, travel agents and careers open up email client
		Assert.assertEquals(driver.findElement(corporateEnquiryLink).getAttribute("href"),
				("mailto:corporate@treebohotels.com"));
		Assert.assertEquals(driver.findElement(travelAgentsLink).getAttribute("href"), ("mailto:ta@treebohotels.com"));
		String careerHref = driver.findElement(careerLink).getAttribute("href");
		Assert.assertTrue(careerHref.contains("mailto:careers@treebohotels.com"), "Incorrect value : " + careerHref);
	}
	public void verifyFeedbackFormWithPopulateClearPlaceHolder() {
		WebDriver driver = DriverManager.getInstance().getDriver();
		browserHelper = new BrowserHelper();
		driver.findElement(feedbackLinkInRightCorner).click();
		driver.findElement(By.xpath(".//*[@id='feedbackName']")).clear();
		driver.findElement(By.xpath(".//*[@id='feedbackEmail']")).clear();
	}

	public String extractUserName() {
		WebDriver driver = DriverManager.getInstance().getDriver();
		WebElement element = driver.findElement(By.xpath("//button[@class='user__name dropbtn']"));
		String name = element.getText().trim();
		return name;
	}

	public void verifyPlaceHolderFeedbackForm(String email, String name) {
		WebDriver driver = DriverManager.getInstance().getDriver();
		driver.findElement(By.xpath(".//*[@id='feedbackName']")).sendKeys(name);
		driver.findElement(By.xpath(".//*[@id='feedbackEmail']")).sendKeys(email);
	}
	public boolean doesFeedbackPopUpOpened(){
		WebDriver driver = DriverManager.getInstance().getDriver();
		return driver.findElement(By.xpath("//div[@class='modal__body feedback-modal__body pos-rel']")).isDisplayed();
	}
	public boolean doesEmailFieldShowEmaiIdPlaceHolder(){
		WebDriver driver = DriverManager.getInstance().getDriver();
		String str1 = "Email id";
		String str2 = driver.findElement(By.xpath("//*[@id='feedbackEmail']")).getAttribute("placeholder").trim();
		System.out.println("*******");
		System.out.println(str2);
		
		System.out.println("*******");
		System.out.println(str1);
		
		System.out.println("*******");
		if((str1.trim()).equals(str2))
			return true;
		return false;
	}
	public boolean doesNameFieldShowNamePlaceHolder(){
		WebDriver driver = DriverManager.getInstance().getDriver();
		String str1 = driver.findElement(By.xpath("//*[@id='feedbackName']")).getAttribute("placeholder").trim();
		String str2 = "Enter Name";
		System.out.println("*******");
		System.out.println(str1);
		
		System.out.println("*******");
		System.out.println(str2);
		
		System.out.println("*******");
		if(str1.equals(str2.trim()))
			return true;
		return false;
	}
	
	public void clickOnFeedback(){
		WebDriver driver = DriverManager.getInstance().getDriver();
		driver.findElement(By.xpath("//a[text()='Feedback']")).click();
	}
	public void clickCloseFeedback(){
		WebDriver driver = DriverManager.getInstance().getDriver();
		WebDriverWait wait = new WebDriverWait(driver,120);
		wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.xpath(".//*[@id='feedbackCancel']")));
	    driver.findElement(By.xpath(".//*[@id='feedbackCancel']")).click();
	}
	public boolean doesEmailIdPopulate(String email){
		WebDriver driver = DriverManager.getInstance().getDriver();
		WebElement element = driver.findElement(By.xpath("//input[@name='email']"));
		String str1 = element.getAttribute("value").trim();
		System.out.println("*******");
		System.out.println(email);
		
		System.out.println("*******");
		System.out.println(str1);
		
		System.out.println("*******");
		
		if(str1.equals(email.trim()))
			return true;
		return false;
	}
	public boolean doesNamePopulate(String name){
		WebDriver driver = DriverManager.getInstance().getDriver();
		String str1 = driver.findElement(By.xpath("//input[@placeholder='Enter Name']")).getAttribute("value").trim();
		System.out.println("*******");
		System.out.println(name);
		
		System.out.println("*******");
		System.out.println(str1);
		
		System.out.println("*******");
		if(str1.equals(name))
			return true;
		return false;
   }
	

}
