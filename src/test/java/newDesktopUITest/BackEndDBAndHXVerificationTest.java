package newDesktopUITest;

import java.io.IOException;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.Map;

import javax.xml.parsers.ParserConfigurationException;

import org.apache.commons.lang3.RandomStringUtils;
import org.apache.http.client.ClientProtocolException;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;
import org.xml.sax.SAXException;

import utils.CommonUtils;
import utils.DBUtils;
import base.TestBaseSetUp;
import desktopUIPageObjects.HomePage;
import desktopUIPageObjects.BookingHistory;
import desktopUIPageObjects.HotelResultsPage;
import desktopUIPageObjects.ItineraryPage;
import desktopUIPageObjects.ConfirmationPage;
import desktopUIPageObjects.HotelDetailsPage;
import base.BrowserHelper;
import base.HotelLogixAPI;

@Test(singleThreaded=true)
public class BackEndDBAndHXVerificationTest extends TestBaseSetUp {

	private HomePage homePage;
	private HotelResultsPage hotelResultsPage;
	private ItineraryPage itineraryPage;
	private ConfirmationPage confirmationPage;
	private HotelDetailsPage hotelDetailsPage;
	private BookingHistory bookingHistory;
	private BrowserHelper browserHelper;

	/**
	 * This Test verifies that for 1 Adult booking with Coupon, informations
	 * stored in DB and HX is appropriate
	 * 
	 * @throws ParseException
	 * @throws SQLException
	 * @throws SAXException
	 * @throws ParserConfigurationException
	 * @throws IOException
	 * @throws ClientProtocolException
	 */
	@Test(groups = { "BackEndTest","HotelBooking" })
	public void testDBHXOneAdultBookingPayAtHotel() throws ParseException, SQLException, ClientProtocolException,
			IOException, ParserConfigurationException, SAXException {
		homePage = new HomePage();
		hotelResultsPage = new HotelResultsPage();
		itineraryPage = new ItineraryPage();
		confirmationPage = new ConfirmationPage();
		browserHelper = new BrowserHelper();
		bookingHistory = new BookingHistory();
		CommonUtils utils = new CommonUtils();
		SoftAssert verify = new SoftAssert();
		DBUtils db = new DBUtils();
		HotelLogixAPI api = new HotelLogixAPI();

		String location = utils.getProperty("Country"); // "India"; // "India";
		String testHotel = utils.getProperty("DummyHotel"); // "Dummy";
		String testUrl = utils.getProperty("TestUrl");
		String strSpecialRequest = utils.getProperty("SpecialRequest");
		int hotelIndex = 0;
		int checkInFromCurrentDate = utils.getRandomNumber(30,45);
		int checkOutFromCurrentDate = checkInFromCurrentDate + 1;
		int roomIndex = 1;
		String adultCount = "1";
		String childCOunt = "0";
		String guestName = utils.getProperty("GuestName");
		String guestEmail = utils.getProperty("TestBookingAccount");
		String guestMobile = utils.getProperty("GuestMobile");
		String[] checkInCheckOutDates = new String[2];

		// Home Page is Displayed
		homePage.verifyHomePagePresence();
		// Do search and get checkin checkout dates
		checkInCheckOutDates = homePage.doSearch(location, checkInFromCurrentDate, checkOutFromCurrentDate, roomIndex);
		hotelResultsPage.verifyHotelResultsPagePresence();

		if (browserHelper.getCurrentUrl().contains(testUrl)) {
			hotelIndex = hotelResultsPage.getHotelIndexByHotelName(testHotel);
		} else {
			hotelIndex = hotelResultsPage.getHotelIndexByHotelAvailable();
		}

		String hotelName = hotelResultsPage.getHotelNameByIndex(hotelIndex);
		String hotelRoomRate = hotelResultsPage.getHotelRoomRateByIndex(hotelIndex);
		String hotelAddress = hotelResultsPage.getHotelAddressByIndex(hotelIndex);
		String hotelId = hotelResultsPage.getHotelIdByHotelName(hotelName);
		System.out.println("Hotel Name: " + hotelName + " ,Hotel id : " + hotelId + " ,Hotel room rate : "
				+ hotelRoomRate + " ,Hotel Address:" + hotelAddress);

		hotelResultsPage.clickQuickBookByIndex(hotelIndex);
		itineraryPage.verifyItineraryPagePresence();
		itineraryPage.clickContinueAsGuest();
		itineraryPage.enterSpecialRequest(strSpecialRequest);

		double[] hotelPriceInfoItineraryPage = itineraryPage.getHotelPriceInfoInItineraryPage();

		itineraryPage.bookHotelAsGuestWithPayAtHotelWithSpecialRequest(guestName, guestMobile, guestEmail,strSpecialRequest);
		confirmationPage.verifyConfirmationPagePresence();
		String bookingId = confirmationPage.getBookingId();
		double[] hotelPriceInfoConfirmationPage = confirmationPage.getHotelPriceInfoInConfirmationPage();

		verify.assertTrue(
				Math.abs((int) Math.round(hotelPriceInfoItineraryPage[0])
						- (int) Math.round(hotelPriceInfoConfirmationPage[0])) <= 1,
				"Room rate Itinerary page :" + (int) Math.round(hotelPriceInfoItineraryPage[0])
						+ " and Confirmation page: " + (int) Math.round(hotelPriceInfoConfirmationPage[0])
						+ " is not same");
		verify.assertTrue(
				Math.abs((int) Math.round(hotelPriceInfoItineraryPage[1])
						- (int) Math.round(hotelPriceInfoConfirmationPage[1])) <= 1,
				"Taxes in Itinerary page : " + (int) Math.round(hotelPriceInfoItineraryPage[1])
						+ " and Confirmation page :" + (int) Math.round(hotelPriceInfoConfirmationPage[1])
						+ " is not same");
		verify.assertTrue(
				Math.abs((int) Math.round(hotelPriceInfoItineraryPage[2])
						- (int) Math.round(hotelPriceInfoConfirmationPage[2])) <= 1,
				"Discount in Itinerary page :" + (int) Math.round(hotelPriceInfoItineraryPage[2])
						+ " and Confirmation page " + (int) Math.round(hotelPriceInfoConfirmationPage[2])
						+ " is not same");
		verify.assertTrue(
				Math.abs((int) Math.round(hotelPriceInfoItineraryPage[3])
						- (int) Math.round(hotelPriceInfoConfirmationPage[3])) <= 101,
				"Total in Itinerary page :" + (int) Math.round(hotelPriceInfoItineraryPage[3])
						+ " and Confirmation page " + (int) Math.round(hotelPriceInfoConfirmationPage[3])
						+ " is not same");

		verify.assertEquals(guestName, confirmationPage.getGuestNameInConfPage());
		verify.assertEquals(guestEmail, confirmationPage.getGuestEmailInConfPage());
		verify.assertEquals(guestMobile, confirmationPage.getGuestMobileInConfPage());
		verify.assertEquals(hotelName, confirmationPage.getHotelNameInConfirmationPage());

		System.out.println(utils.formatDate(checkInCheckOutDates[0]).toUpperCase());
		System.out.println(confirmationPage.getHotelCheckInDateInConfirmationPage().toUpperCase());
		verify.assertEquals(utils.formatDate(checkInCheckOutDates[0]).toUpperCase(),
				confirmationPage.getHotelCheckInDateInConfirmationPage().toUpperCase());
		verify.assertEquals(utils.formatDate(checkInCheckOutDates[1]).toUpperCase(),
				confirmationPage.getHotelCheckOutDateInConfirmationPage().toUpperCase());

		// Verify data in DB
		Map<String, String> dbBookingInfo = db.getBookingInfo(bookingId, hotelId);
		String orderId = dbBookingInfo.get("order_id");
		verify.assertEquals(dbBookingInfo.get("checkin_date"), utils.formatDateOne(checkInCheckOutDates[0]),
				"Checkin date is wrong in DB" + " for hotel :" + hotelName + " checkin : " + checkInCheckOutDates[0]
						+ "checkout : " + checkInCheckOutDates[1]);
		verify.assertEquals(dbBookingInfo.get("checkout_date"), utils.formatDateOne(checkInCheckOutDates[1]),
				"Checkout date is wrong in DB" + " for hotel :" + hotelName + " checkin : " + checkInCheckOutDates[0]
						+ "checkout : " + checkInCheckOutDates[1]);
		verify.assertEquals(dbBookingInfo.get("adult_count"), adultCount, "adult Count is wrong in DB" + " for hotel :"
				+ hotelName + " checkin : " + checkInCheckOutDates[0] + "checkout : " + checkInCheckOutDates[1]);
		verify.assertEquals(dbBookingInfo.get("child_count"), childCOunt, "Checkin date is wrong in DB" + " for hotel :"
				+ hotelName + " checkin : " + checkInCheckOutDates[0] + "checkout : " + checkInCheckOutDates[1]);
		verify.assertEquals((int) Math.round(Double.parseDouble(dbBookingInfo.get("total_amount"))),
				(int) Math.round(hotelPriceInfoConfirmationPage[3]),
				"Total amount is wrong in DB" + " for hotel :" + hotelName + " checkin : " + checkInCheckOutDates[0]
						+ "checkout : " + checkInCheckOutDates[1]);
		verify.assertEquals(dbBookingInfo.get("guest_name"), guestName, "guest Name is wrong in DB" + " for hotel :"
				+ hotelName + " checkin : " + checkInCheckOutDates[0] + "checkout : " + checkInCheckOutDates[1]);
		verify.assertEquals(dbBookingInfo.get("guest_email"), guestEmail, "guest Email is wrong in DB" + " for hotel :"
				+ hotelName + " checkin : " + checkInCheckOutDates[0] + "checkout : " + checkInCheckOutDates[1]);
		verify.assertEquals(dbBookingInfo.get("guest_mobile"), guestMobile,
				"guest Mobile is wrong in DB" + " for hotel :" + hotelName + " checkin : " + checkInCheckOutDates[0]
						+ "checkout : " + checkInCheckOutDates[1]);
		verify.assertEquals((int) Math.round(Double.parseDouble(dbBookingInfo.get("payment_amount"))), 0,
				"paymnet amount is wrong in DB" + " for hotel :" + hotelName + " checkin : " + checkInCheckOutDates[0]
						+ "checkout : " + checkInCheckOutDates[1]);
		verify.assertTrue(dbBookingInfo.get("comments").contains(strSpecialRequest),
				"Special request is wrong in DB" + " for hotel :" + hotelName + " checkin : " + checkInCheckOutDates[0]
						+ "checkout : " + checkInCheckOutDates[1]);
		verify.assertEquals(dbBookingInfo.get("coupon_apply"), "false", "coupon apply is wrong in DB" + " for hotel :"
				+ hotelName + " checkin : " + checkInCheckOutDates[0] + "checkout : " + checkInCheckOutDates[1]);
		verify.assertEquals((int) Math.round(Double.parseDouble(dbBookingInfo.get("discount"))),
				(int) Math.round(hotelPriceInfoConfirmationPage[2]),
				"Checkin date is wrong in DB" + " for hotel :" + hotelName + " checkin : " + checkInCheckOutDates[0]
						+ "checkout : " + checkInCheckOutDates[1]);
		verify.assertEquals(dbBookingInfo.get("payment_mode"), "Not Paid",
				"payment mode is wrong in DB" + " for hotel :" + hotelName + " checkin : " + checkInCheckOutDates[0]
						+ "checkout : " + checkInCheckOutDates[1]);
		verify.assertEquals(dbBookingInfo.get("status"), "1", "booking status is wrong in DB" + " for hotel :"
				+ hotelName + " checkin : " + checkInCheckOutDates[0] + "checkout : " + checkInCheckOutDates[1]);
		verify.assertEquals((int) Math.round(Double.parseDouble(dbBookingInfo.get("pretax_amount"))),
				(int) Math.round(hotelPriceInfoConfirmationPage[0]),
				"Room rate is wrong in DB" + " for hotel :" + hotelName + " checkin : " + checkInCheckOutDates[0]
						+ "checkout : " + checkInCheckOutDates[1]);
		verify.assertEquals((int) Math.round(Double.parseDouble(dbBookingInfo.get("tax_amount"))),
				(int) Math.round(hotelPriceInfoConfirmationPage[1]), "Tax is wrong in DB" + " for hotel :" + hotelName
						+ " checkin : " + checkInCheckOutDates[0] + "checkout : " + checkInCheckOutDates[1]);
		verify.assertEquals(dbBookingInfo.get("channel"), "website", "Channel is wrong in DB" + " for hotel :"
				+ hotelName + " checkin : " + checkInCheckOutDates[0] + "checkout : " + checkInCheckOutDates[1]);

		// verify data in HX get order response
		Map<String, String> bookingInfoInHX = api.getOrderDetailsFromHX(orderId);
		verify.assertEquals(bookingInfoInHX.get("guest_first_name"), guestName,
				"Guest Name is wrong in HX " + " for hotel :" + hotelName + " checkin : " + checkInCheckOutDates[0]
						+ "checkout : " + checkInCheckOutDates[1]);
		verify.assertEquals(bookingInfoInHX.get("guest_email"), guestEmail,
				"Guest Email is wrong in HX " + " for hotel :" + hotelName + " checkin : " + checkInCheckOutDates[0]
						+ "checkout : " + checkInCheckOutDates[1]);
		verify.assertEquals(bookingInfoInHX.get("guest_mobile"), guestMobile,
				"Guest Mobile is wrong in HX" + " for hotel :" + hotelName + " checkin : " + checkInCheckOutDates[0]
						+ "checkout : " + checkInCheckOutDates[1]);
		verify.assertEquals((int) Math.round(Double.parseDouble(bookingInfoInHX.get("total_amount"))),
				(int) Math.round(hotelPriceInfoConfirmationPage[3]),
				"Total amount is wrong in HX" + " for hotel :" + hotelName + " checkin : " + checkInCheckOutDates[0]
						+ "checkout : " + checkInCheckOutDates[1]);
		verify.assertTrue(bookingInfoInHX.get("special_request").contains(strSpecialRequest),
				"Special request is wrong in HX" + " for hotel :" + hotelName + " checkin : " + checkInCheckOutDates[0]
						+ "checkout : " + checkInCheckOutDates[1]);
		verify.assertEquals(bookingInfoInHX.get("checkindate_room1"), utils.formatDateOne(checkInCheckOutDates[0]),
				"checkin date is wrong in HX" + " for hotel :" + hotelName + " checkin : " + checkInCheckOutDates[0]
						+ "checkout : " + checkInCheckOutDates[1]);
		verify.assertEquals(bookingInfoInHX.get("checkoutdate_room1"), utils.formatDateOne(checkInCheckOutDates[1]),
				"checkout date is wrong in HX" + " for hotel :" + hotelName + " checkin : " + checkInCheckOutDates[0]
						+ "checkout : " + checkInCheckOutDates[1]);
		verify.assertEquals(bookingInfoInHX.get("adult_room1"), adultCount,
				"Adult count is wrong in HX" + " for hotel :" + hotelName + " checkin : " + checkInCheckOutDates[0]
						+ "checkout : " + checkInCheckOutDates[1]);
		verify.assertEquals(bookingInfoInHX.get("child_room1"), "0",
				"child count is not zero in HX" + " for hotel :" + hotelName + " checkin : " + checkInCheckOutDates[0]
						+ "checkout : " + checkInCheckOutDates[1]);
		verify.assertEquals(bookingInfoInHX.get("statuscode_room1"),
				"RESERVE", "status code is wrong in HX" + " for hotel :" + hotelName + " checkin : "
						+ checkInCheckOutDates[0] + "checkout : " + checkInCheckOutDates[1]);
		verify.assertEquals((int) Math.round(Double.parseDouble(bookingInfoInHX.get("pretax_room1"))),
				(int) Math.round(hotelPriceInfoConfirmationPage[0]),
				"pretax amount is wrong in HX" + " for hotel :" + hotelName + " checkin : " + checkInCheckOutDates[0]
						+ "checkout : " + checkInCheckOutDates[1]);
		verify.assertEquals((int) Math.round(Double.parseDouble(bookingInfoInHX.get("tax_room1"))),
				(int) Math.round(hotelPriceInfoConfirmationPage[1]), "Tax is wrong in HX" + " for hotel :" + hotelName
						+ " checkin : " + checkInCheckOutDates[0] + "checkout : " + checkInCheckOutDates[1]);

		verify.assertAll();
	}

	/**
	 * This Test verifies that for Adult booking informations stored in DB and
	 * HX is appropriate
	 * 
	 * @throws ParseException
	 * @throws SQLException
	 * @throws SAXException
	 * @throws ParserConfigurationException
	 * @throws IOException
	 * @throws ClientProtocolException
	 */
	@Test(groups = { "BackEndTest","HotelBooking" })
	public void testDBHXTwoAdultTwoKidsTwoNightsBookingPayAtHotel() throws ParseException, SQLException,
			ClientProtocolException, IOException, ParserConfigurationException, SAXException {
		homePage = new HomePage();
		hotelResultsPage = new HotelResultsPage();
		itineraryPage = new ItineraryPage();
		confirmationPage = new ConfirmationPage();
		browserHelper = new BrowserHelper();
		bookingHistory = new BookingHistory();
		CommonUtils utils = new CommonUtils();
		SoftAssert verify = new SoftAssert();
		DBUtils db = new DBUtils();
		HotelLogixAPI api = new HotelLogixAPI();

		String location = utils.getProperty("Country"); // "India"; // "India";
		String testHotel = utils.getProperty("DummyHotel"); // "Dummy";
		String testUrl = utils.getProperty("TestUrl");
		String strSpecialRequest = utils.getProperty("SpecialRequest");
		int hotelIndex = 0;
		int checkInFromCurrentDate = utils.getRandomNumber(30,45);
		int checkOutFromCurrentDate = checkInFromCurrentDate + 1;
		int[][] roomIndex = { { 1, 2, 1 }, { 2, 2, 2 } };
		String adultCount = "4";
		String childCount = "3";
		String guestName = utils.getProperty("GuestName");
		String guestEmail = utils.getProperty("TestBookingAccount");
		String guestMobile = utils.getProperty("GuestMobile");
		String[] checkInCheckOutDates = new String[2];

		// Home Page is Displayed
		homePage.verifyHomePagePresence();
		// Do search and get checkin checkout dates
		checkInCheckOutDates = homePage.doSearch(location, checkInFromCurrentDate, checkOutFromCurrentDate, roomIndex);
		hotelResultsPage.verifyHotelResultsPagePresence();

		if (browserHelper.getCurrentUrl().contains(testUrl)) {
			hotelIndex = hotelResultsPage.getHotelIndexByHotelName(testHotel);
		} else {
			hotelIndex = hotelResultsPage.getHotelIndexByHotelAvailable();
		}

		String hotelName = hotelResultsPage.getHotelNameByIndex(hotelIndex);
		String hotelRoomRate = hotelResultsPage.getHotelRoomRateByIndex(hotelIndex);
		String hotelAddress = hotelResultsPage.getHotelAddressByIndex(hotelIndex);
		String hotelId = hotelResultsPage.getHotelIdByHotelName(hotelName);
		System.out.println("Hotel Name: " + hotelName + " ,Hotel id : " + hotelId + " ,Hotel room rate : "
				+ hotelRoomRate + " ,Hotel Address:" + hotelAddress);

		hotelResultsPage.clickQuickBookByIndex(hotelIndex);
		itineraryPage.verifyItineraryPagePresence();
		itineraryPage.clickContinueAsGuest();
		itineraryPage.enterSpecialRequest(strSpecialRequest);

		double[] hotelPriceInfoItineraryPage = itineraryPage.getHotelPriceInfoInItineraryPage();

		itineraryPage.bookHotelAsGuestWithPayAtHotelWithSpecialRequest(guestName, guestMobile, guestEmail,strSpecialRequest);
		confirmationPage.verifyConfirmationPagePresence();
		String bookingId = confirmationPage.getBookingId();
		double[] hotelPriceInfoConfirmationPage = confirmationPage.getHotelPriceInfoInConfirmationPage();

		verify.assertTrue(
				Math.abs((int) Math.round(hotelPriceInfoItineraryPage[0])
						- (int) Math.round(hotelPriceInfoConfirmationPage[0])) <= 1,
				"Room rate Itinerary page :" + (int) Math.round(hotelPriceInfoItineraryPage[0])
						+ " and Confirmation page: " + (int) Math.round(hotelPriceInfoConfirmationPage[0])
						+ " is not same");
		verify.assertTrue(
				Math.abs((int) Math.round(hotelPriceInfoItineraryPage[1])
						- (int) Math.round(hotelPriceInfoConfirmationPage[1])) <= 1,
				"Taxes in Itinerary page : " + (int) Math.round(hotelPriceInfoItineraryPage[1])
						+ " and Confirmation page :" + (int) Math.round(hotelPriceInfoConfirmationPage[1])
						+ " is not same");
		verify.assertTrue(
				Math.abs((int) Math.round(hotelPriceInfoItineraryPage[2])
						- (int) Math.round(hotelPriceInfoConfirmationPage[2])) <= 1,
				"Discount in Itinerary page :" + (int) Math.round(hotelPriceInfoItineraryPage[2])
						+ " and Confirmation page " + (int) Math.round(hotelPriceInfoConfirmationPage[2])
						+ " is not same");
		verify.assertTrue(
				Math.abs((int) Math.round(hotelPriceInfoItineraryPage[3])
						- (int) Math.round(hotelPriceInfoConfirmationPage[3])) <= 101,
				"Total in Itinerary page :" + (int) Math.round(hotelPriceInfoItineraryPage[3])
						+ " and Confirmation page " + (int) Math.round(hotelPriceInfoConfirmationPage[3])
						+ " is not same");

		verify.assertEquals(guestName, confirmationPage.getGuestNameInConfPage());
		verify.assertEquals(guestEmail, confirmationPage.getGuestEmailInConfPage());
		verify.assertEquals(guestMobile, confirmationPage.getGuestMobileInConfPage());
		verify.assertEquals(hotelName, confirmationPage.getHotelNameInConfirmationPage());

		System.out.println(utils.formatDate(checkInCheckOutDates[0]).toUpperCase());
		System.out.println(confirmationPage.getHotelCheckInDateInConfirmationPage().toUpperCase());
		verify.assertEquals(utils.formatDate(checkInCheckOutDates[0]).toUpperCase(),
				confirmationPage.getHotelCheckInDateInConfirmationPage().toUpperCase());
		verify.assertEquals(utils.formatDate(checkInCheckOutDates[1]).toUpperCase(),
				confirmationPage.getHotelCheckOutDateInConfirmationPage().toUpperCase());

		// Verify data in DB
		Map<String, String> dbBookingInfo = db.getBookingInfo(bookingId, hotelId);
		String orderId = dbBookingInfo.get("order_id");
		verify.assertEquals(dbBookingInfo.get("checkin_date"), utils.formatDateOne(checkInCheckOutDates[0]),
				"Checkin date is wrong in DB" + " for hotel :" + hotelName + " checkin : " + checkInCheckOutDates[0]
						+ "checkout : " + checkInCheckOutDates[1]);
		verify.assertEquals(dbBookingInfo.get("checkout_date"), utils.formatDateOne(checkInCheckOutDates[1]),
				"Checkout date is wrong in DB" + " for hotel :" + hotelName + " checkin : " + checkInCheckOutDates[0]
						+ "checkout : " + checkInCheckOutDates[1]);
		verify.assertEquals(dbBookingInfo.get("adult_count"), adultCount, "adult Count is wrong in DB" + " for hotel :"
				+ hotelName + " checkin : " + checkInCheckOutDates[0] + "checkout : " + checkInCheckOutDates[1]);
		verify.assertEquals(dbBookingInfo.get("child_count"), childCount, "Checkin date is wrong in DB" + " for hotel :"
				+ hotelName + " checkin : " + checkInCheckOutDates[0] + "checkout : " + checkInCheckOutDates[1]);
		verify.assertEquals((int) Math.round(Double.parseDouble(dbBookingInfo.get("total_amount"))),
				(int) Math.round(hotelPriceInfoConfirmationPage[3]),
				"Total amount is wrong in DB" + " for hotel :" + hotelName + " checkin : " + checkInCheckOutDates[0]
						+ "checkout : " + checkInCheckOutDates[1]);
		verify.assertEquals(dbBookingInfo.get("guest_name"), guestName, "guest Name is wrong in DB" + " for hotel :"
				+ hotelName + " checkin : " + checkInCheckOutDates[0] + "checkout : " + checkInCheckOutDates[1]);
		verify.assertEquals(dbBookingInfo.get("guest_email"), guestEmail, "guest Email is wrong in DB" + " for hotel :"
				+ hotelName + " checkin : " + checkInCheckOutDates[0] + "checkout : " + checkInCheckOutDates[1]);
		verify.assertEquals(dbBookingInfo.get("guest_mobile"), guestMobile,
				"guest Mobile is wrong in DB" + " for hotel :" + hotelName + " checkin : " + checkInCheckOutDates[0]
						+ "checkout : " + checkInCheckOutDates[1]);
		verify.assertEquals((int) Math.round(Double.parseDouble(dbBookingInfo.get("payment_amount"))), 0,
				"paymnet amount is wrong in DB" + " for hotel :" + hotelName + " checkin : " + checkInCheckOutDates[0]
						+ "checkout : " + checkInCheckOutDates[1]);
		verify.assertTrue(dbBookingInfo.get("comments").contains(strSpecialRequest),
				"Special request is wrong in DB" + " for hotel :" + hotelName + " checkin : " + checkInCheckOutDates[0]
						+ "checkout : " + checkInCheckOutDates[1]);
		
		verify.assertEquals((int) Math.round(Double.parseDouble(dbBookingInfo.get("discount"))),
				(int) Math.round(hotelPriceInfoConfirmationPage[2]),
				"Discount is wrong in DB" + " for hotel :" + hotelName + " checkin : " + checkInCheckOutDates[0]
						+ "checkout : " + checkInCheckOutDates[1]);
		verify.assertEquals(dbBookingInfo.get("payment_mode"), "Not Paid",
				"payment mode is wrong in DB" + " for hotel :" + hotelName + " checkin : " + checkInCheckOutDates[0]
						+ "checkout : " + checkInCheckOutDates[1]);
		verify.assertEquals(dbBookingInfo.get("status"), "1", "booking status is wrong in DB" + " for hotel :"
				+ hotelName + " checkin : " + checkInCheckOutDates[0] + "checkout : " + checkInCheckOutDates[1]);
		verify.assertEquals((int) Math.round(Double.parseDouble(dbBookingInfo.get("pretax_amount"))),
				(int) Math.round(hotelPriceInfoConfirmationPage[0]),
				"Room rate is wrong in DB" + " for hotel :" + hotelName + " checkin : " + checkInCheckOutDates[0]
						+ "checkout : " + checkInCheckOutDates[1]);
		verify.assertEquals((int) Math.round(Double.parseDouble(dbBookingInfo.get("tax_amount"))),
				(int) Math.round(hotelPriceInfoConfirmationPage[1]), "Tax is wrong in DB" + " for hotel :" + hotelName
						+ " checkin : " + checkInCheckOutDates[0] + "checkout : " + checkInCheckOutDates[1]);
		verify.assertEquals(dbBookingInfo.get("channel"), "website", "Channel is wrong in DB" + " for hotel :"
				+ hotelName + " checkin : " + checkInCheckOutDates[0] + "checkout : " + checkInCheckOutDates[1]);

		// verify data in HX get order response
		Map<String, String> bookingInfoInHX = api.getOrderDetailsFromHX(orderId);
		verify.assertEquals(bookingInfoInHX.get("guest_first_name"), guestName,
				"Guest Name is wrong in HX" + " for hotel :" + hotelName + " checkin : " + checkInCheckOutDates[0]
						+ "checkout : " + checkInCheckOutDates[1]);
		verify.assertEquals(bookingInfoInHX.get("guest_email"), guestEmail,
				"Guest Email is wrong in HX" + " for hotel :" + hotelName + " checkin : " + checkInCheckOutDates[0]
						+ "checkout : " + checkInCheckOutDates[1]);
		verify.assertEquals(bookingInfoInHX.get("guest_mobile"), guestMobile,
				"Guest Mobile is wrong in HX" + " for hotel :" + hotelName + " checkin : " + checkInCheckOutDates[0]
						+ "checkout : " + checkInCheckOutDates[1]);
		verify.assertEquals((int) Math.round(Double.parseDouble(bookingInfoInHX.get("total_amount"))),
				(int) Math.round(hotelPriceInfoConfirmationPage[3]),
				"Total amount is wrong in HX" + " for hotel :" + hotelName + " checkin : " + checkInCheckOutDates[0]
						+ "checkout : " + checkInCheckOutDates[1]);
		verify.assertTrue(bookingInfoInHX.get("special_request").contains(strSpecialRequest),
				"Special request is wrong in HX" + " for hotel :" + hotelName + " checkin : " + checkInCheckOutDates[0]
						+ "checkout : " + checkInCheckOutDates[1]);
		verify.assertEquals(bookingInfoInHX.get("checkindate_room1"), utils.formatDateOne(checkInCheckOutDates[0]),
				"Checkin date wrong in HX" + " for hotel :" + hotelName + " checkin : " + checkInCheckOutDates[0]
						+ "checkout : " + checkInCheckOutDates[1]);
		verify.assertEquals(bookingInfoInHX.get("checkoutdate_room1"), utils.formatDateOne(checkInCheckOutDates[1]),
				"checkout date wrong in HX" + " for hotel :" + hotelName + " checkin : " + checkInCheckOutDates[0]
						+ "checkout : " + checkInCheckOutDates[1]);
		verify.assertEquals(
				Integer.parseInt(bookingInfoInHX.get("adult_room1"))
						+ Integer.parseInt(bookingInfoInHX.get("adult_room2")),
				Integer.parseInt(adultCount), "Adult count is wrong in HX" + " for hotel :" + hotelName + " checkin : "
						+ checkInCheckOutDates[0] + "checkout : " + checkInCheckOutDates[1]);
		verify.assertEquals(
				Integer.parseInt(bookingInfoInHX.get("child_room1"))
						+ Integer.parseInt(bookingInfoInHX.get("child_room2")),
				0, "Child count is not zero in HX" + " for hotel :" + hotelName + " checkin : "
						+ checkInCheckOutDates[0] + "checkout : " + checkInCheckOutDates[1]);
		verify.assertEquals(bookingInfoInHX.get("statuscode_room1"), "RESERVE",
				"Status code is wrong in HX" + " for hotel :" + hotelName + " checkin : " + checkInCheckOutDates[0]
						+ "checkout : " + checkInCheckOutDates[1]);
		verify.assertEquals(bookingInfoInHX.get("statuscode_room2"), "RESERVE",
				"Status code is wrong in HX" + " for hotel :" + hotelName + " checkin : " + checkInCheckOutDates[0]
						+ "checkout : " + checkInCheckOutDates[1]);
		verify.assertTrue(Math.abs(
				(int) Math.round(Double.parseDouble(bookingInfoInHX.get("pretax_room1")))
						+ (int) Math.round(Double.parseDouble(bookingInfoInHX.get("pretax_room2"))) -
				(int) Math.round(hotelPriceInfoConfirmationPage[0])) <=1,
				"Pretax amount is wrong in HX" + " for hotel :" + hotelName + " checkin : " + checkInCheckOutDates[0]
						+ "checkout : " + checkInCheckOutDates[1] + " In HX amount is " + (int) Math.round(Double.parseDouble(bookingInfoInHX.get("pretax_room1")))
						+ (int) Math.round(Double.parseDouble(bookingInfoInHX.get("pretax_room2"))) + " but in confirmation page was : " + (int) Math.round(hotelPriceInfoConfirmationPage[0]));
		verify.assertTrue(Math.abs(
				(int) Math.round(Double.parseDouble(bookingInfoInHX.get("tax_room1")))
						+ (int) Math.round(Double.parseDouble(bookingInfoInHX.get("tax_room1"))) -
				(int) Math.round(hotelPriceInfoConfirmationPage[1])) <= 1, "Tax is wrong in HX" + " for hotel :" + hotelName
						+ " checkin : " + checkInCheckOutDates[0] + "checkout : " + checkInCheckOutDates[1] + " In HX amount is : " + (int) Math.round(Double.parseDouble(bookingInfoInHX.get("tax_room1")))
						+ (int) Math.round(Double.parseDouble(bookingInfoInHX.get("tax_room1"))) + " but in confirmation page was : " +  (int) Math.round(hotelPriceInfoConfirmationPage[1]));

		verify.assertAll();
	}

	/**
	 * This Test verifies that a new user registered on site is stored in DB
	 * 
	 * @throws ParseException
	 */
	@Test(groups = { "BackEndTest" }, enabled = false)
	public void testNewUserRegisteredStoredInDB() throws ParseException {
		homePage = new HomePage();
		CommonUtils utils = new CommonUtils();
		
		String name = "Test " + RandomStringUtils.randomAlphabetic(8);
		browserHelper = new BrowserHelper();
		String email = browserHelper.getDisposableEmail();
		String mobile = utils.getProperty("GuestMobile");
		String password = "password";

		// login link opens Signup pop up
		homePage.clickOnSignUpLink();
		homePage.verifySignUpPopUpPresence();
		homePage.enterSignUpForm(name, mobile, email, password);
        homePage.verifyUserIsSignedIn();
        homePage.clickOnSignOutLink();
        homePage.verifyUserIsNotSignedIn();
        
        
	}
}