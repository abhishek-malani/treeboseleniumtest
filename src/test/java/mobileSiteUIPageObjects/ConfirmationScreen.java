package mobileSiteUIPageObjects;

import java.io.IOException;

import javax.xml.parsers.ParserConfigurationException;

import org.apache.http.client.ClientProtocolException;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.xml.sax.SAXException;

import base.BrowserHelper;
import base.DriverManager;
import base.HotelLogixAPI;

public class ConfirmationScreen {
	private BrowserHelper browserHelper;
	private HomeScreen homeScreen;

	public boolean isConfirmationScreenDisplayed(){
		WebDriver driver = DriverManager.getInstance().getDriver();
		homeScreen = new HomeScreen();
		browserHelper = new BrowserHelper();
		
		//check if 500 page
		if (homeScreen.is500Displayed()){
			Assert.fail("500 displayed instead of itinerary screen for url " + browserHelper.getCurrentUrl());
		}
		try{
			WebDriverWait wait = new WebDriverWait(driver, 60);
			wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(".confirmation-page .confirmation div")));
		}catch (Exception e){
			Assert.fail("Confirmation screen not displayed for url + " + browserHelper.getCurrentUrl());
		}
		
		return driver.findElement(By.cssSelector(".confirmation-page .confirmation div")).isDisplayed();
	}
	
	public String getBookingId() {
		WebDriver driver = DriverManager.getInstance().getDriver();
		String id = driver.findElement(By.xpath("//div[@class='bookingInfo__item'][1]/div[2]")).getText();
		
		System.out.println("Booking Id is " + id);
		// if booking is done through async 
		// Finally verify that booking created in HX
        if (id.contains("TRB")){
    		HotelLogixAPI api = new HotelLogixAPI();
    		try {
				api.getOrderDetailsFromHX(id);
			} catch (ClientProtocolException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (ParserConfigurationException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (SAXException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
        }
        
		return id;
	}
	
	public String getGuestName(){
		WebDriver driver = DriverManager.getInstance().getDriver();
		return driver.findElement(By.xpath("//div[@class='bookingInfo__item'][2]/div[2]")).getText();
	}
	
	public String getHotelName(){
		WebDriver driver = DriverManager.getInstance().getDriver();
		return driver.findElement(By.cssSelector(".hotelDetail__hotel-name")).getText();
	}
	
	public String getCheckInDate(){
		WebDriver driver = DriverManager.getInstance().getDriver();
		return driver.findElement(By.xpath("//div[@class='bookingInfo__item'][3]/div[2]")).getText();
	}
	
	public String getCheckOutDate(){
		WebDriver driver = DriverManager.getInstance().getDriver();
		return driver.findElement(By.xpath("//div[@class='bookingInfo__item'][4]/div[2]")).getText();
	}
	
	public int getRoomCount(){
		WebDriver driver = DriverManager.getInstance().getDriver();
		return Integer.parseInt(driver.findElement(By.xpath("//div[@class='bookingInfo__item'][5]/div[2]")).getText().split(" ")[0]);
	}
	
	public int getGuestCount(){
		WebDriver driver = DriverManager.getInstance().getDriver();
		return Integer.parseInt(driver.findElement(By.xpath("//div[@class='bookingInfo__item'][6]/div[2]")).getText().split(" ")[0]);
	}
	
	public int getChildCount(){
		WebDriver driver = DriverManager.getInstance().getDriver();
		return Integer.parseInt(driver.findElement(By.xpath("//div[@class='bookingInfo__item'][6]/div[2]")).getText().split(" ")[2]);
	}
	
	public String getRoomType(){
		WebDriver driver = DriverManager.getInstance().getDriver();
		return driver.findElement(By.xpath("//div[@class='bookingInfo__item'][7]/div[2]")).getText();
	}
	
	public int getTotalCost(){
		WebDriver driver = DriverManager.getInstance().getDriver();
		return Integer.parseInt(driver.findElement(By.xpath("//div[@class='bookingInfo__item'][8]/div[2]/span")).getText());
	}
	
	public boolean isPayAtHotelImageDisplayed(){
		WebDriver driver = DriverManager.getInstance().getDriver();
		return driver.findElement(By.cssSelector("img[alt='Pay at hotel']")).isDisplayed();	
	}
	
	public void clickOnMakeAnotherBooking(){
		WebDriver driver = DriverManager.getInstance().getDriver();
		driver.findElement(By.xpath("//a[text()='Make Another Booking']")).click();
	}
}
