package newDesktopUITest;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.Map;

import javax.xml.parsers.ParserConfigurationException;

import org.apache.http.client.ClientProtocolException;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import org.testng.Assert;
import org.testng.asserts.SoftAssert;
import org.xml.sax.SAXException;

import utils.CommonUtils;
import utils.DBUtils;
import base.TestBaseSetUp;
import newDesktopUIPageObjects.HomePage;
import newDesktopUIPageObjects.BookingHistory;
import newDesktopUIPageObjects.HotelResultsPage;
import newDesktopUIPageObjects.ItineraryPage;
import newDesktopUIPageObjects.ConfirmationPage;
import newDesktopUIPageObjects.HotelDetailsPage;
import base.BrowserHelper;
import base.HotelLogixAPI;

@Test(singleThreaded = true)
public class BookingFlowTest extends TestBaseSetUp {

	private HomePage homePage;
	private HotelResultsPage hotelResultsPage;
	private ItineraryPage itineraryPage;
	private ConfirmationPage confirmationPage;
	private HotelDetailsPage hotelDetailsPage;
	private BookingHistory bookingHistory;
	private BrowserHelper browserHelper;

	/**
	 * 
	 * @throws ParseException
	 */
	@Test(groups = { "HotelBooking", "Sanity" })
	public void testHotelBookingAsGuestThroughQuickBookPayAtHotel() throws ParseException {
		homePage = new HomePage();
		hotelResultsPage = new HotelResultsPage();
		itineraryPage = new ItineraryPage();
		confirmationPage = new ConfirmationPage();
		browserHelper = new BrowserHelper();
		bookingHistory = new BookingHistory();
		CommonUtils utils = new CommonUtils();
		SoftAssert verify = new SoftAssert();

		String location = utils.getProperty("Country"); // "India"; // "India";
		String testHotel = utils.getProperty("DummyHotel"); // "Dummy";
		String testUrl = utils.getProperty("TestUrl");
		int hotelIndex = 0;
		int checkInFromCurrentDate = utils.getRandomNumber(30, 45);
		int checkOutFromCurrentDate = checkInFromCurrentDate + 1;
		int roomIndex = 1;
		String guestName = utils.getProperty("GuestName"); // "Test";
		String guestEmail = utils.getProperty("TestBookingAccount"); // "";
		String guestMobile = utils.getProperty("GuestMobile");// "9000000000";
		String strGmailPwd = utils.getProperty("GmailFbPassword");
		String[] checkInCheckOutDates = new String[2];

		// Home Page is Displayed
		homePage.verifyHomePagePresence();
		// Do search and get checkin checkout dates
		checkInCheckOutDates = homePage.doSearch(location, checkInFromCurrentDate, checkOutFromCurrentDate, roomIndex);
		hotelResultsPage.verifyHotelResultsPagePresence();

		if (browserHelper.getCurrentUrl().contains(testUrl)) {
			hotelIndex = hotelResultsPage.getHotelIndexByHotelName(testHotel);
		} else {
			hotelIndex = hotelResultsPage.getHotelIndexByHotelAvailable();
		}

		String hotelName = hotelResultsPage.getHotelNameByIndex(hotelIndex);
		String hotelRoomRate = hotelResultsPage.getHotelRoomRateByIndex(hotelIndex);
		String hotelAddress = hotelResultsPage.getHotelAddressByIndex(hotelIndex);
		String hotelId = hotelResultsPage.getHotelIdByHotelName(hotelName);
		System.out.println(
				"Hotel Name: " + hotelName + " ,Hotel id : " + hotelId +" ,Hotel room rate : " + hotelRoomRate + " ,Hotel Address:" + hotelAddress);

		hotelResultsPage.clickQuickBookByIndex(hotelIndex);
		itineraryPage.verifyItineraryPagePresence();
		itineraryPage.clickContinueAsGuest();
		itineraryPage.bookHotelAsGuestWithPayAtHotel(guestName, guestMobile, guestEmail);
		confirmationPage.verifyConfirmationPagePresence();
		String bookingId = confirmationPage.getBookingId();
		verify.assertEquals(guestName, confirmationPage.getGuestNameInConfPage());
		verify.assertEquals(guestEmail, confirmationPage.getGuestEmailInConfPage());
		verify.assertEquals(guestMobile, confirmationPage.getGuestMobileInConfPage());
		verify.assertEquals(hotelName, confirmationPage.getHotelNameInConfirmationPage());

		System.out.println(utils.formatDate(checkInCheckOutDates[0]).toUpperCase());
		System.out.println(confirmationPage.getHotelCheckInDateInConfirmationPage().toUpperCase());
		verify.assertEquals(utils.formatDate(checkInCheckOutDates[0]).toUpperCase(),
				confirmationPage.getHotelCheckInDateInConfirmationPage().toUpperCase());
		verify.assertEquals(utils.formatDate(checkInCheckOutDates[1]).toUpperCase(),
				confirmationPage.getHotelCheckOutDateInConfirmationPage().toUpperCase());

		// String appUrl = browserHelper.getCurrentUrl();
		// Map<String,String> confEmailData =
		// confirmationPage.getConfirmationEmailDetails(guestEmail,strGmailPwd,bookingId);
		// browserHelper.openUrl(appUrl);
		// Assert.assertEquals(confEmailData.get("bookingId"), bookingId);
		// Assert.assertEquals(confEmailData.get("hotelName"), hotelName);
		// Assert.assertEquals(confEmailData.get("guestName"), guestName);
		verify.assertAll();
	}

	/**
	 * 
	 * @throws ParseException
	 */

	@Test(groups = { "HotelBooking", "Sanity" })
	public void testHotelBookingAsTreeboMemberLoggedinAtHomePageThroughQuickBookPayAtHotel() throws ParseException {
		homePage = new HomePage();
		hotelResultsPage = new HotelResultsPage();
		itineraryPage = new ItineraryPage();
		confirmationPage = new ConfirmationPage();
		bookingHistory = new BookingHistory();
		browserHelper = new BrowserHelper();
		CommonUtils utils = new CommonUtils();
		SoftAssert verify = new SoftAssert();

		String location = utils.getProperty("Country"); // "India";
		String testHotel = utils.getProperty("DummyHotel"); // "Dummy";
		String testUrl = utils.getProperty("TestUrl");
		int hotelIndex = 0;
		int checkInFromCurrentDate = utils.getRandomNumber(30, 45);
		int checkOutFromCurrentDate = checkInFromCurrentDate + 1;
		int roomIndex = 1;
		String guestName = utils.getProperty("GuestName"); // "Test";
		String guestEmail = utils.getProperty("TestBookingAccount"); // "";
		String guestMobile = utils.getProperty("GuestMobile");// "9000000000";
		String guestPassword = utils.getProperty("TestLoginPassword"); // "password";
		String[] checkInCheckOutDates = new String[2];

		// Home Page is Displayed
		homePage.verifyHomePagePresence();
		homePage.signInAsTreeboMember(guestEmail, guestPassword);
		// Do search and get checkin checkout dates
		checkInCheckOutDates = homePage.doSearch(location, checkInFromCurrentDate, checkOutFromCurrentDate, roomIndex);
		hotelResultsPage.verifyHotelResultsPagePresence();

		if (browserHelper.getCurrentUrl().contains(testUrl)) {
			hotelIndex = hotelResultsPage.getHotelIndexByHotelName(testHotel);
		} else {
			hotelIndex = hotelResultsPage.getHotelIndexByHotelAvailable();
		}

		String hotelName = hotelResultsPage.getHotelNameByIndex(hotelIndex);
		String hotelRoomRate = hotelResultsPage.getHotelRoomRateByIndex(hotelIndex);
		String hotelAddress = hotelResultsPage.getHotelAddressByIndex(hotelIndex);
		System.out.println(
				"Hotel Name:" + hotelName + " Hotel room rate : " + hotelRoomRate + " Hotel Address:" + hotelAddress);

		hotelResultsPage.clickQuickBookByIndex(hotelIndex);
		itineraryPage.verifyItineraryPagePresence();
		itineraryPage.verifyGuestLoggedIn();
		//itineraryPage.enterGuestDetailsAtItineraryPage(guestName, guestMobile, guestEmail);
		itineraryPage.bookWithPayAtHotel();
		confirmationPage.verifyConfirmationPagePresence();
		String bookingId = confirmationPage.getBookingId();
		verify.assertEquals(guestName, confirmationPage.getGuestNameInConfPage());
		verify.assertEquals(guestEmail, confirmationPage.getGuestEmailInConfPage());
		verify.assertEquals(guestMobile, confirmationPage.getGuestMobileInConfPage());
		verify.assertEquals(hotelName, confirmationPage.getHotelNameInConfirmationPage());

		System.out.println(utils.formatDate(checkInCheckOutDates[0]).toUpperCase());
		System.out.println(confirmationPage.getHotelCheckInDateInConfirmationPage().toUpperCase());
		verify.assertEquals(utils.formatDate(checkInCheckOutDates[0]).toUpperCase(),
				confirmationPage.getHotelCheckInDateInConfirmationPage().toUpperCase());
		verify.assertEquals(utils.formatDate(checkInCheckOutDates[1]).toUpperCase(),
				confirmationPage.getHotelCheckOutDateInConfirmationPage().toUpperCase());
		verify.assertAll();
	}

	@Test(groups = { "HotelBooking", "Sanity" })
	public void testHotelBookingAsTreeboMemberLoggedinAtItineraryPageThroughQuickBookPayAtHotel()
			throws ParseException, InterruptedException {
		homePage = new HomePage();
		hotelResultsPage = new HotelResultsPage();
		itineraryPage = new ItineraryPage();
		confirmationPage = new ConfirmationPage();
		bookingHistory = new BookingHistory();
		browserHelper = new BrowserHelper();
		CommonUtils utils = new CommonUtils();
		SoftAssert verify = new SoftAssert();

		String location = utils.getProperty("Country"); // "India";
		String testHotel = utils.getProperty("DummyHotel"); // "Dummy";
		String testUrl = utils.getProperty("TestUrl");
		int hotelIndex = 0;
		int checkInFromCurrentDate = utils.getRandomNumber(30, 45);
		int checkOutFromCurrentDate = checkInFromCurrentDate + 1;
		int roomIndex = 1;
		String guestName = utils.getProperty("GuestName"); // "Test";
		String guestEmail = utils.getProperty("TestBookingAccount"); // "";
		String guestMobile = utils.getProperty("GuestMobile");// "9000000000";
		String guestPassword = utils.getProperty("TestLoginPassword"); // "password";
		String[] checkInCheckOutDates = new String[2];

		// Home Page is Displayed
		homePage.verifyHomePagePresence();
		// Do search and get checkin checkout dates
		checkInCheckOutDates = homePage.doSearch(location, checkInFromCurrentDate, checkOutFromCurrentDate, roomIndex);
		hotelResultsPage.verifyHotelResultsPagePresence();

		if (browserHelper.getCurrentUrl().contains(testUrl)) {
			hotelIndex = hotelResultsPage.getHotelIndexByHotelName(testHotel);
		} else {
			hotelIndex = hotelResultsPage.getHotelIndexByHotelAvailable();
		}

		String hotelName = hotelResultsPage.getHotelNameByIndex(hotelIndex);
		String hotelRoomRate = hotelResultsPage.getHotelRoomRateByIndex(hotelIndex);
		String hotelAddress = hotelResultsPage.getHotelAddressByIndex(hotelIndex);
		System.out.println(
				"Hotel Name:" + hotelName + " Hotel room rate : " + hotelRoomRate + " Hotel Address:" + hotelAddress);

		hotelResultsPage.clickQuickBookByIndex(hotelIndex);
		itineraryPage.verifyItineraryPagePresence();
		itineraryPage.loginAsTreeboMember(guestEmail, guestPassword);
		browserHelper.waitForPageLoad();
		itineraryPage.verifyGuestLoggedIn();
		//itineraryPage.enterGuestDetailsAtItineraryPage(guestName, guestMobile, guestEmail);
		itineraryPage.bookWithPayAtHotel();
		confirmationPage.verifyConfirmationPagePresence();
		String bookingId = confirmationPage.getBookingId();
		verify.assertEquals(guestName, confirmationPage.getGuestNameInConfPage());
		verify.assertEquals(guestEmail, confirmationPage.getGuestEmailInConfPage());
		verify.assertEquals(guestMobile, confirmationPage.getGuestMobileInConfPage());
		verify.assertEquals(hotelName, confirmationPage.getHotelNameInConfirmationPage());

		System.out.println(utils.formatDate(checkInCheckOutDates[0]).toUpperCase());
		System.out.println(confirmationPage.getHotelCheckInDateInConfirmationPage().toUpperCase());
		verify.assertEquals(utils.formatDate(checkInCheckOutDates[0]).toUpperCase(),
				confirmationPage.getHotelCheckInDateInConfirmationPage().toUpperCase());
		verify.assertEquals(utils.formatDate(checkInCheckOutDates[1]).toUpperCase(),
				confirmationPage.getHotelCheckOutDateInConfirmationPage().toUpperCase());
		verify.assertAll();
	}

	@Test(groups = { "HotelBooking", "Sanity" })
	public void testHotelBookingAsGuestThroughHotelDetailsWithPayAtHotel() throws ParseException {
		homePage = new HomePage();
		hotelResultsPage = new HotelResultsPage();
		itineraryPage = new ItineraryPage();
		confirmationPage = new ConfirmationPage();
		hotelDetailsPage = new HotelDetailsPage();
		bookingHistory = new BookingHistory();
		browserHelper = new BrowserHelper();
		CommonUtils utils = new CommonUtils();
		SoftAssert verify = new SoftAssert();

		String location = utils.getProperty("Country"); // "India";
		String testHotel = utils.getProperty("DummyHotel"); // "Dummy";
		String testUrl = utils.getProperty("TestUrl");
		int hotelIndex = 0;
		int checkInFromCurrentDate = utils.getRandomNumber(30, 45);
		int checkOutFromCurrentDate = checkInFromCurrentDate + 1;
		int roomIndex = 1;
		String guestName = utils.getProperty("GuestName"); // "Test";
		String guestEmail = utils.getProperty("TestBookingAccount"); // "";
		String guestMobile = utils.getProperty("GuestMobile");// "9000000000";
		String[] checkInCheckOutDates = new String[2];

		homePage.verifyHomePagePresence();
		checkInCheckOutDates = homePage.doSearch(location, checkInFromCurrentDate, checkOutFromCurrentDate, roomIndex);
		hotelResultsPage.verifyHotelResultsPagePresence();

		if (browserHelper.getCurrentUrl().contains(testUrl)) {
			hotelIndex = hotelResultsPage.getHotelIndexByHotelName(testHotel);
		} else {
			hotelIndex = hotelResultsPage.getHotelIndexByHotelAvailable();
		}

		String hotelName = hotelResultsPage.getHotelNameByIndex(hotelIndex);
		String hotelRoomRate = hotelResultsPage.getHotelRoomRateByIndex(hotelIndex);
		String hotelAddress = hotelResultsPage.getHotelAddressByIndex(hotelIndex);
		System.out.println(
				"Hotel Name:" + hotelName + " Hotel room rate : " + hotelRoomRate + " Hotel Address:" + hotelAddress);

		hotelResultsPage.clickHotelNameByIndex(hotelIndex);
		hotelDetailsPage.ensureRoomAvailableInHDPage();
		hotelDetailsPage.clickBookNowInHotelsDetailsPage();
		itineraryPage.verifyItineraryPagePresence();
		itineraryPage.clickContinueAsGuest();
		itineraryPage.bookHotelAsGuestWithPayAtHotel(guestName, guestMobile, guestEmail);
		confirmationPage.verifyConfirmationPagePresence();
		String bookingId = confirmationPage.getBookingId();
		verify.assertEquals(guestName, confirmationPage.getGuestNameInConfPage());
		verify.assertEquals(guestEmail, confirmationPage.getGuestEmailInConfPage());
		verify.assertEquals(guestMobile, confirmationPage.getGuestMobileInConfPage());
		verify.assertEquals(hotelName, confirmationPage.getHotelNameInConfirmationPage());

		System.out.println(utils.formatDate(checkInCheckOutDates[0]).toUpperCase());
		System.out.println(confirmationPage.getHotelCheckInDateInConfirmationPage().toUpperCase());
		verify.assertEquals(utils.formatDate(checkInCheckOutDates[0]).toUpperCase(),
				confirmationPage.getHotelCheckInDateInConfirmationPage().toUpperCase());
		verify.assertEquals(utils.formatDate(checkInCheckOutDates[1]).toUpperCase(),
				confirmationPage.getHotelCheckOutDateInConfirmationPage().toUpperCase());
		verify.assertAll();
	}

	
	@Test(groups = { "HotelBooking", "Sanity" })
	public void testHotelBookingAsGuestThroughHotelDetailsWithPayAtHotelGuestWithKidsTwoRoom() throws ParseException {
		homePage = new HomePage();
		hotelResultsPage = new HotelResultsPage();
		itineraryPage = new ItineraryPage();
		confirmationPage = new ConfirmationPage();
		hotelDetailsPage = new HotelDetailsPage();
		bookingHistory = new BookingHistory();
		browserHelper = new BrowserHelper();
		CommonUtils utils = new CommonUtils();
		SoftAssert verify = new SoftAssert();

		String location = utils.getProperty("Country"); // "India";
		String testHotel = utils.getProperty("DummyHotel"); // "Dummy";
		String testUrl = utils.getProperty("TestUrl");
		int hotelIndex = 0;
		int checkInFromCurrentDate = utils.getRandomNumber(30, 45);
		int checkOutFromCurrentDate = checkInFromCurrentDate + 1;
		int[][] roomIndex = { { 1, 2, 1 }, { 2, 2, 2 } };
		String guestName = utils.getProperty("GuestName"); // "Test";
		String guestEmail = utils.getProperty("TestBookingAccount"); // "";
		String guestMobile = utils.getProperty("GuestMobile");// "9000000000";
		String[] checkInCheckOutDates = new String[2];

		homePage.verifyHomePagePresence();
		checkInCheckOutDates = homePage.doSearch(location, checkInFromCurrentDate, checkOutFromCurrentDate, roomIndex);
		hotelResultsPage.verifyHotelResultsPagePresence();

		if (browserHelper.getCurrentUrl().contains(testUrl)) {
			hotelIndex = hotelResultsPage.getHotelIndexByHotelName(testHotel);
		} else {
			hotelIndex = hotelResultsPage.getHotelIndexByHotelAvailable();
		}

		String hotelName = hotelResultsPage.getHotelNameByIndex(hotelIndex);
		String hotelRoomRate = hotelResultsPage.getHotelRoomRateByIndex(hotelIndex);
		String hotelAddress = hotelResultsPage.getHotelAddressByIndex(hotelIndex);
		System.out.println(
				"Hotel Name:" + hotelName + " Hotel room rate : " + hotelRoomRate + " Hotel Address:" + hotelAddress);

		hotelResultsPage.clickHotelNameByIndex(hotelIndex);
		hotelDetailsPage.ensureRoomAvailableInHDPage();
		hotelDetailsPage.clickBookNowInHotelsDetailsPage();
		itineraryPage.verifyItineraryPagePresence();
		verify.assertEquals("4 adults, 3 kids", itineraryPage.getGuestDetailsInItineraryPage().toLowerCase(),"Itinerary page adult info is wrong");
		verify.assertEquals("2", itineraryPage.getRoomInfoInItineraryPage());
		
		itineraryPage.clickContinueAsGuest();
		itineraryPage.bookHotelAsGuestWithPayAtHotel(guestName, guestMobile, guestEmail);
		confirmationPage.verifyConfirmationPagePresence();
		String bookingId = confirmationPage.getBookingId();
		verify.assertEquals(guestName, confirmationPage.getGuestNameInConfPage());
		verify.assertEquals(guestEmail, confirmationPage.getGuestEmailInConfPage());
		verify.assertEquals(guestMobile, confirmationPage.getGuestMobileInConfPage());
		verify.assertEquals(hotelName, confirmationPage.getHotelNameInConfirmationPage());
		verify.assertEquals("4 adults, 3 kids", confirmationPage.getGuestDetailsInConfimrationPage().toLowerCase(),"Confirmation page adult info is wrong");
		verify.assertEquals("2", confirmationPage.getRoomInfoInConfirmationPage());

		System.out.println(utils.formatDate(checkInCheckOutDates[0]).toUpperCase());
		System.out.println(confirmationPage.getHotelCheckInDateInConfirmationPage().toUpperCase());
		verify.assertEquals(utils.formatDate(checkInCheckOutDates[0]).toUpperCase(),
				confirmationPage.getHotelCheckInDateInConfirmationPage().toUpperCase());
		verify.assertEquals(utils.formatDate(checkInCheckOutDates[1]).toUpperCase(),
				confirmationPage.getHotelCheckOutDateInConfirmationPage().toUpperCase());
		verify.assertAll();
	}
	
	
	@Test(groups = { "HotelBooking", "Sanity" })
	public void testHotelBookingAsTreeboMemberLoggedInHomePageThroughHotelDetailsPayAtHotel() throws ParseException {
		homePage = new HomePage();
		hotelResultsPage = new HotelResultsPage();
		itineraryPage = new ItineraryPage();
		confirmationPage = new ConfirmationPage();
		hotelDetailsPage = new HotelDetailsPage();
		bookingHistory = new BookingHistory();
		browserHelper = new BrowserHelper();
		CommonUtils utils = new CommonUtils();
		SoftAssert verify = new SoftAssert();

		String location = utils.getProperty("Country"); // "India";
		String testHotel = utils.getProperty("DummyHotel"); // "Dummy";
		String testUrl = utils.getProperty("TestUrl");
		int hotelIndex = 0;
		int checkInFromCurrentDate = utils.getRandomNumber(30, 45);
		int checkOutFromCurrentDate = checkInFromCurrentDate + 1;
		int roomIndex = 1;
		String guestName = utils.getProperty("GuestName"); // "Test";
		String guestEmail = utils.getProperty("TestBookingAccount"); // "";
		String guestMobile = utils.getProperty("GuestMobile");// "9000000000";
		String guestPassword = utils.getProperty("TestLoginPassword"); // "password";
		String[] checkInCheckOutDates = new String[2];

		homePage.verifyHomePagePresence();
		homePage.signInAsTreeboMember(guestEmail, guestPassword);
		checkInCheckOutDates = homePage.doSearch(location, checkInFromCurrentDate, checkOutFromCurrentDate, roomIndex);
		hotelResultsPage.verifyHotelResultsPagePresence();

		if (browserHelper.getCurrentUrl().contains(testUrl)) {
			hotelIndex = hotelResultsPage.getHotelIndexByHotelName(testHotel);
		} else {
			hotelIndex = hotelResultsPage.getHotelIndexByHotelAvailable();
		}

		String hotelName = hotelResultsPage.getHotelNameByIndex(hotelIndex);
		String hotelRoomRate = hotelResultsPage.getHotelRoomRateByIndex(hotelIndex);
		String hotelAddress = hotelResultsPage.getHotelAddressByIndex(hotelIndex);
		System.out.println(
				"Hotel Name:" + hotelName + " Hotel room rate : " + hotelRoomRate + " Hotel Address:" + hotelAddress);

		hotelResultsPage.clickHotelNameByIndex(hotelIndex);
		hotelDetailsPage.ensureRoomAvailableInHDPage();
		hotelDetailsPage.clickBookNowInHotelsDetailsPage();
		itineraryPage.verifyItineraryPagePresence();
		itineraryPage.verifyGuestLoggedIn();
		//itineraryPage.enterGuestDetailsAtItineraryPage(guestName, guestMobile, guestEmail);
		itineraryPage.bookWithPayAtHotel();
		confirmationPage.verifyConfirmationPagePresence();
		String bookingId = confirmationPage.getBookingId();
		verify.assertEquals(guestName, confirmationPage.getGuestNameInConfPage());
		verify.assertEquals(guestEmail, confirmationPage.getGuestEmailInConfPage());
		verify.assertEquals(guestMobile, confirmationPage.getGuestMobileInConfPage());
		verify.assertEquals(hotelName, confirmationPage.getHotelNameInConfirmationPage());

		System.out.println(utils.formatDate(checkInCheckOutDates[0]).toUpperCase());
		System.out.println(confirmationPage.getHotelCheckInDateInConfirmationPage().toUpperCase());
		verify.assertEquals(utils.formatDate(checkInCheckOutDates[0]).toUpperCase(),
				confirmationPage.getHotelCheckInDateInConfirmationPage().toUpperCase());
		verify.assertEquals(utils.formatDate(checkInCheckOutDates[1]).toUpperCase(),
				confirmationPage.getHotelCheckOutDateInConfirmationPage().toUpperCase());
		verify.assertAll();
	}

	@Test(groups = { "HotelBooking", "Sanity" })
	public void testHotelBookingAsTreeboMemberLoggedInItineraryPageThroughHotelDetailsPayAtHotel()
			throws ParseException, InterruptedException {
		homePage = new HomePage();
		hotelResultsPage = new HotelResultsPage();
		itineraryPage = new ItineraryPage();
		confirmationPage = new ConfirmationPage();
		hotelDetailsPage = new HotelDetailsPage();
		bookingHistory = new BookingHistory();
		browserHelper = new BrowserHelper();
		CommonUtils utils = new CommonUtils();
		SoftAssert verify = new SoftAssert();

		String location = utils.getProperty("Country"); // "India";
		String testHotel = utils.getProperty("DummyHotel"); // "Dummy";
		String testUrl = utils.getProperty("TestUrl");
		int hotelIndex = 0;
		int checkInFromCurrentDate = utils.getRandomNumber(30, 45);
		int checkOutFromCurrentDate = checkInFromCurrentDate + 1;
		int roomIndex = 1;
		String guestName = utils.getProperty("GuestName"); // "Test";
		String guestEmail = utils.getProperty("TestBookingAccount"); // "";
		String guestMobile = utils.getProperty("GuestMobile");// "9000000000";
		String guestPassword = utils.getProperty("TestLoginPassword"); // "password";
		String[] checkInCheckOutDates = new String[2];

		homePage.verifyHomePagePresence();
		checkInCheckOutDates = homePage.doSearch(location, checkInFromCurrentDate, checkOutFromCurrentDate, roomIndex);
		hotelResultsPage.verifyHotelResultsPagePresence();

		if (browserHelper.getCurrentUrl().contains(testUrl)) {
			hotelIndex = hotelResultsPage.getHotelIndexByHotelName(testHotel);
		} else {
			hotelIndex = hotelResultsPage.getHotelIndexByHotelAvailable();
		}

		String hotelName = hotelResultsPage.getHotelNameByIndex(hotelIndex);
		String hotelRoomRate = hotelResultsPage.getHotelRoomRateByIndex(hotelIndex);
		String hotelAddress = hotelResultsPage.getHotelAddressByIndex(hotelIndex);
		System.out.println(
				"Hotel Name:" + hotelName + " Hotel room rate : " + hotelRoomRate + " Hotel Address:" + hotelAddress);

		hotelResultsPage.clickHotelNameByIndex(hotelIndex);
		hotelDetailsPage.ensureRoomAvailableInHDPage();
		hotelDetailsPage.clickBookNowInHotelsDetailsPage();
		itineraryPage.verifyItineraryPagePresence();
		itineraryPage.loginAsTreeboMember(guestEmail, guestPassword);
		itineraryPage.verifyGuestLoggedIn();
		//itineraryPage.enterGuestDetailsAtItineraryPage(guestName, guestMobile, guestEmail);
		itineraryPage.bookWithPayAtHotel();
		confirmationPage.verifyConfirmationPagePresence();
		String bookingId = confirmationPage.getBookingId();
		verify.assertEquals(guestName, confirmationPage.getGuestNameInConfPage());
		verify.assertEquals(guestEmail, confirmationPage.getGuestEmailInConfPage());
		verify.assertEquals(guestMobile, confirmationPage.getGuestMobileInConfPage());
		verify.assertEquals(hotelName, confirmationPage.getHotelNameInConfirmationPage());

		System.out.println(utils.formatDate(checkInCheckOutDates[0]).toUpperCase());
		System.out.println(confirmationPage.getHotelCheckInDateInConfirmationPage().toUpperCase());
		verify.assertEquals(utils.formatDate(checkInCheckOutDates[0]).toUpperCase(),
				confirmationPage.getHotelCheckInDateInConfirmationPage().toUpperCase());
		verify.assertEquals(utils.formatDate(checkInCheckOutDates[1]).toUpperCase(),
				confirmationPage.getHotelCheckOutDateInConfirmationPage().toUpperCase());
		verify.assertAll();
	}


	/***
	 * Book a treebo by logging into website by FB in Itinerary page
	 * 
	 * @throws IOException
	 * @throws FileNotFoundException
	 * @throws InterruptedException
	 */
	@Test(groups = { "HotelBooking", "Sanity" },enabled = false)
	public void testHotelBookingThroughQuickBookPayAtHotelFBSignInItineraryPage()
			throws ParseException, FileNotFoundException, IOException, InterruptedException {
		homePage = new HomePage();
		hotelResultsPage = new HotelResultsPage();
		itineraryPage = new ItineraryPage();
		confirmationPage = new ConfirmationPage();
		browserHelper = new BrowserHelper();
		bookingHistory = new BookingHistory();
		CommonUtils utils = new CommonUtils();
		SoftAssert verify = new SoftAssert();

		String location = utils.getProperty("Country"); // "India";
		String testHotel = utils.getProperty("DummyHotel"); // "Dummy";
		String testUrl = utils.getProperty("TestUrl");
		int hotelIndex = 0;
		int checkInFromCurrentDate = utils.getRandomNumber(30, 45);
		int checkOutFromCurrentDate = checkInFromCurrentDate + 1;
		int roomIndex = 1;
		String[] checkInCheckOutDates = new String[2];
		String strGuestName = utils.getProperty("GuestName");
		String strGuestMobile = utils.getProperty("GuestMobile");// "9000000000";

		// Home Page is Displayed
		homePage.verifyHomePagePresence();
		// Do search and get checkin checkout dates
		checkInCheckOutDates = homePage.doSearch(location, checkInFromCurrentDate, checkOutFromCurrentDate, roomIndex);
		hotelResultsPage.verifyHotelResultsPagePresence();

		if (browserHelper.getCurrentUrl().contains(testUrl)) {
			hotelIndex = hotelResultsPage.getHotelIndexByHotelName(testHotel);
		} else {
			hotelIndex = hotelResultsPage.getHotelIndexByHotelAvailable();
		}

		String hotelName = hotelResultsPage.getHotelNameByIndex(hotelIndex);
		String hotelRoomRate = hotelResultsPage.getHotelRoomRateByIndex(hotelIndex);
		String hotelAddress = hotelResultsPage.getHotelAddressByIndex(hotelIndex);
		System.out.println(
				"Hotel Name:" + hotelName + " Hotel room rate : " + hotelRoomRate + " Hotel Address:" + hotelAddress);

		hotelResultsPage.clickQuickBookByIndex(hotelIndex);
		itineraryPage.verifyItineraryPagePresence();
		String[] guestDetails = itineraryPage.loginAsFBMember();
		itineraryPage.enterGuestDetailsAtItineraryPage(strGuestName, strGuestMobile, guestDetails[0]);
		itineraryPage.bookWithPayAtHotel();

		confirmationPage.verifyConfirmationPagePresence();
		String bookingId = confirmationPage.getBookingId();

		verify.assertEquals(strGuestName, confirmationPage.getGuestNameInConfPage());
		verify.assertEquals(guestDetails[0], confirmationPage.getGuestEmailInConfPage());
		verify.assertEquals(strGuestMobile, confirmationPage.getGuestMobileInConfPage());
		verify.assertEquals(hotelName, confirmationPage.getHotelNameInConfirmationPage());

		System.out.println(utils.formatDate(checkInCheckOutDates[0]).toUpperCase());
		System.out.println(confirmationPage.getHotelCheckInDateInConfirmationPage().toUpperCase());
		verify.assertEquals(utils.formatDate(checkInCheckOutDates[0]).toUpperCase(),
				confirmationPage.getHotelCheckInDateInConfirmationPage().toUpperCase());
		verify.assertEquals(utils.formatDate(checkInCheckOutDates[1]).toUpperCase(),
				confirmationPage.getHotelCheckOutDateInConfirmationPage().toUpperCase());
		verify.assertAll();
	}

	/***
	 * Book a treebo by logging into website by Google+ in Itinerary page
	 * 
	 * @throws InterruptedException
	 */
	@Test(groups = { "HotelBooking", "Sanity" }, enabled = false)
	public void testHotelBookingThroughQuickBookPayAtHotelGoogleSignInItineraryPage()
			throws ParseException, FileNotFoundException, IOException, InterruptedException {
		homePage = new HomePage();
		hotelResultsPage = new HotelResultsPage();
		itineraryPage = new ItineraryPage();
		confirmationPage = new ConfirmationPage();
		browserHelper = new BrowserHelper();
		bookingHistory = new BookingHistory();
		CommonUtils utils = new CommonUtils();
		SoftAssert verify = new SoftAssert();

		String location = utils.getProperty("Country"); // "India";
		String testHotel = utils.getProperty("DummyHotel"); // "Dummy";
		String testUrl = utils.getProperty("TestUrl");
		int hotelIndex = 0;
		int checkInFromCurrentDate = utils.getRandomNumber(30, 45);
		int checkOutFromCurrentDate = checkInFromCurrentDate + 1;
		int roomIndex = 1;
		String[] checkInCheckOutDates = new String[2];
		String strGuestName = utils.getProperty("GuestName");
		String strGuestMobile = utils.getProperty("GuestMobile");// "9000000000";

		// Home Page is Displayed
		homePage.verifyHomePagePresence();
		// Do search and get checkin checkout dates
		checkInCheckOutDates = homePage.doSearch(location, checkInFromCurrentDate, checkOutFromCurrentDate, roomIndex);
		hotelResultsPage.verifyHotelResultsPagePresence();

		if (browserHelper.getCurrentUrl().contains(testUrl)) {
			hotelIndex = hotelResultsPage.getHotelIndexByHotelName(testHotel);
		} else {
			hotelIndex = hotelResultsPage.getHotelIndexByHotelAvailable();
		}

		String hotelName = hotelResultsPage.getHotelNameByIndex(hotelIndex);
		String hotelRoomRate = hotelResultsPage.getHotelRoomRateByIndex(hotelIndex);
		String hotelAddress = hotelResultsPage.getHotelAddressByIndex(hotelIndex);
		System.out.println(
				"Hotel Name:" + hotelName + " Hotel room rate : " + hotelRoomRate + " Hotel Address:" + hotelAddress);

		hotelResultsPage.clickQuickBookByIndex(hotelIndex);
		itineraryPage.verifyItineraryPagePresence();
		String[] guestDetails = itineraryPage.loginAsGoogleMember();
		itineraryPage.enterGuestDetailsAtItineraryPage(strGuestName, strGuestMobile, guestDetails[0]);
		itineraryPage.bookWithPayAtHotel();

		confirmationPage.verifyConfirmationPagePresence();
		String bookingId = confirmationPage.getBookingId();

		verify.assertEquals(strGuestName, confirmationPage.getGuestNameInConfPage());
		verify.assertEquals(guestDetails[0], confirmationPage.getGuestEmailInConfPage());
		verify.assertEquals(strGuestMobile, confirmationPage.getGuestMobileInConfPage());
		verify.assertEquals(hotelName, confirmationPage.getHotelNameInConfirmationPage());

		System.out.println(utils.formatDate(checkInCheckOutDates[0]).toUpperCase());
		System.out.println(confirmationPage.getHotelCheckInDateInConfirmationPage().toUpperCase());
		verify.assertEquals(utils.formatDate(checkInCheckOutDates[0]).toUpperCase(),
				confirmationPage.getHotelCheckInDateInConfirmationPage().toUpperCase());
		verify.assertEquals(utils.formatDate(checkInCheckOutDates[1]).toUpperCase(),
				confirmationPage.getHotelCheckOutDateInConfirmationPage().toUpperCase());
		verify.assertAll();
	}

	/***
	 * 
	 * @throws ParseException
	 */

	@Test(groups = { "HotelBooking", "Sanity", "StagingOnly" },enabled=false)
	public void testHotelBookingAsTreeboMemberLoggedinAtHomePageThroughQuickBookPayNow() throws ParseException {
		homePage = new HomePage();
		hotelResultsPage = new HotelResultsPage();
		itineraryPage = new ItineraryPage();
		confirmationPage = new ConfirmationPage();
		bookingHistory = new BookingHistory();
		browserHelper = new BrowserHelper();
		CommonUtils utils = new CommonUtils();
		SoftAssert verify = new SoftAssert();

		String location = utils.getProperty("Country"); // "India";
		String testHotel = utils.getProperty("DummyHotel"); // "Dummy";
		String testUrl = utils.getProperty("TestUrl");
		int hotelIndex = 0;
		int checkInFromCurrentDate = utils.getRandomNumber(30, 45);
		int checkOutFromCurrentDate = checkInFromCurrentDate + 1;
		int roomIndex = 1;
		String guestName = utils.getProperty("GuestName"); // "Test";
		String guestEmail = utils.getProperty("TestBookingAccount"); // "";
		String guestMobile = utils.getProperty("GuestMobile");// "9000000000";
		String guestPassword = utils.getProperty("TestLoginPassword"); // "password";
		String[] checkInCheckOutDates = new String[2];

		// Home Page is Displayed
		homePage.verifyHomePagePresence();
		homePage.signInAsTreeboMember(guestEmail, guestPassword);
		// Do search and get checkin checkout dates
		checkInCheckOutDates = homePage.doSearch(location, checkInFromCurrentDate, checkOutFromCurrentDate, roomIndex);
		hotelResultsPage.verifyHotelResultsPagePresence();

		if (browserHelper.getCurrentUrl().contains(testUrl)) {
			hotelIndex = hotelResultsPage.getHotelIndexByHotelName(testHotel);
		}

		String hotelName = hotelResultsPage.getHotelNameByIndex(hotelIndex);
		String hotelRoomRate = hotelResultsPage.getHotelRoomRateByIndex(hotelIndex);
		String hotelAddress = hotelResultsPage.getHotelAddressByIndex(hotelIndex);
		System.out.println(
				"Hotel Name:" + hotelName + " Hotel room rate : " + hotelRoomRate + " Hotel Address:" + hotelAddress);

		hotelResultsPage.clickQuickBookByIndex(hotelIndex);
		itineraryPage.verifyItineraryPagePresence();
		itineraryPage.verifyGuestLoggedIn();
		itineraryPage.enterGuestDetailsAtItineraryPage(guestName, guestMobile, guestEmail);

		itineraryPage.bookWithPayNow();
		confirmationPage.verifyConfirmationPagePresence();
		String bookingId = confirmationPage.getBookingId();
		verify.assertEquals(guestName, confirmationPage.getGuestNameInConfPage());
		verify.assertEquals(guestEmail, confirmationPage.getGuestEmailInConfPage());
		verify.assertEquals(guestMobile, confirmationPage.getGuestMobileInConfPage());
		verify.assertEquals(hotelName, confirmationPage.getHotelNameInConfirmationPage());

		System.out.println(utils.formatDate(checkInCheckOutDates[0]).toUpperCase());
		System.out.println(confirmationPage.getHotelCheckInDateInConfirmationPage().toUpperCase());
		verify.assertEquals(utils.formatDate(checkInCheckOutDates[0]).toUpperCase(),
				confirmationPage.getHotelCheckInDateInConfirmationPage().toUpperCase());
		verify.assertEquals(utils.formatDate(checkInCheckOutDates[1]).toUpperCase(),
				confirmationPage.getHotelCheckOutDateInConfirmationPage().toUpperCase());
		verify.assertAll();

	}

	@Test(groups = { "HotelBooking", "Sanity", "StagingOnly" },enabled=false)
	public void testHotelBookingAsTreeboMemberLoggedInHomePageThroughHotelDetailsPayNow() throws ParseException {
		homePage = new HomePage();
		hotelResultsPage = new HotelResultsPage();
		itineraryPage = new ItineraryPage();
		confirmationPage = new ConfirmationPage();
		hotelDetailsPage = new HotelDetailsPage();
		bookingHistory = new BookingHistory();
		browserHelper = new BrowserHelper();
		CommonUtils utils = new CommonUtils();
		SoftAssert verify = new SoftAssert();

		String location = utils.getProperty("Country"); // "India";
		String testHotel = utils.getProperty("DummyHotel"); // "Dummy";
		String testUrl = utils.getProperty("TestUrl");
		int hotelIndex = 0;
		int checkInFromCurrentDate = utils.getRandomNumber(30, 45);
		int checkOutFromCurrentDate = checkInFromCurrentDate + 1;
		int roomIndex = 1;
		String guestName = utils.getProperty("GuestName"); // "Test";
		String guestEmail = utils.getProperty("TestBookingAccount"); // "";
		String guestMobile = utils.getProperty("GuestMobile");// "9000000000";
		String guestPassword = utils.getProperty("TestLoginPassword"); // "password";
		String[] checkInCheckOutDates = new String[2];

		homePage.verifyHomePagePresence();
		homePage.signInAsTreeboMember(guestEmail, guestPassword);
		checkInCheckOutDates = homePage.doSearch(location, checkInFromCurrentDate, checkOutFromCurrentDate, roomIndex);
		hotelResultsPage.verifyHotelResultsPagePresence();

		if (browserHelper.getCurrentUrl().contains(testUrl)) {
			hotelIndex = hotelResultsPage.getHotelIndexByHotelName(testHotel);
		} else {
			hotelIndex = hotelResultsPage.getHotelIndexByHotelAvailable();
		}

		String hotelName = hotelResultsPage.getHotelNameByIndex(hotelIndex);
		String hotelRoomRate = hotelResultsPage.getHotelRoomRateByIndex(hotelIndex);
		String hotelAddress = hotelResultsPage.getHotelAddressByIndex(hotelIndex);
		System.out.println("Hotel Name:" + hotelName);
		System.out.println("Hotel room rate :" + hotelRoomRate);
		System.out.println("Hotel Address:" + hotelAddress);

		hotelResultsPage.clickHotelNameByIndex(hotelIndex);
		hotelDetailsPage.ensureRoomAvailableInHDPage();
		hotelDetailsPage.clickBookNowInHotelsDetailsPage();
		itineraryPage.verifyItineraryPagePresence();
		itineraryPage.verifyGuestLoggedIn();
		itineraryPage.enterGuestDetailsAtItineraryPage(guestName, guestMobile, guestEmail);
		itineraryPage.bookWithPayNow();
		confirmationPage.verifyConfirmationPagePresence();
		String bookingId = confirmationPage.getBookingId();
		verify.assertEquals(guestName, confirmationPage.getGuestNameInConfPage());
		verify.assertEquals(guestEmail, confirmationPage.getGuestEmailInConfPage());
		verify.assertEquals(guestMobile, confirmationPage.getGuestMobileInConfPage());
		verify.assertEquals(hotelName, confirmationPage.getHotelNameInConfirmationPage());

		System.out.println(utils.formatDate(checkInCheckOutDates[0]).toUpperCase());
		System.out.println(confirmationPage.getHotelCheckInDateInConfirmationPage().toUpperCase());
		verify.assertEquals(utils.formatDate(checkInCheckOutDates[0]).toUpperCase(),
				confirmationPage.getHotelCheckInDateInConfirmationPage().toUpperCase());
		verify.assertEquals(utils.formatDate(checkInCheckOutDates[1]).toUpperCase(),
				confirmationPage.getHotelCheckOutDateInConfirmationPage().toUpperCase());
		verify.assertAll();

	}

	/***
	 * This test checks hotel booking, pricing correctness between HD page,
	 * itinerary page, confirmation page
	 * 
	 * @throws ParseException
	 * @throws SQLException 
	 * @throws SAXException 
	 * @throws ParserConfigurationException 
	 * @throws IOException 
	 * @throws ClientProtocolException 
	 */
	@Test(groups = { "HotelBooking", "Sanity" })
	public void testHotelBookingBasicPricingAcrossPages() throws ParseException, SQLException, ClientProtocolException, IOException, ParserConfigurationException, SAXException {
		homePage = new HomePage();
		hotelResultsPage = new HotelResultsPage();
		hotelDetailsPage = new HotelDetailsPage();
		itineraryPage = new ItineraryPage();
		confirmationPage = new ConfirmationPage();
		browserHelper = new BrowserHelper();
		CommonUtils utils = new CommonUtils();
		SoftAssert verify = new SoftAssert();
		DBUtils db = new DBUtils();
		HotelLogixAPI api = new HotelLogixAPI();

		String location = utils.getProperty("Country"); // "India"; // "India";
		String strSpecialRequest = utils.getProperty("SpecialRequest");
		int checkInFromCurrentDate = utils.getRandomNumber(100, 120);
		int checkOutFromCurrentDate = checkInFromCurrentDate + 1;
		int roomIndex = 1;
		String guestName = utils.getProperty("GuestName"); // "Test";
		String guestEmail = utils.getProperty("TestBookingAccount"); // "";
		String guestMobile = utils.getProperty("GuestMobile");// "9000000000";
		String[] checkInCheckOutDates = new String[2];

		// Home Page is Displayed
		homePage.verifyHomePagePresence();
		// Do search and get checkin checkout dates
		checkInCheckOutDates = homePage.doSearch(location, checkInFromCurrentDate, checkOutFromCurrentDate, roomIndex);
		hotelResultsPage.verifyHotelResultsPagePresence();

		int hotelIndex = hotelResultsPage.getHotelIndexByHotelAvailable();
		String hotelName = hotelResultsPage.getHotelNameByIndex(hotelIndex);
		String hotelRoomRate = hotelResultsPage.getHotelRoomRateByIndex(hotelIndex);
		String hotelAddress = hotelResultsPage.getHotelAddressByIndex(hotelIndex);
		String hotelId = hotelResultsPage.getHotelIdByHotelName(hotelName);
		System.out.println(
				"Hotel Name: " + hotelName + " ,Hotel id : " + hotelId +" ,Hotel room rate : " + hotelRoomRate + " ,Hotel Address:" + hotelAddress);


		int[] hotelPriceInSearch = hotelResultsPage.getHotelPriceBreakUpByIndex(hotelIndex);
		int roomRateInSearch = Integer.parseInt(hotelResultsPage.getHotelRoomRateByIndex(hotelIndex));
		int sumInSearch = hotelPriceInSearch[0] + hotelPriceInSearch[1] - hotelPriceInSearch[2];
		verify.assertEquals(hotelPriceInSearch[3], sumInSearch, "Total " + hotelPriceInSearch[3]
				+ ". is not same as sum of room price, tax and discount" + sumInSearch);

		hotelResultsPage.clickHotelNameByIndex(hotelIndex);
		hotelDetailsPage.verifyHotelDetailsPagePresence();
		hotelDetailsPage.ensureRoomAvailableInHDPage();

		double[] hotelPriceInfoHD = hotelDetailsPage.getHotelPriceBreakUpInHD();
		int sumOfRateTaxDiscHDPage = (int) Math.round(hotelPriceInfoHD[0] + hotelPriceInfoHD[1] - hotelPriceInfoHD[2]);
		int hotelRoomRateInHD = hotelDetailsPage.getHotelTotalPriceInHD();
		verify.assertTrue(Math.abs((int) Math.round(hotelPriceInfoHD[3]) - sumOfRateTaxDiscHDPage) <= 1,
				"Total : " + (int) Math.round(hotelPriceInfoHD[3]) + " and sum of room price, tax and discount :"
						+ sumOfRateTaxDiscHDPage + " does not match");
		verify.assertTrue(Math.abs(hotelRoomRateInHD - (int) Math.round(hotelPriceInfoHD[3])) <= 1,
				" Total Displayed in HD :" + hotelRoomRateInHD + " and in break up :"
						+ (int) Math.round(hotelPriceInfoHD[3]) + " not same");
		verify.assertTrue(Math.abs(hotelPriceInSearch[0] - (int) Math.round(hotelPriceInfoHD[0])) <= 1,
				"Room rate in Search page :" + hotelPriceInSearch[0] + " and HD Page :"
						+ (int) Math.round(hotelPriceInfoHD[0]) + " is not same");
		verify.assertTrue(Math.abs(hotelPriceInSearch[1] - (int) Math.round(hotelPriceInfoHD[1])) <= 1,
				"Taxes in Search page: " + hotelPriceInSearch[1] + " and HD Page "
						+ (int) Math.round(hotelPriceInfoHD[1]) + " is not same");
		verify.assertTrue(Math.abs(hotelPriceInSearch[2] - (int) Math.round(hotelPriceInfoHD[2])) <= 1,
				"Discount in Search page :" + hotelPriceInSearch[2] + " and HD Page :"
						+ (int) Math.round(hotelPriceInfoHD[2]) + " is not same");
		verify.assertTrue(Math.abs(hotelPriceInSearch[3] - (int) Math.round(hotelPriceInfoHD[3])) <= 1,
				"Total in Search page : " + hotelPriceInSearch[3] + " and HD Page :"
						+ (int) Math.round(hotelPriceInfoHD[3]) + " is not same");

		hotelDetailsPage.clickBookNowInHotelsDetailsPage();
		itineraryPage.verifyItineraryPagePresence();
		itineraryPage.enterSpecialRequest(strSpecialRequest);

		double[] hotelPriceInfoItineraryPage = itineraryPage.getHotelPriceInfoInItineraryPage();
		verify.assertTrue(Math.abs(hotelRoomRateInHD - (int) Math.round(hotelPriceInfoItineraryPage[3])) <= 1,
				"Total in HD: " + hotelRoomRateInHD + " and Itinerary page :"
						+ (int) Math.round(hotelPriceInfoItineraryPage[3]) + " varies");
		verify.assertTrue(
				Math.abs((int) Math.round(hotelPriceInfoHD[0]) - (int) Math.round(hotelPriceInfoItineraryPage[0])) <= 1,
				"Room rate in HD Page : " + (int) Math.round(hotelPriceInfoHD[0]) + " and Itinerary page :"
						+ (int) Math.round(hotelPriceInfoItineraryPage[0]) + " is not same");
		verify.assertTrue(
				Math.abs((int) Math.round(hotelPriceInfoHD[1]) - (int) Math.round(hotelPriceInfoItineraryPage[1])) <= 1,
				"Taxes in HD Page: " + (int) Math.round(hotelPriceInfoHD[1]) + " and Itinerary page :"
						+ (int) Math.round(hotelPriceInfoItineraryPage[1]) + " is not same");
		verify.assertTrue(
				Math.abs((int) Math.round(hotelPriceInfoHD[2]) - (int) Math.round(hotelPriceInfoItineraryPage[2])) <= 1,
				"Discount in HD Page : " + (int) Math.round(hotelPriceInfoHD[2]) + " and Itinerary page :"
						+ (int) Math.round(hotelPriceInfoItineraryPage[2]) + " is not same");
		verify.assertTrue(
				Math.abs((int) Math.round(hotelPriceInfoHD[3]) - (int) Math.round(hotelPriceInfoItineraryPage[3])) <= 1,
				"Total in HD Page :" + (int) Math.round(hotelPriceInfoHD[3]) + " and Itinerary page : "
						+ (int) Math.round(hotelPriceInfoItineraryPage[3]) + " is not same");

		itineraryPage.clickContinueAsGuest();
		itineraryPage.bookHotelAsGuestWithPayAtHotelWithSpecialRequest(guestName, guestMobile, guestEmail,strSpecialRequest);
		confirmationPage.verifyConfirmationPagePresence();
		String bookingId = confirmationPage.getBookingId();
		double[] hotelPriceInfoConfirmationPage = confirmationPage.getHotelPriceInfoInConfirmationPage();

		verify.assertTrue(
				Math.abs((int) Math.round(hotelPriceInfoItineraryPage[0])
						- (int) Math.round(hotelPriceInfoConfirmationPage[0])) <= 1,
				"Room rate Itinerary page :" + (int) Math.round(hotelPriceInfoItineraryPage[0])
						+ " and Confirmation page: " + (int) Math.round(hotelPriceInfoConfirmationPage[0])
						+ " is not same");
		verify.assertTrue(
				Math.abs((int) Math.round(hotelPriceInfoItineraryPage[1])
						- (int) Math.round(hotelPriceInfoConfirmationPage[1])) <= 1,
				"Taxes in Itinerary page : " + (int) Math.round(hotelPriceInfoItineraryPage[1])
						+ " and Confirmation page :" + (int) Math.round(hotelPriceInfoConfirmationPage[1])
						+ " is not same");
		verify.assertTrue(
				Math.abs((int) Math.round(hotelPriceInfoItineraryPage[2])
						- (int) Math.round(hotelPriceInfoConfirmationPage[2])) <= 1,
				"Discount in Itinerary page :" + (int) Math.round(hotelPriceInfoItineraryPage[2])
						+ " and Confirmation page " + (int) Math.round(hotelPriceInfoConfirmationPage[2])
						+ " is not same");
		verify.assertTrue(
				Math.abs((int) Math.round(hotelPriceInfoItineraryPage[3])
						- (int) Math.round(hotelPriceInfoConfirmationPage[3])) <= 101,
				"Total in Itinerary page :" + (int) Math.round(hotelPriceInfoItineraryPage[3])
						+ " and Confirmation page " + (int) Math.round(hotelPriceInfoConfirmationPage[3])
						+ " is not same");

		verify.assertEquals(guestName, confirmationPage.getGuestNameInConfPage());
		verify.assertEquals(guestEmail, confirmationPage.getGuestEmailInConfPage());
		verify.assertEquals(guestMobile, confirmationPage.getGuestMobileInConfPage());
		verify.assertEquals(hotelName, confirmationPage.getHotelNameInConfirmationPage());
				
		verify.assertAll();
	}
	
	
	/***
	 * 
	 * @throws ParseException
	 */

	@Test(groups = { "HotelBooking", "Sanity" },enabled = false)
	public void testHotelBookingFlowTakeToPayUAsTreeboMemberLoggedinAtHomePageThroughQuickBook() throws ParseException {
		homePage = new HomePage();
		hotelResultsPage = new HotelResultsPage();
		itineraryPage = new ItineraryPage();
		confirmationPage = new ConfirmationPage();
		bookingHistory = new BookingHistory();
		browserHelper = new BrowserHelper();
		CommonUtils utils = new CommonUtils();
		SoftAssert verify = new SoftAssert();

		String location = utils.getProperty("Country"); // "India";
		String testHotel = utils.getProperty("DummyHotel"); // "Dummy";
		String testUrl = utils.getProperty("TestUrl");
		int hotelIndex = 0;
		int checkInFromCurrentDate = utils.getRandomNumber(30, 45);
		int checkOutFromCurrentDate = checkInFromCurrentDate + 1;
		int roomIndex = 1;
		String guestName = utils.getProperty("GuestName"); // "Test";
		String guestEmail = utils.getProperty("TestBookingAccount"); // "";
		String guestMobile = utils.getProperty("GuestMobile");// "9000000000";
		String guestPassword = utils.getProperty("TestLoginPassword"); // "password";
		String[] checkInCheckOutDates = new String[2];

		// Home Page is Displayed
		homePage.verifyHomePagePresence();
		homePage.signInAsTreeboMember(guestEmail, guestPassword);
		// Do search and get checkin checkout dates
		checkInCheckOutDates = homePage.doSearch(location, checkInFromCurrentDate, checkOutFromCurrentDate, roomIndex);
		hotelResultsPage.verifyHotelResultsPagePresence();

		if (browserHelper.getCurrentUrl().contains(testUrl)) {
			hotelIndex = hotelResultsPage.getHotelIndexByHotelName(testHotel);
		}

		String hotelName = hotelResultsPage.getHotelNameByIndex(hotelIndex);
		String hotelRoomRate = hotelResultsPage.getHotelRoomRateByIndex(hotelIndex);
		String hotelAddress = hotelResultsPage.getHotelAddressByIndex(hotelIndex);
		System.out.println(
				"Hotel Name:" + hotelName + " Hotel room rate : " + hotelRoomRate + " Hotel Address:" + hotelAddress);

		hotelResultsPage.clickQuickBookByIndex(hotelIndex);
		itineraryPage.verifyItineraryPagePresence();
		itineraryPage.verifyGuestLoggedIn();
		itineraryPage.enterGuestDetailsAtItineraryPage(guestName, guestMobile, guestEmail);

        itineraryPage.clickOnPayNowButton();
        verify.assertTrue(itineraryPage.isPayUPageDisplayed(),"Pay now page not displayed");
        verify.assertTrue(itineraryPage.isGoBackLinkTreeboDisplayed(),"Go back treebo link on Pay U not dsiaplyed");
        itineraryPage.clickGoBackToTreeboOnPayU();
        itineraryPage.verifyPayUPaymentFailed();
		verify.assertAll();
	}

	
	/**
	 * Open the HomePage
	 * @throws InterruptedException
	 * @throws ParseException
	 * Search for random city and Hotel for Two days 
	 * Search Result Page will shown 
	 * Use mouseOver to go on Price break-up then
	 * Extract the details from there hotelPrice,Tax 
	 * click on viewDailyPriceBreakUp 
	 * After click on viewDailyPriceBreakUp the window will open 
	 * then verify the details on that page
	 */

	@Test(groups = { "HotelBooking", "Sanity" })
	public void testPriceBreakUpOnHotelResultsPage() throws InterruptedException, ParseException {
		homePage = new HomePage();
		hotelResultsPage = new HotelResultsPage();
		itineraryPage = new ItineraryPage();
		confirmationPage = new ConfirmationPage();
		browserHelper = new BrowserHelper();
		bookingHistory = new BookingHistory();
		CommonUtils utils = new CommonUtils();
		SoftAssert verify = new SoftAssert();
		String location = utils.getProperty("city"); // "India";
		String testHotel = utils.getProperty("DummyHotel"); // "Dummy";
		String testUrl = utils.getProperty("TestUrl");
		int hotelIndex = 0;
		int checkInFromCurrentDate = utils.getRandomNumber(30, 45);
		int checkOutFromCurrentDate = checkInFromCurrentDate + 2;
		// int nextDayAfterCheckIn = checkInFromCurrentDate+1;
		int roomIndex = 1;
		String[] checkInCheckOutDates = new String[2];
		// Home Page is Displayed
		homePage.verifyHomePagePresence();
		// Do search and get checkin checkout dates
		checkInCheckOutDates = homePage.doSearch(location, checkInFromCurrentDate, checkOutFromCurrentDate, roomIndex);
		hotelResultsPage.verifyHotelResultsPagePresence();

		if (browserHelper.getCurrentUrl().contains(testUrl)) {
			hotelIndex = hotelResultsPage.getHotelIndexByHotelName(testHotel);
		} else {
			hotelIndex = hotelResultsPage.getHotelIndexByHotelAvailable();
		}

		String hotelName = hotelResultsPage.getHotelNameByIndex(hotelIndex);
		String hotelRoomRate = hotelResultsPage.getHotelRoomRateByIndex(hotelIndex);
		String hotelAddress = hotelResultsPage.getHotelAddressByIndex(hotelIndex);
		System.out.println(
				"Hotel Name:" + hotelName + " Hotel room rate : " + hotelRoomRate + " Hotel Address:" + hotelAddress);
		hotelResultsPage.mouseOverHotelResultsByIndex(hotelIndex);
		int[] hotelPriceInfo = hotelResultsPage.getHotelPriceBreakUpByIndex(hotelIndex);
		String checkIn = utils.formatDateWithoutSuffix(checkInCheckOutDates[0]);
		String checkOut = utils.formatDateWithoutSuffix(checkInCheckOutDates[1]);
		int hotelPrice = hotelPriceInfo[0];
		int tax = hotelPriceInfo[1];
		int discount = hotelPriceInfo[2];
		int totalPrice = hotelPriceInfo[3];
		System.out.println(hotelPrice + " " + tax + " " + discount + " " + totalPrice);
		// hotelResultsPage.mouseOverToPopUp();
		verify.assertTrue(hotelResultsPage.isTotalPriceDisplayCorrect(totalPrice, hotelPrice, discount, tax));
		// click on hotelPrice breakUp by its index
		hotelResultsPage.clickOnDailyPriceBreakUpOnHotelResultPage(hotelIndex);
		verify.assertTrue(hotelResultsPage.isOpenPriceBreakUpWindow(), "Price BreakUp Window is not opened");
		verify.assertTrue(hotelResultsPage.isPriceBreakTitleDisplayed(), "Price Break Up Title is not displayed");
		verify.assertTrue(hotelResultsPage.doesCheckInCheckoutCheckedInTitle(checkIn, checkOut),
				"Not Verified the CheckIn, CheckOut");
		verify.assertTrue(hotelResultsPage.isDateTitleDisplayed(), "Date Title is not displayed");
		verify.assertTrue(hotelResultsPage.isBasePriceTitleDisplayed(), "Base Price Title is not displayed");
		verify.assertTrue(hotelResultsPage.isTaxTitleDisplayed(), "Tax Title is not displayed");
		verify.assertTrue(hotelResultsPage.isFinalPriceTitle(), "Final Price Title is not displayed");
		verify.assertTrue(hotelResultsPage.isCheckInDateSame(checkIn), "CheckIn Date is not same");
		
		verify.assertTrue(hotelResultsPage.compareBasePrice(2, hotelPrice), "Hotel Price is not compared");
		verify.assertTrue(hotelResultsPage.compareTaxPrice(2, tax), "Tax Price is not compared");
		verify.assertTrue(hotelResultsPage.comapareActualPrice(2, totalPrice), "Actual Price is not compared");
		verify.assertTrue(hotelResultsPage.checkRoomPriceOnFooter(hotelPrice), "Room Price not checked");
		verify.assertTrue(hotelResultsPage.checkTaxPriceOnFooter(tax), "Tax price on Footer not checked");
		verify.assertTrue(hotelResultsPage.checkTotalPriceOnFooter(totalPrice), "Total Price not  checked");
		verify.assertTrue(hotelResultsPage.isInclusiveTaxTextDisplayed(), "Inclusive Taxt is not dispalyed");
		verify.assertTrue(hotelResultsPage.isDoneButtonDisplayed(), "Done button is not displayed");
		hotelResultsPage.clickOnDone();
		verify.assertFalse(hotelResultsPage.isOpenPriceBreakUpWindow(), "Price BreakUp Window is opened");

		verify.assertAll();
	}

	/**
	 * 
	 * @throws ParseException
	 * Search by city,CheckIn,CheckOut 
	 * click on hotel by its name
	 * open HD page 
	 * mouseover to PriceBreakUp 
	 * click on the PriceBreakUp 
	 * verify the details on that page
	 * 
	 */
	@Test(groups = { "HotelBooking", "Sanity" })
	public void testPriceBreakUpOnHDPage() throws ParseException {
		homePage = new HomePage();
		hotelResultsPage = new HotelResultsPage();
		itineraryPage = new ItineraryPage();
		confirmationPage = new ConfirmationPage();
		browserHelper = new BrowserHelper();
		bookingHistory = new BookingHistory();
		CommonUtils utils = new CommonUtils();
		SoftAssert verify = new SoftAssert();
		String location = utils.getProperty("city"); // "India";
		String testHotel = utils.getProperty("DummyHotel"); // "Dummy";
		String testUrl = utils.getProperty("TestUrl");
		int hotelIndex = 0;
		int checkInFromCurrentDate = utils.getRandomNumber(30, 45);
		int checkOutFromCurrentDate = checkInFromCurrentDate + 2;
		// int nextDayAfterCheckIn = checkInFromCurrentDate+1;
		int roomIndex = 1;
		String[] checkInCheckOutDates = new String[2];
		// Home Page is Displayed
		homePage.verifyHomePagePresence();
		// Do search and get checkin checkout dates
		checkInCheckOutDates = homePage.doSearch(location, checkInFromCurrentDate, checkOutFromCurrentDate, roomIndex);
		hotelResultsPage.verifyHotelResultsPagePresence();

		if (browserHelper.getCurrentUrl().contains(testUrl)) {
			hotelIndex = hotelResultsPage.getHotelIndexByHotelName(testHotel);
		} else {
			hotelIndex = hotelResultsPage.getHotelIndexByHotelAvailable();
		}

		String hotelName = hotelResultsPage.getHotelNameByIndex(hotelIndex);
		String hotelRoomRate = hotelResultsPage.getHotelRoomRateByIndex(hotelIndex);
		String hotelAddress = hotelResultsPage.getHotelAddressByIndex(hotelIndex);
		System.out.println(
				"Hotel Name:" + hotelName + " Hotel room rate : " + hotelRoomRate + " Hotel Address:" + hotelAddress);
		// click on hotel by its name
		hotelResultsPage.clickOnHotelByName(hotelName);
		// mouse over to fare breakup
		hotelResultsPage.mouseToViewFareBreakup();
		try {
			Thread.sleep(4000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		// Extract hotelprice from HD page
		int hotelPrice = hotelResultsPage.extractRoomPriceFromHDPageBreakupPopUp();
		// Extract tax from HD page
		int tax = hotelResultsPage.extractTaxPriceFromHDBreakupPopUp();
		// Extract discount from HD page
		int discount = hotelResultsPage.extractDiscountPriceFromHDPageBreakupPopUp();
		// Extract discount from HD page
		int totalPrice = hotelResultsPage.extractTotalPriceFromHDPageBreakupPopUp();
		System.out.println(hotelPrice + " " + tax + " " + discount + " " + totalPrice);
		String checkIn = utils.formatDateWithoutSuffix(checkInCheckOutDates[0]);
		String checkOut = utils.formatDateWithoutSuffix(checkInCheckOutDates[1]);

		// click on price breakUp
		hotelResultsPage.clickOnPriceBreakUpOnHDPage();
		// priceBreakup window opened
		verify.assertTrue(hotelResultsPage.isOpenPriceBreakUpWindow(), "Price BreakUp Window is not opened");
		// Title of pricebreakup page verified
		verify.assertTrue(hotelResultsPage.isPriceBreakTitleDisplayed(), "Price Break Up Title is not displayed");
		// verified the checkIn checkOut date on the page
		verify.assertTrue(hotelResultsPage.doesCheckInCheckoutCheckedInTitle(checkIn, checkOut),
				"Not Verified the CheckIn, CheckOut");
		// Date title is verified
		verify.assertTrue(hotelResultsPage.isDateTitleDisplayed(), "Date Title is not displayed");
		// BasePrice Title verified
		verify.assertTrue(hotelResultsPage.isBasePriceTitleDisplayed(), "Base Price Title is not displayed");
		// Tax title verified
		verify.assertTrue(hotelResultsPage.isTaxTitleDisplayed(), "Tax Title is not displayed");
		// Final price title verified
		verify.assertTrue(hotelResultsPage.isFinalPriceTitle(), "Final Price Title is not displayed");
		// verify checkIn date on the page
		verify.assertTrue(hotelResultsPage.isCheckInDateSame(checkIn), "CheckIn Date is not same");
		
		verify.assertTrue(hotelResultsPage.compareBasePrice(2, hotelPrice), "Hotel Price is not compared");
		verify.assertTrue(hotelResultsPage.compareTaxPrice(2, tax), "Tax Price is not compared");
		verify.assertTrue(hotelResultsPage.comapareActualPrice(2, totalPrice), "Actual Price is not compared");
		verify.assertTrue(hotelResultsPage.checkRoomPriceOnFooter(hotelPrice), "Room Price not checked");
		verify.assertTrue(hotelResultsPage.checkTaxPriceOnFooter(tax), "Tax price on Footer not checked");
		verify.assertTrue(hotelResultsPage.checkTotalPriceOnFooter(totalPrice), "Total Price not  checked");
		verify.assertTrue(hotelResultsPage.isInclusiveTaxTextDisplayed(), "Inclusive Taxt is not dispalyed");
		verify.assertTrue(hotelResultsPage.isDoneButtonDisplayed(), "Done button is not displayed");
		hotelResultsPage.clickOnDone();
		verify.assertFalse(hotelResultsPage.isOpenPriceBreakUpWindow(), "Price BreakUp Window is opened");
		verify.assertAll();
	}

	// Commenting out and will create cancel booking job separately
	// @AfterMethod(alwaysRun = true)
	// public void cancelBooking() {
	// try {
	// homePage = new HomePage();
	// browserHelper = new BrowserHelper();
	// bookingHistory = new BookingHistory();
	// CommonUtils utils = new CommonUtils();
	// browserHelper.openUrl(browserHelper.getCurrentUrl());
	//
	// if (!homePage.isUserSignedIn()) {
	// System.out.println("User is not signed in! Doing sign-in to cancel
	// booking...");
	// homePage.signInAsTreeboMember(utils.getProperty("TestBookingAccount"),
	// utils.getProperty("TestLoginPassword"));
	// } else {
	// System.out.println("User is already signed in!");
	// }
	// bookingHistory.cancelAllBookings();
	// } catch (Exception e) {
	// e.printStackTrace();
	// }
	// }
}