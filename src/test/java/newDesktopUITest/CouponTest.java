package newDesktopUITest;

import java.text.ParseException;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import org.testng.Assert;

import utils.CommonUtils;
import base.BrowserHelper;
import base.TestBaseSetUp;
import desktopUIPageObjects.HomePage;
import desktopUIPageObjects.HotelResultsPage;
import desktopUIPageObjects.ItineraryPage;
import desktopUIPageObjects.HotelDetailsPage;

public class CouponTest extends TestBaseSetUp {

	private HomePage homePage;
	private HotelResultsPage hotelResultsPage;
	private ItineraryPage itineraryPage;
	private HotelDetailsPage hotelDetailsPage;
	private BrowserHelper browserHelper;
	
	/**
	 * This test verifies that coupon can be applied and removed
	 * In HD Page
	 * 
	 */
	@Test(groups = { "Coupon" } )
	public void testCouponCanBeAppliedAndRemovedOnHDPage() throws ParseException {
		homePage = new HomePage();
		hotelResultsPage = new HotelResultsPage();
		hotelDetailsPage = new HotelDetailsPage();
		CommonUtils utils = new CommonUtils();

		String location = utils.getProperty("Location"); // Bangalore
		String coupon = utils.getProperty("Coupon");
		String couponDiscountPercent = utils.getProperty("CouponDiscountPercent");

		// Home Page is Displayed
		homePage.verifyHomePagePresence();
		// Do search
		homePage.doSearch(location);
		hotelResultsPage.verifyHotelResultsPagePresence();

		int hotelIndex = hotelResultsPage.getHotelIndexByHotelAvailable();

		hotelResultsPage.clickHotelNameByIndex(hotelIndex);
		hotelDetailsPage.verifyHotelDetailsPagePresence();
		hotelDetailsPage.ensureRoomAvailableInHDPage();

		double[] hotelPriceInfoHD = hotelDetailsPage.getHotelPriceBreakUpInHD();
		int sumOfRateTaxDiscHDPage = (int) Math.round(hotelPriceInfoHD[0] + hotelPriceInfoHD[1] - hotelPriceInfoHD[2]);
		int hotelRoomRateInHD = hotelDetailsPage.getHotelTotalPriceInHD();
		//Before coupon applied, discount is zero
		Assert.assertEquals((int) hotelPriceInfoHD[2], 0, "Discount should be zero before applying coupon");
		Assert.assertEquals((int) Math.round(hotelPriceInfoHD[3]), sumOfRateTaxDiscHDPage,
				"Total and sum of room price, tax and discount does not match");
		Assert.assertEquals(hotelRoomRateInHD, (int) Math.round(hotelPriceInfoHD[3]),
				" Total Displayed in HD and in break up not same");

		// Apply coupon
		hotelDetailsPage.applyCouponInHDPage(coupon);
		int expectedCouponDiscount = (int) Math
				.round((hotelPriceInfoHD[0] * Integer.parseInt(couponDiscountPercent)) / 100);
		int displayedCouponDiscount = hotelDetailsPage.getCouponDiscountDisplayed();
		double[] discountedHotelPriceInfoHD = hotelDetailsPage.getHotelPriceBreakUpInHD();

		Assert.assertEquals(displayedCouponDiscount, expectedCouponDiscount,"Displayed coupon discount not same as expected");
		
		// Remove coupon
		hotelDetailsPage.removeCouponInHDPage();
		double[] hotelPriceInfoHDAfterCouponRemoved = hotelDetailsPage.getHotelPriceBreakUpInHD();
		//Discount should be zero
		Assert.assertEquals((int) hotelPriceInfoHDAfterCouponRemoved[2], 0,"After coupon removed it should have become zero");
		Assert.assertEquals((int) Math.round(hotelPriceInfoHDAfterCouponRemoved[0]),  (int) Math.round(hotelPriceInfoHD[0]),"After coupon removed room price should have return to original");
		Assert.assertEquals((int) Math.round(hotelPriceInfoHDAfterCouponRemoved[1]), (int) Math.round(hotelPriceInfoHD[1]),"After coupon removed room tax should have return to original");
	}

	
	/**
	 * This test verifies that coupon can be applied and removed
	 * in Itinerary page
	 */
	@Test(groups = { "Coupon" })
	public void testCouponCanBeAppliedAndRemovedOnItineraryPage() throws ParseException {
		homePage = new HomePage();
		hotelResultsPage = new HotelResultsPage();
		itineraryPage = new ItineraryPage();
		CommonUtils utils = new CommonUtils();
		browserHelper = new BrowserHelper();

		String location = utils.getProperty("Location"); // "India"; // "India";
		String coupon = utils.getProperty("Coupon");
		String couponDiscountPercent = utils.getProperty("CouponDiscountPercent");
		String testHotel = utils.getProperty("DummyHotel"); // "Dummy";
		String testUrl = utils.getProperty("TestUrl");
		int hotelIndex = 0;

		// Home Page is Displayed
		homePage.verifyHomePagePresence();
		// Do search
		homePage.doSearch(location);
		hotelResultsPage.verifyHotelResultsPagePresence();

		if (browserHelper.getCurrentUrl().contains(testUrl)) {
			hotelIndex = hotelResultsPage.getHotelIndexByHotelName(testHotel);
		} else {
			hotelIndex = hotelResultsPage.getHotelIndexByHotelAvailable();
		}
		hotelResultsPage.clickQuickBookByIndex(hotelIndex);
        itineraryPage.verifyItineraryPagePresence();
        
		double[] hotelPriceInfoItinearyPage = itineraryPage.getHotelPriceInfoInItineraryPage();

		// Apply coupon
		itineraryPage.applyCouponInItineraryPage(coupon);
		int expectedCouponDiscount = (int) Math
				.round((hotelPriceInfoItinearyPage[0] * Integer.parseInt(couponDiscountPercent)) / 100);
		int displayedCouponDiscount = itineraryPage.getCouponDiscountDisplayed();

		Assert.assertEquals(displayedCouponDiscount, expectedCouponDiscount,"Displayed coupon discount not same as expected");
		
		// Remove coupon
		itineraryPage.removeCouponInItineraryPage();
		double[] hotelPriceInfoAfterCouponRemoved = itineraryPage.getHotelPriceInfoInItineraryPage();
		//Discount should be zero
		Assert.assertEquals((int) hotelPriceInfoAfterCouponRemoved[2], 0,"After coupon removed it should have become zero");
		Assert.assertEquals((int) Math.round(hotelPriceInfoAfterCouponRemoved[0]),  (int) Math.round(hotelPriceInfoItinearyPage[0]),"After coupon removed room price should have return to original");
		Assert.assertEquals((int) Math.round(hotelPriceInfoAfterCouponRemoved[1]), (int) Math.round(hotelPriceInfoItinearyPage[1]),"After coupon removed room tax should have return to original");
	}
	
	/**
	 * This test verifies that invalid coupon cannot be applied on
	 * - HD Page
	 * - Itinerary Page
	 */
	@Test(groups = { "Coupon" } )
	public void testInvalidCouponError() throws ParseException {
		homePage = new HomePage();
		hotelResultsPage = new HotelResultsPage();
		hotelDetailsPage = new HotelDetailsPage();
		itineraryPage = new ItineraryPage();
		CommonUtils utils = new CommonUtils();
		browserHelper = new BrowserHelper();

		String location = utils.getProperty("Location"); 
		String invalidCoupon = utils.getProperty("InvalidCoupon");
		String testHotel = utils.getProperty("DummyHotel"); // "Dummy";
		String testUrl = utils.getProperty("TestUrl");
		int hotelIndex = 0;


		// Home Page is Displayed
		homePage.verifyHomePagePresence();
		// Do search
		homePage.doSearch(location);
		hotelResultsPage.verifyHotelResultsPagePresence();

		if (browserHelper.getCurrentUrl().contains(testUrl)) {
			hotelIndex = hotelResultsPage.getHotelIndexByHotelName(testHotel);
		} else {
			hotelIndex = hotelResultsPage.getHotelIndexByHotelAvailable();
		}
		
		//Go to Hotel Details Page
		hotelResultsPage.clickHotelNameByIndex(hotelIndex);
		hotelDetailsPage.verifyHotelDetailsPagePresence();
		hotelDetailsPage.ensureRoomAvailableInHDPage();

		// Apply coupon
		hotelDetailsPage.applyCouponInHDPage(invalidCoupon);
		hotelDetailsPage.verifyInvalidCouponError();

		//Go to Home page and then 
		hotelDetailsPage.goBackToSearch();
		hotelResultsPage.verifyHotelResultsPagePresence();
		hotelResultsPage.clickQuickBookByIndex(hotelIndex);
		
		// Try to apply invalid coupon
		itineraryPage.verifyItineraryPagePresence();
		itineraryPage.applyCouponInItineraryPage(invalidCoupon);
		itineraryPage.verifyInvalidCouponError();
	}
}
