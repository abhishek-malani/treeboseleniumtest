package mobileSiteUIPageObjects;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import base.BrowserHelper;
import base.DriverManager;
import utils.CommonUtils;


public class HotelSearchResultsScreen {
	private BrowserHelper browserHelper;
	private HomeScreen homeScreen;
	
	
	public boolean isHotelSearchScreenDisplayed(){
		WebDriver driver = DriverManager.getInstance().getDriver();
		homeScreen = new HomeScreen();
		browserHelper = new BrowserHelper();
		browserHelper.waitTime(1000);
		WebDriverWait wait = new WebDriverWait(driver, 120);
		
		//check if 500 page
		if (homeScreen.is500Displayed()){
			Assert.fail("500 displayed instead of search screen for url " + browserHelper.getCurrentUrl());
		}
		
		try{
			wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector("#commonLoader.hide")));
			wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(".result-page__title span")));
		}catch(Exception e){
			Assert.fail("Hotel search result screen not displayed for url " + browserHelper.getCurrentUrl());
		}
		
		return driver.findElement(By.cssSelector(".result-page__title span")).isDisplayed();
	}
	
	public boolean isHotelResultDisplayed(){
		WebDriver driver = DriverManager.getInstance().getDriver();
		return (driver.findElements(By.cssSelector("#searchResults .results__row a")).size() > 0);
	}
	
	public String getCitySearched(){
		WebDriver driver = DriverManager.getInstance().getDriver();
		homeScreen = new HomeScreen();
		browserHelper = new BrowserHelper();
		//check if 500 page
		if (homeScreen.is500Displayed()){
			Assert.fail("500 displayed instead of search screen for url " + browserHelper.getCurrentUrl());
		}
		
		return driver.findElement(By.cssSelector(".result-page__title span")).getText();
	}
	
	public String getCheckInDateSearched(){
		WebDriver driver = DriverManager.getInstance().getDriver();
		return driver.findElement(By.xpath("//div[@class='result-search__body']/div[@class='result-search__text'][1]/span[1]")).getText();
	}
	
	public String getCheckOutDateSearched(){
		WebDriver driver = DriverManager.getInstance().getDriver();
		return driver.findElement(By.xpath("//div[@class='result-search__body']/div[@class='result-search__text'][1]/span[2]")).getText();
	}
	
	public int getNumberOfNightsSearched(){
		WebDriver driver = DriverManager.getInstance().getDriver();
		return Integer.parseInt(driver.findElement(By.xpath("//div[@class='result-search__body']/div[@class='result-search__text'][2]/span[1]")).getText().split(" ")[0]);
	}
	
	public int getNumberOfGuestsSearched(){
		WebDriver driver = DriverManager.getInstance().getDriver();
		return Integer.parseInt(driver.findElement(By.xpath("//div[@class='result-search__body']/div[@class='result-search__text'][2]/span[3]")).getText().split(" ")[0]);
	}
	
	public int getNumberOfRoomsSearched(){
		WebDriver driver = DriverManager.getInstance().getDriver();
		return Integer.parseInt(driver.findElement(By.xpath("//div[@class='result-search__body']/div[@class='result-search__text'][2]/span[2]")).getText().split(" ")[0]);
	}
	
	public int getFilteredResultCount(){
		WebDriver driver = DriverManager.getInstance().getDriver();
		return Integer.parseInt(driver.findElement(By.cssSelector("#filteredResultCount")).getText());
	}
	
	public void clickHotelByIndex(int index){
		WebDriver driver = DriverManager.getInstance().getDriver();
		browserHelper = new BrowserHelper();
		String xpathHotelName = String.format("//div[@id='searchResults']/div[contains(@class,'results__row')][%d]//div[@class='hotel__name']", index);
		browserHelper.scrollToElement(driver.findElement(By.xpath(xpathHotelName)));
		driver.findElement(By.xpath(xpathHotelName)).click();
	}
	
	public void clickHotelByName(String hotelName){
		WebDriver driver = DriverManager.getInstance().getDriver();
		browserHelper = new BrowserHelper();
		String locatorHotelName = String.format("//a[contains(@hotelname,'%s')]", hotelName);
		browserHelper.scrollToElement(driver.findElement(By.xpath(locatorHotelName)));
		driver.findElement(By.xpath(locatorHotelName)).click();
	}
	
	public String getHotelIdByName(String hotelName){
		WebDriver driver = DriverManager.getInstance().getDriver();
		String xpath = String.format("//a[@hotelname='%s']/..", hotelName);
		return driver.findElement(By.xpath(xpath)).getAttribute("data-id");
	}
	
	public void clickQuickBookByIndex(int index){
		WebDriver driver = DriverManager.getInstance().getDriver();
		browserHelper = new BrowserHelper();
		String xpathHotelQuickBook = String.format("//div[@id='searchResults']/div[contains(@class,'results__row')][%d]//a[contains(@class,'analytics-quickbook')]", index);
		browserHelper.scrollToElement(driver.findElement(By.xpath(xpathHotelQuickBook)));
		driver.findElement(By.xpath(xpathHotelQuickBook)).click();
	}

	public void clickQuickBookByHotelName(String hotelName){
		WebDriver driver = DriverManager.getInstance().getDriver();
		browserHelper = new BrowserHelper();
		String xpathQuickBook = String.format("//a[contains(@class,'analytics-hotelname')][contains(@hotelname,'%s')]//a[contains(@class,'analytics-quickbook')]", hotelName);
		browserHelper.scrollToElement(driver.findElement(By.xpath(xpathQuickBook)));
		driver.findElement(By.xpath(xpathQuickBook)).click();
	}
	
	public boolean isHotelAvailable(String hotelName){
		WebDriver driver = DriverManager.getInstance().getDriver();
		String xpathQuickBook = String.format("//a[contains(@class,'analytics-hotelname')][contains(@hotelname,'%s')]//a[contains(@class,'analytics-quickbook')]", hotelName);
		return (driver.findElements(By.xpath(xpathQuickBook)).size() > 0);
	}
	
	public String getHotelNameByIndex(int index){
		WebDriver driver = DriverManager.getInstance().getDriver();
		String xpathHotelRoomRate = String.format("//div[@id='searchResults']/div[contains(@class,'results__row')][%d]/a[contains(@class,'analytics-hotelname')]", index);
		String hotelName = driver.findElement(By.xpath(xpathHotelRoomRate)).getAttribute("hotelname");
		return hotelName;
	}
	
	public int getRoomRateByIndex(int index){
		WebDriver driver = DriverManager.getInstance().getDriver();
		String xpathHotelRoomRate = String.format("//div[@id='searchResults']/div[contains(@class,'results__row')][%d]", index);
		int roomRate = Integer.parseInt(driver.findElement(By.xpath(xpathHotelRoomRate)).getAttribute("data-price"));
		return roomRate;
	}
	
	public int getCountOfTotalHotelDisplayed(){
		WebDriver driver = DriverManager.getInstance().getDriver();
		int numOfHotels = driver.findElements(By.xpath("//div[@id='searchResults']/div[contains(@class,'results__row')][not(contains(@style,'none'))]")).size();
		return numOfHotels;
	}
	
	public int getCountOfAvailableHotelDisplayed(){
		WebDriver driver = DriverManager.getInstance().getDriver();
		int numOfHotels = driver.findElements(By.xpath("//div[@id='searchResults']/div[contains(@class,'results__row')][@soldout='True']")).size();
		return numOfHotels;
	}
	
	public int getCountOfSoldOutHotelDisplayed(){
		WebDriver driver = DriverManager.getInstance().getDriver();
		int numOfHotels = driver.findElements(By.xpath("//div[@id='searchResults']/div[contains(@class,'results__row')][@soldout='False']")).size();
		return numOfHotels;
	}
	
	public int getRandomIndexOfAvailableHotel(){
		CommonUtils utils = new CommonUtils();
		int count = getCountOfAvailableHotelDisplayed();
		return utils.getRandomNumber(1, count);
	}
	
	public int getRandomIndexOfSoldOutHotel(){
		CommonUtils utils = new CommonUtils();
		int count = getCountOfSoldOutHotelDisplayed();
		return utils.getRandomNumber(0, count-1);
	}
	
	public int[] getRoomRatesOfAllHotel(){
		WebDriver driver = DriverManager.getInstance().getDriver();
		int[] roomRates = new int[getCountOfTotalHotelDisplayed()];
		List<WebElement> elements = driver.findElements(By.xpath("//div[@id='searchResults']/div[contains(@class,'results__row')][not(contains(@style,'none'))]"));
		int i = 0;
		for (WebElement ele: elements){
			roomRates[i] = Integer.parseInt(ele.getAttribute("data-price"));
			i = i + 1;
		}
		System.out.println("Room Rates of All hotel Displayed :" + Arrays.toString(roomRates));
		return roomRates;
	}
	
	public int[] getRoomRatesOfAvailableHotel(){
		WebDriver driver = DriverManager.getInstance().getDriver();
		int[] roomRatesAvailable = new int[getCountOfAvailableHotelDisplayed()];
		List<WebElement> elements = driver.findElements(By.xpath("//div[@id='searchResults']/div[contains(@class,'results__row')][@soldout='True']"));
		int i = 0;
		for (WebElement ele: elements){
			roomRatesAvailable[i] = Integer.parseInt(ele.getAttribute("data-price"));
			i = i + 1;
		}
		System.out.println("Room Rates of All Available hotel Displayed :" + Arrays.toString(roomRatesAvailable));
		return roomRatesAvailable;
	}
	
	public int[] getRoomRatesOfSoldOutHotel(){
		WebDriver driver = DriverManager.getInstance().getDriver();
		int[] roomRatesSoldOut = new int[getCountOfSoldOutHotelDisplayed()];
		List<WebElement> elements = driver.findElements(By.xpath("//div[@id='searchResults']/div[contains(@class,'results__row')][@soldout='False']"));
		int i = 0;
		for (WebElement ele: elements){
			roomRatesSoldOut[i] = Integer.parseInt(ele.getAttribute("data-price"));
			i = i + 1;
		}
		System.out.println("Room Rates of All Sold Out hotel Displayed :" + Arrays.toString(roomRatesSoldOut));
		return roomRatesSoldOut;
	}
	
	public List<String> getAllHotelNames(){
		WebDriver driver = DriverManager.getInstance().getDriver();
		List<String> hotelNames = new ArrayList<String>();
		List<WebElement> elements = driver.findElements(By.xpath("//div[@id='searchResults']/div[contains(@class,'results__row')]/a[contains(@class,'analytics-hotelname')]"));
		for (WebElement ele: elements){
			hotelNames.add(ele.getAttribute("hotelname"));
		}
		System.out.println("Room Rates of All Available hotel Displayed :" + hotelNames.toString());
		return hotelNames;
	}
	
	public List<String> getAllAvailableHotelNames(){
		WebDriver driver = DriverManager.getInstance().getDriver();
		List<String> availableHotelNames = new ArrayList<String>();
		List<WebElement> elements = driver.findElements(By.xpath("//div[@id='searchResults']/div[contains(@class,'results__row')][@soldout='True']/a[contains(@class,'analytics-hotelname')]"));
		for (WebElement ele: elements){
			availableHotelNames.add(ele.getAttribute("hotelname"));
		}
		System.out.println("Room Rates of All Available hotel Displayed :" + availableHotelNames.toString());
		return availableHotelNames;
	}
	
	public List<String> getAllSoldOutHotelNames(){
		WebDriver driver = DriverManager.getInstance().getDriver();
		List<String> solOutHotelNames = new ArrayList<String>();
		List<WebElement> elements = driver.findElements(By.xpath("//div[@id='searchResults']/div[contains(@class,'results__row')][@soldout='False']/a[contains(@class,'analytics-hotelname')]"));
		for (WebElement ele: elements){
			solOutHotelNames.add(ele.getAttribute("hotelname"));
		}
		System.out.println("Room Rates of All Available hotel Displayed :" + solOutHotelNames.toString());
		return solOutHotelNames;
	}
	
	public String[] modifySearch(String cityName){
		WebDriver driver = DriverManager.getInstance().getDriver();
		homeScreen = new HomeScreen();
		driver.findElement(By.cssSelector("#modifySearch")).click();
		String[] checkInCheckOutDates = homeScreen.doSearch(cityName);
		return checkInCheckOutDates;
	}
	
	public String[] modifySearch(String cityName,int checkin, int checkout){
		WebDriver driver = DriverManager.getInstance().getDriver();
		homeScreen = new HomeScreen();
		driver.findElement(By.cssSelector("#modifySearch")).click();
		String[] checkInCheckOutDates = homeScreen.doSearch(cityName, checkin, checkout);
		return checkInCheckOutDates;
	}
	
	public String[] modifySearch(String cityName, int checkin, int checkout, int[][] roomAdultChild){
		WebDriver driver = DriverManager.getInstance().getDriver();
		homeScreen = new HomeScreen();
		driver.findElement(By.cssSelector("#modifySearch")).click();
		String[] checkInCheckOutDates = homeScreen.doSearch(cityName, checkin, checkout, roomAdultChild);
		return checkInCheckOutDates;
	}
	
	public void openModifyWidget(){
		WebDriver driver = DriverManager.getInstance().getDriver();
		driver.findElement(By.cssSelector("#modifySearch")).click();
	}
	
	public boolean isModifyWidgetOpen(){
		WebDriver driver = DriverManager.getInstance().getDriver();
		return (!(driver.findElements(By.cssSelector(".modal--search.hide")).size() > 0));
	}
	
	public void showOnlyAvailable(){
		WebDriver driver = DriverManager.getInstance().getDriver();
		driver.findElement(By.cssSelector(".filters__title--text")).click();
		driver.findElement(By.cssSelector("input[name='showOnlyAvailable']")).click();
		driver.findElement(By.cssSelector(".filters__actions__apply")).click();
	}
	
	public void applyLocationFilter(){
		WebDriver driver = DriverManager.getInstance().getDriver();
		driver.findElement(By.cssSelector("input[value='Electronic city']")).click();
		driver.findElement(By.cssSelector(".filters__actions__apply")).click();
	}
	
	public void openFilterScreen(){
		WebDriver driver = DriverManager.getInstance().getDriver();
		driver.findElement(By.cssSelector(".filters__title--text")).click();
	}
	
	public void applyPriceFilter(){
		WebDriver driver = DriverManager.getInstance().getDriver();
		driver.findElement(By.cssSelector(".filters__title--text")).click();
		driver.findElement(By.cssSelector("input[name='1501,3000']")).click();
		driver.findElement(By.cssSelector(".filters__actions__apply")).click();
	}
	
	public void applyAmenitiesFilter(){
		WebDriver driver = DriverManager.getInstance().getDriver();
		driver.findElement(By.cssSelector("input[value='Travel Desk']")).click();
		driver.findElement(By.cssSelector(".filters__actions__apply")).click();
	}
	
	public void clearAllFilters(){
		WebDriver driver = DriverManager.getInstance().getDriver();
		driver.findElement(By.cssSelector(".filters__title--text")).click();
		driver.findElement(By.cssSelector(".anchor.filters__actions__clear.lfloat")).click();
		driver.findElement(By.cssSelector(".filters__actions__apply")).click();
	}
	
	public boolean isFiltersOpen(){
		WebDriver driver = DriverManager.getInstance().getDriver();
	    return (!(driver.findElements(By.cssSelector("#filtersDd.hide")).size() > 0));
	}
	
	public void closeFilters(){
		WebDriver driver = DriverManager.getInstance().getDriver();
		driver.findElement(By.cssSelector(".filters__header .icon-cross")).click();
	}
	
	public void closeModifyWidget(){
		WebDriver driver = DriverManager.getInstance().getDriver();
		driver.findElement(By.xpath("//div[@id='searchModal'][not(contains(@class,'hide'))]//i[@class='icon-cross']")).click();
	}
	
	public boolean isSeoCityContentDisplayed(){
		WebDriver driver = DriverManager.getInstance().getDriver();
		return driver.findElement(By.cssSelector(".seo-city")).isDisplayed();
	}
	public int convertStringToInteger(String str){
		float value1= Float.parseFloat(str);
		int value = (int)(value1);
		return value;
	}
	public int extractGrandTotal(){
		WebDriver driver = DriverManager.getInstance().getDriver();
		WebElement element = driver.findElement(By.cssSelector("#grandTotal"));
		String str1 = element.getText().trim();
		int total = convertStringToInteger(str1);
		return total;
	}
	public int extractPriceUsingHotelName(){
		WebDriver driver = DriverManager.getInstance().getDriver();
		WebElement element = driver.findElement(By.cssSelector("#modify-widget-price"));
		String str1 = element.getText().trim();
		int total2 = convertStringToInteger(str1);
		return total2;
	}
	public void clickOnBackButton(){
		WebDriver driver = DriverManager.getInstance().getDriver();
		//WebElement element = driver.findElement(By.cssSelector(".back.js-back-link.sub-heading.itinerary-steps__heading"));
		WebDriverWait wait = new WebDriverWait(driver, 120);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector(".back.js-back-link.sub-heading.itinerary-steps__heading")));
		driver.findElement(By.cssSelector(".back.js-back-link.sub-heading.itinerary-steps__heading")).click();
	}
	public boolean isEqualAllPrices(int n1, int n2, int n3){
		if((n1==n2)&&(n2==n3)&&(n3==n1))
			return true;
		return false;
	}
	
}
