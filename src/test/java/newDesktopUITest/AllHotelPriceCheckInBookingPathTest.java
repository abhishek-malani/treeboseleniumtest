package newDesktopUITest;

import java.text.ParseException;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import base.BrowserHelper;
import base.TestBaseSetUp;
import desktopUIPageObjects.BookingHistory;
import desktopUIPageObjects.ConfirmationPage;
import desktopUIPageObjects.HomePage;
import desktopUIPageObjects.HotelDetailsPage;
import desktopUIPageObjects.HotelResultsPage;
import desktopUIPageObjects.ItineraryPage;
import org.testng.Assert;
import utils.CommonUtils;

public class AllHotelPriceCheckInBookingPathTest extends TestBaseSetUp {

	private HomePage homePage;
	private HotelResultsPage hotelResultsPage;
	private ItineraryPage itineraryPage;
	private ConfirmationPage confirmationPage;
	private HotelDetailsPage hotelDetailsPage;
	private BookingHistory bookingHistory;
	private BrowserHelper browserHelper;

/***
 * This test checks that in Prod for every hotel Hotel Details page loads
 * And if default room is available
 * @throws InterruptedException 
 */
	@Test(groups = { "ProdPriceCheck" })
	public void testAllHotelPriceCheckInBookingPathBengaluru() throws InterruptedException{
	    homePage = new HomePage();
	    hotelResultsPage = new HotelResultsPage();
	    hotelDetailsPage = new HotelDetailsPage();
	    itineraryPage = new ItineraryPage();
	    browserHelper = new BrowserHelper();
	    CommonUtils utils = new CommonUtils();
	    SoftAssert verify = new SoftAssert();
	    
	    String location = "Bengaluru";
	    int checkInFromCurrentDate = utils.getRandomNumber(7, 30);
	    int checkOutFromCurrentDate = checkInFromCurrentDate + 1;
	    int roomIndex = 1;
	    
	    String[] checkInCheckOutDates = homePage.doSearch(location, checkInFromCurrentDate, checkOutFromCurrentDate, roomIndex);
	    Thread.sleep(60000);
	    hotelResultsPage.verifyHotelResultsPagePresence();
	    String searchPageUrl = browserHelper.getCurrentUrl();
	    String[] hotelNames = hotelResultsPage.getDisplayedHotelNames();
	    
	    int i = 1;
	    for (String hotelName : hotelNames){
	    	System.out.println("Currently checking for "+ i + " st/th "+" Hotel: " + hotelName);
	    	int hotelIndex = hotelResultsPage.getHotelIndexByHotelName(hotelName);
			int[] hotelPriceInSearch = hotelResultsPage.getHotelPriceBreakUpByIndex(hotelIndex);
			int roomRateInSearch = Integer.parseInt(hotelResultsPage.getHotelRoomRateByIndex(hotelIndex));
			int sumInSearch = hotelPriceInSearch[0] + hotelPriceInSearch[1] - hotelPriceInSearch[2];
			verify.assertTrue(Math.abs(hotelPriceInSearch[3] - sumInSearch) <= 1, "For Hotel : " + hotelName + " Dates : " + checkInCheckOutDates[0] +" and " + checkInCheckOutDates[1] +" Total " + hotelPriceInSearch[3]
					+ ". is not same as sum of room price, tax and discount" + sumInSearch);
			verify.assertTrue(Math.abs(roomRateInSearch - sumInSearch) <= 1, "For Hotel : " + hotelName + " Dates : " + checkInCheckOutDates[0] +" and " + checkInCheckOutDates[1] +" displayed room rate: " + roomRateInSearch + " and sum: " + sumInSearch + "in price break up is not same!!!");
			
	    	hotelResultsPage.clickHotelNameByIndex(hotelIndex);
	    	hotelDetailsPage.verifyHotelDetailsPagePresence();
	    	verify.assertEquals(hotelDetailsPage.getHotelNameInHotelDetailsPage().toLowerCase(),hotelName.toLowerCase());
	    	
			double[] hotelPriceInfoHD = hotelDetailsPage.getHotelPriceBreakUpInHD();
			int sumOfRateTaxDiscHDPage = (int) Math.round(hotelPriceInfoHD[0] + hotelPriceInfoHD[1] - hotelPriceInfoHD[2]);
			int hotelRoomRateInHD = hotelDetailsPage.getHotelTotalPriceInHD();
			verify.assertTrue(Math.abs((int) Math.round(hotelPriceInfoHD[3]) - sumOfRateTaxDiscHDPage) <= 1,
					"For Hotel : " + hotelName + " Dates : " + checkInCheckOutDates[0] +" and " + checkInCheckOutDates[1] +" Total : " + (int) Math.round(hotelPriceInfoHD[3]) + " and sum of room price, tax and discount :"
							+ sumOfRateTaxDiscHDPage + " does not match");
			verify.assertTrue(Math.abs(hotelRoomRateInHD - (int) Math.round(hotelPriceInfoHD[3])) <= 1,
					"For Hotel : " + hotelName + " Dates : " + checkInCheckOutDates[0] +" and " + checkInCheckOutDates[1] +" Total Displayed in HD :" + hotelRoomRateInHD + " and in break up :"
							+ (int) Math.round(hotelPriceInfoHD[3]) + " not same");
			verify.assertTrue(Math.abs(hotelPriceInSearch[0] - (int) Math.round(hotelPriceInfoHD[0])) <= 1,
					"For Hotel : " + hotelName + " Dates : " + checkInCheckOutDates[0] +" and " + checkInCheckOutDates[1] +" Room rate in Search page :" + hotelPriceInSearch[0] + " and HD Page :"
							+ (int) Math.round(hotelPriceInfoHD[0]) + " is not same");
			verify.assertTrue(Math.abs(hotelPriceInSearch[1] - (int) Math.round(hotelPriceInfoHD[1])) <= 1,
					"For Hotel : " + hotelName + " Dates : " + checkInCheckOutDates[0] +" and " + checkInCheckOutDates[1] +" Taxes in Search page: " + hotelPriceInSearch[1] + " and HD Page "
							+ (int) Math.round(hotelPriceInfoHD[1]) + " is not same");
			verify.assertTrue(Math.abs(hotelPriceInSearch[2] - (int) Math.round(hotelPriceInfoHD[2])) <= 1,
					"For Hotel : " + hotelName + " Dates : " + checkInCheckOutDates[0] +" and " + checkInCheckOutDates[1] +" Discount in Search page :" + hotelPriceInSearch[2] + " and HD Page :"
							+ (int) Math.round(hotelPriceInfoHD[2]) + " is not same");
			verify.assertTrue(Math.abs(hotelPriceInSearch[3] - (int) Math.round(hotelPriceInfoHD[3])) <= 1,
					"For Hotel : " + hotelName + " Dates : " + checkInCheckOutDates[0] +" and " + checkInCheckOutDates[1] +" Total in Search page : " + hotelPriceInSearch[3] + " and HD Page :"
							+ (int) Math.round(hotelPriceInfoHD[3]) + " is not same");
	    	
	    	browserHelper.openUrl(searchPageUrl);
	    	i = i + 1;
	    }
	    verify.assertAll();
	}
	
	@Test(groups = { "ProdPriceCheck" })
	public void testAllHotelPriceCheckInBookingPathHyderabad(){
	    homePage = new HomePage();
	    hotelResultsPage = new HotelResultsPage();
	    hotelDetailsPage = new HotelDetailsPage();
	    itineraryPage = new ItineraryPage();
	    browserHelper = new BrowserHelper();
	    CommonUtils utils = new CommonUtils();
	    SoftAssert verify = new SoftAssert();
	    
	    String location = "Hyderabad";
	    int checkInFromCurrentDate = utils.getRandomNumber(7, 30);
	    int checkOutFromCurrentDate = checkInFromCurrentDate + 1;
	    int roomIndex = 1;
	    
	    String[] checkInCheckOutDates = homePage.doSearch(location, checkInFromCurrentDate, checkOutFromCurrentDate, roomIndex);
	    hotelResultsPage.verifyHotelResultsPagePresence();
	    String searchPageUrl = browserHelper.getCurrentUrl();
	    String[] hotelNames = hotelResultsPage.getDisplayedHotelNames();
	    
	    int i = 1;
	    for (String hotelName : hotelNames){
	    	System.out.println("Currently checking for "+ i + " st/th "+" Hotel: " + hotelName);
	    	int hotelIndex = hotelResultsPage.getHotelIndexByHotelName(hotelName);
			int[] hotelPriceInSearch = hotelResultsPage.getHotelPriceBreakUpByIndex(hotelIndex);
			int roomRateInSearch = Integer.parseInt(hotelResultsPage.getHotelRoomRateByIndex(hotelIndex));
			int sumInSearch = hotelPriceInSearch[0] + hotelPriceInSearch[1] - hotelPriceInSearch[2];
			verify.assertTrue(Math.abs(hotelPriceInSearch[3] - sumInSearch) <= 1, "For Hotel : " + hotelName + " Dates : " + checkInCheckOutDates[0] +" and " + checkInCheckOutDates[1] +" Total " + hotelPriceInSearch[3]
					+ ". is not same as sum of room price, tax and discount" + sumInSearch);
			verify.assertTrue(Math.abs(roomRateInSearch - sumInSearch) <= 1, "For Hotel : " + hotelName + " Dates : " + checkInCheckOutDates[0] +" and " + checkInCheckOutDates[1] +" displayed room rate: " + roomRateInSearch + " and sum: " + sumInSearch + "in price break up is not same!!!");
			
	    	hotelResultsPage.clickHotelNameByIndex(hotelIndex);
	    	hotelDetailsPage.verifyHotelDetailsPagePresence();
	    	verify.assertEquals(hotelDetailsPage.getHotelNameInHotelDetailsPage().toLowerCase(),hotelName.toLowerCase());
	    	
			double[] hotelPriceInfoHD = hotelDetailsPage.getHotelPriceBreakUpInHD();
			int sumOfRateTaxDiscHDPage = (int) Math.round(hotelPriceInfoHD[0] + hotelPriceInfoHD[1] - hotelPriceInfoHD[2]);
			int hotelRoomRateInHD = hotelDetailsPage.getHotelTotalPriceInHD();
			verify.assertTrue(Math.abs((int) Math.round(hotelPriceInfoHD[3]) - sumOfRateTaxDiscHDPage) <= 1,
					"For Hotel : " + hotelName + " Dates : " + checkInCheckOutDates[0] +" and " + checkInCheckOutDates[1] +" Total : " + (int) Math.round(hotelPriceInfoHD[3]) + " and sum of room price, tax and discount :"
							+ sumOfRateTaxDiscHDPage + " does not match");
			verify.assertTrue(Math.abs(hotelRoomRateInHD - (int) Math.round(hotelPriceInfoHD[3])) <= 1,
					"For Hotel : " + hotelName + " Dates : " + checkInCheckOutDates[0] +" and " + checkInCheckOutDates[1] +" Total Displayed in HD :" + hotelRoomRateInHD + " and in break up :"
							+ (int) Math.round(hotelPriceInfoHD[3]) + " not same");
			verify.assertTrue(Math.abs(hotelPriceInSearch[0] - (int) Math.round(hotelPriceInfoHD[0])) <= 1,
					"For Hotel : " + hotelName + " Dates : " + checkInCheckOutDates[0] +" and " + checkInCheckOutDates[1] +" Room rate in Search page :" + hotelPriceInSearch[0] + " and HD Page :"
							+ (int) Math.round(hotelPriceInfoHD[0]) + " is not same");
			verify.assertTrue(Math.abs(hotelPriceInSearch[1] - (int) Math.round(hotelPriceInfoHD[1])) <= 1,
					"For Hotel : " + hotelName + " Dates : " + checkInCheckOutDates[0] +" and " + checkInCheckOutDates[1] +" Taxes in Search page: " + hotelPriceInSearch[1] + " and HD Page "
							+ (int) Math.round(hotelPriceInfoHD[1]) + " is not same");
			verify.assertTrue(Math.abs(hotelPriceInSearch[2] - (int) Math.round(hotelPriceInfoHD[2])) <= 1,
					"For Hotel : " + hotelName + " Dates : " + checkInCheckOutDates[0] +" and " + checkInCheckOutDates[1] +" Discount in Search page :" + hotelPriceInSearch[2] + " and HD Page :"
							+ (int) Math.round(hotelPriceInfoHD[2]) + " is not same");
			verify.assertTrue(Math.abs(hotelPriceInSearch[3] - (int) Math.round(hotelPriceInfoHD[3])) <= 1,
					"For Hotel : " + hotelName + " Dates : " + checkInCheckOutDates[0] +" and " + checkInCheckOutDates[1] +" Total in Search page : " + hotelPriceInSearch[3] + " and HD Page :"
							+ (int) Math.round(hotelPriceInfoHD[3]) + " is not same");
	    	
	    	browserHelper.openUrl(searchPageUrl);
	    	i = i + 1;
	    }
	    verify.assertAll();
	}
	
	@Test(groups = { "ProdPriceCheck" })
	public void testAllHotelPriceCheckInBookingPathMumbai(){
	    homePage = new HomePage();
	    hotelResultsPage = new HotelResultsPage();
	    hotelDetailsPage = new HotelDetailsPage();
	    itineraryPage = new ItineraryPage();
	    browserHelper = new BrowserHelper();
	    CommonUtils utils = new CommonUtils();
	    SoftAssert verify = new SoftAssert();
	    
	    String location = "Mumbai";
	    int checkInFromCurrentDate = utils.getRandomNumber(7, 30);
	    int checkOutFromCurrentDate = checkInFromCurrentDate + 1;
	    int roomIndex = 1;
	    
	    String[] checkInCheckOutDates = homePage.doSearch(location, checkInFromCurrentDate, checkOutFromCurrentDate, roomIndex);
	    hotelResultsPage.verifyHotelResultsPagePresence();
	    String searchPageUrl = browserHelper.getCurrentUrl();
	    String[] hotelNames = hotelResultsPage.getDisplayedHotelNames();
	    
	    int i = 1;
	    for (String hotelName : hotelNames){
	    	System.out.println("Currently checking for "+ i + " st/th "+" Hotel: " + hotelName);
	    	int hotelIndex = hotelResultsPage.getHotelIndexByHotelName(hotelName);
			int[] hotelPriceInSearch = hotelResultsPage.getHotelPriceBreakUpByIndex(hotelIndex);
			int roomRateInSearch = Integer.parseInt(hotelResultsPage.getHotelRoomRateByIndex(hotelIndex));
			int sumInSearch = hotelPriceInSearch[0] + hotelPriceInSearch[1] - hotelPriceInSearch[2];
			verify.assertTrue(Math.abs(hotelPriceInSearch[3] - sumInSearch) <= 1, "For Hotel : " + hotelName + " Dates : " + checkInCheckOutDates[0] +" and " + checkInCheckOutDates[1] +" Total " + hotelPriceInSearch[3]
					+ ". is not same as sum of room price, tax and discount" + sumInSearch);
			verify.assertTrue(Math.abs(roomRateInSearch - sumInSearch) <= 1, "For Hotel : " + hotelName + " Dates : " + checkInCheckOutDates[0] +" and " + checkInCheckOutDates[1] +" displayed room rate: " + roomRateInSearch + " and sum: " + sumInSearch + "in price break up is not same!!!");
			
	    	hotelResultsPage.clickHotelNameByIndex(hotelIndex);
	    	hotelDetailsPage.verifyHotelDetailsPagePresence();
	    	verify.assertEquals(hotelDetailsPage.getHotelNameInHotelDetailsPage().toLowerCase(),hotelName.toLowerCase());
	    	
			double[] hotelPriceInfoHD = hotelDetailsPage.getHotelPriceBreakUpInHD();
			int sumOfRateTaxDiscHDPage = (int) Math.round(hotelPriceInfoHD[0] + hotelPriceInfoHD[1] - hotelPriceInfoHD[2]);
			int hotelRoomRateInHD = hotelDetailsPage.getHotelTotalPriceInHD();
			verify.assertTrue(Math.abs((int) Math.round(hotelPriceInfoHD[3]) - sumOfRateTaxDiscHDPage) <= 1,
					"For Hotel : " + hotelName + " Dates : " + checkInCheckOutDates[0] +" and " + checkInCheckOutDates[1] +" Total : " + (int) Math.round(hotelPriceInfoHD[3]) + " and sum of room price, tax and discount :"
							+ sumOfRateTaxDiscHDPage + " does not match");
			verify.assertTrue(Math.abs(hotelRoomRateInHD - (int) Math.round(hotelPriceInfoHD[3])) <= 1,
					"For Hotel : " + hotelName + " Dates : " + checkInCheckOutDates[0] +" and " + checkInCheckOutDates[1] +" Total Displayed in HD :" + hotelRoomRateInHD + " and in break up :"
							+ (int) Math.round(hotelPriceInfoHD[3]) + " not same");
			verify.assertTrue(Math.abs(hotelPriceInSearch[0] - (int) Math.round(hotelPriceInfoHD[0])) <= 1,
					"For Hotel : " + hotelName + " Dates : " + checkInCheckOutDates[0] +" and " + checkInCheckOutDates[1] +" Room rate in Search page :" + hotelPriceInSearch[0] + " and HD Page :"
							+ (int) Math.round(hotelPriceInfoHD[0]) + " is not same");
			verify.assertTrue(Math.abs(hotelPriceInSearch[1] - (int) Math.round(hotelPriceInfoHD[1])) <= 1,
					"For Hotel : " + hotelName + " Dates : " + checkInCheckOutDates[0] +" and " + checkInCheckOutDates[1] +" Taxes in Search page: " + hotelPriceInSearch[1] + " and HD Page "
							+ (int) Math.round(hotelPriceInfoHD[1]) + " is not same");
			verify.assertTrue(Math.abs(hotelPriceInSearch[2] - (int) Math.round(hotelPriceInfoHD[2])) <= 1,
					"For Hotel : " + hotelName + " Dates : " + checkInCheckOutDates[0] +" and " + checkInCheckOutDates[1] +" Discount in Search page :" + hotelPriceInSearch[2] + " and HD Page :"
							+ (int) Math.round(hotelPriceInfoHD[2]) + " is not same");
			verify.assertTrue(Math.abs(hotelPriceInSearch[3] - (int) Math.round(hotelPriceInfoHD[3])) <= 1,
					"For Hotel : " + hotelName + " Dates : " + checkInCheckOutDates[0] +" and " + checkInCheckOutDates[1] +" Total in Search page : " + hotelPriceInSearch[3] + " and HD Page :"
							+ (int) Math.round(hotelPriceInfoHD[3]) + " is not same");
	    	
	    	browserHelper.openUrl(searchPageUrl);
	    	i = i + 1;
	    }
	    verify.assertAll();
	}
	
	@Test(groups = { "ProdPriceCheck" })
	public void testAllHotelPriceCheckInBookingPathChennai(){
	    homePage = new HomePage();
	    hotelResultsPage = new HotelResultsPage();
	    hotelDetailsPage = new HotelDetailsPage();
	    itineraryPage = new ItineraryPage();
	    browserHelper = new BrowserHelper();
	    CommonUtils utils = new CommonUtils();
	    SoftAssert verify = new SoftAssert();
	    
	    String location = "Chennai";
	    int checkInFromCurrentDate = utils.getRandomNumber(7, 30);
	    int checkOutFromCurrentDate = checkInFromCurrentDate + 1;
	    int roomIndex = 1;
	    
	    String[] checkInCheckOutDates = homePage.doSearch(location, checkInFromCurrentDate, checkOutFromCurrentDate, roomIndex);
	    hotelResultsPage.verifyHotelResultsPagePresence();
	    String searchPageUrl = browserHelper.getCurrentUrl();
	    String[] hotelNames = hotelResultsPage.getDisplayedHotelNames();
	    
	    int i = 1;
	    for (String hotelName : hotelNames){
	    	System.out.println("Currently checking for "+ i + " st/th "+" Hotel: " + hotelName);
	    	int hotelIndex = hotelResultsPage.getHotelIndexByHotelName(hotelName);
			int[] hotelPriceInSearch = hotelResultsPage.getHotelPriceBreakUpByIndex(hotelIndex);
			int roomRateInSearch = Integer.parseInt(hotelResultsPage.getHotelRoomRateByIndex(hotelIndex));
			int sumInSearch = hotelPriceInSearch[0] + hotelPriceInSearch[1] - hotelPriceInSearch[2];
			verify.assertTrue(Math.abs(hotelPriceInSearch[3] - sumInSearch) <= 1, "For Hotel : " + hotelName + " Dates : " + checkInCheckOutDates[0] +" and " + checkInCheckOutDates[1] +" Total " + hotelPriceInSearch[3]
					+ ". is not same as sum of room price, tax and discount" + sumInSearch);
			verify.assertTrue(Math.abs(roomRateInSearch - sumInSearch) <= 1, "For Hotel : " + hotelName + " Dates : " + checkInCheckOutDates[0] +" and " + checkInCheckOutDates[1] +" displayed room rate: " + roomRateInSearch + " and sum: " + sumInSearch + "in price break up is not same!!!");
			
	    	hotelResultsPage.clickHotelNameByIndex(hotelIndex);
	    	hotelDetailsPage.verifyHotelDetailsPagePresence();
	    	verify.assertEquals(hotelDetailsPage.getHotelNameInHotelDetailsPage().toLowerCase(),hotelName.toLowerCase());
	    	
			double[] hotelPriceInfoHD = hotelDetailsPage.getHotelPriceBreakUpInHD();
			int sumOfRateTaxDiscHDPage = (int) Math.round(hotelPriceInfoHD[0] + hotelPriceInfoHD[1] - hotelPriceInfoHD[2]);
			int hotelRoomRateInHD = hotelDetailsPage.getHotelTotalPriceInHD();
			verify.assertTrue(Math.abs((int) Math.round(hotelPriceInfoHD[3]) - sumOfRateTaxDiscHDPage) <= 1,
					"For Hotel : " + hotelName + " Dates : " + checkInCheckOutDates[0] +" and " + checkInCheckOutDates[1] +" Total : " + (int) Math.round(hotelPriceInfoHD[3]) + " and sum of room price, tax and discount :"
							+ sumOfRateTaxDiscHDPage + " does not match");
			verify.assertTrue(Math.abs(hotelRoomRateInHD - (int) Math.round(hotelPriceInfoHD[3])) <= 1,
					"For Hotel : " + hotelName + " Dates : " + checkInCheckOutDates[0] +" and " + checkInCheckOutDates[1] +" Total Displayed in HD :" + hotelRoomRateInHD + " and in break up :"
							+ (int) Math.round(hotelPriceInfoHD[3]) + " not same");
			verify.assertTrue(Math.abs(hotelPriceInSearch[0] - (int) Math.round(hotelPriceInfoHD[0])) <= 1,
					"For Hotel : " + hotelName + " Dates : " + checkInCheckOutDates[0] +" and " + checkInCheckOutDates[1] +" Room rate in Search page :" + hotelPriceInSearch[0] + " and HD Page :"
							+ (int) Math.round(hotelPriceInfoHD[0]) + " is not same");
			verify.assertTrue(Math.abs(hotelPriceInSearch[1] - (int) Math.round(hotelPriceInfoHD[1])) <= 1,
					"For Hotel : " + hotelName + " Dates : " + checkInCheckOutDates[0] +" and " + checkInCheckOutDates[1] +" Taxes in Search page: " + hotelPriceInSearch[1] + " and HD Page "
							+ (int) Math.round(hotelPriceInfoHD[1]) + " is not same");
			verify.assertTrue(Math.abs(hotelPriceInSearch[2] - (int) Math.round(hotelPriceInfoHD[2])) <= 1,
					"For Hotel : " + hotelName + " Dates : " + checkInCheckOutDates[0] +" and " + checkInCheckOutDates[1] +" Discount in Search page :" + hotelPriceInSearch[2] + " and HD Page :"
							+ (int) Math.round(hotelPriceInfoHD[2]) + " is not same");
			verify.assertTrue(Math.abs(hotelPriceInSearch[3] - (int) Math.round(hotelPriceInfoHD[3])) <= 1,
					"For Hotel : " + hotelName + " Dates : " + checkInCheckOutDates[0] +" and " + checkInCheckOutDates[1] +" Total in Search page : " + hotelPriceInSearch[3] + " and HD Page :"
							+ (int) Math.round(hotelPriceInfoHD[3]) + " is not same");
	    	
	    	browserHelper.openUrl(searchPageUrl);
	    	i = i + 1;
	    }
	    verify.assertAll();
	}
	
	@Test(groups = { "ProdPriceCheck" })
	public void testAllHotelPriceCheckInBookingPathJaipur(){
	    homePage = new HomePage();
	    hotelResultsPage = new HotelResultsPage();
	    hotelDetailsPage = new HotelDetailsPage();
	    itineraryPage = new ItineraryPage();
	    browserHelper = new BrowserHelper();
	    CommonUtils utils = new CommonUtils();
	    SoftAssert verify = new SoftAssert();
	    
	    String location = "Jaipur";
	    int checkInFromCurrentDate = utils.getRandomNumber(7, 30);
	    int checkOutFromCurrentDate = checkInFromCurrentDate + 1;
	    int roomIndex = 1;
	    
	    String[] checkInCheckOutDates = homePage.doSearch(location, checkInFromCurrentDate, checkOutFromCurrentDate, roomIndex);
	    hotelResultsPage.verifyHotelResultsPagePresence();
	    String searchPageUrl = browserHelper.getCurrentUrl();
	    String[] hotelNames = hotelResultsPage.getDisplayedHotelNames();
	    
	    int i = 1;
	    for (String hotelName : hotelNames){
	    	System.out.println("Currently checking for "+ i + " st/th "+" Hotel: " + hotelName);
	    	int hotelIndex = hotelResultsPage.getHotelIndexByHotelName(hotelName);
			int[] hotelPriceInSearch = hotelResultsPage.getHotelPriceBreakUpByIndex(hotelIndex);
			int roomRateInSearch = Integer.parseInt(hotelResultsPage.getHotelRoomRateByIndex(hotelIndex));
			int sumInSearch = hotelPriceInSearch[0] + hotelPriceInSearch[1] - hotelPriceInSearch[2];
			verify.assertTrue(Math.abs(hotelPriceInSearch[3] - sumInSearch) <= 1, "For Hotel : " + hotelName + " Dates : " + checkInCheckOutDates[0] +" and " + checkInCheckOutDates[1] +" Total " + hotelPriceInSearch[3]
					+ ". is not same as sum of room price, tax and discount" + sumInSearch);
			verify.assertTrue(Math.abs(roomRateInSearch - sumInSearch) <= 1, "For Hotel : " + hotelName + " Dates : " + checkInCheckOutDates[0] +" and " + checkInCheckOutDates[1] +" displayed room rate: " + roomRateInSearch + " and sum: " + sumInSearch + "in price break up is not same!!!");
			
	    	hotelResultsPage.clickHotelNameByIndex(hotelIndex);
	    	hotelDetailsPage.verifyHotelDetailsPagePresence();
	    	verify.assertEquals(hotelDetailsPage.getHotelNameInHotelDetailsPage().toLowerCase(),hotelName.toLowerCase());
	    	
			double[] hotelPriceInfoHD = hotelDetailsPage.getHotelPriceBreakUpInHD();
			int sumOfRateTaxDiscHDPage = (int) Math.round(hotelPriceInfoHD[0] + hotelPriceInfoHD[1] - hotelPriceInfoHD[2]);
			int hotelRoomRateInHD = hotelDetailsPage.getHotelTotalPriceInHD();
			verify.assertTrue(Math.abs((int) Math.round(hotelPriceInfoHD[3]) - sumOfRateTaxDiscHDPage) <= 1,
					"For Hotel : " + hotelName + " Dates : " + checkInCheckOutDates[0] +" and " + checkInCheckOutDates[1] +" Total : " + (int) Math.round(hotelPriceInfoHD[3]) + " and sum of room price, tax and discount :"
							+ sumOfRateTaxDiscHDPage + " does not match");
			verify.assertTrue(Math.abs(hotelRoomRateInHD - (int) Math.round(hotelPriceInfoHD[3])) <= 1,
					"For Hotel : " + hotelName + " Dates : " + checkInCheckOutDates[0] +" and " + checkInCheckOutDates[1] +" Total Displayed in HD :" + hotelRoomRateInHD + " and in break up :"
							+ (int) Math.round(hotelPriceInfoHD[3]) + " not same");
			verify.assertTrue(Math.abs(hotelPriceInSearch[0] - (int) Math.round(hotelPriceInfoHD[0])) <= 1,
					"For Hotel : " + hotelName + " Dates : " + checkInCheckOutDates[0] +" and " + checkInCheckOutDates[1] +" Room rate in Search page :" + hotelPriceInSearch[0] + " and HD Page :"
							+ (int) Math.round(hotelPriceInfoHD[0]) + " is not same");
			verify.assertTrue(Math.abs(hotelPriceInSearch[1] - (int) Math.round(hotelPriceInfoHD[1])) <= 1,
					"For Hotel : " + hotelName + " Dates : " + checkInCheckOutDates[0] +" and " + checkInCheckOutDates[1] +" Taxes in Search page: " + hotelPriceInSearch[1] + " and HD Page "
							+ (int) Math.round(hotelPriceInfoHD[1]) + " is not same");
			verify.assertTrue(Math.abs(hotelPriceInSearch[2] - (int) Math.round(hotelPriceInfoHD[2])) <= 1,
					"For Hotel : " + hotelName + " Dates : " + checkInCheckOutDates[0] +" and " + checkInCheckOutDates[1] +" Discount in Search page :" + hotelPriceInSearch[2] + " and HD Page :"
							+ (int) Math.round(hotelPriceInfoHD[2]) + " is not same");
			verify.assertTrue(Math.abs(hotelPriceInSearch[3] - (int) Math.round(hotelPriceInfoHD[3])) <= 1,
					"For Hotel : " + hotelName + " Dates : " + checkInCheckOutDates[0] +" and " + checkInCheckOutDates[1] +" Total in Search page : " + hotelPriceInSearch[3] + " and HD Page :"
							+ (int) Math.round(hotelPriceInfoHD[3]) + " is not same");
	    	
	    	browserHelper.openUrl(searchPageUrl);
	    	i = i + 1;
	    }
	    verify.assertAll();
	}
	
	
	@Test(groups = { "ProdPriceCheck" })
	public void testAllHotelPriceCheckInBookingPathNewDelhi(){
	    homePage = new HomePage();
	    hotelResultsPage = new HotelResultsPage();
	    hotelDetailsPage = new HotelDetailsPage();
	    itineraryPage = new ItineraryPage();
	    browserHelper = new BrowserHelper();
	    CommonUtils utils = new CommonUtils();
	    SoftAssert verify = new SoftAssert();
	    
	    String location = "New Delhi";
	    int checkInFromCurrentDate = utils.getRandomNumber(7, 30);
	    int checkOutFromCurrentDate = checkInFromCurrentDate + 1;
	    int roomIndex = 1;
	    
	    String[] checkInCheckOutDates = homePage.doSearch(location, checkInFromCurrentDate, checkOutFromCurrentDate, roomIndex);
	    hotelResultsPage.verifyHotelResultsPagePresence();
	    String searchPageUrl = browserHelper.getCurrentUrl();
	    String[] hotelNames = hotelResultsPage.getDisplayedHotelNames();
	    
	    int i = 1;
	    for (String hotelName : hotelNames){
	    	System.out.println("Currently checking for "+ i + " st/th "+" Hotel: " + hotelName);
	    	int hotelIndex = hotelResultsPage.getHotelIndexByHotelName(hotelName);
			int[] hotelPriceInSearch = hotelResultsPage.getHotelPriceBreakUpByIndex(hotelIndex);
			int roomRateInSearch = Integer.parseInt(hotelResultsPage.getHotelRoomRateByIndex(hotelIndex));
			int sumInSearch = hotelPriceInSearch[0] + hotelPriceInSearch[1] - hotelPriceInSearch[2];
			verify.assertTrue(Math.abs(hotelPriceInSearch[3] - sumInSearch) <= 1, "For Hotel : " + hotelName + " Dates : " + checkInCheckOutDates[0] +" and " + checkInCheckOutDates[1] +" Total " + hotelPriceInSearch[3]
					+ ". is not same as sum of room price, tax and discount" + sumInSearch);
			verify.assertTrue(Math.abs(roomRateInSearch - sumInSearch) <= 1, "For Hotel : " + hotelName + " Dates : " + checkInCheckOutDates[0] +" and " + checkInCheckOutDates[1] +" displayed room rate: " + roomRateInSearch + " and sum: " + sumInSearch + "in price break up is not same!!!");
			
	    	hotelResultsPage.clickHotelNameByIndex(hotelIndex);
	    	hotelDetailsPage.verifyHotelDetailsPagePresence();
	    	verify.assertEquals(hotelDetailsPage.getHotelNameInHotelDetailsPage().toLowerCase(),hotelName.toLowerCase());
	    	
			double[] hotelPriceInfoHD = hotelDetailsPage.getHotelPriceBreakUpInHD();
			int sumOfRateTaxDiscHDPage = (int) Math.round(hotelPriceInfoHD[0] + hotelPriceInfoHD[1] - hotelPriceInfoHD[2]);
			int hotelRoomRateInHD = hotelDetailsPage.getHotelTotalPriceInHD();
			verify.assertTrue(Math.abs((int) Math.round(hotelPriceInfoHD[3]) - sumOfRateTaxDiscHDPage) <= 1,
					"For Hotel : " + hotelName + " Dates : " + checkInCheckOutDates[0] +" and " + checkInCheckOutDates[1] +" Total : " + (int) Math.round(hotelPriceInfoHD[3]) + " and sum of room price, tax and discount :"
							+ sumOfRateTaxDiscHDPage + " does not match");
			verify.assertTrue(Math.abs(hotelRoomRateInHD - (int) Math.round(hotelPriceInfoHD[3])) <= 1,
					"For Hotel : " + hotelName + " Dates : " + checkInCheckOutDates[0] +" and " + checkInCheckOutDates[1] +" Total Displayed in HD :" + hotelRoomRateInHD + " and in break up :"
							+ (int) Math.round(hotelPriceInfoHD[3]) + " not same");
			verify.assertTrue(Math.abs(hotelPriceInSearch[0] - (int) Math.round(hotelPriceInfoHD[0])) <= 1,
					"For Hotel : " + hotelName + " Dates : " + checkInCheckOutDates[0] +" and " + checkInCheckOutDates[1] +" Room rate in Search page :" + hotelPriceInSearch[0] + " and HD Page :"
							+ (int) Math.round(hotelPriceInfoHD[0]) + " is not same");
			verify.assertTrue(Math.abs(hotelPriceInSearch[1] - (int) Math.round(hotelPriceInfoHD[1])) <= 1,
					"For Hotel : " + hotelName + " Dates : " + checkInCheckOutDates[0] +" and " + checkInCheckOutDates[1] +" Taxes in Search page: " + hotelPriceInSearch[1] + " and HD Page "
							+ (int) Math.round(hotelPriceInfoHD[1]) + " is not same");
			verify.assertTrue(Math.abs(hotelPriceInSearch[2] - (int) Math.round(hotelPriceInfoHD[2])) <= 1,
					"For Hotel : " + hotelName + " Dates : " + checkInCheckOutDates[0] +" and " + checkInCheckOutDates[1] +" Discount in Search page :" + hotelPriceInSearch[2] + " and HD Page :"
							+ (int) Math.round(hotelPriceInfoHD[2]) + " is not same");
			verify.assertTrue(Math.abs(hotelPriceInSearch[3] - (int) Math.round(hotelPriceInfoHD[3])) <= 1,
					"For Hotel : " + hotelName + " Dates : " + checkInCheckOutDates[0] +" and " + checkInCheckOutDates[1] +" Total in Search page : " + hotelPriceInSearch[3] + " and HD Page :"
							+ (int) Math.round(hotelPriceInfoHD[3]) + " is not same");
	    	
	    	browserHelper.openUrl(searchPageUrl);
	    	i = i + 1;
	    }
	    verify.assertAll();
	}
	
	
	@Test(groups = { "ProdPriceCheck" })
	public void testAllHotelPriceCheckInBookingPathPune(){
	    homePage = new HomePage();
	    hotelResultsPage = new HotelResultsPage();
	    hotelDetailsPage = new HotelDetailsPage();
	    itineraryPage = new ItineraryPage();
	    browserHelper = new BrowserHelper();
	    CommonUtils utils = new CommonUtils();
	    SoftAssert verify = new SoftAssert();
	    
	    String location = "Pune";
	    int checkInFromCurrentDate = utils.getRandomNumber(7, 30);
	    int checkOutFromCurrentDate = checkInFromCurrentDate + 1;
	    int roomIndex = 1;
	    
	    String[] checkInCheckOutDates = homePage.doSearch(location, checkInFromCurrentDate, checkOutFromCurrentDate, roomIndex);
	    hotelResultsPage.verifyHotelResultsPagePresence();
	    String searchPageUrl = browserHelper.getCurrentUrl();
	    String[] hotelNames = hotelResultsPage.getDisplayedHotelNames();
	    
	    int i = 1;
	    for (String hotelName : hotelNames){
	    	System.out.println("Currently checking for "+ i + " st/th "+" Hotel: " + hotelName);
	    	int hotelIndex = hotelResultsPage.getHotelIndexByHotelName(hotelName);
			int[] hotelPriceInSearch = hotelResultsPage.getHotelPriceBreakUpByIndex(hotelIndex);
			int roomRateInSearch = Integer.parseInt(hotelResultsPage.getHotelRoomRateByIndex(hotelIndex));
			int sumInSearch = hotelPriceInSearch[0] + hotelPriceInSearch[1] - hotelPriceInSearch[2];
			verify.assertTrue(Math.abs(hotelPriceInSearch[3] - sumInSearch) <= 1, "For Hotel : " + hotelName + " Dates : " + checkInCheckOutDates[0] +" and " + checkInCheckOutDates[1] +" Total " + hotelPriceInSearch[3]
					+ ". is not same as sum of room price, tax and discount" + sumInSearch);
			verify.assertTrue(Math.abs(roomRateInSearch - sumInSearch) <= 1, "For Hotel : " + hotelName + " Dates : " + checkInCheckOutDates[0] +" and " + checkInCheckOutDates[1] +" displayed room rate: " + roomRateInSearch + " and sum: " + sumInSearch + "in price break up is not same!!!");
			
	    	hotelResultsPage.clickHotelNameByIndex(hotelIndex);
	    	hotelDetailsPage.verifyHotelDetailsPagePresence();
	    	verify.assertEquals(hotelDetailsPage.getHotelNameInHotelDetailsPage().toLowerCase(),hotelName.toLowerCase());
	    	
			double[] hotelPriceInfoHD = hotelDetailsPage.getHotelPriceBreakUpInHD();
			int sumOfRateTaxDiscHDPage = (int) Math.round(hotelPriceInfoHD[0] + hotelPriceInfoHD[1] - hotelPriceInfoHD[2]);
			int hotelRoomRateInHD = hotelDetailsPage.getHotelTotalPriceInHD();
			verify.assertTrue(Math.abs((int) Math.round(hotelPriceInfoHD[3]) - sumOfRateTaxDiscHDPage) <= 1,
					"For Hotel : " + hotelName + " Dates : " + checkInCheckOutDates[0] +" and " + checkInCheckOutDates[1] +" Total : " + (int) Math.round(hotelPriceInfoHD[3]) + " and sum of room price, tax and discount :"
							+ sumOfRateTaxDiscHDPage + " does not match");
			verify.assertTrue(Math.abs(hotelRoomRateInHD - (int) Math.round(hotelPriceInfoHD[3])) <= 1,
					"For Hotel : " + hotelName + " Dates : " + checkInCheckOutDates[0] +" and " + checkInCheckOutDates[1] +" Total Displayed in HD :" + hotelRoomRateInHD + " and in break up :"
							+ (int) Math.round(hotelPriceInfoHD[3]) + " not same");
			verify.assertTrue(Math.abs(hotelPriceInSearch[0] - (int) Math.round(hotelPriceInfoHD[0])) <= 1,
					"For Hotel : " + hotelName + " Dates : " + checkInCheckOutDates[0] +" and " + checkInCheckOutDates[1] +" Room rate in Search page :" + hotelPriceInSearch[0] + " and HD Page :"
							+ (int) Math.round(hotelPriceInfoHD[0]) + " is not same");
			verify.assertTrue(Math.abs(hotelPriceInSearch[1] - (int) Math.round(hotelPriceInfoHD[1])) <= 1,
					"For Hotel : " + hotelName + " Dates : " + checkInCheckOutDates[0] +" and " + checkInCheckOutDates[1] +" Taxes in Search page: " + hotelPriceInSearch[1] + " and HD Page "
							+ (int) Math.round(hotelPriceInfoHD[1]) + " is not same");
			verify.assertTrue(Math.abs(hotelPriceInSearch[2] - (int) Math.round(hotelPriceInfoHD[2])) <= 1,
					"For Hotel : " + hotelName + " Dates : " + checkInCheckOutDates[0] +" and " + checkInCheckOutDates[1] +" Discount in Search page :" + hotelPriceInSearch[2] + " and HD Page :"
							+ (int) Math.round(hotelPriceInfoHD[2]) + " is not same");
			verify.assertTrue(Math.abs(hotelPriceInSearch[3] - (int) Math.round(hotelPriceInfoHD[3])) <= 1,
					"For Hotel : " + hotelName + " Dates : " + checkInCheckOutDates[0] +" and " + checkInCheckOutDates[1] +" Total in Search page : " + hotelPriceInSearch[3] + " and HD Page :"
							+ (int) Math.round(hotelPriceInfoHD[3]) + " is not same");
	    	
	    	browserHelper.openUrl(searchPageUrl);
	    	i = i + 1;
	    }
	    verify.assertAll();
	}
	
	@Test(groups = { "ProdPriceCheck" })
	public void testAllHotelPriceCheckInBookingPathGoa(){
	    homePage = new HomePage();
	    hotelResultsPage = new HotelResultsPage();
	    hotelDetailsPage = new HotelDetailsPage();
	    itineraryPage = new ItineraryPage();
	    browserHelper = new BrowserHelper();
	    CommonUtils utils = new CommonUtils();
	    SoftAssert verify = new SoftAssert();
	    
	    String location = "Goa";
	    int checkInFromCurrentDate = utils.getRandomNumber(7, 30);
	    int checkOutFromCurrentDate = checkInFromCurrentDate + 1;
	    int roomIndex = 1;
	    
	    String[] checkInCheckOutDates = homePage.doSearch(location, checkInFromCurrentDate, checkOutFromCurrentDate, roomIndex);
	    hotelResultsPage.verifyHotelResultsPagePresence();
	    String searchPageUrl = browserHelper.getCurrentUrl();
	    String[] hotelNames = hotelResultsPage.getDisplayedHotelNames();
	    
	    int i = 1;
	    for (String hotelName : hotelNames){
	    	System.out.println("Currently checking for "+ i + " st/th "+" Hotel: " + hotelName);
	    	int hotelIndex = hotelResultsPage.getHotelIndexByHotelName(hotelName);
			int[] hotelPriceInSearch = hotelResultsPage.getHotelPriceBreakUpByIndex(hotelIndex);
			int roomRateInSearch = Integer.parseInt(hotelResultsPage.getHotelRoomRateByIndex(hotelIndex));
			int sumInSearch = hotelPriceInSearch[0] + hotelPriceInSearch[1] - hotelPriceInSearch[2];
			verify.assertTrue(Math.abs(hotelPriceInSearch[3] - sumInSearch) <= 1, "For Hotel : " + hotelName + " Dates : " + checkInCheckOutDates[0] +" and " + checkInCheckOutDates[1] + " Dates : " + checkInCheckOutDates[0] +" and " + checkInCheckOutDates[1] +" Dates : " + checkInCheckOutDates[0] +" and " + checkInCheckOutDates[1] +" Total " + hotelPriceInSearch[3]
					+ ". is not same as sum of room price, tax and discount" + sumInSearch);
			verify.assertTrue(Math.abs(roomRateInSearch - sumInSearch) <= 1, "For Hotel : " + hotelName + " Dates : " + checkInCheckOutDates[0] +" and " + checkInCheckOutDates[1] +" displayed room rate: " + roomRateInSearch + " and sum: " + sumInSearch + "in price break up is not same!!!");
			
	    	hotelResultsPage.clickHotelNameByIndex(hotelIndex);
	    	hotelDetailsPage.verifyHotelDetailsPagePresence();
	    	verify.assertEquals(hotelDetailsPage.getHotelNameInHotelDetailsPage().toLowerCase(),hotelName.toLowerCase());
	    	
			double[] hotelPriceInfoHD = hotelDetailsPage.getHotelPriceBreakUpInHD();
			int sumOfRateTaxDiscHDPage = (int) Math.round(hotelPriceInfoHD[0] + hotelPriceInfoHD[1] - hotelPriceInfoHD[2]);
			int hotelRoomRateInHD = hotelDetailsPage.getHotelTotalPriceInHD();
			verify.assertTrue(Math.abs((int) Math.round(hotelPriceInfoHD[3]) - sumOfRateTaxDiscHDPage) <= 1,
					"For Hotel : " + hotelName + " Dates : " + checkInCheckOutDates[0] +" and " + checkInCheckOutDates[1] +" Total : " + (int) Math.round(hotelPriceInfoHD[3]) + " and sum of room price, tax and discount :"
							+ sumOfRateTaxDiscHDPage + " does not match");
			verify.assertTrue(Math.abs(hotelRoomRateInHD - (int) Math.round(hotelPriceInfoHD[3])) <= 1,
					"For Hotel : " + hotelName + " Dates : " + checkInCheckOutDates[0] +" and " + checkInCheckOutDates[1] +" Total Displayed in HD :" + hotelRoomRateInHD + " and in break up :"
							+ (int) Math.round(hotelPriceInfoHD[3]) + " not same");
			verify.assertTrue(Math.abs(hotelPriceInSearch[0] - (int) Math.round(hotelPriceInfoHD[0])) <= 1,
					"For Hotel : " + hotelName + " Dates : " + checkInCheckOutDates[0] +" and " + checkInCheckOutDates[1] +" Room rate in Search page :" + hotelPriceInSearch[0] + " and HD Page :"
							+ (int) Math.round(hotelPriceInfoHD[0]) + " is not same");
			verify.assertTrue(Math.abs(hotelPriceInSearch[1] - (int) Math.round(hotelPriceInfoHD[1])) <= 1,
					"For Hotel : " + hotelName + " Dates : " + checkInCheckOutDates[0] +" and " + checkInCheckOutDates[1] +" Taxes in Search page: " + hotelPriceInSearch[1] + " and HD Page "
							+ (int) Math.round(hotelPriceInfoHD[1]) + " is not same");
			verify.assertTrue(Math.abs(hotelPriceInSearch[2] - (int) Math.round(hotelPriceInfoHD[2])) <= 1,
					"For Hotel : " + hotelName + " Dates : " + checkInCheckOutDates[0] +" and " + checkInCheckOutDates[1] +" Discount in Search page :" + hotelPriceInSearch[2] + " and HD Page :"
							+ (int) Math.round(hotelPriceInfoHD[2]) + " is not same");
			verify.assertTrue(Math.abs(hotelPriceInSearch[3] - (int) Math.round(hotelPriceInfoHD[3])) <= 1,
					"For Hotel : " + hotelName + " Dates : " + checkInCheckOutDates[0] +" and " + checkInCheckOutDates[1] +" Total in Search page : " + hotelPriceInSearch[3] + " and HD Page :"
							+ (int) Math.round(hotelPriceInfoHD[3]) + " is not same");
	    	
	    	browserHelper.openUrl(searchPageUrl);
	    	i = i + 1;
	    }
	    verify.assertAll();
	}
	
	@Test(groups = { "ProdPriceCheck" })
	public void testAllHotelPriceCheckInBookingPathKolkata(){
	    homePage = new HomePage();
	    hotelResultsPage = new HotelResultsPage();
	    hotelDetailsPage = new HotelDetailsPage();
	    itineraryPage = new ItineraryPage();
	    browserHelper = new BrowserHelper();
	    CommonUtils utils = new CommonUtils();
	    SoftAssert verify = new SoftAssert();
	    
	    String location = "Kolkata";
	    int checkInFromCurrentDate = utils.getRandomNumber(7, 30);
	    int checkOutFromCurrentDate = checkInFromCurrentDate + 1;
	    int roomIndex = 1;
	    
	    String[] checkInCheckOutDates = homePage.doSearch(location, checkInFromCurrentDate, checkOutFromCurrentDate, roomIndex);
	    hotelResultsPage.verifyHotelResultsPagePresence();
	    String searchPageUrl = browserHelper.getCurrentUrl();
	    String[] hotelNames = hotelResultsPage.getDisplayedHotelNames();
	    
	    int i = 1;
	    for (String hotelName : hotelNames){
	    	System.out.println("Currently checking for "+ i + " st/th "+" Hotel: " + hotelName);
	    	int hotelIndex = hotelResultsPage.getHotelIndexByHotelName(hotelName);
			int[] hotelPriceInSearch = hotelResultsPage.getHotelPriceBreakUpByIndex(hotelIndex);
			int roomRateInSearch = Integer.parseInt(hotelResultsPage.getHotelRoomRateByIndex(hotelIndex));
			int sumInSearch = hotelPriceInSearch[0] + hotelPriceInSearch[1] - hotelPriceInSearch[2];
			verify.assertTrue(Math.abs(hotelPriceInSearch[3] - sumInSearch) <= 1, "For Hotel : " + hotelName + " Dates : " + checkInCheckOutDates[0] +" and " + checkInCheckOutDates[1] +" Total " + hotelPriceInSearch[3]
					+ ". is not same as sum of room price, tax and discount" + sumInSearch);
			verify.assertTrue(Math.abs(roomRateInSearch - sumInSearch) <= 1, "For Hotel : " + hotelName + " Dates : " + checkInCheckOutDates[0] +" and " + checkInCheckOutDates[1] +" displayed room rate: " + roomRateInSearch + " and sum: " + sumInSearch + "in price break up is not same!!!");
			
	    	hotelResultsPage.clickHotelNameByIndex(hotelIndex);
	    	hotelDetailsPage.verifyHotelDetailsPagePresence();
	    	verify.assertEquals(hotelDetailsPage.getHotelNameInHotelDetailsPage().toLowerCase(),hotelName.toLowerCase());
	    	
			double[] hotelPriceInfoHD = hotelDetailsPage.getHotelPriceBreakUpInHD();
			int sumOfRateTaxDiscHDPage = (int) Math.round(hotelPriceInfoHD[0] + hotelPriceInfoHD[1] - hotelPriceInfoHD[2]);
			int hotelRoomRateInHD = hotelDetailsPage.getHotelTotalPriceInHD();
			verify.assertTrue(Math.abs((int) Math.round(hotelPriceInfoHD[3]) - sumOfRateTaxDiscHDPage) <= 1,
					"For Hotel : " + hotelName + " Dates : " + checkInCheckOutDates[0] +" and " + checkInCheckOutDates[1] +" Total : " + (int) Math.round(hotelPriceInfoHD[3]) + " and sum of room price, tax and discount :"
							+ sumOfRateTaxDiscHDPage + " does not match");
			verify.assertTrue(Math.abs(hotelRoomRateInHD - (int) Math.round(hotelPriceInfoHD[3])) <= 1,
					"For Hotel : " + hotelName + " Dates : " + checkInCheckOutDates[0] +" and " + checkInCheckOutDates[1] +" Total Displayed in HD :" + hotelRoomRateInHD + " and in break up :"
							+ (int) Math.round(hotelPriceInfoHD[3]) + " not same");
			verify.assertTrue(Math.abs(hotelPriceInSearch[0] - (int) Math.round(hotelPriceInfoHD[0])) <= 1,
					"For Hotel : " + hotelName + " Dates : " + checkInCheckOutDates[0] +" and " + checkInCheckOutDates[1] +" Room rate in Search page :" + hotelPriceInSearch[0] + " and HD Page :"
							+ (int) Math.round(hotelPriceInfoHD[0]) + " is not same");
			verify.assertTrue(Math.abs(hotelPriceInSearch[1] - (int) Math.round(hotelPriceInfoHD[1])) <= 1,
					"For Hotel : " + hotelName + " Dates : " + checkInCheckOutDates[0] +" and " + checkInCheckOutDates[1] +" Taxes in Search page: " + hotelPriceInSearch[1] + " and HD Page "
							+ (int) Math.round(hotelPriceInfoHD[1]) + " is not same");
			verify.assertTrue(Math.abs(hotelPriceInSearch[2] - (int) Math.round(hotelPriceInfoHD[2])) <= 1,
					"For Hotel : " + hotelName + " Dates : " + checkInCheckOutDates[0] +" and " + checkInCheckOutDates[1] +" Discount in Search page :" + hotelPriceInSearch[2] + " and HD Page :"
							+ (int) Math.round(hotelPriceInfoHD[2]) + " is not same");
			verify.assertTrue(Math.abs(hotelPriceInSearch[3] - (int) Math.round(hotelPriceInfoHD[3])) <= 1,
					"For Hotel : " + hotelName + " Dates : " + checkInCheckOutDates[0] +" and " + checkInCheckOutDates[1] +" Total in Search page : " + hotelPriceInSearch[3] + " and HD Page :"
							+ (int) Math.round(hotelPriceInfoHD[3]) + " is not same");
	    	
	    	browserHelper.openUrl(searchPageUrl);
	    	i = i + 1;
	    }
	    verify.assertAll();
	}
	
	@Test(groups = { "ProdPriceCheck" })
	public void testAllHotelPriceCheckInBookingPathMysore(){
	    homePage = new HomePage();
	    hotelResultsPage = new HotelResultsPage();
	    hotelDetailsPage = new HotelDetailsPage();
	    itineraryPage = new ItineraryPage();
	    browserHelper = new BrowserHelper();
	    CommonUtils utils = new CommonUtils();
	    SoftAssert verify = new SoftAssert();
	    
	    String location = "Mysore";
	    int checkInFromCurrentDate = utils.getRandomNumber(7, 30);
	    int checkOutFromCurrentDate = checkInFromCurrentDate + 1;
	    int roomIndex = 1;
	    
	    String[] checkInCheckOutDates = homePage.doSearch(location, checkInFromCurrentDate, checkOutFromCurrentDate, roomIndex);
	    hotelResultsPage.verifyHotelResultsPagePresence();
	    String searchPageUrl = browserHelper.getCurrentUrl();
	    String[] hotelNames = hotelResultsPage.getDisplayedHotelNames();
	    
	    int i = 1;
	    for (String hotelName : hotelNames){
	    	System.out.println("Currently checking for "+ i + " st/th "+" Hotel: " + hotelName);
	    	int hotelIndex = hotelResultsPage.getHotelIndexByHotelName(hotelName);
			int[] hotelPriceInSearch = hotelResultsPage.getHotelPriceBreakUpByIndex(hotelIndex);
			int roomRateInSearch = Integer.parseInt(hotelResultsPage.getHotelRoomRateByIndex(hotelIndex));
			int sumInSearch = hotelPriceInSearch[0] + hotelPriceInSearch[1] - hotelPriceInSearch[2];
			verify.assertTrue(Math.abs(hotelPriceInSearch[3] - sumInSearch) <= 1, "For Hotel : " + hotelName + " Dates : " + checkInCheckOutDates[0] +" and " + checkInCheckOutDates[1] +" Total " + hotelPriceInSearch[3]
					+ ". is not same as sum of room price, tax and discount" + sumInSearch);
			verify.assertTrue(Math.abs(roomRateInSearch - sumInSearch) <= 1, "For Hotel : " + hotelName + " Dates : " + checkInCheckOutDates[0] +" and " + checkInCheckOutDates[1] +" displayed room rate: " + roomRateInSearch + " and sum: " + sumInSearch + "in price break up is not same!!!");
			
	    	hotelResultsPage.clickHotelNameByIndex(hotelIndex);
	    	hotelDetailsPage.verifyHotelDetailsPagePresence();
	    	verify.assertEquals(hotelDetailsPage.getHotelNameInHotelDetailsPage().toLowerCase(),hotelName.toLowerCase());
	    	
			double[] hotelPriceInfoHD = hotelDetailsPage.getHotelPriceBreakUpInHD();
			int sumOfRateTaxDiscHDPage = (int) Math.round(hotelPriceInfoHD[0] + hotelPriceInfoHD[1] - hotelPriceInfoHD[2]);
			int hotelRoomRateInHD = hotelDetailsPage.getHotelTotalPriceInHD();
			verify.assertTrue(Math.abs((int) Math.round(hotelPriceInfoHD[3]) - sumOfRateTaxDiscHDPage) <= 1,
					"For Hotel : " + hotelName + " Dates : " + checkInCheckOutDates[0] +" and " + checkInCheckOutDates[1] +" Total : " + (int) Math.round(hotelPriceInfoHD[3]) + " and sum of room price, tax and discount :"
							+ sumOfRateTaxDiscHDPage + " does not match");
			verify.assertTrue(Math.abs(hotelRoomRateInHD - (int) Math.round(hotelPriceInfoHD[3])) <= 1,
					"For Hotel : " + hotelName + " Dates : " + checkInCheckOutDates[0] +" and " + checkInCheckOutDates[1] +" Total Displayed in HD :" + hotelRoomRateInHD + " and in break up :"
							+ (int) Math.round(hotelPriceInfoHD[3]) + " not same");
			verify.assertTrue(Math.abs(hotelPriceInSearch[0] - (int) Math.round(hotelPriceInfoHD[0])) <= 1,
					"For Hotel : " + hotelName + " Dates : " + checkInCheckOutDates[0] +" and " + checkInCheckOutDates[1] +" Room rate in Search page :" + hotelPriceInSearch[0] + " and HD Page :"
							+ (int) Math.round(hotelPriceInfoHD[0]) + " is not same");
			verify.assertTrue(Math.abs(hotelPriceInSearch[1] - (int) Math.round(hotelPriceInfoHD[1])) <= 1,
					"For Hotel : " + hotelName + " Dates : " + checkInCheckOutDates[0] +" and " + checkInCheckOutDates[1] +" Taxes in Search page: " + hotelPriceInSearch[1] + " and HD Page "
							+ (int) Math.round(hotelPriceInfoHD[1]) + " is not same");
			verify.assertTrue(Math.abs(hotelPriceInSearch[2] - (int) Math.round(hotelPriceInfoHD[2])) <= 1,
					"For Hotel : " + hotelName + " Dates : " + checkInCheckOutDates[0] +" and " + checkInCheckOutDates[1] +" Discount in Search page :" + hotelPriceInSearch[2] + " and HD Page :"
							+ (int) Math.round(hotelPriceInfoHD[2]) + " is not same");
			verify.assertTrue(Math.abs(hotelPriceInSearch[3] - (int) Math.round(hotelPriceInfoHD[3])) <= 1,
					"For Hotel : " + hotelName + " Dates : " + checkInCheckOutDates[0] +" and " + checkInCheckOutDates[1] +" Total in Search page : " + hotelPriceInSearch[3] + " and HD Page :"
							+ (int) Math.round(hotelPriceInfoHD[3]) + " is not same");
	    	
	    	browserHelper.openUrl(searchPageUrl);
	    	i = i + 1;
	    }
	    verify.assertAll();
	}
	
	@Test(groups = { "ProdPriceCheck" })
	public void testAllHotelPriceCheckInBookingPathGurgaon(){
	    homePage = new HomePage();
	    hotelResultsPage = new HotelResultsPage();
	    hotelDetailsPage = new HotelDetailsPage();
	    itineraryPage = new ItineraryPage();
	    browserHelper = new BrowserHelper();
	    CommonUtils utils = new CommonUtils();
	    SoftAssert verify = new SoftAssert();
	    
	    String location = "Gurgaon";
	    int checkInFromCurrentDate = utils.getRandomNumber(7, 30);
	    int checkOutFromCurrentDate = checkInFromCurrentDate + 1;
	    int roomIndex = 1;
	    
	    String[] checkInCheckOutDates = homePage.doSearch(location, checkInFromCurrentDate, checkOutFromCurrentDate, roomIndex);
	    hotelResultsPage.verifyHotelResultsPagePresence();
	    String searchPageUrl = browserHelper.getCurrentUrl();
	    String[] hotelNames = hotelResultsPage.getDisplayedHotelNames();
	    
	    int i = 1;
	    for (String hotelName : hotelNames){
	    	System.out.println("Currently checking for "+ i + " st/th "+" Hotel: " + hotelName);
	    	int hotelIndex = hotelResultsPage.getHotelIndexByHotelName(hotelName);
			int[] hotelPriceInSearch = hotelResultsPage.getHotelPriceBreakUpByIndex(hotelIndex);
			int roomRateInSearch = Integer.parseInt(hotelResultsPage.getHotelRoomRateByIndex(hotelIndex));
			int sumInSearch = hotelPriceInSearch[0] + hotelPriceInSearch[1] - hotelPriceInSearch[2];
			verify.assertTrue(Math.abs(hotelPriceInSearch[3] - sumInSearch) <= 1, "For Hotel : " + hotelName + " Dates : " + checkInCheckOutDates[0] +" and " + checkInCheckOutDates[1] +" Total " + hotelPriceInSearch[3]
					+ ". is not same as sum of room price, tax and discount" + sumInSearch);
			verify.assertTrue(Math.abs(roomRateInSearch - sumInSearch) <= 1, "For Hotel : " + hotelName + " Dates : " + checkInCheckOutDates[0] +" and " + checkInCheckOutDates[1] +" displayed room rate: " + roomRateInSearch + " and sum: " + sumInSearch + "in price break up is not same!!!");
			
	    	hotelResultsPage.clickHotelNameByIndex(hotelIndex);
	    	hotelDetailsPage.verifyHotelDetailsPagePresence();
	    	verify.assertEquals(hotelDetailsPage.getHotelNameInHotelDetailsPage().toLowerCase(),hotelName.toLowerCase());
	    	
			double[] hotelPriceInfoHD = hotelDetailsPage.getHotelPriceBreakUpInHD();
			int sumOfRateTaxDiscHDPage = (int) Math.round(hotelPriceInfoHD[0] + hotelPriceInfoHD[1] - hotelPriceInfoHD[2]);
			int hotelRoomRateInHD = hotelDetailsPage.getHotelTotalPriceInHD();
			verify.assertTrue(Math.abs((int) Math.round(hotelPriceInfoHD[3]) - sumOfRateTaxDiscHDPage) <= 1,
					"For Hotel : " + hotelName + " Dates : " + checkInCheckOutDates[0] +" and " + checkInCheckOutDates[1] +" Total : " + (int) Math.round(hotelPriceInfoHD[3]) + " and sum of room price, tax and discount :"
							+ sumOfRateTaxDiscHDPage + " does not match");
			verify.assertTrue(Math.abs(hotelRoomRateInHD - (int) Math.round(hotelPriceInfoHD[3])) <= 1,
					"For Hotel : " + hotelName + " Dates : " + checkInCheckOutDates[0] +" and " + checkInCheckOutDates[1] +" Total Displayed in HD :" + hotelRoomRateInHD + " and in break up :"
							+ (int) Math.round(hotelPriceInfoHD[3]) + " not same");
			verify.assertTrue(Math.abs(hotelPriceInSearch[0] - (int) Math.round(hotelPriceInfoHD[0])) <= 1,
					"For Hotel : " + hotelName + " Dates : " + checkInCheckOutDates[0] +" and " + checkInCheckOutDates[1] +" Room rate in Search page :" + hotelPriceInSearch[0] + " and HD Page :"
							+ (int) Math.round(hotelPriceInfoHD[0]) + " is not same");
			verify.assertTrue(Math.abs(hotelPriceInSearch[1] - (int) Math.round(hotelPriceInfoHD[1])) <= 1,
					"For Hotel : " + hotelName + " Dates : " + checkInCheckOutDates[0] +" and " + checkInCheckOutDates[1] +" Taxes in Search page: " + hotelPriceInSearch[1] + " and HD Page "
							+ (int) Math.round(hotelPriceInfoHD[1]) + " is not same");
			verify.assertTrue(Math.abs(hotelPriceInSearch[2] - (int) Math.round(hotelPriceInfoHD[2])) <= 1,
					"For Hotel : " + hotelName + " Dates : " + checkInCheckOutDates[0] +" and " + checkInCheckOutDates[1] +" Discount in Search page :" + hotelPriceInSearch[2] + " and HD Page :"
							+ (int) Math.round(hotelPriceInfoHD[2]) + " is not same");
			verify.assertTrue(Math.abs(hotelPriceInSearch[3] - (int) Math.round(hotelPriceInfoHD[3])) <= 1,
					"For Hotel : " + hotelName + " Dates : " + checkInCheckOutDates[0] +" and " + checkInCheckOutDates[1] +" Total in Search page : " + hotelPriceInSearch[3] + " and HD Page :"
							+ (int) Math.round(hotelPriceInfoHD[3]) + " is not same");
	    	
	    	browserHelper.openUrl(searchPageUrl);
	    	i = i + 1;
	    }
	    verify.assertAll();
	}
	
	@Test(groups = { "ProdPriceCheck" })
	public void testAllHotelPriceCheckInBookingPathOoty(){
	    homePage = new HomePage();
	    hotelResultsPage = new HotelResultsPage();
	    hotelDetailsPage = new HotelDetailsPage();
	    itineraryPage = new ItineraryPage();
	    browserHelper = new BrowserHelper();
	    CommonUtils utils = new CommonUtils();
	    SoftAssert verify = new SoftAssert();
	    
	    String location = "Ooty";
	    int checkInFromCurrentDate = utils.getRandomNumber(7, 30);
	    int checkOutFromCurrentDate = checkInFromCurrentDate + 1;
	    int roomIndex = 1;
	    
	    String[] checkInCheckOutDates = homePage.doSearch(location, checkInFromCurrentDate, checkOutFromCurrentDate, roomIndex);
	    hotelResultsPage.verifyHotelResultsPagePresence();
	    String searchPageUrl = browserHelper.getCurrentUrl();
	    String[] hotelNames = hotelResultsPage.getDisplayedHotelNames();
	    
	    int i = 1;
	    for (String hotelName : hotelNames){
	    	System.out.println("Currently checking for "+ i + " st/th "+" Hotel: " + hotelName);
	    	int hotelIndex = hotelResultsPage.getHotelIndexByHotelName(hotelName);
			int[] hotelPriceInSearch = hotelResultsPage.getHotelPriceBreakUpByIndex(hotelIndex);
			int roomRateInSearch = Integer.parseInt(hotelResultsPage.getHotelRoomRateByIndex(hotelIndex));
			int sumInSearch = hotelPriceInSearch[0] + hotelPriceInSearch[1] - hotelPriceInSearch[2];
			verify.assertTrue(Math.abs(hotelPriceInSearch[3] - sumInSearch) <= 1, "For Hotel : " + hotelName + " Dates : " + checkInCheckOutDates[0] +" and " + checkInCheckOutDates[1] +" Total " + hotelPriceInSearch[3]
					+ ". is not same as sum of room price, tax and discount" + sumInSearch);
			verify.assertTrue(Math.abs(roomRateInSearch - sumInSearch) <= 1, "For Hotel : " + hotelName + " Dates : " + checkInCheckOutDates[0] +" and " + checkInCheckOutDates[1] +" displayed room rate: " + roomRateInSearch + " and sum: " + sumInSearch + "in price break up is not same!!!");
			
	    	hotelResultsPage.clickHotelNameByIndex(hotelIndex);
	    	hotelDetailsPage.verifyHotelDetailsPagePresence();
	    	verify.assertEquals(hotelDetailsPage.getHotelNameInHotelDetailsPage().toLowerCase(),hotelName.toLowerCase());
	    	
			double[] hotelPriceInfoHD = hotelDetailsPage.getHotelPriceBreakUpInHD();
			int sumOfRateTaxDiscHDPage = (int) Math.round(hotelPriceInfoHD[0] + hotelPriceInfoHD[1] - hotelPriceInfoHD[2]);
			int hotelRoomRateInHD = hotelDetailsPage.getHotelTotalPriceInHD();
			verify.assertTrue(Math.abs((int) Math.round(hotelPriceInfoHD[3]) - sumOfRateTaxDiscHDPage) <= 1,
					"For Hotel : " + hotelName + " Dates : " + checkInCheckOutDates[0] +" and " + checkInCheckOutDates[1] +" Total : " + (int) Math.round(hotelPriceInfoHD[3]) + " and sum of room price, tax and discount :"
							+ sumOfRateTaxDiscHDPage + " does not match");
			verify.assertTrue(Math.abs(hotelRoomRateInHD - (int) Math.round(hotelPriceInfoHD[3])) <= 1,
					"For Hotel : " + hotelName + " Dates : " + checkInCheckOutDates[0] +" and " + checkInCheckOutDates[1] +" Total Displayed in HD :" + hotelRoomRateInHD + " and in break up :"
							+ (int) Math.round(hotelPriceInfoHD[3]) + " not same");
			verify.assertTrue(Math.abs(hotelPriceInSearch[0] - (int) Math.round(hotelPriceInfoHD[0])) <= 1,
					"For Hotel : " + hotelName + " Dates : " + checkInCheckOutDates[0] +" and " + checkInCheckOutDates[1] +" Room rate in Search page :" + hotelPriceInSearch[0] + " and HD Page :"
							+ (int) Math.round(hotelPriceInfoHD[0]) + " is not same");
			verify.assertTrue(Math.abs(hotelPriceInSearch[1] - (int) Math.round(hotelPriceInfoHD[1])) <= 1,
					"For Hotel : " + hotelName + " Dates : " + checkInCheckOutDates[0] +" and " + checkInCheckOutDates[1] +" Taxes in Search page: " + hotelPriceInSearch[1] + " and HD Page "
							+ (int) Math.round(hotelPriceInfoHD[1]) + " is not same");
			verify.assertTrue(Math.abs(hotelPriceInSearch[2] - (int) Math.round(hotelPriceInfoHD[2])) <= 1,
					"For Hotel : " + hotelName + " Dates : " + checkInCheckOutDates[0] +" and " + checkInCheckOutDates[1] +" Discount in Search page :" + hotelPriceInSearch[2] + " and HD Page :"
							+ (int) Math.round(hotelPriceInfoHD[2]) + " is not same");
			verify.assertTrue(Math.abs(hotelPriceInSearch[3] - (int) Math.round(hotelPriceInfoHD[3])) <= 1,
					"For Hotel : " + hotelName + " Dates : " + checkInCheckOutDates[0] +" and " + checkInCheckOutDates[1] +" Total in Search page : " + hotelPriceInSearch[3] + " and HD Page :"
							+ (int) Math.round(hotelPriceInfoHD[3]) + " is not same");
	    	
	    	browserHelper.openUrl(searchPageUrl);
	    	i = i + 1;
	    }
	    verify.assertAll();
	}
	
	@Test(groups = { "ProdPriceCheck" })
	public void testAllHotelPriceCheckInBookingPathManali(){
	    homePage = new HomePage();
	    hotelResultsPage = new HotelResultsPage();
	    hotelDetailsPage = new HotelDetailsPage();
	    itineraryPage = new ItineraryPage();
	    browserHelper = new BrowserHelper();
	    CommonUtils utils = new CommonUtils();
	    SoftAssert verify = new SoftAssert();
	    
	    String location = "Manali";
	    int checkInFromCurrentDate = utils.getRandomNumber(7, 30);
	    int checkOutFromCurrentDate = checkInFromCurrentDate + 1;
	    int roomIndex = 1;
	    
	    String[] checkInCheckOutDates = homePage.doSearch(location, checkInFromCurrentDate, checkOutFromCurrentDate, roomIndex);
	    hotelResultsPage.verifyHotelResultsPagePresence();
	    String searchPageUrl = browserHelper.getCurrentUrl();
	    String[] hotelNames = hotelResultsPage.getDisplayedHotelNames();
	    
	    int i = 1;
	    for (String hotelName : hotelNames){
	    	System.out.println("Currently checking for "+ i + " st/th "+" Hotel: " + hotelName);
	    	int hotelIndex = hotelResultsPage.getHotelIndexByHotelName(hotelName);
			int[] hotelPriceInSearch = hotelResultsPage.getHotelPriceBreakUpByIndex(hotelIndex);
			int roomRateInSearch = Integer.parseInt(hotelResultsPage.getHotelRoomRateByIndex(hotelIndex));
			int sumInSearch = hotelPriceInSearch[0] + hotelPriceInSearch[1] - hotelPriceInSearch[2];
			verify.assertTrue(Math.abs(hotelPriceInSearch[3] - sumInSearch) <= 1, "For Hotel : " + hotelName + " Dates : " + checkInCheckOutDates[0] +" and " + checkInCheckOutDates[1] +" Total " + hotelPriceInSearch[3]
					+ ". is not same as sum of room price, tax and discount" + sumInSearch);
			verify.assertTrue(Math.abs(roomRateInSearch - sumInSearch) <= 1, "For Hotel : " + hotelName + " Dates : " + checkInCheckOutDates[0] +" and " + checkInCheckOutDates[1] +" displayed room rate: " + roomRateInSearch + " and sum: " + sumInSearch + "in price break up is not same!!!");
			
	    	hotelResultsPage.clickHotelNameByIndex(hotelIndex);
	    	hotelDetailsPage.verifyHotelDetailsPagePresence();
	    	verify.assertEquals(hotelDetailsPage.getHotelNameInHotelDetailsPage().toLowerCase(),hotelName.toLowerCase());
	    	
			double[] hotelPriceInfoHD = hotelDetailsPage.getHotelPriceBreakUpInHD();
			int sumOfRateTaxDiscHDPage = (int) Math.round(hotelPriceInfoHD[0] + hotelPriceInfoHD[1] - hotelPriceInfoHD[2]);
			int hotelRoomRateInHD = hotelDetailsPage.getHotelTotalPriceInHD();
			verify.assertTrue(Math.abs((int) Math.round(hotelPriceInfoHD[3]) - sumOfRateTaxDiscHDPage) <= 1,
					"For Hotel : " + hotelName + " Dates : " + checkInCheckOutDates[0] +" and " + checkInCheckOutDates[1] +" Total : " + (int) Math.round(hotelPriceInfoHD[3]) + " and sum of room price, tax and discount :"
							+ sumOfRateTaxDiscHDPage + " does not match");
			verify.assertTrue(Math.abs(hotelRoomRateInHD - (int) Math.round(hotelPriceInfoHD[3])) <= 1,
					"For Hotel : " + hotelName + " Dates : " + checkInCheckOutDates[0] +" and " + checkInCheckOutDates[1] +" Total Displayed in HD :" + hotelRoomRateInHD + " and in break up :"
							+ (int) Math.round(hotelPriceInfoHD[3]) + " not same");
			verify.assertTrue(Math.abs(hotelPriceInSearch[0] - (int) Math.round(hotelPriceInfoHD[0])) <= 1,
					"For Hotel : " + hotelName + " Dates : " + checkInCheckOutDates[0] +" and " + checkInCheckOutDates[1] +" Room rate in Search page :" + hotelPriceInSearch[0] + " and HD Page :"
							+ (int) Math.round(hotelPriceInfoHD[0]) + " is not same");
			verify.assertTrue(Math.abs(hotelPriceInSearch[1] - (int) Math.round(hotelPriceInfoHD[1])) <= 1,
					"For Hotel : " + hotelName + " Dates : " + checkInCheckOutDates[0] +" and " + checkInCheckOutDates[1] +" Taxes in Search page: " + hotelPriceInSearch[1] + " and HD Page "
							+ (int) Math.round(hotelPriceInfoHD[1]) + " is not same");
			verify.assertTrue(Math.abs(hotelPriceInSearch[2] - (int) Math.round(hotelPriceInfoHD[2])) <= 1,
					"For Hotel : " + hotelName + " Dates : " + checkInCheckOutDates[0] +" and " + checkInCheckOutDates[1] +" Discount in Search page :" + hotelPriceInSearch[2] + " and HD Page :"
							+ (int) Math.round(hotelPriceInfoHD[2]) + " is not same");
			verify.assertTrue(Math.abs(hotelPriceInSearch[3] - (int) Math.round(hotelPriceInfoHD[3])) <= 1,
					"For Hotel : " + hotelName + " Dates : " + checkInCheckOutDates[0] +" and " + checkInCheckOutDates[1] +" Total in Search page : " + hotelPriceInSearch[3] + " and HD Page :"
							+ (int) Math.round(hotelPriceInfoHD[3]) + " is not same");
	    	
	    	browserHelper.openUrl(searchPageUrl);
	    	i = i + 1;
	    }
	    verify.assertAll();
	}
	
	@Test(groups = { "ProdPriceCheck" })
	public void testAllHotelPriceCheckInBookingPathAgra(){
	    homePage = new HomePage();
	    hotelResultsPage = new HotelResultsPage();
	    hotelDetailsPage = new HotelDetailsPage();
	    itineraryPage = new ItineraryPage();
	    browserHelper = new BrowserHelper();
	    CommonUtils utils = new CommonUtils();
	    SoftAssert verify = new SoftAssert();
	    
	    String location = "Agra";
	    int checkInFromCurrentDate = utils.getRandomNumber(7, 30);
	    int checkOutFromCurrentDate = checkInFromCurrentDate + 1;
	    int roomIndex = 1;
	    
	    String[] checkInCheckOutDates = homePage.doSearch(location, checkInFromCurrentDate, checkOutFromCurrentDate, roomIndex);
	    hotelResultsPage.verifyHotelResultsPagePresence();
	    String searchPageUrl = browserHelper.getCurrentUrl();
	    String[] hotelNames = hotelResultsPage.getDisplayedHotelNames();
	    
	    int i = 1;
	    for (String hotelName : hotelNames){
	    	System.out.println("Currently checking for "+ i + " st/th "+" Hotel: " + hotelName);
	    	int hotelIndex = hotelResultsPage.getHotelIndexByHotelName(hotelName);
			int[] hotelPriceInSearch = hotelResultsPage.getHotelPriceBreakUpByIndex(hotelIndex);
			int roomRateInSearch = Integer.parseInt(hotelResultsPage.getHotelRoomRateByIndex(hotelIndex));
			int sumInSearch = hotelPriceInSearch[0] + hotelPriceInSearch[1] - hotelPriceInSearch[2];
			verify.assertTrue(Math.abs(hotelPriceInSearch[3] - sumInSearch) <= 1, "For Hotel : " + hotelName + " Dates : " + checkInCheckOutDates[0] +" and " + checkInCheckOutDates[1] +" Total " + hotelPriceInSearch[3]
					+ ". is not same as sum of room price, tax and discount" + sumInSearch);
			verify.assertTrue(Math.abs(roomRateInSearch - sumInSearch) <= 1, "For Hotel : " + hotelName + " Dates : " + checkInCheckOutDates[0] +" and " + checkInCheckOutDates[1] +" displayed room rate: " + roomRateInSearch + " and sum: " + sumInSearch + "in price break up is not same!!!");
			
	    	hotelResultsPage.clickHotelNameByIndex(hotelIndex);
	    	hotelDetailsPage.verifyHotelDetailsPagePresence();
	    	verify.assertEquals(hotelDetailsPage.getHotelNameInHotelDetailsPage().toLowerCase(),hotelName.toLowerCase());
	    	
			double[] hotelPriceInfoHD = hotelDetailsPage.getHotelPriceBreakUpInHD();
			int sumOfRateTaxDiscHDPage = (int) Math.round(hotelPriceInfoHD[0] + hotelPriceInfoHD[1] - hotelPriceInfoHD[2]);
			int hotelRoomRateInHD = hotelDetailsPage.getHotelTotalPriceInHD();
			verify.assertTrue(Math.abs((int) Math.round(hotelPriceInfoHD[3]) - sumOfRateTaxDiscHDPage) <= 1,
					"For Hotel : " + hotelName + " Dates : " + checkInCheckOutDates[0] +" and " + checkInCheckOutDates[1] +" Total : " + (int) Math.round(hotelPriceInfoHD[3]) + " and sum of room price, tax and discount :"
							+ sumOfRateTaxDiscHDPage + " does not match");
			verify.assertTrue(Math.abs(hotelRoomRateInHD - (int) Math.round(hotelPriceInfoHD[3])) <= 1,
					"For Hotel : " + hotelName + " Dates : " + checkInCheckOutDates[0] +" and " + checkInCheckOutDates[1] +" Total Displayed in HD :" + hotelRoomRateInHD + " and in break up :"
							+ (int) Math.round(hotelPriceInfoHD[3]) + " not same");
			verify.assertTrue(Math.abs(hotelPriceInSearch[0] - (int) Math.round(hotelPriceInfoHD[0])) <= 1,
					"For Hotel : " + hotelName + " Dates : " + checkInCheckOutDates[0] +" and " + checkInCheckOutDates[1] +" Room rate in Search page :" + hotelPriceInSearch[0] + " and HD Page :"
							+ (int) Math.round(hotelPriceInfoHD[0]) + " is not same");
			verify.assertTrue(Math.abs(hotelPriceInSearch[1] - (int) Math.round(hotelPriceInfoHD[1])) <= 1,
					"For Hotel : " + hotelName + " Dates : " + checkInCheckOutDates[0] +" and " + checkInCheckOutDates[1] +" Taxes in Search page: " + hotelPriceInSearch[1] + " and HD Page "
							+ (int) Math.round(hotelPriceInfoHD[1]) + " is not same");
			verify.assertTrue(Math.abs(hotelPriceInSearch[2] - (int) Math.round(hotelPriceInfoHD[2])) <= 1,
					"For Hotel : " + hotelName + " Dates : " + checkInCheckOutDates[0] +" and " + checkInCheckOutDates[1] +" Discount in Search page :" + hotelPriceInSearch[2] + " and HD Page :"
							+ (int) Math.round(hotelPriceInfoHD[2]) + " is not same");
			verify.assertTrue(Math.abs(hotelPriceInSearch[3] - (int) Math.round(hotelPriceInfoHD[3])) <= 1,
					"For Hotel : " + hotelName + " Dates : " + checkInCheckOutDates[0] +" and " + checkInCheckOutDates[1] +" Total in Search page : " + hotelPriceInSearch[3] + " and HD Page :"
							+ (int) Math.round(hotelPriceInfoHD[3]) + " is not same");
	    	
	    	browserHelper.openUrl(searchPageUrl);
	    	i = i + 1;
	    }
	    verify.assertAll();
	}
	
	@Test(groups = { "ProdPriceCheck" })
	public void testAllHotelPriceCheckInBookingPathUdaipur(){
	    homePage = new HomePage();
	    hotelResultsPage = new HotelResultsPage();
	    hotelDetailsPage = new HotelDetailsPage();
	    itineraryPage = new ItineraryPage();
	    browserHelper = new BrowserHelper();
	    CommonUtils utils = new CommonUtils();
	    SoftAssert verify = new SoftAssert();
	    
	    String location = "Udaipur";
	    int checkInFromCurrentDate = utils.getRandomNumber(7, 30);
	    int checkOutFromCurrentDate = checkInFromCurrentDate + 1;
	    int roomIndex = 1;
	    
	    String[] checkInCheckOutDates = homePage.doSearch(location, checkInFromCurrentDate, checkOutFromCurrentDate, roomIndex);
	    hotelResultsPage.verifyHotelResultsPagePresence();
	    String searchPageUrl = browserHelper.getCurrentUrl();
	    String[] hotelNames = hotelResultsPage.getDisplayedHotelNames();
	    
	    int i = 1;
	    for (String hotelName : hotelNames){
	    	System.out.println("Currently checking for "+ i + " st/th "+" Hotel: " + hotelName);
	    	int hotelIndex = hotelResultsPage.getHotelIndexByHotelName(hotelName);
			int[] hotelPriceInSearch = hotelResultsPage.getHotelPriceBreakUpByIndex(hotelIndex);
			int roomRateInSearch = Integer.parseInt(hotelResultsPage.getHotelRoomRateByIndex(hotelIndex));
			int sumInSearch = hotelPriceInSearch[0] + hotelPriceInSearch[1] - hotelPriceInSearch[2];
			verify.assertTrue(Math.abs(hotelPriceInSearch[3] - sumInSearch) <= 1, "For Hotel : " + hotelName + " Dates : " + checkInCheckOutDates[0] +" and " + checkInCheckOutDates[1] +" Total " + hotelPriceInSearch[3]
					+ ". is not same as sum of room price, tax and discount" + sumInSearch);
			verify.assertTrue(Math.abs(roomRateInSearch - sumInSearch) <= 1, "For Hotel : " + hotelName + " Dates : " + checkInCheckOutDates[0] +" and " + checkInCheckOutDates[1] +" displayed room rate: " + roomRateInSearch + " and sum: " + sumInSearch + "in price break up is not same!!!");
			
	    	hotelResultsPage.clickHotelNameByIndex(hotelIndex);
	    	hotelDetailsPage.verifyHotelDetailsPagePresence();
	    	verify.assertEquals(hotelDetailsPage.getHotelNameInHotelDetailsPage().toLowerCase(),hotelName.toLowerCase());
	    	
			double[] hotelPriceInfoHD = hotelDetailsPage.getHotelPriceBreakUpInHD();
			int sumOfRateTaxDiscHDPage = (int) Math.round(hotelPriceInfoHD[0] + hotelPriceInfoHD[1] - hotelPriceInfoHD[2]);
			int hotelRoomRateInHD = hotelDetailsPage.getHotelTotalPriceInHD();
			verify.assertTrue(Math.abs((int) Math.round(hotelPriceInfoHD[3]) - sumOfRateTaxDiscHDPage) <= 1,
					"For Hotel : " + hotelName + " Dates : " + checkInCheckOutDates[0] +" and " + checkInCheckOutDates[1] +" Total : " + (int) Math.round(hotelPriceInfoHD[3]) + " and sum of room price, tax and discount :"
							+ sumOfRateTaxDiscHDPage + " does not match");
			verify.assertTrue(Math.abs(hotelRoomRateInHD - (int) Math.round(hotelPriceInfoHD[3])) <= 1,
					"For Hotel : " + hotelName + " Dates : " + checkInCheckOutDates[0] +" and " + checkInCheckOutDates[1] +" Total Displayed in HD :" + hotelRoomRateInHD + " and in break up :"
							+ (int) Math.round(hotelPriceInfoHD[3]) + " not same");
			verify.assertTrue(Math.abs(hotelPriceInSearch[0] - (int) Math.round(hotelPriceInfoHD[0])) <= 1,
					"For Hotel : " + hotelName + " Dates : " + checkInCheckOutDates[0] +" and " + checkInCheckOutDates[1] +" Room rate in Search page :" + hotelPriceInSearch[0] + " and HD Page :"
							+ (int) Math.round(hotelPriceInfoHD[0]) + " is not same");
			verify.assertTrue(Math.abs(hotelPriceInSearch[1] - (int) Math.round(hotelPriceInfoHD[1])) <= 1,
					"For Hotel : " + hotelName + " Dates : " + checkInCheckOutDates[0] +" and " + checkInCheckOutDates[1] +" Taxes in Search page: " + hotelPriceInSearch[1] + " and HD Page "
							+ (int) Math.round(hotelPriceInfoHD[1]) + " is not same");
			verify.assertTrue(Math.abs(hotelPriceInSearch[2] - (int) Math.round(hotelPriceInfoHD[2])) <= 1,
					"For Hotel : " + hotelName + " Dates : " + checkInCheckOutDates[0] +" and " + checkInCheckOutDates[1] +" Discount in Search page :" + hotelPriceInSearch[2] + " and HD Page :"
							+ (int) Math.round(hotelPriceInfoHD[2]) + " is not same");
			verify.assertTrue(Math.abs(hotelPriceInSearch[3] - (int) Math.round(hotelPriceInfoHD[3])) <= 1,
					"For Hotel : " + hotelName + " Dates : " + checkInCheckOutDates[0] +" and " + checkInCheckOutDates[1] +" Total in Search page : " + hotelPriceInSearch[3] + " and HD Page :"
							+ (int) Math.round(hotelPriceInfoHD[3]) + " is not same");
	    	
	    	browserHelper.openUrl(searchPageUrl);
	    	i = i + 1;
	    }
	    verify.assertAll();
	}
	
	@Test(groups = { "ProdPriceCheck" })
	public void testAllHotelPriceCheckInBookingPathKodaikanal(){
	    homePage = new HomePage();
	    hotelResultsPage = new HotelResultsPage();
	    hotelDetailsPage = new HotelDetailsPage();
	    itineraryPage = new ItineraryPage();
	    browserHelper = new BrowserHelper();
	    CommonUtils utils = new CommonUtils();
	    SoftAssert verify = new SoftAssert();
	    
	    String location = "Kodaikanal";
	    int checkInFromCurrentDate = utils.getRandomNumber(7, 30);
	    int checkOutFromCurrentDate = checkInFromCurrentDate + 1;
	    int roomIndex = 1;
	    
	    String[] checkInCheckOutDates = homePage.doSearch(location, checkInFromCurrentDate, checkOutFromCurrentDate, roomIndex);
	    hotelResultsPage.verifyHotelResultsPagePresence();
	    String searchPageUrl = browserHelper.getCurrentUrl();
	    String[] hotelNames = hotelResultsPage.getDisplayedHotelNames();
	    
	    int i = 1;
	    for (String hotelName : hotelNames){
	    	System.out.println("Currently checking for "+ i + " st/th "+" Hotel: " + hotelName);
	    	int hotelIndex = hotelResultsPage.getHotelIndexByHotelName(hotelName);
			int[] hotelPriceInSearch = hotelResultsPage.getHotelPriceBreakUpByIndex(hotelIndex);
			int roomRateInSearch = Integer.parseInt(hotelResultsPage.getHotelRoomRateByIndex(hotelIndex));
			int sumInSearch = hotelPriceInSearch[0] + hotelPriceInSearch[1] - hotelPriceInSearch[2];
			verify.assertTrue(Math.abs(hotelPriceInSearch[3] - sumInSearch) <= 1, "For Hotel : " + hotelName + " Dates : " + checkInCheckOutDates[0] +" and " + checkInCheckOutDates[1] +" Total " + hotelPriceInSearch[3]
					+ ". is not same as sum of room price, tax and discount" + sumInSearch);
			verify.assertTrue(Math.abs(roomRateInSearch - sumInSearch) <= 1, "For Hotel : " + hotelName + " Dates : " + checkInCheckOutDates[0] +" and " + checkInCheckOutDates[1] +" displayed room rate: " + roomRateInSearch + " and sum: " + sumInSearch + "in price break up is not same!!!");
			
	    	hotelResultsPage.clickHotelNameByIndex(hotelIndex);
	    	hotelDetailsPage.verifyHotelDetailsPagePresence();
	    	verify.assertEquals(hotelDetailsPage.getHotelNameInHotelDetailsPage().toLowerCase(),hotelName.toLowerCase());
	    	
			double[] hotelPriceInfoHD = hotelDetailsPage.getHotelPriceBreakUpInHD();
			int sumOfRateTaxDiscHDPage = (int) Math.round(hotelPriceInfoHD[0] + hotelPriceInfoHD[1] - hotelPriceInfoHD[2]);
			int hotelRoomRateInHD = hotelDetailsPage.getHotelTotalPriceInHD();
			verify.assertTrue(Math.abs((int) Math.round(hotelPriceInfoHD[3]) - sumOfRateTaxDiscHDPage) <= 1,
					"For Hotel : " + hotelName + " Dates : " + checkInCheckOutDates[0] +" and " + checkInCheckOutDates[1] +" Total : " + (int) Math.round(hotelPriceInfoHD[3]) + " and sum of room price, tax and discount :"
							+ sumOfRateTaxDiscHDPage + " does not match");
			verify.assertTrue(Math.abs(hotelRoomRateInHD - (int) Math.round(hotelPriceInfoHD[3])) <= 1,
					"For Hotel : " + hotelName + " Dates : " + checkInCheckOutDates[0] +" and " + checkInCheckOutDates[1] +" Total Displayed in HD :" + hotelRoomRateInHD + " and in break up :"
							+ (int) Math.round(hotelPriceInfoHD[3]) + " not same");
			verify.assertTrue(Math.abs(hotelPriceInSearch[0] - (int) Math.round(hotelPriceInfoHD[0])) <= 1,
					"For Hotel : " + hotelName + " Dates : " + checkInCheckOutDates[0] +" and " + checkInCheckOutDates[1] +" Room rate in Search page :" + hotelPriceInSearch[0] + " and HD Page :"
							+ (int) Math.round(hotelPriceInfoHD[0]) + " is not same");
			verify.assertTrue(Math.abs(hotelPriceInSearch[1] - (int) Math.round(hotelPriceInfoHD[1])) <= 1,
					"For Hotel : " + hotelName + " Dates : " + checkInCheckOutDates[0] +" and " + checkInCheckOutDates[1] +" Taxes in Search page: " + hotelPriceInSearch[1] + " and HD Page "
							+ (int) Math.round(hotelPriceInfoHD[1]) + " is not same");
			verify.assertTrue(Math.abs(hotelPriceInSearch[2] - (int) Math.round(hotelPriceInfoHD[2])) <= 1,
					"For Hotel : " + hotelName + " Dates : " + checkInCheckOutDates[0] +" and " + checkInCheckOutDates[1] +" Discount in Search page :" + hotelPriceInSearch[2] + " and HD Page :"
							+ (int) Math.round(hotelPriceInfoHD[2]) + " is not same");
			verify.assertTrue(Math.abs(hotelPriceInSearch[3] - (int) Math.round(hotelPriceInfoHD[3])) <= 1,
					"For Hotel : " + hotelName + " Dates : " + checkInCheckOutDates[0] +" and " + checkInCheckOutDates[1] +" Total in Search page : " + hotelPriceInSearch[3] + " and HD Page :"
							+ (int) Math.round(hotelPriceInfoHD[3]) + " is not same");
	    	
	    	browserHelper.openUrl(searchPageUrl);
	    	i = i + 1;
	    }
	    verify.assertAll();
	}
	
	@Test(groups = { "ProdPriceCheck" })
	public void testAllHotelPriceCheckInBookingPathAurangabad(){
	    homePage = new HomePage();
	    hotelResultsPage = new HotelResultsPage();
	    hotelDetailsPage = new HotelDetailsPage();
	    itineraryPage = new ItineraryPage();
	    browserHelper = new BrowserHelper();
	    CommonUtils utils = new CommonUtils();
	    SoftAssert verify = new SoftAssert();
	    
	    String location = "Aurangabad";
	    int checkInFromCurrentDate = utils.getRandomNumber(7, 30);
	    int checkOutFromCurrentDate = checkInFromCurrentDate + 1;
	    int roomIndex = 1;
	    
	    String[] checkInCheckOutDates = homePage.doSearch(location, checkInFromCurrentDate, checkOutFromCurrentDate, roomIndex);
	    hotelResultsPage.verifyHotelResultsPagePresence();
	    String searchPageUrl = browserHelper.getCurrentUrl();
	    String[] hotelNames = hotelResultsPage.getDisplayedHotelNames();
	    
	    int i = 1;
	    for (String hotelName : hotelNames){
	    	System.out.println("Currently checking for "+ i + " st/th "+" Hotel: " + hotelName);
	    	int hotelIndex = hotelResultsPage.getHotelIndexByHotelName(hotelName);
			int[] hotelPriceInSearch = hotelResultsPage.getHotelPriceBreakUpByIndex(hotelIndex);
			int roomRateInSearch = Integer.parseInt(hotelResultsPage.getHotelRoomRateByIndex(hotelIndex));
			int sumInSearch = hotelPriceInSearch[0] + hotelPriceInSearch[1] - hotelPriceInSearch[2];
			verify.assertTrue(Math.abs(hotelPriceInSearch[3] - sumInSearch) <= 1, "For Hotel : " + hotelName + " Dates : " + checkInCheckOutDates[0] +" and " + checkInCheckOutDates[1] +" Total " + hotelPriceInSearch[3]
					+ ". is not same as sum of room price, tax and discount" + sumInSearch);
			verify.assertTrue(Math.abs(roomRateInSearch - sumInSearch) <= 1, "For Hotel : " + hotelName + " Dates : " + checkInCheckOutDates[0] +" and " + checkInCheckOutDates[1] +" displayed room rate: " + roomRateInSearch + " and sum: " + sumInSearch + "in price break up is not same!!!");
			
	    	hotelResultsPage.clickHotelNameByIndex(hotelIndex);
	    	hotelDetailsPage.verifyHotelDetailsPagePresence();
	    	verify.assertEquals(hotelDetailsPage.getHotelNameInHotelDetailsPage().toLowerCase(),hotelName.toLowerCase());
	    	
			double[] hotelPriceInfoHD = hotelDetailsPage.getHotelPriceBreakUpInHD();
			int sumOfRateTaxDiscHDPage = (int) Math.round(hotelPriceInfoHD[0] + hotelPriceInfoHD[1] - hotelPriceInfoHD[2]);
			int hotelRoomRateInHD = hotelDetailsPage.getHotelTotalPriceInHD();
			verify.assertTrue(Math.abs((int) Math.round(hotelPriceInfoHD[3]) - sumOfRateTaxDiscHDPage) <= 1,
					"For Hotel : " + hotelName + " Dates : " + checkInCheckOutDates[0] +" and " + checkInCheckOutDates[1] +" Total : " + (int) Math.round(hotelPriceInfoHD[3]) + " and sum of room price, tax and discount :"
							+ sumOfRateTaxDiscHDPage + " does not match");
			verify.assertTrue(Math.abs(hotelRoomRateInHD - (int) Math.round(hotelPriceInfoHD[3])) <= 1,
					"For Hotel : " + hotelName + " Dates : " + checkInCheckOutDates[0] +" and " + checkInCheckOutDates[1] +" Total Displayed in HD :" + hotelRoomRateInHD + " and in break up :"
							+ (int) Math.round(hotelPriceInfoHD[3]) + " not same");
			verify.assertTrue(Math.abs(hotelPriceInSearch[0] - (int) Math.round(hotelPriceInfoHD[0])) <= 1,
					"For Hotel : " + hotelName + " Dates : " + checkInCheckOutDates[0] +" and " + checkInCheckOutDates[1] +" Room rate in Search page :" + hotelPriceInSearch[0] + " and HD Page :"
							+ (int) Math.round(hotelPriceInfoHD[0]) + " is not same");
			verify.assertTrue(Math.abs(hotelPriceInSearch[1] - (int) Math.round(hotelPriceInfoHD[1])) <= 1,
					"For Hotel : " + hotelName + " Dates : " + checkInCheckOutDates[0] +" and " + checkInCheckOutDates[1] +" Taxes in Search page: " + hotelPriceInSearch[1] + " and HD Page "
							+ (int) Math.round(hotelPriceInfoHD[1]) + " is not same");
			verify.assertTrue(Math.abs(hotelPriceInSearch[2] - (int) Math.round(hotelPriceInfoHD[2])) <= 1,
					"For Hotel : " + hotelName + " Dates : " + checkInCheckOutDates[0] +" and " + checkInCheckOutDates[1] +" Discount in Search page :" + hotelPriceInSearch[2] + " and HD Page :"
							+ (int) Math.round(hotelPriceInfoHD[2]) + " is not same");
			verify.assertTrue(Math.abs(hotelPriceInSearch[3] - (int) Math.round(hotelPriceInfoHD[3])) <= 1,
					"For Hotel : " + hotelName + " Dates : " + checkInCheckOutDates[0] +" and " + checkInCheckOutDates[1] +" Total in Search page : " + hotelPriceInSearch[3] + " and HD Page :"
							+ (int) Math.round(hotelPriceInfoHD[3]) + " is not same");
	    	
	    	browserHelper.openUrl(searchPageUrl);
	    	i = i + 1;
	    }
	    verify.assertAll();
	}
	
	@Test(groups = { "ProdPriceCheck" })
	public void testAllHotelPriceCheckInBookingPathNoida(){
	    homePage = new HomePage();
	    hotelResultsPage = new HotelResultsPage();
	    hotelDetailsPage = new HotelDetailsPage();
	    itineraryPage = new ItineraryPage();
	    browserHelper = new BrowserHelper();
	    CommonUtils utils = new CommonUtils();
	    SoftAssert verify = new SoftAssert();
	    
	    String location = "Noida";
	    int checkInFromCurrentDate = utils.getRandomNumber(7, 30);
	    int checkOutFromCurrentDate = checkInFromCurrentDate + 1;
	    int roomIndex = 1;
	    
	    String[] checkInCheckOutDates = homePage.doSearch(location, checkInFromCurrentDate, checkOutFromCurrentDate, roomIndex);
	    hotelResultsPage.verifyHotelResultsPagePresence();
	    String searchPageUrl = browserHelper.getCurrentUrl();
	    String[] hotelNames = hotelResultsPage.getDisplayedHotelNames();
	    
	    int i = 1;
	    for (String hotelName : hotelNames){
	    	System.out.println("Currently checking for "+ i + " st/th "+" Hotel: " + hotelName);
	    	int hotelIndex = hotelResultsPage.getHotelIndexByHotelName(hotelName);
			int[] hotelPriceInSearch = hotelResultsPage.getHotelPriceBreakUpByIndex(hotelIndex);
			int roomRateInSearch = Integer.parseInt(hotelResultsPage.getHotelRoomRateByIndex(hotelIndex));
			int sumInSearch = hotelPriceInSearch[0] + hotelPriceInSearch[1] - hotelPriceInSearch[2];
			verify.assertTrue(Math.abs(hotelPriceInSearch[3] - sumInSearch) <= 1, "For Hotel : " + hotelName + " Dates : " + checkInCheckOutDates[0] +" and " + checkInCheckOutDates[1] +" Total " + hotelPriceInSearch[3]
					+ ". is not same as sum of room price, tax and discount" + sumInSearch);
			verify.assertTrue(Math.abs(roomRateInSearch - sumInSearch) <= 1, "For Hotel : " + hotelName + " Dates : " + checkInCheckOutDates[0] +" and " + checkInCheckOutDates[1] +" displayed room rate: " + roomRateInSearch + " and sum: " + sumInSearch + "in price break up is not same!!!");
			
	    	hotelResultsPage.clickHotelNameByIndex(hotelIndex);
	    	hotelDetailsPage.verifyHotelDetailsPagePresence();
	    	verify.assertEquals(hotelDetailsPage.getHotelNameInHotelDetailsPage().toLowerCase(),hotelName.toLowerCase());
	    	
			double[] hotelPriceInfoHD = hotelDetailsPage.getHotelPriceBreakUpInHD();
			int sumOfRateTaxDiscHDPage = (int) Math.round(hotelPriceInfoHD[0] + hotelPriceInfoHD[1] - hotelPriceInfoHD[2]);
			int hotelRoomRateInHD = hotelDetailsPage.getHotelTotalPriceInHD();
			verify.assertTrue(Math.abs((int) Math.round(hotelPriceInfoHD[3]) - sumOfRateTaxDiscHDPage) <= 1,
					"For Hotel : " + hotelName + " Dates : " + checkInCheckOutDates[0] +" and " + checkInCheckOutDates[1] +" Total : " + (int) Math.round(hotelPriceInfoHD[3]) + " and sum of room price, tax and discount :"
							+ sumOfRateTaxDiscHDPage + " does not match");
			verify.assertTrue(Math.abs(hotelRoomRateInHD - (int) Math.round(hotelPriceInfoHD[3])) <= 1,
					"For Hotel : " + hotelName + " Dates : " + checkInCheckOutDates[0] +" and " + checkInCheckOutDates[1] +" Total Displayed in HD :" + hotelRoomRateInHD + " and in break up :"
							+ (int) Math.round(hotelPriceInfoHD[3]) + " not same");
			verify.assertTrue(Math.abs(hotelPriceInSearch[0] - (int) Math.round(hotelPriceInfoHD[0])) <= 1,
					"For Hotel : " + hotelName + " Dates : " + checkInCheckOutDates[0] +" and " + checkInCheckOutDates[1] +" Room rate in Search page :" + hotelPriceInSearch[0] + " and HD Page :"
							+ (int) Math.round(hotelPriceInfoHD[0]) + " is not same");
			verify.assertTrue(Math.abs(hotelPriceInSearch[1] - (int) Math.round(hotelPriceInfoHD[1])) <= 1,
					"For Hotel : " + hotelName + " Dates : " + checkInCheckOutDates[0] +" and " + checkInCheckOutDates[1] +" Taxes in Search page: " + hotelPriceInSearch[1] + " and HD Page "
							+ (int) Math.round(hotelPriceInfoHD[1]) + " is not same");
			verify.assertTrue(Math.abs(hotelPriceInSearch[2] - (int) Math.round(hotelPriceInfoHD[2])) <= 1,
					"For Hotel : " + hotelName + " Dates : " + checkInCheckOutDates[0] +" and " + checkInCheckOutDates[1] +" Discount in Search page :" + hotelPriceInSearch[2] + " and HD Page :"
							+ (int) Math.round(hotelPriceInfoHD[2]) + " is not same");
			verify.assertTrue(Math.abs(hotelPriceInSearch[3] - (int) Math.round(hotelPriceInfoHD[3])) <= 1,
					"For Hotel : " + hotelName + " Dates : " + checkInCheckOutDates[0] +" and " + checkInCheckOutDates[1] +" Total in Search page : " + hotelPriceInSearch[3] + " and HD Page :"
							+ (int) Math.round(hotelPriceInfoHD[3]) + " is not same");
	    	
	    	browserHelper.openUrl(searchPageUrl);
	    	i = i + 1;
	    }
	    verify.assertAll();
	}
	
	@Test(groups = { "ProdPriceCheck" })
	public void testAllHotelPriceCheckInBookingPathAhmedabad(){
	    homePage = new HomePage();
	    hotelResultsPage = new HotelResultsPage();
	    hotelDetailsPage = new HotelDetailsPage();
	    itineraryPage = new ItineraryPage();
	    browserHelper = new BrowserHelper();
	    CommonUtils utils = new CommonUtils();
	    SoftAssert verify = new SoftAssert();
	    
	    String location = "Ahmedabad";
	    int checkInFromCurrentDate = utils.getRandomNumber(7, 30);
	    int checkOutFromCurrentDate = checkInFromCurrentDate + 1;
	    int roomIndex = 1;
	    
	    String[] checkInCheckOutDates = homePage.doSearch(location, checkInFromCurrentDate, checkOutFromCurrentDate, roomIndex);
	    hotelResultsPage.verifyHotelResultsPagePresence();
	    String searchPageUrl = browserHelper.getCurrentUrl();
	    String[] hotelNames = hotelResultsPage.getDisplayedHotelNames();
	    
	    int i = 1;
	    for (String hotelName : hotelNames){
	    	System.out.println("Currently checking for "+ i + " st/th "+" Hotel: " + hotelName);
	    	int hotelIndex = hotelResultsPage.getHotelIndexByHotelName(hotelName);
			int[] hotelPriceInSearch = hotelResultsPage.getHotelPriceBreakUpByIndex(hotelIndex);
			int roomRateInSearch = Integer.parseInt(hotelResultsPage.getHotelRoomRateByIndex(hotelIndex));
			int sumInSearch = hotelPriceInSearch[0] + hotelPriceInSearch[1] - hotelPriceInSearch[2];
			verify.assertTrue(Math.abs(hotelPriceInSearch[3] - sumInSearch) <= 1, "For Hotel : " + hotelName + " Dates : " + checkInCheckOutDates[0] +" and " + checkInCheckOutDates[1] +" Total " + hotelPriceInSearch[3]
					+ ". is not same as sum of room price, tax and discount" + sumInSearch);
			verify.assertTrue(Math.abs(roomRateInSearch - sumInSearch) <= 1, "For Hotel : " + hotelName + " Dates : " + checkInCheckOutDates[0] +" and " + checkInCheckOutDates[1] +" displayed room rate: " + roomRateInSearch + " and sum: " + sumInSearch + "in price break up is not same!!!");
			
	    	hotelResultsPage.clickHotelNameByIndex(hotelIndex);
	    	hotelDetailsPage.verifyHotelDetailsPagePresence();
	    	verify.assertEquals(hotelDetailsPage.getHotelNameInHotelDetailsPage().toLowerCase(),hotelName.toLowerCase());
	    	
			double[] hotelPriceInfoHD = hotelDetailsPage.getHotelPriceBreakUpInHD();
			int sumOfRateTaxDiscHDPage = (int) Math.round(hotelPriceInfoHD[0] + hotelPriceInfoHD[1] - hotelPriceInfoHD[2]);
			int hotelRoomRateInHD = hotelDetailsPage.getHotelTotalPriceInHD();
			verify.assertTrue(Math.abs((int) Math.round(hotelPriceInfoHD[3]) - sumOfRateTaxDiscHDPage) <= 1,
					"For Hotel : " + hotelName + " Dates : " + checkInCheckOutDates[0] +" and " + checkInCheckOutDates[1] +" Total : " + (int) Math.round(hotelPriceInfoHD[3]) + " and sum of room price, tax and discount :"
							+ sumOfRateTaxDiscHDPage + " does not match");
			verify.assertTrue(Math.abs(hotelRoomRateInHD - (int) Math.round(hotelPriceInfoHD[3])) <= 1,
					"For Hotel : " + hotelName + " Dates : " + checkInCheckOutDates[0] +" and " + checkInCheckOutDates[1] +" Total Displayed in HD :" + hotelRoomRateInHD + " and in break up :"
							+ (int) Math.round(hotelPriceInfoHD[3]) + " not same");
			verify.assertTrue(Math.abs(hotelPriceInSearch[0] - (int) Math.round(hotelPriceInfoHD[0])) <= 1,
					"For Hotel : " + hotelName + " Dates : " + checkInCheckOutDates[0] +" and " + checkInCheckOutDates[1] +" Room rate in Search page :" + hotelPriceInSearch[0] + " and HD Page :"
							+ (int) Math.round(hotelPriceInfoHD[0]) + " is not same");
			verify.assertTrue(Math.abs(hotelPriceInSearch[1] - (int) Math.round(hotelPriceInfoHD[1])) <= 1,
					"For Hotel : " + hotelName + " Dates : " + checkInCheckOutDates[0] +" and " + checkInCheckOutDates[1] +" Taxes in Search page: " + hotelPriceInSearch[1] + " and HD Page "
							+ (int) Math.round(hotelPriceInfoHD[1]) + " is not same");
			verify.assertTrue(Math.abs(hotelPriceInSearch[2] - (int) Math.round(hotelPriceInfoHD[2])) <= 1,
					"For Hotel : " + hotelName + " Dates : " + checkInCheckOutDates[0] +" and " + checkInCheckOutDates[1] +" Discount in Search page :" + hotelPriceInSearch[2] + " and HD Page :"
							+ (int) Math.round(hotelPriceInfoHD[2]) + " is not same");
			verify.assertTrue(Math.abs(hotelPriceInSearch[3] - (int) Math.round(hotelPriceInfoHD[3])) <= 1,
					"For Hotel : " + hotelName + " Dates : " + checkInCheckOutDates[0] +" and " + checkInCheckOutDates[1] +" Total in Search page : " + hotelPriceInSearch[3] + " and HD Page :"
							+ (int) Math.round(hotelPriceInfoHD[3]) + " is not same");
	    	
	    	browserHelper.openUrl(searchPageUrl);
	    	i = i + 1;
	    }
	    verify.assertAll();
	}
	
	@Test(groups = { "ProdPriceCheck" })
	public void testAllHotelPriceCheckInBookingPathChandigarh(){
	    homePage = new HomePage();
	    hotelResultsPage = new HotelResultsPage();
	    hotelDetailsPage = new HotelDetailsPage();
	    itineraryPage = new ItineraryPage();
	    browserHelper = new BrowserHelper();
	    CommonUtils utils = new CommonUtils();
	    SoftAssert verify = new SoftAssert();
	    
	    String location = "Chandigarh";
	    int checkInFromCurrentDate = utils.getRandomNumber(7, 30);
	    int checkOutFromCurrentDate = checkInFromCurrentDate + 1;
	    int roomIndex = 1;
	    
	    String[] checkInCheckOutDates = homePage.doSearch(location, checkInFromCurrentDate, checkOutFromCurrentDate, roomIndex);
	    hotelResultsPage.verifyHotelResultsPagePresence();
	    String searchPageUrl = browserHelper.getCurrentUrl();
	    String[] hotelNames = hotelResultsPage.getDisplayedHotelNames();
	    
	    int i = 1;
	    for (String hotelName : hotelNames){
	    	System.out.println("Currently checking for "+ i + " st/th "+" Hotel: " + hotelName);
	    	int hotelIndex = hotelResultsPage.getHotelIndexByHotelName(hotelName);
			int[] hotelPriceInSearch = hotelResultsPage.getHotelPriceBreakUpByIndex(hotelIndex);
			int roomRateInSearch = Integer.parseInt(hotelResultsPage.getHotelRoomRateByIndex(hotelIndex));
			int sumInSearch = hotelPriceInSearch[0] + hotelPriceInSearch[1] - hotelPriceInSearch[2];
			verify.assertTrue(Math.abs(hotelPriceInSearch[3] - sumInSearch) <= 1, "For Hotel : " + hotelName + " Dates : " + checkInCheckOutDates[0] +" and " + checkInCheckOutDates[1] +" Total " + hotelPriceInSearch[3]
					+ ". is not same as sum of room price, tax and discount" + sumInSearch);
			verify.assertTrue(Math.abs(roomRateInSearch - sumInSearch) <= 1, "For Hotel : " + hotelName + " Dates : " + checkInCheckOutDates[0] +" and " + checkInCheckOutDates[1] +" displayed room rate: " + roomRateInSearch + " and sum: " + sumInSearch + "in price break up is not same!!!");
			
	    	hotelResultsPage.clickHotelNameByIndex(hotelIndex);
	    	hotelDetailsPage.verifyHotelDetailsPagePresence();
	    	verify.assertEquals(hotelDetailsPage.getHotelNameInHotelDetailsPage().toLowerCase(),hotelName.toLowerCase());
	    	
			double[] hotelPriceInfoHD = hotelDetailsPage.getHotelPriceBreakUpInHD();
			int sumOfRateTaxDiscHDPage = (int) Math.round(hotelPriceInfoHD[0] + hotelPriceInfoHD[1] - hotelPriceInfoHD[2]);
			int hotelRoomRateInHD = hotelDetailsPage.getHotelTotalPriceInHD();
			verify.assertTrue(Math.abs((int) Math.round(hotelPriceInfoHD[3]) - sumOfRateTaxDiscHDPage) <= 1,
					"For Hotel : " + hotelName + " Dates : " + checkInCheckOutDates[0] +" and " + checkInCheckOutDates[1] +" Total : " + (int) Math.round(hotelPriceInfoHD[3]) + " and sum of room price, tax and discount :"
							+ sumOfRateTaxDiscHDPage + " does not match");
			verify.assertTrue(Math.abs(hotelRoomRateInHD - (int) Math.round(hotelPriceInfoHD[3])) <= 1,
					"For Hotel : " + hotelName + " Dates : " + checkInCheckOutDates[0] +" and " + checkInCheckOutDates[1] +" Total Displayed in HD :" + hotelRoomRateInHD + " and in break up :"
							+ (int) Math.round(hotelPriceInfoHD[3]) + " not same");
			verify.assertTrue(Math.abs(hotelPriceInSearch[0] - (int) Math.round(hotelPriceInfoHD[0])) <= 1,
					"For Hotel : " + hotelName + " Dates : " + checkInCheckOutDates[0] +" and " + checkInCheckOutDates[1] +" Room rate in Search page :" + hotelPriceInSearch[0] + " and HD Page :"
							+ (int) Math.round(hotelPriceInfoHD[0]) + " is not same");
			verify.assertTrue(Math.abs(hotelPriceInSearch[1] - (int) Math.round(hotelPriceInfoHD[1])) <= 1,
					"For Hotel : " + hotelName + " Dates : " + checkInCheckOutDates[0] +" and " + checkInCheckOutDates[1] +" Taxes in Search page: " + hotelPriceInSearch[1] + " and HD Page "
							+ (int) Math.round(hotelPriceInfoHD[1]) + " is not same");
			verify.assertTrue(Math.abs(hotelPriceInSearch[2] - (int) Math.round(hotelPriceInfoHD[2])) <= 1,
					"For Hotel : " + hotelName + " Dates : " + checkInCheckOutDates[0] +" and " + checkInCheckOutDates[1] +" Discount in Search page :" + hotelPriceInSearch[2] + " and HD Page :"
							+ (int) Math.round(hotelPriceInfoHD[2]) + " is not same");
			verify.assertTrue(Math.abs(hotelPriceInSearch[3] - (int) Math.round(hotelPriceInfoHD[3])) <= 1,
					"For Hotel : " + hotelName + " Dates : " + checkInCheckOutDates[0] +" and " + checkInCheckOutDates[1] +" Total in Search page : " + hotelPriceInSearch[3] + " and HD Page :"
							+ (int) Math.round(hotelPriceInfoHD[3]) + " is not same");
	    	
	    	browserHelper.openUrl(searchPageUrl);
	    	i = i + 1;
	    }
	    verify.assertAll();
	}
	
	@Test(groups = { "ProdPriceCheck" })
	public void testAllHotelPriceCheckInBookingPathCoimbatore(){
	    homePage = new HomePage();
	    hotelResultsPage = new HotelResultsPage();
	    hotelDetailsPage = new HotelDetailsPage();
	    itineraryPage = new ItineraryPage();
	    browserHelper = new BrowserHelper();
	    CommonUtils utils = new CommonUtils();
	    SoftAssert verify = new SoftAssert();
	    
	    String location = "Coimbatore";
	    int checkInFromCurrentDate = utils.getRandomNumber(7, 30);
	    int checkOutFromCurrentDate = checkInFromCurrentDate + 1;
	    int roomIndex = 1;
	    
	    String[] checkInCheckOutDates = homePage.doSearch(location, checkInFromCurrentDate, checkOutFromCurrentDate, roomIndex);
	    hotelResultsPage.verifyHotelResultsPagePresence();
	    String searchPageUrl = browserHelper.getCurrentUrl();
	    String[] hotelNames = hotelResultsPage.getDisplayedHotelNames();
	    
	    int i = 1;
	    for (String hotelName : hotelNames){
	    	System.out.println("Currently checking for "+ i + " st/th "+" Hotel: " + hotelName);
	    	int hotelIndex = hotelResultsPage.getHotelIndexByHotelName(hotelName);
			int[] hotelPriceInSearch = hotelResultsPage.getHotelPriceBreakUpByIndex(hotelIndex);
			int roomRateInSearch = Integer.parseInt(hotelResultsPage.getHotelRoomRateByIndex(hotelIndex));
			int sumInSearch = hotelPriceInSearch[0] + hotelPriceInSearch[1] - hotelPriceInSearch[2];
			verify.assertTrue(Math.abs(hotelPriceInSearch[3] - sumInSearch) <= 1, "For Hotel : " + hotelName + " Dates : " + checkInCheckOutDates[0] +" and " + checkInCheckOutDates[1] +" Total " + hotelPriceInSearch[3]
					+ ". is not same as sum of room price, tax and discount" + sumInSearch);
			verify.assertTrue(Math.abs(roomRateInSearch - sumInSearch) <= 1, "For Hotel : " + hotelName + " Dates : " + checkInCheckOutDates[0] +" and " + checkInCheckOutDates[1] +" displayed room rate: " + roomRateInSearch + " and sum: " + sumInSearch + "in price break up is not same!!!");
			
	    	hotelResultsPage.clickHotelNameByIndex(hotelIndex);
	    	hotelDetailsPage.verifyHotelDetailsPagePresence();
	    	verify.assertEquals(hotelDetailsPage.getHotelNameInHotelDetailsPage().toLowerCase(),hotelName.toLowerCase());
	    	
			double[] hotelPriceInfoHD = hotelDetailsPage.getHotelPriceBreakUpInHD();
			int sumOfRateTaxDiscHDPage = (int) Math.round(hotelPriceInfoHD[0] + hotelPriceInfoHD[1] - hotelPriceInfoHD[2]);
			int hotelRoomRateInHD = hotelDetailsPage.getHotelTotalPriceInHD();
			verify.assertTrue(Math.abs((int) Math.round(hotelPriceInfoHD[3]) - sumOfRateTaxDiscHDPage) <= 1,
					"For Hotel : " + hotelName + " Dates : " + checkInCheckOutDates[0] +" and " + checkInCheckOutDates[1] +" Total : " + (int) Math.round(hotelPriceInfoHD[3]) + " and sum of room price, tax and discount :"
							+ sumOfRateTaxDiscHDPage + " does not match");
			verify.assertTrue(Math.abs(hotelRoomRateInHD - (int) Math.round(hotelPriceInfoHD[3])) <= 1,
					"For Hotel : " + hotelName + " Dates : " + checkInCheckOutDates[0] +" and " + checkInCheckOutDates[1] +" Total Displayed in HD :" + hotelRoomRateInHD + " and in break up :"
							+ (int) Math.round(hotelPriceInfoHD[3]) + " not same");
			verify.assertTrue(Math.abs(hotelPriceInSearch[0] - (int) Math.round(hotelPriceInfoHD[0])) <= 1,
					"For Hotel : " + hotelName + " Dates : " + checkInCheckOutDates[0] +" and " + checkInCheckOutDates[1] +" Room rate in Search page :" + hotelPriceInSearch[0] + " and HD Page :"
							+ (int) Math.round(hotelPriceInfoHD[0]) + " is not same");
			verify.assertTrue(Math.abs(hotelPriceInSearch[1] - (int) Math.round(hotelPriceInfoHD[1])) <= 1,
					"For Hotel : " + hotelName + " Dates : " + checkInCheckOutDates[0] +" and " + checkInCheckOutDates[1] +" Taxes in Search page: " + hotelPriceInSearch[1] + " and HD Page "
							+ (int) Math.round(hotelPriceInfoHD[1]) + " is not same");
			verify.assertTrue(Math.abs(hotelPriceInSearch[2] - (int) Math.round(hotelPriceInfoHD[2])) <= 1,
					"For Hotel : " + hotelName + " Dates : " + checkInCheckOutDates[0] +" and " + checkInCheckOutDates[1] +" Discount in Search page :" + hotelPriceInSearch[2] + " and HD Page :"
							+ (int) Math.round(hotelPriceInfoHD[2]) + " is not same");
			verify.assertTrue(Math.abs(hotelPriceInSearch[3] - (int) Math.round(hotelPriceInfoHD[3])) <= 1,
					"For Hotel : " + hotelName + " Dates : " + checkInCheckOutDates[0] +" and " + checkInCheckOutDates[1] +" Total in Search page : " + hotelPriceInSearch[3] + " and HD Page :"
							+ (int) Math.round(hotelPriceInfoHD[3]) + " is not same");
	    	
	    	browserHelper.openUrl(searchPageUrl);
	    	i = i + 1;
	    }
	    verify.assertAll();
	}
	
	@Test(groups = { "ProdPriceCheck" })
	public void testAllHotelPriceCheckInBookingPathIndore(){
	    homePage = new HomePage();
	    hotelResultsPage = new HotelResultsPage();
	    hotelDetailsPage = new HotelDetailsPage();
	    itineraryPage = new ItineraryPage();
	    browserHelper = new BrowserHelper();
	    CommonUtils utils = new CommonUtils();
	    SoftAssert verify = new SoftAssert();
	    
	    String location = "Indore";
	    int checkInFromCurrentDate = utils.getRandomNumber(7, 30);
	    int checkOutFromCurrentDate = checkInFromCurrentDate + 1;
	    int roomIndex = 1;
	    
	    String[] checkInCheckOutDates = homePage.doSearch(location, checkInFromCurrentDate, checkOutFromCurrentDate, roomIndex);
	    hotelResultsPage.verifyHotelResultsPagePresence();
	    String searchPageUrl = browserHelper.getCurrentUrl();
	    String[] hotelNames = hotelResultsPage.getDisplayedHotelNames();
	    
	    int i = 1;
	    for (String hotelName : hotelNames){
	    	System.out.println("Currently checking for "+ i + " st/th "+" Hotel: " + hotelName);
	    	int hotelIndex = hotelResultsPage.getHotelIndexByHotelName(hotelName);
			int[] hotelPriceInSearch = hotelResultsPage.getHotelPriceBreakUpByIndex(hotelIndex);
			int roomRateInSearch = Integer.parseInt(hotelResultsPage.getHotelRoomRateByIndex(hotelIndex));
			int sumInSearch = hotelPriceInSearch[0] + hotelPriceInSearch[1] - hotelPriceInSearch[2];
			verify.assertTrue(Math.abs(hotelPriceInSearch[3] - sumInSearch) <= 1, "For Hotel : " + hotelName + " Dates : " + checkInCheckOutDates[0] +" and " + checkInCheckOutDates[1] +" Total " + hotelPriceInSearch[3]
					+ ". is not same as sum of room price, tax and discount" + sumInSearch);
			verify.assertTrue(Math.abs(roomRateInSearch - sumInSearch) <= 1, "For Hotel : " + hotelName + " Dates : " + checkInCheckOutDates[0] +" and " + checkInCheckOutDates[1] +" displayed room rate: " + roomRateInSearch + " and sum: " + sumInSearch + "in price break up is not same!!!");
			
	    	hotelResultsPage.clickHotelNameByIndex(hotelIndex);
	    	hotelDetailsPage.verifyHotelDetailsPagePresence();
	    	verify.assertEquals(hotelDetailsPage.getHotelNameInHotelDetailsPage().toLowerCase(),hotelName.toLowerCase());
	    	
			double[] hotelPriceInfoHD = hotelDetailsPage.getHotelPriceBreakUpInHD();
			int sumOfRateTaxDiscHDPage = (int) Math.round(hotelPriceInfoHD[0] + hotelPriceInfoHD[1] - hotelPriceInfoHD[2]);
			int hotelRoomRateInHD = hotelDetailsPage.getHotelTotalPriceInHD();
			verify.assertTrue(Math.abs((int) Math.round(hotelPriceInfoHD[3]) - sumOfRateTaxDiscHDPage) <= 1,
					"For Hotel : " + hotelName + " Dates : " + checkInCheckOutDates[0] +" and " + checkInCheckOutDates[1] +" Total : " + (int) Math.round(hotelPriceInfoHD[3]) + " and sum of room price, tax and discount :"
							+ sumOfRateTaxDiscHDPage + " does not match");
			verify.assertTrue(Math.abs(hotelRoomRateInHD - (int) Math.round(hotelPriceInfoHD[3])) <= 1,
					"For Hotel : " + hotelName + " Dates : " + checkInCheckOutDates[0] +" and " + checkInCheckOutDates[1] +" Total Displayed in HD :" + hotelRoomRateInHD + " and in break up :"
							+ (int) Math.round(hotelPriceInfoHD[3]) + " not same");
			verify.assertTrue(Math.abs(hotelPriceInSearch[0] - (int) Math.round(hotelPriceInfoHD[0])) <= 1,
					"For Hotel : " + hotelName + " Dates : " + checkInCheckOutDates[0] +" and " + checkInCheckOutDates[1] +" Room rate in Search page :" + hotelPriceInSearch[0] + " and HD Page :"
							+ (int) Math.round(hotelPriceInfoHD[0]) + " is not same");
			verify.assertTrue(Math.abs(hotelPriceInSearch[1] - (int) Math.round(hotelPriceInfoHD[1])) <= 1,
					"For Hotel : " + hotelName + " Dates : " + checkInCheckOutDates[0] +" and " + checkInCheckOutDates[1] +" Taxes in Search page: " + hotelPriceInSearch[1] + " and HD Page "
							+ (int) Math.round(hotelPriceInfoHD[1]) + " is not same");
			verify.assertTrue(Math.abs(hotelPriceInSearch[2] - (int) Math.round(hotelPriceInfoHD[2])) <= 1,
					"For Hotel : " + hotelName + " Dates : " + checkInCheckOutDates[0] +" and " + checkInCheckOutDates[1] +" Discount in Search page :" + hotelPriceInSearch[2] + " and HD Page :"
							+ (int) Math.round(hotelPriceInfoHD[2]) + " is not same");
			verify.assertTrue(Math.abs(hotelPriceInSearch[3] - (int) Math.round(hotelPriceInfoHD[3])) <= 1,
					"For Hotel : " + hotelName + " Dates : " + checkInCheckOutDates[0] +" and " + checkInCheckOutDates[1] +" Total in Search page : " + hotelPriceInSearch[3] + " and HD Page :"
							+ (int) Math.round(hotelPriceInfoHD[3]) + " is not same");
	    	
	    	browserHelper.openUrl(searchPageUrl);
	    	i = i + 1;
	    }
	    verify.assertAll();
	}
	
	@Test(groups = { "ProdPriceCheck" })
	public void testAllHotelPriceCheckInBookingPathPondicherry(){
	    homePage = new HomePage();
	    hotelResultsPage = new HotelResultsPage();
	    hotelDetailsPage = new HotelDetailsPage();
	    itineraryPage = new ItineraryPage();
	    browserHelper = new BrowserHelper();
	    CommonUtils utils = new CommonUtils();
	    SoftAssert verify = new SoftAssert();
	    
	    String location = "Pondicherry";
	    int checkInFromCurrentDate = utils.getRandomNumber(7, 30);
	    int checkOutFromCurrentDate = checkInFromCurrentDate + 1;
	    int roomIndex = 1;
	    
	    String[] checkInCheckOutDates = homePage.doSearch(location, checkInFromCurrentDate, checkOutFromCurrentDate, roomIndex);
	    hotelResultsPage.verifyHotelResultsPagePresence();
	    String searchPageUrl = browserHelper.getCurrentUrl();
	    String[] hotelNames = hotelResultsPage.getDisplayedHotelNames();
	    
	    int i = 1;
	    for (String hotelName : hotelNames){
	    	System.out.println("Currently checking for "+ i + " st/th "+" Hotel: " + hotelName);
	    	int hotelIndex = hotelResultsPage.getHotelIndexByHotelName(hotelName);
			int[] hotelPriceInSearch = hotelResultsPage.getHotelPriceBreakUpByIndex(hotelIndex);
			int roomRateInSearch = Integer.parseInt(hotelResultsPage.getHotelRoomRateByIndex(hotelIndex));
			int sumInSearch = hotelPriceInSearch[0] + hotelPriceInSearch[1] - hotelPriceInSearch[2];
			verify.assertTrue(Math.abs(hotelPriceInSearch[3] - sumInSearch) <= 1, "For Hotel : " + hotelName + " Dates : " + checkInCheckOutDates[0] +" and " + checkInCheckOutDates[1] +" Total " + hotelPriceInSearch[3]
					+ ". is not same as sum of room price, tax and discount" + sumInSearch);
			verify.assertTrue(Math.abs(roomRateInSearch - sumInSearch) <= 1, "For Hotel : " + hotelName + " Dates : " + checkInCheckOutDates[0] +" and " + checkInCheckOutDates[1] +" displayed room rate: " + roomRateInSearch + " and sum: " + sumInSearch + "in price break up is not same!!!");
			
	    	hotelResultsPage.clickHotelNameByIndex(hotelIndex);
	    	hotelDetailsPage.verifyHotelDetailsPagePresence();
	    	verify.assertEquals(hotelDetailsPage.getHotelNameInHotelDetailsPage().toLowerCase(),hotelName.toLowerCase());
	    	
			double[] hotelPriceInfoHD = hotelDetailsPage.getHotelPriceBreakUpInHD();
			int sumOfRateTaxDiscHDPage = (int) Math.round(hotelPriceInfoHD[0] + hotelPriceInfoHD[1] - hotelPriceInfoHD[2]);
			int hotelRoomRateInHD = hotelDetailsPage.getHotelTotalPriceInHD();
			verify.assertTrue(Math.abs((int) Math.round(hotelPriceInfoHD[3]) - sumOfRateTaxDiscHDPage) <= 1,
					"For Hotel : " + hotelName + " Dates : " + checkInCheckOutDates[0] +" and " + checkInCheckOutDates[1] +" Total : " + (int) Math.round(hotelPriceInfoHD[3]) + " and sum of room price, tax and discount :"
							+ sumOfRateTaxDiscHDPage + " does not match");
			verify.assertTrue(Math.abs(hotelRoomRateInHD - (int) Math.round(hotelPriceInfoHD[3])) <= 1,
					"For Hotel : " + hotelName + " Dates : " + checkInCheckOutDates[0] +" and " + checkInCheckOutDates[1] +" Total Displayed in HD :" + hotelRoomRateInHD + " and in break up :"
							+ (int) Math.round(hotelPriceInfoHD[3]) + " not same");
			verify.assertTrue(Math.abs(hotelPriceInSearch[0] - (int) Math.round(hotelPriceInfoHD[0])) <= 1,
					"For Hotel : " + hotelName + " Dates : " + checkInCheckOutDates[0] +" and " + checkInCheckOutDates[1] +" Room rate in Search page :" + hotelPriceInSearch[0] + " and HD Page :"
							+ (int) Math.round(hotelPriceInfoHD[0]) + " is not same");
			verify.assertTrue(Math.abs(hotelPriceInSearch[1] - (int) Math.round(hotelPriceInfoHD[1])) <= 1,
					"For Hotel : " + hotelName + " Dates : " + checkInCheckOutDates[0] +" and " + checkInCheckOutDates[1] +" Taxes in Search page: " + hotelPriceInSearch[1] + " and HD Page "
							+ (int) Math.round(hotelPriceInfoHD[1]) + " is not same");
			verify.assertTrue(Math.abs(hotelPriceInSearch[2] - (int) Math.round(hotelPriceInfoHD[2])) <= 1,
					"For Hotel : " + hotelName + " Dates : " + checkInCheckOutDates[0] +" and " + checkInCheckOutDates[1] +" Discount in Search page :" + hotelPriceInSearch[2] + " and HD Page :"
							+ (int) Math.round(hotelPriceInfoHD[2]) + " is not same");
			verify.assertTrue(Math.abs(hotelPriceInSearch[3] - (int) Math.round(hotelPriceInfoHD[3])) <= 1,
					"For Hotel : " + hotelName + " Dates : " + checkInCheckOutDates[0] +" and " + checkInCheckOutDates[1] +" Total in Search page : " + hotelPriceInSearch[3] + " and HD Page :"
							+ (int) Math.round(hotelPriceInfoHD[3]) + " is not same");
	    	
	    	browserHelper.openUrl(searchPageUrl);
	    	i = i + 1;
	    }
	    verify.assertAll();
	}
	
	@Test(groups = { "ProdPriceCheck" })
	public void testAllHotelPriceCheckInBookingPathNainital(){
	    homePage = new HomePage();
	    hotelResultsPage = new HotelResultsPage();
	    hotelDetailsPage = new HotelDetailsPage();
	    itineraryPage = new ItineraryPage();
	    browserHelper = new BrowserHelper();
	    CommonUtils utils = new CommonUtils();
	    SoftAssert verify = new SoftAssert();
	    
	    String location = "Nainital";
	    int checkInFromCurrentDate = utils.getRandomNumber(7, 30);
	    int checkOutFromCurrentDate = checkInFromCurrentDate + 1;
	    int roomIndex = 1;
	    
	    String[] checkInCheckOutDates = homePage.doSearch(location, checkInFromCurrentDate, checkOutFromCurrentDate, roomIndex);
	    hotelResultsPage.verifyHotelResultsPagePresence();
	    String searchPageUrl = browserHelper.getCurrentUrl();
	    String[] hotelNames = hotelResultsPage.getDisplayedHotelNames();
	    
	    int i = 1;
	    for (String hotelName : hotelNames){
	    	System.out.println("Currently checking for "+ i + " st/th "+" Hotel: " + hotelName);
	    	int hotelIndex = hotelResultsPage.getHotelIndexByHotelName(hotelName);
			int[] hotelPriceInSearch = hotelResultsPage.getHotelPriceBreakUpByIndex(hotelIndex);
			int roomRateInSearch = Integer.parseInt(hotelResultsPage.getHotelRoomRateByIndex(hotelIndex));
			int sumInSearch = hotelPriceInSearch[0] + hotelPriceInSearch[1] - hotelPriceInSearch[2];
			verify.assertTrue(Math.abs(hotelPriceInSearch[3] - sumInSearch) <= 1, "For Hotel : " + hotelName + " Dates : " + checkInCheckOutDates[0] +" and " + checkInCheckOutDates[1] +" Total " + hotelPriceInSearch[3]
					+ ". is not same as sum of room price, tax and discount" + sumInSearch);
			verify.assertTrue(Math.abs(roomRateInSearch - sumInSearch) <= 1, "For Hotel : " + hotelName + " Dates : " + checkInCheckOutDates[0] +" and " + checkInCheckOutDates[1] +" displayed room rate: " + roomRateInSearch + " and sum: " + sumInSearch + "in price break up is not same!!!");
			
	    	hotelResultsPage.clickHotelNameByIndex(hotelIndex);
	    	hotelDetailsPage.verifyHotelDetailsPagePresence();
	    	verify.assertEquals(hotelDetailsPage.getHotelNameInHotelDetailsPage().toLowerCase(),hotelName.toLowerCase());
	    	
			double[] hotelPriceInfoHD = hotelDetailsPage.getHotelPriceBreakUpInHD();
			int sumOfRateTaxDiscHDPage = (int) Math.round(hotelPriceInfoHD[0] + hotelPriceInfoHD[1] - hotelPriceInfoHD[2]);
			int hotelRoomRateInHD = hotelDetailsPage.getHotelTotalPriceInHD();
			verify.assertTrue(Math.abs((int) Math.round(hotelPriceInfoHD[3]) - sumOfRateTaxDiscHDPage) <= 1,
					"For Hotel : " + hotelName + " Dates : " + checkInCheckOutDates[0] +" and " + checkInCheckOutDates[1] +" Total : " + (int) Math.round(hotelPriceInfoHD[3]) + " and sum of room price, tax and discount :"
							+ sumOfRateTaxDiscHDPage + " does not match");
			verify.assertTrue(Math.abs(hotelRoomRateInHD - (int) Math.round(hotelPriceInfoHD[3])) <= 1,
					"For Hotel : " + hotelName + " Dates : " + checkInCheckOutDates[0] +" and " + checkInCheckOutDates[1] +" Total Displayed in HD :" + hotelRoomRateInHD + " and in break up :"
							+ (int) Math.round(hotelPriceInfoHD[3]) + " not same");
			verify.assertTrue(Math.abs(hotelPriceInSearch[0] - (int) Math.round(hotelPriceInfoHD[0])) <= 1,
					"For Hotel : " + hotelName + " Dates : " + checkInCheckOutDates[0] +" and " + checkInCheckOutDates[1] +" Room rate in Search page :" + hotelPriceInSearch[0] + " and HD Page :"
							+ (int) Math.round(hotelPriceInfoHD[0]) + " is not same");
			verify.assertTrue(Math.abs(hotelPriceInSearch[1] - (int) Math.round(hotelPriceInfoHD[1])) <= 1,
					"For Hotel : " + hotelName + " Dates : " + checkInCheckOutDates[0] +" and " + checkInCheckOutDates[1] +" Taxes in Search page: " + hotelPriceInSearch[1] + " and HD Page "
							+ (int) Math.round(hotelPriceInfoHD[1]) + " is not same");
			verify.assertTrue(Math.abs(hotelPriceInSearch[2] - (int) Math.round(hotelPriceInfoHD[2])) <= 1,
					"For Hotel : " + hotelName + " Dates : " + checkInCheckOutDates[0] +" and " + checkInCheckOutDates[1] +" Discount in Search page :" + hotelPriceInSearch[2] + " and HD Page :"
							+ (int) Math.round(hotelPriceInfoHD[2]) + " is not same");
			verify.assertTrue(Math.abs(hotelPriceInSearch[3] - (int) Math.round(hotelPriceInfoHD[3])) <= 1,
					"For Hotel : " + hotelName + " Dates : " + checkInCheckOutDates[0] +" and " + checkInCheckOutDates[1] +" Total in Search page : " + hotelPriceInSearch[3] + " and HD Page :"
							+ (int) Math.round(hotelPriceInfoHD[3]) + " is not same");
	    	
	    	browserHelper.openUrl(searchPageUrl);
	    	i = i + 1;
	    }
	    verify.assertAll();
	}
	
	@Test(groups = { "ProdPriceCheck" })
	public void testAllHotelPriceCheckInBookingPathShirdi(){
	    homePage = new HomePage();
	    hotelResultsPage = new HotelResultsPage();
	    hotelDetailsPage = new HotelDetailsPage();
	    itineraryPage = new ItineraryPage();
	    browserHelper = new BrowserHelper();
	    CommonUtils utils = new CommonUtils();
	    SoftAssert verify = new SoftAssert();
	    
	    String location = "Shirdi";
	    int checkInFromCurrentDate = utils.getRandomNumber(7, 30);
	    int checkOutFromCurrentDate = checkInFromCurrentDate + 1;
	    int roomIndex = 1;
	    
	    String[] checkInCheckOutDates = homePage.doSearch(location, checkInFromCurrentDate, checkOutFromCurrentDate, roomIndex);
	    hotelResultsPage.verifyHotelResultsPagePresence();
	    String searchPageUrl = browserHelper.getCurrentUrl();
	    String[] hotelNames = hotelResultsPage.getDisplayedHotelNames();
	    
	    int i = 1;
	    for (String hotelName : hotelNames){
	    	System.out.println("Currently checking for "+ i + " st/th "+" Hotel: " + hotelName);
	    	int hotelIndex = hotelResultsPage.getHotelIndexByHotelName(hotelName);
			int[] hotelPriceInSearch = hotelResultsPage.getHotelPriceBreakUpByIndex(hotelIndex);
			int roomRateInSearch = Integer.parseInt(hotelResultsPage.getHotelRoomRateByIndex(hotelIndex));
			int sumInSearch = hotelPriceInSearch[0] + hotelPriceInSearch[1] - hotelPriceInSearch[2];
			verify.assertTrue(Math.abs(hotelPriceInSearch[3] - sumInSearch) <= 1, "For Hotel : " + hotelName + " Dates : " + checkInCheckOutDates[0] +" and " + checkInCheckOutDates[1] +" Total " + hotelPriceInSearch[3]
					+ ". is not same as sum of room price, tax and discount" + sumInSearch);
			verify.assertTrue(Math.abs(roomRateInSearch - sumInSearch) <= 1, "For Hotel : " + hotelName + " Dates : " + checkInCheckOutDates[0] +" and " + checkInCheckOutDates[1] +" displayed room rate: " + roomRateInSearch + " and sum: " + sumInSearch + "in price break up is not same!!!");
			
	    	hotelResultsPage.clickHotelNameByIndex(hotelIndex);
	    	hotelDetailsPage.verifyHotelDetailsPagePresence();
	    	verify.assertEquals(hotelDetailsPage.getHotelNameInHotelDetailsPage().toLowerCase(),hotelName.toLowerCase());
	    	
			double[] hotelPriceInfoHD = hotelDetailsPage.getHotelPriceBreakUpInHD();
			int sumOfRateTaxDiscHDPage = (int) Math.round(hotelPriceInfoHD[0] + hotelPriceInfoHD[1] - hotelPriceInfoHD[2]);
			int hotelRoomRateInHD = hotelDetailsPage.getHotelTotalPriceInHD();
			verify.assertTrue(Math.abs((int) Math.round(hotelPriceInfoHD[3]) - sumOfRateTaxDiscHDPage) <= 1,
					"For Hotel : " + hotelName + " Dates : " + checkInCheckOutDates[0] +" and " + checkInCheckOutDates[1] +" Total : " + (int) Math.round(hotelPriceInfoHD[3]) + " and sum of room price, tax and discount :"
							+ sumOfRateTaxDiscHDPage + " does not match");
			verify.assertTrue(Math.abs(hotelRoomRateInHD - (int) Math.round(hotelPriceInfoHD[3])) <= 1,
					"For Hotel : " + hotelName + " Dates : " + checkInCheckOutDates[0] +" and " + checkInCheckOutDates[1] +" Total Displayed in HD :" + hotelRoomRateInHD + " and in break up :"
							+ (int) Math.round(hotelPriceInfoHD[3]) + " not same");
			verify.assertTrue(Math.abs(hotelPriceInSearch[0] - (int) Math.round(hotelPriceInfoHD[0])) <= 1,
					"For Hotel : " + hotelName + " Dates : " + checkInCheckOutDates[0] +" and " + checkInCheckOutDates[1] +" Room rate in Search page :" + hotelPriceInSearch[0] + " and HD Page :"
							+ (int) Math.round(hotelPriceInfoHD[0]) + " is not same");
			verify.assertTrue(Math.abs(hotelPriceInSearch[1] - (int) Math.round(hotelPriceInfoHD[1])) <= 1,
					"For Hotel : " + hotelName + " Dates : " + checkInCheckOutDates[0] +" and " + checkInCheckOutDates[1] +" Taxes in Search page: " + hotelPriceInSearch[1] + " and HD Page "
							+ (int) Math.round(hotelPriceInfoHD[1]) + " is not same");
			verify.assertTrue(Math.abs(hotelPriceInSearch[2] - (int) Math.round(hotelPriceInfoHD[2])) <= 1,
					"For Hotel : " + hotelName + " Dates : " + checkInCheckOutDates[0] +" and " + checkInCheckOutDates[1] +" Discount in Search page :" + hotelPriceInSearch[2] + " and HD Page :"
							+ (int) Math.round(hotelPriceInfoHD[2]) + " is not same");
			verify.assertTrue(Math.abs(hotelPriceInSearch[3] - (int) Math.round(hotelPriceInfoHD[3])) <= 1,
					"For Hotel : " + hotelName + " Dates : " + checkInCheckOutDates[0] +" and " + checkInCheckOutDates[1] +" Total in Search page : " + hotelPriceInSearch[3] + " and HD Page :"
							+ (int) Math.round(hotelPriceInfoHD[3]) + " is not same");
	    	
	    	browserHelper.openUrl(searchPageUrl);
	    	i = i + 1;
	    }
	    verify.assertAll();
	}
	
	@Test(groups = { "ProdPriceCheck" })
	public void testAllHotelPriceCheckInBookingPathKochi(){
	    homePage = new HomePage();
	    hotelResultsPage = new HotelResultsPage();
	    hotelDetailsPage = new HotelDetailsPage();
	    itineraryPage = new ItineraryPage();
	    browserHelper = new BrowserHelper();
	    CommonUtils utils = new CommonUtils();
	    SoftAssert verify = new SoftAssert();
	    
	    String location = "Kochi";
	    int checkInFromCurrentDate = utils.getRandomNumber(7, 30);
	    int checkOutFromCurrentDate = checkInFromCurrentDate + 1;
	    int roomIndex = 1;
	    
	    String[] checkInCheckOutDates = homePage.doSearch(location, checkInFromCurrentDate, checkOutFromCurrentDate, roomIndex);
	    hotelResultsPage.verifyHotelResultsPagePresence();
	    String searchPageUrl = browserHelper.getCurrentUrl();
	    String[] hotelNames = hotelResultsPage.getDisplayedHotelNames();
	    
	    int i = 1;
	    for (String hotelName : hotelNames){
	    	System.out.println("Currently checking for "+ i + " st/th "+" Hotel: " + hotelName);
	    	int hotelIndex = hotelResultsPage.getHotelIndexByHotelName(hotelName);
			int[] hotelPriceInSearch = hotelResultsPage.getHotelPriceBreakUpByIndex(hotelIndex);
			int roomRateInSearch = Integer.parseInt(hotelResultsPage.getHotelRoomRateByIndex(hotelIndex));
			int sumInSearch = hotelPriceInSearch[0] + hotelPriceInSearch[1] - hotelPriceInSearch[2];
			verify.assertTrue(Math.abs(hotelPriceInSearch[3] - sumInSearch) <= 1, "For Hotel : " + hotelName + " Dates : " + checkInCheckOutDates[0] +" and " + checkInCheckOutDates[1] +" Total " + hotelPriceInSearch[3]
					+ ". is not same as sum of room price, tax and discount" + sumInSearch);
			verify.assertTrue(Math.abs(roomRateInSearch - sumInSearch) <= 1, "For Hotel : " + hotelName + " Dates : " + checkInCheckOutDates[0] +" and " + checkInCheckOutDates[1] +" displayed room rate: " + roomRateInSearch + " and sum: " + sumInSearch + "in price break up is not same!!!");
			
	    	hotelResultsPage.clickHotelNameByIndex(hotelIndex);
	    	hotelDetailsPage.verifyHotelDetailsPagePresence();
	    	verify.assertEquals(hotelDetailsPage.getHotelNameInHotelDetailsPage().toLowerCase(),hotelName.toLowerCase());
	    	
			double[] hotelPriceInfoHD = hotelDetailsPage.getHotelPriceBreakUpInHD();
			int sumOfRateTaxDiscHDPage = (int) Math.round(hotelPriceInfoHD[0] + hotelPriceInfoHD[1] - hotelPriceInfoHD[2]);
			int hotelRoomRateInHD = hotelDetailsPage.getHotelTotalPriceInHD();
			verify.assertTrue(Math.abs((int) Math.round(hotelPriceInfoHD[3]) - sumOfRateTaxDiscHDPage) <= 1,
					"For Hotel : " + hotelName + " Dates : " + checkInCheckOutDates[0] +" and " + checkInCheckOutDates[1] +" Total : " + (int) Math.round(hotelPriceInfoHD[3]) + " and sum of room price, tax and discount :"
							+ sumOfRateTaxDiscHDPage + " does not match");
			verify.assertTrue(Math.abs(hotelRoomRateInHD - (int) Math.round(hotelPriceInfoHD[3])) <= 1,
					"For Hotel : " + hotelName + " Dates : " + checkInCheckOutDates[0] +" and " + checkInCheckOutDates[1] +" Total Displayed in HD :" + hotelRoomRateInHD + " and in break up :"
							+ (int) Math.round(hotelPriceInfoHD[3]) + " not same");
			verify.assertTrue(Math.abs(hotelPriceInSearch[0] - (int) Math.round(hotelPriceInfoHD[0])) <= 1,
					"For Hotel : " + hotelName + " Dates : " + checkInCheckOutDates[0] +" and " + checkInCheckOutDates[1] +" Room rate in Search page :" + hotelPriceInSearch[0] + " and HD Page :"
							+ (int) Math.round(hotelPriceInfoHD[0]) + " is not same");
			verify.assertTrue(Math.abs(hotelPriceInSearch[1] - (int) Math.round(hotelPriceInfoHD[1])) <= 1,
					"For Hotel : " + hotelName + " Dates : " + checkInCheckOutDates[0] +" and " + checkInCheckOutDates[1] +" Taxes in Search page: " + hotelPriceInSearch[1] + " and HD Page "
							+ (int) Math.round(hotelPriceInfoHD[1]) + " is not same");
			verify.assertTrue(Math.abs(hotelPriceInSearch[2] - (int) Math.round(hotelPriceInfoHD[2])) <= 1,
					"For Hotel : " + hotelName + " Dates : " + checkInCheckOutDates[0] +" and " + checkInCheckOutDates[1] +" Discount in Search page :" + hotelPriceInSearch[2] + " and HD Page :"
							+ (int) Math.round(hotelPriceInfoHD[2]) + " is not same");
			verify.assertTrue(Math.abs(hotelPriceInSearch[3] - (int) Math.round(hotelPriceInfoHD[3])) <= 1,
					"For Hotel : " + hotelName + " Dates : " + checkInCheckOutDates[0] +" and " + checkInCheckOutDates[1] +" Total in Search page : " + hotelPriceInSearch[3] + " and HD Page :"
							+ (int) Math.round(hotelPriceInfoHD[3]) + " is not same");
	    	
	    	browserHelper.openUrl(searchPageUrl);
	    	i = i + 1;
	    }
	    verify.assertAll();
	}
	
	@Test(groups = { "ProdPriceCheck" })
	public void testAllHotelPriceCheckInBookingPathThiruvananthapuram(){
	    homePage = new HomePage();
	    hotelResultsPage = new HotelResultsPage();
	    hotelDetailsPage = new HotelDetailsPage();
	    itineraryPage = new ItineraryPage();
	    browserHelper = new BrowserHelper();
	    CommonUtils utils = new CommonUtils();
	    SoftAssert verify = new SoftAssert();
	    
	    String location = "Thiruvananthapuram";
	    int checkInFromCurrentDate = utils.getRandomNumber(7, 30);
	    int checkOutFromCurrentDate = checkInFromCurrentDate + 1;
	    int roomIndex = 1;
	    
	    String[] checkInCheckOutDates = homePage.doSearch(location, checkInFromCurrentDate, checkOutFromCurrentDate, roomIndex);
	    hotelResultsPage.verifyHotelResultsPagePresence();
	    String searchPageUrl = browserHelper.getCurrentUrl();
	    String[] hotelNames = hotelResultsPage.getDisplayedHotelNames();
	    
	    int i = 1;
	    for (String hotelName : hotelNames){
	    	System.out.println("Currently checking for "+ i + " st/th "+" Hotel: " + hotelName);
	    	int hotelIndex = hotelResultsPage.getHotelIndexByHotelName(hotelName);
			int[] hotelPriceInSearch = hotelResultsPage.getHotelPriceBreakUpByIndex(hotelIndex);
			int roomRateInSearch = Integer.parseInt(hotelResultsPage.getHotelRoomRateByIndex(hotelIndex));
			int sumInSearch = hotelPriceInSearch[0] + hotelPriceInSearch[1] - hotelPriceInSearch[2];
			verify.assertTrue(Math.abs(hotelPriceInSearch[3] - sumInSearch) <= 1, "For Hotel : " + hotelName + " Dates : " + checkInCheckOutDates[0] +" and " + checkInCheckOutDates[1] +" Total " + hotelPriceInSearch[3]
					+ ". is not same as sum of room price, tax and discount" + sumInSearch);
			verify.assertTrue(Math.abs(roomRateInSearch - sumInSearch) <= 1, "For Hotel : " + hotelName + " Dates : " + checkInCheckOutDates[0] +" and " + checkInCheckOutDates[1] +" displayed room rate: " + roomRateInSearch + " and sum: " + sumInSearch + "in price break up is not same!!!");
			
	    	hotelResultsPage.clickHotelNameByIndex(hotelIndex);
	    	hotelDetailsPage.verifyHotelDetailsPagePresence();
	    	verify.assertEquals(hotelDetailsPage.getHotelNameInHotelDetailsPage().toLowerCase(),hotelName.toLowerCase());
	    	
			double[] hotelPriceInfoHD = hotelDetailsPage.getHotelPriceBreakUpInHD();
			int sumOfRateTaxDiscHDPage = (int) Math.round(hotelPriceInfoHD[0] + hotelPriceInfoHD[1] - hotelPriceInfoHD[2]);
			int hotelRoomRateInHD = hotelDetailsPage.getHotelTotalPriceInHD();
			verify.assertTrue(Math.abs((int) Math.round(hotelPriceInfoHD[3]) - sumOfRateTaxDiscHDPage) <= 1,
					"For Hotel : " + hotelName + " Dates : " + checkInCheckOutDates[0] +" and " + checkInCheckOutDates[1] +" Total : " + (int) Math.round(hotelPriceInfoHD[3]) + " and sum of room price, tax and discount :"
							+ sumOfRateTaxDiscHDPage + " does not match");
			verify.assertTrue(Math.abs(hotelRoomRateInHD - (int) Math.round(hotelPriceInfoHD[3])) <= 1,
					"For Hotel : " + hotelName + " Dates : " + checkInCheckOutDates[0] +" and " + checkInCheckOutDates[1] +" Total Displayed in HD :" + hotelRoomRateInHD + " and in break up :"
							+ (int) Math.round(hotelPriceInfoHD[3]) + " not same");
			verify.assertTrue(Math.abs(hotelPriceInSearch[0] - (int) Math.round(hotelPriceInfoHD[0])) <= 1,
					"For Hotel : " + hotelName + " Dates : " + checkInCheckOutDates[0] +" and " + checkInCheckOutDates[1] +" Room rate in Search page :" + hotelPriceInSearch[0] + " and HD Page :"
							+ (int) Math.round(hotelPriceInfoHD[0]) + " is not same");
			verify.assertTrue(Math.abs(hotelPriceInSearch[1] - (int) Math.round(hotelPriceInfoHD[1])) <= 1,
					"For Hotel : " + hotelName + " Dates : " + checkInCheckOutDates[0] +" and " + checkInCheckOutDates[1] +" Taxes in Search page: " + hotelPriceInSearch[1] + " and HD Page "
							+ (int) Math.round(hotelPriceInfoHD[1]) + " is not same");
			verify.assertTrue(Math.abs(hotelPriceInSearch[2] - (int) Math.round(hotelPriceInfoHD[2])) <= 1,
					"For Hotel : " + hotelName + " Dates : " + checkInCheckOutDates[0] +" and " + checkInCheckOutDates[1] +" Discount in Search page :" + hotelPriceInSearch[2] + " and HD Page :"
							+ (int) Math.round(hotelPriceInfoHD[2]) + " is not same");
			verify.assertTrue(Math.abs(hotelPriceInSearch[3] - (int) Math.round(hotelPriceInfoHD[3])) <= 1,
					"For Hotel : " + hotelName + " Dates : " + checkInCheckOutDates[0] +" and " + checkInCheckOutDates[1] +" Total in Search page : " + hotelPriceInSearch[3] + " and HD Page :"
							+ (int) Math.round(hotelPriceInfoHD[3]) + " is not same");
	    	
	    	browserHelper.openUrl(searchPageUrl);
	    	i = i + 1;
	    }
	    verify.assertAll();
	}
	
	@Test(groups = { "ProdPriceCheck" })
	public void testAllHotelPriceCheckInBookingPathAmritsar(){
	    homePage = new HomePage();
	    hotelResultsPage = new HotelResultsPage();
	    hotelDetailsPage = new HotelDetailsPage();
	    itineraryPage = new ItineraryPage();
	    browserHelper = new BrowserHelper();
	    CommonUtils utils = new CommonUtils();
	    SoftAssert verify = new SoftAssert();
	    
	    String location = "Amritsar";
	    int checkInFromCurrentDate = utils.getRandomNumber(7, 30);
	    int checkOutFromCurrentDate = checkInFromCurrentDate + 1;
	    int roomIndex = 1;
	    
	    String[] checkInCheckOutDates = homePage.doSearch(location, checkInFromCurrentDate, checkOutFromCurrentDate, roomIndex);
	    hotelResultsPage.verifyHotelResultsPagePresence();
	    String searchPageUrl = browserHelper.getCurrentUrl();
	    String[] hotelNames = hotelResultsPage.getDisplayedHotelNames();
	    
	    int i = 1;
	    for (String hotelName : hotelNames){
	    	System.out.println("Currently checking for "+ i + " st/th "+" Hotel: " + hotelName);
	    	int hotelIndex = hotelResultsPage.getHotelIndexByHotelName(hotelName);
			int[] hotelPriceInSearch = hotelResultsPage.getHotelPriceBreakUpByIndex(hotelIndex);
			int roomRateInSearch = Integer.parseInt(hotelResultsPage.getHotelRoomRateByIndex(hotelIndex));
			int sumInSearch = hotelPriceInSearch[0] + hotelPriceInSearch[1] - hotelPriceInSearch[2];
			verify.assertTrue(Math.abs(hotelPriceInSearch[3] - sumInSearch) <= 1, "For Hotel : " + hotelName + " Dates : " + checkInCheckOutDates[0] +" and " + checkInCheckOutDates[1] +" Total " + hotelPriceInSearch[3]
					+ ". is not same as sum of room price, tax and discount" + sumInSearch);
			verify.assertTrue(Math.abs(roomRateInSearch - sumInSearch) <= 1, "For Hotel : " + hotelName + " Dates : " + checkInCheckOutDates[0] +" and " + checkInCheckOutDates[1] +" displayed room rate: " + roomRateInSearch + " and sum: " + sumInSearch + "in price break up is not same!!!");
			
	    	hotelResultsPage.clickHotelNameByIndex(hotelIndex);
	    	hotelDetailsPage.verifyHotelDetailsPagePresence();
	    	verify.assertEquals(hotelDetailsPage.getHotelNameInHotelDetailsPage().toLowerCase(),hotelName.toLowerCase());
	    	
			double[] hotelPriceInfoHD = hotelDetailsPage.getHotelPriceBreakUpInHD();
			int sumOfRateTaxDiscHDPage = (int) Math.round(hotelPriceInfoHD[0] + hotelPriceInfoHD[1] - hotelPriceInfoHD[2]);
			int hotelRoomRateInHD = hotelDetailsPage.getHotelTotalPriceInHD();
			verify.assertTrue(Math.abs((int) Math.round(hotelPriceInfoHD[3]) - sumOfRateTaxDiscHDPage) <= 1,
					"For Hotel : " + hotelName + " Dates : " + checkInCheckOutDates[0] +" and " + checkInCheckOutDates[1] +" Total : " + (int) Math.round(hotelPriceInfoHD[3]) + " and sum of room price, tax and discount :"
							+ sumOfRateTaxDiscHDPage + " does not match");
			verify.assertTrue(Math.abs(hotelRoomRateInHD - (int) Math.round(hotelPriceInfoHD[3])) <= 1,
					"For Hotel : " + hotelName + " Dates : " + checkInCheckOutDates[0] +" and " + checkInCheckOutDates[1] +" Total Displayed in HD :" + hotelRoomRateInHD + " and in break up :"
							+ (int) Math.round(hotelPriceInfoHD[3]) + " not same");
			verify.assertTrue(Math.abs(hotelPriceInSearch[0] - (int) Math.round(hotelPriceInfoHD[0])) <= 1,
					"For Hotel : " + hotelName + " Dates : " + checkInCheckOutDates[0] +" and " + checkInCheckOutDates[1] +" Room rate in Search page :" + hotelPriceInSearch[0] + " and HD Page :"
							+ (int) Math.round(hotelPriceInfoHD[0]) + " is not same");
			verify.assertTrue(Math.abs(hotelPriceInSearch[1] - (int) Math.round(hotelPriceInfoHD[1])) <= 1,
					"For Hotel : " + hotelName + " Dates : " + checkInCheckOutDates[0] +" and " + checkInCheckOutDates[1] +" Taxes in Search page: " + hotelPriceInSearch[1] + " and HD Page "
							+ (int) Math.round(hotelPriceInfoHD[1]) + " is not same");
			verify.assertTrue(Math.abs(hotelPriceInSearch[2] - (int) Math.round(hotelPriceInfoHD[2])) <= 1,
					"For Hotel : " + hotelName + " Dates : " + checkInCheckOutDates[0] +" and " + checkInCheckOutDates[1] +" Discount in Search page :" + hotelPriceInSearch[2] + " and HD Page :"
							+ (int) Math.round(hotelPriceInfoHD[2]) + " is not same");
			verify.assertTrue(Math.abs(hotelPriceInSearch[3] - (int) Math.round(hotelPriceInfoHD[3])) <= 1,
					"For Hotel : " + hotelName + " Dates : " + checkInCheckOutDates[0] +" and " + checkInCheckOutDates[1] +" Total in Search page : " + hotelPriceInSearch[3] + " and HD Page :"
							+ (int) Math.round(hotelPriceInfoHD[3]) + " is not same");
	    	
	    	browserHelper.openUrl(searchPageUrl);
	    	i = i + 1;
	    }
	    verify.assertAll();
	}
}