package mobileSiteUIPageObjects;

import java.util.List;

import org.apache.commons.lang3.RandomStringUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import base.BrowserHelper;
import base.DriverManager;
import mobileSiteUIPageObjects.HomeScreen;
import utils.CommonUtils;

public class ItineraryScreen {
	private BrowserHelper browserHelper;
	private HomeScreen homeScreen;

	public boolean isItineraryScreenDisplayed() {
		WebDriver driver = DriverManager.getInstance().getDriver();
		
		homeScreen = new HomeScreen();
		browserHelper = new BrowserHelper();
		
		//check if 500 page
		if (homeScreen.is500Displayed()){
			Assert.fail("500 displayed instead of itinerary screen for url " + browserHelper.getCurrentUrl());
		}
		
		WebDriverWait wait = new WebDriverWait(driver, 120);
		try{
			wait.until(ExpectedConditions
					.presenceOfElementLocated(By.cssSelector(".itinerary-page__review__heading .heading")));
		}catch (Exception e){
			Assert.fail("Itinerary screen not displayed! for url " + browserHelper.getCurrentUrl());
		}
		
		return driver.findElement(By.cssSelector(".itinerary-page__review__heading .heading")).isDisplayed();
	}

	public double getTotalCost() {
		WebDriver driver = DriverManager.getInstance().getDriver();
		return Double.parseDouble(driver.findElement(By.cssSelector("#grandTotal")).getText().trim());
	}

	public double getBasePrice() {
		WebDriver driver = DriverManager.getInstance().getDriver();
		return Double.parseDouble(driver.findElement(By.cssSelector(".pretax-price")).getText().trim());
	}

	public double getTax() {
		WebDriver driver = DriverManager.getInstance().getDriver();
		return Double.parseDouble(driver.findElement(By.cssSelector("#totalTax")).getText().trim());
	}

	public double getDiscount() {
		WebDriver driver = DriverManager.getInstance().getDriver();
		return Double.parseDouble(driver.findElement(By.cssSelector("#discountValue")).getText().trim());
	}

	public void applyCoupon(String couponName) {
		WebDriver driver = DriverManager.getInstance().getDriver();
		browserHelper = new BrowserHelper();
		driver.findElement(By.cssSelector(".analytics-voucher")).sendKeys(couponName);
		driver.findElement(By.cssSelector(".discount__applybtn")).click();
		browserHelper.waitTime(1000);
		WebDriverWait wait = new WebDriverWait(driver, 120);
		try {
			wait.until(ExpectedConditions
					.presenceOfElementLocated(By.cssSelector("#discountCouponContainer .loader-container.hide")));
		} catch (Exception e) {
			// Do nothing
		}
	}

	public double getCouponAppliedValue() {
		WebDriver driver = DriverManager.getInstance().getDriver();
		return Double.parseDouble(driver.findElement(By.cssSelector(".analytics-discountvalue")).getText().trim());
	}

	public void removeCouponApplied() {
		WebDriver driver = DriverManager.getInstance().getDriver();
		browserHelper = new BrowserHelper();
		driver.findElement(By.xpath("//a[text()='remove']")).click();
		browserHelper.waitTime(30000);
	}

	public boolean invalidCouponErrorDisplayed() {
		WebDriver driver = DriverManager.getInstance().getDriver();
		return driver.findElement(By.cssSelector("#discountError")).isDisplayed();
	}

	public boolean invalidCouponErrorIsAsExpected() {
		WebDriver driver = DriverManager.getInstance().getDriver();
		System.out.println(
				"Invalid coupon error text : " + driver.findElement(By.cssSelector("#discountError")).getText());
		return (driver.findElement(By.cssSelector("#discountError")).getText()
				.equals("Sorry! This coupon code is not valid"));
	}

	public String getHotelName() {
		WebDriver driver = DriverManager.getInstance().getDriver();
		return driver.findElement(By.cssSelector(".itinerary-page__review__content__hotel__info h3")).getText();
	}

	public String getItineraryCheckInDate() {
		WebDriver driver = DriverManager.getInstance().getDriver();
		return driver.findElement(By.cssSelector(".analytics-checkin")).getText();
	}

	public String getItineraryCheckOutDate() {
		WebDriver driver = DriverManager.getInstance().getDriver();
		return driver.findElement(By.cssSelector(".analytics-checkout")).getText();
	}

	public String getGuestName() {
		WebDriver driver = DriverManager.getInstance().getDriver();
		return driver.findElement(By.cssSelector("#guestName")).getAttribute("value");
	}

	public String getGuestMobile() {
		WebDriver driver = DriverManager.getInstance().getDriver();
		return driver.findElement(By.cssSelector("#guestMobile")).getAttribute("value");
	}

	public String getGuestEmail() {
		WebDriver driver = DriverManager.getInstance().getDriver();
		return driver.findElement(By.cssSelector("#guestEmail")).getAttribute("value");
	}

	// public String getItinerayRoomType(){
	// WebDriver driver = DriverManager.getInstance().getDriver();
	// return
	// driver.findElement(By.cssSelector(".itinerary-page__review__content__hotel__info
	// h3")).getText();
	// }
	//
	// public int getItinerayDuration(){
	// WebDriver driver = DriverManager.getInstance().getDriver();
	// return
	// driver.findElement(By.cssSelector(".itinerary-page__review__content__hotel__info
	// h3")).getText();
	// }
	//
	// public int getItinerayNumOfGuests(){
	// WebDriver driver = DriverManager.getInstance().getDriver();
	// return
	// driver.findElement(By.cssSelector(".itinerary-page__review__content__hotel__info
	// h3")).getText();
	// }
	//
	// public int getItinerayNumOfRooms(){
	// WebDriver driver = DriverManager.getInstance().getDriver();
	// return
	// driver.findElement(By.cssSelector(".itinerary-page__review__content__hotel__info
	// h3")).getText();
	// }

	public void clickOnNextStepOne() {
		WebDriver driver = DriverManager.getInstance().getDriver();
		driver.findElement(By.cssSelector(".analytics-next")).click();
	}

	public void clickOnNextOnStepTwo() {
		WebDriver driver = DriverManager.getInstance().getDriver();
		driver.findElement(By.cssSelector(".itinerary-page__action__button")).click();
	}

	public void loginAsTreeboMember(String strEmail, String strPassword) {
		WebDriver driver = DriverManager.getInstance().getDriver();
		browserHelper = new BrowserHelper();
		driver.findElement(By.cssSelector("#loginEmailInput")).sendKeys(strEmail);
		driver.findElement(By.cssSelector("#loginPassowrdlInput")).sendKeys(strPassword);
		driver.findElement(By.cssSelector("#loginButton")).click();
		browserHelper.waitTime(3000);
	}

	public void continueAsGuest() {
		WebDriver driver = DriverManager.getInstance().getDriver();
		browserHelper = new BrowserHelper();
		driver.findElement(By.cssSelector("#countinueGuest")).click();
		browserHelper.waitTime(3000);
	}

	public void enterGuestDetails(String strName, String strMobile, String strEmail) {
		WebDriver driver = DriverManager.getInstance().getDriver();
		driver.findElement(By.cssSelector("#guestName")).sendKeys(strName);
		driver.findElement(By.cssSelector("#guestMobile")).sendKeys(strMobile);
		driver.findElement(By.cssSelector("#guestEmail")).sendKeys(strEmail);
	}

	public void clickOnRegister() {
		browserHelper = new BrowserHelper();
		WebDriver driver = DriverManager.getInstance().getDriver();
		driver.findElement(By.cssSelector(".login__register")).click();
		browserHelper.waitTime(3000);
	}

	public void signUpOnItineraryScreen(String strName, String strMobile, String strEmail, String strPassword) {
		WebDriver driver = DriverManager.getInstance().getDriver();
		browserHelper = new BrowserHelper();
		homeScreen = new HomeScreen();
		driver.findElement(By.cssSelector("#signupNamelInput")).sendKeys(strName);
		driver.findElement(By.cssSelector("#signupMobileInput")).sendKeys(strMobile);
		driver.findElement(By.cssSelector("#signupEmailInput")).sendKeys(strEmail);
		driver.findElement(By.cssSelector("#signupPassowrdlInput")).sendKeys(strPassword);
		driver.findElement(By.cssSelector("#signupButton")).click();
		homeScreen.waitForLoaderToDisappear();
		browserHelper.waitTime(3000);
	}

	public String[] registerOnItineraryScreen() {
		CommonUtils utils = new CommonUtils();
		String name = "Test";
//		String uniqueEmail = "test" + RandomStringUtils.randomAlphanumeric(15);
//		String email = uniqueEmail + "@gmail.com";
		browserHelper = new BrowserHelper();
		String email = browserHelper.getDisposableEmail();
		String mobile = Integer.toString(utils.getRandomNumber(2, 6)) + RandomStringUtils.randomNumeric(9);
		String password = "password";
		String[] regInfo = { name, email, mobile, password };
		signUpOnItineraryScreen(name, mobile, email, password);

		System.out.println("Registered with name : " + name + " email : " + email + " mobile : " + mobile
				+ " password : " + password);
		return regInfo;
	}

	public void clickOnPayAtHotel() {
		WebDriver driver = DriverManager.getInstance().getDriver();
		driver.findElement(By.cssSelector("#payatHotel")).click();
	}

	public void clickOnPayNow() {
		WebDriver driver = DriverManager.getInstance().getDriver();
		driver.findElement(By.cssSelector("#payNow")).click();
	}

	public boolean isBackButtonThere() {
		WebDriver driver = DriverManager.getInstance().getDriver();
		return (driver.findElements(By.cssSelector(".back.js-back-link.sub-heading.itinerary-steps__heading"))
				.size() > 0);
	}

	public void clickOnBackOnItineraryScreen() {
		WebDriver driver = DriverManager.getInstance().getDriver();
		driver.findElement(By.cssSelector(".back.js-back-link.sub-heading.itinerary-steps__heading")).click();
	}

	public boolean isRazorPayScreenDisplayed() {
		WebDriver driver = DriverManager.getInstance().getDriver();
		return driver.findElement(By.cssSelector(".merchant-image")).isDisplayed();
	}

	public void closeRazorPayScreen() {
		WebDriver driver = DriverManager.getInstance().getDriver();
		driver.findElement(By.cssSelector("#modal-close")).click();
	}
 
	
	
	//click on price break up
	public void clickOnViewCompleteBreakUp(){
		WebDriver driver = DriverManager.getInstance().getDriver();
		WebDriverWait wait = new WebDriverWait(driver,120);
		wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.cssSelector(".view-complete-breakup")));
		driver.findElement(By.cssSelector(".view-complete-breakup")).click();
	}
	//verify pop up 
	public boolean doesBreakUpPopUpOpened(){
		WebDriver driver = DriverManager.getInstance().getDriver();
		return driver.findElement(By.xpath("//div[@class='modal__header']/span[text()='DAILY PRICE BREAK UP']")).isDisplayed();
	}
	//verify checkIn and Checkout date on break up popup
	public boolean doesDatesSame(String checkIn,String checkOut){
		WebDriver driver = DriverManager.getInstance().getDriver();
		String days=driver.findElement(By.cssSelector(".price-detail-days")).getText().trim();
		String[] checkInCheckOut = days.split("-");
		String breakUpCheckIn = checkInCheckOut[0].trim();
		String breakUpCheckOutWithNight = checkInCheckOut[1].trim();
		String[] breakUpCheckOutArray = breakUpCheckOutWithNight.split("\\(");
		String breakUpCheckOut =breakUpCheckOutArray[0].trim();
		String newCheckOut[] = checkOut.split(" ");
		int newCheckOut2 = Integer.parseInt(newCheckOut[0])-1;
		String checkOut3 = Integer.toString(newCheckOut2)+" "+newCheckOut[1];
		
	
		if((breakUpCheckIn.trim()).equals(checkIn) && (breakUpCheckOut.trim()).equals(checkOut3.trim()))
			return true;
		return false;
	}
	//verify stayout night date
	public boolean checkStayNightDateWithCheckInDate(String checkIn){
		WebDriver driver = DriverManager.getInstance().getDriver();
	    WebElement element = driver.findElement(By.xpath("//table[@class='price-detail-table mb30']//td[1]"));
	    String stayNightDate = element.getText().trim();
	    if((stayNightDate.trim()).equals(checkIn.trim()))
	    	return true;
	    return false;
	}
	
	//check calculate base price in price break and then compare with room price on popup
	public boolean checkBasePriceEqualWithBreakPrice(){
		WebDriver driver = DriverManager.getInstance().getDriver();
		double sum=0;
		int index=0;
		List<WebElement> elements= driver.findElements(By.xpath("//div[@class='price-table-scroll']//tr[@class='grayTxt']/td[2]/span[1]"));
		for (WebElement element : elements) {
			String calculateBreakPriceString = element.getText().trim();
			double calculateBreakPrice=Double.parseDouble(calculateBreakPriceString);
			sum=sum+calculateBreakPrice;
			index=index+1;
		}
		WebElement element2= driver.findElement(By.xpath("//div[@class='price-detail-footer__room text-center price-detail__item']//span[@class='price-detail-footer__amount']/span[1]"));
		String basePriceString = element2.getText().trim();
		double basePrice = Double.parseDouble(basePriceString.trim());
		
		if(sum==basePrice)
			return true;
		return false;
	}
	public boolean checkTaxPriceEqualWithBreakTaxPrice(){
		WebDriver driver = DriverManager.getInstance().getDriver();
		double sum=0;
		int index=0;
		List<WebElement> elements= driver.findElements(By.xpath("//div[@class='price-table-scroll']//tr[@class='grayTxt']/td[3]/span[1]"));
		for (WebElement element : elements) {
			String calculateBreakPriceString = element.getText().trim();
			double calculateBreakPrice=Double.parseDouble(calculateBreakPriceString);
			sum=sum+calculateBreakPrice;
			index=index+1;
		}
		WebElement element2= driver.findElement(By.xpath("//div[@class='price-detail-footer__tax text-center price-detail__item']//span[@class='price-detail-footer__amount']/span[1]"));
		String baseTaxString = element2.getText().trim();
		double baseTax = Double.parseDouble(baseTaxString.trim());
		
		if(sum==baseTax)
			return true;
		return false;
	}
	public boolean checkTotalPriceEqualWithBreakFinalPrice(){
		WebDriver driver = DriverManager.getInstance().getDriver();
		double sum=0;
		int index=0;
		List<WebElement> elements= driver.findElements(By.xpath("//div[@class='price-table-scroll']//tr[@class='grayTxt']/td[4]/span[1]"));
		for (WebElement element : elements) {
			String calculateBreakPriceString = element.getText().trim();
			double calculateBreakPrice=Double.parseDouble(calculateBreakPriceString);
			sum=sum+calculateBreakPrice;
			index=index+1;
		}
		WebElement element2= driver.findElement(By.xpath("//span[@class='price-detail-footer__total-cost']/span[2]"));
		String TotalPriceString = element2.getText().trim();
		double TotalPriceDouble = Double.parseDouble(TotalPriceString.trim());
		int Sum = (int) sum;
		int TotalPrice = (int) TotalPriceDouble;
	
		if(TotalPrice==Sum || TotalPrice==Sum+1)
			return true;
		return false;
	}
	public boolean checkTotalPrice(){
		WebDriver driver = DriverManager.getInstance().getDriver();
		WebElement element2= driver.findElement(By.xpath("//div[@class='price-detail-footer__room text-center price-detail__item']//span[@class='price-detail-footer__amount']/span[1]"));
		String basePriceString = element2.getText().trim();
		double basePrice = Double.parseDouble(basePriceString.trim());
		WebElement element3= driver.findElement(By.xpath("//div[@class='price-detail-footer__tax text-center price-detail__item']//span[@class='price-detail-footer__amount']/span[1]"));
		String baseTaxString = element3.getText().trim();
		double baseTax = Double.parseDouble(baseTaxString.trim());
		WebElement element4= driver.findElement(By.xpath("//span[@class='price-detail-footer__total-cost']/span[2]"));
		String totalPriceString = element4.getText().trim();
		double TotalPriceDouble = Double.parseDouble(totalPriceString.trim());
		double calculateTotalPriceDouble = basePrice+baseTax;
		int calculateTotalPrice =(int) calculateTotalPriceDouble;
	    int totalPrice = (int) TotalPriceDouble;
		
		if(totalPrice==calculateTotalPrice || totalPrice==calculateTotalPrice+1)
			return true;
		return false;
	}
	//verify inclusive all tax text
	public boolean isInclusiveTaxTextDisplayed(){
		WebDriver driver = DriverManager.getInstance().getDriver();
	   return driver.findElement(By.xpath("//p[text()='(inclusive all of taxes) ']")).isDisplayed();
		
	}
	//display the done button
	public boolean isDoneButtonDisplayed(){
			WebDriver driver = DriverManager.getInstance().getDriver();
			browserHelper.scrollToElement(driver.findElement(By.cssSelector("#doneDetail")));
			return driver.findElement(By.cssSelector("#doneDetail")).isDisplayed();
		}
	//click on done button
	public void clickOnDoneButton(){
		WebDriver driver = DriverManager.getInstance().getDriver();
		browserHelper.scrollToElement(driver.findElement(By.cssSelector("#doneDetail")));
		driver.findElement(By.cssSelector("#doneDetail")).click();
	}
	
	//method for pay at hotel and pay now
	public boolean doesOTPPopupDisplayed(){
		WebDriver driver = DriverManager.getInstance().getDriver();
		WebDriverWait wait = new WebDriverWait(driver,60);
		wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.id("otpForm")));
		
		if(driver.findElements(By.id("otpForm")).size()>0)
			return true;
		return false;
	}
	// check OTP title 
	public boolean doesOTPPopupTitleDisplayed(){
		WebDriver driver = DriverManager.getInstance().getDriver();
		WebDriverWait wait = new WebDriverWait(driver,60);
		wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.xpath("//p[text()='Verify your Mobile Number']")));
		return driver.findElements(By.xpath("//p[text()='Verify your Mobile Number']")).size()>0;
	}
  //check change number clickable 
	public boolean doesClickableChangeNumber(){
		WebDriver driver = DriverManager.getInstance().getDriver();
		WebDriverWait wait = new WebDriverWait(driver,60);
		wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.xpath("//small[text()='Change Number']")));
		return driver.findElement(By.xpath("//small[text()='Change Number']")).isEnabled();
	}
	//check resend sms clickable
	public boolean doesClickableResendSms(){
		WebDriver driver = DriverManager.getInstance().getDriver();
		WebDriverWait wait = new WebDriverWait(driver,60);
		wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.xpath("//small[text()='Resend Sms']")));
		return driver.findElement(By.xpath("//small[text()='Resend Sms']")).isEnabled();
	}
	// check verify button
	public boolean doesVerifyButtonDisplayed(){
		WebDriver driver = DriverManager.getInstance().getDriver();
		return driver.findElement(By.xpath("//button[text()='VERIFY']")).isDisplayed();
	}
	//click on verify button
	public void clickOnVerifyButton(){
		WebDriver driver = DriverManager.getInstance().getDriver();
		driver.findElement(By.xpath("//button[text()='VERIFY']")).click();
	}
	//extract mobile number from otp pop up
	public String extractMobileNumberFromOTPPopup(){
		WebDriver driver = DriverManager.getInstance().getDriver();
		String mobile= driver.findElement(By.xpath("//span[@class='popup__mobile']")).getText().trim();
		return mobile;
	}
	public boolean doesBothStringEqual(String str1,String str2){
		if((str1.trim()).equals(str2.trim()))
			return true;
		return false;
	}
	
	//Enter otp 
	public void enterOTP(String otp){
		WebDriver driver = DriverManager.getInstance().getDriver();
		driver.findElement(By.xpath("//input[@name='otp']")).clear();
		driver.findElement(By.xpath("//input[@name='otp']")).sendKeys(otp.trim());
	}
	
	//click on change number 
	public void clickOnChangeNumber(){
		WebDriver driver = DriverManager.getInstance().getDriver();
		driver.findElement(By.cssSelector(".popup__edit")).click();
	}
	//update new number
	public void changeNumber(String newNumber){
		WebDriver driver= DriverManager.getInstance().getDriver();
		driver.findElement(By.xpath("//div[@class='float-labels popup__mobileform']/input[@class='float-labels__input']")).clear();
		driver.findElement(By.xpath(".//*[@id='mobileForm']//input[@class='float-labels__input parsley-error']")).sendKeys(newNumber.trim());
	}
	//check change mobile pop up
	public boolean doesChangeNumberMobilePopupOpened(){
		WebDriver driver = DriverManager.getInstance().getDriver();
		if(driver.findElements(By.id("mobileForm")).size()>0)
			return true;
		return false;
	}
	//check title of change number pop up
	public boolean doesChangeNumberPopUpTitleDisplayed(){
		WebDriver driver = DriverManager.getInstance().getDriver();
		return driver.findElement(By.xpath("//p[text()='Enter Mobile Number']")).isDisplayed();
	}
	public boolean doesSendOTPButtonDisplayed(){
		WebDriver driver = DriverManager.getInstance().getDriver();
		return driver.findElement(By.xpath("//button[text()='SEND OTP']")).isDisplayed();
	}
	//click on send otp button
	public void clickOnSendOTP(){
		WebDriver driver =DriverManager.getInstance().getDriver();
		driver.findElement(By.xpath("//button[text()='SEND OTP']")).click();
	}
//	public boolean doesOTPPopupDisplayed(){
//		WebDriver driver = DriverManager.getInstance().getDriver();
//		if(driver.findElements(By.cssSelector("#otpForm")).size()>0)
//			return true;
//		return false;
//	}
//	// check OTP title 
//	public boolean does
	
  //check PayNow option is there on OTP popup
	public boolean doesPayNowOptionDisplayedOnOTPPopup(){
		WebDriver driver = DriverManager.getInstance().getDriver();
		return driver.findElement(By.xpath("//button[text()='SKIP, I WILL PAY NOW']")).isDisplayed();
	}
	//click on Pay Now option at OTP popup
	public void clickOnSkipIWillPayNow(){
		WebDriver driver = DriverManager.getInstance().getDriver();
		driver.findElement(By.xpath("//button[text()='SKIP, I WILL PAY NOW']")).click();
	}
	//check Transaction page title 
	public boolean doesTransactionPageTitleDisplayed(){
		WebDriver driver = DriverManager.getInstance().getDriver();
		//WebDriverWait wait = new WebDriverWait(driver,20);
		try {
			Thread.sleep(30000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		//switch to iframe
		driver.switchTo().frame(0);
		//wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.xpath("//div[text()='Treebo Hotels']")));
		return driver.findElements(By.xpath("//div[text()='Treebo Hotels']")).size()>0;
	}
	//get mobile number from transaction page
	public String getMobileNumberFromTransactionPage(){
		WebDriver driver = DriverManager.getInstance().getDriver();
		String mobile= driver.findElement(By.id("contact")).getAttribute("value").trim();
		return mobile;
	}
	//get email address from transaction page
	public String getEmailAddressFromTransactionPage(){
		WebDriver driver = DriverManager.getInstance().getDriver();
		String email= driver.findElement(By.xpath(".//*[@id='email']")).getAttribute("value").trim();
		return email;
	}
	//check OTP error message
	public boolean doesErrorMessageDisplayed(){
		WebDriver driver = DriverManager.getInstance().getDriver();
		return driver.findElement(By.xpath("//div[text()='Please enter correct OTP.']")).isDisplayed();
	}
	
}
