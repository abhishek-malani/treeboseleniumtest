package mobileSiteUIPageObjects;

import org.apache.commons.lang3.RandomStringUtils;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import base.BrowserHelper;
import base.DriverManager;
import mobileSiteUIPageObjects.HomeScreen;
import mobileSiteUIPageObjects.HotelSearchResultsScreen;
import utils.CommonUtils;

public class FOTScreen {
	private BrowserHelper browserHelper;
	private HomeScreen homePage;
    private By fotPage = By.cssSelector(".fotpage");
	private By joinUsLink = By.cssSelector(".btn.fot-image__submit");
	private By loginLink = By.cssSelector("a[href='/fot/login/']");
	private By loginFotLink=By.cssSelector(".btn.signup-schedule__login");
	private By LoginButton = By.cssSelector("#loginButton");
	private By signUpPage = By.cssSelector(".signupPage");
	private By fotName = By.cssSelector("#userName");
	private By fotEmail = By.cssSelector("#email");
	private By fotMobile = By.cssSelector("#mobile");
	private By fotPassword = By.cssSelector("#pwd");
	private By confirmfotPassword = By.cssSelector("#confirmPwd");
	private By fotContinue = By.cssSelector("#signupSubmit");
	private By continueToForm = By.cssSelector("#continueToForm");
	private By regSuccess = By.xpath("//div[@id='testSuccess'][not(contains(@class,'hide'))]");
	private By scheduleFirstAuditLink = By.cssSelector("#scheduleFirstAudit");
	private By scheduleAuditLink = By.xpath("//a[contains(@href,'/fot/search')]");
	private By emailinput = By.cssSelector("#loginEmailInput");
	private By passwordinput = By.cssSelector("#loginPassowrdlInput");
	private By BookTreebo = By.cssSelector(".btn.searchpage__book-btn");
	private By scheduleAuditLinkFOT=By.cssSelector(".btn.fot-image__submit.fot-image__schedule-btn.submit");
	private By BookTreeboFailedUser=By.cssSelector(".btn.fot-image__submit.fot-image__booking-btn.submit");
	public void openFOTPage() {
		WebDriver driver = DriverManager.getInstance().getDriver();
		browserHelper = new BrowserHelper();
		homePage = new HomeScreen();
		// Home Page is Displayed
		if(homePage.isHomeScreenDisplayed())
		{browserHelper.openUrl(browserHelper.getCurrentUrl() + "fot");
		
		//check if 500 page
		if (homePage.is500Displayed()){
			Assert.fail("500 displayed instead of FOT page for url " + browserHelper.getCurrentUrl());
		}
		
		Assert.assertTrue(driver.findElement(fotPage).isDisplayed(),"FOT Page is not displayed " + browserHelper.getCurrentUrl());
		System.out.println("FOT Page is opened");
		}
		else
			System.out.println("Home Page is not displayed");
	}
	public String[] fotSignUp() {
		WebDriver driver = DriverManager.getInstance().getDriver();
		driver.findElement(joinUsLink).click();
		Assert.assertTrue(isSignUpPageDisplayed(), "Sign up page not displayed");
		String[] guestDetails = enterSignUpUserDetails();
		continueToFOTSignUp();
		fillPersonalInfo();
		selectProblemSolvingAnswers1();
		selectProblemSolvingAnswers2();
		Assert.assertTrue(isRegistrationSuccessful(), "Sign up page not displayed");
		return guestDetails;
	}
	public boolean isSignUpPageDisplayed() {
		WebDriver driver = DriverManager.getInstance().getDriver();
		return (driver.findElements(signUpPage).size() > 0);
	}
	public String[] enterSignUpUserDetails() {
		WebDriver driver = DriverManager.getInstance().getDriver();
		CommonUtils utils = new CommonUtils();
	    String name = "Test";
		browserHelper = new BrowserHelper();
		String email = browserHelper.getDisposableEmail();
		String mobile = Integer.toString(utils.getRandomNumber(2, 6)) + RandomStringUtils.randomNumeric(9);
		String password = "password";

		System.out.println("Default Sign up with following details. Name : " + name + ", mobile :" + mobile
				+ ", email : " + email + ", password:" + password);
		driver.findElement(fotName).sendKeys(name);
		driver.findElement(fotMobile).sendKeys(mobile);
		driver.findElement(fotEmail).sendKeys(email);
		driver.findElement(fotPassword).sendKeys(password);
		driver.findElement(confirmfotPassword).sendKeys(password);
		driver.findElement(fotContinue).click();
		String[] guestDetails = { name, email, mobile, password };
		return guestDetails;
	}
	public void continueToFOTSignUp() {
		WebDriver driver = DriverManager.getInstance().getDriver();
		WebDriverWait wait = new WebDriverWait(driver, 120);
		wait.until(ExpectedConditions.visibilityOfElementLocated(continueToForm));
		browserHelper.scrollToElement(driver.findElement(continueToForm));
		driver.findElement(continueToForm).click();
	}
	
	public void fillPersonalInfo() {
		WebDriver driver = DriverManager.getInstance().getDriver();
		CommonUtils utils = new CommonUtils();
		String city = utils.getProperty("city");
		driver.findElement(By.cssSelector("#address")).sendKeys(city);
		
		driver.findElement(By.cssSelector("input[value='Married']")).click();
		browserHelper.scrollToElement(driver.findElement(By.cssSelector("input[value='month']")));
		driver.findElement(By.cssSelector("input[value='month']")).click();
		browserHelper.scrollToElement(driver.findElement(By.xpath("//div[@id='personal-info']//input[@type='button']")));
		driver.findElement(By.xpath("//div[@id='personal-info']//input[@type='button']")).click();
	}
	public void selectProblemSolvingAnswers1() {
		WebDriver driver = DriverManager.getInstance().getDriver();
		WebDriverWait wait = new WebDriverWait(driver, 120);
	// Question 1, answer 1
	    driver.findElement(By
			.xpath("//div[contains(@class,'signup-page__enter-detail')][not(contains(@class,'hide'))]/div[@class='fot-form'][1]//*[contains(@class,'fot-form__check')][1]//input"))
			.click();
	// Question 2, answer 1
	    driver.findElement(By
			.xpath("//div[contains(@class,'signup-page__enter-detail')][not(contains(@class,'hide'))]/div[@class='fot-form'][2]//*[contains(@class,'fot-form__check')][1]//input"))
			.click();
	// Question 3, answer 1
//	driver.findElement(By
//			.xpath("//div[contains(@class,'signup-page__enter-detail')][not(contains(@class,'hide'))]/div[@class='fot-form'][3]//*[contains(@class,'fot-form__check')][1]//input"))
//			.click();
	// Question 3, answer 4
	    //wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[contains(@class,'signup-page__enter-detail')]/div[@class='fot-form'][3]//*[contains(@class,'fot-form__check')][4]//input")));
	    browserHelper.scrollToElement(driver.findElement(By.xpath("//div[not(contains(@class,'hide'))]/div[@class='fot-form'][3]//*[contains(@class,'fot-form__check')][4]//input")));
	    driver.findElement(By
			.xpath("//div[contains(@class,'signup-page__enter-detail')][not(contains(@class,'hide'))]/div[@class='fot-form'][3]//*[contains(@class,'fot-form__check')][4]//input"))
			.click();
	// Question 4, answer 4
	    //browserHelper.scrollToElement(driver.findElement(By.xpath("//div[not(contains(@class,'hide'))]/div[@class='fot-form'][4]//*[contains(@class,'fot-form__check')][4]//input")));
	    driver.findElement(By
			.xpath("//div[contains(@class,'signup-page__enter-detail')][not(contains(@class,'hide'))]/div[@class='fot-form'][4]//*[contains(@class,'fot-form__check')][4]//input"))
			.click();
	// Question 5, answer 4
	    //wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.xpath("//div[not(contains(@class,'hide'))]/div[@class='fot-form'][5]//*[contains(@class,'fot-form__check')][4]//input")));
	    browserHelper.scrollToElement(driver.findElement(By.xpath(".//*[@id='testFormTwo']/div/div[7]/div[6]/label/input")));
	     driver.findElement(By.xpath(".//*[@id='testFormTwo']/div/div[7]/div[6]/label/input")).click();
	        
	// Question 6, answer 2
	   // wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.xpath("//div[not(contains(@class,'hide'))]/div[@class='fot-form'][6]//*[contains(@class,'fot-form__check')][2]//input")));
	    //browserHelper.scrollToElement(driver.findElement(By.xpath("//div[not(contains(@class,'hide'))]/div[@class='fot-form'][6]//*[contains(@class,'fot-form__check')][2]//input")));
	     driver.findElement(By
			.xpath(".//*[@id='testFormTwo']/div/div[8]/div[4]/label/input")).click();
	        
	// continue
	    browserHelper.scrollToElement(driver.findElement(By.xpath(".//*[@id='testFormTwo']/div/div[9]/input")));
	    driver.findElement(By.xpath(".//*[@id='testFormTwo']/div/div[9]/input")).click();

			
	}
	public void selectProblemSolvingAnswers2(){
		WebDriver driver = DriverManager.getInstance().getDriver();
		// Question 1, answer 2
		       driver.findElement(By
					.xpath(".//*[@id='testFormThree']/div/div[2]/div[3]/label/input")).click();
				
				// Question 2, answer 2
				
				driver.findElement(By
						.xpath(".//*[@id='testFormThree']/div/div[3]/div[3]/label/input")).click();;
				

				// Age check and Agree to terms and conditions
				browserHelper.scrollToElement(driver.findElement(By.cssSelector("#ageCheck")));
				   driver.findElement(By.cssSelector("#ageCheck")).click();
				   browserHelper.scrollToElement(driver.findElement(By.cssSelector("#termsCheck")));
				   driver.findElement(By.cssSelector("#termsCheck")).click();
				// submit
				driver.findElement(By.xpath(".//*[@id='testSubmit']")).click();
		
	}
	public boolean isRegistrationSuccessful() {
		WebDriver driver = DriverManager.getInstance().getDriver();
		WebDriverWait wait = new WebDriverWait(driver, 120);
		wait.until(ExpectedConditions.presenceOfElementLocated(regSuccess));
		return (driver.findElements(regSuccess).size() > 0);
	}
	public boolean isScheduleFirstAuditDisplayed() {
		WebDriver driver = DriverManager.getInstance().getDriver();
		WebDriverWait wait = new WebDriverWait(driver, 30);
		wait.until(ExpectedConditions.presenceOfElementLocated(scheduleFirstAuditLink));
		return driver.findElement(scheduleFirstAuditLink).isDisplayed();
	}
	public void clickScheduleFirstAudit() {
		WebDriver driver = DriverManager.getInstance().getDriver();
		WebDriverWait wait = new WebDriverWait(driver, 30);
		browserHelper = new BrowserHelper();
		wait.until(ExpectedConditions.visibilityOfElementLocated(scheduleFirstAuditLink));
		driver.findElement(scheduleFirstAuditLink).click();
		browserHelper.waitTime(3000);
	}

	public boolean isScheduleAuditDisplayed() {
		WebDriver driver = DriverManager.getInstance().getDriver();
		WebDriverWait wait = new WebDriverWait(driver, 30);
		wait.until(ExpectedConditions.presenceOfElementLocated(scheduleAuditLink));
		return driver.findElement(scheduleAuditLink).isDisplayed();
	}
	public boolean isScheduleAuditDisplayedFOT()
	{
		WebDriver driver = DriverManager.getInstance().getDriver();
		WebDriverWait wait = new WebDriverWait(driver, 30);
		wait.until(ExpectedConditions.presenceOfElementLocated(scheduleAuditLinkFOT));
		return driver.findElement(scheduleAuditLinkFOT).isDisplayed();
	}
	public void clickScheduleAudit() {
		WebDriver driver = DriverManager.getInstance().getDriver();
		browserHelper = new BrowserHelper();
		driver.findElement(scheduleAuditLink).click();
		browserHelper.waitTime(10000);
	}
	public void clickScheduleAuditFOT() {
		WebDriver driver = DriverManager.getInstance().getDriver();
		browserHelper = new BrowserHelper();
		driver.findElement(scheduleAuditLinkFOT).click();
		browserHelper.waitTime(10000);
	}
	public boolean isDefaultSearchAsExpected() {
		WebDriver driver = DriverManager.getInstance().getDriver();
		DateTime dt = new DateTime();
		DateTimeFormatter dtf = DateTimeFormat.forPattern("dd/MM/yyyy");
		String date = dtf.print(dt.plusDays(1));
		return (driver.findElement(By.cssSelector(".dr-date")).getText().equals(date));
	}
	
	public boolean isJoinUSDisplayedOnFOTHomePage() {
		WebDriver driver = DriverManager.getInstance().getDriver();
		return (driver.findElements(joinUsLink).size() > 0);
	}
	public boolean isLoginDisplayedOnFOTHomePage() {
		WebDriver driver = DriverManager.getInstance().getDriver();
		return (driver.findElements(loginFotLink).size() > 0);
	}
   public void clickonlogin(){
	   WebDriver driver = DriverManager.getInstance().getDriver();
	   driver.findElement(loginLink).click();
	   
   }
   public void clickonFOTlogin()
   {
	   WebDriver driver = DriverManager.getInstance().getDriver();
	   driver.findElement(loginFotLink).click();
   }
	public void loginToFOT(String email, String password) {
		WebDriver driver = DriverManager.getInstance().getDriver();
		homePage = new HomeScreen();
		
		driver.findElement(emailinput).sendKeys(email);
		driver.findElement(passwordinput).sendKeys(password);
		
		driver.findElement(LoginButton).click();
	//check out if any error
	   // homePage.clickonFOTlogin();
	}
	
	public boolean doesClickBookATreeboOpensTreeboLandingPageafterScheduleAudit() {
		WebDriver driver = DriverManager.getInstance().getDriver();
		browserHelper = new BrowserHelper();
		//CommonUtils utils = new CommonUtils();
		WebDriverWait wait = new WebDriverWait(driver, 30);
		wait.until(ExpectedConditions.presenceOfElementLocated(BookTreebo));
		driver.findElement(BookTreebo).click();
		return homePage.isHomeScreenDisplayed();
	}
   public boolean doesBookATreeboDisplaysForFailedFOT() {
		WebDriver driver = DriverManager.getInstance().getDriver();
		WebDriverWait wait = new WebDriverWait(driver, 300);
		wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//div[text()='BOOK A TREEBO']")));
		return (driver.findElements(By.xpath("//div[text()='BOOK A TREEBO']")).size() > 0);
	}
   public boolean doesBookATreeboDisplaysForFailedMFOT() {
		WebDriver driver = DriverManager.getInstance().getDriver();
		WebDriverWait wait = new WebDriverWait(driver, 30);
		wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(".btn.fot-image__submit.fot-image__booking-btn.submit")));
		return (driver.findElements(By.cssSelector(".btn.fot-image__submit.fot-image__booking-btn.submit")).size() > 0);
	}
  
   
	public boolean doesClickBookATreeboForFailedFOTOpensLandingPage() {
		homePage = new HomeScreen();
		return homePage.isHomeScreenDisplayed();
	}
	public void clickonBookTreeboForFailedUser()
	{WebDriver driver = DriverManager.getInstance().getDriver();
	WebDriverWait wait = new WebDriverWait(driver, 30);
	wait.until(ExpectedConditions.presenceOfElementLocated(By
			.cssSelector(".btn.fot-image__submit.fot-image__booking-btn.submit")));
	driver.findElement(By.cssSelector(".btn.fot-image__submit.fot-image__booking-btn.submit")).click();
	
		
	}
	

}
