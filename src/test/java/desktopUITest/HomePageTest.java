package desktopUITest;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Arrays;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import utils.CommonUtils;
import base.BrowserHelper;
import base.TestBaseSetUp;
import desktopUIPageObjects.HomePage;
import desktopUIPageObjects.HotelResultsPage;
import desktopUIPageObjects.Footer;

public class HomePageTest extends TestBaseSetUp {
	
	private HomePage homePage;
	private HotelResultsPage hotelResultsPage;
	private Footer footer;
	private BrowserHelper browserHelper;

	/**
	 * This test verifies the header links Navigation About, Join Our Network,
	 * Login, Signup, Treebo Image link, retryAnalyzer=Retry.class
	 * 
	 * @throws IOException
	 * @throws FileNotFoundException
	 */
	@Test(groups = { "HomePage" })
	public void testHeaderLinksNavigation() throws FileNotFoundException, IOException {
		homePage = new HomePage();
		browserHelper = new BrowserHelper();
		CommonUtils utils = new CommonUtils();
		String browserTitle = utils.getProperty("HomePageTitle");

		// Treebo Home Page
		homePage.verifyHomePagePresence();
		Assert.assertEquals(browserHelper.getBrowserTitle(), browserTitle);

		// About link takes to About Us page
		homePage.clickOnAbout();
		homePage.verifyAboutUsPagePresence();

		// Join our network link takes to join us page
		homePage.clickOnJoinOurNetworkLink();
		homePage.verifyJoinUsPagePresence();

		// Treebo image link takes to home page
		homePage.clickOnHomeTreeboLink();
		homePage.verifyHomePagePresence();

		// login link opens login pop up
		homePage.clickOnLoginLink();
		homePage.verifyLoginPopUpPresence();
		homePage.closeLoginPopUp();

		// Sign Up Link opens sign up form
		homePage.clickOnSignUpLink();
		homePage.verifySignUpPopUpPresence();
		homePage.closeSignUpPopUp();

		// Friends of treebo link
		homePage.clickOnFriendsOfTreeboLink();
		browserHelper.switchToNewWindow();
		Assert.assertTrue(browserHelper.getCurrentUrl().contains("fot"),"wrong URL: " + browserHelper.getCurrentUrl());
	}
	

	/**
	 * This test verifies that 1. 5 cities displays at a time 2. when clicked on
	 * city link on home page it takes to search page for city clicked
	 */

	@Test(groups = { "HomePage" })
	public void testCityLinkTakesToSearchResultPage() throws InterruptedException {
		homePage = new HomePage();
		hotelResultsPage = new HotelResultsPage();

		homePage.verifyCountOfCitiesDisplayedAtATime();
		String[] cityInitialList = homePage.getCitiesDisplayAtATime();
		homePage.clickOnNextLinkInCityDisplay();
		String[] cityListTwo = homePage.getCitiesDisplayAtATime();
		Assert.assertFalse(Arrays.equals(cityInitialList, cityListTwo));
		homePage.clickOnPrevLinkInCityDisplay();
		String[] cityListFinal = homePage.getCitiesDisplayAtATime();
		Assert.assertTrue(Arrays.equals(cityInitialList, cityListFinal));

		String cityNameClicked = homePage.clickCityLinkInHomePage();
		hotelResultsPage.verifyHotelResultsPagePresence();
		Assert.assertEquals(cityNameClicked.toLowerCase(), hotelResultsPage.getCitySearched().toLowerCase());
	}

	  /**
	   * Verify clicking on Friends of Treebo re-directs user to http://www.treebohotels.com/fot/
	   * Verify clicking on 'Signup' in fot page re-directs user to http://www.treebohotels.com/fot/register/
	   * Verify user can enter values in all the mandatory field and can click on Submit button
	   * Verify clicking on 'Schedule Audit' button takes to http://www.treebohotels.com/fot/audit/
	   * Verify user can schedule audit from http://www.treebohotels.com/fot/audit/
	   * Join Our Network
	   * Verify clicking on 'Join Our Network' takes user to http://www.treebohotels.com/joinus/
	   * Verify 'Video' can be played by clicking on Play button
	   * Verify user can fill all the mandatory fields in Join our Network and can click on 'Submit' button
	   * Verify clicking on 'About Us' takes user to http://www.treebohotels.com/aboutus/
	   */

	@Test(groups = { "HomePage","FOT" },enabled=false)
	public void testFOTlinksAndFeatures() {
		footer = new Footer();
		footer.verifyFOTLinkAndFeature();
	}
	
	/**
	 * Verify clicking on 'Feedback' tab brings up feedback pop up
     * Verify user can enter Name, Email ID and Message and can click on Submit button
     * Verify Feed back pop up has 'X' and Cancel button
     * Verify clicking on 'X' closes the popup
     * Verify Clicking on 'Cancel' button closes popup
	 */
	
	@Test(groups = { "HomePage","Feedback" })
	public void testFeedbackFeature() {
		footer = new Footer();
		footer.verifyFeedbackForm();
	}
	/**
	 * login on homePage 
	 * then click on Feedback link
	 * verify the emailID and Name are placed
	 */
	@Test(groups = { "HomePage","Feedback" })
	public void testFeedbackFeaturePopulateEmailName(){
		homePage = new HomePage();
		browserHelper = new BrowserHelper();
		SoftAssert verify = new SoftAssert();
		CommonUtils utils = new CommonUtils();
		footer = new Footer();
		
		String email = utils.getProperty("TestLoginEmail");  //"treebotest@gmail.com"
		String password = utils.getProperty("TestLoginPassword");
		
		homePage.verifyHomePagePresence();
		//Open feedback pop up
		// Name and Email field should be empty
		// Close the feedback
		footer.clickOnFeedback();
		verify.assertTrue(footer.doesFeedbackPopUpOpened(),"Feedback Popup is opened");
		verify.assertTrue(footer.doesEmailFieldShowEmaiIdPlaceHolder(),"Email field is not show email placeholder");
		verify.assertTrue(footer.doesNameFieldShowNamePlaceHolder(),"Name field does not show name placeholder");
		footer.clickCloseFeedback();
		
		//click on login 
		//enter login details
		homePage.clickOnLoginLink();
		homePage.enterLoginForm(email, password);
        homePage.verifyUserIsSignedIn();
        
        //Open feedback pop up
        // Name and Email field should be matching logged in user name and email
        footer.clickOnFeedback();
        String Name = footer.extractUserName();
       verify.assertTrue(footer.doesEmailIdPopulate(email),"Email is not placed");
        verify.assertTrue(footer.doesNamePopulate(Name),"Name is not placed");
       
        verify.assertAll();
      }
	
	/**
	 * Clicking on 'FB' icon, opens https://www.facebook.com/TreeboHotels in new tab
     * Clicking on 'Twitter' icon, open up https://twitter.com/TreeboHotels in new tab
     * Clicking on 'Linkedin' icon, opens https://www.linkedin.com/company/treebo-hotels in new tab
     * Clicking on 'Google+' icon, opens https://plus.google.com/+Treebohotels1/about in new tab
	 * @throws InterruptedException 
	 */
	
	@Test(groups = { "HomePage","JoinUs" })
	public void testJoinUsFeature() throws InterruptedException {
		footer = new Footer();
		footer.verifyJoinUsLinks();
	}
	
	/**
	 * Clicking on 'About' link, opens http://www.treebohotels.com/aboutus/  in new tab
     * Clicking on 'Contact Us' link, opens http://www.treebohotels.com/contactus/ in new tab
     * Clicking on 'FAQ' link, opens http://www.treebohotels.com/faq/  in new tab
     * Clicking on 'Terms of Service' link, opens http://www.treebohotels.com/terms/ in new tab
     * Clicking on 'Privacy Policy' link, opens http://www.treebohotels.com/policy/  in new tab
     * Clicking on 'Blog link, opens http://blog.treebohotels.com/  in new tab
	 */
	
	@Test(groups = { "HomePage","CompanyLinks" })
	public void testLinksInCompanySection() {
		footer = new Footer();
		footer.verifyLinksInCompanySection();
	}
	
	/**
	 * Verify clicking on 'Join Us' takes user to http://www.treebohotels.com/joinus/
     * Verify clicking on 'Corporate Enquiry' brings up the default emailer in the compose mode
     * Verify clicking on 'Travel Agents' brings up the default emailer in the compose mode
     * Verify clicking on 'Alpha' takes user to http://www.treebohotels.com/joinus/
     * Verify clicking on Friends of Treebo re-directs user to http://www.treebohotels.com/fot/
     * Verify clicking on 'Careers'  brings up the default emailer in the compose mode
	 */
	
	@Test(groups = { "HomePage","BusinessDiscover" })
	public void testLinksInBusinessAndDiscoverSection() {
		footer = new Footer();
		footer.verifyLinksInBusinessAndDiscoverSection();
	}
	
	/**
	 * 
	 */
	
	@Test(groups = { "HomePage" })
	public void testGuaranteePolicyStaticLink() {
		browserHelper = new BrowserHelper();
		String url = "https://static.treebohotels.com/dist/desktop/docs/guarantee_policy.pdf";
		browserHelper.openUrl(url);
		browserHelper.waitTime(5000);
		browserHelper.openUrl(url);
		browserHelper.waitTime(10000);
		//System.out.println(browserHelper.getPageSource());
		Assert.assertTrue(browserHelper.getPageSource().contains(url),"Guarantee Policy page is down with error embedded in : " + browserHelper.getPageSource());
		//Assert.assertTrue(browserHelper.getBrowserTitle().equals("guarantee_policy.pdf"),"Title is: " + browserHelper.getBrowserTitle());
	}
	/**
	 * Open homePage
	 * verify the checkIn 
	 * click on checkIn
	 * verify the arrow 
	 * click on right arrow
	 * extract month name
	 * then again on next page click left arrow
	 * also extract month name
	 * verify the monnths are not same
	 */
	@Test(groups = { "HomePage" })
	public void testCalendarNavigation(){
		homePage = new HomePage();
		browserHelper = new BrowserHelper();
		SoftAssert verify = new SoftAssert();
		
		//home page verified 
		homePage.verifyHomePagePresence();
		verify.assertTrue(homePage.isCheckInDisplayed());
		//click on checkIn
		homePage.clickOnCheckin();
		//extract month 
		String month1 = homePage.extractMonthName().trim();
		//Calendra displayed
		verify.assertTrue(homePage.isCalendraDisplayed());
		//left arrow not displayed
		verify.assertFalse(homePage.isLeftArrowDisplayed());
		browserHelper.waitTime(5000);
		//right arrow displayed
		homePage.clickonRightArrow();
		browserHelper.waitTime(5000);
		String month2 = homePage.extractMonthName().trim();
		//verify the equality
		verify.assertFalse(homePage.isEqualMonthName(month1, month2));
		//left arrow displayed
		verify.assertTrue(homePage.isLeftArrowDisplayed());
		//click on left arrow
		homePage.clickonLeftArrow();
		String month3 = homePage.extractMonthName().trim();
		verify.assertFalse(homePage.isEqualMonthName(month2, month3));
		verify.assertTrue(homePage.isEqualMonthName(month1, month3));
		verify.assertAll();
		}
	/**
	 * Open home page 
	 * verify the join our network link presence 
	 * click on the join our network link
	 * fill the required data
	 * then click on submit 
	 * verify the succesful pop up
	 */
	@Test(groups = { "HomePage" },enabled = false)
	public void testJoinOurNetwork(){
		homePage = new HomePage();
		browserHelper = new BrowserHelper();
		CommonUtils utils = new CommonUtils();
		SoftAssert verify = new SoftAssert();
		
		//home page verified
		homePage.verifyHomePagePresence();
		
		//join our network displayed
		verify.assertTrue(homePage.isJoinOurNetworlLinkDisplayed());
		//click on join our network
		homePage.clickOnJoinOurNetworkLink();
		
		String contactPerson = utils.getProperty("GuestName"); // "Test";
		String email = utils.getProperty("TestBookingAccount"); // "";
		String guestMobile = utils.getProperty("GuestMobile");// "9000000000";
		
		String testHotel = utils.getProperty("DummyHotel");
		String hotelAddress1 = utils.getProperty("city");
		String hotelAddress2 =utils.getProperty("city");
	
		String pinCode = "560095";
		//join our network registration
		homePage.registerAtJoinOurNetwork(contactPerson,email,guestMobile,testHotel,hotelAddress1,hotelAddress2,pinCode);
		//verified successful popup
		verify.assertTrue(homePage.isSuccessfulPopUpdisplayed());
		verify.assertAll();
	}
	
}
