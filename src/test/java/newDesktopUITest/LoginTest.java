package newDesktopUITest;

import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import base.TestBaseSetUp;
import desktopUIPageObjects.Account;
import desktopUIPageObjects.HomePage;

import base.BrowserHelper;

import utils.CommonUtils;

import java.io.FileNotFoundException;
import java.io.IOException;

import org.apache.commons.lang3.RandomStringUtils;

public class LoginTest extends TestBaseSetUp {

	private BrowserHelper browserHelper;
	private HomePage homePage;
	private Account account;

	/**
	 * This test verifies the Login as Treebo member from Home Page
	 * @throws IOException 
	 * @throws FileNotFoundException 
	 */
	@Test(groups = { "Login","ProdSanity" })
	public void testLoginAsTreeboMember() throws FileNotFoundException, IOException {
		homePage = new HomePage();
		CommonUtils utils = new CommonUtils();
		String email = utils.getProperty("TestLoginEmail");  //"treebotest@gmail.com"
		String password = utils.getProperty("TestLoginPassword"); //"password"

		// login link opens login pop up
		homePage.clickOnLoginLink();
		homePage.enterLoginForm(email, password);
        homePage.verifyUserIsSignedIn();
        homePage.clickOnSignOutLink();
        homePage.verifyUserIsNotSignedIn();
	}
	
	/**
	 * This test verifies the Login through gmail
	 * @throws IOException 
	 * @throws FileNotFoundException 
	 */
	@Test(groups = { "Login" })
	public void testLoginThroughGmail() throws FileNotFoundException, IOException {
		homePage = new HomePage();
		account = new Account();
		CommonUtils utils = new CommonUtils();
		String email = utils.getProperty("TestLoginEmail");  //"treebotest@gmail.com"
		String password = utils.getProperty("GmailFbPassword"); //"treeboqa"

		// login through gmail
		homePage.verifyUserIsNotSignedIn();
		homePage.clickOnLoginLink();
        homePage.signInThroughGmail(email, password);
        homePage.verifyUserIsSignedIn();
        account.doSignOut();
	}
	
	/**
	 * This test verifies the Login through FB
	 * @throws IOException 
	 * @throws FileNotFoundException 
	 */
	@Test(groups = { "Login" })
	public void testLoginThroughFacebook() throws FileNotFoundException, IOException {
		homePage = new HomePage();
		account = new Account();
		CommonUtils utils = new CommonUtils();
		String email = utils.getProperty("FBLogin");  
		String password = utils.getProperty("GmailFbPassword"); 

		// login through fb
		homePage.verifyUserIsNotSignedIn();
		homePage.clickOnLoginLink();
        homePage.signInThroughFB(email, password);
        homePage.verifyUserIsSignedIn();
        account.doSignOut();
	}
	
	/**
	 * This test verifies the Signup from Home Page
	 */
	@Test(groups = { "Login" })
	public void testSignUpAsTreeboMember() {
		homePage = new HomePage();
		CommonUtils utils = new CommonUtils();
		browserHelper = new BrowserHelper();
		
		String name = "Test " + RandomStringUtils.randomAlphabetic(8);
//		String uniqueEmail = "test" + RandomStringUtils.randomAlphanumeric(15);
//		String email = uniqueEmail + "@gmail.com";
		String email = browserHelper.getDisposableEmail();
		String mobile = Integer.toString(utils.getRandomNumber(2, 6)) + RandomStringUtils.randomNumeric(9);
		String password = "password";

		// login link opens Signup pop up
		homePage.clickOnSignUpLink();
		homePage.verifySignUpPopUpPresence();
		homePage.enterSignUpForm(name, mobile, email, password);
        homePage.verifyUserIsSignedIn();
        homePage.clickOnSignOutLink();
        homePage.verifyUserIsNotSignedIn();
	}
	
	/**
	 * This test verifies the Forgot password flow from Home Page
	 * @throws IOException 
	 * @throws FileNotFoundException 
	 */
	@Test(groups = { "Login","ForgotPwd" })
	public void testForgotPasswordFlow() throws FileNotFoundException, IOException {
		homePage = new HomePage();
		browserHelper = new BrowserHelper();
		CommonUtils utils = new CommonUtils();
		String email = utils.getProperty("EmailForgotPassword");  //treebotestone@gmail.com
		String password = utils.getProperty("GmailFbPassword"); //"treeboqa"
		String textToSearch = utils.getProperty("TextToSearchForForgotPasswordEmail"); 
		String pwdToReset = RandomStringUtils.randomAlphabetic(10);
		
        homePage.clickOnLoginLink();
        homePage.enterEmailForResetPasswordAndSend(email);
        homePage.signInToRediffMail(email, password);
        homePage.searchRediffMailWithTextAndOpenEmail(textToSearch);
        browserHelper.switchToNewWindow();
        homePage.resetPassword(pwdToReset, pwdToReset);
		homePage.clickOnLoginLink();
		homePage.verifyLoginPopUpPresence();
		homePage.enterLoginForm(email, pwdToReset);
        homePage.verifyUserIsSignedIn();
	}
	
	/**
	 * 1.Enter invalid user name and invalid password
	 * 2.Enter invalid user name and valid password
	 * 3.Enter valid user name and invalid password
	 * 4.Click on X icon in sign in page
	 * 5.CLick on login button without entering email address and password
	 * 6.Try to access accounts page using direct url without login
	 * 7.Login and click on browser back button
	 * 8.Click on signup button without filling any details
	 * 9.Click on login link in Signup page
	 * 10.Click on X icon in signup page
	 * 11.Close the signup page
	 * 12.Verify character limit for password and phone number fields
	 * 13.Click on Terms of services and Privacy policy link in signup page
	 * 14.Click on signup via FB and Google+ buttons displayed
	 * 15.Already have a Treebo Account? Login is displayed
	 * @throws InterruptedException 
	 */
	@Test(groups = { "Login" })
	public void testDifferentLoginScenarios() throws FileNotFoundException, IOException, InterruptedException {
		homePage = new HomePage();
		browserHelper = new BrowserHelper();
		CommonUtils utils = new CommonUtils();
		
		String validEmail = utils.getProperty("TestLoginEmail");
		String validPassword = utils.getProperty("TestLoginPassword");
		String invalidEmail = utils.getProperty("InvalidEmail");
		String invalidPassword = utils.getProperty("InvalidPassword");

		String[][] emailPwd = {{invalidEmail,invalidPassword},{invalidEmail,validPassword},{invalidEmail,validPassword}};
		for (String[] x : emailPwd ){
			homePage.clickOnLoginLink();
			homePage.enterLoginForm(x[0], x[1]);
			Thread.sleep(3000);
			homePage.verifyLoginError();
			browserHelper.browserRefresh();
		}
		
		// click login button, without entering anything
		homePage.clickOnLoginLink();
		homePage.clickOnLoginButton();
		homePage.verifyEmailError();
		homePage.verifyPasswordError();
		
		// Click on X icon in sign in page
		Assert.assertTrue(homePage.isLoginPopUpDisplayed());
		homePage.closeLoginPopUp();
		Assert.assertFalse(homePage.isLoginPopUpDisplayed());
		
		// Sign Up pop up - X button
		homePage.clickOnSignUpLink();
		Assert.assertTrue(homePage.isSignUpPopUpDisplayed());
		homePage.closeSignUpPopUp();
		Assert.assertFalse(homePage.isSignUpPopUpDisplayed());
		
		// Sign up pop up
		homePage.clickOnSignUpLink();
		Assert.assertTrue(homePage.isSignUpPopUpDisplayed());
		homePage.clickOnSignUpButton();
		homePage.verifyEmailInSignUpError();
		homePage.verifyNameInSignUpError();
		homePage.verifyEmailInSignUpError();
		homePage.verifyMobileInSignUpError();
		// password and mobile character limit
		homePage.verifyCharacterLimitforMobile();
		homePage.verifyCharacterLimitforPassword();
		homePage.verifyExistingMemberTextInSignUpPopUp();
		homePage.clickOnLoginLinkInSignUpPopUp();
		Assert.assertTrue(homePage.isSignUpPopUpDisplayed());
		
		// Go to Home Page
		browserHelper.browserRefresh();
		homePage.clickOnSignUpLink();
		homePage.verifyTermsOfServicesLinkInSignUp();
		homePage.verifyPrivacyPolicyLinkInSignUp();
		
		// Login and click on browser back
		browserHelper.browserRefresh();
		String currentUrl = browserHelper.getCurrentUrl();
		browserHelper.openUrl(currentUrl);
		homePage.signInAsTreeboMember(validEmail, validPassword);
		homePage.doSearch();
		browserHelper.browserGoBack();
		homePage.verifyHomePagePresence();
	}
}
