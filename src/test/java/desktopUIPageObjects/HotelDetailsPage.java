package desktopUIPageObjects;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.asserts.SoftAssert;

import base.BrowserHelper;
import base.DriverManager;
import desktopUIPageObjects.HomePage;
import utils.CommonUtils;

public class HotelDetailsPage {
	private BrowserHelper browserHelper;
	private HomePage homePage;

	private By hotelDetailsPage = By.xpath("//div[@class='hotel-detail-page']");
	private By hotelName = By.xpath("//div[@class='hdp__header']/div[contains(@class,'main__title')]/div");
	private By hotelAddress = By.xpath("div[@class='main__address']/span");
	private By hotelImagesInHD = By.xpath(
			"//div[contains(@class,'main-gallery__carousel')]//div[contains(@class,'main-gallery__carousel__item')]/img");

	private By bookNowButton = By.xpath("//button[contains(@class,'analytics-book')]");
	private By bookNowButtonEnabled = By.cssSelector("button.analytics-book:not(.sidebar__book-disabled)");
	private By bookNowButtonDisabled = By.cssSelector("button.analytics-book.sidebar__book-disabled");
	private By selectRoomButton = By.cssSelector(".analytics-select:not(.hide)");
	private By totalPriceDisplaedInSideRack = By.xpath("//span[contains(@class,'analytics-totalprice')]");
	private By fareBreakUp = By.xpath("//div[@id='price-popup']/div[contains(@class,'farebreakuptitle')]");
	private By roomPriceInBreakUp = By.xpath("//div[text()='Room Price']/following-sibling::div/span[2]");
	private By taxesInBreakUp = By.xpath("//div[text()='Taxes']/following-sibling::div/span[2]");
	private By discountInBreakUp = By.xpath("//div[text()='Discount']/following-sibling::div/span[3]");
	private By totalInBreakUp = By.xpath("//div[text()='Total']/following-sibling::div/span/span");

	private By calendarLeftIcon = By.xpath(
			"//div[@class='dr-calendar'][not(contains(@style,'none'))]//i[contains(@class,'dr-left')][not(contains(@class,'dr-disabled'))]");
	private By checkin = By
			.xpath("//div[@id='calendar']//div[contains(@class,'dr-date dr-date-start search-box__title')]");

	private By applyCouponCodeLink = By.cssSelector(".js-applycoupon");
	private By inputCoupon = By.xpath("//div[@class='apply-screen']//input[contains(@class,'discount__voucher')]");
	private By applyCouponButton = By
			.xpath("//div[@class='apply-screen']//input[contains(@class,'discount__applybtn')]");
	private By discountAppliedValue = By.cssSelector(".discount__applied-screen:not(.hide) .analytics-discountvalue");
	private By couponApplied = By.cssSelector(".discount__applied-screen:not(.hide) .analytics-coupon");
	private By removeCoupon = By.cssSelector(".discount__applied-screen:not(.hide) .text-right a");
	private By couponInvalidError = By.xpath("//div[@class='apply-screen']//div[@id='discountError']");
	private By abouthotel = By.cssSelector(".about__title.mb10.block__title");
	private By abouthotelcontent = By.cssSelector(".about__desc.js-about");
	private By abouthotelcontentmorelink = By.cssSelector(".about__more.morelink");
	private By morelinkcontent = By.cssSelector(".morecontent>span");
	private By Treebo_Trilights = By.xpath("//h2[text()='TREEBO TRILIGHTS']");
	private By Trilights__wrapper = By.cssSelector(".trilights__wrapper");
	private By assured_quality = By.cssSelector(".amenities__guarantee.lazy");
	private By Hotel_Amenities = By.cssSelector(".hotel.mt30.block");
	private By Hotel_Accessibilities = By.cssSelector(".hotel__accessibility.block__title");
	private By Hotel_Accessibilities_Wrapper = By.cssSelector(".access.mt30.block");
	private By Rules_Policies = By.cssSelector(".about__title.mb10.block__title");
	private By ExpandButton = By.cssSelector(".icon-expand.pos-abs.main__expand.hand");
	private By ExpandImage = By.xpath(
			".//*[@id='content']//div[@class='banner-carousel__item pos-rel slick-slide slick-current slick-active']/img");
	private By NextButton = By.cssSelector(".js-main-gallery .slick-next.slick-arrow");

	public void verifyHotelDetailsPagePresence() {
		WebDriver driver = DriverManager.getInstance().getDriver();
		browserHelper = new BrowserHelper();
		homePage = new HomePage();

		// check if 500 page
		if (homePage.is500Displayed()) {
			Assert.fail("500 displayed instead of HD page for url " + browserHelper.getCurrentUrl());
		}

		try {
			WebDriverWait wait = new WebDriverWait(driver, 120);
			wait.until(ExpectedConditions.presenceOfElementLocated(hotelDetailsPage));
		} catch (Exception e) {
			// do nothing
		}

		Assert.assertTrue((driver.findElements(hotelName).size() > 0),
				"HD Page is not displayed. Current page URL is " + browserHelper.getCurrentUrl());
		Assert.assertTrue(driver.findElement(hotelName).isDisplayed(),
				"Hotel name is not displayed. Current page URL is " + browserHelper.getCurrentUrl());
		Assert.assertTrue(driver.findElement(bookNowButton).isDisplayed());
		System.out.println("Hotel Details Page is Displayed");
	}

	public String getHotelNameInHotelDetailsPage() {
		WebDriver driver = DriverManager.getInstance().getDriver();
		return driver.findElement(hotelName).getText().trim();
	}

	public String getHotelAddressInHotelDetailsPage() {
		WebDriver driver = DriverManager.getInstance().getDriver();
		return driver.findElement(hotelAddress).getText();
	}

	public void clickBookNowInHotelsDetailsPage() {
		WebDriver driver = DriverManager.getInstance().getDriver();
		browserHelper = new BrowserHelper();
		System.out.println("clicking on book now in Hotel details page");
		WebDriverWait wait = new WebDriverWait(driver, 120);
		wait.until(ExpectedConditions.presenceOfElementLocated(bookNowButton));

		browserHelper.jsclick(driver.findElement(bookNowButtonEnabled));
		System.out.println("Leaving Hotel Details Page...");
	}

	public void ensureRoomAvailableInHDPage() {
		WebDriver driver = DriverManager.getInstance().getDriver();
		if (!isBookNowButtonEnabled()) {
			if (driver.findElement(By.cssSelector(".sidebar__error")).getText()
					.equals("No of guests is greater than room capacity")) {
				Assert.assertTrue(false, "The selected room does not support room config");
			}
			System.out.println(
					"Book Now Button is disabled in HD page, trying to select either other room available or change date...");
			changeRoomOrDatesIfBookNowIsDisabled();
		}
	}

	public boolean isBookNowButtonDisplayed() {
		WebDriver driver = DriverManager.getInstance().getDriver();
		WebDriverWait wait = new WebDriverWait(driver, 120);
		wait.until(ExpectedConditions.presenceOfElementLocated(bookNowButton));
		return driver.findElement(bookNowButton).isDisplayed();
	}

	public boolean isBookNowButtonEnabled() {
		WebDriver driver = DriverManager.getInstance().getDriver();
		boolean flag = false;
		if (driver.findElements(bookNowButtonEnabled).size() > 0) {
			System.out.println("Book Now Button is enabled");
			flag = true;
		}
		if (driver.findElements(bookNowButtonDisabled).size() > 0) {
			System.out.println("Book Now Button is disabled");
			flag = false;
		}
		return flag;
	}

	public String[] getAllRoomTypesAvailable() {
		WebDriver driver = DriverManager.getInstance().getDriver();
		List<WebElement> elements = driver.findElements(By.cssSelector(".room-types__room"));
		System.out.println(("number of rooms :" + elements.size()));
		String[] roomType = new String[elements.size()];
		int index = 0;
		for (WebElement element : elements) {
			roomType[index] = element.getAttribute("roomtype");
			index = index + 1;
		}
		System.out.println("Room Types in this hotel are :" + Arrays.toString(roomType));
		return roomType;
	}

	public boolean selectRoomType(String roomType) {
		WebDriver driver = DriverManager.getInstance().getDriver();
		boolean roomAvailable;
		String selectRoomTypeButton = String.format("div[roomtype=%s] .analytics-select:not(.hide)", roomType);
		if ((roomType == "Mahogany")
				&& (driver.findElements(By.cssSelector(".room-types .slick-next.slick-arrow")).size() > 0)) {
			driver.findElement(By.cssSelector(".room-types .slick-next.slick-arrow")).click();
		}
		if (driver.findElements(By.cssSelector(selectRoomTypeButton)).size() > 0) {
			roomAvailable = true;
			driver.findElement(By.cssSelector(selectRoomTypeButton)).click();
		} else {
			roomAvailable = false;
		}
		return roomAvailable;
	}

	public void changeRoomOrDatesIfBookNowIsDisabled() {
		WebDriver driver = DriverManager.getInstance().getDriver();
		homePage = new HomePage();
		CommonUtils utils = new CommonUtils();
		if (!isBookNowButtonEnabled()) {
			if (isAnyRoomTypeAvailable()) {
				driver.findElement(selectRoomButton).click();
				try {
					Thread.sleep(5000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			} else {
				// change the date to next day
				int checkIn = utils.getDaysDiffFromSelectedDateToCurrentDate(homePage.getEnteredCheckInDate());
				int checkOut = utils.getDaysDiffFromSelectedDateToCurrentDate(homePage.getEnteredCheckOutDate());
				int updatedCheckIn = checkIn + 2;
				int updatedCheckOut = checkOut + 2;
				System.out.println("******" + checkIn + checkOut + updatedCheckIn + updatedCheckOut);
				changeDatesFromHDPage(updatedCheckIn, updatedCheckOut);
				try {
					Thread.sleep(5000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				changeRoomOrDatesIfBookNowIsDisabled();
			}
		}
	}

	// checkin days from current date, and same way checkout days from current
	// date
	public void changeDatesFromHDPage(int checkIn, int checkOut) {
		WebDriver driver = DriverManager.getInstance().getDriver();
		homePage = new HomePage();
		driver.findElement(checkin).click();
		// reset the calendar to current month
		while (driver.findElements(calendarLeftIcon).size() > 0) {
			driver.findElement(calendarLeftIcon).click();
		}
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		homePage.selectDate(checkIn);
		homePage.selectCheckOutDate(checkIn, checkOut);
	}

	public boolean isAnyRoomTypeAvailable() {
		WebDriver driver = DriverManager.getInstance().getDriver();
		return (driver.findElements(selectRoomButton).size() > 0);
	}

	public int getHotelTotalPriceInHD() {
		WebDriver driver = DriverManager.getInstance().getDriver();
		int totalPriceInHD = Integer.parseInt(driver.findElement(totalPriceDisplaedInSideRack).getText());
		System.out.println("Total Price Displayed in HD Page :" + totalPriceInHD);
		return totalPriceInHD;
	}
	public void mouseOverPriceBreakUpInHD() {
		WebDriver driver = DriverManager.getInstance().getDriver();
		browserHelper = new BrowserHelper();
		WebDriverWait wait = new WebDriverWait(driver, 120);
		wait.until(ExpectedConditions.presenceOfElementLocated(fareBreakUp));
		browserHelper.mouseOver(driver.findElement(fareBreakUp));
	}

	public void mouseOutPriceBreakUpInHD() {
		WebDriver driver = DriverManager.getInstance().getDriver();
		browserHelper = new BrowserHelper();
		browserHelper.mouseOver(driver.findElement(hotelName));
	}

	public double[] getHotelPriceBreakUpInHD() {
		WebDriver driver = DriverManager.getInstance().getDriver();
		WebDriverWait wait = new WebDriverWait(driver,120);
		double[] hotelPriceInfo = new double[4];
		mouseOverPriceBreakUpInHD();
		wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.cssSelector(".hotel__price__breakup__popup.hover-popup__twisty")));
		
		hotelPriceInfo[0] = Double.parseDouble(driver.findElement(roomPriceInBreakUp).getText());
		hotelPriceInfo[1] = Double.parseDouble(driver.findElement(taxesInBreakUp).getText());
		hotelPriceInfo[2] = Double.parseDouble(driver.findElement(discountInBreakUp).getText());
		hotelPriceInfo[3] = Double.parseDouble(driver.findElement(totalInBreakUp).getText());
		mouseOutPriceBreakUpInHD();
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println("HD Page Price details : " + "Room price : " + hotelPriceInfo[0] + " Taxes : "
				+ hotelPriceInfo[1] + " Discount :" + hotelPriceInfo[2] + " Total : " + hotelPriceInfo[3]);
		return hotelPriceInfo;
	}

	public boolean isPriceCalcultaionInBreakUpCorrectInHDPage() {
		double[] hotelPriceInfo = getHotelPriceBreakUpInHD();
		System.out.println("Should be equal :" + hotelPriceInfo[3] + " and "
				+ (hotelPriceInfo[0] + hotelPriceInfo[1] - hotelPriceInfo[2]));
		return ((int) hotelPriceInfo[3] == (int) Math.round(hotelPriceInfo[0] + hotelPriceInfo[1] - hotelPriceInfo[2]));
	}

	public boolean isTotalInBreakUpAndRoomRateEqualInHDPage() {
		int roomRate = getHotelTotalPriceInHD();
		int totalInBreakUp = (int) getHotelPriceBreakUpInHD()[3];
		System.out.println("Room rate is : " + roomRate + " and total in break up is : " + totalInBreakUp);
		return (roomRate == totalInBreakUp);
	}

	public void applyCouponInHDPage(String coupon) {
		WebDriver driver = DriverManager.getInstance().getDriver();
		System.out.println("Applying coupon in HD page :" + coupon);
		driver.findElement(applyCouponCodeLink).click();
		WebDriverWait wait = new WebDriverWait(driver, 120);
		wait.until(ExpectedConditions.presenceOfElementLocated(inputCoupon));
		driver.findElement(inputCoupon).sendKeys(coupon);
		driver.findElement(applyCouponButton).click();
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
   //get coupon discount
	public int getCouponDiscountDisplayed() {
		WebDriver driver = DriverManager.getInstance().getDriver();
		int discountApplied = (int) Math
				.round(Double.parseDouble((driver.findElement(discountAppliedValue).getText())));
		System.out.println("Discount coupon applied :" + discountApplied);
		return discountApplied;
	}
   
	public void removeCouponInHDPage() {
		WebDriver driver = DriverManager.getInstance().getDriver();
		browserHelper = new BrowserHelper();
		System.out.println("removing coupon applied");
		driver.findElement(removeCoupon).click();
		browserHelper.waitTime(3000);
	}

	public void verifyInvalidCouponError() {
		WebDriver driver = DriverManager.getInstance().getDriver();
		CommonUtils utils = new CommonUtils();
		String errorText = utils.getProperty("InvalidCouponError");
		System.out.println("Verifying invalid error");
		Assert.assertTrue(driver.findElement(couponInvalidError).isDisplayed(),
				"Invalid coupon error is not displayed");
		Assert.assertEquals(driver.findElement(couponInvalidError).getText(), errorText, "Error text not matching");
	}
   
	public void goBackToSearch() {
		WebDriver driver = DriverManager.getInstance().getDriver();
		System.out.println("Clicking on go to search from HD page");
		driver.findElement(By.cssSelector(".search-back-text")).click();
	}

	public List<String> verifyNoDuplicateImagesInHDPage() {
		WebDriver driver = DriverManager.getInstance().getDriver();
		browserHelper = new BrowserHelper();
		List<WebElement> elements = driver.findElements(hotelImagesInHD);
		System.out.println("Number of images are :" + elements.size());
		List<String> srcPathList = new ArrayList<String>();
		List<String> srcPathListDupe = new ArrayList<String>();

		for (WebElement element : elements) {
			String srcPath = element.getAttribute("src");
			System.out.println("srcPath : " + srcPath);
			driver.findElement(By.cssSelector(".slick-next.slick-arrow")).click();
			browserHelper.waitTime(1000);
			if (srcPathList.contains(srcPath)) {
				srcPathListDupe.add(srcPath);
			}
			srcPathList.add(srcPath);
		}
		System.out.println(srcPathList.toString());
		System.out.println(srcPathListDupe.toString());
		return srcPathListDupe;
	}

	public boolean areThreeTrilightsItemDisplayed() {
		WebDriver driver = DriverManager.getInstance().getDriver();
		return (driver.findElements(By.cssSelector(".trilights__item")).size() == 3);
	}

	public String getTrilightsContent() {
		WebDriver driver = DriverManager.getInstance().getDriver();
		return driver.findElement(By.cssSelector(".trilights__wrapper")).getText();
	}

	public boolean isHotelAccessibilityShown() {
		WebDriver driver = DriverManager.getInstance().getDriver();
		return (driver.findElements(By.cssSelector(".access__item")).size() > 0);
	}

	public String getHotelAccessibilityContent() {
		WebDriver driver = DriverManager.getInstance().getDriver();
		return (driver.findElement(By.cssSelector(".access")).getText());
	}

	public boolean isSimilarTreebosNearbyShown() {
		WebDriver driver = DriverManager.getInstance().getDriver();
		boolean similarTreebosDisplayed = (driver.findElements(By.cssSelector(".similar__title")).size() > 0);
		System.out.println("Similar Treebo hotels displayed : " + similarTreebosDisplayed);
		return similarTreebosDisplayed;
	}

	public void clickonhotel(String hotel) {
		WebDriver driver = DriverManager.getInstance().getDriver();
		driver.findElement(By.linkText(hotel)).click();
	}

	// verify the hotel title
	public boolean doesHotelTitleDisplayed() {
		WebDriver driver = DriverManager.getInstance().getDriver();
		System.out.println("Verify About Hotel Title is displayed");
		WebDriverWait wait = new WebDriverWait(driver, 60);
		wait.until(ExpectedConditions.presenceOfElementLocated(abouthotel));
		return driver.findElement(abouthotel).isDisplayed();

	}

	// verify the content under hotel
	public boolean doesAboutHotelContentDisplayed() {
		WebDriver driver = DriverManager.getInstance().getDriver();
		System.out.println("Verify About Hotel Content is displayed");
		WebDriverWait wait = new WebDriverWait(driver, 60);
		wait.until(ExpectedConditions.presenceOfElementLocated(abouthotelcontent));
		return driver.findElement(abouthotelcontent).isDisplayed();

	}

	// verify the taxt that appear after click on morelink
	public void clickMoreLinkandOpenedTextVerified() {
		WebDriver driver = DriverManager.getInstance().getDriver();
		System.out.println("Verify About Hotel Content More link is displayed");
		WebDriverWait wait = new WebDriverWait(driver, 60);
		wait.until(ExpectedConditions.presenceOfElementLocated(abouthotelcontentmorelink));
		Assert.assertTrue(driver.findElement(abouthotelcontentmorelink).isDisplayed());
		driver.findElement(abouthotelcontentmorelink).click();
		wait.until(ExpectedConditions.presenceOfElementLocated(morelinkcontent));
		Assert.assertTrue(driver.findElement(morelinkcontent).isDisplayed());
	}

	// verify the treebo trilight title displayed
	public boolean doesTreeboTrilightsTitleDisplayed() {
		WebDriver driver = DriverManager.getInstance().getDriver();
		browserHelper = new BrowserHelper();
		System.out.println("Verify About Treebo Trilights Title is displayed");
		WebDriverWait wait = new WebDriverWait(driver, 60);
		wait.until(ExpectedConditions.presenceOfElementLocated(Treebo_Trilights));
		return driver.findElements(Treebo_Trilights).size()>0;

	}

	// verify the treebo trilight wrapper displayed
	public boolean doesTreeboTrilightWrapperDisplayed() {
		WebDriver driver = DriverManager.getInstance().getDriver();
		System.out.println("Verify About Treebo Trilights Content is displayed");
		WebDriverWait wait = new WebDriverWait(driver, 60);
		wait.until(ExpectedConditions.presenceOfElementLocated(Trilights__wrapper));
		return driver.findElement(Trilights__wrapper).isDisplayed();
	}

	// verify the AssuredQualityImageDisplayed
	public boolean doesDisplayAssuredQualityImageDisplayed() {
		WebDriver driver = DriverManager.getInstance().getDriver();
		System.out.println("Verify Assured Quality Image is displayed");
		WebDriverWait wait = new WebDriverWait(driver, 60);
		wait.until(ExpectedConditions.presenceOfElementLocated(assured_quality));
		return driver.findElement(assured_quality).isDisplayed();

	}

	// verify the hotel amenities wrapper
	public boolean doesHotelAmenitiesWrapperDisplayed() {
		WebDriver driver = DriverManager.getInstance().getDriver();
		System.out.println("Verify Hotel Amenities is displayed");
		WebDriverWait wait = new WebDriverWait(driver, 60);
		wait.until(ExpectedConditions.presenceOfElementLocated(Hotel_Amenities));
		return driver.findElement(Hotel_Amenities).isDisplayed();
	}

	// verify the hotel accessbilities title
	public boolean doesHotelAccessbilitiesTitleDisplayed() {
		WebDriver driver = DriverManager.getInstance().getDriver();
		System.out.println("Verify Hotel Accessbilities Title is displayed");
		WebDriverWait wait = new WebDriverWait(driver, 60);
		wait.until(ExpectedConditions.presenceOfElementLocated(Hotel_Accessibilities));
		return driver.findElement(Hotel_Accessibilities).isDisplayed();
	}

	// verify hotel accessbilities wrapper
	public boolean doesHotelAccessbilitiesWrapperDisplayed() {
		WebDriver driver = DriverManager.getInstance().getDriver();
		System.out.println("Verify Hotel Accessbilities Wrapperis displayed");
		WebDriverWait wait = new WebDriverWait(driver, 60);
		wait.until(ExpectedConditions.presenceOfElementLocated(Hotel_Accessibilities_Wrapper));
		return driver.findElement(Hotel_Accessibilities_Wrapper).isDisplayed();

	}

	// verify rules and policy title
	public boolean doesRulesandPoliciesTitleDisplayed() {
		WebDriver driver = DriverManager.getInstance().getDriver();
		System.out.println("Verify Rules and Policies Title is displayed");
		WebDriverWait wait = new WebDriverWait(driver, 60);
		wait.until(ExpectedConditions.presenceOfElementLocated(Rules_Policies));
		return driver.findElement(Rules_Policies).isDisplayed();
	}

	// click on know More link
	public void clickKnowMoreLink(int id) {
		WebDriver driver = DriverManager.getInstance().getDriver();
		System.out.println("Click Know More Link");
		WebDriverWait wait = new WebDriverWait(driver, 30);
		wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(
				"//div[@class='flex-row mt10']//div[2]/p[" + id + "]/a[@class='rules__link']")));
		driver.findElement(By.xpath(
				"//div[@class='flex-row mt10']//div[2]/p[" + id + "]/a[@class='rules__link']"))
				.click();
	}

	// verify the knowmore link page
	public boolean doesKnowMoreLinkOpenedCorrectPage(int id) {
		WebDriver driver = DriverManager.getInstance().getDriver();
		browserHelper = new BrowserHelper();
		String parentWindow = browserHelper.switchToNewWindow();
		System.out.println("Verify Know More Opened Correct Page");
		boolean flag = false;

		switch (id) {
		case 1:
			flag = driver.findElement(By.xpath("//div[text()='IMPORTANT POLICIES']")).isDisplayed();
			driver.close();
			break;
		case 2:
			flag = driver.findElement(By.xpath("//div[text()='IMPORTANT POLICIES']")).isDisplayed();
			driver.close();
			break;
		case 3:
			flag = driver.findElement(By.xpath("//h3[text()=' CANCELLATION POLICY']")).isDisplayed();
			driver.close();
			break;
		case 4:
			flag = driver.findElement(By.xpath("//h3[text()=' CHECKIN-CHECKOUT']")).isDisplayed();
			driver.close();
			break;
		}
		driver.switchTo().window(parentWindow);
		return flag;
	}

	// verify the normal image displayed
	public boolean isNormalImageDisplayed() {
		WebDriver driver = DriverManager.getInstance().getDriver();
		WebDriverWait wait = new WebDriverWait(driver, 30);
		wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(
				".//*[@id='content']//div[@class='pos-rel main-gallery__carousel__item slick-slide slick-current slick-active']/img")));
		return driver
				.findElement(By
						.xpath("//*[@id='content']//div[@class='pos-rel main-gallery__carousel__item slick-slide slick-current slick-active']/img"))
				.isDisplayed();
	}

	// click on expand to zoom
	public void clickOnZoomInButton() {
		WebDriver driver = DriverManager.getInstance().getDriver();
		WebDriverWait wait = new WebDriverWait(driver, 30);
		wait.until(ExpectedConditions.presenceOfElementLocated(ExpandButton));
		driver.findElement(ExpandButton).click();
	}

	// verify the gallary image expand
	public boolean doesGallaryImageDisplayed() {
		WebDriver driver = DriverManager.getInstance().getDriver();
		WebDriverWait wait = new WebDriverWait(driver, 30);
		wait.until(ExpectedConditions.presenceOfElementLocated(ExpandImage));
		return driver.findElement(ExpandImage).isDisplayed();
	}

	// click on the next icon the gallary page
	public void clickOnNextButton() {
		WebDriver driver = DriverManager.getInstance().getDriver();
		WebDriverWait wait = new WebDriverWait(driver, 30);
		wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(
				"//*[@id='content']//div[@class='js-details-carousel banner-carousel__container pos-rel slick-initialized slick-slider']/button[contains(@class,'slick-next')]")));
		driver.findElement(By
				.xpath("//*[@id='content']//div[@class='js-details-carousel banner-carousel__container pos-rel slick-initialized slick-slider']/button[contains(@class,'slick-next')]"))
				.click();
	}

	// verify the next gallary image
	public boolean isNextGallaryImageDisplayed() {
		WebDriver driver = DriverManager.getInstance().getDriver();
		// driver.findElement(By.cssSelector(".js-main-gallery
		// .slick-next")).click();
		WebDriverWait wait = new WebDriverWait(driver, 30);
		wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath(
				"//*[@id='content']//div[@class='banner-carousel__item pos-rel slick-slide slick-current slick-active']/img"))));
		return driver
				.findElement(By
						.xpath("//*[@id='content']//div[@class='banner-carousel__item pos-rel slick-slide slick-current slick-active']/img"))
				.isDisplayed();
	}

	// click on Cross icon to close expand
	public void clickOnZoomOutButton() {
		WebDriver driver = DriverManager.getInstance().getDriver();
		driver.findElement(By.cssSelector(".banner-carousel__close.js-banner-close.icon-cross.pos-abs")).click();
	}

	// Extracting the defualt selected room type with price
	public String extactDafaultRoomTypeWithPrice() {
		WebDriver driver = DriverManager.getInstance().getDriver();
		String roomType = driver
				.findElement(By.xpath("//div[@class='sidebar__selectedroomtype hand flex-row flex--align-center']"))
				.getText().trim();
		return roomType;
	}

	public void extractRoomTypeUnavailable() {
		WebDriver driver = DriverManager.getInstance().getDriver();
		String room = driver
				.findElement(By.xpath("//div[@class='room-types__room pos-rel unavailable box-shadow flex-column']"))
				.getAttribute("roomtype");

		System.out.println(room);
	}

	// Extracting the soldout roomtype
	public String[] getSoldOutRoom() {
		WebDriver driver = DriverManager.getInstance().getDriver();
		List<WebElement> elements = driver
				.findElements(By.xpath("//div[@class='room-types__room pos-rel unavailable box-shadow flex-column']"));
		System.out.println(("number of soldOut rooms :" + elements.size()));
		String[] roomType = new String[elements.size()];
		int index = 0;
		for (WebElement element : elements) {
			roomType[index] = element.getAttribute("roomtype");
			index = index + 1;
		}
		System.out.println("Room Types in this hotel are :" + Arrays.toString(roomType));
		return roomType;
	}

	// Extracting the available roomtype
	public String[] getAvailableRoomTypes() {
		WebDriver driver = DriverManager.getInstance().getDriver();
		List<WebElement> elements = driver
				.findElements(By.xpath("//div[@class='room-types__room pos-rel box-shadow flex-column']"));
		System.out.println(("number of available rooms :" + elements.size()));
		String[] roomType = new String[elements.size()];
		int index = 0;
		for (WebElement element : elements) {
			roomType[index] = element.getAttribute("roomtype");
			index = index + 1;
		}
		System.out.println("Room Types in this hotel which are available:" + Arrays.toString(roomType));
		return roomType;
	}

	// verified the available roomtype
	public boolean doesRoomTypeAppearCorrect(String str1, String str2) {
		// split the string
		String[] room = str1.split(" ");
		System.out.println(room[0]);
		if ((room[0].trim()).equals(str2.trim()))
			return true;
		return false;
	}
	
	//click on the dropdown
	public void clickOnDropdown(){
		WebDriver driver=DriverManager.getInstance().getDriver();
		driver.findElement(By.xpath("//i[@class='icon-down-arrow js-roomtypearrow']")).click();
	}

	// get all available roomtype which not selected on dropdown
	public String[] getAvailableRoomTypesDropDownNotSelected() {
		WebDriver driver = DriverManager.getInstance().getDriver();
		List<WebElement> elements = driver
				.findElements(By.xpath("//div[@class='sidebar__roomoption flex-row flex--align-center']"));
		System.out.println(("number of rooms which are not selected :" + elements.size()));
		//check if
		if(elements.size()==0){
			System.out.println("number of rooms which are not selected  are not exist ");
			return null;
		}
		else{
		String[] roomType = new String[elements.size()];
		// String roomTypeOne = getSelectedRoomTypesDropDown();
		int index=0;
		roomType[index]=getSelectedRoomTypeDropDown();
		for (WebElement element : elements) {
			roomType[index] = element.getAttribute("roomtype");
			index = index + 1;
		}
		
		System.out.println("Room Types in this hotel which are not selected :" + Arrays.toString(roomType));
		
		return roomType;
		}

	}

	// get roomType that is selected in dropdown
	public String getSelectedRoomTypeDropDown() {
		WebDriver driver = DriverManager.getInstance().getDriver();
		String selectedRoomType = driver
				.findElement(By
						.xpath("//div[@class='sidebar__roomoption flex-row flex--align-center sidebar__roomoption--selected']"))
				.getAttribute("roomtype").trim();
	
		System.out.println(selectedRoomType);
		
		return selectedRoomType;
	}
	//get total available roomtype
	public String[] getTotalAvailableRoomTypeDropDown(){
		String selectRoom = getSelectedRoomTypeDropDown();
		WebDriver driver = DriverManager.getInstance().getDriver();
		List<WebElement> elements = driver
				.findElements(By.xpath("//div[@class='sidebar__roomoption flex-row flex--align-center']"));
		System.out.println(("number of rooms which are not selected :" + elements.size()));
		String[] roomType = new String[elements.size()+1];
		roomType[0]=selectRoom;
		int index=1;
		for (WebElement element : elements) {
			roomType[index] = element.getAttribute("roomtype");
			index = index + 1;
		}
      System.out.println("total available room in drop down  :" + Arrays.toString(roomType));
		
	return roomType;
	}

	// get all room type dropdown
	public String[] getAllRoomTypesDropDown() {
		WebDriver driver = DriverManager.getInstance().getDriver();
		List<WebElement> elements = driver
				.findElements(By.cssSelector(".sidebar__roomoption.flex-row.flex--align-center"));
		System.out.println(("number of rooms (Drop Down):" + elements.size()));
		String[] roomType = new String[elements.size()];
		int index = 0;
		for (WebElement element : elements) {
			roomType[index] = element.getAttribute("roomtype");
			index = index + 1;
		}
		System.out.println("All Room Types(Drop Down) in this hotel are :" + Arrays.toString(roomType));
		return roomType;

	}
	//get the soldout room type of drop down
	// Extracting the soldout roomtype
	public String[] getSoldOutRoomDropDown() {
		WebDriver driver = DriverManager.getInstance().getDriver();
		List<WebElement> elements = driver
				.findElements(By.xpath("//div[@class='sidebar__roomoption flex-row flex--align-center sidebar__sold']"));
		System.out.println(("number of soldOut rooms drop down:" + elements.size()));
		String[] roomType = new String[elements.size()];
		int index = 0;
		for (WebElement element : elements) {
			roomType[index] = element.getAttribute("roomtype");
			index = index + 1;
		}
		System.out.println("Room Types in this hotel are :" + Arrays.toString(roomType));
		return roomType;
	}
	//verify the available rooms type status on both hd page and drop down same
	public boolean doesRoomStatusSame(String[] arr, String[] barr){
		if(Arrays.equals(arr, barr))
			return true;
    return false;
	}

}
