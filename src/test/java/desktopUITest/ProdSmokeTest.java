package desktopUITest;

import java.text.ParseException;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import base.BrowserHelper;
import base.TestBaseSetUp;
import desktopUIPageObjects.BookingHistory;
import desktopUIPageObjects.ConfirmationPage;
import desktopUIPageObjects.HomePage;
import desktopUIPageObjects.HotelDetailsPage;
import desktopUIPageObjects.HotelResultsPage;
import desktopUIPageObjects.ItineraryPage;
import org.testng.Assert;
import utils.CommonUtils;

@Test(singleThreaded=true)
public class ProdSmokeTest extends TestBaseSetUp {

	private HomePage homePage;
	private HotelResultsPage hotelResultsPage;
	private ItineraryPage itineraryPage;
	private ConfirmationPage confirmationPage;
	private HotelDetailsPage hotelDetailsPage;
	private BookingHistory bookingHistory;
	private BrowserHelper browserHelper;

	/***
	 * This test checks hotel booking for D+1, D+2, D+7, D+15 and D+30
	 * 
	 * @throws ParseException
	 */
	@Test(groups = {  "HotelBooking" })
	public void testHotelBookingWithGuestForDifferentDaysDPlus2() throws ParseException {
		homePage = new HomePage();
		hotelResultsPage = new HotelResultsPage();
		hotelDetailsPage = new HotelDetailsPage();
		itineraryPage = new ItineraryPage();
		confirmationPage = new ConfirmationPage();
		browserHelper = new BrowserHelper();
		CommonUtils utils = new CommonUtils();

		String location = utils.getProperty("Country"); // "India"; // "India";
		int checkInFromCurrentDate = 3;
		int checkOutFromCurrentDate = checkInFromCurrentDate + 1;
		int roomIndex = 1;
		String guestName = utils.getProperty("GuestName"); // "Test";
		String guestEmail = utils.getProperty("TestBookingAccount"); // "";
		String guestMobile = utils.getProperty("GuestMobile");// "9000000000";
		String[] checkInCheckOutDates = new String[2];

		// Home Page is Displayed
		homePage.verifyHomePagePresence();
		// Do search and get checkin checkout dates
		checkInCheckOutDates = homePage.doSearch(location, checkInFromCurrentDate, checkOutFromCurrentDate, roomIndex);
		hotelResultsPage.verifyHotelResultsPagePresence();
		int hotelIndex = hotelResultsPage.getHotelIndexByHotelAvailable();
		
		String hotelName = hotelResultsPage.getHotelNameByIndex(hotelIndex);
		String hotelRoomRate = hotelResultsPage.getHotelRoomRateByIndex(hotelIndex);
		String hotelAddress = hotelResultsPage.getHotelAddressByIndex(hotelIndex);
		System.out.println(
				" Hotel Name:" + hotelName + " ,Hotel room rate : " + hotelRoomRate + " ,Hotel Address:" + hotelAddress);

		hotelResultsPage.clickQuickBookByIndex(hotelIndex);
		itineraryPage.verifyItineraryPagePresence();
		itineraryPage.clickContinueAsGuest();
		itineraryPage.bookHotelAsGuestWithPayAtHotel(guestName, guestMobile, guestEmail);
		confirmationPage.verifyConfirmationPagePresence();
		String bookingId = confirmationPage.getBookingId();
		Assert.assertEquals(guestName, confirmationPage.getGuestNameInConfPage());
		Assert.assertEquals(guestEmail, confirmationPage.getGuestEmailInConfPage());
		Assert.assertEquals(guestMobile, confirmationPage.getGuestMobileInConfPage());
		Assert.assertEquals(hotelName, confirmationPage.getHotelNameInConfirmationPage());

		System.out.println(utils.formatDate(checkInCheckOutDates[0]).toUpperCase());
		System.out.println(confirmationPage.getHotelCheckInDateInConfirmationPage().toUpperCase());
		Assert.assertEquals(utils.formatDate(checkInCheckOutDates[0]).toUpperCase(),
				confirmationPage.getHotelCheckInDateInConfirmationPage().toUpperCase());
		Assert.assertEquals(utils.formatDate(checkInCheckOutDates[1]).toUpperCase(),
				confirmationPage.getHotelCheckOutDateInConfirmationPage().toUpperCase());
	}
	
	@Test(groups = { "HotelBooking" })
	public void testHotelBookingWithGuestForDifferentDaysDPlus7() throws ParseException {
		homePage = new HomePage();
		hotelResultsPage = new HotelResultsPage();
		hotelDetailsPage = new HotelDetailsPage();
		itineraryPage = new ItineraryPage();
		confirmationPage = new ConfirmationPage();
		browserHelper = new BrowserHelper();
		CommonUtils utils = new CommonUtils();

		String location = utils.getProperty("Country"); // "India"; // "India";
		int checkInFromCurrentDate = 8;
		int checkOutFromCurrentDate = checkInFromCurrentDate + 1;
		int roomIndex = 1;
		String guestName = utils.getProperty("GuestName"); // "Test";
		String guestEmail = utils.getProperty("TestBookingAccount"); // "";
		String guestMobile = utils.getProperty("GuestMobile");// "9000000000";
		String[] checkInCheckOutDates = new String[2];

		// Home Page is Displayed
		homePage.verifyHomePagePresence();
		// Do search and get checkin checkout dates
		checkInCheckOutDates = homePage.doSearch(location, checkInFromCurrentDate, checkOutFromCurrentDate, roomIndex);
		hotelResultsPage.verifyHotelResultsPagePresence();
		int hotelIndex = hotelResultsPage.getHotelIndexByHotelAvailable();
		
		String hotelName = hotelResultsPage.getHotelNameByIndex(hotelIndex);
		String hotelRoomRate = hotelResultsPage.getHotelRoomRateByIndex(hotelIndex);
		String hotelAddress = hotelResultsPage.getHotelAddressByIndex(hotelIndex);
		System.out.println(
				" Hotel Name:" + hotelName + " ,Hotel room rate : " + hotelRoomRate + " ,Hotel Address:" + hotelAddress);

		hotelResultsPage.clickQuickBookByIndex(hotelIndex);
		itineraryPage.verifyItineraryPagePresence();
		itineraryPage.clickContinueAsGuest();
		itineraryPage.bookHotelAsGuestWithPayAtHotel(guestName, guestMobile, guestEmail);
		confirmationPage.verifyConfirmationPagePresence();
		String bookingId = confirmationPage.getBookingId();
		Assert.assertEquals(guestName, confirmationPage.getGuestNameInConfPage());
		Assert.assertEquals(guestEmail, confirmationPage.getGuestEmailInConfPage());
		Assert.assertEquals(guestMobile, confirmationPage.getGuestMobileInConfPage());
		Assert.assertEquals(hotelName, confirmationPage.getHotelNameInConfirmationPage());

		System.out.println(utils.formatDate(checkInCheckOutDates[0]).toUpperCase());
		System.out.println(confirmationPage.getHotelCheckInDateInConfirmationPage().toUpperCase());
		Assert.assertEquals(utils.formatDate(checkInCheckOutDates[0]).toUpperCase(),
				confirmationPage.getHotelCheckInDateInConfirmationPage().toUpperCase());
		Assert.assertEquals(utils.formatDate(checkInCheckOutDates[1]).toUpperCase(),
				confirmationPage.getHotelCheckOutDateInConfirmationPage().toUpperCase());
	}
	
	@Test(groups = { "HotelBooking" })
	public void testHotelBookingWithGuestForDifferentDaysDPlus15() throws ParseException {
		homePage = new HomePage();
		hotelResultsPage = new HotelResultsPage();
		hotelDetailsPage = new HotelDetailsPage();
		itineraryPage = new ItineraryPage();
		confirmationPage = new ConfirmationPage();
		browserHelper = new BrowserHelper();
		CommonUtils utils = new CommonUtils();

		String location = utils.getProperty("Country"); // "India"; // "India";
		int checkInFromCurrentDate = 16;
		int checkOutFromCurrentDate = checkInFromCurrentDate + 1;
		int roomIndex = 1;
		String guestName = utils.getProperty("GuestName"); // "Test";
		String guestEmail = utils.getProperty("TestBookingAccount"); // "";
		String guestMobile = utils.getProperty("GuestMobile");// "9000000000";
		String[] checkInCheckOutDates = new String[2];

		// Home Page is Displayed
		homePage.verifyHomePagePresence();
		// Do search and get checkin checkout dates
		checkInCheckOutDates = homePage.doSearch(location, checkInFromCurrentDate, checkOutFromCurrentDate, roomIndex);
		hotelResultsPage.verifyHotelResultsPagePresence();
		int hotelIndex = hotelResultsPage.getHotelIndexByHotelAvailable();
		
		String hotelName = hotelResultsPage.getHotelNameByIndex(hotelIndex);
		String hotelRoomRate = hotelResultsPage.getHotelRoomRateByIndex(hotelIndex);
		String hotelAddress = hotelResultsPage.getHotelAddressByIndex(hotelIndex);
		System.out.println(
				" Hotel Name:" + hotelName + " ,Hotel room rate : " + hotelRoomRate + " ,Hotel Address:" + hotelAddress);

		hotelResultsPage.clickQuickBookByIndex(hotelIndex);
		itineraryPage.verifyItineraryPagePresence();
		itineraryPage.clickContinueAsGuest();
		itineraryPage.bookHotelAsGuestWithPayAtHotel(guestName, guestMobile, guestEmail);
		confirmationPage.verifyConfirmationPagePresence();
		String bookingId = confirmationPage.getBookingId();
		Assert.assertEquals(guestName, confirmationPage.getGuestNameInConfPage());
		Assert.assertEquals(guestEmail, confirmationPage.getGuestEmailInConfPage());
		Assert.assertEquals(guestMobile, confirmationPage.getGuestMobileInConfPage());
		Assert.assertEquals(hotelName, confirmationPage.getHotelNameInConfirmationPage());

		System.out.println(utils.formatDate(checkInCheckOutDates[0]).toUpperCase());
		System.out.println(confirmationPage.getHotelCheckInDateInConfirmationPage().toUpperCase());
		Assert.assertEquals(utils.formatDate(checkInCheckOutDates[0]).toUpperCase(),
				confirmationPage.getHotelCheckInDateInConfirmationPage().toUpperCase());
		Assert.assertEquals(utils.formatDate(checkInCheckOutDates[1]).toUpperCase(),
				confirmationPage.getHotelCheckOutDateInConfirmationPage().toUpperCase());
	}
	
	@Test(groups = { "HotelBooking" })
	public void testHotelBookingWithGuestForDifferentDaysDPlus30() throws ParseException {
		homePage = new HomePage();
		hotelResultsPage = new HotelResultsPage();
		hotelDetailsPage = new HotelDetailsPage();
		itineraryPage = new ItineraryPage();
		confirmationPage = new ConfirmationPage();
		browserHelper = new BrowserHelper();
		CommonUtils utils = new CommonUtils();

		String location = utils.getProperty("Country"); // "India"; // "India";
		int checkInFromCurrentDate = 31;
		int checkOutFromCurrentDate = checkInFromCurrentDate + 1;
		int roomIndex = 1;
		String guestName = utils.getProperty("GuestName"); // "Test";
		String guestEmail = utils.getProperty("TestBookingAccount"); // "";
		String guestMobile = utils.getProperty("GuestMobile");// "9000000000";
		String[] checkInCheckOutDates = new String[2];

		// Home Page is Displayed
		homePage.verifyHomePagePresence();
		// Do search and get checkin checkout dates
		checkInCheckOutDates = homePage.doSearch(location, checkInFromCurrentDate, checkOutFromCurrentDate, roomIndex);
		hotelResultsPage.verifyHotelResultsPagePresence();
		int hotelIndex = hotelResultsPage.getHotelIndexByHotelAvailable();
		
		String hotelName = hotelResultsPage.getHotelNameByIndex(hotelIndex);
		String hotelRoomRate = hotelResultsPage.getHotelRoomRateByIndex(hotelIndex);
		String hotelAddress = hotelResultsPage.getHotelAddressByIndex(hotelIndex);
		System.out.println(
				" Hotel Name:" + hotelName + " ,Hotel room rate : " + hotelRoomRate + " ,Hotel Address:" + hotelAddress);

		hotelResultsPage.clickQuickBookByIndex(hotelIndex);
		itineraryPage.verifyItineraryPagePresence();
		itineraryPage.clickContinueAsGuest();
		itineraryPage.bookHotelAsGuestWithPayAtHotel(guestName, guestMobile, guestEmail);
		confirmationPage.verifyConfirmationPagePresence();
		String bookingId = confirmationPage.getBookingId();
		Assert.assertEquals(guestName, confirmationPage.getGuestNameInConfPage());
		Assert.assertEquals(guestEmail, confirmationPage.getGuestEmailInConfPage());
		Assert.assertEquals(guestMobile, confirmationPage.getGuestMobileInConfPage());
		Assert.assertEquals(hotelName, confirmationPage.getHotelNameInConfirmationPage());

		System.out.println(utils.formatDate(checkInCheckOutDates[0]).toUpperCase());
		System.out.println(confirmationPage.getHotelCheckInDateInConfirmationPage().toUpperCase());
		Assert.assertEquals(utils.formatDate(checkInCheckOutDates[0]).toUpperCase(),
				confirmationPage.getHotelCheckInDateInConfirmationPage().toUpperCase());
		Assert.assertEquals(utils.formatDate(checkInCheckOutDates[1]).toUpperCase(),
				confirmationPage.getHotelCheckOutDateInConfirmationPage().toUpperCase());
	}
	
//	@DataProvider(name = "checkinDate",parallel=true)
//	public Object[][] createData(){  
//		return new Object[][] {
//		      { new Integer(3) },
//		      { new Integer(8) },
//		      { new Integer(16) },
//		      { new Integer(31) },
//		};
//   } 
}