package desktopUITest;

import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.testng.annotations.Test;
import com.google.common.collect.Maps;
import base.HotelLogixAPI;
import utils.DBUtils;
import java.util.Map;
import java.util.Set;
import java.io.FileNotFoundException;
import java.io.IOException;
import javax.xml.parsers.ParserConfigurationException;
import org.xml.sax.SAXException;
import java.sql.SQLException;

public class AvailabilityTest {

	/**
	 * This test checks the availability in HX and DB and compare
	 * 
	 * @throws InterruptedException
	 * @throws IOException
	 * @throws SQLException
	 * @throws SAXException
	 * @throws ParserConfigurationException
	 * @throws FileNotFoundException
	 */
	@Test(groups = { "Availability" })
	public void testHXDBDifferenceReport()
			throws IOException, InterruptedException, SQLException, ParserConfigurationException, SAXException {

		HotelLogixAPI api = new HotelLogixAPI();
		DBUtils db = new DBUtils();
		int loopCnt = Integer.parseInt(System.getProperty("numberOfDays"));
		String checkin = null, checkout = null;
		//System.out.println("******* date *******");
		DateTime dt = new DateTime();
		DateTimeFormatter dtf = DateTimeFormat.forPattern("yyyy-MM-dd");
		// check availability for next 15 days
		for (int i = 0; i < loopCnt; i++) {
			checkin = dtf.print(dt.plusDays(i));
			checkout = dtf.print(dt.plusDays(i + 1));

			//System.out.println("############################ " + checkin + " #############################\n");

			Map<String, Integer> hxAvailability = api.hxRoomAvailability(checkin, checkout);
			Map<String, Integer> dbAvailaiblity = db.getRoomAvailability(checkin);

			// remove same entry if key exists in both
			for (String key : hxAvailability.keySet()) {
				if (hxAvailability.containsKey(key) && dbAvailaiblity.containsKey(key)) {
					if (hxAvailability.get(key) == dbAvailaiblity.get(key)) {
						hxAvailability.remove(key);
						dbAvailaiblity.remove(key);
					}
				}
			}

			// remove if value is zero in dbAvailability and its not present in
			// hxAvailability, which means that it was sold out
			for (String key : dbAvailaiblity.keySet()) {
				if (dbAvailaiblity.get(key) == 0) {
					if (!hxAvailability.containsKey(key)) {
						dbAvailaiblity.remove(key);
					}
				}
			}

			System.out.println("******************* Difference in HX vs DB *****************************");
			System.out.println(" |     Date     |   HX Availability  |  DB Availability  | Hotel Name and Room Type             ");
			System.out.println(" __________________________________________________________________________________________");
			int cnt = 0;
			for (String key : hxAvailability.keySet()) {
				if (hxAvailability.containsKey(key) && dbAvailaiblity.containsKey(key)) {
					if (!(hxAvailability.get(key) == dbAvailaiblity.get(key))) {
						int hxcount = hxAvailability.get(key);
						String hxCount = String.format("%02d", hxcount);
						int dbcount = dbAvailaiblity.get(key);
						String dbCount = String.format("%02d", dbcount);
						//System.out.println("For " + key + " availability in HX is :" + hxCount
						//		+ " whereas in DB :" + dbCount);	
						System.out.println(" | "+checkin+"   |          " +hxCount+"        |        "    +dbCount+ "         |     " +  key             );
						cnt = cnt + 1;
					}
				}
			}
			if (cnt == 0){
				System.out.println(" | "+checkin+"   |          " +  "    Wow!!! No Discrepancy    "           );
			}
			
			//System.out.println("\n\n");
			
			
			//System.out.println(" |     Date     |   HX Availability  |  DB Availability  | Hotel Name and Room Type             ");
			//System.out.println(" __________________________________________________________________________________________");
			int cnt2 = 0;
			int cnt13 = 0;
			for (String key : dbAvailaiblity.keySet()) {
				if (!hxAvailability.containsKey(key)) {
					int dbcount = dbAvailaiblity.get(key);
					String dbCount = String.format("%02d", dbcount);
					//String hxCount = "";
					//System.out.println("For " + key + " with availability " + dbAvailaiblity.get(key)
						//	+ " present only in DB but not in HX.");
					cnt13 = cnt13 +1;
					if (cnt13 == 1){
						System.out.println("\n***** Below discrepancy is due to no response from HX for availability count, due to sold out etc.*****");	
					}
					System.out.println(" | "+checkin+"   |          " + "00" + "        |        "    +dbCount+ "         |     " +  key             );
					cnt2 = cnt2 + 1;
				}
			}
//			if (cnt2 == 0){
//				System.out.println(" | "+checkin+"   |          " +  "    Wow!!! No Discrepancy    "           );
//			}
			System.out.println("\n\n");

            
    			System.out.println("************************************* In HX but not in DB *****************************");
    			System.out.println(" |     Date     |   HX Availability  |  DB Availability  | Hotel Name and Room Type             ");
    			System.out.println(" __________________________________________________________________________________________");
    			int cnt1 = 0;
    			for (String key : hxAvailability.keySet()) {
    				if (!dbAvailaiblity.containsKey(key)) {
    					int hxcount = hxAvailability.get(key);
    					String hxCount = String.format("%02d", hxcount);
    					//System.out.println("For " + key + " with availability " + hxAvailability.get(key)
    						//	+ " present only in HX but not in DB.");
    					System.out.println(" | "+checkin+"   |          " +hxCount+"        |        "    + "           |     " +  key             );
    					cnt1 = cnt1 + 1;
    				}
    			}
    			if (cnt1 == 0){
    				System.out.println(" | "+checkin+"   |          " +  "    Wow!!! No Discrepancy    "           );
    			}
    			System.out.println("\n\n");
            


			// Maps.difference(hxAvailability,dbAvailaiblity);
			// System.out.println(Maps.difference(hxAvailability,dbAvailaiblity).toString());
			//System.out.println("############################ " + checkin + " #############################\n\n");
		}
		System.out.println("TreeboAvailability");
	}
	
//	@Test(groups = { "AvailabilityPackage" },timeOut = 3600000,enabled = false)
//	public void testHotelAvailablePackageReport()
//			throws IOException, InterruptedException, SQLException, ParserConfigurationException, SAXException {
//
//		HotelLogixAPI api = new HotelLogixAPI();
//		int loopCnt = Integer.parseInt(System.getProperty("numberOfDays"));
//		String checkin = null, checkout = null;
//		//System.out.println("******* date *******");
//		DateTime dt = new DateTime();
//		DateTimeFormatter dtf = DateTimeFormat.forPattern("yyyy-MM-dd");
//         
//			checkin = dtf.print(dt.plusDays(loopCnt));
//			checkout = dtf.print(dt.plusDays(loopCnt + 1));
//			
//			System.out.println(api.getRoomTypeAndMaxPaxFromHX("2016-05-05", "2016-05-06", "Elmas").toString());
//
//			//System.out.println("############################ " + checkin + " #############################\n");
//
////			Map<String, Set<String>> hxPackageAvailability = api.hxHotelPackagesAvailable(checkin, checkout);
//			
////			System.out.println("Checkin Date used : " + checkin);
////			System.out.println("Number of hotels : " + hxPackageAvailability.size());
////			
////			for (String key : hxPackageAvailability.keySet()){
////				System.out.println("Hotel Name : " + key + ", Package name available: " + hxPackageAvailability.get(key).toString());
////			}
////				
////			
////			
////			System.out.println("\n\n");
////		System.out.println("TreeboAvailability");
//	}
}