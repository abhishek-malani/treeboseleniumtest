package desktopUITest;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;

import javax.xml.parsers.ParserConfigurationException;

import org.apache.http.ParseException;
import org.apache.http.client.ClientProtocolException;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.json.JSONException;
import org.testng.annotations.Test;
import org.xml.sax.SAXException;

import base.HotelLogixAPI;
import utils.DBUtils;

public class HXOBDNRTest {

	@Test(groups = { "CancelBooking" })
	public void testMoveOBDNR() throws SQLException, ClientProtocolException, IOException, ParserConfigurationException,
			SAXException, ParseException, JSONException {

		/*
		 * API calls to change the group stay
		 * 
		 * DB Call to get all bookings with name OBDNR and todays date as
		 * checkin
		 * 
		 * For each booking - (For Single booking - S, for group booking G)
		 * 
		 * WSAUTH - access key Get Counter - get with hotel id Hotel Login -
		 * counter id and access Key Get group/Get booking - - Group booking ID
		 * Editbooking - Get Group => Main id ChangeStay - "checkOutDate":
		 * "2017-02-11", "id": "A3SvIguXuA||", commiteditbooking - Use id of
		 * group from edit booking response logout - access Key
		 */
		// Method to get the access key

		DBUtils db = new DBUtils();
		HotelLogixAPI api = new HotelLogixAPI();

		// *******************
		// DB Call to get all bookings with name OBDNR and todays date as
		// checkin
		ConcurrentHashMap<Integer, ConcurrentHashMap<String, String>> bookingRecords = db.getOBDNRBookingsCurrentDate();
		// *******************

		List<String> failedMoveMessage = new ArrayList<String>();
		List<String> canceledOBDNR = new ArrayList<String>();

		// Iterate Through the booking
		// for (Integer key : bookingRecords.keySet()) {
		bookingRecords.keySet().parallelStream().forEach((key) -> {
			try {
				ConcurrentHashMap<String, String> bookingOBDNRInfo = bookingRecords.get(key);
				// String HotelId = "14074";//bookingOBDNRInfo.get("HotelCode");
				// String GroupCode = "G
				// 09224590";//bookingOBDNRInfo.get("GroupCode");
				// String BookingId =
				// "09228260";//bookingOBDNRInfo.get("BookingId");
				// String CheckinDate =
				// "2016-11-22";//bookingOBDNRInfo.get("CheckinDate");
				// String CheckoutDate =
				// "2016-11-28";//bookingOBDNRInfo.get("CheckoutDate");

				String HotelId = bookingOBDNRInfo.get("HotelCode");
				String GroupCode = bookingOBDNRInfo.get("GroupCode");
				String BookingId = bookingOBDNRInfo.get("BookingId");
				String CheckinDate = bookingOBDNRInfo.get("CheckinDate");
				String CheckoutDate = bookingOBDNRInfo.get("CheckoutDate");

				// Get Next checkinDate
				DateTime dt = new DateTime(CheckinDate);
				DateTimeFormatter dtf = DateTimeFormat.forPattern("yyyy-MM-dd");
				String newCheckinDate = dtf.print(dt.plusDays(1));
				System.out.println("Old checkin date: " + CheckinDate + " new checkin date: " + newCheckinDate);

				if (newCheckinDate.equals(CheckoutDate)) {
					System.out.println("Single day duration. Can not change stay. Need to be canceled");
//					System.out.println("Currently canceling OBDNR with details : " + "HotelId: " + HotelId
//							+ " GroupCode: " + GroupCode + " BookingId: " + BookingId + " CheckinDate: " + CheckinDate
//							+ " CheckoutDate: " + CheckoutDate);

//					// Get Counter - get with hotel id
//					String counterId = api.getHotelCounters(HotelId);
//
//					// Login to Hotel with counterId extracted and get access
//					// key and secret
//					String[] keyNSecret = api.loginToHotelAndGetAccessKeyAndAccessSecret(HotelId, counterId);
//
//					// Get Group booking
//					String groupMainId = api.getGroup(keyNSecret[0], keyNSecret[1], GroupCode);
//
//					// Call Edit booking with groupMainId
//					String[] id = api.extractBookingIdAndGroupId(
//							api.editBooking(keyNSecret[0], keyNSecret[1], groupMainId), BookingId);
//
//					// Call cancel and commit booking
//					api.cancelAndCommitBooking(keyNSecret[0], keyNSecret[1], id[0]);

					// Record the cancel OBDNRs
					canceledOBDNR.add("\n" + "Single day OBDNR, details : " + "HotelId: " + HotelId + " GroupCode: "
							+ GroupCode + " BookingId: " + BookingId + " CheckinDate: " + CheckinDate
							+ " CheckoutDate: " + CheckoutDate);

//					// Logged out from hotel
//					api.logoutHotel(keyNSecret[0], keyNSecret[1]);

				} else {

					System.out.println("Currently moving OBDNR with details : " + "HotelId: " + HotelId + " GroupCode: "
							+ GroupCode + " BookingId: " + BookingId + " CheckinDate: " + CheckinDate
							+ " CheckoutDate: " + CheckoutDate);

					// Get Counter - get with hotel id
					String counterId = api.getHotelCounters(HotelId);

					// Login to Hotel with counterId extracted and get access
					// key and secret
					String[] keyNSecret = api.loginToHotelAndGetAccessKeyAndAccessSecret(HotelId, counterId);

					// Get Group booking
					String groupMainId = api.getGroup(keyNSecret[0], keyNSecret[1], GroupCode);

					// Call Edit booking with groupMainId
					String[] id = api.extractBookingIdAndGroupId(
							api.editBooking(keyNSecret[0], keyNSecret[1], groupMainId), BookingId);

					// Call change stay to update the date from today checkin to
					// next day
					api.changeStay(keyNSecret[0], keyNSecret[1], id[0], newCheckinDate, CheckoutDate);

					// Call commit edit booking to save the changes
					api.commitEditBooking(keyNSecret[0], keyNSecret[1], id[1]);

					// Logged out from hotel
					api.logoutHotel(keyNSecret[0], keyNSecret[1]);
				}

			} catch (Exception e) {
				e.printStackTrace();
				failedMoveMessage.add("\n" + "Error happened with " + bookingRecords.get(key));
			}
		});

		System.out.println("Start token");
		if (failedMoveMessage.size() > 0) {
			System.out.println("Error happened with " + Arrays.toString(failedMoveMessage.toArray()));
		}
		System.out.println("\n");
		System.out.println("Single Day OBDNRs " + Arrays.toString(canceledOBDNR.toArray()));
		System.out.println("End token");

	}
}
