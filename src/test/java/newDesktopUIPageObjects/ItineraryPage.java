package newDesktopUIPageObjects;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;

import org.apache.commons.lang3.RandomStringUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import utils.CommonUtils;
import base.BrowserHelper;
import base.DriverManager;
import desktopUIPageObjects.HomePage;

public class ItineraryPage {
	private BrowserHelper browserHelper;
	private HomePage homePage;

	private By itineraryPage = By.xpath("//div[@class='itinerary-page']");
	private By continueAsGuest = By.xpath("//button[@id='countinueGuest']");
	private By guestName = By.xpath("//div[contains(@class,'guest-details__name')]/input");
	private By guestMobile = By.xpath("//div[contains(@class,'guest-details__mobile')]/input");
	private By guestEmail = By.xpath("//div[contains(@class,'guest-details__email')]/input");
	private By guestSpecialRequest = By.xpath("//div[contains(@class,'request-details__specific')]/textarea");
	private By payNowButton = By.xpath("//button[@id='payNow']");
	private By payAtHotelButton = By.xpath("//button[@id='payatHotel']");
	private By soldoutMessage = By.xpath("//div[contains(@class,'soldout-modal')]");
	private By soldoutModalTitle = By.xpath("//div[@class='soldout-modal__title']"); // Oops!
																						// The
																						// hotel
																						// you
																						// picked
																						// is
																						// no
																						// longer
																						// available.
	private By soldoutModalInfo = By.xpath("//div[@class='soldout-modal__info']"); // Please
																					// choose
																					// another
																					// hotel
																					// or
																					// modify
																					// your
																					// travel
																					// dates.
	private By chooseAnotherHotel = By.xpath("//a[text()= 'choose another hotel']");
	private By modifyTravelDates = By.xpath("//a[text()= 'modify travel dates']");

	private By hotelNameInItineraryPage = By.xpath("//div[@class='itinerary-view__hotel-info__name']/a");
	private By hotelAddressInItineraryPage = By.xpath("//div[@class='itinerary-view__hotel-info__address']");
	private By hotelCheckInDateInItineraryPage = By.xpath("//div[contains(@class,'analytics-checkin')]");
	private By hotelCheckOutDateInItineraryPage = By.xpath("//div[contains(@class,'analytics-checkout')]");
	private By guestDetailsInItineraryPage = By
			.xpath("//div[@class='itinerary-view__booking']/div[3]//div[@class='itinerary-view__booking-info']");
	private By roomInfoInItineraryPage = By.xpath(
			"//div[@class='itinerary-view__booking']/div[4]//div[contains(@class,'itinerary-view__booking-info')]/span");
	private By roomTypeInItineraryPage = By
			.xpath("//div[@class='itinerary-view__booking']/div[5]//div[@class='itinerary-view__booking-info']");

	private By roomPrice = By.xpath("//span[@id='roomPrice']");
	private By totalTax = By.xpath("//span[@id='totalTax']");
	private By discountValue = By.xpath("//span[@class='analytics-discountvalue']");
	private By grandTotal = By.xpath("//span[@id='grandTotal']");

	private By applyCouponCodeLink = By.cssSelector(".js-applycoupon");
	private By inputCoupon = By.xpath("//div[@class='apply-screen']//input[contains(@class,'discount__voucher')]");
	private By applyCouponButton = By
			.xpath("//div[@class='apply-screen']//input[contains(@class,'discount__applybtn')]");
	private By discountAppliedValue = By.cssSelector(".discount__applied-screen:not(.hide) .analytics-discountvalue");
	private By couponApplied = By.cssSelector(".discount__applied-screen:not(.hide) .analytics-coupon");
	private By removeCoupon = By.cssSelector(".discount__applied-screen:not(.hide) .text-right a");
	private By couponInvalidError = By.xpath("//div[@class='apply-screen']//div[@id='discountError']");

	private By guestLoginTreeboEmail = By.xpath("//input[@id='guestLoginEmail']");
	private By guestLoginTreeboPassword = By.xpath("//input[@id='guestLoginPassword']");
	private By guestLoginButton = By.xpath("//button[@id='guestLoginBtn']");

	private By newUserRegisterLink = By.xpath("//span[@id='guestRegister']");

	private By guestLoginFBButton = By.xpath("//button[@id='guestFBLogin']");
	private By fbEmailField = By.xpath("//html[@id='facebook']//input[@id='email']");
	private By fbPasswordField = By.xpath("//html[@id='facebook']//input[@id='pass']");
	private By fbLoginButton = By.xpath("//html[@id='facebook']//input[@name='login']");

	private By guestLoginGoogleButton = By.xpath("//button[@id='guestGoogleLogin']");
	private By gmailEmailField = By.xpath("//input[@id='Email']");
	private By gmailNextLink = By.xpath("//input[@id='next']");
	private By gmailPasswordField = By.xpath("//input[@id='Passwd']");
	private By gmailSignInButton = By.xpath("//input[@id='signIn']");

	private By guestLoggedIn = By.xpath("//div[@id='userLoggedin']//a[@id='changeUser']");

	// payU
	private By ccNumber = By.xpath("//div[@id='credit']//input[@id='ccard_number']");
	private By ccName = By.xpath("//div[@id='credit']//input[@id='cname_on_card']");
	private By cvvNumber = By.xpath("//div[@id='credit']//input[@id='ccvv_number']");
	private By monthSelect = By.xpath("//div[@id='credit']//select[@id='cexpiry_date_month']");
	private By mayMonth = By.xpath("//div[@id='credit']//select[@id='cexpiry_date_month']/option[@value='05']");
	private By YearSelect = By.xpath("//div[@id='credit']//select[@id='cexpiry_date_year']");
	private By yearOption = By.xpath("//div[@id='credit']//select[@id='cexpiry_date_year']/option[@value='2017']");
	private By ccPayButton = By.xpath("//div[@id='credit']//input[@id='pay_button']");
	private By email = By.xpath("//div[@class='float-labels guest-login__name']/input");
	private By checkBox = By.xpath("//input[@type='checkbox']");

	public void verifyItineraryPagePresence() {
		WebDriver driver = DriverManager.getInstance().getDriver();
		browserHelper = new BrowserHelper();
		homePage = new HomePage();

		// check if 500 page
		if (homePage.is500Displayed()) {
			Assert.fail("500 displayed instead of itinerary page for url " + browserHelper.getCurrentUrl());
		}

		try {
			WebDriverWait wait = new WebDriverWait(driver, 120);
			wait.until(ExpectedConditions.presenceOfElementLocated(itineraryPage));
		} catch (Exception e) {
			// Do nothing
		}

		Assert.assertTrue((driver.findElements(itineraryPage).size() > 0),
				"Itinerary page is not displayed for URL " + browserHelper.getCurrentUrl());
		Assert.assertTrue(driver.findElement(itineraryPage).isDisplayed(),
				"Itinerary page is not displayed for URL " + browserHelper.getCurrentUrl());
		System.out.println("Itinerary Page is Displayed");
	}

	public void clickContinueAsGuest() {
		// WebDriver driver = DriverManager.getInstance().getDriver();
		// try {
		// Thread.sleep(5000);
		// System.out.println("clicked on continue as guest");
		// driver.findElement(continueAsGuest).click();
		// } catch (Exception e) {
		// // TODO Auto-generated catch block
		// e.printStackTrace();
		// }
		// try {
		// Thread.sleep(5000);
		// } catch (InterruptedException e) {
		// // TODO Auto-generated catch block
		// e.printStackTrace();
		// }
	}

	public void bookHotelAsGuestWithPayAtHotel(String strGuestName, String strGuestMobile, String strGuestEmail) {
		WebDriver driver = DriverManager.getInstance().getDriver();
		homePage = new HomePage();
		WebDriverWait wait = new WebDriverWait(driver, 120);

		driver.findElement(By.cssSelector(".guest-login__form-container input[name=email]")).sendKeys(strGuestEmail);
		driver.findElement(By.cssSelector(".analytics-emaillogin.checkout__action.checkout__action--continue")).click();
		wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.cssSelector(".guest-details__name input"))));
		driver.findElement(By.cssSelector(".guest-details__name input")).sendKeys(strGuestName);
		driver.findElement(By.cssSelector(".guest-details__mobile input")).sendKeys(strGuestMobile);
		driver.findElement(By.cssSelector(".checkout__action.checkout__action--continue")).click();
		wait.until(ExpectedConditions.visibilityOf(driver.findElement(
				By.cssSelector(".analytics-pay.itinerary-view__action__item.btn.btn--secondary.btn--round"))));
		driver.findElement(By.cssSelector(".analytics-pay.itinerary-view__action__item.btn.btn--secondary.btn--round"))
				.click();

		homePage.waitForLoaderToDisappear();
		System.out.println("Booking hotel with details: " + "Guest Name : " + strGuestName + " Guest Mobile : "
				+ strGuestMobile + " Guest Email : " + strGuestEmail);
		Assert.assertFalse(checkIfItineraryPageHasError(), "Booking error");
	}

	public void bookHotelAsGuestWithPayAtHotelWithSpecialRequest(String strGuestName, String strGuestMobile,
			String strGuestEmail, String strSpecialRequest) {
		WebDriver driver = DriverManager.getInstance().getDriver();
		homePage = new HomePage();
		WebDriverWait wait = new WebDriverWait(driver, 120);

		driver.findElement(By.cssSelector(".guest-login__form-container input[name=email]")).sendKeys(strGuestEmail);
		driver.findElement(By.cssSelector(".analytics-emaillogin.checkout__action.checkout__action--continue")).click();
		wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.cssSelector(".guest-details__name input"))));
		driver.findElement(By.cssSelector(".guest-details__name input")).sendKeys(strGuestName);
		driver.findElement(By.cssSelector(".guest-details__mobile input")).sendKeys(strGuestMobile);
		driver.findElement(By.cssSelector(".checkout__action.checkout__action--continue")).click();
		wait.until(ExpectedConditions.visibilityOf(driver.findElement(
				By.cssSelector(".analytics-pay.itinerary-view__action__item.btn.btn--secondary.btn--round"))));
		driver.findElement(By.cssSelector(".request-details__item.request-details__specific>textarea")).clear();
		driver.findElement(By.cssSelector(".request-details__item.request-details__specific>textarea"))
				.sendKeys(strSpecialRequest);
		driver.findElement(By.cssSelector(".analytics-pay.itinerary-view__action__item.btn.btn--secondary.btn--round"))
				.click();

		homePage.waitForLoaderToDisappear();
		System.out.println("Booking hotel with details: " + "Guest Name : " + strGuestName + " Guest Mobile : "
				+ strGuestMobile + " Guest Email : " + strGuestEmail);
		Assert.assertFalse(checkIfItineraryPageHasError(), "Booking error");
	}

	public boolean checkIfItineraryPageHasError() {
		WebDriver driver = DriverManager.getInstance().getDriver();
		browserHelper = new BrowserHelper();

		if (browserHelper.getCurrentUrl().contains("error=Unable%20to%20save%20booking")) {
			System.out.println("******* Unable to save booking ***********" + browserHelper.getCurrentUrl());
			return true;
		} else if (browserHelper.getCurrentUrl().contains("error")) {
			System.out.println("********* Some error : *******" + browserHelper.getCurrentUrl());
			return true;
		}

		return false;
	}

	public String[] bookHotelAsGuestWithPayAtHotel() throws FileNotFoundException, IOException {
		WebDriver driver = DriverManager.getInstance().getDriver();
		CommonUtils utils = new CommonUtils();
		homePage = new HomePage();
		WebDriverWait wait = new WebDriverWait(driver, 120);
		String[] guestDetails = { utils.getProperty("GuestName"), utils.getProperty("GuestMobile"),
				utils.getProperty("TestBookingAccount") };

		driver.findElement(By.cssSelector(".guest-login__form-container input[name=email]")).sendKeys(guestDetails[2]);
		driver.findElement(By.cssSelector(".analytics-emaillogin.checkout__action.checkout__action--continue")).click();
		wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.cssSelector(".guest-details__name input"))));
		driver.findElement(By.cssSelector(".guest-details__name input")).sendKeys(guestDetails[0]);
		driver.findElement(By.cssSelector(".guest-details__mobile input")).sendKeys(guestDetails[1]);
		driver.findElement(By.cssSelector(".checkout__action.checkout__action--continue")).click();
		wait.until(ExpectedConditions.visibilityOf(driver.findElement(
				By.cssSelector(".analytics-pay.itinerary-view__action__item.btn.btn--secondary.btn--round"))));
		driver.findElement(By.cssSelector(".analytics-pay.itinerary-view__action__item.btn.btn--secondary.btn--round"))
				.click();

		homePage.waitForLoaderToDisappear();
		System.out.println("Booking hotel with details: " + "Guest Name : " + guestDetails[0] + " Guest Mobile : "
				+ guestDetails[1] + " Guest Email : " + guestDetails[2]);
		Assert.assertFalse(checkIfItineraryPageHasError(), "Error displayed");
		return guestDetails;
	}

	public void bookWithPayAtHotel() {
		WebDriver driver = DriverManager.getInstance().getDriver();
		homePage = new HomePage();
		homePage.waitForLoaderToDisappear();
		WebDriverWait wait = new WebDriverWait(driver, 60);

		try {
			Thread.sleep(5000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		if (driver.findElements(By.cssSelector(".checkout__action.checkout__action--continue")).size() > 0) {
			driver.findElement(By.cssSelector(".checkout__action.checkout__action--continue")).click();
		}
		wait.until(ExpectedConditions.visibilityOf(driver.findElement(
				By.cssSelector(".analytics-pay.itinerary-view__action__item.btn.btn--secondary.btn--round"))));
		driver.findElement(By.cssSelector(".analytics-pay.itinerary-view__action__item.btn.btn--secondary.btn--round"))
				.click();

		Assert.assertFalse(checkIfItineraryPageHasError(), "Error displayed");
	}

	public void enterGuestDetailsAtItineraryPage(String strGuestName, String strGuestMobile, String strGuestEmail) {
		WebDriver driver = DriverManager.getInstance().getDriver();
		WebDriverWait wait = new WebDriverWait(driver, 120);

		driver.findElement(By.cssSelector(".guest-login__form-container input[name=email]")).clear();
		driver.findElement(By.cssSelector(".guest-login__form-container input[name=email]")).sendKeys(strGuestEmail);
		driver.findElement(By.cssSelector(".analytics-emaillogin.checkout__action.checkout__action--continue")).click();
		wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.cssSelector(".guest-details__name input"))));
		driver.findElement(By.cssSelector(".guest-details__name input")).clear();
		driver.findElement(By.cssSelector(".guest-details__name input")).sendKeys(strGuestName);
		driver.findElement(By.cssSelector(".guest-details__mobile input")).clear();
		driver.findElement(By.cssSelector(".guest-details__mobile input")).sendKeys(strGuestMobile);
		driver.findElement(By.cssSelector(".checkout__action.checkout__action--continue")).click();
		wait.until(ExpectedConditions.visibilityOf(driver.findElement(
				By.cssSelector(".analytics-pay.itinerary-view__action__item.btn.btn--secondary.btn--round"))));
	}

	public String[] enterGuestDetailsAtItineraryPage() throws FileNotFoundException, IOException {
		WebDriver driver = DriverManager.getInstance().getDriver();
		CommonUtils utils = new CommonUtils();
		WebDriverWait wait = new WebDriverWait(driver, 120);

		String[] guestDetails = { utils.getProperty("GuestName"), utils.getProperty("GuestMobile"),
				utils.getProperty("TestBookingAccount") };

		driver.findElement(By.cssSelector(".guest-login__form-container input[name=email]")).clear();
		driver.findElement(By.cssSelector(".guest-login__form-container input[name=email]")).sendKeys(guestDetails[2]);
		driver.findElement(By.cssSelector(".analytics-emaillogin.checkout__action.checkout__action--continue")).click();
		wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.cssSelector(".guest-details__name input"))));
		driver.findElement(By.cssSelector(".guest-details__name input")).clear();
		driver.findElement(By.cssSelector(".guest-details__name input")).sendKeys(guestDetails[0]);
		driver.findElement(By.cssSelector(".guest-details__mobile input")).clear();
		driver.findElement(By.cssSelector(".guest-details__mobile input")).sendKeys(guestDetails[1]);
		driver.findElement(By.cssSelector(".checkout__action.checkout__action--continue")).click();
		wait.until(ExpectedConditions.visibilityOf(driver.findElement(
				By.cssSelector(".analytics-pay.itinerary-view__action__item.btn.btn--secondary.btn--round"))));

		return guestDetails;
	}

	public void enterSpecialRequest(String strSpecialRequest) {

	}

	public void loginAsTreeboMember(String strGuestEmail, String strGuestPassword) throws InterruptedException {
		WebDriver driver = DriverManager.getInstance().getDriver();

		driver.findElement(By.cssSelector("input[name='account']")).click();
		driver.findElement(By.cssSelector(".guest-login__form-container input[name=email]")).sendKeys(strGuestEmail);
		driver.findElement(By.cssSelector(".guest-login__form-container .guest-login__password input"))
				.sendKeys(strGuestPassword);
		driver.findElement(By.cssSelector(".analytics-emaillogin.checkout__action.checkout__action--continue")).click();

		homePage = new HomePage();
		homePage.waitForLoaderToDisappear();
		Thread.sleep(5000);
	}

	public String[] loginAsTreeboMember() throws FileNotFoundException, IOException {
		WebDriver driver = DriverManager.getInstance().getDriver();
		CommonUtils utils = new CommonUtils();
		String[] guestDetails = { utils.getProperty("TestBookingAccount"), utils.getProperty("TestLoginPassword") };

		driver.findElement(By.cssSelector("input[name='account']")).click();
		driver.findElement(By.cssSelector(".guest-login__form-container input[name=email]")).sendKeys(guestDetails[0]);
		driver.findElement(By.cssSelector(".guest-login__form-container .guest-login__password input"))
				.sendKeys(guestDetails[1]);
		driver.findElement(By.cssSelector(".analytics-emaillogin.checkout__action.checkout__action--continue")).click();

		homePage = new HomePage();
		homePage.waitForLoaderToDisappear();
		return guestDetails;
	}

	public void loginAsFBMember(String strFBEmail, String strFBPassword) throws InterruptedException {
		WebDriver driver = DriverManager.getInstance().getDriver();
		browserHelper = new BrowserHelper();

		driver.findElement(guestLoginFBButton).click();
		String parentWindow = browserHelper.switchToNewWindow();
		driver.findElement(fbEmailField).sendKeys(strFBEmail);
		driver.findElement(fbPasswordField).sendKeys(strFBPassword);
		driver.findElement(fbLoginButton).click();
		Thread.sleep(30000);
		driver.switchTo().window(parentWindow);
		homePage = new HomePage();
		homePage.waitForLoaderToDisappear();
	}

	public String[] loginAsFBMember() throws FileNotFoundException, IOException, InterruptedException {
		WebDriver driver = DriverManager.getInstance().getDriver();
		CommonUtils utils = new CommonUtils();
		homePage = new HomePage();
		browserHelper = new BrowserHelper();
		String[] guestFBDetails = { utils.getProperty("FBLogin"), utils.getProperty("GmailFbPassword") };

		driver.findElement(guestLoginFBButton).click();
		browserHelper.waitTime(10000);
		String parentWindow = browserHelper.switchToNewWindow();
		driver.findElement(fbEmailField).sendKeys(guestFBDetails[0]);
		driver.findElement(fbPasswordField).sendKeys(guestFBDetails[1]);
		driver.findElement(fbLoginButton).click();
		Thread.sleep(30000);
		driver.switchTo().window(parentWindow);
		homePage.waitForLoaderToDisappear();
		return guestFBDetails;
	}

	public void loginAsGoogleMember(String strGmail, String strGmailPassword) throws InterruptedException {
		WebDriver driver = DriverManager.getInstance().getDriver();
		browserHelper = new BrowserHelper();
		homePage = new HomePage();
		driver.findElement(guestLoginGoogleButton).click();
		String parentWindow = browserHelper.switchToNewWindow();
		driver.findElement(gmailEmailField).sendKeys(strGmail);
		driver.findElement(gmailNextLink).click();
		WebDriverWait wait = new WebDriverWait(driver, 120);
		wait.until(ExpectedConditions.presenceOfElementLocated(gmailPasswordField));
		driver.findElement(gmailPasswordField).sendKeys(strGmailPassword);
		driver.findElement(gmailSignInButton).click();
		Thread.sleep(30000);
		driver.switchTo().window(parentWindow);
		homePage.waitForLoaderToDisappear();
	}

	public String[] loginAsGoogleMember() throws FileNotFoundException, IOException, InterruptedException {
		WebDriver driver = DriverManager.getInstance().getDriver();
		browserHelper = new BrowserHelper();
		homePage = new HomePage();
		CommonUtils utils = new CommonUtils();
		String[] guestGmailDetails = { utils.getProperty("TestBookingAccount"), utils.getProperty("GmailFbPassword") };

		driver.findElement(guestLoginGoogleButton).click();
		String parentWindow = browserHelper.switchToNewWindow();
		driver.findElement(gmailEmailField).sendKeys(guestGmailDetails[0]);
		driver.findElement(gmailNextLink).click();
		WebDriverWait wait = new WebDriverWait(driver, 120);
		wait.until(ExpectedConditions.presenceOfElementLocated(gmailPasswordField));
		driver.findElement(gmailPasswordField).sendKeys(guestGmailDetails[1]);
		driver.findElement(gmailSignInButton).click();
		Thread.sleep(90000);
		driver.switchTo().window(parentWindow);
		homePage.waitForLoaderToDisappear();
		return guestGmailDetails;
	}

	public void signupInItineraryPage(String name, String mobile, String email, String password)
			throws InterruptedException {
		WebDriver driver = DriverManager.getInstance().getDriver();
		homePage = new HomePage();
		driver.findElement(newUserRegisterLink).click();
		homePage.waitForLoaderToDisappear();
		homePage.enterSignUpForm(name, mobile, email, password);
		Thread.sleep(30000);
		homePage.waitForLoaderToDisappear();
	}

	public String[] signupInItineraryPage() {
		WebDriver driver = DriverManager.getInstance().getDriver();
		homePage = new HomePage();
		driver.findElement(newUserRegisterLink).click();
		homePage.waitForLoaderToDisappear();
		return homePage.enterSignUpForm();
	}

	public void verifyGuestLoggedIn() {

	}

	public boolean isPayUPageDisplayed() {
		WebDriver driver = DriverManager.getInstance().getDriver();
		WebDriverWait wait = new WebDriverWait(driver, 120);
		wait.until(ExpectedConditions.presenceOfElementLocated(ccPayButton));
		return driver.findElement(ccPayButton).isDisplayed();
	}

	public boolean isGoBackLinkTreeboDisplayed() {
		WebDriver driver = DriverManager.getInstance().getDriver();
		return driver.findElement(By.xpath("//a[text()='www.treebohotels.com']")).isDisplayed();
	}

	public void clickGoBackToTreeboOnPayU() {
		WebDriver driver = DriverManager.getInstance().getDriver();
		driver.findElement(By.xpath("//a[text()='www.treebohotels.com']")).click();
	}

	public void verifyPayUPaymentFailed() {
		WebDriver driver = DriverManager.getInstance().getDriver();
		WebDriverWait wait = new WebDriverWait(driver, 120);
		wait.until(ExpectedConditions.presenceOfElementLocated(payNowButton));
		Assert.assertEquals(driver.findElement(By.cssSelector(".alert__msg")).getText(), "PayU payment failed",
				"Error not displayed");
	}

	public void clickOnPayNowButton() {
		WebDriver driver = DriverManager.getInstance().getDriver();
		if (driver.findElements(By.cssSelector(".checkout__action.checkout__action--continue")).size() > 0) {
			driver.findElement(By.cssSelector(".checkout__action.checkout__action--continue")).click();
		}

		driver.findElement(By.cssSelector(".analytics-pay.itinerary-view__action__item.checkout__action--continue"))
				.click();

	}

	public void bookWithPayNow() {
		WebDriver driver = DriverManager.getInstance().getDriver();
		homePage = new HomePage();
		String strCCNumber = "5123456789012346";
		String strName = "Test";
		String strCVVNumber = "123";

		WebDriverWait wait = new WebDriverWait(driver, 180);
		driver.findElement(payNowButton).click();
		wait.until(ExpectedConditions.presenceOfElementLocated(ccNumber));
		for (char ch : strCCNumber.toCharArray()) {
			driver.findElement(ccNumber).sendKeys(String.valueOf(ch));
		}
		driver.findElement(ccName).sendKeys(strName);
		driver.findElement(cvvNumber).sendKeys(strCVVNumber);
		driver.findElement(monthSelect).click();
		driver.findElement(mayMonth).click();
		driver.findElement(YearSelect).click();
		driver.findElement(yearOption).click();
		driver.findElement(ccPayButton).click();
		homePage.waitForLoaderToDisappear();
		wait.until(ExpectedConditions
				.presenceOfElementLocated(By.xpath("//div[@class='confirmation-page__header']/div[@class='cp-head']")));
	}

	public double[] getHotelPriceInfoInItineraryPage() {
		WebDriver driver = DriverManager.getInstance().getDriver();
		double[] hotelPriceInfo = new double[4];
		hotelPriceInfo[0] = Double.parseDouble(driver.findElement(roomPrice).getText());
		hotelPriceInfo[1] = Double.parseDouble(driver.findElement(totalTax).getText());
		try {
			hotelPriceInfo[2] = Double.parseDouble(driver.findElement(discountValue).getText());
		} catch (NumberFormatException e) {
			hotelPriceInfo[2] = Double.parseDouble("0");
		}

		hotelPriceInfo[3] = Double.parseDouble(driver.findElement(grandTotal).getText());
		System.out.println(" Itinerary Page Price :" + "Room Price : " + hotelPriceInfo[0] + " Total Tax : "
				+ hotelPriceInfo[1] + " Total Discount : " + hotelPriceInfo[2] + " Grand Total :" + hotelPriceInfo[3]);
		return hotelPriceInfo;
	}

	public void applyCouponInItineraryPage(String coupon) {
		WebDriver driver = DriverManager.getInstance().getDriver();
		System.out.println("Applying coupon in Itinerary page :" + coupon);
		driver.findElement(applyCouponCodeLink).click();
		WebDriverWait wait = new WebDriverWait(driver, 120);
		wait.until(ExpectedConditions.presenceOfElementLocated(inputCoupon));
		driver.findElement(inputCoupon).sendKeys(coupon);
		driver.findElement(applyCouponButton).click();
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void removeCouponInItineraryPage() {
		WebDriver driver = DriverManager.getInstance().getDriver();
		browserHelper = new BrowserHelper();
		System.out.println("removing coupon applied");
		WebDriverWait wait = new WebDriverWait(driver, 120);
		driver.findElement(removeCoupon).click();
		browserHelper.waitTime(3000);
	}

	public int getCouponDiscountDisplayed() {
		WebDriver driver = DriverManager.getInstance().getDriver();
		// wait for element
		WebDriverWait wait = new WebDriverWait(driver, 120);
		wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(discountAppliedValue));
		int discountApplied = (int) Math.round(Double.parseDouble(driver.findElement(discountAppliedValue).getText()));
		System.out.println("Discount coupon applied :" + discountApplied);
		return discountApplied;
	}

	public void verifyInvalidCouponError() {
		WebDriver driver = DriverManager.getInstance().getDriver();
		System.out.println("Verifying invalid error");
		CommonUtils utils = new CommonUtils();
		String errorText = utils.getProperty("InvalidCouponError");
		Assert.assertTrue(driver.findElement(couponInvalidError).isDisplayed(),
				"Invalid coupon error is not displayed");
		Assert.assertEquals(driver.findElement(couponInvalidError).getText(), errorText, "Error text not matching");
	}

	public String getGuestDetailsInItineraryPage() {
		WebDriver driver = DriverManager.getInstance().getDriver();
		return driver.findElement(guestDetailsInItineraryPage).getText();
	}

	public String getRoomInfoInItineraryPage() {
		WebDriver driver = DriverManager.getInstance().getDriver();
		return driver.findElement(roomInfoInItineraryPage).getText();
	}

	// verify title
	public boolean isEmailAddressTitleDisplayed() {
		WebDriver driver = DriverManager.getInstance().getDriver();
		return driver.findElement(By.xpath("//h2[@class='checkout__section-title checkout__section-title--active']"))
				.isDisplayed();
	}

	// verify email box
	public boolean doesEmailBoxDisplayed() {
		WebDriver driver = DriverManager.getInstance().getDriver();
		return driver.findElement(By.xpath("//div[@class='float-labels guest-login__name']")).isDisplayed();
	}

	// verify email place holder
	public boolean doesEmailPlaceHolderDisplayed() {
		WebDriver driver = DriverManager.getInstance().getDriver();
		return driver.findElement(By.xpath("//*[@id='checkout']//input[@placeholder='Email']")).isDisplayed();
	}

	public void enterEmailAddress(String emailAddress) {
		WebDriver driver = DriverManager.getInstance().getDriver();
		WebDriverWait wait = new WebDriverWait(driver, 120);
		wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(email));
		driver.findElement(email).sendKeys(emailAddress.trim());
	}

	// check box display
	public boolean doesCheckBoxDisplayed() {
		WebDriver driver = DriverManager.getInstance().getDriver();
		return driver.findElement(checkBox).isDisplayed();
	}

	public void clickOnCheckBox() {
		WebDriver driver = DriverManager.getInstance().getDriver();
		driver.findElement(checkBox).click();
	}

	// verified pass box
	public boolean doesPasswordBoxDisplayed() {
		WebDriver driver = DriverManager.getInstance().getDriver();
		return driver.findElement(By.xpath("//div[@class='float-labels guest-login__password']")).isDisplayed();
	}

	public void enterPassword(String pass) {
		WebDriver driver = DriverManager.getInstance().getDriver();
		driver.findElement(By.xpath("//*[@id='checkout']//input[@name='password']")).sendKeys(pass.trim());
	}

	// verify continue button
	public boolean doesContinueButtonDisplayed() {
		WebDriver driver = DriverManager.getInstance().getDriver();
		return driver.findElement(By.xpath("//*[@id='checkout']//button[@type='submit']")).isDisplayed();

	}

	public void clickOnContinueButton() {
		WebDriver driver = DriverManager.getInstance().getDriver();
		WebDriverWait wait = new WebDriverWait(driver, 120);
		wait.until(ExpectedConditions
				.presenceOfAllElementsLocatedBy(By.xpath("//*[@id='checkout']//button[@type='submit']")));
		driver.findElement(By.xpath("//*[@id='checkout']//button[@type='submit']")).click();
	}

	public boolean doesInvalidCredentialsMessageDisplayed() {
		WebDriver driver = DriverManager.getInstance().getDriver();
		WebDriverWait wait = new WebDriverWait(driver, 120);
		wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(
				By.cssSelector(".travller-login-error.login__error.alert.alert--error")));
		return driver.findElement(By.cssSelector(".travller-login-error.login__error.alert.alert--error"))
				.isDisplayed();
	}

	public boolean doesEnterDetailsOfPrimaryTravellerTitleDisplayed() {
		WebDriver driver = DriverManager.getInstance().getDriver();
		return driver.findElement(By.cssSelector(".checkout__section-title.checkout__section-title--active"))
				.isDisplayed();
	}

	public boolean doesNameLabelDisplayed() {
		WebDriver driver = DriverManager.getInstance().getDriver();
		WebDriverWait wait = new WebDriverWait(driver, 120);
		wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By
				.xpath("//*[@id='guestWidget']//div[@class='float-labels guest-details__name']/label[text()='Name']")));

		return driver
				.findElement(By
						.xpath("//*[@id='guestWidget']//div[@class='float-labels guest-details__name']/label[@class='float-labels__label']"))
				.isDisplayed();
	}

	public void enterName(String name) {
		WebDriver driver = DriverManager.getInstance().getDriver();
		driver.findElement(By.xpath("//*[@id='guestWidget']//input[@placeholder='Name']")).sendKeys(name.trim());
	}

	public boolean doesMobileNumberLableDisplayed() {
		WebDriver driver = DriverManager.getInstance().getDriver();
		return driver.findElement(By.xpath("//div[@class='float-labels guest-details__mobile']/label")).isDisplayed();
	}

	public void enterMobileNumber(String number) {
		WebDriver driver = DriverManager.getInstance().getDriver();
		driver.findElement(By.xpath("//div[@class='float-labels guest-details__mobile']/input[@ type='number']"))
				.sendKeys(number.trim());
	}

	public boolean doesEmailIdLabelDisplayed() {
		WebDriver driver = DriverManager.getInstance().getDriver();
		WebDriverWait wait = new WebDriverWait(driver, 120);
		wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(
				By.xpath("//div[@class='float-labels guest-details__email']/label[@class='float-labels__label']")));
		return driver
				.findElement(By
						.xpath("//div[@class='float-labels guest-details__email']/label[@class='float-labels__label']"))
				.isDisplayed();
	}

	public void enteremailId(String emailAddress) {
		WebDriver driver = DriverManager.getInstance().getDriver();
		driver.findElement(By.xpath("//div[@class='float-labels guest-details__email']/input"))
				.sendKeys(emailAddress.trim());
	}

	public boolean doesMentionSpecialRequestDisplayed() {
		WebDriver driver = DriverManager.getInstance().getDriver();
		return driver.findElement(By.xpath("//h2[@class='checkout__section-title checkout__section-title--active']"))
				.isDisplayed();
	}

	public boolean doesCheckInBoxDisplayed() {
		WebDriver driver = DriverManager.getInstance().getDriver();
		return driver.findElement(By.xpath("//select[@name='check_in_time']")).isDisplayed();
	}

	public boolean doesCheckOutBoxDisplayed() {
		WebDriver driver = DriverManager.getInstance().getDriver();
		return driver.findElement(By.xpath("//select[@name='check_out_time']")).isDisplayed();
	}

	public boolean doesCommentBoxDisplayed() {
		WebDriver driver = DriverManager.getInstance().getDriver();
		return driver.findElement(By.xpath(".//*[@id='checkout']//textarea[@ name='message']")).isDisplayed();
	}

	public boolean doesPayAtHotelLinkDisplayed() {
		WebDriver driver = DriverManager.getInstance().getDriver();
		return driver.findElement(By.cssSelector(".analytics-pay.btn.btn--secondary.btn--round")).isDisplayed();
	}

	public boolean doesRegistrationNameLabelDisplayed() {
		WebDriver driver = DriverManager.getInstance().getDriver();
		return driver
				.findElement(By.xpath("//div[@class='float-labels guest-details__name']/input[@placeholder='Name']"))
				.isDisplayed();
	}

	public boolean doesRegistrationMobileNumberLabelDisplayed() {
		WebDriver driver = DriverManager.getInstance().getDriver();
		return driver.findElement(By.xpath("//input[@placeholder='Mobile number']")).isDisplayed();
	}

	public void clickOnPayNow() {
		WebDriver driver = DriverManager.getInstance().getDriver();
		driver.findElement(By
				.xpath("//button[@class='analytics-pay itinerary-view__action__item checkout__action--continue btn btn--round']"))
				.click();
	}

	public boolean doesCreditDebitCardDisplayed() {
		WebDriver driver = DriverManager.getInstance().getDriver();
		return driver.findElement(By.xpath("#react-tabs-0")).isDisplayed();
	}

	public void clickOnCreditDebitCardOption() {
		WebDriver driver = DriverManager.getInstance().getDriver();
		driver.findElement(By.xpath("//div[text()='Credit/Debit Card']")).click();
	}

	public boolean doesCardNumberLabelDispalyed() {
		WebDriver driver = DriverManager.getInstance().getDriver();
		WebDriverWait wait = new WebDriverWait(driver, 120);
		wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(
				By.xpath("//div[1]/label[@class='float-labels__label']/span[text()='Card Number']")));
		return driver.findElement(By.xpath("//div[1]/label[@class='float-labels__label']/span[text()='Card Number']"))
				.isDisplayed();
	}

	public boolean doesCardHolderNameDisplayed() {
		WebDriver driver = DriverManager.getInstance().getDriver();
		return driver.findElement(By.xpath("//div[2]/label[@class='float-labels__label']/span[@contain]"))
				.isDisplayed();
	}

	public boolean doesExpiryDateLabelDisplayed() {
		WebDriver driver = DriverManager.getInstance().getDriver();
		WebDriverWait wait = new WebDriverWait(driver, 120);
		wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(
				By.xpath("//label[@class='float-labels__label card-pay__label']/span[text()='Expiry Date']")));
		return driver
				.findElement(
						By.xpath("//label[@class='float-labels__label card-pay__label']/span[text()='Expiry Date']"))
				.isDisplayed();
	}

	public boolean doesMonthBoxDisplayed() {
		WebDriver driver = DriverManager.getInstance().getDriver();
		return driver.findElement(By.xpath("//select[@name='month']")).isDisplayed();
	}

	public boolean doesYearBoxDisplayed() {
		WebDriver driver = DriverManager.getInstance().getDriver();
		return driver.findElement(By.xpath("//select[@name='year']")).isDisplayed();
	}

	public boolean doesCVVLabelDisplayed() {
		WebDriver driver = DriverManager.getInstance().getDriver();
		return driver.findElement(By.xpath("//div[4]/label[@class='float-labels__label']/span[text()='CVV']"))
				.isDisplayed();
	}

	public boolean doesCVVDetailsImageDisplayed() {
		WebDriver driver = DriverManager.getInstance().getDriver();
		return driver.findElement(By.xpath("//div[@class='card-pay__cvv-details']/span[@class='card-pay__cvv-image']"))
				.isDisplayed();
	}

	public boolean doesPayNowButtonForFinalTranscationDisplayed() {
		WebDriver driver = DriverManager.getInstance().getDriver();
		return driver
				.findElement(By.xpath("//button[@class='checkout__action checkout__action--continue btn btn--round']"))
				.isDisplayed();
	}

	public void clickPayNowForFinalTranscation() {
		WebDriver driver = DriverManager.getInstance().getDriver();
		driver.findElement(By.xpath("//button[@class='checkout__action checkout__action--continue btn btn--round']"))
				.click();
	}

	public boolean doesMobikwikLinkDisplayed() {
		WebDriver driver = DriverManager.getInstance().getDriver();
		return driver.findElement(By.xpath("//*[@id='react-tabs-2']/div[text()='Mobikwik Wallet']")).isDisplayed();
	}

	public void clickOnMobikwik() {
		WebDriver driver = DriverManager.getInstance().getDriver();
		WebDriverWait wait = new WebDriverWait(driver, 120);
		wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.xpath("//div[text()='Mobikwik Wallet']")));
		driver.findElement(By.xpath("//div[text()='Mobikwik Wallet']")).click();
	}

	public boolean doesMobikwikImageDisplayed() {
		WebDriver driver = DriverManager.getInstance().getDriver();
		return driver.findElement(By.cssSelector(".wallet__input-container"))
				.isDisplayed();
	}

	public boolean checkProcessingPaymentForFinalTransactionMobikwik() {
		WebDriver driver = DriverManager.getInstance().getDriver();
		browserHelper = new BrowserHelper();
		driver.findElement(By.xpath("//button[@class='checkout__action checkout__action--continue btn btn--round']"))
				.click();
		String parentWindow = browserHelper.switchToNewWindow();
		WebDriverWait wait = new WebDriverWait(driver, 360);
		wait.until(
				ExpectedConditions.presenceOfAllElementsLocatedBy(By.xpath("//h2[text()='Login to MobiKwik Wallet']")));
		return driver.findElement(By.xpath("//h2[text()='Login to MobiKwik Wallet']")).isDisplayed();
	}

	public void clickOnNetBankingOption() {
		WebDriver driver = DriverManager.getInstance().getDriver();
		WebDriverWait wait = new WebDriverWait(driver, 120);
		wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.xpath("//div[text()='Net Banking']")));
		driver.findElement(By.xpath("//div[text()='Net Banking']")).click();
	}

	public boolean doesOtherBankBoxDisplayed() {
		WebDriver driver = DriverManager.getInstance().getDriver();
		return driver.findElement(By.xpath("//select[@class='netbank__list-select']")).isDisplayed();
	}

	public boolean doesSBIBankIconDisplayed() {
		WebDriver driver = DriverManager.getInstance().getDriver();
		// WebDriverWait wait = new WebDriverWait(driver,120);
		// wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.xpath("//span[text()='SBI']")));
		return driver.findElement(By.xpath("//span[text()='SBI']")).isDisplayed();
	}

	public boolean doesHDFCBankIconDisplayed() {
		WebDriver driver = DriverManager.getInstance().getDriver();
		return driver.findElement(By.xpath("//span[text()='HDFC']")).isDisplayed();
	}

	public boolean doesICICIBankIconDisplayed() {
		WebDriver driver = DriverManager.getInstance().getDriver();
		return driver.findElement(By.xpath("//span[@class='netbank__display netbank__display--icici']")).isDisplayed();
	}

	public boolean doesAxisBankIconDisplayed() {
		WebDriver driver = DriverManager.getInstance().getDriver();
		return driver.findElement(By.xpath("//span[text()='Axis']")).isDisplayed();
	}

	public boolean doesKotakBankIconDisplayed() {
		WebDriver driver = DriverManager.getInstance().getDriver();
		return driver.findElement(By.xpath("//span[@class='netbank__display netbank__display--kotak']")).isDisplayed();
	}

	public boolean doesYesBankIconDisplayed() {
		WebDriver driver = DriverManager.getInstance().getDriver();
		return driver.findElement(By.xpath("//span[text()='Yes']")).isDisplayed();
	}

	public void clickOnSBI() {
		WebDriver driver = DriverManager.getInstance().getDriver();
		driver.findElement(By.xpath("//span[@class='netbank__display netbank__display--sbi']")).click();
	}

	public void clickOnPayNowForFinalTransaction() {
		WebDriver driver = DriverManager.getInstance().getDriver();
		driver.findElement(By.xpath("//button[@class='checkout__action checkout__action--continue btn btn--round']"))
				.click();

	}

	public boolean checkProcessingPaymentForFinalTransactionNetBanking() {
		WebDriver driver = DriverManager.getInstance().getDriver();
		browserHelper = new BrowserHelper();
		driver.findElement(By.xpath("//button[@class='checkout__action checkout__action--continue btn btn--round']"))
				.click();
		String parentWindow = browserHelper.switchToNewWindow();
		WebDriverWait wait = new WebDriverWait(driver, 360);
		wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.xpath("//div[@id='loginpage']")));
		return driver.findElement(By.xpath("//div[@id='loginpage']")).isDisplayed();
	}

	public void clickOnPriceBreakUpDetails() {
		WebDriver driver = DriverManager.getInstance().getDriver();
		driver.findElement(By.xpath(".//*[@id='price-detail-itinerary']")).click();
	}

	public float extractRoomPrice() {
		WebDriver driver = DriverManager.getInstance().getDriver();
		WebElement element = driver.findElement(By.xpath(".//*[@id='roomPrice']"));
		String roomPriceString = element.getText().trim();
		float roomPrice = Float.parseFloat(roomPriceString);

		return roomPrice;
	}

	public float extractTaxPrice() {
		WebDriver driver = DriverManager.getInstance().getDriver();
		WebElement element = driver.findElement(By.xpath(".//*[@id='totalTax']"));
		String totalTaxString = element.getText().trim();
		float totalTax = Float.parseFloat(totalTaxString);

		return totalTax;
	}

	public int extractTotalPrice() {
		WebDriver driver = DriverManager.getInstance().getDriver();
		WebElement element = driver.findElement(By.xpath(".//*[@id='grandTotal']"));
		String totalPriceString = element.getText().trim();
		float totalPricefl = Float.parseFloat(totalPriceString);
		int totalPrice = (int) totalPricefl;
		return totalPrice;
	}

	public boolean doesPayNowButtonDisplayed() {
		WebDriver driver = DriverManager.getInstance().getDriver();
		if (driver.findElements(By.cssSelector(".checkout__action.checkout__action--continue")).size() > 0) {
			return driver.findElement(By.cssSelector(".checkout__action.checkout__action--continue")).isDisplayed();
		}

		try {
			return driver.findElement(payNowButton).isDisplayed();
		} catch (Exception e) {
			return driver
					.findElement(
							By.cssSelector(".analytics-pay.itinerary-view__action__item.checkout__action--continue"))
					.isDisplayed();
		}
	}

	// method for Pay At Hotel
	public boolean doesOTPPopupDisplayed() {
		WebDriver driver = DriverManager.getInstance().getDriver();
		if(driver.findElements(By.cssSelector(".otp__container")).size()>0)
			return true;
		return false;
		
	}

	//verify otp Title
	public boolean doesOTPPopupTitleDisplayed(){
		WebDriver driver = DriverManager.getInstance().getDriver();
		return driver.findElement(By.xpath("//h2[text()='Verify your Mobile Number']")).isDisplayed();
	}
	//check resend sms clickable
	public boolean doesClickableResendSms(){
		WebDriver driver = DriverManager.getInstance().getDriver();
		return driver.findElement(By.xpath("//a[text()='Resend Sms']")).isEnabled();
	}
	//check change number clickable
	public boolean doesClickableChangeNumber(){
		WebDriver driver = DriverManager.getInstance().getDriver();
		return driver.findElement(By.cssSelector(".otp__edit")).isEnabled();
	}
	//extract mobile number from otp pop up
	public String extractMobileNumber(){
		WebDriver driver = DriverManager.getInstance().getDriver();
		String mobile = driver.findElement(By.xpath("//div[@class='otp__subtitle']//span[2]")).getText().trim();
		return mobile;
	}
	
	public boolean doesDoneButtonOnOTPPopupDisplayed() {
		WebDriver driver = DriverManager.getInstance().getDriver();
		return driver.findElement(By.cssSelector(".otp__verify-btn")).isDisplayed();
	}

	public void clickOnDoneButtonForOTPVerification() {
		WebDriver driver = DriverManager.getInstance().getDriver();
		driver.findElement(By.cssSelector(".otp__verify-btn")).click();
	}
   
	//change number
	public void clickOnChangeNumber() {
		WebDriver driver = DriverManager.getInstance().getDriver();
		WebDriverWait wait = new WebDriverWait(driver,60);
		wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.cssSelector(".otp__edit")));
		driver.findElement(By.cssSelector(".otp__edit")).click();
	}
	//check change mobile number pop title 
	public boolean doesChangeMobileNumberTitleDisplayed(){
		WebDriver driver = DriverManager.getInstance().getDriver();
		return driver.findElements(By.xpath("//h2[text()='Enter Mobile Number']")).size()>0;
	}
   //update number
	public void changeMobileNumber(String mobileNumber){
		WebDriver driver= DriverManager.getInstance().getDriver();
		driver.findElement(By.cssSelector(".float-labels__input.otp__phone")).clear();
		driver.findElement(By.cssSelector(".float-labels__input.otp__phone")).sendKeys(mobileNumber.trim());
	}
	//click On Done button to get OTP
	public void clickOnDone(){
		WebDriver driver= DriverManager.getInstance().getDriver();
		driver.findElement(By.cssSelector(".otp__verify-btn")).click();
	}
	public void enterOTP(String otp) {
		WebDriver driver = DriverManager.getInstance().getDriver();
		driver.findElement(By.xpath("//input[@type='number']")).clear();
        driver.findElement(By.xpath("//input[@type='number']")).sendKeys(otp.trim());
	}

	public boolean doesShowErrorMessage(){
		WebDriver driver = DriverManager.getInstance().getDriver();
		return driver.findElements(By.cssSelector(".otp__error-msg")).size()>0;
		
	}
	
	public boolean doesEqualBothString(String str1, String str2){
		if((str1.trim()).equals(str2.trim()))
			return true;
		return false;
	}
	
	//check for Pay Now option on OTP Popup
	public boolean doesPayNowOptionDisplayedOnOTPPopup(){
		WebDriver driver= DriverManager.getInstance().getDriver();
		return driver.findElement(By.xpath("//button[text()='SKIP, I WILL PAY NOW']")).isDisplayed();
	}
	//go to pay now option on otp popup
	public void clickOnSkippIWillPayNow(){
		WebDriver driver = DriverManager.getInstance().getDriver();
		driver.findElement(By.xpath("//button[text()='SKIP, I WILL PAY NOW']")).click();
	}
	//verify transaction page presence
	public boolean doesLogoOnTransactionPageDisplayed(){
		WebDriver driver = DriverManager.getInstance().getDriver();
		return driver.findElements(By.xpath("//img[@class='logo']")).size()>0;
	}
	}
