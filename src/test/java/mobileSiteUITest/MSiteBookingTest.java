package mobileSiteUITest;

import java.text.ParseException;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import base.BrowserHelper;
import base.TestBaseSetUp;
import mobileSiteUIPageObjects.ConfirmationScreen;
import mobileSiteUIPageObjects.HomeScreen;
import mobileSiteUIPageObjects.HotelDetailsScreen;
import mobileSiteUIPageObjects.HotelSearchResultsScreen;
import mobileSiteUIPageObjects.ItineraryScreen;
import utils.CommonUtils;

@Test(singleThreaded=true)
public class MSiteBookingTest extends TestBaseSetUp{
	private HomeScreen homeScreen;
	private HotelSearchResultsScreen hotelSearchResultScreen;
	private HotelDetailsScreen hotelDetailsScreen;
	private ItineraryScreen itineraryScreen;
	private ConfirmationScreen confirmationScreen;
	private BrowserHelper browserHelper;
	
	/**
	 * This test checks booking as Guest
	 * @throws ParseException 
	 */
	
	@Test(groups={"MSite" , "MSiteBooking"})
	public void testMobileBookingAsGuestThroughQuickBook() throws ParseException{
		homeScreen = new HomeScreen();
		hotelSearchResultScreen = new HotelSearchResultsScreen();
		hotelDetailsScreen = new HotelDetailsScreen();
		itineraryScreen = new ItineraryScreen(); 
		confirmationScreen = new ConfirmationScreen();
		browserHelper = new BrowserHelper();
		CommonUtils utils = new CommonUtils();
		SoftAssert verify = new SoftAssert();
		
		String location = utils.getProperty("city"); // "Bengaluru";
		String testHotel = utils.getProperty("DummyHotel"); // "Elmas";
		String testUrl = utils.getProperty("TestUrl");
		int hotelIndex = 0;
		int checkInFromCurrentDate = utils.getRandomNumber(30, 45);
		int checkOutFromCurrentDate = checkInFromCurrentDate + 1;
		String guestName = utils.getProperty("GuestName"); // "Test";
		String guestEmail = utils.getProperty("TestBookingAccount"); // "";
		String guestMobile = utils.getProperty("GuestMobile");// "9000000000";
		String[] checkInCheckOutDates = new String[2];
		
		// Check if home screen is displayed
		verify.assertTrue(homeScreen.isHomeScreenDisplayed(),"Home Screen is not displayed");
		//Do Search
		checkInCheckOutDates = homeScreen.doSearch(location, checkInFromCurrentDate, checkOutFromCurrentDate);
		// check if search results displayed
		verify.assertTrue(hotelSearchResultScreen.isHotelSearchScreenDisplayed(),"search result screen not displayed");
		//Select a random hotel or test hotel
		if (browserHelper.getCurrentUrl().contains(testUrl)) {
			hotelIndex = hotelSearchResultScreen.getAllAvailableHotelNames().indexOf(testHotel);
		}else{
			hotelIndex = hotelSearchResultScreen.getRandomIndexOfAvailableHotel();
		}
		
		String hotelName = hotelSearchResultScreen.getHotelNameByIndex(hotelIndex);
		int hotelRoomRate = hotelSearchResultScreen.getRoomRateByIndex(hotelIndex);
		System.out.println("Hotel Name: " + hotelName + " Hotel room rate : " + hotelRoomRate );

		
		//Click quick book 
		hotelSearchResultScreen.clickQuickBookByIndex(hotelIndex);
		//Check If Itinerary screen
		verify.assertTrue(itineraryScreen.isItineraryScreenDisplayed());
		// click on next
		itineraryScreen.clickOnNextStepOne();
		itineraryScreen.continueAsGuest();
		itineraryScreen.enterGuestDetails(guestName, guestMobile, guestEmail);
		itineraryScreen.clickOnPayAtHotel();
		
		verify.assertTrue(confirmationScreen.isConfirmationScreenDisplayed(),"Confirmation screen not displayed");
		System.out.println("Booking id : " + confirmationScreen.getBookingId());
		verify.assertEquals(guestName, confirmationScreen.getGuestName());
		verify.assertEquals(hotelName, confirmationScreen.getHotelName());
		verify.assertEquals(1 , confirmationScreen.getGuestCount());
		verify.assertEquals(1 , confirmationScreen.getRoomCount());
		verify.assertEquals(utils.formatDate(checkInCheckOutDates[0]).toUpperCase(),
				confirmationScreen.getCheckInDate().toUpperCase());
		verify.assertEquals(utils.formatDate(checkInCheckOutDates[1]).toUpperCase(),
				confirmationScreen.getCheckOutDate().toUpperCase());
		verify.assertTrue(confirmationScreen.isPayAtHotelImageDisplayed(),"Pay At Hotel image not displayed");
		verify.assertAll();
	}
	
	/**
	 * This test checks booking as Treebo Member Logged in at home Screen 
	 * through HD Screen
	 * @throws ParseException 
	 */
	
	@Test(groups={"MSite" , "MSiteBooking"})
	public void testMobileBookingAsTreeboMemberLoggedInAtHomeScreenThroughHDScreen() throws ParseException{
		homeScreen = new HomeScreen();
		hotelSearchResultScreen = new HotelSearchResultsScreen();
		hotelDetailsScreen = new HotelDetailsScreen();
		itineraryScreen = new ItineraryScreen(); 
		confirmationScreen = new ConfirmationScreen();
		browserHelper = new BrowserHelper();
		CommonUtils utils = new CommonUtils();
		SoftAssert verify = new SoftAssert();
		
		String location = utils.getProperty("city"); // "Bengaluru";
		String testHotel = utils.getProperty("DummyHotel"); // "Elmas";
		String testUrl = utils.getProperty("TestUrl");
		int hotelIndex = 0;
		int checkInFromCurrentDate = utils.getRandomNumber(30, 45);
		int checkOutFromCurrentDate = checkInFromCurrentDate + 1;
		String guestName = utils.getProperty("GuestName"); // "Test";
		String guestEmail = utils.getProperty("TestBookingAccount"); // "";
		String guestMobile = utils.getProperty("GuestMobile");// "9000000000";
		String guestPassword = utils.getProperty("TestLoginPassword");
		String[] checkInCheckOutDates = new String[2];
		
		// Check if home screen is displayed
		verify.assertTrue(homeScreen.isHomeScreenDisplayed(),"Home Screen is not displayed");
		
		//Login
		homeScreen.openDrawer();
		homeScreen.clickOnLogin();
		homeScreen.loginAsTreeboMember(guestEmail, guestPassword);
		
		//Do Search
		checkInCheckOutDates = homeScreen.doSearch(location, checkInFromCurrentDate, checkOutFromCurrentDate);
		// check if search results displayed
		verify.assertTrue(hotelSearchResultScreen.isHotelSearchScreenDisplayed(),"search result screen not displayed");
		//Select a random hotel or test hotel
		if (browserHelper.getCurrentUrl().contains(testUrl)) {
			hotelIndex = hotelSearchResultScreen.getAllAvailableHotelNames().indexOf(testHotel);
		}else{
			hotelIndex = hotelSearchResultScreen.getRandomIndexOfAvailableHotel();
		}
		
		String hotelName = hotelSearchResultScreen.getHotelNameByIndex(hotelIndex);
		int hotelRoomRate = hotelSearchResultScreen.getRoomRateByIndex(hotelIndex);
		System.out.println("Hotel Name: " + hotelName + " Hotel room rate : " + hotelRoomRate );

		
		//Click hotel name 
		hotelSearchResultScreen.clickHotelByIndex(hotelIndex);
		
		// Hotel Details Screen
		verify.assertTrue(hotelDetailsScreen.isHDScreenDisplayed(),"HD Screen is not displayed");
		verify.assertEquals(hotelDetailsScreen.getHotelNameInHDScreen(), hotelName,"Hotel Name is wrong in HD");
		
		hotelDetailsScreen.clickOnBook();
		
		//Check If Itinerary screen
		verify.assertTrue(itineraryScreen.isItineraryScreenDisplayed());
		
		// click on next
		itineraryScreen.clickOnNextStepOne();
        verify.assertEquals(itineraryScreen.getGuestName(),guestName,"Prefilled name on itinerary is not correct");
        verify.assertEquals(itineraryScreen.getGuestMobile(),guestMobile,"Prefilled mobile on itinerary is not correct");
        verify.assertEquals(itineraryScreen.getGuestEmail(),guestEmail,"Prefilled email on itinerary is not correct");
		itineraryScreen.clickOnPayAtHotel();
		
		verify.assertTrue(confirmationScreen.isConfirmationScreenDisplayed(),"Confirmation screen not displayed");
		System.out.println("Booking id : " + confirmationScreen.getBookingId());
		verify.assertEquals(guestName, confirmationScreen.getGuestName());
		verify.assertEquals(hotelName, confirmationScreen.getHotelName());
		verify.assertEquals(1 , confirmationScreen.getGuestCount());
		verify.assertEquals(1 , confirmationScreen.getRoomCount());
		verify.assertEquals(utils.formatDate(checkInCheckOutDates[0]).toUpperCase(),
				confirmationScreen.getCheckInDate().toUpperCase());
		verify.assertEquals(utils.formatDate(checkInCheckOutDates[1]).toUpperCase(),
				confirmationScreen.getCheckOutDate().toUpperCase());
		verify.assertTrue(confirmationScreen.isPayAtHotelImageDisplayed(),"Pay At Hotel image not displayed");
		verify.assertAll();
	}
	
	/**
	 * This test checks booking as Treebo Member Logged in at itinerary screen
	 * @throws ParseException 
	 */
	
	@Test(groups={"MSite" , "MSiteBooking"})
	public void testMobileBookingAsTreeboMemberLoggedInAtItineraryScreen() throws ParseException{
		homeScreen = new HomeScreen();
		hotelSearchResultScreen = new HotelSearchResultsScreen();
		hotelDetailsScreen = new HotelDetailsScreen();
		itineraryScreen = new ItineraryScreen(); 
		confirmationScreen = new ConfirmationScreen();
		browserHelper = new BrowserHelper();
		CommonUtils utils = new CommonUtils();
		SoftAssert verify = new SoftAssert();
		
		String location = utils.getProperty("city"); // "Bengaluru";
		String testHotel = utils.getProperty("DummyHotel"); // "Elmas";
		String testUrl = utils.getProperty("TestUrl");
		int hotelIndex = 0;
		int checkInFromCurrentDate = utils.getRandomNumber(30, 45);
		int checkOutFromCurrentDate = checkInFromCurrentDate + 1;
		String guestName = utils.getProperty("GuestName"); // "Test";
		String guestEmail = utils.getProperty("TestBookingAccount"); // "";
		String guestMobile = utils.getProperty("GuestMobile");// "9000000000";
		String guestPassword = utils.getProperty("TestLoginPassword");
		String[] checkInCheckOutDates = new String[2];
		
		// Check if home screen is displayed
		verify.assertTrue(homeScreen.isHomeScreenDisplayed(),"Home Screen is not displayed");

		//Do Search
		checkInCheckOutDates = homeScreen.doSearch(location, checkInFromCurrentDate, checkOutFromCurrentDate);
		// check if search results displayed
		verify.assertTrue(hotelSearchResultScreen.isHotelSearchScreenDisplayed(),"search result screen not displayed");
		//Select a random hotel or test hotel
		if (browserHelper.getCurrentUrl().contains(testUrl)) {
			hotelIndex = hotelSearchResultScreen.getAllAvailableHotelNames().indexOf(testHotel);
		}else{
			hotelIndex = hotelSearchResultScreen.getRandomIndexOfAvailableHotel();
		}
		
		String hotelName = hotelSearchResultScreen.getHotelNameByIndex(hotelIndex);
		int hotelRoomRate = hotelSearchResultScreen.getRoomRateByIndex(hotelIndex);
		System.out.println("Hotel Name: " + hotelName + " Hotel room rate : " + hotelRoomRate );

		
		//Click hotel name 
		hotelSearchResultScreen.clickHotelByIndex(hotelIndex);
		
		// Hotel Details Screen
		verify.assertTrue(hotelDetailsScreen.isHDScreenDisplayed(),"HD Screen is not displayed");
		verify.assertEquals(hotelDetailsScreen.getHotelNameInHDScreen(), hotelName,"Hotel Name is wrong in HD");
		
		hotelDetailsScreen.clickOnBook();
		
		//Check If Itinerary screen
		verify.assertTrue(itineraryScreen.isItineraryScreenDisplayed());
		
		// click on next
		itineraryScreen.clickOnNextStepOne();
		
		itineraryScreen.loginAsTreeboMember(guestEmail, guestPassword);
        verify.assertEquals(itineraryScreen.getGuestName(),guestName,"Prefilled name on itinerary is not correct");
        verify.assertEquals(itineraryScreen.getGuestMobile(),guestMobile,"Prefilled mobile on itinerary is not correct");
        verify.assertEquals(itineraryScreen.getGuestEmail(),guestEmail,"Prefilled email on itinerary is not correct");
        
		itineraryScreen.clickOnPayAtHotel();
		
		verify.assertTrue(confirmationScreen.isConfirmationScreenDisplayed(),"Confirmation screen not displayed");
		System.out.println("Booking id : " + confirmationScreen.getBookingId());
		verify.assertEquals(guestName, confirmationScreen.getGuestName());
		verify.assertEquals(hotelName, confirmationScreen.getHotelName());
		verify.assertEquals(1 , confirmationScreen.getGuestCount());
		verify.assertEquals(1 , confirmationScreen.getRoomCount());
		verify.assertEquals(utils.formatDate(checkInCheckOutDates[0]).toUpperCase(),
				confirmationScreen.getCheckInDate().toUpperCase());
		verify.assertEquals(utils.formatDate(checkInCheckOutDates[1]).toUpperCase(),
				confirmationScreen.getCheckOutDate().toUpperCase());
		verify.assertTrue(confirmationScreen.isPayAtHotelImageDisplayed(),"Pay At Hotel image not displayed");
		verify.assertAll();
	}
	
	/**
	 * This test checks booking as Treebo Member for 1A+1C for one room
	 * @throws ParseException 
	 */
	
	@Test(groups={"MSite" , "MSiteBooking"})
	public void testMobileBookingAsTreeboMemberOneAdultOneChildConfig() throws ParseException{
		homeScreen = new HomeScreen();
		hotelSearchResultScreen = new HotelSearchResultsScreen();
		hotelDetailsScreen = new HotelDetailsScreen();
		itineraryScreen = new ItineraryScreen(); 
		confirmationScreen = new ConfirmationScreen();
		browserHelper = new BrowserHelper();
		CommonUtils utils = new CommonUtils();
		SoftAssert verify = new SoftAssert();
		
		String location = utils.getProperty("city"); // "Bengaluru";
		String testHotel = utils.getProperty("DummyHotel"); // "Elmas";
		String testUrl = utils.getProperty("TestUrl");
		int[][] roomIndex = { { 1, 2, 1 } };
		int hotelIndex = 0;
		int checkInFromCurrentDate = utils.getRandomNumber(30, 45);
		int checkOutFromCurrentDate = checkInFromCurrentDate + 1;
		String guestName = utils.getProperty("GuestName"); // "Test";
		String guestEmail = utils.getProperty("TestBookingAccount"); // "";
		String guestMobile = utils.getProperty("GuestMobile");// "9000000000";
		String guestPassword = utils.getProperty("TestLoginPassword");
		String[] checkInCheckOutDates = new String[2];
		
		// Check if home screen is displayed
		verify.assertTrue(homeScreen.isHomeScreenDisplayed(),"Home Screen is not displayed");
		
		//Login
		homeScreen.openDrawer();
		homeScreen.clickOnLogin();
		homeScreen.loginAsTreeboMember(guestEmail, guestPassword);
		
		//Do Search
		checkInCheckOutDates = homeScreen.doSearch(location, checkInFromCurrentDate, checkOutFromCurrentDate,roomIndex);
		// check if search results displayed
		verify.assertTrue(hotelSearchResultScreen.isHotelSearchScreenDisplayed(),"search result screen not displayed");
		//Select a random hotel or test hotel
		if (browserHelper.getCurrentUrl().contains(testUrl)) {
			hotelIndex = hotelSearchResultScreen.getAllAvailableHotelNames().indexOf(testHotel);
		}else{
			hotelIndex = hotelSearchResultScreen.getRandomIndexOfAvailableHotel();
		}
		
		String hotelName = hotelSearchResultScreen.getHotelNameByIndex(hotelIndex);
		int hotelRoomRate = hotelSearchResultScreen.getRoomRateByIndex(hotelIndex);
		System.out.println("Hotel Name: " + hotelName + " Hotel room rate : " + hotelRoomRate );

		
		//Click hotel name 
		hotelSearchResultScreen.clickHotelByIndex(hotelIndex);
		
		// Hotel Details Screen
		verify.assertTrue(hotelDetailsScreen.isHDScreenDisplayed(),"HD Screen is not displayed");
		verify.assertEquals(hotelDetailsScreen.getHotelNameInHDScreen(), hotelName,"Hotel Name is wrong in HD");
		
		hotelDetailsScreen.clickOnBook();
		
		//Check If Itinerary screen
		verify.assertTrue(itineraryScreen.isItineraryScreenDisplayed());
		
		// click on next
		itineraryScreen.clickOnNextStepOne();
        verify.assertEquals(itineraryScreen.getGuestName(),guestName,"Prefilled name on itinerary is not correct");
        verify.assertEquals(itineraryScreen.getGuestMobile(),guestMobile,"Prefilled mobile on itinerary is not correct");
        verify.assertEquals(itineraryScreen.getGuestEmail(),guestEmail,"Prefilled email on itinerary is not correct");
		itineraryScreen.clickOnPayAtHotel();
		
		verify.assertTrue(confirmationScreen.isConfirmationScreenDisplayed(),"Confirmation screen not displayed");
		System.out.println("Booking id : " + confirmationScreen.getBookingId());
		verify.assertEquals(guestName, confirmationScreen.getGuestName());
		verify.assertEquals(hotelName, confirmationScreen.getHotelName());
		verify.assertEquals(2 , confirmationScreen.getGuestCount());
		verify.assertEquals(1 , confirmationScreen.getChildCount());
		verify.assertEquals(1 , confirmationScreen.getRoomCount());
		verify.assertEquals(utils.formatDate(checkInCheckOutDates[0]).toUpperCase(),
				confirmationScreen.getCheckInDate().toUpperCase());
		verify.assertEquals(utils.formatDate(checkInCheckOutDates[1]).toUpperCase(),
				confirmationScreen.getCheckOutDate().toUpperCase());
		verify.assertTrue(confirmationScreen.isPayAtHotelImageDisplayed(),"Pay At Hotel image not displayed");
		verify.assertAll();
	}
	/***
	 * open homePage and search for random checkIn and checkOut date
	 * click on search button
	 * then extract the random hotelname and Room price of that hotel
	 * after extracting the roomPrice from there click on quickBook
	 * then after extracting the price from that page
	 * click on back button
	 * and then click on hotel using hotel name
	 * extracting price from that page also
	 * then compare all three prices
	 * @throws ParseException
	 */
	@Test(groups={"MSite" , "MSiteBooking"})
	public void testMobileBookingPriceCheck() throws ParseException{
		homeScreen = new HomeScreen();
		hotelSearchResultScreen = new HotelSearchResultsScreen();
		hotelDetailsScreen = new HotelDetailsScreen();
		itineraryScreen = new ItineraryScreen(); 
		confirmationScreen = new ConfirmationScreen();
		browserHelper = new BrowserHelper();
		CommonUtils utils = new CommonUtils();
		SoftAssert verify = new SoftAssert();
		
		String location = utils.getProperty("city"); // "Bengaluru";
		String testHotel = utils.getProperty("DummyHotel"); // "Elmas";
		String testUrl = utils.getProperty("TestUrl");
		int hotelIndex = 0;
		int checkInFromCurrentDate = utils.getRandomNumber(30, 45);
		int checkOutFromCurrentDate = checkInFromCurrentDate + 1;
		String[] checkInCheckOutDates = new String[2];
		
		// Check if home screen is displayed
		verify.assertTrue(homeScreen.isHomeScreenDisplayed(),"Home Screen is not displayed");
		//Do Search
		checkInCheckOutDates = homeScreen.doSearch(location, checkInFromCurrentDate, checkOutFromCurrentDate);
		// check if search results displayed
		verify.assertTrue(hotelSearchResultScreen.isHotelSearchScreenDisplayed(),"search result screen not displayed");
		//Select a random hotel or test hotel
		if (browserHelper.getCurrentUrl().contains(testUrl)) {
			hotelIndex = hotelSearchResultScreen.getAllAvailableHotelNames().indexOf(testHotel);
		}else{
			hotelIndex = hotelSearchResultScreen.getRandomIndexOfAvailableHotel();
		}
		
		String hotelName = hotelSearchResultScreen.getHotelNameByIndex(hotelIndex);
		int hotelRoomRate = hotelSearchResultScreen.getRoomRateByIndex(hotelIndex);
		System.out.println("Hotel Name: " + hotelName + " Hotel room rate : " + hotelRoomRate );
		
		hotelSearchResultScreen.clickQuickBookByIndex(hotelIndex);
		int hotelRoomRate2=hotelSearchResultScreen.extractGrandTotal();
		hotelSearchResultScreen.clickOnBackButton();

		hotelSearchResultScreen.clickHotelByName(hotelName);
		int hotelRoomRate3=hotelSearchResultScreen.extractPriceUsingHotelName();
		
		verify.assertTrue(hotelSearchResultScreen.isEqualAllPrices(hotelRoomRate, hotelRoomRate2, hotelRoomRate3),"All three prices are not equal");
		
		verify.assertAll();
	    }
}
