package newDesktopUIPageObjects;

import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.text.WordUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import base.BrowserHelper;
import base.DriverManager;
import utils.CommonUtils;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

public class HomePage {
	private BrowserHelper browserHelper;

	private By homePageName = By.xpath("//body[@pagename='homePage']");
	private By destination = By.xpath("//input[@id='searchInput']");
	private By checkin = By
			.xpath("//div[@id='calendar']//div[contains(@class,'dr-date dr-date-start search-box__title')]");
	private By checkout = By
			.xpath("//div[@id='calendar']//div[contains(@class,'dr-date dr-date-end search-box__title')]");
	private By cityDropDown = By.xpath("//ul[contains(@class,'extra-cities')]/li/span[1]");

	private By homeTreeboLink = By.xpath("//ul[contains(@class,'header-nav')]/li/a[@id='home']/img");
	private By friendsOfTreeboLink = By.xpath("//ul[contains(@class,'header-nav')]/li/a[text()='Friends of Treebo']");
	private By signUpFriendsOfTreeboLink = By.xpath("//a[contains(@href,'/fot/register/')]");
	private By joinOurNetworkLink = By.xpath("//ul[contains(@class,'header-nav')]/li/a[text()='Join Our Network']");
	private By aboutLink = By.xpath("//ul[contains(@class,'header-nav')]/li/a[text()='About']");
	private By callLink = By.xpath("//ul[contains(@class,'header-nav')]/li/a[@href='tel::(+91) 9322800100']");
	private By loginLink = By.xpath("//ul[contains(@class,'header-nav')]/li/a[text()='Login']");
	private By signUpLink = By.xpath("//ul[contains(@class,'header-nav')]/li/a[text()='Signup']");
	private By forgotPasswordLink = By.xpath("//a[text()='Forgot Password?']");
	private By emailInForgotPasswordPopUp = By.xpath("//form[@id='popup-forgot-form']//input[@type='email']");
	private By sendLinkInForgotPasswordPopUp = By.xpath(
			"//form[@id='popup-forgot-form']/input[contains(@class,'forgot__btn js-forgot-btn')][@value='SEND LINK']");
	private By forgotPasswordSuccessText = By.xpath("//div[text()='Link sent successfully' or text()='Reset password link sent successfully']");
	private By passwordFieldInResetPasswordForm = By.xpath("//form[@id='resetForm']//input[@id='pass']");
	private By repeatPasswordFieldInResetPasswordForm = By.xpath("//form[@id='resetForm']//input[@id='repeatpass']");
	private By submitButtonInResetPasswordForm = By.xpath("//form[@id='resetForm']//input[@id='resetSubmit']");
	private By successMsgInResetPasswordText = By
			.xpath("//div[contains(@class,'resetpassword')]/div[@class='resetmsg']/div");
	private By loginErrorMessage = By.xpath("//div[contains(@class,'login__error')]");

	private By signInThroughFb = By.xpath("//button[contains(@class,'js-fblogin')]");
	private By fbEmailField = By.xpath("//html[@id='facebook']//input[@id='email']");
	private By fbPasswordField = By.xpath("//html[@id='facebook']//input[@id='pass']");
	private By fbLoginButton = By.xpath("//html[@id='facebook']//input[@name='login']");

	private By signInThroughGmail = By.xpath("//button[contains(@class,'js-google-login')]");
	private By gmailEmailField = By.xpath("//input[@id='Email']");
	private By gmailNextLink = By.xpath("//input[@id='next']");
	private By gmailPasswordField = By.xpath("//input[@id='Passwd']");
	private By gmailSignInButton = By.xpath("//input[@id='signIn']");

	private By aboutUsPage = By.xpath("//div[@class='container aboutus']");
	private By joinUsPage = By.xpath("//div[@class='joinus_content']");

	private By loginPopUp = By.xpath("//div[@id='commonModal'][not(contains(@class,'hide'))]//div[@id='loginPopup']");
	private By emailFieldInLoginPopUp = By.xpath(
			"//div[@id='commonModal'][not(contains(@class,'hide'))]//form[@id='popup-login-form']//input[@type='email']");
	private By errorEmailField = By.xpath(
			"//div[@id='commonModal'][not(contains(@class,'hide'))]//form[@id='popup-login-form']//input[@type='email']/../ul/li");
	private By passwordFieldInLoginPopUp = By.xpath(
			"//div[@id='commonModal'][not(contains(@class,'hide'))]//form[@id='popup-login-form']//input[@type='password']");
	private By errorPasswordField = By.xpath(
			"//div[@id='commonModal'][not(contains(@class,'hide'))]//form[@id='popup-login-form']//input[@type='password']/../ul/li");
	private By loginButtonInLoginPopUp = By.xpath(
			"//div[@id='commonModal'][not(contains(@class,'hide'))]//form[@id='popup-login-form']//button[text()='LOGIN']");
	private By closeLoginPopUp = By
			.xpath("//div[@id='commonModal'][not(contains(@class,'hide'))]//div[contains(@class,'icon-close')]/i");
	private By signoutLink = By.xpath("//li[@id='user']//a[@href='/logout/']");
	private By signedInUser = By.cssSelector(".user__name.dropbtn");
	private By myAccount = By.xpath("//li[@id='user']//a[@href='/account/']");

	private By signUpPopUp = By
			.xpath("//div[@id='commonModal'][not(contains(@class,'hide'))]//form[@id='popup-signup-form']");
	private By nameInSignUpPopUp = By.xpath(
			"//div[@id='commonModal'][not(contains(@class,'hide'))]//form[@id='popup-signup-form']/div[1]/input");
	private By mobileInSignUpPopUp = By.xpath(
			"//div[@id='commonModal'][not(contains(@class,'hide'))]//form[@id='popup-signup-form']/div[2]/input");
	private By emailInSignUpPopUp = By.xpath(
			"//div[@id='commonModal'][not(contains(@class,'hide'))]//form[@id='popup-signup-form']/div[3]/input");
	private By passwordInSignUpPopUp = By.xpath(
			"//div[@id='commonModal'][not(contains(@class,'hide'))]//form[@id='popup-signup-form']/div[4]/input");
	private By nameErrorInSignUpPopUp = By.xpath(
			"//div[@id='commonModal'][not(contains(@class,'hide'))]//form[@id='popup-signup-form']/div[1]/input/../ul/li");
	private By mobileErrorInSignUpPopUp = By.xpath(
			"//div[@id='commonModal'][not(contains(@class,'hide'))]//form[@id='popup-signup-form']/div[2]/input/../ul/li");
	private By emailErrorInSignUpPopUp = By.xpath(
			"//div[@id='commonModal'][not(contains(@class,'hide'))]//form[@id='popup-signup-form']/div[3]/input/../ul/li");
	private By passwordErrorInSignUpPopUp = By.xpath(
			"//div[@id='commonModal'][not(contains(@class,'hide'))]//form[@id='popup-signup-form']/div[4]/input/../ul/li");
	private By signUpButton = By.xpath(
			"//div[@id='commonModal'][not(contains(@class,'hide'))]//button[contains(@class,'signup__full signup__button')]");
	private By closeSignUpPopUp = By
			.xpath("//div[@id='commonModal'][not(contains(@class,'hide'))]//div[contains(@class,'icon-close')]/i");
	private By textForTreeboMember = By.cssSelector(".pos-abs.signup__newtotreebo");
	private By loginLinkInSignUpPopUp = By.xpath(
			"//div[@id='commonModal'][not(contains(@class,'hide'))]//div[contains(@class,'signup__form')]//div[contains(@class,'signup__newtotreebo')]/a");
	private By termsOfServiceLinkInSignupPopUp = By.xpath(
			"//div[@id='commonModal'][not(contains(@class,'hide'))]//div[contains(@class,'signup__form')]//div[@class='signup__terms']/a[@href='/terms/']");
	private By privacyPolicyInSignipPopUp = By.xpath(
			"//div[@id='commonModal'][not(contains(@class,'hide'))]//div[contains(@class,'signup__form')]//div[@class='signup__terms']/a[@href='/policy/']");

	private By cityLink = By.xpath("//a[contains(@class,'slick-active')]//h3");
	private By lastCityLink = By.xpath("//a[contains(@class,'slick-active')][last()]//h3");
	private By prevLinkCityDisplay = By.xpath("//button[contains(@class,'slick-prev')]");
	private By nextLinkCityDisplay = By.xpath("//button[contains(@class,'slick-next')]");

	private By calendar = By.xpath("//div[@class='dr-calendar']//ul[@class='dr-day-list']");
	private By calendarRightClick = By.cssSelector(".icon-right");
	private By calendarLeftClick = By.cssSelector(".icon-back");
	private By availableDateInMonth = By
			.xpath("//ul[@class='dr-day-list']/li[not((contains(@class,'dr-outside') or contains(@class,'dr-fade')))]");
	private By checkOutDateXpath = By.xpath("//li[contains(@class,'dr-day dr-end dr-current')]");
	private By guestNRoomField = By.xpath("//div[@class='room-widget__total search-box__title']");

	private By searchButton = By.xpath("//button[@id='searchSubmitBtn']");

	private By customizeGuestNRoomLink = By.xpath("//div[contains(@class,'room-widget__options__item')]");
	private By roomConfigAdults = By.xpath("//div[contains(@class,'room-config__adults')]/select/option");
	private By addRoomByPlusIcon = By.xpath("//div[contains(@class,'room-widget__config__add-room')]/i");
	private By addRoomByAddLink = By.xpath("//div[contains(@class,'room-widget__config__add-room')]/span");
	private By removeRoomLink = By.xpath("//ul[@class='config-options-list']/li//a[@class='room-config__remove-link']");
	
	private By monthName = By.cssSelector(".dr-switcher.dr-month-switcher.flex-row>span");
	private By joinUsNetwork = By.xpath("//*[@id='content']/div[1]//li[@class='header-nav__item']/a[@href='/joinus/']");

	public void verifyHomePagePresence() {
		WebDriver driver = DriverManager.getInstance().getDriver();
		browserHelper = new BrowserHelper();
		System.out.println("Verify Home page is displayed");

		//check if 500 page
		if (is500Displayed()){
			Assert.fail("500 displayed instead of search page for url " + browserHelper.getCurrentUrl());
		}
		
		try{
			WebDriverWait wait = new WebDriverWait(driver, 120);
			wait.until(ExpectedConditions.presenceOfElementLocated(homePageName));
		}catch (Exception e){
			// do nothing
		}
		
		Assert.assertTrue((driver.findElements(homePageName).size() > 0),
				"Home page is not Displayed, current page has url: " + driver.getCurrentUrl());
		Assert.assertTrue(driver.findElement(homePageName).isDisplayed(),
				"Home page is not Displayed, current page has url: " + driver.getCurrentUrl());
	}

	public void goToHomePage() {
		WebDriver driver = DriverManager.getInstance().getDriver();
		System.out.println("Go To Home Page");
		driver.findElement(homeTreeboLink).click();
	}

	public void selectDate(int checkIn) {
		WebDriver driver = DriverManager.getInstance().getDriver();
		WebDriverWait wait = new WebDriverWait(driver, 120);
		// number of days available in current month
		// first check if checkIn days available in current month
		if (checkIn <= getAvailableDatesInMonth()) {
			clickDate(checkIn);
		} else if (checkIn > getAvailableDatesInMonth()) {
			int currentMonthAvailableDate = getAvailableDatesInMonth();
			driver.findElement(calendarRightClick).click();
			wait.until(ExpectedConditions.presenceOfElementLocated(calendar));
			int updatedCheckIn = checkIn - currentMonthAvailableDate;
			selectDate(updatedCheckIn);
		}
	}

	public void selectCheckOutDate(int checkInDate, int checkOutDate) {
		WebDriver driver = DriverManager.getInstance().getDriver();
		WebDriverWait wait = new WebDriverWait(driver, 120);

		// if its 1 night, i.e checkout -checkin = 1, checkout automatically
		// selected
		if ((checkOutDate - checkInDate) == 1) {
			driver.findElement(checkOutDateXpath).click();
		} else {
			// number of days available in current month after checkin
			// first check if checkIn days available in current month

			// Get checkin date number selected
			int checkIndateNumberInCalendar = Integer
					.parseInt(driver.findElement(By.xpath("//li[contains(@class,'dr-day dr-start')]")).getText());

			int lastDate = Integer.parseInt(driver.findElement(By.xpath("//li[@class='dr-day'][last()]")).getText());
			int lastSelectedDate = Integer.parseInt(
					driver.findElement(By.xpath("//li[contains(@class,'dr-day dr-end dr-current')]")).getText());
			// last available date in calendar
			int lastAvailableDateIndex = (lastDate >= lastSelectedDate) ? lastDate : lastSelectedDate;
			int selectableDaysInCurrentMonth = lastAvailableDateIndex - checkIndateNumberInCalendar;

			// If checkout date is available in current month
			if ((checkOutDate - checkInDate) <= selectableDaysInCurrentMonth) {
				// Now need to select checkout date with reference to checkin
				// Date
				String xpathCheckOutDate = String.format("//li[text()='%d']",
						(checkIndateNumberInCalendar + checkOutDate - checkInDate));
				driver.findElement(By.xpath(xpathCheckOutDate)).click();
			} else {
				// Move to next month
				driver.findElement(calendarRightClick).click();
				int currentMonthAvailableDate = getAvailableDatesInMonth();
				// Available in this month
				int updatedCheckOutdate = checkOutDate - checkInDate - selectableDaysInCurrentMonth;

				if ((updatedCheckOutdate) <= currentMonthAvailableDate) {
					String xpathCheckOutDateOne = String.format("//li[text()='%d']", (updatedCheckOutdate));
					driver.findElement(By.xpath(xpathCheckOutDateOne)).click();
				} else {
					driver.findElement(calendarRightClick).click();
					String xpathCheckOutDateTwo = String.format("//li[text()='%d']",
							(updatedCheckOutdate - currentMonthAvailableDate));
					driver.findElement(By.xpath(xpathCheckOutDateTwo)).click();
				}
			}
		}
	}

	public void selectCheckInCheckOutDate(int checkInDate, int checkOutDate) {
		WebDriver driver = DriverManager.getInstance().getDriver();
		// select checkin date
		driver.findElement(checkin).click();
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		selectDate(checkInDate);
		selectCheckOutDate(checkInDate, checkOutDate);
	}

	public String getEnteredCheckInDate() {
		WebDriver driver = DriverManager.getInstance().getDriver();
		System.out.println("Entered Checkin date is :" + driver.findElement(checkin).getText());
		return driver.findElement(checkin).getText();
	}

	public String getEnteredCheckOutDate() {
		WebDriver driver = DriverManager.getInstance().getDriver();
		System.out.println("Entered Checkout date is :" + driver.findElement(checkout).getText());
		return driver.findElement(checkout).getText();
	}

	public void clickDate(int dateToSelect) {
		WebDriver driver = DriverManager.getInstance().getDriver();
		String xpathDateToSelect = String.format(
				"//ul[contains(@class,'dr-day-list')]/li[not((contains(@class,'dr-outside') or contains(@class,'dr-fade')))][%d]",
				dateToSelect);
		System.out.println(xpathDateToSelect);
		driver.findElement(By.xpath(xpathDateToSelect)).click();
	}

	public int getAvailableDatesInMonth() {
		WebDriver driver = DriverManager.getInstance().getDriver();
		WebDriverWait wait = new WebDriverWait(driver, 120);
		wait.until(ExpectedConditions.presenceOfElementLocated(calendar));
		List<WebElement> dates = driver.findElements(availableDateInMonth);
		return dates.size();
	}

	public void verifyPastDatesAreDisabled() {
		//WebDriver driver = DriverManager.getInstance().getDriver();
		// driver.findElement(checkin).click();
		// String getText =
		// driver.findElement(By.xpath("//li[contains(@class,'dr-current')]")).getText();
		// int currentDate = Integer.parseInt(getText);
		// // current date should not be first date in calendar, in which case
		// there is no past date
		// if (currentDate != 1){
		// for ( int i = (currentDate -1);i<=1;--i){
		// Assert.assertTrue(driver.findElement(By.xpath(String.format("//li[text()='%d']",
		// i))).getAttribute("class").contains("outside"));
		// }
		// }
	}

	public void enterDestination(String dest) {
		WebDriver driver = DriverManager.getInstance().getDriver();
		System.out.println("Enter the destination as :" + dest);
		driver.findElement(destination).sendKeys(dest);
	}

	public void selectGuestNRoom(int index) {
		WebDriver driver = DriverManager.getInstance().getDriver();
		driver.findElement(guestNRoomField).click();
		String xpathGuestRoomIndex = String.format("//div[contains(@class,'room-widget__options')]/ul/li[%d]", index);
		driver.findElement(By.xpath(xpathGuestRoomIndex)).click();
	}

	public void selectGuestNRoom(int[][] roomAdultChild) {
		WebDriver driver = DriverManager.getInstance().getDriver();
		driver.findElement(guestNRoomField).click();
		driver.findElement(customizeGuestNRoomLink).click();
		// example array of array roomAdultChild - {{1,2,1},{2,2,0},{3,1,2}}
		// Get each room config i.e. count of Adult and Child
		// First get how many rooms are there
		int numOfRooms = roomAdultChild.length;
		System.out.println("Number of rooms :" + numOfRooms);
		System.out.println("Select adult and child for each room");
		for (int[] config : roomAdultChild) {
			String xpathAdult = String.format(
					"//ul[@class='config-options-list']/li[%d]//div[contains(@class,'room-config__adults')]/select",
					config[0]);
			String xpathSelectAdult = String.format(
					"//ul[@class='config-options-list']/li[%d]//div[contains(@class,'room-config__adults')]/select/option[%d]",
					config[0], config[1]);
			driver.findElement(By.xpath(xpathAdult)).click();
			driver.findElement(By.xpath(xpathSelectAdult)).click();
			String xpathChild = String.format(
					"//ul[@class='config-options-list']/li[%d]//div[contains(@class,'room-config__children')]/select",
					config[0]);
			String xpathSelectChild = String.format(
					"//ul[@class='config-options-list']/li[%d]//div[contains(@class,'room-config__children')]/select/option[%d]",
					config[0], (config[2] + 1));
			driver.findElement(By.xpath(xpathChild)).click();
			driver.findElement(By.xpath(xpathSelectChild)).click();
			if (numOfRooms > 1) {
				addRoomByAddLink();
				numOfRooms = numOfRooms - 1;
			}
		}
	}

	public void addRoomByAddLink() {
		WebDriver driver = DriverManager.getInstance().getDriver();
		driver.findElement(addRoomByAddLink).click();
	}

	public void addRoomByPlusSign() {
		WebDriver driver = DriverManager.getInstance().getDriver();
		driver.findElement(addRoomByPlusIcon).click();
	}

	public void removeRoomsInConfig() {
		WebDriver driver = DriverManager.getInstance().getDriver();
		System.out.println("Remove All rooms");
		while (driver.findElements(removeRoomLink).size() > 0) {
			driver.findElement(removeRoomLink).click();
		}
	}

	public void doSearch(String location) {
		WebDriver driver = DriverManager.getInstance().getDriver();
		browserHelper = new BrowserHelper();
		enterDestination(location);
		System.out.println("clicking search button");
		driver.findElement(searchButton).click();
		browserHelper.waitTime(5000);
	}

	public void doSearch() throws FileNotFoundException, IOException {
		WebDriver driver = DriverManager.getInstance().getDriver();
		browserHelper = new BrowserHelper();
		CommonUtils utils = new CommonUtils();
		String location = "Bangalore";// utils.getProperty("Location");
										// //"Bangalore";
		enterDestination(location);
		System.out.println("Doing Search with location as: " + location + " with default selected dates");
		driver.findElement(searchButton).click();
		browserHelper.waitTime(5000);
	}

	public String[] doSearch(String location, int checkInFromCurrentDate, int checkOutFromCurrentDate, int roomIndex) {
		WebDriver driver = DriverManager.getInstance().getDriver();
		browserHelper = new BrowserHelper();
		String[] checkInCheckOutDates = new String[2];
		enterDestination(location);
		selectCheckInCheckOutDate(checkInFromCurrentDate, checkOutFromCurrentDate);
		selectGuestNRoom(roomIndex);
		checkInCheckOutDates[0] = getEnteredCheckInDate();
		checkInCheckOutDates[1] = getEnteredCheckOutDate();
		System.out.println("clicking search button");
		driver.findElement(searchButton).click();
		System.out.println("Hotel search done for: " + location + " with checkin date as: " + checkInCheckOutDates[0]
				+ " and checkout date as: " + checkInCheckOutDates[1]);
		browserHelper.waitTime(5000);
		return checkInCheckOutDates;
	}

	public String[] doSearch(String location, int checkInFromCurrentDate, int checkOutFromCurrentDate,
			int[][] roomAdultChild) {
		WebDriver driver = DriverManager.getInstance().getDriver();
		browserHelper = new BrowserHelper();
		String[] checkInCheckOutDates = new String[2];
		enterDestination(location);
		selectCheckInCheckOutDate(checkInFromCurrentDate, checkOutFromCurrentDate);
		selectGuestNRoom(roomAdultChild);
		checkInCheckOutDates[0] = getEnteredCheckInDate();
		checkInCheckOutDates[1] = getEnteredCheckOutDate();
		System.out.println("clicking search button");
		driver.findElement(searchButton).click();
		System.out.println("Hotel search done for: " + location + " with checkin date as: " + checkInCheckOutDates[0]
				+ " and checkout date as: " + checkInCheckOutDates[1]);
		browserHelper.waitTime(5000);
		return checkInCheckOutDates;
	}

	public void clickSearchButton() {
		WebDriver driver = DriverManager.getInstance().getDriver();
		driver.findElement(searchButton).click();
	}

	public void verifyCityDropDownDisplayed() {
		WebDriver driver = DriverManager.getInstance().getDriver();
		Assert.assertTrue(driver.findElement(cityDropDown).isDisplayed());
	}

	public void selectCityFromDropDown() {
		WebDriver driver = DriverManager.getInstance().getDriver();
		driver.findElement(cityDropDown).click();
	}

	public void clickOnHomeTreeboLink() {
		WebDriver driver = DriverManager.getInstance().getDriver();
		System.out.println("clicking search treebo image link");
		driver.findElement(homeTreeboLink).click();
	}

	public void clickOnFriendsOfTreeboLink() {
		WebDriver driver = DriverManager.getInstance().getDriver();
		System.out.println("clicking friends of treebo link");
		driver.findElement(friendsOfTreeboLink).click();
	}

	public void clickOnJoinOurNetworkLink() {
		WebDriver driver = DriverManager.getInstance().getDriver();
		System.out.println("clicking Join our network link");
		driver.findElement(joinOurNetworkLink).click();
	}

	public void clickOnAbout() {
		WebDriver driver = DriverManager.getInstance().getDriver();
		System.out.println("clicking About link");
		driver.findElement(aboutLink).click();
	}

	public void clickOnCallLink() {
		WebDriver driver = DriverManager.getInstance().getDriver();
		System.out.println("clicking call link");
		driver.findElement(callLink).click();
	}

	public void clickOnLoginLink() {
		WebDriver driver = DriverManager.getInstance().getDriver();
		System.out.println("clicking Login link");
		driver.findElement(loginLink).click();
	}

	public boolean isLoginLinkAvailable() {
		WebDriver driver = DriverManager.getInstance().getDriver();
		return (driver.findElements(loginLink).size() > 0);
	}

	public void clickOnSignUpLink() {
		WebDriver driver = DriverManager.getInstance().getDriver();
		System.out.println("clicking sign up link");
		driver.findElement(signUpLink).click();
	}

	public void verifyAboutUsPagePresence() {
		WebDriver driver = DriverManager.getInstance().getDriver();
		System.out.println("Verify About us page is displayed");
		WebDriverWait wait = new WebDriverWait(driver, 120);
		wait.until(ExpectedConditions.presenceOfElementLocated(aboutUsPage));
		Assert.assertTrue(driver.findElement(aboutUsPage).isDisplayed());
	}

	public void verifyJoinUsPagePresence() {
		WebDriver driver = DriverManager.getInstance().getDriver();
		System.out.println("Verify Join us page is displayed");
		WebDriverWait wait = new WebDriverWait(driver, 120);
		wait.until(ExpectedConditions.presenceOfElementLocated(joinUsPage));
		Assert.assertTrue(driver.findElement(joinUsPage).isDisplayed());
	}

	public void verifyLoginPopUpPresence() {
		WebDriver driver = DriverManager.getInstance().getDriver();
		System.out.println("Verify Login Pop up is displayed");
		WebDriverWait wait = new WebDriverWait(driver, 120);
		wait.until(ExpectedConditions.presenceOfElementLocated(loginPopUp));
		Assert.assertTrue(driver.findElement(loginPopUp).isDisplayed());
		Assert.assertTrue(driver.findElement(emailFieldInLoginPopUp).isDisplayed());
		Assert.assertTrue(driver.findElement(passwordFieldInLoginPopUp).isDisplayed());
		Assert.assertTrue(driver.findElement(loginButtonInLoginPopUp).isDisplayed());
	}

	public boolean isLoginPopUpDisplayed() throws InterruptedException {
		WebDriver driver = DriverManager.getInstance().getDriver();
		Thread.sleep(3000);
		try {
			driver.findElement(emailFieldInLoginPopUp);
			return true;
		} catch (NoSuchElementException e) {
			return false;
		}
	}

	public void closeLoginPopUp() {
		WebDriver driver = DriverManager.getInstance().getDriver();
		System.out.println("close login pop up");
		driver.findElement(closeLoginPopUp).click();
	}

	public void verifySignUpPopUpPresence() {
		WebDriver driver = DriverManager.getInstance().getDriver();
		System.out.println("Verify Sign up Pop up is displayed");
		WebDriverWait wait = new WebDriverWait(driver, 120);
		wait.until(ExpectedConditions.presenceOfElementLocated(signUpPopUp));
		Assert.assertTrue(driver.findElement(signUpPopUp).isDisplayed());
		Assert.assertTrue(driver.findElement(nameInSignUpPopUp).isDisplayed());
		Assert.assertTrue(driver.findElement(mobileInSignUpPopUp).isDisplayed());
		Assert.assertTrue(driver.findElement(emailInSignUpPopUp).isDisplayed());
		Assert.assertTrue(driver.findElement(passwordInSignUpPopUp).isDisplayed());
		Assert.assertTrue(driver.findElement(signUpButton).isDisplayed());

	}

	public boolean isSignUpPopUpDisplayed() throws InterruptedException {
		WebDriver driver = DriverManager.getInstance().getDriver();
		Thread.sleep(2000);
		try {
			driver.findElement(emailInSignUpPopUp).isDisplayed();
			return true;
		} catch (NoSuchElementException e) {
			return false;
		}
	}

	public void closeSignUpPopUp() {
		WebDriver driver = DriverManager.getInstance().getDriver();
		System.out.println("close signup pop up");
		driver.findElement(closeSignUpPopUp).click();
	}

	public void verifyCountOfCitiesDisplayedAtATime() {
		WebDriver driver = DriverManager.getInstance().getDriver();
		System.out.println("Verify 5 city displayed at a time");
		List<WebElement> cityDisplayed = driver.findElements(cityLink);
		Assert.assertEquals(cityDisplayed.size(), 5);
	}

	public String[] getCitiesDisplayAtATime() {
		WebDriver driver = DriverManager.getInstance().getDriver();
		List<WebElement> cityDisplayed = driver.findElements(cityLink);
		String[] cityList = new String[cityDisplayed.size()];
		int i = 0;
		for (WebElement element : cityDisplayed) {
			cityList[i] = element.getText();
			i = i + 1;
		}
		System.out.println("Cities Displayed at a time are:" + Arrays.toString(cityList));
		return cityList;
	}

	public String clickCityLinkInHomePage() {
		WebDriver driver = DriverManager.getInstance().getDriver();
		String cityName = driver.findElement(cityLink).getText();
		driver.findElement(cityLink).click();
		System.out.println("clicking city name : " + cityName);
		return cityName;
	}

	public void clickOnNextLinkInCityDisplay() throws InterruptedException {
		WebDriver driver = DriverManager.getInstance().getDriver();
		System.out.println("Click on next link");
		driver.findElement(nextLinkCityDisplay).click();
		Thread.sleep(4000);
	}

	public void clickOnPrevLinkInCityDisplay() throws InterruptedException {
		WebDriver driver = DriverManager.getInstance().getDriver();
		System.out.println("Click on prev link");
		driver.findElement(prevLinkCityDisplay).click();
		Thread.sleep(4000);
	}

	public void enterLoginForm(String email, String password) {
		WebDriver driver = DriverManager.getInstance().getDriver();
		System.out.println("Login with email : " + email + " and password as : " + password);
		driver.findElement(emailFieldInLoginPopUp).sendKeys(email);
		driver.findElement(passwordFieldInLoginPopUp).sendKeys(password);
		driver.findElement(loginButtonInLoginPopUp).click();
	}

	public void enterLoginForm() {
		WebDriver driver = DriverManager.getInstance().getDriver();
		CommonUtils utils = new CommonUtils();
		String email = utils.getProperty("TestBookingAccount");
		String password = utils.getProperty("TestLoginPassword");
		System.out.println("Login with default test email : " + email + " and password as : " + password);
		driver.findElement(emailFieldInLoginPopUp).sendKeys(email);
		driver.findElement(passwordFieldInLoginPopUp).sendKeys(password);
		driver.findElement(loginButtonInLoginPopUp).click();
	}

	public void clickOnLoginButton() {
		WebDriver driver = DriverManager.getInstance().getDriver();
		driver.findElement(loginButtonInLoginPopUp).click();
	}

	public void clickOnSignOutLink() {
		WebDriver driver = DriverManager.getInstance().getDriver();
		System.out.println("Click on Signout ");
		driver.findElement(signedInUser).click();
		driver.findElement(signoutLink).click();
	}

	public boolean isUserSignedIn() {
		WebDriver driver = DriverManager.getInstance().getDriver();
		if (driver.findElements(signedInUser).size() > 0) {
			return true;
		} else {
			return false;
		}
	}

	public void verifyUserIsSignedIn() {
		WebDriver driver = DriverManager.getInstance().getDriver();
		System.out.println("Verify User is signed in by checking if User Link is displayed");
		WebDriverWait wait = new WebDriverWait(driver, 120);
		wait.until(ExpectedConditions.presenceOfElementLocated(signedInUser));
	}

	public void verifyUserIsNotSignedIn() {
		WebDriver driver = DriverManager.getInstance().getDriver();
		System.out.println("Verify User is not signed in by checking if login Link is displayed");
		WebDriverWait wait = new WebDriverWait(driver, 120);
		wait.until(ExpectedConditions.presenceOfElementLocated(loginLink));
		Assert.assertTrue(driver.findElement(loginLink).isDisplayed());
	}

	public void enterSignUpForm(String name, String mobile, String email, String password) {
		WebDriver driver = DriverManager.getInstance().getDriver();
		verifySignUpPopUpPresence();
		System.out.println("Sign up with following details. Name : " + name + ", mobile :" + mobile + ", email : "
				+ email + ", password:" + password);
		driver.findElement(nameInSignUpPopUp).sendKeys(name);
		driver.findElement(mobileInSignUpPopUp).sendKeys(mobile);
		driver.findElement(emailInSignUpPopUp).sendKeys(email);
		driver.findElement(passwordInSignUpPopUp).sendKeys(password);
		driver.findElement(signUpButton).click();
	}

	public String[] enterSignUpForm() {
		WebDriver driver = DriverManager.getInstance().getDriver();
		CommonUtils utils = new CommonUtils();
		browserHelper = new BrowserHelper();
		
		String name = "Test";
//		String uniqueEmail = "test" + RandomStringUtils.randomAlphanumeric(15);
//		String email = uniqueEmail + "@gmail.com";
		String email = browserHelper.getDisposableEmail();
		String mobile = Integer.toString(utils.getRandomNumber(2, 6)) + RandomStringUtils.randomNumeric(9);
		String password = "password";

		verifySignUpPopUpPresence();
		System.out.println("Default Sign up with following details. Name : " + name + ", mobile :" + mobile
				+ ", email : " + email + ", password:" + password);
		driver.findElement(nameInSignUpPopUp).sendKeys(name);
		driver.findElement(mobileInSignUpPopUp).sendKeys(mobile);
		driver.findElement(emailInSignUpPopUp).sendKeys(email);
		driver.findElement(passwordInSignUpPopUp).sendKeys(password);
		driver.findElement(signUpButton).click();
		String[] guestDetails = { name, email, mobile, password };
		return guestDetails;
	}

	public void clickOnSignUpButton() {
		WebDriver driver = DriverManager.getInstance().getDriver();
		driver.findElement(signUpButton).click();
	}

	public void signInAsTreeboMember(String email, String password) {
		WebDriver driver = DriverManager.getInstance().getDriver();
		if (!isLoginLinkAvailable()) {
			goToHomePage();
		}
		if (!(driver.findElements(loginPopUp).size() > 0)){
			clickOnLoginLink();
		}
		verifyLoginPopUpPresence();
		enterLoginForm(email, password);
		verifyUserIsSignedIn();
	}

	public void signInAsTreeboMember() {
		WebDriver driver = DriverManager.getInstance().getDriver();
		CommonUtils utils = new CommonUtils();
		String email = utils.getProperty("TestBookingAccount");
		String password = utils.getProperty("TestLoginPassword");

		try {
			if (driver.findElement(By.xpath("//input[@id='guestLoginEmail']")).isDisplayed()) {
				driver.findElement(By.xpath("//input[@id='guestLoginEmail']")).sendKeys(email);
				driver.findElement(By.xpath("//input[@id='guestLoginPassword']")).sendKeys(password);
				driver.findElement(By.xpath("//button[@id='guestLoginBtn']")).click();
				verifyUserIsSignedIn();
			}
		} catch (NoSuchElementException e) {
			clickOnLoginLink();
			verifyLoginPopUpPresence();
			enterLoginForm(email, password);
			verifyUserIsSignedIn();
		}
	}

	public void signInThroughFB(String email, String password) {
		WebDriver driver = DriverManager.getInstance().getDriver();
		browserHelper = new BrowserHelper();

		driver.findElement(signInThroughFb).click();
		String parentWindow = browserHelper.switchToNewWindow();
		driver.findElement(fbEmailField).sendKeys(email);
		driver.findElement(fbPasswordField).sendKeys(password);
		driver.findElement(fbLoginButton).click();
		driver.switchTo().window(parentWindow);
	}

	public void signInThroughGmail(String email, String password) {
		WebDriver driver = DriverManager.getInstance().getDriver();
		browserHelper = new BrowserHelper();

		driver.findElement(signInThroughGmail).click();
		String parentWindow = browserHelper.switchToNewWindow();
		signInToGmailForLogin(email, password);
		driver.switchTo().window(parentWindow);
	}

	public void enterEmailForResetPasswordAndSend(String email) {
		WebDriver driver = DriverManager.getInstance().getDriver();
		Assert.assertTrue(driver.findElement(forgotPasswordLink).isDisplayed());
		driver.findElement(forgotPasswordLink).click();
		driver.findElement(emailInForgotPasswordPopUp).sendKeys(email);
		driver.findElement(sendLinkInForgotPasswordPopUp).click();
		WebDriverWait wait = new WebDriverWait(driver, 120);
		wait.until(ExpectedConditions.presenceOfElementLocated(forgotPasswordSuccessText));
		Assert.assertTrue(driver.findElement(forgotPasswordSuccessText).isDisplayed());
	}

	public void signInToGmailForLogin(String email, String password) {
		WebDriver driver = DriverManager.getInstance().getDriver();
		driver.findElement(gmailEmailField).sendKeys(email);
		driver.findElement(gmailNextLink).click();
		WebDriverWait wait = new WebDriverWait(driver, 120);
		wait.until(ExpectedConditions.presenceOfElementLocated(gmailPasswordField));
		driver.findElement(gmailPasswordField).sendKeys(password);
		driver.findElement(gmailSignInButton).click();
	}

	public void signInToGmail(String email, String password) {
		WebDriver driver = DriverManager.getInstance().getDriver();
		browserHelper = new BrowserHelper();
		System.out.println("Signin to gmail");
		driver.get("http://gmail.com");
		driver.findElement(By.xpath("//a[text()='Sign in']")).click();
		driver.findElement(gmailEmailField).sendKeys(email);
		driver.findElement(gmailNextLink).click();
		WebDriverWait wait = new WebDriverWait(driver, 120);
		wait.until(ExpectedConditions.presenceOfElementLocated(gmailPasswordField));
		System.out.println(browserHelper.getAllWindowHandles().toString());
		driver.findElement(gmailPasswordField).sendKeys(password);
		driver.findElement(gmailSignInButton).click();
	}
	
	public void signInToRediffMail(String email,String password){
		WebDriver driver = DriverManager.getInstance().getDriver();
		browserHelper = new BrowserHelper();
		System.out.println("Signin to rediffmail");
		driver.get("http://rediff.com");
		driver.findElement(By.xpath("//u[text()='rediffmail']")).click();
		driver.findElement(By.cssSelector("#login1")).sendKeys(email);
		driver.findElement(By.cssSelector("#password")).sendKeys(password);
		driver.findElement(By.xpath("//input[@value='Go']")).click();
	}
	
	public void searchRediffMailWithTextAndOpenEmail(String text) {
		WebDriver driver = DriverManager.getInstance().getDriver();
		browserHelper = new BrowserHelper();
		WebDriverWait wait = new WebDriverWait(driver, 120);
		wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//input[@id='inp_search_box']")));
		driver.findElement(By.xpath("//input[@id='inp_search_box']")).sendKeys(text);
		driver.findElement(By.cssSelector(".rd_srcbtn")).click();
		wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(".rd_subject[title='Treebo Hotels: Reset Password Link']")));
		browserHelper.waitTime(5000);
		driver.findElement(By.cssSelector(".rd_subject[title='Treebo Hotels: Reset Password Link']")).click();
		browserHelper.waitTime(5000);
		driver.findElement(By.xpath("//a[contains(text(),'reset')]")).click();
	}

	public void searchGmailWithTextAndOpenEmail(String text) {
		WebDriver driver = DriverManager.getInstance().getDriver();
		browserHelper = new BrowserHelper();
		WebDriverWait wait = new WebDriverWait(driver, 120);
		wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//input[@name='q']")));
//		browserHelper.waitTime(3000);
//		System.out.println("Searching for email with text :" + text);
//		driver.findElement(By.xpath("//input[@name='q']")).sendKeys(text);
//		driver.findElement(By.xpath("//button[@id='gbqfb']")).click();
//		browserHelper.waitTime(3000);
		wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//b[contains(text(),'Treebo Hotels: Reset Password Link')]")));
		browserHelper.waitTime(10000);
		driver.findElement(By.xpath("//b[contains(text(),'Treebo Hotels: Reset Password Link')]")).click();
	}
	
	public void searchGmailWithTextAndOpenTheEmail(String text) {
		WebDriver driver = DriverManager.getInstance().getDriver();
		browserHelper = new BrowserHelper();
		WebDriverWait wait = new WebDriverWait(driver, 120);
		wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//input[@name='q']")));
		System.out.println("Searching for email with text :" + text);
		driver.findElement(By.xpath("//input[@name='q']")).sendKeys(text);
		driver.findElement(By.xpath("//button[@id='gbqfb']")).click();
		System.out.println("Opening email for booking");
		browserHelper.waitTime(3000);
		wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//b[contains(text(),'Booking')]")));
		browserHelper.waitTime(5000);
		driver.findElement(By.xpath("//b[contains(text(),'Booking')]")).click();
	}

	public void clickOnResetPasswordLinkInGmail() {
		WebDriver driver = DriverManager.getInstance().getDriver();
		List<WebElement> resetPwdLink = driver.findElements(By.xpath("//a[contains(@href,'reset-password')]"));
		resetPwdLink.get(resetPwdLink.size() - 1).click();
		driver.findElement(By.xpath("//a[contains(@href,'reset-password')]")).click();
	}

	public void resetPassword(String password, String repeatPassword) {
		WebDriver driver = DriverManager.getInstance().getDriver();
		String expectedMessage = "Password changed successfully. Click here to login";
		driver.findElement(passwordFieldInResetPasswordForm).sendKeys(password);
		driver.findElement(repeatPasswordFieldInResetPasswordForm).sendKeys(repeatPassword);
		driver.findElement(submitButtonInResetPasswordForm).click();
	}

	public void verifyLoginError() {
		WebDriver driver = DriverManager.getInstance().getDriver();
		CommonUtils utils = new CommonUtils();
		String errorText = utils.getProperty("LoginError"); // "Invalid Username
															// or Password";
		String textInApp = driver.findElement(loginErrorMessage).getText();
		System.out.println(errorText);
		System.out.println(textInApp);
		Assert.assertTrue(textInApp.equals(errorText), errorText + " and " + textInApp + " are not same.");
	}

	public void verifyEmailError() {
		WebDriver driver = DriverManager.getInstance().getDriver();
		CommonUtils utils = new CommonUtils();
		String errorText = utils.getProperty("EmailError");
		Assert.assertTrue(driver.findElement(errorEmailField).getText().equals(errorText));
	}

	public void verifyPasswordError() {
		WebDriver driver = DriverManager.getInstance().getDriver();
		CommonUtils utils = new CommonUtils();
		String errorText = utils.getProperty("PasswordError");
		Assert.assertTrue(driver.findElement(errorPasswordField).getText().equals(errorText));
	}

	public void verifyEmailInSignUpError() {
		WebDriver driver = DriverManager.getInstance().getDriver();
		CommonUtils utils = new CommonUtils();
		String errorText = utils.getProperty("EmailError");
		Assert.assertTrue(driver.findElement(emailErrorInSignUpPopUp).getText().equals(errorText));
	}

	public void verifyPasswordInSignUpError() {
		WebDriver driver = DriverManager.getInstance().getDriver();
		CommonUtils utils = new CommonUtils();
		String errorText = utils.getProperty("PasswordError");
		Assert.assertTrue(driver.findElement(passwordErrorInSignUpPopUp).getText().equals(errorText));
	}

	public void verifyMobileInSignUpError() {
		WebDriver driver = DriverManager.getInstance().getDriver();
		CommonUtils utils = new CommonUtils();
		String errorText = utils.getProperty("MobileError");
		Assert.assertTrue(driver.findElement(mobileErrorInSignUpPopUp).getText().equals(errorText));
	}

	public void verifyNameInSignUpError() {
		WebDriver driver = DriverManager.getInstance().getDriver();
		CommonUtils utils = new CommonUtils();
		String errorText = utils.getProperty("NameError");
		Assert.assertTrue(driver.findElement(nameErrorInSignUpPopUp).getText().equals(errorText));
	}

	public void verifyCharacterLimitforPassword() {
		WebDriver driver = DriverManager.getInstance().getDriver();
		CommonUtils utils = new CommonUtils();
		String password = RandomStringUtils.randomAlphanumeric(8);
		String errorText = utils.getProperty("PasswordErrorForLimit");
		int i = 0;
		for (char x : password.toCharArray()) {
			driver.findElement(passwordInSignUpPopUp).sendKeys(String.valueOf(x));
			i = i + 1;
			if (i < 6) {
				Assert.assertTrue(driver.findElement(passwordErrorInSignUpPopUp).getText().equals(errorText));
			} else if (i >= 6) {
				Assert.assertTrue(driver.findElements(passwordErrorInSignUpPopUp).size() == 0);
			}
		}
	}

	public void verifyCharacterLimitforMobile() {
		WebDriver driver = DriverManager.getInstance().getDriver();
		CommonUtils utils = new CommonUtils();
		String mobile = RandomStringUtils.randomNumeric(12);
		String errorTextLower = utils.getProperty("MobileErrorForLomitLower");
		String errorTextHigher = utils.getProperty("MobileErrorForLimitHigher");
		int i = 0;
		for (char x : mobile.toCharArray()) {
			driver.findElement(mobileInSignUpPopUp).sendKeys(String.valueOf(x));
			i = i + 1;
			if (i < 10) {
				Assert.assertTrue(driver.findElement(mobileErrorInSignUpPopUp).getText().equals(errorTextLower));
			} else if (i == 10) {
				Assert.assertTrue(driver.findElements(mobileErrorInSignUpPopUp).size() == 0);
			} else if (i > 10) {
				Assert.assertTrue(driver.findElement(mobileErrorInSignUpPopUp).getText().equals(errorTextHigher));
			}
		}
	}

	public void verifyExistingMemberTextInSignUpPopUp() {
		WebDriver driver = DriverManager.getInstance().getDriver();
		CommonUtils utils = new CommonUtils();
		String text = utils.getProperty("TextForExistingMember");
		Assert.assertTrue(driver.findElement(textForTreeboMember).getText().equals(text));
	}

	public void clickOnLoginLinkInSignUpPopUp() {
		WebDriver driver = DriverManager.getInstance().getDriver();
		driver.findElement(loginLinkInSignUpPopUp).click();
	}

	public void verifyTermsOfServicesLinkInSignUp() {
		WebDriver driver = DriverManager.getInstance().getDriver();
		browserHelper = new BrowserHelper();
		Assert.assertTrue(driver.findElement(termsOfServiceLinkInSignupPopUp).isDisplayed());
		driver.findElement(termsOfServiceLinkInSignupPopUp).click();
		String parentWindow = browserHelper.switchToNewWindow();
		Assert.assertTrue(driver.findElement(By.xpath("//div[@class='termsPage']")).isDisplayed());
		driver.close();
		driver.switchTo().window(parentWindow);
	}

	public void verifyPrivacyPolicyLinkInSignUp() {
		WebDriver driver = DriverManager.getInstance().getDriver();
		browserHelper = new BrowserHelper();
		Assert.assertTrue(driver.findElement(privacyPolicyInSignipPopUp).isDisplayed());
		driver.findElement(privacyPolicyInSignipPopUp).click();
		String parentWindow = browserHelper.switchToNewWindow();
		Assert.assertTrue(driver.findElement(By.xpath("//div[@class='policyPage']")).isDisplayed());
		driver.close();
		driver.switchTo().window(parentWindow);
	}

	public void waitForLoaderToDisappear() {
		WebDriver driver = DriverManager.getInstance().getDriver();
		WebDriverWait wait = new WebDriverWait(driver, 120);
		System.out.println("****waiting for page to load****");
		wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector("div#commonLoader.hide")));
	}

	public Set<String> getAllCityLinkInCarousel() {
		WebDriver driver = DriverManager.getInstance().getDriver();
		browserHelper = new BrowserHelper();
		Set<String> cityNames = new TreeSet<String>();
        
		// to get all name, click next link till existing city name does not come
		while (!(cityNames.contains(driver.findElement(lastCityLink).getText().trim()))) {
			//take the name
			List<WebElement> elements = driver.findElements(By.xpath("//a[contains(@class,'slick-active')]//h3"));
			for (WebElement ele : elements) {
				cityNames.add(ele.getText().trim());
			}
			driver.findElement(nextLinkCityDisplay).click();
			browserHelper.waitTime(1000);
		};
		
		System.out.println("City links :" + cityNames.toString());
		return cityNames;
	}

	public void clickOnCityLinkInCarousel(String city) {
		WebDriver driver = DriverManager.getInstance().getDriver();
		System.out.println("clicking on city link :" + city);
		browserHelper = new BrowserHelper();
		String cityXpath = String.format("//a[contains(@class,'slick-active')]//h3[contains(text(),'%s')]", WordUtils.capitalizeFully(city));
		while (!(driver.findElements(By.xpath(cityXpath)).size() > 0)) {
				driver.findElement(nextLinkCityDisplay).click();
				browserHelper.waitTime(1000);	
		}
		System.out.println("clicking on link: " + cityXpath);
		driver.findElement(By.xpath(cityXpath)).click();
	}
	
	public Set<String> getAllCityLinkInSEO(){
		WebDriver driver = DriverManager.getInstance().getDriver();
		List<WebElement> elements = driver.findElements(By.cssSelector(".footer__seo-hotel-link a"));
		Set<String> seoNames = new HashSet<String>();
		for (WebElement ele : elements) {
			String x = ele.getText().trim();
			seoNames.add(x);
		}
		System.out.println("SEO City links :" + seoNames.toString());
		return seoNames;
	}
	
	public void clickOnSeoLink(String seoLink){
		WebDriver driver = DriverManager.getInstance().getDriver();
		String seoLinkXpath = String.format("//div[contains(@class,'footer__seo-hotel-link')]/a[contains(text(),'%s')]", seoLink);
		System.out.println("clicking on link: " + seoLink);
		driver.findElement(By.xpath(seoLinkXpath)).click();
	}
	
	public boolean is500Displayed(){
		WebDriver driver = DriverManager.getInstance().getDriver();
		return (driver.findElements(By.cssSelector("img[alt='500 page']")).size() > 0);
	}
	//verify checkIn box display
	public boolean isCheckInDisplayed(){
		WebDriver driver = DriverManager.getInstance().getDriver();
		System.out.println("Check Checkin Displayed");
		return driver.findElement(checkin).isDisplayed();
	}
	//click on checkIn
	public void clickOnCheckin(){
		WebDriver driver = DriverManager.getInstance().getDriver();
		System.out.println("Click on CheckIn Date");
		driver.findElement(checkin).click();
	}
	//verify the calender presence
	public boolean isCalendraDisplayed(){
		WebDriver driver = DriverManager.getInstance().getDriver();
		System.out.println("Verified Calender Displayed");
		return driver.findElement(By.cssSelector(".dr-calendar")).isDisplayed();
	}
	//verify left arrow on calender
	public boolean isLeftArrowDisplayed(){
		WebDriver driver = DriverManager.getInstance().getDriver();
		System.out.println("Left Arrow is Displayed");
		return driver.findElement(calendarLeftClick).isDisplayed();
	}
	//click on right arrow on calender
	public void clickonRightArrow(){
		WebDriver driver = DriverManager.getInstance().getDriver();
		System.out.println("Click on Right Arrow");
		driver.findElement(calendarRightClick).click();
	}
	//click on left arrow on calender
	public void clickonLeftArrow(){
		WebDriver driver = DriverManager.getInstance().getDriver();
		System.out.println("Click on Left Arrow");
		driver.findElement(calendarLeftClick).click();
	}
	//extract month from calender 
	public String extractMonthName(){
		WebDriver driver = DriverManager.getInstance().getDriver();
		System.out.println("Extracting the Month Name");
		WebElement element = driver.findElement(monthName);
		String month = element.getText();
		return month;
    }
	//checking the months equality
	public boolean isEqualMonthName(String month1, String month2){
		System.out.println("Compare Current and Next or Previous Month");
	    if(month1.equals(month2))
	    	  return true;
	    return false; 
	   }
	//Join our network link display
	public boolean isJoinOurNetworlLinkDisplayed(){
		WebDriver driver = DriverManager.getInstance().getDriver();
		return driver.findElement(joinOurNetworkLink).isDisplayed();
	}
	public void clickOnJoinUsNetwork(){
		WebDriver driver = DriverManager.getInstance().getDriver();
		driver.findElement(joinOurNetworkLink).click();
	}
	public void registerAtJoinOurNetwork(String contactPersonName, String emailId, String mobileNumber,String hotelName, String hotelAddress1,String hotelAddress2,String pinCode){
		WebDriver driver = DriverManager.getInstance().getDriver();
		driver.findElement(By.xpath("//*[@id='contact-person']")).sendKeys(contactPersonName.trim());
		driver.findElement(By.xpath("//*[@id='email-address']")).sendKeys(emailId.trim());
		driver.findElement(By.xpath("//*[@id='mobile-number']")).sendKeys(mobileNumber);
		driver.findElement(By.xpath("//*[@id='hotel-name']")).sendKeys(hotelName.trim());
		driver.findElement(By.xpath("//*[@id='hotel-address1']")).sendKeys(hotelAddress1.trim());
		driver.findElement(By.xpath("//*[@id='hotel-address2']")).sendKeys(hotelAddress2.trim());

		driver.findElement(By.xpath("//*[@id='pincode']")).sendKeys(pinCode.trim());
		
		driver.findElement(By.xpath("//*[@id='feedbackSubmitForm']")).click();
	}
	public boolean isSuccessfulPopUpdisplayed(){
		WebDriver driver = DriverManager.getInstance().getDriver();
		WebDriverWait wait =new WebDriverWait(driver, 30);
        wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.xpath("//*[@id='toast-container']/div")));
        return driver.findElement(By.xpath("//*[@id='toast-container']/div")).isDisplayed();
   }
	
	
}
