package utils;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentSkipListMap;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import base.DatabaseConnection;

public class DBUtils {
	
	public Map<String, Integer> getRoomAvailability(String date) throws SQLException {

		// DB credentials
		// String server = "172.40.20.210";
		// String databaseName = "ashishmantri";
		// String user = "ashishmantri";
		// String PGPASSWORD = "ashishmantri";

		String server = "treebo-pg-slave.cadmhukcksta.ap-southeast-1.rds.amazonaws.com";
		String databaseName = "treebo";
		String user = "treeboadmin";
		String PGPASSWORD = "caTwimEx3";

		String[] churnedOutHotels = { "Canop Le Trans", "Midaas Comfort", "Alreef Residency", "Hotel Airport Grand",
				"Blossom Studios", "Epic", "Dummy", "Zipotel Silk Board" };

		// String date = "2016-04-30";

		String sql = String.format(
				"SELECT hotelogix_name,room_type,availablerooms FROM bookingstash_availability INNER JOIN hotels_room ON bookingstash_availability.room_id = hotels_room.id and bookingstash_availability.date = '%s' INNER JOIN hotels_hotel ON hotels_hotel.id = hotel_id where hotels_hotel.status = '1' order by hotelogix_name asc",
				date);

		DatabaseConnection dc = new DatabaseConnection(server, databaseName, user, PGPASSWORD);
		Iterator itr = dc.executeQuery(sql).iterator();
		Map<String, Integer> dbRoomAvailability = new ConcurrentSkipListMap<String, Integer>();
		while (itr.hasNext()) {
			String[] strArr = itr.next().toString().split(",");
			String hotelName = strArr[0].replaceAll("\\[", "").trim();
			if (!stringContainsItemFromArray(hotelName, churnedOutHotels)) {
				String roomType = strArr[1].trim();
				int available = Integer.parseInt(strArr[2].replaceAll("\\]", "").trim());
				dbRoomAvailability.put(hotelName + "_" + roomType, available);
			}
		}
		// System.out.println(dbRoomAvailability.keySet().toString());
		dc.close();
		// System.out.println(
		// "********************************************** DB AVAILABILITY
		// **********************************************************");
		// for (String key : dbRoomAvailability.keySet()) {
		// System.out.println(key + " " + dbRoomAvailability.get(key));
		// }
		// System.out.println(
		// "********************************************** DB AVAILABILITY
		// **********************************************************");
		return dbRoomAvailability;
	}
	
	
	public ArrayList<String> getActiveHotelId() throws SQLException {
		// DB to connect
		String server = "treebo-pg-slave.cadmhukcksta.ap-southeast-1.rds.amazonaws.com";
		String databaseName = "treebo";
		String user = "treeboadmin";
		String PGPASSWORD = "caTwimEx3";

		String sql = "select hotelogix_id from hotels_hotel where status = '1';" ;
				
		DatabaseConnection dc = new DatabaseConnection(server, databaseName, user, PGPASSWORD);
		Iterator itr = dc.executeQuery(sql).iterator();
		
		ArrayList<String> activeHotelId = new ArrayList<String>();
		while (itr.hasNext()) {
			String hotelId = itr.next().toString();
			activeHotelId.add(hotelId);
			}
		
		return activeHotelId;
		}
	

	public Map<String, Integer> getRoomAvailabilityByHotel(String strHotelName, String date) throws SQLException {

		// DB credentials
		// String server = "172.40.20.210";
		// String databaseName = "ashishmantri";
		// String user = "ashishmantri";
		// String PGPASSWORD = "ashishmantri";

		String server = "treebo-pg-slave.cadmhukcksta.ap-southeast-1.rds.amazonaws.com";
		String databaseName = "treebo";
		String user = "treeboadmin";
		String PGPASSWORD = "caTwimEx3";

		String[] churnedOutHotels = { "Canop Le Trans", "Midaas Comfort", "Alreef Residency", "Hotel Airport Grand",
				"Blossom Studios", "Epic", "Dummy", "Zipotel Silk Board" };

		// String date = "2016-04-30";

		String sql = String.format(
				"SELECT hotelogix_name,room_type,availablerooms FROM bookingstash_availability INNER JOIN hotels_room ON bookingstash_availability.room_id = hotels_room.id and bookingstash_availability.date = '%s' INNER JOIN hotels_hotel ON hotels_hotel.id = hotel_id order by hotelogix_name asc",
				date);

		DatabaseConnection dc = new DatabaseConnection(server, databaseName, user, PGPASSWORD);
		Iterator itr = dc.executeQuery(sql).iterator();
		Map<String, Integer> dbRoomAvailability = new ConcurrentSkipListMap<String, Integer>();
		while (itr.hasNext()) {
			String[] strArr = itr.next().toString().split(",");
			String hotelName = strArr[0].replaceAll("\\[", "").trim();
			if ((hotelName.toLowerCase()).contains(strHotelName.toLowerCase())) {
				if (!stringContainsItemFromArray(hotelName, churnedOutHotels)) {
					String roomType = strArr[1].trim();
					int available = Integer.parseInt(strArr[2].replaceAll("\\]", "").trim());
					dbRoomAvailability.put(hotelName + "_" + roomType, available);
				}
			}
		}
		// System.out.println(dbRoomAvailability.keySet().toString());
		dc.close();
		// System.out.println(
		// "********************************************** DB AVAILABILITY
		// **********************************************************");
		// for (String key : dbRoomAvailability.keySet()) {
		// System.out.println(key + " " + dbRoomAvailability.get(key));
		// }
		// System.out.println(
		// "********************************************** DB AVAILABILITY
		// **********************************************************");
		return dbRoomAvailability;
	}

	public ConcurrentHashMap<String,String> getOrderIdAndReservationId(String email,String date) throws SQLException {		
		String server = "treebo-pg-slave.cadmhukcksta.ap-southeast-1.rds.amazonaws.com";
		String databaseName = "treebo";
		String user = "treeboadmin";
		String PGPASSWORD = "caTwimEx3";
		
		String name = System.getProperty("email");
		
		String tracking = System.getProperty("tracking");
		if(tracking.isEmpty()){
			tracking = "created_at";
		}
		
		
		String sql = String.format(
				"select bookings_booking.order_id,bookings_roombooking.reservation_id from bookings_booking INNER JOIN bookings_roombooking ON bookings_booking.id=bookings_roombooking.booking_id where Lower(bookings_booking.guest_name) like '%%%s%%' AND bookings_booking.booking_status = 'Confirm' AND bookings_booking.%s >= '%s' ORDER By bookings_booking.created_at DESC;",name,tracking,date);
		DatabaseConnection dc = new DatabaseConnection(server, databaseName, user, PGPASSWORD);
		System.out.println("Executing the sql query : " + sql);
		Iterator itr = dc.executeQuery(sql).iterator();
		ConcurrentHashMap<String,String> orderAndReservationId = new ConcurrentHashMap<String,String>();
		while (itr.hasNext()) {
			String order[] = itr.next().toString().replaceAll("\\[", "").replaceAll("\\]", "").split(",");
			orderAndReservationId.put(order[1].trim(), order[0].trim());
		}
		dc.close();
		System.out.println("Number of bookings to be canceled is :" + orderAndReservationId.size());
		System.out.println("Number of bookings to be canceled details are :" + orderAndReservationId.toString());
		System.out.println("\n");
		return orderAndReservationId;
	}
	
	
	public ConcurrentHashMap<Integer,ConcurrentHashMap<String,String>> getOBDNRBookingsCurrentDate() throws SQLException {		
		String server = "rds-replica-hmssync.cadmhukcksta.ap-southeast-1.rds.amazonaws.com";
		String databaseName = "hmssync";
		String user = "hmssync";
		String PGPASSWORD = "arHunNuj5";
		
		DateTime dt = new DateTime();
		DateTimeFormatter dtf = DateTimeFormat.forPattern("yyyy-MM-dd");
		String date = dtf.print(dt.plusDays(0));
		//System.out.println(date);
		
		String sql = String.format(
				"select hotel_code,group_code,booking_id,checkin_date,checkout_date from bookingstash_reservationbooking where group_first_name like '%%OBDNR%%' and checkin_date = '%s' and status not in ('BLOCKED','CANCEL');",date);
		
		DatabaseConnection dc = new DatabaseConnection(server, databaseName, user, PGPASSWORD);
		System.out.println("Executing the sql query : " + sql);
		Iterator itr = dc.executeQuery(sql).iterator();
		
		ConcurrentHashMap<Integer,ConcurrentHashMap<String,String>> bookingRecords = new ConcurrentHashMap<Integer,ConcurrentHashMap<String,String>>();
		int count = 1;
		
		while (itr.hasNext()) {
			String[] booking = itr.next().toString().replaceAll("\\[", "").replaceAll("\\]", "").split(",");
			ConcurrentHashMap<String,String> tempData = new ConcurrentHashMap<String,String>();
			tempData.put("HotelCode", booking[0].trim());
			tempData.put("GroupCode", booking[1].trim());
			tempData.put("BookingId", booking[2].trim());
			tempData.put("CheckinDate", booking[3].trim());
			tempData.put("CheckoutDate", booking[4].trim());
			bookingRecords.put(count, tempData);
			count += 1;
		}
		dc.close();
		
		System.out.println("Number of bookings are : " + bookingRecords.size());
        //System.out.println(bookingRecords.toString());
		return bookingRecords;
	}
	
	
	/**
	 * Returns 
	 * id,order_id,checkin_date,checkout_date,adult_count,child_count
	 * total_amount,guest_name,guest_email,guest_mobile,payment_amount
	 * comments, 
	 * coupon_apply,coupon_code,discount,payment_mode
	 * status
	 * pretax_amount,tax_amount
	 * channel
	 * 
	 * @param bookingId
	 * @param hotelId
	 * @return
	 * @throws SQLException
	 */
	
	public Map<String,String> getBookingInfo(String bookingId, String hotelId) throws SQLException{
		String server = "treebo-pg-slave.cadmhukcksta.ap-southeast-1.rds.amazonaws.com";
		String databaseName = "treebo";
		String user = "treeboadmin";
		String PGPASSWORD = "caTwimEx3";
		
		String sql = null;
		String sqlComment = null;
		String spReq = null;
		if (bookingId.contains("G")){
			sql = String.format("select * from bookings_booking where group_code='%s' AND hotel_id='%s';",bookingId,hotelId);
			sqlComment = String.format("select comments from bookings_booking where group_code='%s' AND hotel_id='%s';",bookingId,hotelId);
		}else if(bookingId.contains("TRB")){
			sql = String.format("select * from bookings_booking where order_id='%s' AND hotel_id='%s';",bookingId,hotelId);
			sqlComment = String.format("select comments from bookings_booking where order_id ='%s' AND hotel_id='%s';",bookingId,hotelId);
		}else{
			sql = String.format("select * from bookings_booking where booking_code='%s|' AND hotel_id='%s';",bookingId,hotelId);
			sqlComment = String.format("select comments from bookings_booking where booking_code='%s|' AND hotel_id='%s';",bookingId,hotelId);
		}
		
		DatabaseConnection dc = new DatabaseConnection(server, databaseName, user, PGPASSWORD);
		Iterator itr = dc.executeQuery(sql).iterator();
		Iterator itr1 = dc.executeQuery(sqlComment).iterator();
		while (itr1.hasNext()) {
			String spReqStr = itr1.next().toString();
			spReq = spReqStr.substring(1, spReqStr.length() - 1);
		}
		
		Map<String,String> orderInfoInDB = new HashMap<String,String>();
		while (itr.hasNext()) {
			String[] order = itr.next().toString().replace(spReq,"").replaceAll("\\[", "").replaceAll("\\]", "").split(",");
			orderInfoInDB.put("id", order[0].trim());
			orderInfoInDB.put("order_id", order[3].trim());
			orderInfoInDB.put("checkin_date", order[4].trim());
			orderInfoInDB.put("checkout_date", order[5].trim());
			orderInfoInDB.put("adult_count", order[6].trim());
			orderInfoInDB.put("child_count", order[7].trim());
			orderInfoInDB.put("total_amount", order[8].trim());
			orderInfoInDB.put("guest_name", order[9].trim());
			orderInfoInDB.put("guest_email", order[10].trim());
			orderInfoInDB.put("guest_mobile", order[11].trim());
			orderInfoInDB.put("payment_amount", order[17].trim());
			orderInfoInDB.put("comments", spReq.trim());
			orderInfoInDB.put("coupon_apply", order[20].trim());
			orderInfoInDB.put("coupon_code", order[21].trim());
			orderInfoInDB.put("discount", order[22].trim());
			orderInfoInDB.put("payment_mode", order[23].trim());
			orderInfoInDB.put("status", order[25].trim());
			orderInfoInDB.put("pretax_amount", order[27].trim());
			orderInfoInDB.put("tax_amount", order[28].trim());
			orderInfoInDB.put("channel", order[30].trim());
			orderInfoInDB.put("is_audit", order[31].trim());
		}
		dc.close();
	    System.out.println(orderInfoInDB.toString());
		return orderInfoInDB;
	}

	public boolean stringContainsItemFromArray(String inputString, String[] items) {
		for (int i = 0; i < items.length; i++) {
			if (inputString.contains(items[i])) {
				return true;
			}
		}
		return false;
	}
	
	public String getOTP(String mobile) throws SQLException {		
		String server = "treebo-pg-slave.cadmhukcksta.ap-southeast-1.rds.amazonaws.com";
		String databaseName = "treebo";
		String user = "treeboadmin";
		String PGPASSWORD = "caTwimEx3";
		String otp = null;
		
		String sql = String.format(
				"select otp from dbcommon_otp where mobile like '%s' ORDER BY id DESC limit 1;",mobile);
		DatabaseConnection dc = new DatabaseConnection(server, databaseName, user, PGPASSWORD);
		System.out.println("Executing the sql query : " + sql);
		Iterator itr = dc.executeQuery(sql).iterator();

		while (itr.hasNext()) {
			otp = itr.next().toString().replaceAll("\\[", "").replaceAll("\\]", "");
		}
		dc.close();

		System.out.println("OTP for mobile number: " + mobile + " is " + otp);
		return otp;
	}
	
	
	public boolean getOtpVerifiedStatus(String email) throws SQLException {		
		String server = "treebo-pg-slave.cadmhukcksta.ap-southeast-1.rds.amazonaws.com";
		String databaseName = "treebo";
		String user = "treeboadmin";
		String PGPASSWORD = "caTwimEx3";

		String status = null;
		boolean flag = false;
		
		String sql = String.format(
				"select is_otp_verified from profiles_user where email like '%s';",email);
		DatabaseConnection dc = new DatabaseConnection(server, databaseName, user, PGPASSWORD);
		System.out.println("Executing the sql query : " + sql);
		Iterator itr = dc.executeQuery(sql).iterator();

		while (itr.hasNext()) {
			status = itr.next().toString().replaceAll("\\[", "").replaceAll("\\]", "");
		}
		dc.close();
		
		if (status.equals("true"))
		{
			flag = true;
			System.out.println("correct");
		}
		else if(status.equals("false"))
		{
			flag = false;
			System.out.println("Incorrect");
		}
		
		System.out.println("OTP Verified status for email " + email + " is : " + flag);
		
		return flag;
	}

}
