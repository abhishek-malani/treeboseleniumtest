package desktopUITest;

import java.text.ParseException;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;
import utils.CommonUtils;
import base.BrowserHelper;
import base.TestBaseSetUp;
import desktopUIPageObjects.HomePage;
import desktopUIPageObjects.Account;

public class AccountTest extends TestBaseSetUp {

	private HomePage homePage;
    private Account account;
    private BrowserHelper browserHelper;
	
	/**
	 * This test verifies that profile fields can be updated
	 */
	@Test(groups = { "Sanity", "Account" })
	public void testProfileFieldUpdates() throws ParseException {
		homePage = new HomePage();
        account = new Account();
		CommonUtils utils = new CommonUtils();
		SoftAssert verify = new SoftAssert();
		
		String guestEmail = utils.getProperty("TestAccountLogin"); // """";
		String guestPassword = utils.getProperty("TestLoginPassword"); // "password";
		
		String firstNameToUpdate = "UpdatedTest";
		String lastNameToUpdate = "UpdatedLast";
		String mobileToUpdate = "5000000000";
		
		// Home Page is Displayed
		homePage.verifyHomePagePresence();
		homePage.signInAsTreeboMember(guestEmail, guestPassword);
		
		//go to profile page
		account.goToProfilePage();
		
		// Update Name, last name and Mobile
		//Get current values
		String origFirstName = account.getProfileFieldValue("first_name");
		String origLastName =  account.getProfileFieldValue("last_name");
		String origMobile = account.getProfileFieldValue("phone");
		
		//Now Update values
		account.updateNameAndMobile(firstNameToUpdate, lastNameToUpdate, mobileToUpdate);
		
		// Go to Home Page and come back to check new data
		homePage.goToHomePage();
		
		//Go to profile 
		account.goToProfilePage();
		//Get updated values
		String updatedFirstName = account.getProfileFieldValue("first_name");
		String updatedLastName =  account.getProfileFieldValue("last_name");
		String updatedMobile = account.getProfileFieldValue("phone");
		
		//Now assert
		verify.assertEquals(updatedFirstName, firstNameToUpdate);
		verify.assertEquals(updatedLastName, lastNameToUpdate);
		verify.assertEquals(updatedMobile, mobileToUpdate);
		
		// Sign out and sign in and then verify new data persists
		account.doSignOut();
		homePage.signInAsTreeboMember(guestEmail, guestPassword);
		account.goToProfilePage();
		
		//Get updated values
		String updatedFirstName1 = account.getProfileFieldValue("first_name");
		String updatedLastName1 =  account.getProfileFieldValue("last_name");
		String updatedMobile1 = account.getProfileFieldValue("phone");
		
		//Now assert
		verify.assertEquals(updatedFirstName1, firstNameToUpdate);
		verify.assertEquals(updatedLastName1, lastNameToUpdate);
		verify.assertEquals(updatedMobile1, mobileToUpdate);
		
		//reset
		account.updateNameAndMobile(origFirstName, origLastName, origMobile);
		homePage.goToHomePage();
		account.goToProfilePage();
		String updatedFirstName2 = account.getProfileFieldValue("first_name");
		//Now assert
		verify.assertEquals(updatedFirstName2, origFirstName);
		verify.assertAll();// finally assert All
	}
	
	/**
	 * This test verifies password can be reset 
	 */
	@Test(groups = { "Sanity", "Account" })
	public void testAccountPaaswordReset() throws ParseException {
		homePage = new HomePage();
        account = new Account();
        browserHelper = new BrowserHelper();
		CommonUtils utils = new CommonUtils();

		String guestEmail = utils.getProperty("TestAccountPwdReset"); // """";
		String guestPassword = utils.getProperty("TestLoginPassword"); // "password";
		String newPassword = "password1";

		// Home Page is Displayed
		homePage.verifyHomePagePresence();
		try{
			homePage.signInAsTreeboMember(guestEmail, guestPassword);
			// Go to Settings page
			account.goToSettingsPage();
			account.changePassword(guestPassword, newPassword);
			
			account.doSignOut();
			
			//Now check if can login with new password
			homePage.signInAsTreeboMember(guestEmail, newPassword);
			
			// Now reset the password to original 
			account.goToSettingsPage();
			account.changePassword(newPassword,guestPassword);
			account.doSignOut();
			homePage.signInAsTreeboMember(guestEmail, guestPassword);
		}catch(Exception e){
			browserHelper.browserRefresh();
			guestPassword = "password1";
			newPassword = utils.getProperty("TestLoginPassword");
			homePage.signInAsTreeboMember(guestEmail, guestPassword);
			// Go to Settings page
			account.goToSettingsPage();
			account.changePassword(guestPassword, newPassword);
			
			account.doSignOut();
			
			//Now check if can login with new password
			homePage.signInAsTreeboMember(guestEmail, newPassword);
			
			// Now reset the password to original 
			account.goToSettingsPage();
			account.changePassword(newPassword,guestPassword);
			account.doSignOut();
			homePage.signInAsTreeboMember(guestEmail, guestPassword);
		}
	}
	
	
	/*
	 * Open HomePage
	 * go to myAccount information
	 * then click on MyReferrals
	 * then checking MyReferrals link is displayed
	 * checking the transparent link is displayed
	 * checking copy button is displayed
	 * checkin the facebook and gmail link is displayed
	 * checking the final text SignedUp and Converted is displayed
	 */
	@Test(groups = { "Sanity", "Account" })
	public void testMyAccount(){
		homePage = new HomePage();
        account = new Account();
		CommonUtils utils = new CommonUtils();
		SoftAssert verify = new SoftAssert();
		
		String guestEmail = utils.getProperty("TestAccountLogin"); // """";
		String guestPassword = utils.getProperty("TestLoginPassword"); // "password";
		
		//homepage verified
		homePage.verifyHomePagePresence();
		//login with treebo credentials
		homePage.signInAsTreeboMember(guestEmail, guestPassword);
		
		account.goToMyAccountPage();
		//click on referrals
		account.clickOnMyReferrals();
		//My Referrals link verified
		verify.assertTrue(account.isMyReferralsDisplayed(),"My Referrals link is not displayed");
		//Transparent text displayed
		verify.assertTrue(account.isTransparentTextDisplayed(),"Transparent text is not displayed");
		//copy button displayed
		verify.assertTrue(account.doesCopyButtonDisplayed(),"Copy Button is not displayed");
		//facebook login link displayed
		verify.assertTrue(account.doesFacebookLinkDisplayed(),"Facebook link is not displayed");
		//gmail login link displayed
		verify.assertTrue(account.doesGmailLinkDisplayed(),"Gmail link is not displayed");
		//SignedUp and Converted text displayed
		verify.assertTrue(account.doesSignedUpandConverteDisplayed(),"SignedUp and Converted Text line is not displayed");
		
		verify.assertAll();
	}
}
