package desktopUITest;

import java.text.ParseException;

import org.testng.Assert;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;
import utils.CommonUtils;
import base.BrowserHelper;
import base.TestBaseSetUp;
import desktopUIPageObjects.FOTPage;
import desktopUIPageObjects.HomePage;


public class FOTTest extends TestBaseSetUp {

	private HomePage homePage;
	private FOTPage fotPage;
    private BrowserHelper browserHelper;
	
	/**
	 * FOT Registration Test - Successful
	 */
	@Test(groups = {"FOT"},enabled =false)
	public void testFOTRegistration() throws ParseException {
		homePage = new HomePage();
		fotPage = new FOTPage();

		// Home Page is Displayed
		homePage.verifyHomePagePresence();
        fotPage.openFOTPage();
        fotPage.fotSignUp();
        Assert.assertTrue(fotPage.isScheduleFirstAuditDisplayed(),"Schedule first audit is not displayed");
	}
		
	/**
	 * FOT Rejected User - Home Page
	 * - Book a treebo link is displayed
	 * - Clicking on book a treebo link opens treebo landing page
	 */
	@Test(groups = { "FOT"})
	public void testFailedFOTUserLogin() throws ParseException {
		homePage = new HomePage();
		fotPage = new FOTPage();
		CommonUtils utils = new CommonUtils();
		SoftAssert verify = new SoftAssert();
		
		String failedUserEmail = utils.getProperty("FOTRejectedUser"); 
		String password = utils.getProperty("TestLoginPassword");

		// Home Page is Displayed
		homePage.verifyHomePagePresence();
        fotPage.openFOTPage();
        fotPage.loginToFOT(failedUserEmail, password);
        verify.assertTrue(fotPage.doesBookATreeboDisplaysForFailedFOT(),"Book A Treebo was not displayed!");
        verify.assertTrue(fotPage.doesClickBookATreeboForFailedFOTOpensLandingPage(),"Treebo Landing page was not displayed!");
        verify.assertAll();
	}	
	
	/**
	 * Existing FOT User - 
	 * - Home Page - Schedule Audit link
	 * - Default search - Bangalore for next day
	 */
	@Test(groups = { "FOT"})
	public void testExistingFOTUserLogin() throws ParseException {
		homePage = new HomePage();
		fotPage = new FOTPage();
		CommonUtils utils = new CommonUtils();
		String email = utils.getProperty("FOTlogin"); 
		String password = utils.getProperty("TestLoginPassword");  
		String location = utils.getProperty("city1"); 
		int fotAuditDaysAllowed = Integer.parseInt(utils.getProperty("FOTAuditDaysAllowed")); 

		// Home Page is Displayed
		homePage.verifyHomePagePresence();
        fotPage.openFOTPage();
        Assert.assertTrue(fotPage.isLoginDisplayedOnFOTHomePage(),"Login did not displayed on FOT home page");
        Assert.assertTrue(fotPage.isJoinUSDisplayedOnFOTHomePage(),"Join us did not displayed on FOT home page");
        fotPage.loginToFOT(email, password);
        Assert.assertTrue(fotPage.isScheduleAuditDisplayed(),"Schedule Audit link missing!");
        fotPage.clickScheduleAudit();
        Assert.assertTrue(fotPage.isFOTSearchPageDisplayed(),"FOT search page is not dispalyed");
        Assert.assertTrue(fotPage.isDefaultSearchAsExpected(),"FOT default search does not do default search for next day");
        Assert.assertEquals(fotPage.getAvailableAuditDates(), fotAuditDaysAllowed,"Number of audit days available is not equal to: "+ fotAuditDaysAllowed);
	}
	
	/**
	 * Existing FOT User - 
	 * - Schedule first Audit
	 * - Verify second audit cannot be scheduled on same date
	 * - verify second audit cannot be scheduled in same hotel
	 * - Verify in Account page - FOT Audit label comes for the booking created
	 */
	@Test(groups = { "FOT"})
	public void testExistingFOTUserAuditEndToEndFlow() throws ParseException {
		homePage = new HomePage();
		fotPage = new FOTPage();
		CommonUtils utils = new CommonUtils(); 
		String location = utils.getProperty("city1"); 
		String locationOne = utils.getProperty("city"); 
		int guestCount = 1;
		int auditFromCurrentDate = utils.getRandomNumber(15,30);
		SoftAssert verify = new SoftAssert();

		// Home Page is Displayed
		homePage.verifyHomePagePresence();
        fotPage.openFOTPage();
        String[] guestDetails = fotPage.fotSignUp();
		String email = guestDetails[1];
		String password = guestDetails[3]; 
        Assert.assertTrue(fotPage.isScheduleFirstAuditDisplayed(),"Schedule first audit is not displayed");
        fotPage.clickScheduleFirstAudit();
        homePage.goToHomePage();
        homePage.clickOnSignOutLink();
        
        // 
		homePage.verifyHomePagePresence();
        fotPage.openFOTPage();
        fotPage.loginToFOT(email, password);
        Assert.assertTrue(fotPage.isScheduleAuditDisplayed(),"Schedule Audit link missing!");
        fotPage.clickScheduleAudit();
        fotPage.doFOTSearch(location, auditFromCurrentDate, guestCount);
        verify.assertTrue(fotPage.isFOTSearchPageDisplayed(),"FOT Search page is not displayed!");
        
        // Schedule first audit
        // Get hotel index to do audit
        int indexFirstAudit = fotPage.getHotelIndexByAvailableForAudit();
        String hotelNameFirstAudit = fotPage.getHotelNameByIndex(indexFirstAudit);
        
        // Schedule first audit
        fotPage.scheduleHotelAudit(hotelNameFirstAudit);
        verify.assertTrue(fotPage.isConfirmAuditDisplayed(),"Confirm audit is not displayed");
        fotPage.confirmAudit();
        
        //
        verify.assertTrue(fotPage.isScheduleAuditSuccessMessageDisplayed(),"Error happened instead of successfult audit message");
        verify.assertTrue(fotPage.isBookAnotherAuditIsDisplayed(),"Book Another audit is not displayed");
        
        // Click book Another audit
        fotPage.clickOnBookAnotherAudit();
        verify.assertTrue(fotPage.doesErrorMessageForSameDayAuditDisplayed(),"Same day audit error message not displayed!");
        verify.assertTrue(fotPage.doesCloseAllAuditDisplayForAllHotelsOtherThanOne(),"Issue with Close Audit");
        verify.assertTrue(fotPage.doesCancelAuditForAlreadyScheduleAuditDisplayed(hotelNameFirstAudit),"Cancel Audit not displayed");
        
        verify.assertTrue(fotPage.doesHereLinkTakesToAccount(),"Here Link takes to audit page");
        
        // Sign out 
        homePage.goToHomePage();
        homePage.clickOnSignOutLink();
        
        // Book another audit by searching next date
		homePage.verifyHomePagePresence();
        fotPage.openFOTPage();
        fotPage.loginToFOT(email, password);
        Assert.assertTrue(fotPage.isScheduleAuditDisplayed(),"Schedule Audit link missing!");
        fotPage.clickScheduleAudit();
        fotPage.doFOTSearch(location, auditFromCurrentDate+1, guestCount);
        
        // Sign out 
        homePage.goToHomePage();
        homePage.clickOnSignOutLink();
        
        // Book another audit by searching next date
		homePage.verifyHomePagePresence();
        fotPage.openFOTPage();
        fotPage.loginToFOT(email, password);
        Assert.assertTrue(fotPage.isScheduleAuditDisplayed(),"Schedule Audit link missing!");
        fotPage.clickScheduleAudit();
        
        // Search for another city
        fotPage.doFOTSearch(locationOne, auditFromCurrentDate, guestCount);
        verify.assertTrue(fotPage.doesCloseAllAuditDisplayForAllHotelsInDifferentCity(),"Some hotels are available for audit in another city on same date");
        
        // Sign out 
        homePage.goToHomePage();
        homePage.clickOnSignOutLink();
        
        // Book another audit by searching next date
		homePage.verifyHomePagePresence();
        fotPage.openFOTPage();
        fotPage.loginToFOT(email, password);
        Assert.assertTrue(fotPage.isScheduleAuditDisplayed(),"Schedule Audit link missing!");
        fotPage.clickScheduleAudit();
        fotPage.doFOTSearch(location, auditFromCurrentDate+1, guestCount);
        
        // Schedule second audit
        // Get hotel index to do audit
        int indexSecondAudit = fotPage.getHotelIndexByAvailableForAudit();
        String hotelNameSecondAudit = fotPage.getHotelNameByIndex(indexSecondAudit);
        fotPage.scheduleHotelAudit(hotelNameSecondAudit);
        verify.assertTrue(fotPage.isConfirmAuditDisplayed(),"Confirm audit is not displayed");
        fotPage.confirmAudit();
        
        // 
        verify.assertTrue(fotPage.doesSecondAuditSuccessMsgDisplayedWithLimitMentioned(),"Message is not correct after 2nd Audit");
        verify.assertTrue(fotPage.doesSecondAuditSucessPopUpDisplaysExpectedLinks(),"Book A trrebo and Back to search link not displayed");
        
        verify.assertTrue(fotPage.doesClickBookATreeboOpensTreeboLandingPage(),"Issue with Book A Treebo link");
        fotPage.clickBackToSearch();
        Assert.assertTrue(fotPage.isDefaultSearchAsExpected(),"FOT default search does not do default search for next day");

        //
        fotPage.doFOTSearch(location, auditFromCurrentDate, guestCount);
        verify.assertTrue(fotPage.doesLimitMessageDisplayedAfterTwoAuditScheduled(),"Message is not matching");
        verify.assertTrue(fotPage.doesCancelAuditForAlreadyScheduleAuditDisplayed(hotelNameSecondAudit),"Cancel Audit not displayed");
        
        fotPage.clickCancelAuditForAlreadyScheduleAudit(hotelNameFirstAudit);
        verify.assertTrue(fotPage.doesCancelAuditSuccessMessageDisplayed(),"Issue with cancel Audit message");
        fotPage.clickYesForBookAnotherAuditInCanceledAuditPopUp();
        
        // Now cancel the second audit
        fotPage.clickCancelAuditForAlreadyScheduleAudit(hotelNameSecondAudit);
        verify.assertTrue(fotPage.doesCancelAuditSuccessMessageDisplayed(),"Issue with cancel Audit message");
        fotPage.clickYesForBookAnotherAuditInCanceledAuditPopUp();
        verify.assertAll();
	}	
}
