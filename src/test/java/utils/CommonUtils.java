package utils;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Date;
import java.util.Properties;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Random;
import java.util.concurrent.TimeUnit;

import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.testng.Assert;

public class CommonUtils {

	public String getProperty(String key) {
		Properties prop = new Properties();
		
		// InputStream in =
		// CommonUtils.class.getResourceAsStream("/testData.properties");

		try {
			InputStream in = Thread.currentThread().getContextClassLoader().getResourceAsStream("cfgdata.properties");
			prop.load(in);
		} catch (Exception e) {
			e.printStackTrace();
		}
		String value = prop.getProperty(key);
		System.out.println("property value of :" + key + " is :" + value );
		return value;
	}

	public String getConfigProperty(String key) throws FileNotFoundException, IOException {
		Properties prop = new Properties();
		InputStream in = getClass().getResourceAsStream("/testData/config.properties");
		prop.load(in);
		return prop.getProperty(key);
	}

	public String[] getNightAuditAndAccessToken() {
		String[] nightAuditAndAccessToken = {};

		return nightAuditAndAccessToken;
	}

	public void getRequest() throws ClientProtocolException, IOException {
		CloseableHttpClient httpclient = HttpClients.createDefault();
		HttpGet httpget = new HttpGet("http://localhost/");
		CloseableHttpResponse response = httpclient.execute(httpget);
		response.close();
	}

	// String originalDateString = "6/16/2016";
	// returns 6 Jun 2016
	public String formatDate(String originalDateString) throws ParseException {
		SimpleDateFormat sourceFormat = new SimpleDateFormat("MM/dd/yyyy");
		Date date = sourceFormat.parse(originalDateString);
		
		SimpleDateFormat destinationFormat = new SimpleDateFormat("d MMM''yy");
		String modifiedDateFormat = destinationFormat.format(date);
		String[] arr = modifiedDateFormat.split(" ");
		String suffix = getDayNumberSuffix(Integer.parseInt(arr[0]));
		String finalDateFormat = arr[0] + suffix + " " + arr[1];
		System.out.println("Formatted date is:" + finalDateFormat.toString());
		return finalDateFormat;
	}
	
	// String originalDateString = "6/16/2016";
	// returns 2016-06-16
	public String formatDateOne(String originalDateString) throws ParseException {
		SimpleDateFormat sourceFormat = new SimpleDateFormat("MM/dd/yyyy");
		Date date = sourceFormat.parse(originalDateString);
		
		SimpleDateFormat destinationFormat = new SimpleDateFormat("yyyy-MM-dd");
		String modifiedDateFormat = destinationFormat.format(date);
		System.out.println("Formatted date is:" + modifiedDateFormat.toString());
		return modifiedDateFormat;
	}
	
	private String getDayNumberSuffix(int day) {
	    if (day >= 11 && day <= 13) {
	        return "TH";
	    }
	    switch (day % 10) {
	    case 1:
	        return "ST";
	    case 2:
	        return "ND";
	    case 3:
	        return "RD";
	    default:
	        return "TH";
	    }
	}	
	public int getDaysDiffFromSelectedDateToCurrentDate(String dateFormat){
		SimpleDateFormat sourceFormat = new SimpleDateFormat("MM/dd/yyyy");
		String currentDate = sourceFormat.format(new Date());
		int days = 0;
		try {
			   Date date2 = sourceFormat.parse(dateFormat);
		    Date date1 = sourceFormat.parse(currentDate);
		    long diff = date2.getTime() - date1.getTime();
		    days = (int) TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS);
		} catch (ParseException e) {
		    e.printStackTrace();
		}
		return days;
	}
	
	
	public int getRandomNumber(int min, int max) {
		Random rand = new Random();
		return rand.nextInt((max - min) + 1) + min;
	}

	public String formatDateWithoutSuffix(String originalDateString) throws ParseException {
		SimpleDateFormat sourceFormat = new SimpleDateFormat("MM/dd/yyyy");
		Date date = sourceFormat.parse(originalDateString);
		
		SimpleDateFormat destinationFormat = new SimpleDateFormat("dd MMM''yy");
		String modifiedDateFormat = destinationFormat.format(date);
		String[] arr = modifiedDateFormat.split(" ");
		//String suffix = getDayNumberSuffix(Integer.parseInt(arr[0]));
		String finalDateFormat = arr[0]  + " " + arr[1];
		System.out.println(finalDateFormat.toString());
		return finalDateFormat;
	}
}
