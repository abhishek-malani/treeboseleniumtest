package desktopUIPageObjects;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.lang3.ArrayUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import utils.CommonUtils;
import base.BrowserHelper;
import base.DriverManager;

public class HotelResultsPage {
	private BrowserHelper browserHelper;
	private HomePage homePage;

	private By hotelResultsPage = By.xpath("//div[@class='result-page']");
	private By destinationInHotelResultsPage = By.xpath("//input[@id='searchInput']");
	private By searchButton = By.xpath("//button[@id='searchSubmitBtn']");
	private By homeInHotelResultsBreadcrumb = By.xpath("//div[contains(@class,'result-page__breadcrumb')]//ul/li[1]/a");
	private By cityInHotelResultsBreadcrumb = By.xpath("//div[contains(@class,'result-page__breadcrumb')]//ul/li[2]/a");
	private By localityInHotelResultsBreadcrumb = By
			.xpath("//div[contains(@class,'result-page__breadcrumb')]//ul/li[3]/a");

	private By firstHotelResult = By.xpath(
			"//div[@class='results']//div[contains(@class,'results__row')][@style='order: 0;' or @style='order:0']");
	private By firstHotelName = By.xpath(
			"//div[@class='results']//div[contains(@class,'results__row')][@style='order: 0;' or @style='order:0']//div[@class='hotel__name']/a");
	private By firstHotelAddress = By.xpath(
			"//div[@class='results']//div[contains(@class,'results__row')][@style='order: 0;' or @style='order:0']//div[contains(@class,'hotel__address')]/div");
	private By firstHotelRoomRate = By.xpath(
			"//div[@class='results']//div[contains(@class,'results__row')][@style='order: 0;' or @style='order:0']//div[contains(@class,'hotel__price')]/div");
	private By firstHotelQuickBookLink = By.xpath(
			"//div[@class='results']//div[contains(@class,'results__row')][@style='order: 0;' or @style='order:0']//a[contains(@class,'analytics-quickbook')]");
	private By firstHotelViewDetailsLink = By.xpath(
			"//div[@class='results']//div[contains(@class,'results__row')][@style='order: 0;' or @style='order:0']//a[contains(@class,'analytics-viewdetails')]");
	private By showOnlyAvailableCheckBox = By.xpath("//input[@name='showOnlyAvailable']");
	private By noResults = By.xpath("//div[@class='no-result__title']");
	private By quickBookLink = By.xpath(
			"//div[@class='results']//div[contains(@class,'results__row')]//a[contains(@class,'analytics-quickbook')]");
	private By soldOutLink = By
			.xpath("//div[@class='results']//div[contains(@class,'results__row')]//button[text()='Sold Out']");
	private By filteredResultCount = By.xpath("//span[@id='filteredResultCount']");
	private By hotelNameLink = By.xpath(
			"//div[@class='results']//div[contains(@class,'results__row')][not(contains(@style,'none'))]//div[@class='hotel__name']/a");
	private By hotelRoomRate = By.xpath(
			"//div[@class='results']//div[contains(@class,'results__row')][not(contains(@style,'none'))]//div[@class='a-price hotel__price--available']");
	private By hotelDistanceFrom = By.xpath(
			"//div[@class='results']//div[contains(@class,'results__row')][not(contains(@style,'none'))]//div[@class='hotel__from']");
	private By hotelResultRow = By.xpath("//div[@class='results']//div[contains(@class,'results__row')]");

	private By filterHotelLink = By.xpath("//div[@class='filters__title']//i");
	private By filterByPrice = By.xpath("//input[@name='1501,3000']"); // 1500-3000
	private By filterByLocation = By.xpath("//input[@name='Electronic city']");
	private By filterByGym = By.xpath("//input[@name='Gym']");
	private By filterApplyButton = By.xpath("//button[contains(@class,'filters__actions__apply')]");
	private By clearAllFilters = By.xpath("//div[contains(@class,'filters__actions__clear')]");
	private By filterChecbox = By.xpath("//div[@class='flex-row filters__container']//input");
	private By hotelAddressText = By.xpath(
			"//div[@class='results']//div[contains(@class,'results__row')][not(contains(@style,'none'))]//div[contains(@class,'hotel__address')]/div");
	private By defaultSortOption = By.xpath("//div[@class='sort__title uc']/span/span");
	private By email = By.xpath("//div[@class='float-labels guest-login__name']/input");
	private By checkBox = By.xpath("//input[@type='checkbox']");

	public void verifyHotelResultsPagePresence() {
		WebDriver driver = DriverManager.getInstance().getDriver();
		browserHelper = new BrowserHelper();
		homePage = new HomePage();
		
		//check if 500 page
		if (homePage.is500Displayed()){
			Assert.fail("500 displayed instead of search page for url " + browserHelper.getCurrentUrl());
		}
		
		try{
			WebDriverWait wait = new WebDriverWait(driver, 120);
			wait.until(ExpectedConditions.presenceOfElementLocated(hotelResultsPage));
		}catch (Exception e){
			// do nothing
		}
        
		Assert.assertTrue((driver.findElements(hotelResultsPage).size() > 0),"Hotel results page not displayed. " + "URL for failed page is " + browserHelper.getCurrentUrl());
		Assert.assertTrue(driver.findElement(hotelResultsPage).isDisplayed(),"Hotel results page not displayed. " + "URL for failed page is " + browserHelper.getCurrentUrl());
		if (driver.findElements(By.xpath("//div[contains(@class,'icon close')]")).size() > 0){
			driver.findElement(By.xpath("//div[contains(@class,'icon close')]")).click();
		}
		System.out.println("Hotel Results Page displayed");
	}

	public void doSearch(String location) {
		WebDriver driver = DriverManager.getInstance().getDriver();
		driver.findElement(destinationInHotelResultsPage).clear();
		driver.findElement(destinationInHotelResultsPage).sendKeys(location);
		driver.findElement(searchButton).click();
	}

	public int[] getAllStyleOrderForResults() {
		WebDriver driver = DriverManager.getInstance().getDriver();
		List<WebElement> ele = driver.findElements(hotelResultRow);
		int[] hotelIndex = new int[ele.size()];
		int i = 0;
		for (WebElement el : ele) {
			hotelIndex[i] = Integer.parseInt(el.getAttribute("style").replaceAll("[^0-9]", ""));
			i = i + 1;
		}
		Arrays.sort(hotelIndex);
//		 System.out.println("Index in displayed order: " +
//		 Arrays.toString(hotelIndex));
		return hotelIndex;
	}

	public int[] getAllStyleOrderForAvailableResults() {
		WebDriver driver = DriverManager.getInstance().getDriver();
		List<WebElement> ele = driver.findElements(
				By.xpath("//div[@class='results']//div[contains(@class,'results__row')][@soldout='True']"));
		int[] hotelIndex = new int[ele.size()];
		int i = 0;
		for (WebElement el : ele) {
			hotelIndex[i] = Integer.parseInt(el.getAttribute("style").replaceAll("[^0-9]", ""));
			i = i + 1;
		}
		Arrays.sort(hotelIndex);
		return hotelIndex;
	}

	public int[] getAllStyleOrderForSoldOutResults() {
		WebDriver driver = DriverManager.getInstance().getDriver();
		List<WebElement> ele = driver.findElements(
				By.xpath("//div[@class='results']//div[contains(@class,'results__row')][@soldout='False']"));
		int[] hotelIndex = new int[ele.size()];
		int i = 0;
		for (WebElement el : ele) {
			hotelIndex[i] = Integer.parseInt(el.getAttribute("style").replaceAll("[^0-9]", ""));
			i = i + 1;
		}
		Arrays.sort(hotelIndex);
		return hotelIndex;
	}

	public void verifyAtleastOneHotelResultIsDisplayed() {
		WebDriver driver = DriverManager.getInstance().getDriver();
		System.out.println("Verify at least one hotel result is displayed");
		int[] indexList = getAllStyleOrderForResults();
		int hotelIndex = indexList[0];
		String xpathFirstHotelName = String.format(
				"//div[@class='results']//div[contains(@class,'results__row')][@style='order: %d;' or @style='order:%d']//div[@class='hotel__name']/a",
				hotelIndex, hotelIndex);
		Assert.assertTrue(driver.findElement(By.xpath(xpathFirstHotelName)).isDisplayed());
	}

	public String getHotelNameByIndex(int hotelIndex) {
		WebDriver driver = DriverManager.getInstance().getDriver();
		int[] indexList = getAllStyleOrderForResults();
		int hotelDisplayIndex = indexList[hotelIndex];
		String xpathHotelName = String.format(
				"//div[@class='results']//div[contains(@class,'results__row')][@style='order: %d;' or @style='order:%d']//div[@class='hotel__name']/a",
				hotelDisplayIndex, hotelDisplayIndex);
		return driver.findElement(By.xpath(xpathHotelName)).getText();
	}
	
	public String getHotelIdByHotelName(String hotelName){
		WebDriver driver = DriverManager.getInstance().getDriver();
		String xpath = String.format("//a[@hotelname='%s']/../../../..", hotelName);
		return driver.findElement(By.xpath(xpath)).getAttribute("data-id");
	}

	public String getHotelAddressByIndex(int hotelIndex) {
		WebDriver driver = DriverManager.getInstance().getDriver();
		int[] indexList = getAllStyleOrderForResults();
		int hotelDisplayIndex = indexList[hotelIndex];
		String xpathHotelAddress = String.format(
				"//div[@class='results']//div[contains(@class,'results__row')][@style='order: %d;' or @style='order:%d']//div[contains(@class,'hotel__address')]/div",
				hotelDisplayIndex, hotelDisplayIndex);
		return driver.findElement(By.xpath(xpathHotelAddress)).getText();
	}

	public String getHotelRoomRateByIndex(int hotelIndex) {
		WebDriver driver = DriverManager.getInstance().getDriver();
		int[] indexList = getAllStyleOrderForResults();
		int hotelDisplayIndex = indexList[hotelIndex];
		String xpathHotelRoomRate = String.format(
				"//div[@class='results']//div[contains(@class,'results__row')][@style='order: %d;' or @style='order:%d']//div[@price]",
				hotelDisplayIndex, hotelDisplayIndex);
		return driver.findElement(By.xpath(xpathHotelRoomRate)).getAttribute("price");
	}

	public void clickHotelNameByIndex(int hotelIndex) {
		WebDriver driver = DriverManager.getInstance().getDriver();
		int[] indexList = getAllStyleOrderForResults();
		int hotelDisplayIndex = indexList[hotelIndex];
		String xpathHotelName = String.format(
				"//div[@class='results']//div[contains(@class,'results__row')][@style='order: %d;' or @style='order:%d']//div[@class='hotel__name']/a",
				hotelDisplayIndex, hotelDisplayIndex);
		driver.findElement(By.xpath(xpathHotelName)).click();
	}

	public void clickQuickBookByIndex(int hotelIndex) {
		WebDriver driver = DriverManager.getInstance().getDriver();
		int[] indexList = getAllStyleOrderForResults();
		int hotelDisplayIndex = indexList[hotelIndex];
		String xpathQuickBook = String.format(
				"//div[@class='results']//div[contains(@class,'results__row')][@style='order: %d;' or @style='order:%d']//a[contains(@class,'analytics-quickbook')]",
				hotelDisplayIndex, hotelDisplayIndex);
		driver.findElement(By.xpath(xpathQuickBook)).click();
	}

	public void clickHotelDetailsByIndex(int hotelIndex) {
		WebDriver driver = DriverManager.getInstance().getDriver();
		int[] indexList = getAllStyleOrderForResults();
		int hotelDisplayIndex = indexList[hotelIndex];
		String xpathHotelDetails = String.format(
				"//div[@class='results']//div[contains(@class,'results__row')][@style='order: %d;' or @style='order:%d']//a[contains(@class,'analytics-viewdetails')]",
				hotelDisplayIndex, hotelDisplayIndex);
		driver.findElement(By.xpath(xpathHotelDetails)).click();
	}

	public void showOnlyAvailableHotels() {
		WebDriver driver = DriverManager.getInstance().getDriver();
		System.out.println("Click on Show Only Available Hotels");
		driver.findElement(showOnlyAvailableCheckBox).click();
	}

	public int getHotelIndexByHotelName(String hotelName) {
		WebDriver driver = DriverManager.getInstance().getDriver();
		int[] indexList = getAllStyleOrderForResults();
		String xpathHotelName = String
				.format("//a[text()=\"%s\"]/ancestor::div[contains(@class,'results__row')]", hotelName);
		String style = driver.findElement(By.xpath(xpathHotelName)).getAttribute("style");
		int styleOrder = Integer.parseInt(style.replaceAll("[^0-9]", ""));
		int indexToReturn = ArrayUtils.indexOf(indexList, styleOrder); //Arrays.asList(indexList).indexOf(styleOrder);
		System.out.println(hotelName +" style index is : " + styleOrder + " and displayed as " + indexToReturn + " th result" );
		return indexToReturn;
	}

	public int getHotelIndexByHotelAvailable() {
		WebDriver driver = DriverManager.getInstance().getDriver();
		int[] indexList = getAllStyleOrderForAvailableResults();
		CommonUtils utils = new CommonUtils();
//		String style;
//		List<WebElement> elementList = driver
//				.findElements(By.xpath("//a[text()='Quick Book']/ancestor::div[contains(@class,'results__row')]"));
//		int[] indexArray = new int[elementList.size()];
//		int index = 0;
//		for (WebElement element : elementList) {
//			style = element.getAttribute("style");
//			indexArray[index] = Integer.parseInt(style.replaceAll("[^0-9]", ""));
//			index = index + 1;
//		}
		
		int indexToReturn = utils.getRandomNumber(0, indexList.length - 1);
		System.out.println("index of hotels style attribute available " + Arrays.toString(indexList));
		System.out.println("Style index of hotel to be selected is : " + indexList[indexToReturn] + " and actual index is " + indexToReturn);
		return indexToReturn;
	}

	public int[] getAllAvailableHotelIndex() {
		WebDriver driver = DriverManager.getInstance().getDriver();
		String style;
		List<WebElement> elementList = driver
				.findElements(By.xpath("//a[text()='Quick Book']/ancestor::div[contains(@class,'results__row')]"));
		int[] indexArray = new int[elementList.size()];
		int index = 0;
		for (WebElement element : elementList) {
			style = element.getAttribute("style");
			indexArray[index] = Integer.parseInt(style.replaceAll("[^0-9]", ""));
			index = index + 1;
		}
		System.out.println("index of hotels style attribute available " + Arrays.toString(indexArray));
		return indexArray;
	}

	public String getCitySearched() {
		WebDriver driver = DriverManager.getInstance().getDriver();
		return driver.findElement(cityInHotelResultsBreadcrumb).getText();
	}

	public String getLocalitySearched() {
		WebDriver driver = DriverManager.getInstance().getDriver();
		return driver.findElement(localityInHotelResultsBreadcrumb).getText();
	}

	public void clickOnHomeInBreadcrumb() {
		WebDriver driver = DriverManager.getInstance().getDriver();
		driver.findElement(homeInHotelResultsBreadcrumb).click();
	}

	public void clickOnCityInBreadcrumb() {
		WebDriver driver = DriverManager.getInstance().getDriver();
		driver.findElement(cityInHotelResultsBreadcrumb).click();
	}

	public void clickOnLocalityInBreadcrumb() {
		WebDriver driver = DriverManager.getInstance().getDriver();
		driver.findElement(localityInHotelResultsBreadcrumb).click();
	}

	public void verifyNoResults() {
		WebDriver driver = DriverManager.getInstance().getDriver();
		System.out.println("Verify No results page displayed");
		Assert.assertTrue(driver.findElement(noResults).isDisplayed());
	}

	public int getTotalDisplayedHotelResults() {
		WebDriver driver = DriverManager.getInstance().getDriver();
		int countTotal = driver.findElements(hotelNameLink).size();
		System.out.println("Get Total Displayed Hotel results: " + countTotal);
		return countTotal;
	}

	public String[] getDisplayedHotelNames() {
		WebDriver driver = DriverManager.getInstance().getDriver();
		List<WebElement> hotelDisplayed = driver.findElements(hotelNameLink);
		int countOfHotelDisplayed = hotelDisplayed.size();
		String[] hotelNamesArray = new String[countOfHotelDisplayed];
		int i = 0;
		for (WebElement element : hotelDisplayed) {
			hotelNamesArray[i] = element.getText();
			i = i + 1;
		}
		System.out.println("Hotel Names Displayed are :" + Arrays.toString(hotelNamesArray));
		return hotelNamesArray;
	}

	public void clickOnHotelByName(String hotelName) {
		WebDriver driver = DriverManager.getInstance().getDriver();
		String hotelNameXpath = String.format("//a[text()=\"%s\"]", hotelName);
		driver.findElement(By.xpath(hotelNameXpath)).click();
		System.out.println("clicked on hotel : " + hotelName);
	}

	public void clickOnTheHotelByName(String hotelName) {
		WebDriver driver = DriverManager.getInstance().getDriver();
		String hotelNameXpath = String.format("//a[contains(text(),'%s')]", hotelName);
		driver.findElement(By.xpath(hotelNameXpath)).click();
		System.out.println("clicked on hotel : " + hotelName);
	}

	public int getTotalAvailableHotelResults() {
		WebDriver driver = DriverManager.getInstance().getDriver();
		int countTotal = driver.findElements(quickBookLink).size();
		System.out.println("Get Total Available Hotel Results :" + countTotal);
		return countTotal;
	}

	public int getTotalSoldOutHotelResults() {
		WebDriver driver = DriverManager.getInstance().getDriver();
		int countTotal = driver.findElements(soldOutLink).size();
		System.out.println("Get Total sold out hotel results: " + countTotal);
		return countTotal;
	}

	public int[] getAllHotelRoomRates() {
		WebDriver driver = DriverManager.getInstance().getDriver();
		int[] indexList = getAllStyleOrderForResults();
		List<WebElement> elementRoomRates = driver.findElements(hotelRoomRate);
		List<WebElement> totalHotelResultRow = driver.findElements(hotelResultRow);
		int[] roomRates = new int[elementRoomRates.size()];
		int j = 0;
		for (int i = 0; i < totalHotelResultRow.size(); i++) {
			String xpathRoomRate = String.format(
					"//div[@class='results']//div[contains(@class,'results__row')][not(contains(@style,'none'))][@style='order: %d;' or @style='order:%d' or @style='order: %d; display: block;' or @style='order: %d; display: none;']",
					indexList[i], indexList[i], indexList[i], indexList[i]);
			try {
				if (driver.findElement(By.xpath(xpathRoomRate)).isDisplayed()) {
					int roomRate = Integer
							.parseInt(driver.findElement(By.xpath(xpathRoomRate)).getAttribute("data-price"));
					roomRates[j] = roomRate;
					j = j + 1;
					// System.out.println("xpath :" + xpathRoomRate);
					// System.out.println("roomRate :" + roomRate);
				}
			} catch (NoSuchElementException e) {
				System.out.println("ignore since this result is hidden" + i);
			}
		}
		System.out.println("Room Rates in Hotel Search results In Displayed Order are " + Arrays.toString(roomRates));
		return roomRates;
	}

	public int[] getAllHotelRoomRatesForAvailableHotel() {
		WebDriver driver = DriverManager.getInstance().getDriver();
		int[] indexList = getAllStyleOrderForAvailableResults();
		int count = getTotalAvailableHotelResults();
		int[] roomRates = new int[count];
		int j = 0;
		for (int i = 0; i < count; i++) {
			String xpathRoomRate = String.format(
					"//div[@class='results']//div[contains(@class,'results__row')][@soldout='True'][not(contains(@style,'none'))][@style='order: %d;' or @style='order:%d' or @style='order: %d; display: block;' or @style='order: %d; display: none;']",
					indexList[i], indexList[i], indexList[i], indexList[i]);
			try {
				if (driver.findElement(By.xpath(xpathRoomRate)).isDisplayed()) {
					roomRates[j] = Integer
							.parseInt(driver.findElement(By.xpath(xpathRoomRate)).getAttribute("data-price"));
					j = j + 1;
				}
			} catch (NoSuchElementException e) {
				System.out.println("ignore since this result is hidden" + i);
			}
		}
		System.out.println(
				"Available Room Rates in Hotel Search results In Displayed Order are " + Arrays.toString(roomRates));
		return roomRates;
	}

	public int[] getAllHotelRoomRatesForSoldOutHotel() {
		WebDriver driver = DriverManager.getInstance().getDriver();
		int[] indexList = getAllStyleOrderForSoldOutResults();
		int count = getTotalSoldOutHotelResults();
		int[] roomRates = new int[count];
		int j = 0;
		for (int i = 0; i < count; i++) {
			String xpathRoomRate = String.format(
					"//div[@class='results']//div[contains(@class,'results__row')][@soldout='False'][not(contains(@style,'none'))][@style='order: %d;' or @style='order:%d' or @style='order: %d; display: block;' or @style='order: %d; display: none;']",
					indexList[i], indexList[i], indexList[i], indexList[i]);
			try {
				if (driver.findElement(By.xpath(xpathRoomRate)).isDisplayed()) {
					roomRates[j] = Integer
							.parseInt(driver.findElement(By.xpath(xpathRoomRate)).getAttribute("data-price"));
					j = j + 1;
				}
			} catch (NoSuchElementException e) {
				System.out.println("ignore since this result is hidden" + i);
			}
		}
		System.out.println(
				"Sold out Room Rates in Hotel Search results In Displayed Order are " + Arrays.toString(roomRates));
		return roomRates;
	}

	public double[] getAllHotelDistanceFrom() {
		WebDriver driver = DriverManager.getInstance().getDriver();
		int[] indexList = getAllStyleOrderForResults();
		List<WebElement> elementHotelDistanceFrom = driver.findElements(hotelDistanceFrom);
		List<WebElement> totalHotelResultRow = driver.findElements(hotelResultRow);
		double[] hotelDistanceFrom = new double[elementHotelDistanceFrom.size()];
		int j = 0;
		for (int i = 0; i < totalHotelResultRow.size(); i++) {
			String xpathHotelDistanceFrom = String.format(
					"//div[@class='results']//div[contains(@class,'results__row')][not(contains(@style,'none'))][@style='order: %d;' or @style='order:%d' or @style='order: %d; display: block;' or @style='order: %d; display: none;']//div[@class='hotel__from']",
					indexList[i], indexList[i], indexList[i], indexList[i]);
			try {
				if (driver.findElement(By.xpath(xpathHotelDistanceFrom)).isDisplayed()) {
					hotelDistanceFrom[j] = Double.parseDouble(
							driver.findElement(By.xpath(xpathHotelDistanceFrom)).getText().trim().split("km")[0]
									.trim());
					j = j + 1;
				}
			} catch (NoSuchElementException e) {
				System.out.println("ignore since this result is hidden" + i);
			}
		}
		System.out.println(
				"Distance in Hotel Search results In Displayed Order are " + Arrays.toString(hotelDistanceFrom));
		return hotelDistanceFrom;
	}

	public double[] getAllHotelDistanceFromForAvailableHotel() {
		WebDriver driver = DriverManager.getInstance().getDriver();
		int[] indexList = getAllStyleOrderForAvailableResults();
		int count = getTotalAvailableHotelResults();
		double[] hotelDistanceFrom = new double[count];
		int j = 0;

		for (int i = 0; i < count; i++) {
			String xpathHotelDistanceFrom = String.format(
					"//div[@class='results']//div[contains(@class,'results__row')][@soldout='True'][not(contains(@style,'none'))][@style='order: %d;' or @style='order:%d' or @style='order: %d; display: block;' or @style='order: %d; display: none;']//div[@class='hotel__from']",
					indexList[i], indexList[i], indexList[i], indexList[i]);
			try {
				if (driver.findElement(By.xpath(xpathHotelDistanceFrom)).isDisplayed()) {
					hotelDistanceFrom[j] = Double.parseDouble(
							driver.findElement(By.xpath(xpathHotelDistanceFrom)).getText().trim().split("km")[0]
									.trim());
					j = j + 1;
				}
			} catch (NoSuchElementException e) {
				System.out.println("ignore since this result is hidden" + i);
			}
		}
		System.out.println(
				"Distance in Hotel Search results In Displayed Order are " + Arrays.toString(hotelDistanceFrom));
		return hotelDistanceFrom;
	}

	public double[] getAllHotelDistanceFromForSoldOutHotel() {
		WebDriver driver = DriverManager.getInstance().getDriver();
		int[] indexList = getAllStyleOrderForSoldOutResults();
		int count = getTotalSoldOutHotelResults();
		double[] hotelDistanceFrom = new double[count];
		int j = 0;
		for (int i = 0; i < count; i++) {
			String xpathHotelDistanceFrom = String.format(
					"//div[@class='results']//div[contains(@class,'results__row')][@soldout='False'][not(contains(@style,'none'))][@style='order: %d;' or @style='order:%d' or @style='order: %d; display: block;' or @style='order: %d; display: none;']//div[@class='hotel__from']",
					indexList[i], indexList[i], indexList[i], indexList[i]);
			try {
				if (driver.findElement(By.xpath(xpathHotelDistanceFrom)).isDisplayed()) {
					hotelDistanceFrom[j] = Double.parseDouble(
							driver.findElement(By.xpath(xpathHotelDistanceFrom)).getText().trim().split("km")[0]
									.trim());
					j = j + 1;
				}
			} catch (NoSuchElementException e) {
				System.out.println("ignore since this result is hidden" + i);
			}
		}
		System.out.println(
				"Distance in Hotel Search results In Displayed Order are " + Arrays.toString(hotelDistanceFrom));
		return hotelDistanceFrom;
	}

	public void applyPriceFilter() {
		WebDriver driver = DriverManager.getInstance().getDriver();
		System.out.println("Select price filter 1500-3000");
		driver.findElement(filterHotelLink).click();
		driver.findElement(filterByPrice).click();
		Assert.assertTrue(driver.findElement(filterByPrice).isSelected());
		driver.findElement(filterApplyButton).click();
		;
	}

	public String applyLocationFilter() {
		WebDriver driver = DriverManager.getInstance().getDriver();
		driver.findElement(filterHotelLink).click();
		driver.findElement(filterByLocation).click();
		Assert.assertTrue(driver.findElement(filterByLocation).isSelected());
		String locationName = driver.findElement(filterByLocation).getAttribute("name");
		System.out.println("location filter selected : " + locationName);
		driver.findElement(filterApplyButton).click();
		return locationName;
	}

	public void applyAmenityFilter() {
		WebDriver driver = DriverManager.getInstance().getDriver();
		driver.findElement(filterHotelLink).click();
		driver.findElement(filterByGym).click();
		Assert.assertTrue(driver.findElement(filterByGym).isSelected());
		driver.findElement(filterApplyButton).click();
	}

	public void clearAllFilters() {
		WebDriver driver = DriverManager.getInstance().getDriver();
		driver.findElement(clearAllFilters).click();
		driver.findElement(filterApplyButton).click();
	}

	public void selectAllFilters() {
		WebDriver driver = DriverManager.getInstance().getDriver();
		List<WebElement> filters = driver.findElements(filterChecbox);
		for (WebElement filter : filters) {
			filter.click();
			Assert.assertTrue(filter.isSelected());
		}
	}

	public String[] getAddressForDisplayedHotels() {
		WebDriver driver = DriverManager.getInstance().getDriver();
		List<WebElement> address = driver.findElements(hotelAddressText);
		String[] addressText = new String[address.size()];
		int i = 0;
		for (WebElement addressElement : address) {
			addressText[i] = addressElement.getText();
			i = i + 1;
		}
		System.out.println("Address for displayed hotels are:" + addressText.toString());
		return addressText;
	}

	public String getSortName() {
		WebDriver driver = DriverManager.getInstance().getDriver();
		String sortName = driver.findElement(defaultSortOption).getText().toLowerCase();
		System.out.println("Current Sort is by: " + sortName);
		return sortName;
	}

	public void mouseOverHotelResultsByIndex(int hotelIndex) {
		WebDriver driver = DriverManager.getInstance().getDriver();
		int[] indexList = getAllStyleOrderForResults();
		browserHelper = new BrowserHelper();
		String xpathHotelResultContent = String.format(
				"//div[@class='results']//div[contains(@class,'results__row')][not(contains(@style,'none'))][contains(@style,'order: %d;') or contains(@style,'order:%d')]//div[contains(@class,'hotel__content')]",
				indexList[hotelIndex], indexList[hotelIndex]);
		String xpathHotelPriceBreakUp = String.format(
				"//div[@class='results']//div[contains(@class,'results__row')][not(contains(@style,'none'))][contains(@style,'order: %d;') or contains(@style,'order:%d')]//div[contains(@class,'hotel__price__breakup')]/div",
				indexList[hotelIndex], indexList[hotelIndex]);

		browserHelper.mouseOver(driver.findElement(By.xpath(xpathHotelResultContent)));
		browserHelper.mouseOver(driver.findElement(By.xpath(xpathHotelPriceBreakUp)));
	}

	public int[] getHotelPriceBreakUpByIndex(int hotelIndex) {
		WebDriver driver = DriverManager.getInstance().getDriver();
		int[] indexList = getAllStyleOrderForResults();
		String xpathHotelRoomPrice = String.format(
				"//div[@class='results']//div[contains(@class,'results__row')][not(contains(@style,'none'))][contains(@style,'order: %d;') or contains(@style,'order:%d')]//div[text()='Room Price']/following-sibling::div",
				indexList[hotelIndex], indexList[hotelIndex]);
		String xpathHotelTax = String.format(
				"//div[@class='results']//div[contains(@class,'results__row')][not(contains(@style,'none'))][contains(@style,'order: %d;') or contains(@style,'order:%d')]//div[text()='Taxes']/following-sibling::div",
				indexList[hotelIndex], indexList[hotelIndex]);
		String xpathHotelDiscount = String.format(
				"//div[@class='results']//div[contains(@class,'results__row')][not(contains(@style,'none'))][contains(@style,'order: %d;') or contains(@style,'order:%d')]//div[text()='Discount']/following-sibling::div",
				indexList[hotelIndex], indexList[hotelIndex]);
		String xpathHotelTotalPrice = String.format(
				"//div[@class='results']//div[contains(@class,'results__row')][not(contains(@style,'none'))][contains(@style,'order: %d;') or contains(@style,'order:%d')]//div[text()='Total']/following-sibling::div",
				indexList[hotelIndex], indexList[hotelIndex]);

		mouseOverHotelResultsByIndex(hotelIndex);
		int[] hotelPriceInfo = new int[4];
		hotelPriceInfo[0] = Integer
				.parseInt(driver.findElement(By.xpath(xpathHotelRoomPrice)).getText().replaceAll("[^0-9]", ""));
		hotelPriceInfo[1] = Integer
				.parseInt(driver.findElement(By.xpath(xpathHotelTax)).getText().replaceAll("[^0-9]", ""));
		hotelPriceInfo[2] = Integer
				.parseInt(driver.findElement(By.xpath(xpathHotelDiscount)).getText().replaceAll("[^0-9]", ""));
		hotelPriceInfo[3] = Integer
				.parseInt(driver.findElement(By.xpath(xpathHotelTotalPrice)).getText().replaceAll("[^0-9]", ""));
		System.out.println("Room Rate : " + hotelPriceInfo[0] + " Tax :" + hotelPriceInfo[1] + " Discount :"
				+ hotelPriceInfo[2] + " Total :" + hotelPriceInfo[3]);
		return hotelPriceInfo;
	}

	public boolean isRoomPriceCalcultaionCorrect(int hotelIndex) {
		int[] indexList = getAllStyleOrderForResults();
		int hotelDisplayedIndex = indexList[hotelIndex];
		int[] hotelPriceInfo = getHotelPriceBreakUpByIndex(hotelDisplayedIndex);
		System.out.println("Should be equal :" + hotelPriceInfo[3] + " and "
				+ (hotelPriceInfo[0] + hotelPriceInfo[1] - hotelPriceInfo[2]));
		return (hotelPriceInfo[3] == (hotelPriceInfo[0] + hotelPriceInfo[1] - hotelPriceInfo[2]));
	}

	public boolean isTotalInBreakUpAndRoomRateEqual(int hotelIndex) {
		int[] indexList = getAllStyleOrderForResults();
		int hotelDisplayedIndex = indexList[hotelIndex];
		int[] hotelPriceInfo = getHotelPriceBreakUpByIndex(hotelDisplayedIndex);
		int hotelRoomRate = Integer.parseInt(getHotelRoomRateByIndex(hotelDisplayedIndex));
		System.out.println("Should be equal " + "Room Rate displayed : " + hotelRoomRate + " Total in price break up : "
				+ hotelPriceInfo[3]);
		return (hotelPriceInfo[3] == hotelRoomRate);
	}

	public List<String> verifyNoDuplicateImagesInSearchPageForHotel(String hotelName) {
		WebDriver driver = DriverManager.getInstance().getDriver();
		browserHelper = new BrowserHelper();
		String imagesXpath = String.format(
				"//a[contains(text(),'%s')]/../../../div[@class='hotel__images']/div[contains(@class,'image-carousel')]//div[contains(@class,'carousel__img')]/img",
				hotelName);
		List<WebElement> elements = driver.findElements(By.xpath(imagesXpath));
		System.out.println("Number of images are :" + elements.size());
		List<String> srcPathList = new ArrayList<String>();
		List<String> srcPathListDupe = new ArrayList<String>();

		for (WebElement element : elements) {
			String srcPath = element.getAttribute("src");
			System.out.println("srcPath : " + srcPath);
			String imageNextLinkxpath = String.format(
					"//a[contains(text(),'%s')]/../../../div[@class='hotel__images']//button[contains(@class,'slick-next')]",
					hotelName);
			driver.findElement(By.xpath(imageNextLinkxpath)).click();
			browserHelper.waitTime(1000);
			if (srcPathList.contains(srcPath)) {
				srcPathListDupe.add(srcPath);
			}
			srcPathList.add(srcPath);
		}
		System.out.println(srcPathList.toString());
		System.out.println(srcPathListDupe.toString());
		return srcPathListDupe;
	}

	public boolean isDistanceDisplayed() {
		WebDriver driver = DriverManager.getInstance().getDriver();
		return (driver.findElements(hotelDistanceFrom).size() > 0);
	}

	public boolean isSEOCityDisplayedInDescription(String cityName) {
		WebDriver driver = DriverManager.getInstance().getDriver();
		return driver.findElement((By.cssSelector(".seo-bottom__header"))).getText().contains(cityName);
	}

	public boolean isSEODescriptionDisplayed(String cityName) {
		WebDriver driver = DriverManager.getInstance().getDriver();
		return driver.findElement((By.cssSelector(".seo-bottom__para .container"))).getText().contains(cityName);
	}
	
	public boolean doesPopupOpened(){
		WebDriver driver = DriverManager.getInstance().getDriver();
		return driver.findElement(By.cssSelector(".hotel__price__breakup__popup.hover-popup__body.hover-popup__twisty.hover-popup__twisty--left")).isDisplayed();
	}
	// total amount verofied 
	public boolean isTotalPriceDisplayCorrect(int total, int roomPrice, int discount, int tax)
	{
		int sum=roomPrice+discount+tax;
		if(total==sum||total==sum-1||total==sum+1)
			return true;
		return false;
	}
	//click on Daily Price breakup on Hotel Result Page
	public void clickOnDailyPriceBreakUpOnHotelResultPage(int hotelIndex)
	{
		WebDriver driver = DriverManager.getInstance().getDriver();
		WebDriverWait wait = new WebDriverWait(driver, 120);
		wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath("//div[@class='results']//div[contains(@class,'results__row')][not(contains(@style,'none'))][contains(@style,'order: "+hotelIndex+";') or contains(@style,'order:"+hotelIndex+"')]//div[@id='price-detail-search']"))));
		driver.findElement(By.xpath("//div[@class='results']//div[contains(@class,'results__row')][not(contains(@style,'none'))][contains(@style,'order: "+hotelIndex+";') or contains(@style,'order:"+hotelIndex+"')]//div[@id='price-detail-search']")).click();
	}
	//Open price break up window
	public boolean isOpenPriceBreakUpWindow(){
		WebDriver driver = DriverManager.getInstance().getDriver();
		return driver.findElement(By.xpath("//*[@id='price-detail']/div[@class='modal__body price-popup-modal-body pos-rel']")).isDisplayed();
	}
	//verified the price break up title
	public boolean isPriceBreakTitleDisplayed(){
		WebDriver driver = DriverManager.getInstance().getDriver();
		return driver.findElement(By.cssSelector(".price-detail-title")).isDisplayed();
	}
	
	//verify the checkIn and CheckOut date in title
	public boolean doesCheckInCheckoutCheckedInTitle(String str1, String str2){
		WebDriver driver = DriverManager.getInstance().getDriver();
		WebElement element = driver.findElement(By.xpath("//p[@class='mt10 grayTxt price-detail-days']/span[2]"));
		WebDriverWait wait = new WebDriverWait(driver, 120);
		wait.until(ExpectedConditions.visibilityOf(element));
		String CheckInCheckOutNumberofStay = element.getText();
		String[] str_array =CheckInCheckOutNumberofStay .split("-");
		String checkIn = str_array[0].trim(); 
	    String[] stringb = str_array[1].split("\\(");
		String[] checkOut2 = stringb[0].split(" ");
		String checkOut = (checkOut2[1]+" "+checkOut2[2]).trim();
		
		System.out.println("*********");
		System.out.println(checkIn);
		System.out.println(str1);
		System.out.println(checkOut);
		System.out.println(str2);
		System.out.println("*********");
		
	    if((checkIn.equals(str1.trim())) &&(checkOut.equals(str2.trim()))){
			return true;}
		return false;
    }
	//verify date title on price breakup page
	public boolean isDateTitleDisplayed(){
		WebDriver driver = DriverManager.getInstance().getDriver();
		return driver.findElement(By.xpath("//*[@id='price-detail']/div//th[1]")).isDisplayed();
	}
	//verify basePrice title on price breakup page
	public boolean isBasePriceTitleDisplayed(){
		WebDriver driver = DriverManager.getInstance().getDriver();
		return driver.findElement(By.xpath("//*[@id='price-detail']/div//th[2]")).isDisplayed();
	}
	//verify tax title on price breakup page
	public boolean isTaxTitleDisplayed(){
		WebDriver driver = DriverManager.getInstance().getDriver();
		return driver.findElement(By.xpath("//*[@id='price-detail']/div//th[3]")).isDisplayed();
	}
	//verify final price title on price breakup page
	public boolean isFinalPriceTitle(){
		WebDriver driver = DriverManager.getInstance().getDriver();
		return driver.findElement(By.xpath("//*[@id='price-detail']/div//th[4]")).isDisplayed();
	}
	//verify the checkIn date are same
	public boolean isCheckInDateSame(String checkIn){
		WebDriver driver = DriverManager.getInstance().getDriver();
		WebElement element = driver.findElement(By.xpath("//tr[@class='grayTxt']/td[1]"));
		String str = element.getText();
		if(checkIn.equals(str)){
			 return true;}
		return false;
	}
	//convert string to int
	public int convertStringToInt(String str){
		float value1= Float.parseFloat(str);
		int value = (int)(value1);
		return value;
	}
	//compare hotel price
	public boolean compareBasePrice(int days,int hotelPrice){
		WebDriver driver = DriverManager.getInstance().getDriver();
		float sumfl = 0;
		for (int i = 1; i <= days; i++) {
			WebElement element = driver.findElement(By.xpath(".//*[@id='price-detail']//tr[" + i + "]/td[2]/span[1]"));
			String basePriceString = element.getText();
			float basePrice=Float.parseFloat(basePriceString);
			//double basePrice = Double.parseDouble(basePriceString);
			sumfl = sumfl + basePrice;
		}
		int sum =(int)sumfl;
		
		if ((hotelPrice == sum) || (hotelPrice==sum+1)){
			    return true;}
		return false;
	}
	//compare tax price
	public boolean compareTaxPrice(int days,int tax){
		WebDriver driver = DriverManager.getInstance().getDriver();
		float sumTaxfl = 0;
		for (int i = 1; i <= days; i++) {
			WebElement element = driver.findElement(By.xpath(".//*[@id='price-detail']//tr["+i+"]/td[3]/span[1]"));
			String basePriceString = element.getText();
			float baseTax = Float.parseFloat(basePriceString);
			sumTaxfl = sumTaxfl + baseTax;
		}
		int sumTax = (int)sumTaxfl;
		if ((tax == sumTax) || (tax==sumTax+1)){
			   return true;}
		return false;
	}
	//compare actual price
	public boolean comapareActualPrice(int days,int total){
		WebDriver driver = DriverManager.getInstance().getDriver();
		float sumActualPricefl = 0;
		for (int i = 1; i <= days; i++) {
			WebElement element = driver.findElement(By.xpath(".//*[@id='price-detail']//tr["+i+"]/td[4]/span[1]"));
			String basePriceString = element.getText();
			float baseTotal = Float.parseFloat(basePriceString);
			sumActualPricefl = sumActualPricefl + baseTotal;
		}
		int sumActualPrice = (int)sumActualPricefl;
		
		if ((total == sumActualPrice)|| (total==sumActualPrice+1)){
			     return true;}
		return false;
	}
	//verify the room price on footer
	public boolean checkRoomPriceOnFooter(int originalPrice){
		WebDriver driver = DriverManager.getInstance().getDriver();
		WebElement element = driver.findElement(By.xpath("//div[@class='price-detail-footer__room text-center']//span[@class='price-detail-footer__amount']/span[1]"));
		String str= element.getText();
		int footerPrice=convertStringToInt(str);
		
		if((originalPrice == footerPrice) || (originalPrice == footerPrice+1)){
			     return true;}
		return false;
	}
	//verify the tax price on footer
	public boolean checkTaxPriceOnFooter(int originalTax){
	    WebDriver driver = DriverManager.getInstance().getDriver();
	    WebElement element = driver.findElement(By.xpath("//div[@class='price-detail-footer__tax text-center']//span[@class='price-detail-footer__amount']/span[1]"));
	    String str= element.getText();
		int footerTax=convertStringToInt(str);
		
	    if(originalTax == footerTax || originalTax==footerTax+1){
		     return true;}
	    return false;
	}
	//verify the total price on footer
	public boolean checkTotalPriceOnFooter(int totalPrice){
	    WebDriver driver = DriverManager.getInstance().getDriver();
	    WebElement element = driver.findElement(By.xpath("//span[@class='price-detail-footer__total-cost']/span[2]"));
	    String str= element.getText();
		int footerTotal=convertStringToInt(str);
	    if(totalPrice==footerTotal || totalPrice==footerTotal+1){
		      return true;}
	    return false;
	}
	//verify the inclusive tax text 
	public boolean isInclusiveTaxTextDisplayed(){
		WebDriver driver = DriverManager.getInstance().getDriver();
	   return driver.findElement(By.xpath("//p[@class='grayTxt price-detail-footer__inclusive']")).isDisplayed();
		
	}
	//display the done button
	public boolean isDoneButtonDisplayed(){
		WebDriver driver = DriverManager.getInstance().getDriver();
		return driver.findElement(By.cssSelector("#doneDetail")).isDisplayed();
	}
	//click on display button
	public void clickOnDone(){
		WebDriver driver = DriverManager.getInstance().getDriver();
		driver.findElement(By.cssSelector("#doneDetail")).click();
	}
	//mouse over to view fare break up
	public void mouseToViewFareBreakup()
	{
		WebDriver driver = DriverManager.getInstance().getDriver();
	  
	    Actions action = new Actions(driver);
	    WebElement elem = driver.findElement(By.cssSelector(".farebreakuptitle.link"));
	    action.moveToElement(elem);
	    action.perform();
	    try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	//click on price break up on HD page
	public void clickOnPriceBreakUpOnHDPage(){
		WebDriver driver = DriverManager.getInstance().getDriver();
		WebDriverWait wait = new WebDriverWait(driver, 120);
		wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath("//*[@id='price-detail-link']"))));
		driver.findElement(By.xpath("//*[@id='price-detail-link']")).click();
	}
	//room price from HD page breakup Popup
	public int extractRoomPriceFromHDPageBreakupPopUp(){
		WebDriver driver = DriverManager.getInstance().getDriver();
		WebElement element = driver.findElement(By.xpath("//div[@class='hotel__price__breakup__popup hover-popup__body hover-popup__twisty hover-popup__twisty--bottom']/div[1]//span[2]"));
		String priceString  = element.getText().trim();
		int price = convertStringToInt(priceString);
		return price;
	}
	//extract the tax price from HD breakup PopUp
	public int extractTaxPriceFromHDBreakupPopUp(){
		WebDriver driver = DriverManager.getInstance().getDriver();
		WebElement element = driver.findElement(By.xpath("//div[@class='hotel__price__breakup__popup hover-popup__body hover-popup__twisty hover-popup__twisty--bottom']/div[2]//span[2]"));
		String priceString  = element.getText().trim();
		int price = convertStringToInt(priceString);
		return price;
	}
	//extract discount price from HD page breakUp pop Up
	public int extractDiscountPriceFromHDPageBreakupPopUp(){
		WebDriver driver = DriverManager.getInstance().getDriver();
		WebElement element = driver.findElement(By.xpath("//div[@class='hotel__price__breakup__popup hover-popup__body hover-popup__twisty hover-popup__twisty--bottom']/div[3]//span[3]"));
		String priceString  = element.getText().trim();
		int price = convertStringToInt(priceString);
		return price;
	}
	//extract total price from HD page breakup pop up
	public int  extractTotalPriceFromHDPageBreakupPopUp(){
		WebDriver driver = DriverManager.getInstance().getDriver();
		WebElement element = driver.findElement(By.xpath("//span[@class='total-price']//span[1]"));
		String priceString  = element.getText().trim();
		int  price = convertStringToInt(priceString);
		return price;
	}
	
	
	
}
