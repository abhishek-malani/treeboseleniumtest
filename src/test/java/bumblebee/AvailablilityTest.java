package bumblebee;

import java.io.IOException;

import org.apache.http.HttpEntity;
import org.apache.http.ParseException;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;
import org.testng.Assert;
import org.testng.annotations.Test;

import utils.BumblebeeUtils;

public class AvailablilityTest{
	@Test(groups = {"availability", "bumblebee"})
	public void availabilityBasicTest() throws IOException, ParseException, JSONException{
		CloseableHttpClient httpClient = HttpClients.createDefault(); 
		HttpGet httpGet = new HttpGet(BumblebeeUtils.getMultipleParams("checkin_date","nights=1","adult=1", "child=1", "no_of_rooms=1","hotel_id","user_id"));
		httpGet.addHeader("Authorization","Bearer "+ BumblebeeUtils.getToken());
		
		CloseableHttpResponse response = httpClient.execute(httpGet);
		try{
			Assert.assertTrue(response.getStatusLine().getStatusCode() == 200,"Response code is "+response.getStatusLine().getStatusCode());

		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
		finally{
			httpClient.close();
		}
	}

	@Test(groups = {"availability", "bumblebee"})
	public void responseOneAdult() throws IOException, ParseException, JSONException{
		CloseableHttpClient httpClient = HttpClients.createDefault(); 
		HttpGet httpGet = new HttpGet(BumblebeeUtils.getMultipleParams("checkin_date","nights=1","adult=1", "child=1", "no_of_rooms=1","hotel_id","user_id"));
		httpGet.addHeader("Authorization","Bearer "+ BumblebeeUtils.getToken());
		
		CloseableHttpResponse response = httpClient.execute(httpGet);
		try{
			Assert.assertTrue(response.getStatusLine().getStatusCode() == 200,"Response code is "+response.getStatusLine().getStatusCode());
			HttpEntity entity = response.getEntity();

			JSONObject jsonObject = new JSONObject(EntityUtils.toString(entity));

			for(int i = 0; i<jsonObject.getJSONObject("data").getJSONArray("rooms").length(); i++){
				Assert.assertTrue(jsonObject.getJSONObject("data").getJSONArray("rooms").getJSONObject(i).getJSONArray("charges").length()==1);
				Assert.assertTrue(Integer.parseInt(jsonObject.getJSONObject("data").getJSONArray("rooms").getJSONObject(i).getString("max_occupancy"))>0);
				for(int j=0; j<jsonObject.getJSONObject("data").getJSONArray("rooms").getJSONObject(i).getJSONArray("charges").length();j++){
					for(int k=0; k<jsonObject.getJSONObject("data").getJSONArray("rooms").getJSONObject(i).getJSONArray("charges").getJSONObject(j).getJSONArray("prices").length();k++){
						Assert.assertTrue(jsonObject.getJSONObject("data").getJSONArray("rooms").getJSONObject(i).getJSONArray("charges").getJSONObject(j).getJSONArray("prices").getJSONObject(k).getInt("price")>0);
						Assert.assertTrue(jsonObject.getJSONObject("data").getJSONArray("rooms").getJSONObject(i).getJSONArray("charges").getJSONObject(j).getJSONArray("prices").getJSONObject(k).getInt("tax")>0);
					}
				}
			}


		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		finally{
			httpClient.close();
		}
	}

	@Test(groups = {"availability", "bumblebee"})
	public void responseTwoAdult() throws IOException, ParseException, JSONException{
		CloseableHttpClient httpClient = HttpClients.createDefault(); 
		HttpGet httpGet = new HttpGet(BumblebeeUtils.getMultipleParams("checkin_date","nights=1","adult=2", "child=1", "no_of_rooms=1","hotel_id","user_id"));
		httpGet.addHeader("Authorization","Bearer "+ BumblebeeUtils.getToken());

		CloseableHttpResponse response = httpClient.execute(httpGet);
		try{
			Assert.assertTrue(response.getStatusLine().getStatusCode() == 200,"Response code is "+response.getStatusLine().getStatusCode());
			HttpEntity entity = response.getEntity();

			JSONObject jsonObject = new JSONObject(EntityUtils.toString(entity));

			for(int i = 0; i<jsonObject.getJSONObject("data").getJSONArray("rooms").length(); i++){
				Assert.assertTrue(jsonObject.getJSONObject("data").getJSONArray("rooms").getJSONObject(i).getJSONArray("charges").length()==2);
				Assert.assertTrue(Integer.parseInt(jsonObject.getJSONObject("data").getJSONArray("rooms").getJSONObject(i).getString("max_occupancy"))>0);
				for(int j=0; j<jsonObject.getJSONObject("data").getJSONArray("rooms").getJSONObject(i).getJSONArray("charges").length();j++){
					for(int k=0; k<jsonObject.getJSONObject("data").getJSONArray("rooms").getJSONObject(i).getJSONArray("charges").getJSONObject(j).getJSONArray("prices").length();k++){
						Assert.assertTrue(jsonObject.getJSONObject("data").getJSONArray("rooms").getJSONObject(i).getJSONArray("charges").getJSONObject(j).getJSONArray("prices").getJSONObject(k).getInt("price")>0);
						Assert.assertTrue(jsonObject.getJSONObject("data").getJSONArray("rooms").getJSONObject(i).getJSONArray("charges").getJSONObject(j).getJSONArray("prices").getJSONObject(k).getInt("tax")>0);
					}
				}
			}


		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		finally{
			httpClient.close();
		}
	}

	@Test(groups = {"availability", "bumblebee"})
	public void responseThreeAdult() throws IOException, ParseException, JSONException{
		CloseableHttpClient httpClient = HttpClients.createDefault(); 
		HttpGet httpGet = new HttpGet(BumblebeeUtils.getMultipleParams("checkin_date","nights=1","adult=3", "child=1", "no_of_rooms=1","hotel_id","user_id"));
		httpGet.addHeader("Authorization","Bearer "+ BumblebeeUtils.getToken());

		CloseableHttpResponse response = httpClient.execute(httpGet);
		try{
			Assert.assertTrue(response.getStatusLine().getStatusCode() == 200,"Response code is "+response.getStatusLine().getStatusCode());
			HttpEntity entity = response.getEntity();

			JSONObject jsonObject = new JSONObject(EntityUtils.toString(entity));
			Assert.assertTrue(jsonObject.getJSONObject("data").getJSONArray("rooms").getJSONObject(0).getJSONArray("charges").length()==3);
			for(int i = 0; i<jsonObject.getJSONObject("data").getJSONArray("rooms").length(); i++){

				Assert.assertTrue(Integer.parseInt(jsonObject.getJSONObject("data").getJSONArray("rooms").getJSONObject(i).getString("max_occupancy"))>0);
				for(int j=0; j<jsonObject.getJSONObject("data").getJSONArray("rooms").getJSONObject(i).getJSONArray("charges").length();j++){
					for(int k=0; k<jsonObject.getJSONObject("data").getJSONArray("rooms").getJSONObject(i).getJSONArray("charges").getJSONObject(j).getJSONArray("prices").length();k++){
						Assert.assertTrue(jsonObject.getJSONObject("data").getJSONArray("rooms").getJSONObject(i).getJSONArray("charges").getJSONObject(j).getJSONArray("prices").getJSONObject(k).getInt("price")>0);
						Assert.assertTrue(jsonObject.getJSONObject("data").getJSONArray("rooms").getJSONObject(i).getJSONArray("charges").getJSONObject(j).getJSONArray("prices").getJSONObject(k).getInt("tax")>0);
					}
				}
			}


		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		finally{
			httpClient.close();
		}
	}

	@Test(groups = {"availability", "bumblebee"})
	public void responseFourAdult() throws IOException, ParseException, JSONException{
		CloseableHttpClient httpClient = HttpClients.createDefault(); 
		HttpGet httpGet = new HttpGet(BumblebeeUtils.getMultipleParams("checkin_date","nights=1","adult=4", "child=1", "no_of_rooms=1","hotel_id","user_id"));
		httpGet.addHeader("Authorization","Bearer "+ BumblebeeUtils.getToken());

		CloseableHttpResponse response = httpClient.execute(httpGet);
		try{
			Assert.assertTrue(response.getStatusLine().getStatusCode() == 200,"Response code is "+response.getStatusLine().getStatusCode());
			HttpEntity entity = response.getEntity();

			JSONObject jsonObject = new JSONObject(EntityUtils.toString(entity));
			Assert.assertTrue(jsonObject.getJSONObject("data").getJSONArray("rooms").getJSONObject(0).getJSONArray("charges").length()==4, "error, response is "+jsonObject.getJSONObject("data").getJSONArray("rooms").getJSONObject(0).getJSONArray("charges").length());
			for(int i = 0; i<jsonObject.getJSONObject("data").getJSONArray("rooms").length(); i++){

				Assert.assertTrue(Integer.parseInt(jsonObject.getJSONObject("data").getJSONArray("rooms").getJSONObject(i).getString("max_occupancy"))>0);
				for(int j=0; j<jsonObject.getJSONObject("data").getJSONArray("rooms").getJSONObject(i).getJSONArray("charges").length();j++){
					for(int k=0; k<jsonObject.getJSONObject("data").getJSONArray("rooms").getJSONObject(i).getJSONArray("charges").getJSONObject(j).getJSONArray("prices").length();k++){
						Assert.assertTrue(jsonObject.getJSONObject("data").getJSONArray("rooms").getJSONObject(i).getJSONArray("charges").getJSONObject(j).getJSONArray("prices").getJSONObject(k).getInt("price")>0);
						Assert.assertTrue(jsonObject.getJSONObject("data").getJSONArray("rooms").getJSONObject(i).getJSONArray("charges").getJSONObject(j).getJSONArray("prices").getJSONObject(k).getInt("tax")>0);
					}
				}
			}


		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		finally{
			httpClient.close();
		}
	}

	@Test(groups = {"availability", "bumblebee"})
	public void responseFiveAdult() throws IOException, ParseException, JSONException{
		CloseableHttpClient httpClient = HttpClients.createDefault(); 
		HttpGet httpGet = new HttpGet(BumblebeeUtils.getMultipleParams("checkin_date","nights=1","adult=5", "child=1", "no_of_rooms=1","hotel_id","user_id"));
		httpGet.addHeader("Authorization","Bearer "+ BumblebeeUtils.getToken());
		
		CloseableHttpResponse response = httpClient.execute(httpGet);
		try{
			Assert.assertTrue(response.getStatusLine().getStatusCode() == 200,"Response code is "+response.getStatusLine().getStatusCode());
			HttpEntity entity = response.getEntity();

			JSONObject jsonObject = new JSONObject(EntityUtils.toString(entity));
			Assert.assertTrue(jsonObject.getJSONObject("data").getJSONArray("rooms").getJSONObject(0).getJSONArray("charges").length()==5);
			for(int i = 0; i<jsonObject.getJSONObject("data").getJSONArray("rooms").length(); i++){

				Assert.assertTrue(Integer.parseInt(jsonObject.getJSONObject("data").getJSONArray("rooms").getJSONObject(i).getString("max_occupancy"))>0);
				for(int j=0; j<jsonObject.getJSONObject("data").getJSONArray("rooms").getJSONObject(i).getJSONArray("charges").length();j++){
					for(int k=0; k<jsonObject.getJSONObject("data").getJSONArray("rooms").getJSONObject(i).getJSONArray("charges").getJSONObject(j).getJSONArray("prices").length();k++){
						Assert.assertTrue(jsonObject.getJSONObject("data").getJSONArray("rooms").getJSONObject(i).getJSONArray("charges").getJSONObject(j).getJSONArray("prices").getJSONObject(k).getInt("price")>0);
						Assert.assertTrue(jsonObject.getJSONObject("data").getJSONArray("rooms").getJSONObject(i).getJSONArray("charges").getJSONObject(j).getJSONArray("prices").getJSONObject(k).getInt("tax")>0);
					}
				}
			}


		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		finally{
			httpClient.close();
		}
	}

	@Test(groups = {"availability", "bumblebee"})
	public void responseSixAdult() throws IOException, ParseException, JSONException{
		CloseableHttpClient httpClient = HttpClients.createDefault(); 
		HttpGet httpGet = new HttpGet(BumblebeeUtils.getMultipleParams("checkin_date","nights=1","adult=6", "child=1", "no_of_rooms=1","hotel_id","user_id"));
		httpGet.addHeader("Authorization","Bearer "+ BumblebeeUtils.getToken());

		CloseableHttpResponse response = httpClient.execute(httpGet);
		try{
			Assert.assertTrue(response.getStatusLine().getStatusCode() == 200,"Response code is "+response.getStatusLine().getStatusCode());
			HttpEntity entity = response.getEntity();

			JSONObject jsonObject = new JSONObject(EntityUtils.toString(entity));
			Assert.assertTrue(jsonObject.getJSONObject("data").getJSONArray("rooms").getJSONObject(0).getJSONArray("charges").length()==6);
			for(int i = 0; i<jsonObject.getJSONObject("data").getJSONArray("rooms").length(); i++){

				Assert.assertTrue(Integer.parseInt(jsonObject.getJSONObject("data").getJSONArray("rooms").getJSONObject(i).getString("max_occupancy"))>0);
				for(int j=0; j<jsonObject.getJSONObject("data").getJSONArray("rooms").getJSONObject(i).getJSONArray("charges").length();j++){
					for(int k=0; k<jsonObject.getJSONObject("data").getJSONArray("rooms").getJSONObject(i).getJSONArray("charges").getJSONObject(j).getJSONArray("prices").length();k++){
						Assert.assertTrue(jsonObject.getJSONObject("data").getJSONArray("rooms").getJSONObject(i).getJSONArray("charges").getJSONObject(j).getJSONArray("prices").getJSONObject(k).getInt("price")>0);
						Assert.assertTrue(jsonObject.getJSONObject("data").getJSONArray("rooms").getJSONObject(i).getJSONArray("charges").getJSONObject(j).getJSONArray("prices").getJSONObject(k).getInt("tax")>0);
					}
				}
			}


		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		finally{
			httpClient.close();
		}
	}

	@Test(groups = {"availability", "bumblebee"})
	public void responseOneNight() throws IOException, ParseException, JSONException{
		CloseableHttpClient httpClient = HttpClients.createDefault(); 
		HttpGet httpGet = new HttpGet(BumblebeeUtils.getMultipleParams("checkin_date","nights=1","adult=1", "child=1", "no_of_rooms=1","hotel_id","user_id"));
		httpGet.addHeader("Authorization","Bearer "+ BumblebeeUtils.getToken());

		CloseableHttpResponse response = httpClient.execute(httpGet);
		try{
			Assert.assertTrue(response.getStatusLine().getStatusCode() == 200,"Response code is "+response.getStatusLine().getStatusCode());
			HttpEntity entity = response.getEntity();

			JSONObject jsonObject = new JSONObject(EntityUtils.toString(entity));

			for(int i = 0; i<jsonObject.getJSONObject("data").getJSONArray("rooms").length(); i++){

				Assert.assertTrue(Integer.parseInt(jsonObject.getJSONObject("data").getJSONArray("rooms").getJSONObject(i).getString("max_occupancy"))>0);
				for(int j=0; j<jsonObject.getJSONObject("data").getJSONArray("rooms").getJSONObject(i).getJSONArray("charges").length();j++){
					Assert.assertTrue(jsonObject.getJSONObject("data").getJSONArray("rooms").getJSONObject(i).getJSONArray("charges").getJSONObject(j).getJSONArray("prices").length()==1);
					for(int k=0; k<jsonObject.getJSONObject("data").getJSONArray("rooms").getJSONObject(i).getJSONArray("charges").getJSONObject(j).getJSONArray("prices").length();k++){
						Assert.assertTrue(jsonObject.getJSONObject("data").getJSONArray("rooms").getJSONObject(i).getJSONArray("charges").getJSONObject(j).getJSONArray("prices").getJSONObject(k).getInt("price")>0);
						Assert.assertTrue(jsonObject.getJSONObject("data").getJSONArray("rooms").getJSONObject(i).getJSONArray("charges").getJSONObject(j).getJSONArray("prices").getJSONObject(k).getInt("tax")>0);
					}
				}
			}


		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		finally{
			httpClient.close();
		}
	}

	@Test(groups = {"availability", "bumblebee"})
	public void responseTwoNight() throws IOException, ParseException, JSONException{
		CloseableHttpClient httpClient = HttpClients.createDefault(); 
		HttpGet httpGet = new HttpGet(BumblebeeUtils.getMultipleParams("checkin_date","nights=2","adult=1", "child=1", "no_of_rooms=1","hotel_id","user_id"));
		httpGet.addHeader("Authorization","Bearer "+ BumblebeeUtils.getToken());

		CloseableHttpResponse response = httpClient.execute(httpGet);
		try{
			Assert.assertTrue(response.getStatusLine().getStatusCode() == 200,"Response code is "+response.getStatusLine().getStatusCode());
			HttpEntity entity = response.getEntity();

			JSONObject jsonObject = new JSONObject(EntityUtils.toString(entity));

			for(int i = 0; i<jsonObject.getJSONObject("data").getJSONArray("rooms").length(); i++){

				Assert.assertTrue(Integer.parseInt(jsonObject.getJSONObject("data").getJSONArray("rooms").getJSONObject(i).getString("max_occupancy"))>0);
				for(int j=0; j<jsonObject.getJSONObject("data").getJSONArray("rooms").getJSONObject(i).getJSONArray("charges").length();j++){
					Assert.assertTrue(jsonObject.getJSONObject("data").getJSONArray("rooms").getJSONObject(i).getJSONArray("charges").getJSONObject(j).getJSONArray("prices").length()==2);
					for(int k=0; k<jsonObject.getJSONObject("data").getJSONArray("rooms").getJSONObject(i).getJSONArray("charges").getJSONObject(j).getJSONArray("prices").length();k++){
						Assert.assertTrue(jsonObject.getJSONObject("data").getJSONArray("rooms").getJSONObject(i).getJSONArray("charges").getJSONObject(j).getJSONArray("prices").getJSONObject(k).getInt("price")>0);
						Assert.assertTrue(jsonObject.getJSONObject("data").getJSONArray("rooms").getJSONObject(i).getJSONArray("charges").getJSONObject(j).getJSONArray("prices").getJSONObject(k).getInt("tax")>0);
					}
				}
			}


		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		finally{
			httpClient.close();
		}
	}

	@Test(groups = {"availability", "bumblebee"})
	public void responseThreeNight() throws IOException, ParseException, JSONException{
		CloseableHttpClient httpClient = HttpClients.createDefault(); 
		HttpGet httpGet = new HttpGet(BumblebeeUtils.getMultipleParams("checkin_date","nights=3","adult=1", "child=1", "no_of_rooms=1","hotel_id","user_id"));
		httpGet.addHeader("Authorization","Bearer "+ BumblebeeUtils.getToken());

		CloseableHttpResponse response = httpClient.execute(httpGet);
		try{
			Assert.assertTrue(response.getStatusLine().getStatusCode() == 200,"Response code is "+response.getStatusLine().getStatusCode());
			HttpEntity entity = response.getEntity();

			JSONObject jsonObject = new JSONObject(EntityUtils.toString(entity));

			for(int i = 0; i<jsonObject.getJSONObject("data").getJSONArray("rooms").length(); i++){

				Assert.assertTrue(Integer.parseInt(jsonObject.getJSONObject("data").getJSONArray("rooms").getJSONObject(i).getString("max_occupancy"))>0);
				for(int j=0; j<jsonObject.getJSONObject("data").getJSONArray("rooms").getJSONObject(i).getJSONArray("charges").length();j++){
					Assert.assertTrue(jsonObject.getJSONObject("data").getJSONArray("rooms").getJSONObject(i).getJSONArray("charges").getJSONObject(j).getJSONArray("prices").length()==3);
					for(int k=0; k<jsonObject.getJSONObject("data").getJSONArray("rooms").getJSONObject(i).getJSONArray("charges").getJSONObject(j).getJSONArray("prices").length();k++){
						Assert.assertTrue(jsonObject.getJSONObject("data").getJSONArray("rooms").getJSONObject(i).getJSONArray("charges").getJSONObject(j).getJSONArray("prices").getJSONObject(k).getInt("price")>0);
						Assert.assertTrue(jsonObject.getJSONObject("data").getJSONArray("rooms").getJSONObject(i).getJSONArray("charges").getJSONObject(j).getJSONArray("prices").getJSONObject(k).getInt("tax")>0);
					}
				}
			}


		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		finally{
			httpClient.close();
		}
	}

	@Test(groups = {"availability", "bumblebee"})
	public void responseWeekNight() throws IOException, ParseException, JSONException{
		CloseableHttpClient httpClient = HttpClients.createDefault(); 
		HttpGet httpGet = new HttpGet(BumblebeeUtils.getMultipleParams("checkin_date","nights=7","adult=1", "child=1", "no_of_rooms=1","hotel_id","user_id"));
		httpGet.addHeader("Authorization","Bearer "+ BumblebeeUtils.getToken());

		CloseableHttpResponse response = httpClient.execute(httpGet);
		try{
			Assert.assertTrue(response.getStatusLine().getStatusCode() == 200,"Response code is "+response.getStatusLine().getStatusCode());
			HttpEntity entity = response.getEntity();

			JSONObject jsonObject = new JSONObject(EntityUtils.toString(entity));

			for(int i = 0; i<jsonObject.getJSONObject("data").getJSONArray("rooms").length(); i++){

				Assert.assertTrue(Integer.parseInt(jsonObject.getJSONObject("data").getJSONArray("rooms").getJSONObject(i).getString("max_occupancy"))>0);
				for(int j=0; j<jsonObject.getJSONObject("data").getJSONArray("rooms").getJSONObject(i).getJSONArray("charges").length();j++){
					Assert.assertTrue(jsonObject.getJSONObject("data").getJSONArray("rooms").getJSONObject(i).getJSONArray("charges").getJSONObject(j).getJSONArray("prices").length()==7);
					for(int k=0; k<jsonObject.getJSONObject("data").getJSONArray("rooms").getJSONObject(i).getJSONArray("charges").getJSONObject(j).getJSONArray("prices").length();k++){
						Assert.assertTrue(jsonObject.getJSONObject("data").getJSONArray("rooms").getJSONObject(i).getJSONArray("charges").getJSONObject(j).getJSONArray("prices").getJSONObject(k).getInt("price")>0);
						Assert.assertTrue(jsonObject.getJSONObject("data").getJSONArray("rooms").getJSONObject(i).getJSONArray("charges").getJSONObject(j).getJSONArray("prices").getJSONObject(k).getInt("tax")>0);
					}
				}
			}


		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		finally{
			httpClient.close();
		}
	}

	@Test(groups = {"availability", "bumblebee"})
	public void responseMonthNight() throws IOException, ParseException, JSONException{
		CloseableHttpClient httpClient = HttpClients.createDefault(); 
		HttpGet httpGet = new HttpGet(BumblebeeUtils.getMultipleParams("checkin_date","nights=31","adult=1", "child=1", "no_of_rooms=1","hotel_id","user_id"));
		httpGet.addHeader("Authorization","Bearer "+ BumblebeeUtils.getToken());

		CloseableHttpResponse response = httpClient.execute(httpGet);
		try{
			Assert.assertTrue(response.getStatusLine().getStatusCode() == 200,"Response code is "+response.getStatusLine().getStatusCode());
			HttpEntity entity = response.getEntity();

			JSONObject jsonObject = new JSONObject(EntityUtils.toString(entity));

			for(int i = 0; i<jsonObject.getJSONObject("data").getJSONArray("rooms").length(); i++){

				Assert.assertTrue(Integer.parseInt(jsonObject.getJSONObject("data").getJSONArray("rooms").getJSONObject(i).getString("max_occupancy"))>0);
				for(int j=0; j<jsonObject.getJSONObject("data").getJSONArray("rooms").getJSONObject(i).getJSONArray("charges").length();j++){
					Assert.assertTrue(jsonObject.getJSONObject("data").getJSONArray("rooms").getJSONObject(i).getJSONArray("charges").getJSONObject(j).getJSONArray("prices").length()==31);
					for(int k=0; k<jsonObject.getJSONObject("data").getJSONArray("rooms").getJSONObject(i).getJSONArray("charges").getJSONObject(j).getJSONArray("prices").length();k++){
						Assert.assertTrue(jsonObject.getJSONObject("data").getJSONArray("rooms").getJSONObject(i).getJSONArray("charges").getJSONObject(j).getJSONArray("prices").getJSONObject(k).getInt("price")>0);
						Assert.assertTrue(jsonObject.getJSONObject("data").getJSONArray("rooms").getJSONObject(i).getJSONArray("charges").getJSONObject(j).getJSONArray("prices").getJSONObject(k).getInt("tax")>0);
					}
				}
			}

		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		finally{
			httpClient.close();
		}
	}

	@Test(groups = {"availability", "bumblebee"})
	public void responseMultipleParam() throws IOException, ParseException, JSONException{
		CloseableHttpClient httpClient = HttpClients.createDefault(); 
		HttpGet httpGet = new HttpGet(BumblebeeUtils.getMultipleParams("checkin_date","nights=4","adult=4", "child=6", "no_of_rooms=2","hotel_id","user_id"));
		httpGet.addHeader("Authorization","Bearer "+ BumblebeeUtils.getToken());

		CloseableHttpResponse response = httpClient.execute(httpGet);
		try{
			Assert.assertTrue(response.getStatusLine().getStatusCode() == 200,"Response code is "+response.getStatusLine().getStatusCode());
			HttpEntity entity = response.getEntity();

			JSONObject jsonObject = new JSONObject(EntityUtils.toString(entity));

			for(int i = 0; i<jsonObject.getJSONObject("data").getJSONArray("rooms").length(); i++){
				Assert.assertTrue(jsonObject.getJSONObject("data").getJSONArray("rooms").getJSONObject(0).getJSONArray("charges").length()==4, "error, "+jsonObject.getJSONObject("data").getJSONArray("rooms").getJSONObject(0).getJSONArray("charges").length());
				Assert.assertTrue(Integer.parseInt(jsonObject.getJSONObject("data").getJSONArray("rooms").getJSONObject(i).getString("max_occupancy"))>0);
				for(int j=0; j<jsonObject.getJSONObject("data").getJSONArray("rooms").getJSONObject(i).getJSONArray("charges").length();j++){
					Assert.assertTrue(jsonObject.getJSONObject("data").getJSONArray("rooms").getJSONObject(i).getJSONArray("charges").getJSONObject(j).getJSONArray("prices").length()==4,"error, response is "+jsonObject.getJSONObject("data").getJSONArray("rooms").getJSONObject(i).getJSONArray("charges").getJSONObject(j).getJSONArray("prices").length());
					for(int k=0; k<jsonObject.getJSONObject("data").getJSONArray("rooms").getJSONObject(i).getJSONArray("charges").getJSONObject(j).getJSONArray("prices").length();k++){
						Assert.assertTrue(jsonObject.getJSONObject("data").getJSONArray("rooms").getJSONObject(i).getJSONArray("charges").getJSONObject(j).getJSONArray("prices").getJSONObject(k).getInt("price")>0);
						Assert.assertTrue(jsonObject.getJSONObject("data").getJSONArray("rooms").getJSONObject(i).getJSONArray("charges").getJSONObject(j).getJSONArray("prices").getJSONObject(k).getInt("tax")>0);
					}
				}
			}

		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		finally{
			httpClient.close();
		}
	}

	@Test(groups = {"availability", "bumblebee"})
	public void nullCheckinTest() throws IOException, ParseException, JSONException{
		CloseableHttpClient httpClient = HttpClients.createDefault(); 
		HttpGet httpGet = new HttpGet(BumblebeeUtils.getMultipleParams("nights=1","adult=1", "child=1", "no_of_rooms=1","hotel_id","user_id"));
		httpGet.addHeader("Authorization","Bearer "+ BumblebeeUtils.getToken());

		CloseableHttpResponse response = httpClient.execute(httpGet);
		try{
			Assert.assertTrue(response.getStatusLine().getStatusCode() == 200,"Response code is "+response.getStatusLine().getStatusCode());
			HttpEntity entity = response.getEntity();

			JSONObject jsonObject = new JSONObject(EntityUtils.toString(entity));
			Assert.assertTrue(jsonObject.getString("status").equals("error"));
			Assert.assertTrue(jsonObject.getString("msg").equals("'checkin_date'"));
			Assert.assertTrue(jsonObject.getString("code").equals("101"));
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		finally{
			httpClient.close();
		}
	}
	// put compare 
	@Test(groups = {"availability", "bumblebee"})
	public void invalidCheckinTest() throws IOException, ParseException, JSONException{
		CloseableHttpClient httpClient = HttpClients.createDefault(); 
		HttpGet httpGet = new HttpGet(BumblebeeUtils.getMultipleParams("checkin_date=asf","nights=1","adult=1", "child=1", "no_of_rooms=1","hotel_id","user_id"));
		httpGet.addHeader("Authorization","Bearer "+ BumblebeeUtils.getToken());

		CloseableHttpResponse response = httpClient.execute(httpGet);
		try{
			Assert.assertTrue(response.getStatusLine().getStatusCode() == 200,"Response code is "+response.getStatusLine().getStatusCode());
			HttpEntity entity = response.getEntity();

			JSONObject jsonObject = new JSONObject(EntityUtils.toString(entity));
			Assert.assertTrue(jsonObject.getString("status").equals("error"));
			Assert.assertTrue(jsonObject.getString("msg").contains("does not match format"), "error, "+jsonObject.getString("msg"));
			Assert.assertTrue(jsonObject.getString("code").equals("101"));
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		finally{
			httpClient.close();
		}
	}
	
	@Test(groups = {"availability", "bumblebee"})
	public void nullCheckoutTest() throws IOException, ParseException, JSONException{
		CloseableHttpClient httpClient = HttpClients.createDefault(); 
		HttpGet httpGet = new HttpGet(BumblebeeUtils.getMultipleParams("checkin_date","adult=1", "child=1", "no_of_rooms=1","hotel_id","user_id"));
		httpGet.addHeader("Authorization","Bearer "+ BumblebeeUtils.getToken());
		
		CloseableHttpResponse response = httpClient.execute(httpGet);
		try{
			Assert.assertTrue(response.getStatusLine().getStatusCode() == 200,"Response code is "+response.getStatusLine().getStatusCode());
			HttpEntity entity = response.getEntity();

			JSONObject jsonObject = new JSONObject(EntityUtils.toString(entity));
			Assert.assertTrue(jsonObject.getString("status").equals("error"), "error "+jsonObject.getString("status"));
			Assert.assertTrue(jsonObject.getString("msg").equals("'checkout_date'"));
			Assert.assertTrue(jsonObject.getString("code").equals("101"));
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		finally{
			httpClient.close();
		}
	}
	
	@Test(groups = {"availability", "bumblebee"})
	public void invalidCheckoutTest() throws IOException, ParseException, JSONException{
		CloseableHttpClient httpClient = HttpClients.createDefault(); 
		HttpGet httpGet = new HttpGet(BumblebeeUtils.getMultipleParams("checkin_date","checkout_date=dgf","adult=1", "child=1", "no_of_rooms=1","hotel_id","user_id"));
		httpGet.addHeader("Authorization","Bearer "+ BumblebeeUtils.getToken());

		CloseableHttpResponse response = httpClient.execute(httpGet);
		try{
			Assert.assertTrue(response.getStatusLine().getStatusCode() == 200,"Response code is "+response.getStatusLine().getStatusCode());
			HttpEntity entity = response.getEntity();

			JSONObject jsonObject = new JSONObject(EntityUtils.toString(entity));
			Assert.assertTrue(jsonObject.getString("status").equals("error"));
			Assert.assertTrue(jsonObject.getString("msg").contains("does not match format"));
			Assert.assertTrue(jsonObject.getString("code").equals("101"));
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		finally{
			httpClient.close();
		}
	}
	
	@Test(groups = {"availability", "bumblebee"})
	public void nullAdultTest() throws IOException, ParseException, JSONException{
		CloseableHttpClient httpClient = HttpClients.createDefault(); 
		HttpGet httpGet = new HttpGet(BumblebeeUtils.getMultipleParams("checkin_date","nights=2", "child=1", "no_of_rooms=1","hotel_id","user_id"));
		httpGet.addHeader("Authorization","Bearer "+ BumblebeeUtils.getToken());

		CloseableHttpResponse response = httpClient.execute(httpGet);
		try{
			Assert.assertTrue(response.getStatusLine().getStatusCode() == 200,"Response code is "+response.getStatusLine().getStatusCode());
			HttpEntity entity = response.getEntity();

			JSONObject jsonObject = new JSONObject(EntityUtils.toString(entity));
			Assert.assertTrue(jsonObject.getString("status").equals("error"));
			Assert.assertTrue(jsonObject.getString("msg").equals("'adult'"));
			Assert.assertTrue(jsonObject.getString("code").equals("101"));
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		finally{
			httpClient.close();
		}
	}
	
	@Test(groups = {"availability", "bumblebee"})
	public void invalidAdultTest() throws IOException, ParseException, JSONException{
		CloseableHttpClient httpClient = HttpClients.createDefault(); 
		HttpGet httpGet = new HttpGet(BumblebeeUtils.getMultipleParams("checkin_date","nights=2","adult=a", "child=1", "no_of_rooms=1","hotel_id","user_id"));
		httpGet.addHeader("Authorization","Bearer "+ BumblebeeUtils.getToken());
		
		CloseableHttpResponse response = httpClient.execute(httpGet);
		try{
			Assert.assertTrue(response.getStatusLine().getStatusCode() == 200,"Response code is "+response.getStatusLine().getStatusCode());
			HttpEntity entity = response.getEntity();

			JSONObject jsonObject = new JSONObject(EntityUtils.toString(entity));
			Assert.assertTrue(jsonObject.getString("status").equals("success"));
			Assert.assertTrue(jsonObject.getString("data").equals("Error in searching"));
			
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		finally{
			httpClient.close();
		}
	}
	
	@Test(groups = {"availability", "bumblebee"})
	public void nullChildTest() throws IOException, ParseException, JSONException{
		CloseableHttpClient httpClient = HttpClients.createDefault(); 
		HttpGet httpGet = new HttpGet(BumblebeeUtils.getMultipleParams("checkin_date","nights=2","adult=1", "no_of_rooms=1","hotel_id","user_id"));
		httpGet.addHeader("Authorization","Bearer "+ BumblebeeUtils.getToken());
		
		CloseableHttpResponse response = httpClient.execute(httpGet);
		try{
			Assert.assertTrue(response.getStatusLine().getStatusCode() == 200,"Response code is "+response.getStatusLine().getStatusCode());
			HttpEntity entity = response.getEntity();

			JSONObject jsonObject = new JSONObject(EntityUtils.toString(entity));
			Assert.assertTrue(jsonObject.getString("status").equals("error"));
			Assert.assertTrue(jsonObject.getString("msg").equals("'child'"), "error, "+jsonObject.getString("msg"));
			Assert.assertTrue(jsonObject.getString("code").equals("101"));
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		finally{
			httpClient.close();
		}
	}
	
	@Test(groups = {"availability", "bumblebee"})
	public void nullNoOfRoomsTest() throws IOException, ParseException, JSONException{
		CloseableHttpClient httpClient = HttpClients.createDefault(); 
		HttpGet httpGet = new HttpGet(BumblebeeUtils.getMultipleParams("checkin_date","nights=2","adult=1", "child=1","hotel_id","user_id"));
		httpGet.addHeader("Authorization","Bearer "+ BumblebeeUtils.getToken());

		CloseableHttpResponse response = httpClient.execute(httpGet);
		try{
			Assert.assertTrue(response.getStatusLine().getStatusCode() == 200,"Response code is "+response.getStatusLine().getStatusCode());
			HttpEntity entity = response.getEntity();

			JSONObject jsonObject = new JSONObject(EntityUtils.toString(entity));
			Assert.assertTrue(jsonObject.getString("status").equals("error"));
			Assert.assertTrue(jsonObject.getString("msg").equals("'no_of_rooms'"));
			Assert.assertTrue(jsonObject.getString("code").equals("101"));
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		finally{
			httpClient.close();
		}
	}
	
	@Test(groups = {"availability", "bumblebee"})
	public void invalidNoOfRoomsTest() throws IOException, ParseException, JSONException{
		CloseableHttpClient httpClient = HttpClients.createDefault(); 
		HttpGet httpGet = new HttpGet(BumblebeeUtils.getMultipleParams("checkin_date","nights=2","adult=1", "child=1", "no_of_rooms=d","hotel_id","user_id"));
		httpGet.addHeader("Authorization","Bearer "+ BumblebeeUtils.getToken());

		CloseableHttpResponse response = httpClient.execute(httpGet);
		try{
			Assert.assertTrue(response.getStatusLine().getStatusCode() == 200,"Response code is "+response.getStatusLine().getStatusCode());
			HttpEntity entity = response.getEntity();

			JSONObject jsonObject = new JSONObject(EntityUtils.toString(entity));
			Assert.assertTrue(jsonObject.getString("status").equals("success"));
			Assert.assertTrue(jsonObject.getJSONObject("data").getJSONArray("rooms").length()==0);
			
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		finally{
			httpClient.close();
		}
	}
	
	@Test(groups = {"availability", "bumblebee"})
	public void nullHotelIDTest() throws IOException, ParseException, JSONException{
		CloseableHttpClient httpClient = HttpClients.createDefault(); 
		HttpGet httpGet = new HttpGet(BumblebeeUtils.getMultipleParams("checkin_date","nights=2","adult=1", "child=1", "no_of_rooms=1","user_id"));
		httpGet.addHeader("Authorization","Bearer "+ BumblebeeUtils.getToken());

		CloseableHttpResponse response = httpClient.execute(httpGet);
		try{
			Assert.assertTrue(response.getStatusLine().getStatusCode() == 200,"Response code is "+response.getStatusLine().getStatusCode());
			HttpEntity entity = response.getEntity();

			JSONObject jsonObject = new JSONObject(EntityUtils.toString(entity));
			Assert.assertTrue(jsonObject.getString("status").equals("error"));
			Assert.assertTrue(jsonObject.getString("msg").equals("'hotel_id'"));
			Assert.assertTrue(jsonObject.getString("code").equals("101"));
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		finally{
			httpClient.close();
		}
	}
	
	@Test(groups = {"availability", "bumblebee"})
	public void nullUserIDTest() throws IOException, ParseException, JSONException{
		CloseableHttpClient httpClient = HttpClients.createDefault(); 
		HttpGet httpGet = new HttpGet(BumblebeeUtils.getMultipleParams("checkin_date","nights=2","adult=1", "child=1", "no_of_rooms=1","hotel_id"));
		httpGet.addHeader("Authorization","Bearer "+ BumblebeeUtils.getToken());

		CloseableHttpResponse response = httpClient.execute(httpGet);
		try{
			Assert.assertTrue(response.getStatusLine().getStatusCode() == 200,"Response code is "+response.getStatusLine().getStatusCode());
			HttpEntity entity = response.getEntity();

			JSONObject jsonObject = new JSONObject(EntityUtils.toString(entity));
			Assert.assertTrue(jsonObject.getString("status").equals("error"));
			Assert.assertTrue(jsonObject.getString("msg").equals("'user_id'"));
			Assert.assertTrue(jsonObject.getString("code").equals("101"));
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		finally{
			httpClient.close();
		}
	}
	
	@Test(groups = {"availability", "bumblebee"})
	public void invalidUserIDTest() throws IOException, ParseException, JSONException{
		CloseableHttpClient httpClient = HttpClients.createDefault(); 
		HttpGet httpGet = new HttpGet(BumblebeeUtils.getMultipleParams("checkin_date","nights=2","adult=1", "child=1", "no_of_rooms=1","hotel_id","user_id=gbc"));
		httpGet.addHeader("Authorization","Bearer "+ BumblebeeUtils.getToken());

		CloseableHttpResponse response = httpClient.execute(httpGet);
		try{
			Assert.assertTrue(response.getStatusLine().getStatusCode() == 200,"Response code is "+response.getStatusLine().getStatusCode());
			HttpEntity entity = response.getEntity();

			JSONObject jsonObject = new JSONObject(EntityUtils.toString(entity));
			Assert.assertTrue(jsonObject.getString("status").equals("success"));
			Assert.assertTrue(jsonObject.getString("data").equals("Error in searching"));
			
		} catch (ParseException e) {	
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		finally{
			httpClient.close();
		}
	}
	
	@Test(groups = {"availability", "bumblebee"})
	public void invalidHotelIDTest() throws IOException, ParseException, JSONException{
		CloseableHttpClient httpClient = HttpClients.createDefault(); 
		HttpGet httpGet = new HttpGet(BumblebeeUtils.getMultipleParams("checkin_date","nights=2","adult=1", "child=1", "no_of_rooms=1","hotel_id=sdvc","user_id"));
		httpGet.addHeader("Authorization","Bearer "+ BumblebeeUtils.getToken());

		CloseableHttpResponse response = httpClient.execute(httpGet);
		try{
			Assert.assertTrue(response.getStatusLine().getStatusCode() == 200,"Response code is "+response.getStatusLine().getStatusCode());
			HttpEntity entity = response.getEntity();

			JSONObject jsonObject = new JSONObject(EntityUtils.toString(entity));
			Assert.assertTrue(jsonObject.getString("status").equals("error"));
			Assert.assertTrue(jsonObject.getString("msg").equals("Hotel matching query does not exist."));
			Assert.assertTrue(jsonObject.getString("code").equals("101"));
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		finally{
			httpClient.close();
		}
	}

}
