package bumblebee;

import java.io.IOException;

import org.apache.http.HttpEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.testng.Assert;
import org.testng.annotations.Test;

import utils.BumblebeeUtils;

import org.json.JSONException;
import org.json.JSONObject;;

public class AllHotelsBasicTest {

	@Test(groups={"allHotels", "bumblebee"})
	public void allHotelsBasicTest() throws IOException, JSONException {
		CloseableHttpClient httpClient = HttpClients.createDefault(); 
		HttpGet httpGet = new HttpGet(BumblebeeUtils.makeString("allHotels"));
		CloseableHttpResponse response = httpClient.execute(httpGet);
		try{
			Assert.assertTrue(response.getStatusLine().getStatusCode() == 200,"Response code is "+response.getStatusLine().getStatusCode());
			HttpEntity entity = response.getEntity();
			JSONObject jsonObject = new JSONObject(EntityUtils.toString(entity));


			int numberOfHotels = 0;
			for(int i = 0;i<jsonObject.getJSONArray("data").length();i++){
				numberOfHotels += jsonObject.getJSONArray("data").getJSONObject(i).getJSONArray("hotels").length();
			}
			if(BumblebeeUtils.makeString("allHotels").equals("http://staging.treebohotels.com/bumblebee/v1/allhotels/")){
				Assert.assertTrue(numberOfHotels>Integer.parseInt(BumblebeeUtils.getFromProperty("hotelsStaging")), "Hotels expected to be greater than 100 but actual hotel count is :" + numberOfHotels);
				for(int i=0; i<jsonObject.getJSONArray("data").length();i++){
					Assert.assertTrue(jsonObject.getJSONArray("data").length()>Integer.parseInt(BumblebeeUtils.getFromProperty("citiesStaging")), "Number of cities are more than 15");
				}
			}
			else{
				Assert.assertTrue(numberOfHotels>Integer.parseInt(BumblebeeUtils.getFromProperty("hotelsProd")), "Hotels expected to be greater than 100 but actual hotel count is :" + numberOfHotels);
				for(int i=0; i<jsonObject.getJSONArray("data").length();i++){
					Assert.assertTrue(jsonObject.getJSONArray("data").length()>Integer.parseInt(BumblebeeUtils.getFromProperty("citiesProd")), "Number of cities are more than 15");
				}
			}
		}
		finally{
			response.close();
			httpClient.close();
		}
	}






}