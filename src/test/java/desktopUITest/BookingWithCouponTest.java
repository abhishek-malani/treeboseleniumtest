package desktopUITest;

import java.text.ParseException;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import org.testng.Assert;
import org.testng.asserts.SoftAssert;

import utils.CommonUtils;
import base.TestBaseSetUp;
import desktopUIPageObjects.HomePage;
import desktopUIPageObjects.HotelResultsPage;
import desktopUIPageObjects.ItineraryPage;
import desktopUIPageObjects.ConfirmationPage;
import desktopUIPageObjects.HotelDetailsPage;
import desktopUIPageObjects.BookingHistory;
import base.BrowserHelper;

@Test(singleThreaded=true)
public class BookingWithCouponTest extends TestBaseSetUp {

	private HomePage homePage;
	private HotelResultsPage hotelResultsPage;
	private ItineraryPage itineraryPage;
	private ConfirmationPage confirmationPage;
	private HotelDetailsPage hotelDetailsPage;
	private BookingHistory bookingHistory;
	private BrowserHelper browserHelper;

	/**
	 * This test applies coupon in HD page and go on booking path
	 * 
	 * @throws ParseException
	 */
	@Test(groups = { "Sanity", "Coupon","HotelBooking" })
	public void testHotelBookingWithCouponThroughHDPage() throws ParseException {
		homePage = new HomePage();
		hotelResultsPage = new HotelResultsPage();
		hotelDetailsPage = new HotelDetailsPage();
		itineraryPage = new ItineraryPage();
		confirmationPage = new ConfirmationPage();
		browserHelper = new BrowserHelper();
		CommonUtils utils = new CommonUtils();
		SoftAssert verify = new SoftAssert();

		String location = utils.getProperty("Country"); // "India"; // "India";
		String testHotel = utils.getProperty("DummyHotel"); // "Dummy";
		String testUrl = utils.getProperty("TestUrl");
		int hotelIndex = 0;
		int checkInFromCurrentDate = utils.getRandomNumber(30, 45);
		int checkOutFromCurrentDate = checkInFromCurrentDate + 1;
		int roomIndex = 1;
		String guestName = utils.getProperty("GuestName"); // "Test";
		String guestEmail = utils.getProperty("TestBookingAccount"); // "";
		String guestMobile = utils.getProperty("GuestMobile");// "9000000000";
		String guestPassword = utils.getProperty("TestLoginPassword"); // "password";
		String coupon = utils.getProperty("Coupon");
		String couponDiscountPercent = utils.getProperty("CouponDiscountPercent");

		// Home Page is Displayed
		homePage.verifyHomePagePresence();
		homePage.signInAsTreeboMember(guestEmail, guestPassword);
		// Do search and get checkin checkout dates
		homePage.doSearch(location, checkInFromCurrentDate, checkOutFromCurrentDate, roomIndex);
		hotelResultsPage.verifyHotelResultsPagePresence();

		if (browserHelper.getCurrentUrl().contains(testUrl)) {
			hotelIndex = hotelResultsPage.getHotelIndexByHotelName(testHotel);
		} else {
			hotelIndex = hotelResultsPage.getHotelIndexByHotelAvailable();
		}

		String hotelName = hotelResultsPage.getHotelNameByIndex(hotelIndex);
		String hotelRoomRate = hotelResultsPage.getHotelRoomRateByIndex(hotelIndex);
		String hotelAddress = hotelResultsPage.getHotelAddressByIndex(hotelIndex);
		System.out.println(" Hotel Name:" + hotelName + " ,Hotel room rate : " + hotelRoomRate + " ,Hotel Address:"
				+ hotelAddress);

		hotelResultsPage.clickHotelNameByIndex(hotelIndex);
		hotelDetailsPage.verifyHotelDetailsPagePresence();
		hotelDetailsPage.ensureRoomAvailableInHDPage();

		double[] hotelPriceInfoHD = hotelDetailsPage.getHotelPriceBreakUpInHD();
		int sumOfRateTaxDiscHDPage = (int) Math.round(hotelPriceInfoHD[0] + hotelPriceInfoHD[1] - hotelPriceInfoHD[2]);
		int hotelRoomRateInHD = hotelDetailsPage.getHotelTotalPriceInHD();
		verify.assertTrue(Math.abs((int) Math.round(hotelPriceInfoHD[3]) - sumOfRateTaxDiscHDPage) <= 1,
				"Total : " + (int) Math.round(hotelPriceInfoHD[3]) + " and sum of room price, tax and discount :"
						+ sumOfRateTaxDiscHDPage + " does not match");
		verify.assertTrue(Math.abs(hotelRoomRateInHD - (int) Math.round(hotelPriceInfoHD[3])) <= 1,
				" Total Displayed in HD : " + hotelRoomRateInHD + " and in break up: "
						+ (int) Math.round(hotelPriceInfoHD[3]) + " not same");

		// Apply coupon
		hotelDetailsPage.applyCouponInHDPage(coupon);
		int expectedCouponDiscount = (int) Math
				.round((hotelPriceInfoHD[0] * Integer.parseInt(couponDiscountPercent)) / 100);
		int displayedCouponDiscount = hotelDetailsPage.getCouponDiscountDisplayed();
		double[] discountedHotelPriceInfoHD = hotelDetailsPage.getHotelPriceBreakUpInHD();

		verify.assertTrue(Math.abs(displayedCouponDiscount - expectedCouponDiscount) <= 1, "Displayed coupon discount: "
				+ displayedCouponDiscount + " not same as expected :" + expectedCouponDiscount);
		hotelDetailsPage.clickBookNowInHotelsDetailsPage();
		itineraryPage.verifyItineraryPagePresence();

		double[] hotelPriceInfoItineraryPage = itineraryPage.getHotelPriceInfoInItineraryPage();
		verify.assertTrue(
				Math.abs((int) Math.round(discountedHotelPriceInfoHD[3])
						- (int) Math.round(hotelPriceInfoItineraryPage[3])) <= 1,
				"Total in HD : " + (int) Math.round(discountedHotelPriceInfoHD[3]) + " and Itinerary page : "
						+ (int) Math.round(hotelPriceInfoItineraryPage[3]) + " varies");
		verify.assertTrue(Math.abs((int) Math.round(hotelPriceInfoItineraryPage[2]) - displayedCouponDiscount) <= 1,
				"coupon amount mismatch on itineray page :" + (int) Math.round(hotelPriceInfoItineraryPage[2]) + " but "
						+ displayedCouponDiscount);

		itineraryPage.bookWithPayAtHotel();
		confirmationPage.verifyConfirmationPagePresence();
		String bookingId = confirmationPage.getBookingId();
		double[] hotelPriceInfoConfirmationPage = confirmationPage.getHotelPriceInfoInConfirmationPage();

		verify.assertTrue(
				Math.abs((int) Math.round(hotelPriceInfoConfirmationPage[3])
						- (int) Math.round(hotelPriceInfoItineraryPage[3])) <= 101,
				"Total in conf page :" + hotelPriceInfoConfirmationPage[3] + " is not same as Total in itinerary page :"
						+ hotelPriceInfoItineraryPage[3]);
		verify.assertTrue(Math.abs((int) Math.round(hotelPriceInfoConfirmationPage[2]) - displayedCouponDiscount) <= 1,
				"coupon amount mismatch on confirmation page " + (int) Math.round(hotelPriceInfoConfirmationPage[2])
						+ " but " + displayedCouponDiscount);

		verify.assertEquals(guestName, confirmationPage.getGuestNameInConfPage());
		verify.assertEquals(guestEmail, confirmationPage.getGuestEmailInConfPage());
		verify.assertEquals(guestMobile, confirmationPage.getGuestMobileInConfPage());
		verify.assertEquals(hotelName, confirmationPage.getHotelNameInConfirmationPage());
		verify.assertAll();
	}

	/**
	 * This test applies coupon in Itinerary page and go on booking path
	 * 
	 * @throws ParseException
	 */
	@Test(groups = { "Sanity", "Coupon","HotelBooking" })
	public void testHotelBookingWithCouponThroughItineraryPage() throws ParseException {
		homePage = new HomePage();
		hotelResultsPage = new HotelResultsPage();
		hotelDetailsPage = new HotelDetailsPage();
		itineraryPage = new ItineraryPage();
		confirmationPage = new ConfirmationPage();
		browserHelper = new BrowserHelper();
		CommonUtils utils = new CommonUtils();
		SoftAssert verify = new SoftAssert();

		String location = utils.getProperty("Country"); // "India"; // "India";
		int checkInFromCurrentDate = utils.getRandomNumber(30, 45);
		int checkOutFromCurrentDate = checkInFromCurrentDate + 1;
		int roomIndex = 1;
		String testHotel = utils.getProperty("DummyHotel"); // "Dummy";
		String testUrl = utils.getProperty("TestUrl");
		int hotelIndex = 0;
		String guestName = utils.getProperty("GuestName"); // "Test";
		String guestEmail = utils.getProperty("TestBookingAccount"); // "";
		String guestMobile = utils.getProperty("GuestMobile");// "9000000000";
		String guestPassword = utils.getProperty("TestLoginPassword"); // "password";
		String coupon = utils.getProperty("Coupon");
		String couponDiscountPercent = utils.getProperty("CouponDiscountPercent");
		String[] checkInCheckOutDates = new String[2];

		// Home Page is Displayed
		homePage.verifyHomePagePresence();
		homePage.signInAsTreeboMember(guestEmail, guestPassword);
		// Do search and get checkin checkout dates
		checkInCheckOutDates = homePage.doSearch(location, checkInFromCurrentDate, checkOutFromCurrentDate, roomIndex);
		hotelResultsPage.verifyHotelResultsPagePresence();

		if (browserHelper.getCurrentUrl().contains(testUrl)) {
			hotelIndex = hotelResultsPage.getHotelIndexByHotelName(testHotel);
		} else {
			hotelIndex = hotelResultsPage.getHotelIndexByHotelAvailable();
		}

		String hotelName = hotelResultsPage.getHotelNameByIndex(hotelIndex);
		String hotelRoomRate = hotelResultsPage.getHotelRoomRateByIndex(hotelIndex);
		String hotelAddress = hotelResultsPage.getHotelAddressByIndex(hotelIndex);
		System.out.println(" Hotel Name:" + hotelName + " ,Hotel room rate : " + hotelRoomRate + " ,Hotel Address:"
				+ hotelAddress);

		hotelResultsPage.clickQuickBookByIndex(hotelIndex);
		itineraryPage.verifyItineraryPagePresence();

		// Get Price
		double[] hotelPriceInfoItineraryPage = itineraryPage.getHotelPriceInfoInItineraryPage();
		// Apply coupon
		itineraryPage.applyCouponInItineraryPage(coupon);
		double[] discountedHotelPriceInfoItineraryPage = itineraryPage.getHotelPriceInfoInItineraryPage();

		int expectedCouponDiscount = (int) Math
				.round((hotelPriceInfoItineraryPage[0] * Integer.parseInt(couponDiscountPercent)) / 100);
		int displayedCouponDiscount = itineraryPage.getCouponDiscountDisplayed();

		verify.assertTrue(Math.abs(displayedCouponDiscount - expectedCouponDiscount) <= 1, "Displayed coupon discount :"
				+ displayedCouponDiscount + " not same as expected :" + expectedCouponDiscount);
		verify.assertTrue(
				Math.abs((int) Math.round(discountedHotelPriceInfoItineraryPage[2]) - displayedCouponDiscount) <= 1,
				"coupon amount mismatch on itineray page :" + (int) Math.round(discountedHotelPriceInfoItineraryPage[2])
						+ " but " + displayedCouponDiscount);
		int expectedTotalWithDiscount = (int) Math.round(discountedHotelPriceInfoItineraryPage[0])
				+ (int) Math.round(discountedHotelPriceInfoItineraryPage[1])
				- (int) Math.round(discountedHotelPriceInfoItineraryPage[2]);
		verify.assertTrue(
				Math.abs(expectedTotalWithDiscount - (int) Math.round(discountedHotelPriceInfoItineraryPage[3])) <= 1,
				"Mismatch in total in Itinerary page :" + expectedTotalWithDiscount + " but "
						+ (int) Math.round(discountedHotelPriceInfoItineraryPage[3]));

		itineraryPage.bookWithPayAtHotel();
		confirmationPage.verifyConfirmationPagePresence();
		String bookingId = confirmationPage.getBookingId();
		double[] hotelPriceInfoConfirmationPage = confirmationPage.getHotelPriceInfoInConfirmationPage();

		verify.assertTrue(Math.abs((int) Math.round(hotelPriceInfoConfirmationPage[3]) -
				(int) Math.round(discountedHotelPriceInfoItineraryPage[3])) <= 101,
				"Total in conf page :" + (int) Math.round(hotelPriceInfoConfirmationPage[3])
						+ " is not same as Total in itinerary page :"
						+ (int) Math.round(discountedHotelPriceInfoItineraryPage[3]));
		verify.assertTrue(Math.abs((int) Math.round(hotelPriceInfoConfirmationPage[2]) - displayedCouponDiscount) <= 1,
				"coupon amount mismatch on confirmation page :" + (int) Math.round(hotelPriceInfoConfirmationPage[2]) + " but " + displayedCouponDiscount);

		verify.assertEquals(guestName, confirmationPage.getGuestNameInConfPage());
		verify.assertEquals(guestEmail, confirmationPage.getGuestEmailInConfPage());
		verify.assertEquals(guestMobile, confirmationPage.getGuestMobileInConfPage());
		verify.assertEquals(hotelName, confirmationPage.getHotelNameInConfirmationPage());
		verify.assertAll();
	}

	/**
	 * Booking with coupon for multiple guests and rooms Room 2 Guests - 2
	 * Adults + 1 Child , 2 Adults + 2 child
	 */

	@Test(groups = { "Sanity", "Coupon" })
	public void testHotelBookingWithCouponForMultipleRoomAndGuestsThroughItineraryPage() throws ParseException {
		homePage = new HomePage();
		hotelResultsPage = new HotelResultsPage();
		hotelDetailsPage = new HotelDetailsPage();
		itineraryPage = new ItineraryPage();
		confirmationPage = new ConfirmationPage();
		browserHelper = new BrowserHelper();
		CommonUtils utils = new CommonUtils();
		SoftAssert verify = new SoftAssert();

		String location = utils.getProperty("Country"); // "India"; // "India";
		int checkInFromCurrentDate = utils.getRandomNumber(30, 45);
		int checkOutFromCurrentDate = checkInFromCurrentDate + 1;
		int[][] roomIndex = { { 1, 2, 1 }, { 2, 2, 2 } };
		String testHotel = utils.getProperty("DummyHotel"); // "Dummy";
		String testUrl = utils.getProperty("TestUrl");
		int hotelIndex = 0;
		String guestName = utils.getProperty("GuestName"); // "Test";
		String guestEmail = utils.getProperty("TestBookingAccount"); // "";
		String guestMobile = utils.getProperty("GuestMobile");// "9000000000";
		String guestPassword = utils.getProperty("TestLoginPassword"); // "password";
		String coupon = utils.getProperty("Coupon");
		String couponDiscountPercent = utils.getProperty("CouponDiscountPercent");
		String[] checkInCheckOutDates = new String[2];

		// Home Page is Displayed
		homePage.verifyHomePagePresence();
		homePage.signInAsTreeboMember(guestEmail, guestPassword);
		// Do search and get checkin checkout dates
		checkInCheckOutDates = homePage.doSearch(location, checkInFromCurrentDate, checkOutFromCurrentDate, roomIndex);
		hotelResultsPage.verifyHotelResultsPagePresence();

		if (browserHelper.getCurrentUrl().contains(testUrl)) {
			hotelIndex = hotelResultsPage.getHotelIndexByHotelName(testHotel);
		} else {
			hotelIndex = hotelResultsPage.getHotelIndexByHotelAvailable();
		}

		String hotelName = hotelResultsPage.getHotelNameByIndex(hotelIndex);
		String hotelRoomRate = hotelResultsPage.getHotelRoomRateByIndex(hotelIndex);
		String hotelAddress = hotelResultsPage.getHotelAddressByIndex(hotelIndex);
		System.out.println(" Hotel Name:" + hotelName + " ,Hotel room rate : " + hotelRoomRate + " ,Hotel Address:"
				+ hotelAddress);

		hotelResultsPage.clickQuickBookByIndex(hotelIndex);
		itineraryPage.verifyItineraryPagePresence();

		// Get Price
		double[] hotelPriceInfoItineraryPage = itineraryPage.getHotelPriceInfoInItineraryPage();
		// Apply coupon
		itineraryPage.applyCouponInItineraryPage(coupon);
		double[] discountedHotelPriceInfoItineraryPage = itineraryPage.getHotelPriceInfoInItineraryPage();

		int expectedCouponDiscount = (int) Math
				.round((hotelPriceInfoItineraryPage[0] * Integer.parseInt(couponDiscountPercent)) / 100);
		int displayedCouponDiscount = itineraryPage.getCouponDiscountDisplayed();

		verify.assertTrue(Math.abs(displayedCouponDiscount - expectedCouponDiscount) <= 1,
				"Displayed coupon discount : "+ displayedCouponDiscount + " not same as expected : " + expectedCouponDiscount);
		verify.assertTrue(Math.abs((int) Math.round(discountedHotelPriceInfoItineraryPage[2]) - displayedCouponDiscount) <= 1,
				"coupon amount mismatch on itineray page " +  (int) Math.round(discountedHotelPriceInfoItineraryPage[2]) + " but :" + displayedCouponDiscount);
		int expectedTotalWithDiscount = (int) Math.round(discountedHotelPriceInfoItineraryPage[0])
				+ (int) Math.round(discountedHotelPriceInfoItineraryPage[1])
				- (int) Math.round(discountedHotelPriceInfoItineraryPage[2]);
		verify.assertTrue(Math.abs(expectedTotalWithDiscount - (int) Math.round(discountedHotelPriceInfoItineraryPage[3])) <= 1,
				"Mismatch in total in Itinerary page. Expected :" + expectedTotalWithDiscount + " but " + (int) Math.round(discountedHotelPriceInfoItineraryPage[3]));

		itineraryPage.bookWithPayAtHotel();
		confirmationPage.verifyConfirmationPagePresence();
		String bookingId = confirmationPage.getBookingId();
		double[] hotelPriceInfoConfirmationPage = confirmationPage.getHotelPriceInfoInConfirmationPage();

		verify.assertTrue(Math.abs((int) Math.round(hotelPriceInfoConfirmationPage[3]) -
				(int) Math.round(discountedHotelPriceInfoItineraryPage[3])) <= 101,
				"Total in conf page :" + (int) Math.round(hotelPriceInfoConfirmationPage[3])
						+ " is not same as Total in itinerary page :"
						+ (int) Math.round(discountedHotelPriceInfoItineraryPage[3]));
		verify.assertTrue(Math.abs((int) Math.round(hotelPriceInfoConfirmationPage[2]) - displayedCouponDiscount) <= 1,
				"coupon amount mismatch on confirmation page " + (int) Math.round(hotelPriceInfoConfirmationPage[2]) + " but "+ displayedCouponDiscount);

		verify.assertEquals(guestName, confirmationPage.getGuestNameInConfPage());
		verify.assertEquals(guestEmail, confirmationPage.getGuestEmailInConfPage());
		verify.assertEquals(guestMobile, confirmationPage.getGuestMobileInConfPage());
		verify.assertEquals(hotelName, confirmationPage.getHotelNameInConfirmationPage());
		verify.assertAll();
	}

//	@AfterMethod(alwaysRun = true)
//	public void cancelBooking() {
//		try {
//			homePage = new HomePage();
//			browserHelper = new BrowserHelper();
//			bookingHistory = new BookingHistory();
//			CommonUtils utils = new CommonUtils();
//			browserHelper.openUrl(browserHelper.getCurrentUrl());
//
//			if (!homePage.isUserSignedIn()) {
//				System.out.println("User is not signed in! Doing sign-in to cancel booking...");
//				homePage.signInAsTreeboMember(utils.getProperty("TestBookingAccount"),
//						utils.getProperty("TestLoginPassword"));
//			} else {
//				System.out.println("User is already signed in!");
//			}
//			bookingHistory.cancelAllBookings();
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//	}
}
