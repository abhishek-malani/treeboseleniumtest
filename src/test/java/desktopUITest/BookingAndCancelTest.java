package desktopUITest;

import java.sql.SQLException;
import java.text.ParseException;

import org.apache.commons.lang3.RandomStringUtils;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import utils.CommonUtils;
import utils.DBUtils;
import base.TestBaseSetUp;
import desktopUIPageObjects.HomePage;
import desktopUIPageObjects.BookingHistory;
import desktopUIPageObjects.HotelResultsPage;
import desktopUIPageObjects.ItineraryPage;
import desktopUIPageObjects.ConfirmationPage;
import base.BrowserHelper;

public class BookingAndCancelTest extends TestBaseSetUp {

	private HomePage homePage;
	private HotelResultsPage hotelResultsPage;
	private ItineraryPage itineraryPage;
	private ConfirmationPage confirmationPage;
	private BookingHistory bookingHistory;
	private BrowserHelper browserHelper;

	/***
	 * Book a treebo hotel by registering to the website in Itinerary page
	 * Cancel the booking
	 * 
	 * @throws InterruptedException
	 * @throws SQLException 
	 */
	@Test(groups = { "HotelBooking", "Sanity" })
	public void testHotelBookingAndCancellationThroughQuickBookPayAtHotelSignUpInHomePage()
			throws ParseException, InterruptedException, SQLException {
		homePage = new HomePage();
		hotelResultsPage = new HotelResultsPage();
		itineraryPage = new ItineraryPage();
		confirmationPage = new ConfirmationPage();
		browserHelper = new BrowserHelper();
		bookingHistory = new BookingHistory();
		CommonUtils utils = new CommonUtils();
		DBUtils dbUtills = new DBUtils();
		SoftAssert verify = new SoftAssert();

		String name = "Test";
		String email = browserHelper.getDisposableEmail();
		String mobile = Integer.toString(utils.getRandomNumber(2, 6)) + RandomStringUtils.randomNumeric(9);
		String password = "password";

		String location = utils.getProperty("Country"); // "India";
		String testHotel = utils.getProperty("DummyHotel"); // "Dummy";
		String testUrl = utils.getProperty("TestUrl");
		int hotelIndex = 0;
		int checkInFromCurrentDate = utils.getRandomNumber(30, 45);
		int checkOutFromCurrentDate = checkInFromCurrentDate + 1;
		int roomIndex = 1;
		String[] checkInCheckOutDates = new String[2];

		// Home Page is Displayed
		homePage.verifyHomePagePresence();
		
		// login link opens Signup pop up
		homePage.clickOnSignUpLink();
		homePage.verifySignUpPopUpPresence();
		homePage.enterSignUpForm(name, mobile, email, password);
        homePage.verifyUserIsSignedIn();
        
		// Do search and get checkin checkout dates
		checkInCheckOutDates = homePage.doSearch(location, checkInFromCurrentDate, checkOutFromCurrentDate, roomIndex);
		hotelResultsPage.verifyHotelResultsPagePresence();

		if (browserHelper.getCurrentUrl().contains(testUrl)) {
			hotelIndex = hotelResultsPage.getHotelIndexByHotelName(testHotel);
		} else {
			hotelIndex = hotelResultsPage.getHotelIndexByHotelAvailable();
		}

		String hotelName = hotelResultsPage.getHotelNameByIndex(hotelIndex);
		String hotelRoomRate = hotelResultsPage.getHotelRoomRateByIndex(hotelIndex);
		String hotelAddress = hotelResultsPage.getHotelAddressByIndex(hotelIndex);
		System.out.println(
				"Hotel Name:" + hotelName + " Hotel room rate : " + hotelRoomRate + " Hotel Address:" + hotelAddress);

		hotelResultsPage.clickQuickBookByIndex(hotelIndex);
		itineraryPage.verifyItineraryPagePresence();
        itineraryPage.bookWithPayAtHotel();
        
        Thread.sleep(3000);
		//verify OTP pop up
		verify.assertTrue(itineraryPage.doesOTPPopupDisplayed(),"OTP pop up is not displayed");
		
		//check OTP pop up title
		verify.assertTrue(itineraryPage.doesOTPPopupTitleDisplayed(),"OTP Title is not displayed");
		
		//check resend sms  clickable
		verify.assertTrue(itineraryPage.doesClickableResendSms(),"Resend sms is not clickable");
		
		//check change number clickable
		verify.assertTrue(itineraryPage.doesClickableChangeNumber(),"change number is not clickable");
		
		//extract mobile number from OTP pop up
		String popupMobile=itineraryPage.extractMobileNumber();
		
		//mobile number verification
		verify.assertTrue(itineraryPage.doesEqualBothString(popupMobile.trim(),mobile.trim()),"Mobile number is not equal");
		
		String otp = null;

	    otp = dbUtills.getOTP(mobile.trim());
	    itineraryPage.enterOTP(otp.trim());
	    
	    verify.assertTrue(itineraryPage.doesDoneButtonOnOTPPopupDisplayed(), "Done button is not displayed");
	    itineraryPage.clickOnDoneButtonForOTPVerification();
	    //verify.assertFalse(itineraryPage.doesOTPPopupDisplay
	    
	    Thread.sleep(5000);

		confirmationPage.verifyConfirmationPagePresence();
		String bookingId = confirmationPage.getBookingId();

		verify.assertEquals(name, confirmationPage.getGuestNameInConfPage());
		verify.assertEquals(email, confirmationPage.getGuestEmailInConfPage());
		verify.assertEquals(mobile, confirmationPage.getGuestMobileInConfPage());
		verify.assertEquals(hotelName, confirmationPage.getHotelNameInConfirmationPage());

		System.out.println(utils.formatDate(checkInCheckOutDates[0]).toUpperCase());
		System.out.println(confirmationPage.getHotelCheckInDateInConfirmationPage().toUpperCase());
		verify.assertEquals(utils.formatDate(checkInCheckOutDates[0]).toUpperCase(),
				confirmationPage.getHotelCheckInDateInConfirmationPage().toUpperCase());
		verify.assertEquals(utils.formatDate(checkInCheckOutDates[1]).toUpperCase(),
				confirmationPage.getHotelCheckOutDateInConfirmationPage().toUpperCase());

		bookingHistory.cancelAllBookings();
		verify.assertFalse(bookingHistory.isConfirmedBookingAvailable(),
				"Booking should have been cancelled! " + " Email : " + email);

		verify.assertAll();
	}
	}