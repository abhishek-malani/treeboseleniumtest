package desktopUITest;

import java.io.FileNotFoundException;
import java.io.IOException;

import org.testng.annotations.Test;

import base.TestBaseSetUp;
import desktopUIPageObjects.BookingHistory;
import desktopUIPageObjects.HomePage;
import utils.CommonUtils;

public class CancelBookingTest extends TestBaseSetUp{
	
	private HomePage homePage;
	private BookingHistory bookingHistory;

	@Test(groups = { "CancelBooking" })
	public void testCancelAllBookings() throws FileNotFoundException, IOException {
		homePage = new HomePage();
		bookingHistory = new BookingHistory();
		CommonUtils utils = new CommonUtils();
		
		homePage.signInAsTreeboMember(utils.getProperty("TestBookingAccount"),
				utils.getProperty("TestLoginPassword"));
		bookingHistory.cancelAllBookings();
	}
}
