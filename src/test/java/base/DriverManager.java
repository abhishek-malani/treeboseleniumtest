package base;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;

import utils.CommonUtils;

public class DriverManager {

	private DriverManager() {
		// Do nothing
	}

	private static DriverManager driverManager = new DriverManager();

	public static DriverManager getInstance() {
		return driverManager;
	}

	// To use as e.g.
	// WebDriver driver = DriverManager.getInstance().getDriver();
	// driver.getTitle();
	private static ThreadLocal<RemoteWebDriver> driver = new ThreadLocal<RemoteWebDriver>() {
		@Override
		protected RemoteWebDriver initialValue() {
			System.setProperty("webdriver.chrome.driver", "/usr/local/share/chromedriver");
			CommonUtils utils = new CommonUtils();
			String browser = System.getProperty("browser");
			String device = System.getProperty("device");
			String host = System.getProperty("host");
			String deviceId = System.getProperty("deviceId");
			
			DesiredCapabilities cap = new DesiredCapabilities();
			
			if (device.equals("mobile")){
				try {
					Thread.sleep(30000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				String appUrl = utils.getProperty("appUrlPath");
				
			    cap.setCapability("deviceName", "Android Emulator");
			    cap.setCapability("platformName", "Android");
			    cap.setCapability("automationName", "Appium");
			    cap.setCapability("browserName", "");
			    cap.setCapability("app", appUrl);
			    cap.setCapability("full-reset", true);
			    cap.setCapability("newCommandTimeout", "120");
			    cap.setCapability("command-timeout", "120");
				
			    //capabilities.setCapability("avd", "avd");

			    // for real device, attach device, and run adb devices, and copy the number here
			    if (!deviceId.isEmpty()) {
			      cap.setCapability("udid", deviceId);
			    }
			}else{
				if (browser == null || browser.isEmpty()) {
					browser = "chrome";
					System.out.println("No browser provided! defaulting to " + browser);
				} else {
					System.out.println("Browser : " + browser);
				}

				if (browser.equals("firefox")) {
					cap = DesiredCapabilities.firefox();
					cap.setBrowserName("firefox");
				} else if (browser.equals("chrome")) {
					cap = DesiredCapabilities.chrome();
					cap.setBrowserName("chrome");
				} else if (browser.equals("chromeMobile")) {
					Map<String, String> mobileEmulation = new HashMap<String, String>();
					mobileEmulation.put("deviceName", "Google Nexus 5");

					Map<String, Object> chromeOptions = new HashMap<String, Object>();
					chromeOptions.put("mobileEmulation", mobileEmulation);
					cap = DesiredCapabilities.chrome();
					cap.setCapability(ChromeOptions.CAPABILITY, chromeOptions);
				}
			}



			URL url = null;
			try {
				url = new URL("http://172.40.20.159:4444/wd/hub");
			} catch (MalformedURLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			if (host == null || host.isEmpty()) {
				host = "grid";
			}

			if (host.equals("grid")) {
				return new RemoteWebDriver(url, cap);
			} else if (host.equals("localhost")) {
				if (device.equals("mobile")){
					try {
						return new RemoteWebDriver(new URL("http://127.0.0.1:4723/wd/hub"), cap);
					} catch (MalformedURLException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}else{
					if (browser.equals("firefox")) {
						return new FirefoxDriver(cap);
					} else if (browser.equals("chrome")) {
						return new ChromeDriver(cap);
					} else if (browser.equals("chromeMobile")) {
						return new ChromeDriver(cap);
					}
				}

			}
			return null;
		}
	};

	public WebDriver getDriver() {
		return driver.get();
	}

	public void removeDriver() {
		driver.get().quit();
		driver.remove();
	}
}