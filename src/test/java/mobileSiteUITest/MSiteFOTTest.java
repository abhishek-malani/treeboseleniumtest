package mobileSiteUITest;

import java.text.ParseException;

import org.testng.Assert;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;
import utils.CommonUtils;
import base.BrowserHelper;
import base.TestBaseSetUp;
import mobileSiteUIPageObjects.FOTScreen;
import mobileSiteUIPageObjects.HomeScreen;

/*
 * Register as FOT 
 * While click on Schedule Audit, displays message that its available only on desktop
 */

/*
 * Login as FOT 
 * While click on Schedule Audit, displays message that its available only on desktop
 */
public class MSiteFOTTest extends TestBaseSetUp {
	private HomeScreen homePage;
	private FOTScreen fotPage;
	private BrowserHelper browserHelper;
	
     /**
     *     
     * @throws ParseException
     * open homepage
     * open FOT page
     * click on join us
     * sign up details
     * complete signup process and click on schedule audit link
     * got to book treebo page
     */
	@Test(groups = { "FOTM", "MSite" })
	public void testFOTRegistration() throws ParseException {
		homePage = new HomeScreen();
		fotPage = new FOTScreen();
		SoftAssert verify = new SoftAssert();

		// Home Page is Displayed
		homePage.isHomeScreenDisplayed();
		fotPage.openFOTPage();
		//click on join us and entered all details 
		fotPage.fotSignUp();
		Assert.assertTrue(fotPage.isScheduleFirstAuditDisplayed(), "Schedule first audit is not displayed");
		fotPage.clickScheduleFirstAudit();
		//redirected to book a treebo page
		fotPage.doesClickBookATreeboOpensTreeboLandingPageafterScheduleAudit();
		verify.assertAll();

	}

	/**
	 * FOT Rejected User - Home Page - Book a treebo link is displayed -
	 * Clicking on book a treebo link opens treebo landing page
	 */

	@Test(groups = { "FOTM", "MSite" })
	public void testFailedFOTUserLogin() throws ParseException {
		homePage = new HomeScreen();
		fotPage = new FOTScreen();
		CommonUtils utils = new CommonUtils();
		SoftAssert verify = new SoftAssert();

		String failedUserEmail = utils.getProperty("FOTRejectedUser");
		String password = utils.getProperty("TestLoginPassword");

		// Home Page is Displayed
		homePage.isHomeScreenDisplayed();
		fotPage.openFOTPage();
		Assert.assertTrue(fotPage.isLoginDisplayedOnFOTHomePage(), "Login did not displayed on FOT home page");
		fotPage.clickonFOTlogin();
		fotPage.loginToFOT(failedUserEmail, password);
		verify.assertTrue(fotPage.doesBookATreeboDisplaysForFailedMFOT(), "Book A Treebo was not displayed!");
		fotPage.clickonBookTreeboForFailedUser();
		homePage.isHomeScreenDisplayed();
		verify.assertAll();

	}
    /**
    * @throws ParseException
    * open homepage
    * open FOT page
    * click on login 
    * login with credentials
    * click on schedule audit link
    * got to book treebo page
    *  */
	@Test(groups = { "FOTM","MSite"})
	public void ExistingFOTUserLogin() throws ParseException {
		homePage = new HomeScreen();
		fotPage = new FOTScreen();
		CommonUtils utils = new CommonUtils();
		SoftAssert verify = new SoftAssert();

		String email = utils.getProperty("FOTlogin"); 
		String password = utils.getProperty("TestLoginPassword");  
		
		// Home Page is Displayed
		homePage.isHomeScreenDisplayed();
		fotPage.openFOTPage();
        Assert.assertTrue(fotPage.isLoginDisplayedOnFOTHomePage(),"Login did not displayed on FOT home page");
        Assert.assertTrue(fotPage.isJoinUSDisplayedOnFOTHomePage(),"Join us did not displayed on FOT home page");
        fotPage.clickonFOTlogin();
        fotPage.loginToFOT(email, password);
        Assert.assertTrue(fotPage.isScheduleAuditDisplayedFOT(),"Schedule Audit link missing!");
        fotPage.clickScheduleAuditFOT();
        fotPage.doesClickBookATreeboOpensTreeboLandingPageafterScheduleAudit();
        verify.assertAll();
		
}
}
