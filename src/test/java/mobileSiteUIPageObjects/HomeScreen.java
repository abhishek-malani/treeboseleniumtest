package mobileSiteUIPageObjects;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.text.WordUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import base.BrowserHelper;
import base.DriverManager;
import utils.CommonUtils;

public class HomeScreen {
	private BrowserHelper browserHelper;
	
	private By searchHeader = By.xpath("//h2[contains(@class,'search-box__header')][contains(text(),'BOOK A TREEBO')]");
	private By searchInput = By.cssSelector("#searchInput");
	private By submitSearch = By.cssSelector("#searchSubmitBtn");
	private By checkin = By
			.xpath("//div[@id='calendar']//div[contains(@class,'dr-date dr-date-start search-box__title')]");
	private By checkout = By
			.xpath("//div[@id='calendar']//div[contains(@class,'dr-date dr-date-end search-box__title')]");
	private By calendar = By.xpath("//div[@class='dr-calendar']//ul[@class='dr-day-list']");
	private By calendarRightClick = By.cssSelector(".dr-right.icon-right");
	private By calendarLeftClick = By.cssSelector(".icon-back");
	private By availableDateInMonth = By
			.xpath("//div[@class='home-page' or contains(@class,'modal--search')][not(contains(@class,'hide'))]//ul[@class='dr-day-list']/li[not((contains(@class,'dr-outside') or contains(@class,'dr-fade')))]");
	private By checkOutDateXpath = By.xpath("//li[contains(@class,'dr-day dr-end dr-current')]");
	
	private By guestDetailField = By.cssSelector(".room-widget__total");
	private By customizeRoomWidgetCrossLink = By.cssSelector(".icon-cross");
	private By customizeRoomWidgetDoneButton = By.cssSelector(".js-modal__close");
	private By addRoomIcon = By.cssSelector(".icon-plus-circle");
	private By addAnotherRoom = By.xpath("//span[text()='Add another room']");
	private By removeRoomLink = By.xpath("//ul[@class='config-options-list']/li//a[@class='room-config__remove-link']");
	private By forgotLink =By.xpath("//a[@class='forgotpassword anchor pos-abs login__forgot-link']");
		
	/**
	 * Checks if home screen is current screen
	 * @return true if home screen else false
	 */
	
	public boolean isHomeScreenDisplayed(){
		WebDriver driver = DriverManager.getInstance().getDriver();
		browserHelper = new BrowserHelper();
		
		//check if 500 page
		if (is500Displayed()){
			Assert.fail("500 displayed instead of home screen for url " + browserHelper.getCurrentUrl());
		}
		
		try{
			WebDriverWait wait = new WebDriverWait(driver, 120);
			wait.until(ExpectedConditions.presenceOfElementLocated(searchHeader));
		}catch (Exception e){
			Assert.fail("Home Screen not displayed for url " + browserHelper.getCurrentUrl());
		}
		return driver.findElement(searchHeader).isDisplayed();
	}
	
	public void goToHomeScreen(){
		WebDriver driver = DriverManager.getInstance().getDriver();
		driver.findElement(By.cssSelector(".logo")).click();
	}
	
	public boolean isSpotLightDisplayed(){
		WebDriver driver = DriverManager.getInstance().getDriver();
		return (driver.findElements(By.cssSelector(".spotlight")).size() > 0);
	}
	
	public void openDrawer(){
		WebDriver driver = DriverManager.getInstance().getDriver();
		driver.findElement(By.cssSelector(".call__icon.icon-menu")).click();
	}
	
	public boolean isDrawerOpen(){
		WebDriver driver = DriverManager.getInstance().getDriver();
		return (driver.findElements(By.cssSelector(".page--open")).size() > 0);
	}
	
	public void closeDrawer(){
		WebDriver driver = DriverManager.getInstance().getDriver();
		browserHelper = new BrowserHelper();
		browserHelper.waitTime(2000);
		driver.findElement(By.cssSelector(".call__icon.icon-menu")).click();
	}
	
	public void clickOnHomeLink(){
		WebDriver driver = DriverManager.getInstance().getDriver();
		driver.findElement(By.cssSelector(".menu-nav__link[href='/']")).click();
	}
	
	public void clickOnSignOutLink(){
		WebDriver driver = DriverManager.getInstance().getDriver();
		browserHelper = new BrowserHelper();
		browserHelper.jsclick(driver.findElement(By.cssSelector(".menu-nav__link[href='/logout/']")));
		//driver.findElement(By.cssSelector(".menu-nav__link[href='/logout/']")).click();
	}
	
	public String getLoggedInUserText(){
		WebDriver driver = DriverManager.getInstance().getDriver();
		return driver.findElement(By.cssSelector("#user")).getText();
	}
	
	public boolean isUserSignedIn(){
		WebDriver driver = DriverManager.getInstance().getDriver();
		return (driver.findElements(By.cssSelector("#user")).size() > 0);
	}
	
	public void clickOnLogin(){
		WebDriver driver = DriverManager.getInstance().getDriver();
		browserHelper = new BrowserHelper();
		browserHelper.jsclick(driver.findElement(By.xpath("//a[@href='/login/']")));
		//driver.findElement(By.xpath("//a[@href='/login/']")).click();
	}
	
	public void clickOnSignUp(){
		WebDriver driver = DriverManager.getInstance().getDriver();
		browserHelper = new BrowserHelper();
		browserHelper.jsclick(driver.findElement(By.cssSelector(".menu-nav__link[href='/register/']")));
	    //driver.findElement(By.cssSelector(".menu-nav__link[href='/register/']")).click();
	}
	
	public void loginAsTreeboMember(String strEmail,String strPassword){
		System.out.println("Signin with : " + strEmail + " and password : " + strPassword);
		WebDriver driver = DriverManager.getInstance().getDriver();
		browserHelper = new BrowserHelper();
		driver.findElement(By.cssSelector("#loginEmailInput")).sendKeys(strEmail);
		driver.findElement(By.cssSelector("#loginPassowrdlInput")).sendKeys(strPassword);
		driver.findElement(By.cssSelector("#loginButton")).click();
		browserHelper.waitTime(3000);
	}
	
	public void signUpOnHomePage(String strName,String strMobile,String strEmail,String strPassword){
		WebDriver driver = DriverManager.getInstance().getDriver();
		browserHelper = new BrowserHelper();
		driver.findElement(By.cssSelector("#signupNamelInput")).sendKeys(strName);
		driver.findElement(By.cssSelector("#signupMobileInput")).sendKeys(strMobile);
		driver.findElement(By.cssSelector("#signupEmailInput")).sendKeys(strEmail);
		driver.findElement(By.cssSelector("#signupPassowrdlInput")).sendKeys(strPassword);
		driver.findElement(By.cssSelector("#signupButton")).click();
		waitForLoaderToDisappear();
		browserHelper.waitTime(3000);
	}
	
	public String[] registerOnHomePage(){
		CommonUtils utils = new CommonUtils();
		String name = utils.getProperty("GuestName"); //"Test";
//		String uniqueEmail = "test" + RandomStringUtils.randomAlphanumeric(15);
//		String email = uniqueEmail + "@gmail.com";
		browserHelper = new BrowserHelper();
		String email = browserHelper.getDisposableEmail();
		String mobile = Integer.toString(utils.getRandomNumber(2, 6)) + RandomStringUtils.randomNumeric(9);
		String password = utils.getProperty("TestLoginPassword"); //"password";
		
		signUpOnHomePage(name, mobile, email, password);
		String[] guestInfo = new String[]{name,mobile,email,password};
		return guestInfo;
	}	
	
	public void requestForForgotPassword(String strEmail){
		WebDriver driver = DriverManager.getInstance().getDriver();
		driver.findElement(By.cssSelector(".forgotpassword.anchor.pos-abs.login__forgot-link")).click();
		driver.findElement(By.cssSelector("#forgotEmailInput")).sendKeys(strEmail);
		driver.findElement(By.cssSelector("#forgotButton")).click();
	}
	
	public boolean forgotPasswordSuccessMessageDisplayed(){
		WebDriver driver = DriverManager.getInstance().getDriver();
		return (driver.findElements(By.cssSelector(".forgot__success.alert.alert--success")).size() > 0);
	}
	
	public void clickOnBackToLoginLink(){
		WebDriver driver = DriverManager.getInstance().getDriver();
		driver.findElement(By.cssSelector(".auth__login-back-link")).click();
	}
	
	/**
	 * select city which has been passed to it
	 * @param cityName
	 */
	
	public void selectCity(String cityName){
		WebDriver driver = DriverManager.getInstance().getDriver();
		WebDriverWait wait = new WebDriverWait(driver, 120);
		wait.until(ExpectedConditions.presenceOfElementLocated(searchInput));
		driver.findElement(searchInput).click();
		String cityOption = String.format("//option[text()='%s']", cityName);
		driver.findElement(By.xpath(cityOption)).click();
	}
	
	/**
	 * clicks on search button
	 */
	
	public void submitSearchButton(){
		browserHelper = new BrowserHelper();
		WebDriver driver = DriverManager.getInstance().getDriver();
		browserHelper.waitTime(2000);
		driver.findElement(submitSearch).click();
	}
	
	public void selectDate(int checkIn) {
		WebDriver driver = DriverManager.getInstance().getDriver();
		browserHelper = new BrowserHelper();
		WebDriverWait wait = new WebDriverWait(driver, 120);
		// number of days available in current month
		// first check if checkIn days available in current month
		if (checkIn <= getAvailableDatesInMonth()) {
			clickDate(checkIn);
		} else if (checkIn > getAvailableDatesInMonth()) {
			int currentMonthAvailableDate = getAvailableDatesInMonth();
		    driver.findElement(calendarRightClick).click();
			wait.until(ExpectedConditions.presenceOfElementLocated(calendar));
			int updatedCheckIn = checkIn - currentMonthAvailableDate;
			selectDate(updatedCheckIn);
		}
	}

	public void selectCheckOutDate(int checkInDate, int checkOutDate) {
		WebDriver driver = DriverManager.getInstance().getDriver();
		WebDriverWait wait = new WebDriverWait(driver, 120);

		// if its 1 night, i.e checkout -checkin = 1, checkout automatically
		// selected
		if ((checkOutDate - checkInDate) == 1) {
			driver.findElement(checkOutDateXpath).click();
		} else {
			// number of days available in current month after checkin
			// first check if checkIn days available in current month

			// Get checkin date number selected
			int checkIndateNumberInCalendar;
			int lastDate;
			
			//In case checkIn element is visible
			if((driver.findElements(By.xpath("//li[contains(@class,'dr-day dr-start')][not(contains(@class,'dr-fade'))][not(contains(@class,'dr-outside'))]")).size()>0))
			{
				

				checkIndateNumberInCalendar = Integer
						.parseInt(driver.findElement(By.xpath("//li[contains(@class,'dr-day dr-start')][not(contains(@class,'dr-fade'))][not(contains(@class,'dr-outside'))]")).getText());
			 
			}
			//In case checkIn element is not visible after switching month 
			else
			{
				// back to previous month 
				 driver.findElement(calendarLeftClick).click();
				  
				  wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.xpath("//li[contains(@class,'dr-day dr-start')][not(contains(@class,'dr-fade'))][not(contains(@class,'dr-outside'))]")));
				   
				 // extract checkIn
				 checkIndateNumberInCalendar = Integer
						.parseInt(driver.findElement(By.xpath("//li[contains(@class,'dr-day dr-start')][not(contains(@class,'dr-fade'))][not(contains(@class,'dr-outside'))]")).getText());
				
			    //back to next month calendar  
			    driver.findElement(calendarRightClick).click();
			    
			}
			 
			lastDate = Integer.parseInt(driver.findElement(By.xpath("//li[@class='dr-day'][last()]")).getText());
			
			int lastSelectedDate = Integer.parseInt(
					driver.findElement(By.xpath("//li[contains(@class,'dr-day dr-end dr-current')]")).getText());
			// last available date in calendar
			int lastAvailableDateIndex = (lastDate >= lastSelectedDate) ? lastDate : lastSelectedDate;
			int selectableDaysInCurrentMonth = lastAvailableDateIndex - checkIndateNumberInCalendar;

			// If checkout date is available in current month
			if ((checkOutDate - checkInDate) <= selectableDaysInCurrentMonth) {
				// Now need to select checkout date with reference to checkin
				// Date
				String xpathCheckOutDate = String.format("//li[not(contains(@class,'dr-fade'))][not(contains(@class,'dr-outside'))][text()='%d']",
						(checkIndateNumberInCalendar + checkOutDate - checkInDate));
				driver.findElement(By.xpath(xpathCheckOutDate)).click();
			} else {
				// Move to next month
				driver.findElement(calendarRightClick).click();
				int currentMonthAvailableDate = getAvailableDatesInMonth();
				// Available in this month
				int updatedCheckOutdate = checkOutDate - checkInDate - selectableDaysInCurrentMonth;

				if ((updatedCheckOutdate) <= currentMonthAvailableDate) {
					String xpathCheckOutDateOne = String.format("//li[text()='%d']", (updatedCheckOutdate));
					driver.findElement(By.xpath(xpathCheckOutDateOne)).click();
				} else {
					driver.findElement(calendarRightClick).click();
					String xpathCheckOutDateTwo = String.format("//li[text()='%d']",
							(updatedCheckOutdate - currentMonthAvailableDate));
					driver.findElement(By.xpath(xpathCheckOutDateTwo)).click();
				}
			}
		}
	}

	public void selectCheckInCheckOutDate(int checkInDate, int checkOutDate) {
		WebDriver driver = DriverManager.getInstance().getDriver();
		// select checkin date
		driver.findElement(checkin).click();
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		selectDate(checkInDate);
		selectCheckOutDate(checkInDate, checkOutDate);
	}
	
	public void clickDate(int dateToSelect) {
		WebDriver driver = DriverManager.getInstance().getDriver();
		String xpathDateToSelect = String.format(
				"//ul[contains(@class,'dr-day-list')]/li[not((contains(@class,'dr-outside') or contains(@class,'dr-fade')))][%d]",
				dateToSelect);
		System.out.println(xpathDateToSelect);
		driver.findElement(By.xpath(xpathDateToSelect)).click();
	}
	
	public int getAvailableDatesInMonth() {
		WebDriver driver = DriverManager.getInstance().getDriver();
		WebDriverWait wait = new WebDriverWait(driver, 120);
		wait.until(ExpectedConditions.presenceOfElementLocated(calendar));
		List<WebElement> dates = driver.findElements(availableDateInMonth);
		//System.out.println("Available dates in month is" + dates.size());
		return dates.size();
	}
	
	public String getEnteredCheckInDate() {
		WebDriver driver = DriverManager.getInstance().getDriver();
		System.out.println("Entered Checkin date is :" + driver.findElement(checkin).getText());
		return driver.findElement(checkin).getText();
	}

	public String getEnteredCheckOutDate() {
		WebDriver driver = DriverManager.getInstance().getDriver();
		System.out.println("Entered Checkout date is :" + driver.findElement(checkout).getText());
		return driver.findElement(checkout).getText();
	}
	
	/**
	 * Add rooms as required and selects adult and child for each room configuration
	 * as provided
	 * Example {{1,1,0},{2,2,0},{3,2,1}}
	 * @param roomAdultChild
	 */
	public void selectGuests(int[][] roomAdultChild){
		WebDriver driver = DriverManager.getInstance().getDriver();
		browserHelper = new BrowserHelper();
		browserHelper.waitTime(2000);
		driver.findElement(guestDetailField).click();
		// example array of array roomAdultChild - {{1,2,1},{2,2,0},{3,1,2}}
		// Get each room config i.e. count of Adult and Child
		// First get how many rooms are there
		int numOfRooms = roomAdultChild.length;
		System.out.println("Number of rooms :" + numOfRooms);
		System.out.println("Select adult and child for each room");
		for (int[] config : roomAdultChild) {
			String xpathAdult = String.format(
					"//ul[@class='config-options-list']/li[%d]//div[contains(@class,'room-config__adults')]//select",
					config[0]);
			String xpathSelectAdult = String.format(
					"//ul[@class='config-options-list']/li[%d]//div[contains(@class,'room-config__adults')]//select/option[%d]",
					config[0], config[1]);
			driver.findElement(By.xpath(xpathAdult)).click();
			driver.findElement(By.xpath(xpathSelectAdult)).click();
			String xpathChild = String.format(
					"//ul[@class='config-options-list']/li[%d]//div[contains(@class,'room-config__children')]//select",
					config[0]);
			String xpathSelectChild = String.format(
					"//ul[@class='config-options-list']/li[%d]//div[contains(@class,'room-config__children')]//select/option[%d]",
					config[0], (config[2] + 1));
			driver.findElement(By.xpath(xpathChild)).click();
			driver.findElement(By.xpath(xpathSelectChild)).click();
			if (numOfRooms > 1) {
				addRoomByAddLink();
				numOfRooms = numOfRooms - 1;
			}
		}
		driver.findElement(customizeRoomWidgetDoneButton).click();
	}
	
	public void addRoomByAddLink() {
		WebDriver driver = DriverManager.getInstance().getDriver();
		driver.findElement(addAnotherRoom).click();
	}

	public void addRoomByPlusSign() {
		WebDriver driver = DriverManager.getInstance().getDriver();
		driver.findElement(addRoomIcon).click();
	}

	public void removeRoomsInConfig() {
		WebDriver driver = DriverManager.getInstance().getDriver();
		System.out.println("Remove All rooms");
		while (driver.findElements(removeRoomLink).size() > 0) {
			driver.findElement(removeRoomLink).click();
		}
	}
	
	/**
	 * Default Search - Click on Search
	 * Default location - Bengaluru, default dates - current date, default guest and room {1,1}
	 */
	public String[] doSearch(){
		String[] checkInCheckOutDates = new String[2];
		checkInCheckOutDates[0] = getEnteredCheckInDate();
		checkInCheckOutDates[1] = getEnteredCheckOutDate();
		System.out.println("clicking search button");
		submitSearchButton();
		System.out.println("Hotel search done for default city Bengaluru with checkin date as: " + checkInCheckOutDates[0]
				+ " and checkout date as: " + checkInCheckOutDates[1]);
		return checkInCheckOutDates;
	}
	
	/**
	 * Does search with city name provided
	 * @param cityName
	 */
	
	public String[] doSearch(String cityName){
		String[] checkInCheckOutDates = new String[2];
		selectCity(cityName);
		checkInCheckOutDates[0] = getEnteredCheckInDate();
		checkInCheckOutDates[1] = getEnteredCheckOutDate();
		System.out.println("clicking search button");
		submitSearchButton();
		System.out.println("Hotel search done for: " + cityName + " with checkin date as: " + checkInCheckOutDates[0]
				+ " and checkout date as: " + checkInCheckOutDates[1]);
		return checkInCheckOutDates;
	}
	
	/**
	 * Does search with default guest and room - 1 Adult, 1 room
	 * @param cityName
	 * @param checkIn
	 * @param checkout
	 */
	public String[] doSearch(String cityName, int checkin, int checkout){
		String[] checkInCheckOutDates = new String[2];
		selectCity(cityName);
		selectCheckInCheckOutDate(checkin, checkout);
		checkInCheckOutDates[0] = getEnteredCheckInDate();
		checkInCheckOutDates[1] = getEnteredCheckOutDate();
		System.out.println("clicking search button");
		submitSearchButton();
		System.out.println("Hotel search done for: " + cityName + " with checkin date as: " + checkInCheckOutDates[0]
				+ " and checkout date as: " + checkInCheckOutDates[1]);
		return checkInCheckOutDates;
	}
	
	/**
	 * Does search with 
	 * @param cityName
	 * @param checkin
	 * @param checkout
	 * @param roomAdultChild
	 */
	
	public String[] doSearch(String cityName, int checkin, int checkout, int[][] roomAdultChild){
		String[] checkInCheckOutDates = new String[2];
		selectCity(cityName);
		selectCheckInCheckOutDate(checkin, checkout);
		checkInCheckOutDates[0] = getEnteredCheckInDate();
		checkInCheckOutDates[1] = getEnteredCheckOutDate();
		selectGuests(roomAdultChild);
		System.out.println("clicking search button");
		submitSearchButton();
		System.out.println("Hotel search done for: " + cityName + " with checkin date as: " + checkInCheckOutDates[0]
				+ " and checkout date as: " + checkInCheckOutDates[1]);
		return checkInCheckOutDates;
	}
	
	/**
	 * Click on city link in bottom of screen
	 * @param cityName
	 */
	public void clickOnCityLink(String cityName){
		WebDriver driver = DriverManager.getInstance().getDriver();
		browserHelper = new BrowserHelper();
		String xpathCityLink = String.format("//span[@class='find-treebo__name'][text()='%s']", WordUtils.capitalizeFully(cityName));
		System.out.println("clicking on city link " + xpathCityLink);
		browserHelper.scrollToElement(driver.findElement(By.xpath(xpathCityLink)));
		driver.findElement(By.xpath(xpathCityLink)).click();
	}
	
	public List<String> getCityLinks(){
		WebDriver driver = DriverManager.getInstance().getDriver();
		List<WebElement> cityLinksEle = driver.findElements(By.cssSelector("ul.find-treebo__list li span"));
		List<String> cityLinks = new ArrayList<String>();
		for (WebElement el: cityLinksEle){
			cityLinks.add(el.getText());
		}
		System.out.println("City Links on Home Screen " + cityLinks.toString());
		return cityLinks;
	}
	
	public void signInToGmail(String email, String password) {
		WebDriver driver = DriverManager.getInstance().getDriver();
		System.out.println("Signin to gmail");
		driver.get("http://gmail.com");
		if (driver.findElements(By.cssSelector("a:contains('mobile Gmail site')")).size() > 0){
			driver.findElement(By.cssSelector("a:contains('mobile Gmail site')")).click();
		}
		driver.findElement(By.xpath("//a[text()='Sign in']")).click();
		driver.findElement(By.cssSelector("#Email")).sendKeys(email);
		driver.findElement(By.cssSelector("#next")).click();
		WebDriverWait wait = new WebDriverWait(driver, 120);
		wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector("#Passwd")));
		driver.findElement(By.cssSelector("#Passwd")).sendKeys(password);
		driver.findElement(By.cssSelector("#signIn")).click();
	}
	
	public void waitForLoaderToDisappear() {
		WebDriver driver = DriverManager.getInstance().getDriver();
		WebDriverWait wait = new WebDriverWait(driver, 180);
		System.out.println("****waiting for page to load****");
		wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector("div#commonLoader.hide")));
	}
	
	public boolean is500Displayed(){
		WebDriver driver = DriverManager.getInstance().getDriver();
		return (driver.findElements(By.cssSelector("img[alt='500 page']")).size() > 0);
	}
	public boolean doescheckIndisplayed(){
		WebDriver driver= DriverManager.getInstance().getDriver();
		return driver.findElement(checkin).isDisplayed();
	}
	public void clickOnCheckIn(){
		WebDriver driver= DriverManager.getInstance().getDriver();
		driver.findElement(checkin).click();
	}
	public boolean doesRightArrowDisplayed(){
		WebDriver driver = DriverManager.getInstance().getDriver();
		return driver.findElement(calendarRightClick).isDisplayed();
	}
	public void clickOnRightArrow(){
		WebDriver driver = DriverManager.getInstance().getDriver();
		WebDriverWait wait = new WebDriverWait(driver, 180);
		wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(calendarRightClick));
		driver.findElement(calendarRightClick).click();
	}
	public boolean doesLeftArrowDisplayed(){
		WebDriver driver = DriverManager.getInstance().getDriver();
		return driver.findElement(calendarLeftClick).isDisplayed();
	}
	public void clickOnLeftArrow(){
		WebDriver driver = DriverManager.getInstance().getDriver();
		WebDriverWait wait = new WebDriverWait(driver, 180);
		wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(calendarLeftClick));
		driver.findElement(calendarLeftClick).click();
	}
	public String extractMonthNameFromCalandar(){
		WebDriver driver = DriverManager.getInstance().getDriver();
		WebElement element = driver.findElement(By.xpath("//span[@dr-month-switcher-month='']"));
		String month = element.getText().trim();
		return month;
	}
	
	public boolean isEqualMonthName(String month1, String month2){
		WebDriver driver = DriverManager.getInstance().getDriver();
		if((month1.trim()).equals(month2.trim()))
			return true;
		return false;
		
	}
	
	public void clickOnReferrals(){
		WebDriver driver = DriverManager.getInstance().getDriver();
		browserHelper = new BrowserHelper();
		browserHelper.jsclick(driver.findElement(By.cssSelector(".menu-nav__link.js-accounts-link[href='/account/referrals']")));
	}
	public boolean isMainTitleTextDisplayed(){
		WebDriver driver = DriverManager.getInstance().getDriver();
		return driver.findElement(By.cssSelector(".referrals__main__title")).isDisplayed();
	}
	public boolean isTransparentLinkDisplayed(){
		WebDriver driver = DriverManager.getInstance().getDriver();
		return driver.findElement(By.xpath(".//*[@id='accountPage']//div[contains(@class,'referrals__main__url')]/input[contains(@value,'http://invite.treebohotels.com/')]")).isDisplayed();
	}
	public boolean isCopyButtonDisplayed(){
		WebDriver driver = DriverManager.getInstance().getDriver();
		return driver.findElement(By.xpath(".//*[@id='ref-copy']")).isDisplayed();
	}
	public boolean isFacebookLinkDisplayed(){
		WebDriver driver = DriverManager.getInstance().getDriver();
		return driver.findElement(By.cssSelector(".btn.btn--facebook")).isDisplayed();
	}
	public boolean isWhatsAppLinkDisplayed(){
		WebDriver driver = DriverManager.getInstance().getDriver();
		return driver.findElement(By.cssSelector(".btn.btn--whatsapp")).isDisplayed();
	}
	public boolean isGmailLinkDisplayed(){
		WebDriver driver = DriverManager.getInstance().getDriver();
		return driver.findElement(By.cssSelector(".btn.btn--google")).isDisplayed();
	}
	public boolean doesSignedUpTextDisplayed(){
		WebDriver driver = DriverManager.getInstance().getDriver();
		return driver.findElement(By.xpath(".//*[@id='accountPage']//div[@data-reactid='.1.2.0.0']/div[@class='box__sub-text']")).isDisplayed();
	}
	public boolean doesConvertedTextDisplayed(){
		WebDriver driver = DriverManager.getInstance().getDriver();
		return driver.findElement(By.xpath(".//*[@id='accountPage']//div[@data-reactid='.1.2.0.1']/div[@class='box__sub-text']")).isDisplayed();
	}
	public void clickOnGmailLogin(){
		WebDriver driver = DriverManager.getInstance().getDriver();
		driver.findElement(By.cssSelector(".btn.btn--google")).click();
	}
	public void clickOnFacebookLogin(){
		WebDriver driver = DriverManager.getInstance().getDriver();
		driver.findElement(By.cssSelector(".btn.btn--facebook")).click();
	}
	public void loginThroughGmail(String email, String password) {
		WebDriver driver = DriverManager.getInstance().getDriver();
		browserHelper = new BrowserHelper();
		String parentWindow = browserHelper.switchToNewWindow();
		driver.findElement(By.cssSelector("#Email")).sendKeys(email.trim());
		driver.findElement(By.cssSelector("#next")).click();
		WebDriverWait wait = new WebDriverWait(driver, 120);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("#Passwd")));
		driver.findElement(By.cssSelector("#Passwd")).sendKeys(password.trim());
		driver.findElement(By.cssSelector("#signIn")).click();
	}
	public boolean isForgotLinkDisplayed(){
		WebDriver driver = DriverManager.getInstance().getDriver();
		return driver.findElement(forgotLink).isDisplayed();
	}
	public void clickOnForgotLink(){
		WebDriver driver = DriverManager.getInstance().getDriver();
		driver.findElement(forgotLink).click();
	}
	
	
}
