package mobileSiteUITest;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import org.testng.Assert;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import base.TestBaseSetUp;
import mobileSiteUIPageObjects.HomeScreen;
import mobileSiteUIPageObjects.HotelDetailsScreen;
import mobileSiteUIPageObjects.HotelSearchResultsScreen;
import mobileSiteUIPageObjects.ItineraryScreen;
import utils.CommonUtils;

public class MSiteSearchTest extends TestBaseSetUp {

	private HomeScreen homeScreen;
	private HotelSearchResultsScreen hotelSearchResultScreen;
	private HotelDetailsScreen hotelDetailsScreen;
	private ItineraryScreen itineraryScreen;

	/**
	 * This test verifies Price sorting First Available hotels are sorted Then
	 * sold out hotels are sorted
	 */

	@Test(groups = { "MSite", "MSiteSearch" })
	public void testMobileSearchPriceSorting() {
		homeScreen = new HomeScreen();
		hotelSearchResultScreen = new HotelSearchResultsScreen();
		CommonUtils utils = new CommonUtils();
		SoftAssert verify = new SoftAssert();

		String location = utils.getProperty("city"); // "Bengaluru";

		homeScreen.doSearch(location);
		verify.assertTrue(hotelSearchResultScreen.isHotelSearchScreenDisplayed(),
				"Hotel Search Screen is not displayed!!!");
		verify.assertTrue(hotelSearchResultScreen.isHotelResultDisplayed(), "Results not displayed!!!");

		int[] roomRatesAvailable = hotelSearchResultScreen.getRoomRatesOfAvailableHotel();
		for (int i = 0; i < roomRatesAvailable.length - 1; i++) {
			verify.assertTrue(roomRatesAvailable[i] > 500, "Room rate is less than 500 from Available Hotels");
			verify.assertTrue(roomRatesAvailable[i + 1] >= roomRatesAvailable[i],
					"Available Hotels are not displayed with proper room rate." + roomRatesAvailable[i + 1]
							+ " should have been greater or equal than " + roomRatesAvailable[i]);
		}
		int[] roomRatesSoldOut = hotelSearchResultScreen.getRoomRatesOfSoldOutHotel();
		for (int i = 0; i < roomRatesSoldOut.length - 1; i++) {
			verify.assertTrue(roomRatesSoldOut[i] > 500, "Room rate is less than 500 from Sold out hotels");
			verify.assertTrue(roomRatesSoldOut[i + 1] >= roomRatesSoldOut[i],
					"Sold out Hotels are not displayed with proper room rate." + roomRatesSoldOut[i + 1]
							+ " should have been greater or equal than " + roomRatesSoldOut[i]);
		}
		verify.assertAll();
	}

	/**
	 * This test verifies Filters Show Only Available
	 */

	@Test(groups = { "MSite", "MSiteSearch" })
	public void testMobileSearchFilterShowOnlyAvailable() {
		homeScreen = new HomeScreen();
		hotelSearchResultScreen = new HotelSearchResultsScreen();
		CommonUtils utils = new CommonUtils();
		SoftAssert verify = new SoftAssert();

		String location = utils.getProperty("city"); // "Bengaluru";

		homeScreen.doSearch(location);
		verify.assertTrue(hotelSearchResultScreen.isHotelSearchScreenDisplayed(),
				"Hotel Search Screen is not displayed!!!");
		verify.assertTrue(hotelSearchResultScreen.isHotelResultDisplayed(), "Results not displayed!!!");

		int countOfTotalHotel = hotelSearchResultScreen.getCountOfTotalHotelDisplayed();
		int countOfAvailableHotel = hotelSearchResultScreen.getCountOfAvailableHotelDisplayed();

		verify.assertTrue(hotelSearchResultScreen.getFilteredResultCount() == countOfTotalHotel, "Count mismatch");
		hotelSearchResultScreen.showOnlyAvailable();
		verify.assertTrue(hotelSearchResultScreen.getFilteredResultCount() == countOfAvailableHotel,
				"Count mismatch after applying show only available");
		verify.assertAll();
	}

	/**
	 * This test verifies Filters Price Filter
	 */
	@Test(groups = { "MSite", "MSiteSearch" })
	public void testMobileSearchPriceFilter() {
		homeScreen = new HomeScreen();
		hotelSearchResultScreen = new HotelSearchResultsScreen();
		CommonUtils utils = new CommonUtils();
		SoftAssert verify = new SoftAssert();

		String location = utils.getProperty("city"); // "Bengaluru";

		homeScreen.doSearch(location);
		verify.assertTrue(hotelSearchResultScreen.isHotelSearchScreenDisplayed(),
				"Hotel Search Screen is not displayed!!!");
		verify.assertTrue(hotelSearchResultScreen.isHotelResultDisplayed(), "Results not displayed!!!");

		hotelSearchResultScreen.applyPriceFilter();

		for (int x : hotelSearchResultScreen.getRoomRatesOfAllHotel()) {
			verify.assertTrue(1500 < x && x < 3000);
		}
		verify.assertAll();
	}

	/**
	 * This test verifies Filters Clear Filter
	 */

	@Test(groups = { "MSite", "MSiteSearch" })
	public void testMobileSearchClearFilter() {
		homeScreen = new HomeScreen();
		hotelSearchResultScreen = new HotelSearchResultsScreen();
		CommonUtils utils = new CommonUtils();
		SoftAssert verify = new SoftAssert();

		String location = utils.getProperty("city"); // "Bengaluru";

		homeScreen.doSearch(location);
		verify.assertTrue(hotelSearchResultScreen.isHotelSearchScreenDisplayed(),
				"Hotel Search Screen is not displayed!!!");
		verify.assertTrue(hotelSearchResultScreen.isHotelResultDisplayed(), "Results not displayed!!!");

		int countOfTotalHotel = hotelSearchResultScreen.getCountOfTotalHotelDisplayed();
		int countOfAvailableHotel = hotelSearchResultScreen.getCountOfAvailableHotelDisplayed();

		verify.assertTrue(hotelSearchResultScreen.getFilteredResultCount() == countOfTotalHotel, "Count mismatch");
		hotelSearchResultScreen.showOnlyAvailable();
		verify.assertTrue(hotelSearchResultScreen.getFilteredResultCount() == countOfAvailableHotel,
				"Count mismatch after applying show only available");
		hotelSearchResultScreen.clearAllFilters();
		verify.assertTrue(hotelSearchResultScreen.getFilteredResultCount() == countOfTotalHotel, "Count mismatch");
		verify.assertAll();
	}

	/**
	 * This test verifies if hotels are nearby Similar treebos are displayed in
	 * HD Screen
	 * 
	 */

	@Test(groups = { "MSite", "MSiteSearch" })
	public void testMobileSearchSimilarTreebosDisplayed() {
		homeScreen = new HomeScreen();
		hotelSearchResultScreen = new HotelSearchResultsScreen();
		hotelDetailsScreen = new HotelDetailsScreen();
		CommonUtils utils = new CommonUtils();
		SoftAssert verify = new SoftAssert();

		String location = utils.getProperty("city"); // "Bengaluru";

		homeScreen.doSearch(location);
		verify.assertTrue(hotelSearchResultScreen.isHotelSearchScreenDisplayed(),
				"Hotel Search Screen is not displayed!!!");
		verify.assertTrue(hotelSearchResultScreen.isHotelResultDisplayed(), "Results not displayed!!!");

		hotelSearchResultScreen.clickHotelByIndex(1);
		// Verify in HD page similar treebos displayed
		verify.assertTrue(hotelDetailsScreen.isHDScreenDisplayed(), "HD Screen is not displayed");
		verify.assertTrue(hotelDetailsScreen.isSimilarTreebosDisplayed(), "Similar Treebos not displayed");
		verify.assertAll();
	}

	/**
	 * This test verifies if hotels are nearby Similar treebos are displayed in
	 * HD Screen in order of distance
	 * 
	 */

	@Test(groups = { "MSite", "MSiteSearch" })
	public void testMobileSearchSimilarTreebosDisplayedSortedByDistance() {
		homeScreen = new HomeScreen();
		hotelSearchResultScreen = new HotelSearchResultsScreen();
		hotelDetailsScreen = new HotelDetailsScreen();
		CommonUtils utils = new CommonUtils();
		SoftAssert verify = new SoftAssert();

		String location = utils.getProperty("city"); // "Bengaluru";

		homeScreen.doSearch(location);
		verify.assertTrue(hotelSearchResultScreen.isHotelSearchScreenDisplayed(),
				"Hotel Search Screen is not displayed!!!");
		verify.assertTrue(hotelSearchResultScreen.isHotelResultDisplayed(), "Results not displayed!!!");
		int resultCount = hotelSearchResultScreen.getCountOfTotalHotelDisplayed();

		hotelSearchResultScreen.clickHotelByIndex(1);
		// Verify in HD page similar treebos displayed
		verify.assertTrue(hotelDetailsScreen.isHDScreenDisplayed(), "HD Screen is not displayed");
		verify.assertTrue(hotelDetailsScreen.isSimilarTreebosDisplayed(), "Similar Treebos not displayed");

		// Get count of Similar Treebos
		int countOfSimilarTreebos = hotelDetailsScreen.getCountOfSimilarTreebos();
		verify.assertTrue((resultCount - countOfSimilarTreebos) == 1, "Similar treebos count is wrong");

//		// Get distance of all Similar Treebos
//		List<Double> distSimilarTreebos = new ArrayList<Double>();
//		for (int i = 1; i <= countOfSimilarTreebos; i++) {
//			distSimilarTreebos.add(hotelDetailsScreen.getDistanceOfCurrentSimilarTreebo());
//			hotelDetailsScreen.clickOnNextInSimilarTreebos();
//		}
//
//		System.out.println("Similar Treebos distance :" + distSimilarTreebos.toString());
//		// Check if list items are in sorted order
//		for (int j = 1; j < distSimilarTreebos.size(); j++) {
//			verify.assertTrue(distSimilarTreebos.get(j - 1).compareTo(distSimilarTreebos.get(j)) <= 0,
//					"failed for : " + distSimilarTreebos.get(j - 1) + " and " + distSimilarTreebos.get(j));
//		}
		verify.assertAll();
	}

	/**
	 * This test verifies Search parameters shown on Search Screen is correct
	 * 
	 * @throws ParseException
	 * 
	 */
	@Test(groups = { "MSite", "MSiteSearch" })
	public void testMobileSearchParametersAsPerSearchDone() throws ParseException {
		homeScreen = new HomeScreen();
		hotelSearchResultScreen = new HotelSearchResultsScreen();
		hotelDetailsScreen = new HotelDetailsScreen();
		CommonUtils utils = new CommonUtils();
		SoftAssert verify = new SoftAssert();

		String location = utils.getProperty("city"); // "Bengaluru";
		int checkInFromCurrentDate = utils.getRandomNumber(60, 90);
		int checkOutFromCurrentDate = checkInFromCurrentDate + 2;
		int[][] roomIndex = { { 1, 2, 1 }, { 2, 2, 2 } };

		String[] checkInCheckOutDates = homeScreen.doSearch(location, checkInFromCurrentDate, checkOutFromCurrentDate,
				roomIndex);
		verify.assertTrue(hotelSearchResultScreen.isHotelSearchScreenDisplayed(),
				"Hotel Search Screen is not displayed!!!");
		verify.assertTrue(hotelSearchResultScreen.isHotelResultDisplayed(), "Results not displayed!!!");

		//
		verify.assertEquals(hotelSearchResultScreen.getCitySearched(), location, "City searched is wrong");
		verify.assertEquals(hotelSearchResultScreen.getNumberOfGuestsSearched(), 4, "guest count is wrong");
		verify.assertEquals(hotelSearchResultScreen.getNumberOfNightsSearched(), 2, "number of nights is wrong");
		verify.assertEquals(hotelSearchResultScreen.getNumberOfRoomsSearched(), 2, "Number of rooms is wrong");
		verify.assertEquals(utils.formatDate(checkInCheckOutDates[0]).toUpperCase(),
				hotelSearchResultScreen.getCheckInDateSearched().toUpperCase());
		verify.assertEquals(utils.formatDate(checkInCheckOutDates[1]).toUpperCase(),
				hotelSearchResultScreen.getCheckOutDateSearched().toUpperCase());

		verify.assertAll();
	}

	/**
	 * This test verifies User can modify Search
	 * 
	 */
	@Test(groups = { "MSite", "MSiteSearch" })
	public void testMobileModifySearch() throws ParseException {
		homeScreen = new HomeScreen();
		hotelSearchResultScreen = new HotelSearchResultsScreen();
		hotelDetailsScreen = new HotelDetailsScreen();
		CommonUtils utils = new CommonUtils();
		SoftAssert verify = new SoftAssert();

		String location = utils.getProperty("city"); // "Bengaluru";

		String modifiedLocation = utils.getProperty("city1"); // "Hyderabad";
		int checkInFromCurrentDate = utils.getRandomNumber(1, 5);
		int checkOutFromCurrentDate = checkInFromCurrentDate + 2;

		String[] checkInCheckOutDates = homeScreen.doSearch(location);
		verify.assertTrue(hotelSearchResultScreen.isHotelSearchScreenDisplayed(),
				"Hotel Search Screen is not displayed!!!");
		verify.assertTrue(hotelSearchResultScreen.isHotelResultDisplayed(), "Results not displayed!!!");

		//
		verify.assertEquals(hotelSearchResultScreen.getCitySearched(), location, "City searched is wrong");
		verify.assertEquals(hotelSearchResultScreen.getNumberOfGuestsSearched(), 1, "guest count is wrong");
		verify.assertEquals(hotelSearchResultScreen.getNumberOfNightsSearched(), 1, "number of nights is wrong");
		verify.assertEquals(hotelSearchResultScreen.getNumberOfRoomsSearched(), 1, "Number of rooms is wrong");
		verify.assertEquals(utils.formatDate(checkInCheckOutDates[0]).toUpperCase(),
				hotelSearchResultScreen.getCheckInDateSearched().toUpperCase());
		verify.assertEquals(utils.formatDate(checkInCheckOutDates[1]).toUpperCase(),
				hotelSearchResultScreen.getCheckOutDateSearched().toUpperCase());

		String[] checkInCheckOutDatesModified = hotelSearchResultScreen.modifySearch(modifiedLocation,
				checkInFromCurrentDate, checkOutFromCurrentDate);

		verify.assertEquals(hotelSearchResultScreen.getCitySearched(), modifiedLocation, "City modified is wrong");
		verify.assertEquals(hotelSearchResultScreen.getNumberOfGuestsSearched(), 1, "guest count is wrong");
		verify.assertEquals(hotelSearchResultScreen.getNumberOfNightsSearched(), 2, "number of nights is wrong");
		verify.assertEquals(hotelSearchResultScreen.getNumberOfRoomsSearched(), 1, "Number of rooms is wrong");
		verify.assertEquals(utils.formatDate(checkInCheckOutDatesModified[0]).toUpperCase(),
				hotelSearchResultScreen.getCheckInDateSearched().toUpperCase());
		verify.assertEquals(utils.formatDate(checkInCheckOutDatesModified[1]).toUpperCase(),
				hotelSearchResultScreen.getCheckOutDateSearched().toUpperCase());
		verify.assertAll();
	}

	/**
	 * This test verifies User can close modify search widget
	 * 
	 */
	@Test(groups = { "MSite", "MSiteSearch" })
	public void testMobileModifySearchWidgetCanBeClosed() throws ParseException {
		homeScreen = new HomeScreen();
		hotelSearchResultScreen = new HotelSearchResultsScreen();
		hotelDetailsScreen = new HotelDetailsScreen();
		CommonUtils utils = new CommonUtils();
		SoftAssert verify = new SoftAssert();

		String location = utils.getProperty("city"); // "Bengaluru";

		homeScreen.doSearch(location);
		verify.assertTrue(hotelSearchResultScreen.isHotelSearchScreenDisplayed(),
				"Hotel Search Screen is not displayed!!!");
		verify.assertTrue(hotelSearchResultScreen.isHotelResultDisplayed(), "Results not displayed!!!");

		// open modify widget
		hotelSearchResultScreen.openModifyWidget();

		verify.assertTrue(hotelSearchResultScreen.isModifyWidgetOpen(), "Modify widget is closed");

		hotelSearchResultScreen.closeModifyWidget();
		verify.assertFalse(hotelSearchResultScreen.isModifyWidgetOpen(), "Modify widget is still open");

		verify.assertAll();
	}

	/**
	 * This test verifies User can close filters screen
	 * 
	 */
	@Test(groups = { "MSite", "MSiteSearch" })
	public void testMobileSearchFilterCanBeClosed() throws ParseException {
		homeScreen = new HomeScreen();
		hotelSearchResultScreen = new HotelSearchResultsScreen();
		hotelDetailsScreen = new HotelDetailsScreen();
		CommonUtils utils = new CommonUtils();
		SoftAssert verify = new SoftAssert();

		String location = utils.getProperty("city"); // "Bengaluru";

		homeScreen.doSearch(location);
		verify.assertTrue(hotelSearchResultScreen.isHotelSearchScreenDisplayed(),
				"Hotel Search Screen is not displayed!!!");
		verify.assertTrue(hotelSearchResultScreen.isHotelResultDisplayed(), "Results not displayed!!!");

		// open filters
		hotelSearchResultScreen.openFilterScreen();
		verify.assertTrue(hotelSearchResultScreen.isFiltersOpen(), "Filters should have been open");

		// close the filter now by clicking X icon
		hotelSearchResultScreen.closeFilters();
		verify.assertFalse(hotelSearchResultScreen.isFiltersOpen(), "Filters should have been closed");

		verify.assertAll();
	}

	/**
	 * This test verifies User can apply location filter
	 * 
	 */
	@Test(groups = { "MSite", "MSiteSearch" })
	public void testMobileSearchLocationFilterApply() throws ParseException {
		homeScreen = new HomeScreen();
		hotelSearchResultScreen = new HotelSearchResultsScreen();
		hotelDetailsScreen = new HotelDetailsScreen();
		CommonUtils utils = new CommonUtils();
		SoftAssert verify = new SoftAssert();

		String location = utils.getProperty("city"); // "Bengaluru";

		homeScreen.doSearch(location);
		verify.assertTrue(hotelSearchResultScreen.isHotelSearchScreenDisplayed(),
				"Hotel Search Screen is not displayed!!!");
		verify.assertTrue(hotelSearchResultScreen.isHotelResultDisplayed(), "Results not displayed!!!");

		//
		int totalHotel = hotelSearchResultScreen.getCountOfTotalHotelDisplayed();
		// open filters
		hotelSearchResultScreen.openFilterScreen();
		verify.assertTrue(hotelSearchResultScreen.isFiltersOpen(), "Filters should have been open");

		// apply location filter
		hotelSearchResultScreen.applyLocationFilter();
		int filteredResults = hotelSearchResultScreen.getFilteredResultCount();

		verify.assertTrue(totalHotel > filteredResults, "results does not seems to be filtered");

		verify.assertAll();
	}

	/**
	 * This test verifies User can apply amenities filter
	 * 
	 */
	@Test(groups = { "MSite", "MSiteSearch" })
	public void testMobileSearchAmenitiesFilterApply() throws ParseException {
		homeScreen = new HomeScreen();
		hotelSearchResultScreen = new HotelSearchResultsScreen();
		hotelDetailsScreen = new HotelDetailsScreen();
		CommonUtils utils = new CommonUtils();
		SoftAssert verify = new SoftAssert();

		String location = utils.getProperty("city"); // "Bengaluru";

		homeScreen.doSearch(location);
		verify.assertTrue(hotelSearchResultScreen.isHotelSearchScreenDisplayed(),
				"Hotel Search Screen is not displayed!!!");
		verify.assertTrue(hotelSearchResultScreen.isHotelResultDisplayed(), "Results not displayed!!!");

		//
		int totalHotel = hotelSearchResultScreen.getCountOfTotalHotelDisplayed();
		// open filters
		hotelSearchResultScreen.openFilterScreen();
		verify.assertTrue(hotelSearchResultScreen.isFiltersOpen(), "Filters should have been open");

		// apply location filter
		hotelSearchResultScreen.applyAmenitiesFilter();
		int filteredResults = hotelSearchResultScreen.getFilteredResultCount();

		verify.assertTrue(totalHotel > filteredResults, "results does not seems to be filtered");

		verify.assertAll();
	}
}
