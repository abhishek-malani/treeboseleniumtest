package mobileSiteUIPageObjects;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import base.BrowserHelper;
import base.DriverManager;

public class HotelDetailsScreen {	
	private HomeScreen homeScreen;
	private BrowserHelper browserHelper;
	
	public boolean isHDScreenDisplayed(){
		WebDriver driver = DriverManager.getInstance().getDriver();
		browserHelper = new BrowserHelper();
		browserHelper.waitTime(1000);
		homeScreen = new HomeScreen();

		WebDriverWait wait = new WebDriverWait(driver, 120);
		
		//check if 500 page
		if (homeScreen.is500Displayed()){
			Assert.fail("500 displayed instead of HD screen for url " + browserHelper.getCurrentUrl());
		}

		try{
			wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector("#commonLoader.hide")));
			wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(".hotel-detail__header")));
		}catch(Exception e){
			Assert.fail("HD Screen not displayed for url "+ browserHelper.getCurrentUrl());
		}
		
		return driver.findElement(By.cssSelector(".hotel-detail__header")).isDisplayed();
	}
	
	public String getHotelNameInHDScreen(){
		WebDriver driver = DriverManager.getInstance().getDriver();
		return driver.findElement(By.cssSelector(".hotel-detail__header")).getText().trim();
	}
	public void clickOnBook(){
		WebDriver driver = DriverManager.getInstance().getDriver();
		driver.findElement(By.cssSelector(".modify__widget__book-button")).click();
	}

	public int getSelectedRoomRateInHDScreen(){
		WebDriver driver = DriverManager.getInstance().getDriver();
		return Integer.parseInt(driver.findElement(By.cssSelector("#modify-widget-price")).getText());
	}
	
	public String getSelectedCheckInDate(){
		WebDriver driver = DriverManager.getInstance().getDriver();
		return driver.findElement(By.cssSelector("#modify-widget-checkin")).getText();
	}
	
	public String getSelectedCheckOutDate(){
		WebDriver driver = DriverManager.getInstance().getDriver();
		return driver.findElement(By.cssSelector("#modify-widget-checkout")).getText();
	}
	
	public int getSelectedItineraryDuration(){
		WebDriver driver = DriverManager.getInstance().getDriver();
		return Integer.parseInt(driver.findElement(By.cssSelector("#modify-widget-nights")).getText().replaceAll("[^0-9]", ""));
	}
	
	public int getSelectedItineraryNumOfRooms(){
		WebDriver driver = DriverManager.getInstance().getDriver();
		return Integer.parseInt((driver.findElement(By.cssSelector("#modify-widget-rooms")).getText().split(",")[0]).trim().replaceAll("[^0-9]", ""));
	}
	
	public int getSelectedItineraryNumOfGuests(){
		WebDriver driver = DriverManager.getInstance().getDriver();
		return Integer.parseInt((driver.findElement(By.cssSelector("#modify-widget-rooms")).getText().split(",")[1]).trim().replaceAll("[^0-9]", ""));
	}
	
	public int getCountOfRoomTypes(){
		WebDriver driver = DriverManager.getInstance().getDriver();
		return driver.findElements(By.cssSelector(".room-details__rooms")).size();
	}
	
	public String getSelectedRoomType(){
		WebDriver driver = DriverManager.getInstance().getDriver();
		return driver.findElement(By.cssSelector(".room-details__rooms--selected")).getAttribute("data-room");
	}
	
	public String[] getAllRoomType(){
		WebDriver driver = DriverManager.getInstance().getDriver();
		String[] roomType = new String[getCountOfRoomTypes()];
		int i = 0;
		List<WebElement> roomTypeElement = driver.findElements(By.cssSelector(".room-details__rooms"));
		for (WebElement ele : roomTypeElement){
			roomType[i] = ele.getAttribute("data-room");
			i = i + 1;
		}
		return roomType;
	}
	
	public String[] getAllAvailableRoomType(){
		WebDriver driver = DriverManager.getInstance().getDriver();
		int count = driver.findElements(By.cssSelector("li.room-details__rooms:not(.room-details__rooms--unavailable)")).size();
		String[] roomType = new String[count];
		int i = 0;
		List<WebElement> roomTypeElement = driver.findElements(By.cssSelector("li.room-details__rooms:not(.room-details__rooms--unavailable)"));
		for (WebElement ele : roomTypeElement){
			roomType[i] = ele.getAttribute("data-room");
			i = i + 1;
		}
		return roomType;
	}
	
	public String[] getAllUnavailableRoomType(){
		WebDriver driver = DriverManager.getInstance().getDriver();
		int count = driver.findElements(By.cssSelector("li.room-details__rooms.room-details__rooms--unavailable")).size();
		String[] roomType = new String[count];
		int i = 0;
		List<WebElement> roomTypeElement = driver.findElements(By.cssSelector("li.room-details__rooms.room-details__rooms--unavailable"));
		for (WebElement ele : roomTypeElement){
			roomType[i] = ele.getAttribute("data-room");
			i = i + 1;
		}
		return roomType;
	}
	
	public void openModifyWidget(){
		WebDriver driver = DriverManager.getInstance().getDriver();
		driver.findElement(By.cssSelector(".modify__widget__details__price__modify.analytics-modify")).click();
	}
	
	public void modifyRoomTypeForSelectedHotel(String roomType){
		WebDriver driver = DriverManager.getInstance().getDriver();
		driver.findElement(By.cssSelector("#roomType")).click();
		String xpathRoomType = String.format("option[value='%s']",roomType);
		driver.findElement(By.cssSelector(xpathRoomType)).click();
	}
	
//	public void modifyDatesForSelectedHotel(int checkin,int checkout){
//		WebDriver driver = DriverManager.getInstance().getDriver();
//		homeScreen = new HomeScreen();
//	}
	
	public void modifyRoomConfigForSelectedHotel(int[][] roomAdultChild){
		homeScreen = new HomeScreen();
		homeScreen.selectGuests(roomAdultChild);
	}
	
	public void clickDoneInModify(){
		WebDriver driver = DriverManager.getInstance().getDriver();
		browserHelper = new BrowserHelper();
		driver.findElement(By.cssSelector(".btn.btn--block.modify-search__done")).click();
		browserHelper.waitTime(2000);
	}
	
	public void closeModifyWidget(){
		WebDriver driver = DriverManager.getInstance().getDriver();
		driver.findElement(By.xpath("//div[@id='searchModal'][not(contains(@class,'hide'))]//i[@class='icon-cross']")).click();
	}
	
	public boolean isSimilarTreebosDisplayed(){
		WebDriver driver = DriverManager.getInstance().getDriver();
		return driver.findElement(By.cssSelector(".similar.carousel")).isDisplayed();
	}
	
	public void clickOnNextInSimilarTreebos(){
		System.out.println("Move next to Similar treebo");
		WebDriver driver = DriverManager.getInstance().getDriver();
		browserHelper = new BrowserHelper();
		browserHelper.scrollToElement(driver.findElement(By.cssSelector(".results__row.analytics-similar.slick-current.slick-active")));
//		try{
//			driver.findElement(By.cssSelector(".similar.carousel .slick-next.slick-arrow")).click();
//		}catch(Exception e){
			browserHelper.dragAndDrop(driver.findElement(By.cssSelector(".results__row.analytics-similar.slick-current.slick-active .hotel__details.pos-rel")), driver.findElement(By.cssSelector(".results__row.analytics-similar.slick-current.slick-active+div .hotel__details.pos-rel")));
//		}
			browserHelper.waitTime(2000);
	}
	
	public int getCountOfSimilarTreebos(){
		WebDriver driver = DriverManager.getInstance().getDriver();
		return driver.findElements(By.xpath("//div[contains(@class,'analytics-similar')][not(contains(@class,'slick-cloned'))]")).size();
	}
	
	public double getDistanceOfCurrentSimilarTreebo(){
		WebDriver driver = DriverManager.getInstance().getDriver();
		return Double.parseDouble(driver.findElement(By.cssSelector(".results__row.analytics-similar.slick-current.slick-active")).getAttribute("data-distance"));
	}
	
	public String getNameOfCurrentSimilarTreebo(){
		WebDriver driver = DriverManager.getInstance().getDriver();
		return driver.findElement(By.cssSelector(".results__row.analytics-similar.slick-current.slick-active a")).getAttribute("hotelname");
	}
	
	public void clickCurrentSimilarTreebo(){
		WebDriver driver = DriverManager.getInstance().getDriver();
		browserHelper = new BrowserHelper();
		browserHelper.scrollToElement(driver.findElement(By.cssSelector(".results__row.analytics-similar.slick-current.slick-active a")));
		driver.findElement(By.cssSelector(".results__row.analytics-similar.slick-current.slick-active a")).click();
	}
	
	public int getRateOfCurrentSimilarTreebo(){
		WebDriver driver = DriverManager.getInstance().getDriver();
		return Integer.parseInt(driver.findElement(By.cssSelector(".results__row.analytics-similar.slick-current.slick-active .a-price.hotel__price--available")).getAttribute("price"));
	}
	
	public void openImageGallery(){
		WebDriver driver = DriverManager.getInstance().getDriver();
		driver.findElement(By.cssSelector(".js-room-carousel .slick-current img")).click();
	}
	
	public boolean isImageGalleryOpen(){
		WebDriver driver = DriverManager.getInstance().getDriver();
		return (!(driver.findElements(By.cssSelector("#commonModal.hide img")).size() > 0));
	}
	
	public void closeImageGalllery(){
		WebDriver driver = DriverManager.getInstance().getDriver();
		driver.findElement(By.cssSelector("#commonModal .icon-close")).click();
	}
	
	public void clickNextArrow(){
		WebDriver driver = DriverManager.getInstance().getDriver();
		
	}
}
