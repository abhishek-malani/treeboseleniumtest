package desktopUIPageObjects;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import desktopUIPageObjects.HomePage;

import base.BrowserHelper;
import base.DriverManager;

public class BookingHistory {
	private HomePage homePage;
	private BrowserHelper browserHelper;

	private By accountName = By.xpath("//li[@id='user']/div/button");
	private By myAccount = By.xpath("//li[@id='user']//a[@href='/account/']");
	private By bookingHistory = By.xpath("//div[@class='bookingShow']/div[text()='Booking History']");
	private By cancelBookingButton = By.xpath("//input[@id='cancelSubmit']");
	private By hotelNameInBookingHistory = By.xpath("//div[@class='hotel__name']");
	private By cancelYesButton = By.xpath("//input[@id='yes_btn']");
	private By cancelNoButton = By.xpath("//input[@id='no_btn']");
	private By closeCancelPopUP = By.xpath("//div[@id='closePopup']/i");
	private By cancelSuccessMessage = By.xpath("//div[@class='toast-message']"); // [text()='Your
																					// booking
																					// is
																					// cancelled
																					// successfully']
	private By viewDetailsLinkForConfirmedBooking = By.xpath(
			"//div[@id='confirm']/span[text()='Confirmed']/../../../div[@class='pages__booking-status']/div/u[text()='View Details']");
	private By backToBookingHistory = By.xpath("//span[text()='Back to booking history']");

	public void goToBookingHistoryPage() {
		WebDriver driver = DriverManager.getInstance().getDriver();
		try{
			WebDriverWait wait = new WebDriverWait(driver, 120);
			wait.until(ExpectedConditions.presenceOfElementLocated(accountName));
		}catch (Exception e){
			//
		}
		driver.findElement(accountName).click();
		driver.findElement(myAccount).click();
	}

	public void verifyBookingHistoryPagePresence() {
		WebDriver driver = DriverManager.getInstance().getDriver();
		browserHelper = new BrowserHelper();
		homePage = new HomePage();
		
		System.out.println("Verify Booking History page is displayed");
		
		//check if 500 page
		if (homePage.is500Displayed()){
			Assert.fail("500 displayed instead of Booking History page for url " + browserHelper.getCurrentUrl());
		}
		
		
		try{
			WebDriverWait wait = new WebDriverWait(driver, 120);
			wait.until(ExpectedConditions.presenceOfElementLocated(bookingHistory));
		}catch (Exception e){
			//
		}
		
		Assert.assertTrue(driver.findElement(bookingHistory).isDisplayed(),
				"Displayed page has url: " + driver.getCurrentUrl());
	}

	public boolean checkIfBookingIsConfirmed(String bookingId) {
		WebDriver driver = DriverManager.getInstance().getDriver();
		String xpathBookingConfirmed = String.format(
				"//div[@class='pages__booking-id group_id']/span[2][text()='%s']/../../../div[@class='pages__booking-status']/div[@id='confirm']/span[text()='Confirmed']",
				bookingId);
		return (driver.findElements(By.xpath(xpathBookingConfirmed)).size() > 0);
	}

	public void clickOnBookingDetailsByBookingId(String bookingId) {
		WebDriver driver = DriverManager.getInstance().getDriver();
		String xpathBookingDetails = String.format(
				"//div[@class='pages__booking-id group_id']/span[2][text()='%s']/../../../div[@class='pages__booking-status']/div/u[text()='View Details']",
				bookingId);
		driver.findElement(By.xpath(xpathBookingDetails)).click();
	}

	public void clickOnCancelBooking() {
		WebDriver driver = DriverManager.getInstance().getDriver();
		driver.findElement(cancelBookingButton).click();
	}

	public void clickOnYesInCancelPopUp() {
		WebDriver driver = DriverManager.getInstance().getDriver();
		WebDriverWait wait = new WebDriverWait(driver, 120);
		wait.until(ExpectedConditions.presenceOfElementLocated(cancelYesButton));
		driver.findElement(cancelYesButton).click();
	}

	public void clickOnNoInCancelPopUp() {
		WebDriver driver = DriverManager.getInstance().getDriver();
		WebDriverWait wait = new WebDriverWait(driver, 120);
		wait.until(ExpectedConditions.presenceOfElementLocated(cancelNoButton));
		driver.findElement(cancelNoButton).click();
	}

	public boolean isCancelPopUpDisplayed() {
		WebDriver driver = DriverManager.getInstance().getDriver();
		boolean flag = false;
		try {
			driver.findElement(cancelYesButton).isDisplayed();
			flag = true;
		} catch (NoSuchElementException e) {
			flag = false;
		}
		return flag;
	}

	public boolean isCancelButtonDisplayed() {
		WebDriver driver = DriverManager.getInstance().getDriver();
		return !driver.findElement(cancelBookingButton).getAttribute("class").contains("hide");
	}

	public boolean didSuccessMessageDisplayed() {
		WebDriver driver = DriverManager.getInstance().getDriver();
		try {
			WebDriverWait wait = new WebDriverWait(driver, 120);
			wait.until(ExpectedConditions.presenceOfElementLocated(cancelSuccessMessage));
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	public void waitForLoaderToDisappear() {
		WebDriver driver = DriverManager.getInstance().getDriver();
		WebDriverWait wait = new WebDriverWait(driver, 120);
		wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector("div#commonLoader.hide")));
	}

	public void cancelBookingByBookingId(String bookingId) {
		WebDriver driver = DriverManager.getInstance().getDriver();
		homePage = new HomePage();
		homePage.goToHomePage();
		goToBookingHistoryPage();
		clickOnBookingDetailsByBookingId(bookingId);
		clickOnCancelBooking();
		clickOnYesInCancelPopUp();
		homePage.waitForLoaderToDisappear();
	}

	public void cancelBookingByBookingId(String email, String password, String bookingId) {
		WebDriver driver = DriverManager.getInstance().getDriver();
		homePage = new HomePage();
		homePage.goToHomePage();
		homePage.signInAsTreeboMember(email, password);
		System.out.println("Cancelling booking by id: " + bookingId + " ,for email id: " + email);
		goToBookingHistoryPage();
		clickOnBookingDetailsByBookingId(bookingId);
		clickOnCancelBooking();
		clickOnYesInCancelPopUp();
	}

	public void cancelAllBookings() {
		WebDriver driver = DriverManager.getInstance().getDriver();
		// should be already logged in
		homePage = new HomePage();
		browserHelper = new BrowserHelper();
		goToBookingHistoryPage();
		String url = browserHelper.getCurrentUrl();
		int j = countOfConfirmBooking();
		System.out.println("Number of confirm bookings are :" + j);
		for (int i = 1; i <= j ; i++) {
			if (isConfirmedBookingAvailable()) {
				System.out.println("cancelling confirmed booking :" + i);
				String xpathConfirm = String.format("(//span[text()='Confirmed'])[%d]/../..//u[text()='View Details']", (i));
				driver.findElement(By.xpath(xpathConfirm)).click();
				clickOnCancelBooking();
				clickOnYesInCancelPopUp();
				try {
					homePage.waitForLoaderToDisappear();
					Thread.sleep(20000);
				} catch (Exception e) {
					e.printStackTrace();
				}
				try{
					driver.findElement(backToBookingHistory).click();
				}catch (Exception e){
					browserHelper.openUrl(url);
				}
				
			}
		}
	}

	public boolean isConfirmedBookingAvailable() {
		WebDriver driver = DriverManager.getInstance().getDriver();
		if (driver.findElements(viewDetailsLinkForConfirmedBooking).size() > 0) {
			System.out.println("Confirmed bookings are there");
			return true;
		} else {
			System.out.println("All bookings are canceled");
			return false;
		}
	}

	public int countOfConfirmBooking() {
		WebDriver driver = DriverManager.getInstance().getDriver();
		return driver.findElements(viewDetailsLinkForConfirmedBooking).size();
	}
}
