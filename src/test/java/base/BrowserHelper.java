package base;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Set;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

import io.selendroid.server.common.exceptions.StaleElementReferenceException;

public class BrowserHelper {
	public String switchToNewWindow() {
		WebDriver driver = DriverManager.getInstance().getDriver();
		String parentWindow = driver.getWindowHandle();
		System.out.println("parent window" + parentWindow);
		ArrayList<String> windowHandles = new ArrayList<String>(driver.getWindowHandles());
		System.out.println("All window handles displayed: " + windowHandles.toString());
		driver.switchTo().window(windowHandles.get(1));
		return parentWindow;
	}
	
	public Set<String> getAllWindowHandles(){
		WebDriver driver = DriverManager.getInstance().getDriver();
		return driver.getWindowHandles();
	}
	
	public String getCurrentWindowHandle(){
		WebDriver driver = DriverManager.getInstance().getDriver();
		return driver.getWindowHandle();
	}
	
	public void switchToWindow(String windowHandle){
		WebDriver driver = DriverManager.getInstance().getDriver();
		driver.switchTo().window(windowHandle);
	}

	public void SwitchToParentWindow(String parentWindow) {
		WebDriver driver = DriverManager.getInstance().getDriver();
		driver.switchTo().window(parentWindow);
	}

	public String getCurrentUrl() {
		WebDriver driver = DriverManager.getInstance().getDriver();
		String curUrl = driver.getCurrentUrl();
		System.out.println("Current url is :" + curUrl);
		return curUrl;
	}

	public String getBrowserTitle() {
		WebDriver driver = DriverManager.getInstance().getDriver();
		System.out.println("Page title is :" + driver.getTitle());
		return driver.getTitle();
	}
	
	public String getPageSource(){
		WebDriver driver = DriverManager.getInstance().getDriver();
		System.out.println("Page source is :" + driver.getPageSource());
		return driver.getPageSource();
	}
	
	public void openUrl(String url){
		WebDriver driver = DriverManager.getInstance().getDriver();
		try {
			Thread.sleep(3000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println("Opening Url : " + url);
		driver.get(url);
	}
	
	public void browserGoBack(){
		WebDriver driver = DriverManager.getInstance().getDriver();
		driver.navigate().back();
	}
	
	public void waitTime(long time){
		try {
			Thread.sleep(time);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void browserGoForward(){
		WebDriver driver = DriverManager.getInstance().getDriver();
		driver.navigate().forward();
	}
	
	public void browserRefresh(){
		WebDriver driver = DriverManager.getInstance().getDriver();
		driver.navigate().refresh();
	}

	public void jsclick(WebElement element){
		WebDriver driver = DriverManager.getInstance().getDriver();
		JavascriptExecutor js = (JavascriptExecutor) driver;
		System.out.println("Using Javascript to click");
		js.executeScript("arguments[0].click();", element);
	}
	
	public void addCookie(String cookieName,String cookieValue){
		WebDriver driver = DriverManager.getInstance().getDriver();
		JavascriptExecutor js = (JavascriptExecutor) driver;
		System.out.println("Adding cookie " + cookieName + " with value " + cookieValue);
		String jsCookieString = String.format("document.cookie = '%s=%s; path = /;'", cookieName,cookieValue);
		    
		js.executeScript(jsCookieString);
	}
	
	public void openNewWindow(String url){
		WebDriver driver = DriverManager.getInstance().getDriver();
		JavascriptExecutor js = (JavascriptExecutor) driver;
		System.out.println("Using Javascript to open new window with url :" + url);
		js.executeScript("window.open(arguments[0]);",url);
	}
	
	public String getDisposableEmail(){
		WebDriver driver = DriverManager.getInstance().getDriver();
		openNewWindow("https://10minutemail.com");
		String email = null;
		String parentWindow = switchToNewWindow();
		
		try{
			email = driver.findElement(By.cssSelector("#mailAddress")).getAttribute("value");
		}catch(NoSuchElementException e){
			driver.get("https://10minutemail.net/");
			email = driver.findElement(By.cssSelector("#fe_text")).getAttribute("value");
		}catch(Exception e){
			driver.get("https://10minutemail.net/");
			email = driver.findElement(By.cssSelector(".lead")).getText();
		}
		
		driver.close();
		driver.switchTo().window(parentWindow);
		System.out.println("Disposable email for 10 minutes validity is: " + email);
		return email;
	}
	
	public void takeScreenshot(String filePath) throws IOException{
		WebDriver driver = DriverManager.getInstance().getDriver();
		File scrFile = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
		FileUtils.copyFile(scrFile, new File(filePath));
	}
	
	public void mouseOver(WebElement element){
		WebDriver driver = DriverManager.getInstance().getDriver();
		Actions actions = new Actions(driver);
		actions.moveToElement(element).click().build().perform();
	}
	
	public void scrollToElement(WebElement element){
		WebDriver driver = DriverManager.getInstance().getDriver();
		((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", element);
	}
	
	public void acceptAlert(){
		WebDriver driver = DriverManager.getInstance().getDriver();
		try{
			driver.switchTo().alert().accept();
		}catch(Exception e){
			// Do nothing
		}
	}
	
	public void dragAndDrop(WebElement sourceElement, WebElement destinationElement) {
		WebDriver driver = DriverManager.getInstance().getDriver();
		try {
			if (sourceElement.isDisplayed() && destinationElement.isDisplayed()) {
				Actions action = new Actions(driver);
				action.dragAndDrop(sourceElement, destinationElement).build().perform();
			} else {
				System.out.println("Element was not displayed to drag");
			}
		} catch (StaleElementReferenceException e) {
			System.out.println("Element with " + sourceElement + "or" + destinationElement + "is not attached to the page document "
					+ e.getStackTrace());
		} catch (NoSuchElementException e) {
			System.out.println("Element " + sourceElement + "or" + destinationElement + " was not found in DOM "+ e.getStackTrace());
		} catch (Exception e) {
			System.out.println("Error occurred while performing drag and drop operation "+ e.getStackTrace());
		}
	}
	
	public void waitForPageLoad() throws InterruptedException {
		WebDriver driver = DriverManager.getInstance().getDriver();
		JavascriptExecutor js = (JavascriptExecutor) driver;

		// Initially bellow given if condition will check ready state of page.
		// Check the page state if its complete, if not, poll every 2 second if state changes to complete
		// break if status changes to complete
		
        int waitTime = 0;
		while (waitTime > 120) {
	           Thread.sleep(1000);
	           if(js.executeScript("return document.readyState").toString().equals("complete")){
	        	   break;
	           }
	           waitTime = waitTime + 1;
		}
	}
}