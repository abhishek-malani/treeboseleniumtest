package desktopUITest;

import java.text.ParseException;

import org.openqa.selenium.UnhandledAlertException;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import base.BrowserHelper;
import base.TestBaseSetUp;
import desktopUIPageObjects.BookingHistory;
import desktopUIPageObjects.ConfirmationPage;
import desktopUIPageObjects.HomePage;
import desktopUIPageObjects.HotelDetailsPage;
import desktopUIPageObjects.HotelResultsPage;
import desktopUIPageObjects.ItineraryPage;
import org.testng.Assert;
import utils.CommonUtils;

public class AllHotelHDPage extends TestBaseSetUp {

	private HomePage homePage;
	private HotelResultsPage hotelResultsPage;
	private ItineraryPage itineraryPage;
	private ConfirmationPage confirmationPage;
	private HotelDetailsPage hotelDetailsPage;
	private BookingHistory bookingHistory;
	private BrowserHelper browserHelper;

/***
 * This test checks that in Prod for every hotel Hotel Details page loads
 * And if default room is available
 */
	@Test(groups = { "ProdSmoke","ProdSanity" })
	public void testAllHotelHDPageLoadsBengaluru(){
	    homePage = new HomePage();
	    hotelResultsPage = new HotelResultsPage();
	    hotelDetailsPage = new HotelDetailsPage();
	    itineraryPage = new ItineraryPage();
	    browserHelper = new BrowserHelper();
	    CommonUtils utils = new CommonUtils();
	    SoftAssert verify = new SoftAssert();
	    
	    String location = "Bengaluru";
	    int checkInFromCurrentDate = utils.getRandomNumber(15, 30);
	    int checkOutFromCurrentDate = checkInFromCurrentDate + 1;
	    int roomIndex = 1;
	    
	    homePage.doSearch(location, checkInFromCurrentDate, checkOutFromCurrentDate, roomIndex);
	    hotelResultsPage.verifyHotelResultsPagePresence();
	    String searchPageUrl = browserHelper.getCurrentUrl();
	    String[] hotelNames = hotelResultsPage.getDisplayedHotelNames();
	    
	    int i = 1;
	    for (String hotelName : hotelNames){
	    	System.out.println("Currently checking for "+ i + " st/th "+" Hotel: " + hotelName);
	    	hotelResultsPage.clickOnHotelByName(hotelName);
	    	hotelDetailsPage.verifyHotelDetailsPagePresence();
	    	verify.assertEquals(hotelDetailsPage.getHotelNameInHotelDetailsPage().toLowerCase(),hotelName.toLowerCase());
	    	browserHelper.openUrl(searchPageUrl);
	    	i = i + 1;
	    }
	    verify.assertAll();
	}
	
	@Test(groups = { "ProdSmoke","ProdSanity" })
	public void testAllHotelHDPageLoadsHyderabad(){
	    homePage = new HomePage();
	    hotelResultsPage = new HotelResultsPage();
	    hotelDetailsPage = new HotelDetailsPage();
	    itineraryPage = new ItineraryPage();
	    browserHelper = new BrowserHelper();
	    CommonUtils utils = new CommonUtils();
	    SoftAssert verify = new SoftAssert();
	    
	    String location = "Hyderabad";
	    int checkInFromCurrentDate = utils.getRandomNumber(15, 30);
	    int checkOutFromCurrentDate = checkInFromCurrentDate + 1;
	    int roomIndex = 1;
	    
	    homePage.doSearch(location, checkInFromCurrentDate, checkOutFromCurrentDate, roomIndex);
	    hotelResultsPage.verifyHotelResultsPagePresence();
	    String searchPageUrl = browserHelper.getCurrentUrl();
	    String[] hotelNames = hotelResultsPage.getDisplayedHotelNames();
	    
	    int i = 1;
	    for (String hotelName : hotelNames){
	    	System.out.println("Currently checking for "+ i + " st/th "+" Hotel: " + hotelName);
	    	hotelResultsPage.clickOnHotelByName(hotelName);
	    	hotelDetailsPage.verifyHotelDetailsPagePresence();
	    	verify.assertEquals(hotelDetailsPage.getHotelNameInHotelDetailsPage().toLowerCase(),hotelName.toLowerCase());
	    	browserHelper.openUrl(searchPageUrl);
	    	i = i + 1;
	    }
	    verify.assertAll();
	}
	
	@Test(groups = { "ProdSmoke","ProdSanity" })
	public void testAllHotelHDPageLoadsMumbai(){
	    homePage = new HomePage();
	    hotelResultsPage = new HotelResultsPage();
	    hotelDetailsPage = new HotelDetailsPage();
	    itineraryPage = new ItineraryPage();
	    browserHelper = new BrowserHelper();
	    CommonUtils utils = new CommonUtils();
	    SoftAssert verify = new SoftAssert();
	    
	    String location = "Mumbai";
	    int checkInFromCurrentDate = utils.getRandomNumber(15, 30);
	    int checkOutFromCurrentDate = checkInFromCurrentDate + 1;
	    int roomIndex = 1;
	    
	    homePage.doSearch(location, checkInFromCurrentDate, checkOutFromCurrentDate, roomIndex);
	    hotelResultsPage.verifyHotelResultsPagePresence();
	    String searchPageUrl = browserHelper.getCurrentUrl();
	    String[] hotelNames = hotelResultsPage.getDisplayedHotelNames();
	    
	    int i = 1;
	    for (String hotelName : hotelNames){
	    	System.out.println("Currently checking for "+ i + " st/th "+" Hotel: " + hotelName);
	    	hotelResultsPage.clickOnHotelByName(hotelName);
	    	hotelDetailsPage.verifyHotelDetailsPagePresence();
	    	verify.assertEquals(hotelDetailsPage.getHotelNameInHotelDetailsPage().toLowerCase(),hotelName.toLowerCase());
	    	browserHelper.openUrl(searchPageUrl);
	    	i = i + 1;
	    }
	    verify.assertAll();
	}
	
	@Test(groups = { "ProdSmoke","ProdSanity" })
	public void testAllHotelHDPageLoadsChennai(){
	    homePage = new HomePage();
	    hotelResultsPage = new HotelResultsPage();
	    hotelDetailsPage = new HotelDetailsPage();
	    itineraryPage = new ItineraryPage();
	    browserHelper = new BrowserHelper();
	    CommonUtils utils = new CommonUtils();
	    SoftAssert verify = new SoftAssert();
	    
	    String location = "Chennai";
	    int checkInFromCurrentDate = utils.getRandomNumber(15, 30);
	    int checkOutFromCurrentDate = checkInFromCurrentDate + 1;
	    int roomIndex = 1;
	    
	    homePage.doSearch(location, checkInFromCurrentDate, checkOutFromCurrentDate, roomIndex);
	    hotelResultsPage.verifyHotelResultsPagePresence();
	    String searchPageUrl = browserHelper.getCurrentUrl();
	    String[] hotelNames = hotelResultsPage.getDisplayedHotelNames();
	    
	    int i = 1;
	    for (String hotelName : hotelNames){
	    	System.out.println("Currently checking for "+ i + " st/th "+" Hotel: " + hotelName);
	    	try{
	    		hotelResultsPage.clickOnHotelByName(hotelName);
	    		hotelDetailsPage.verifyHotelDetailsPagePresence();
	    	}catch(UnhandledAlertException e){
	    		browserHelper.acceptAlert();
	    	}
	    	verify.assertEquals(hotelDetailsPage.getHotelNameInHotelDetailsPage().toLowerCase(),hotelName.toLowerCase());
	    	browserHelper.openUrl(searchPageUrl);
	    	i = i + 1;
	    }
	    verify.assertAll();
	}
	
	@Test(groups = { "ProdSmoke","ProdSanity" })
	public void testAllHotelHDPageLoadsJaipur(){
	    homePage = new HomePage();
	    hotelResultsPage = new HotelResultsPage();
	    hotelDetailsPage = new HotelDetailsPage();
	    itineraryPage = new ItineraryPage();
	    browserHelper = new BrowserHelper();
	    CommonUtils utils = new CommonUtils();
	    SoftAssert verify = new SoftAssert();
	    
	    String location = "Jaipur";
	    int checkInFromCurrentDate = utils.getRandomNumber(15, 30);
	    int checkOutFromCurrentDate = checkInFromCurrentDate + 1;
	    int roomIndex = 1;
	    
	    homePage.doSearch(location, checkInFromCurrentDate, checkOutFromCurrentDate, roomIndex);
	    hotelResultsPage.verifyHotelResultsPagePresence();
	    String searchPageUrl = browserHelper.getCurrentUrl();
	    String[] hotelNames = hotelResultsPage.getDisplayedHotelNames();
	    
	    int i = 1;
	    for (String hotelName : hotelNames){
	    	System.out.println("Currently checking for "+ i + " st/th "+" Hotel: " + hotelName);
	    	hotelResultsPage.clickOnHotelByName(hotelName);
	    	hotelDetailsPage.verifyHotelDetailsPagePresence();
	    	verify.assertEquals(hotelDetailsPage.getHotelNameInHotelDetailsPage().toLowerCase(),hotelName.toLowerCase());
	    	browserHelper.openUrl(searchPageUrl);
	    	i = i + 1;
	    }
	    verify.assertAll();
	}
	
	
	@Test(groups = { "ProdSmoke","ProdSanity" })
	public void testAllHotelHDPageLoadsNewDelhi(){
	    homePage = new HomePage();
	    hotelResultsPage = new HotelResultsPage();
	    hotelDetailsPage = new HotelDetailsPage();
	    itineraryPage = new ItineraryPage();
	    browserHelper = new BrowserHelper();
	    CommonUtils utils = new CommonUtils();
	    SoftAssert verify = new SoftAssert();
	    
	    String location = "New Delhi";
	    int checkInFromCurrentDate = utils.getRandomNumber(15, 30);
	    int checkOutFromCurrentDate = checkInFromCurrentDate + 1;
	    int roomIndex = 1;
	    
	    homePage.doSearch(location, checkInFromCurrentDate, checkOutFromCurrentDate, roomIndex);
	    hotelResultsPage.verifyHotelResultsPagePresence();
	    String searchPageUrl = browserHelper.getCurrentUrl();
	    String[] hotelNames = hotelResultsPage.getDisplayedHotelNames();
	    
	    int i = 1;
	    for (String hotelName : hotelNames){
	    	System.out.println("Currently checking for "+ i + " st/th "+" Hotel: " + hotelName);
	    	hotelResultsPage.clickOnHotelByName(hotelName);
	    	hotelDetailsPage.verifyHotelDetailsPagePresence();
	    	verify.assertEquals(hotelDetailsPage.getHotelNameInHotelDetailsPage().toLowerCase(),hotelName.toLowerCase());
	    	browserHelper.openUrl(searchPageUrl);
	    	i = i + 1;
	    }
	    verify.assertAll();
	}
	
	
	@Test(groups = { "ProdSmoke","ProdSanity" })
	public void testAllHotelHDPageLoadsPune(){
	    homePage = new HomePage();
	    hotelResultsPage = new HotelResultsPage();
	    hotelDetailsPage = new HotelDetailsPage();
	    itineraryPage = new ItineraryPage();
	    browserHelper = new BrowserHelper();
	    CommonUtils utils = new CommonUtils();
	    SoftAssert verify = new SoftAssert();
	    
	    String location = "Pune";
	    int checkInFromCurrentDate = utils.getRandomNumber(15, 30);
	    int checkOutFromCurrentDate = checkInFromCurrentDate + 1;
	    int roomIndex = 1;
	    
	    homePage.doSearch(location, checkInFromCurrentDate, checkOutFromCurrentDate, roomIndex);
	    hotelResultsPage.verifyHotelResultsPagePresence();
	    String searchPageUrl = browserHelper.getCurrentUrl();
	    String[] hotelNames = hotelResultsPage.getDisplayedHotelNames();
	    
	    int i = 1;
	    for (String hotelName : hotelNames){
	    	System.out.println("Currently checking for "+ i + " st/th "+" Hotel: " + hotelName);
	    	hotelResultsPage.clickOnHotelByName(hotelName);
	    	hotelDetailsPage.verifyHotelDetailsPagePresence();
	    	verify.assertEquals(hotelDetailsPage.getHotelNameInHotelDetailsPage().toLowerCase(),hotelName.toLowerCase());
	    	browserHelper.openUrl(searchPageUrl);
	    	i = i + 1;
	    }
	    verify.assertAll();
	}
	
	@Test(groups = { "ProdSmoke","ProdSanity" })
	public void testAllHotelHDPageLoadsGoa(){
	    homePage = new HomePage();
	    hotelResultsPage = new HotelResultsPage();
	    hotelDetailsPage = new HotelDetailsPage();
	    itineraryPage = new ItineraryPage();
	    browserHelper = new BrowserHelper();
	    CommonUtils utils = new CommonUtils();
	    SoftAssert verify = new SoftAssert();
	    
	    String location = "Goa";
	    int checkInFromCurrentDate = utils.getRandomNumber(15, 30);
	    int checkOutFromCurrentDate = checkInFromCurrentDate + 1;
	    int roomIndex = 1;
	    
	    homePage.doSearch(location, checkInFromCurrentDate, checkOutFromCurrentDate, roomIndex);
	    hotelResultsPage.verifyHotelResultsPagePresence();
	    String searchPageUrl = browserHelper.getCurrentUrl();
	    String[] hotelNames = hotelResultsPage.getDisplayedHotelNames();
	    
	    int i = 1;
	    for (String hotelName : hotelNames){
	    	System.out.println("Currently checking for "+ i + " st/th "+" Hotel: " + hotelName);
	    	hotelResultsPage.clickOnHotelByName(hotelName);
	    	hotelDetailsPage.verifyHotelDetailsPagePresence();
	    	verify.assertEquals(hotelDetailsPage.getHotelNameInHotelDetailsPage().toLowerCase(),hotelName.toLowerCase());
	    	browserHelper.openUrl(searchPageUrl);
	    	i = i + 1;
	    }
	    verify.assertAll();
	}
	
	@Test(groups = { "ProdSmoke","ProdSanity" })
	public void testAllHotelHDPageLoadsKolkata(){
	    homePage = new HomePage();
	    hotelResultsPage = new HotelResultsPage();
	    hotelDetailsPage = new HotelDetailsPage();
	    itineraryPage = new ItineraryPage();
	    browserHelper = new BrowserHelper();
	    CommonUtils utils = new CommonUtils();
	    SoftAssert verify = new SoftAssert();
	    
	    String location = "Kolkata";
	    int checkInFromCurrentDate = utils.getRandomNumber(15, 30);
	    int checkOutFromCurrentDate = checkInFromCurrentDate + 1;
	    int roomIndex = 1;
	    
	    homePage.doSearch(location, checkInFromCurrentDate, checkOutFromCurrentDate, roomIndex);
	    hotelResultsPage.verifyHotelResultsPagePresence();
	    String searchPageUrl = browserHelper.getCurrentUrl();
	    String[] hotelNames = hotelResultsPage.getDisplayedHotelNames();
	    
	    int i = 1;
	    for (String hotelName : hotelNames){
	    	System.out.println("Currently checking for "+ i + " st/th "+" Hotel: " + hotelName);
	    	hotelResultsPage.clickOnHotelByName(hotelName);
	    	hotelDetailsPage.verifyHotelDetailsPagePresence();
	    	verify.assertEquals(hotelDetailsPage.getHotelNameInHotelDetailsPage().toLowerCase(),hotelName.toLowerCase());
	    	browserHelper.openUrl(searchPageUrl);
	    	i = i + 1;
	    }
	    verify.assertAll();
	}
	
	@Test(groups = { "ProdSmoke","ProdSanity" })
	public void testAllHotelHDPageLoadsMysore(){
	    homePage = new HomePage();
	    hotelResultsPage = new HotelResultsPage();
	    hotelDetailsPage = new HotelDetailsPage();
	    itineraryPage = new ItineraryPage();
	    browserHelper = new BrowserHelper();
	    CommonUtils utils = new CommonUtils();
	    SoftAssert verify = new SoftAssert();
	    
	    String location = "Mysore";
	    int checkInFromCurrentDate = utils.getRandomNumber(15, 30);
	    int checkOutFromCurrentDate = checkInFromCurrentDate + 1;
	    int roomIndex = 1;
	    
	    homePage.doSearch(location, checkInFromCurrentDate, checkOutFromCurrentDate, roomIndex);
	    hotelResultsPage.verifyHotelResultsPagePresence();
	    String searchPageUrl = browserHelper.getCurrentUrl();
	    String[] hotelNames = hotelResultsPage.getDisplayedHotelNames();
	    
	    int i = 1;
	    for (String hotelName : hotelNames){
	    	System.out.println("Currently checking for "+ i + " st/th "+" Hotel: " + hotelName);
	    	hotelResultsPage.clickOnHotelByName(hotelName);
	    	hotelDetailsPage.verifyHotelDetailsPagePresence();
	    	verify.assertEquals(hotelDetailsPage.getHotelNameInHotelDetailsPage().toLowerCase(),hotelName.toLowerCase());
	    	browserHelper.openUrl(searchPageUrl);
	    	i = i + 1;
	    }
	    verify.assertAll();
	}
	
	@Test(groups = { "ProdSmoke","ProdSanity" })
	public void testAllHotelHDPageLoadsGurgaon(){
	    homePage = new HomePage();
	    hotelResultsPage = new HotelResultsPage();
	    hotelDetailsPage = new HotelDetailsPage();
	    itineraryPage = new ItineraryPage();
	    browserHelper = new BrowserHelper();
	    CommonUtils utils = new CommonUtils();
	    SoftAssert verify = new SoftAssert();
	    
	    String location = "Gurgaon";
	    int checkInFromCurrentDate = utils.getRandomNumber(15, 30);
	    int checkOutFromCurrentDate = checkInFromCurrentDate + 1;
	    int roomIndex = 1;
	    
	    homePage.doSearch(location, checkInFromCurrentDate, checkOutFromCurrentDate, roomIndex);
	    hotelResultsPage.verifyHotelResultsPagePresence();
	    String searchPageUrl = browserHelper.getCurrentUrl();
	    String[] hotelNames = hotelResultsPage.getDisplayedHotelNames();
	    
	    int i = 1;
	    for (String hotelName : hotelNames){
	    	System.out.println("Currently checking for "+ i + " st/th "+" Hotel: " + hotelName);
	    	hotelResultsPage.clickOnHotelByName(hotelName);
	    	hotelDetailsPage.verifyHotelDetailsPagePresence();
	    	verify.assertEquals(hotelDetailsPage.getHotelNameInHotelDetailsPage().toLowerCase(),hotelName.toLowerCase());
	    	browserHelper.openUrl(searchPageUrl);
	    	i = i + 1;
	    }
	    verify.assertAll();
	}
	
	@Test(groups = { "ProdSmoke","ProdSanity" })
	public void testAllHotelHDPageLoadsOoty(){
	    homePage = new HomePage();
	    hotelResultsPage = new HotelResultsPage();
	    hotelDetailsPage = new HotelDetailsPage();
	    itineraryPage = new ItineraryPage();
	    browserHelper = new BrowserHelper();
	    CommonUtils utils = new CommonUtils();
	    SoftAssert verify = new SoftAssert();
	    
	    String location = "Ooty";
	    int checkInFromCurrentDate = utils.getRandomNumber(15, 30);
	    int checkOutFromCurrentDate = checkInFromCurrentDate + 1;
	    int roomIndex = 1;
	    
	    homePage.doSearch(location, checkInFromCurrentDate, checkOutFromCurrentDate, roomIndex);
	    hotelResultsPage.verifyHotelResultsPagePresence();
	    String searchPageUrl = browserHelper.getCurrentUrl();
	    String[] hotelNames = hotelResultsPage.getDisplayedHotelNames();
	    
	    int i = 1;
	    for (String hotelName : hotelNames){
	    	System.out.println("Currently checking for "+ i + " st/th "+" Hotel: " + hotelName);
	    	hotelResultsPage.clickOnHotelByName(hotelName);
	    	hotelDetailsPage.verifyHotelDetailsPagePresence();
	    	verify.assertEquals(hotelDetailsPage.getHotelNameInHotelDetailsPage().toLowerCase(),hotelName.toLowerCase());
	    	browserHelper.openUrl(searchPageUrl);
	    	i = i + 1;
	    }
	    verify.assertAll();
	}
	
	@Test(groups = { "ProdSmoke","ProdSanity" })
	public void testAllHotelHDPageLoadsManali(){
	    homePage = new HomePage();
	    hotelResultsPage = new HotelResultsPage();
	    hotelDetailsPage = new HotelDetailsPage();
	    itineraryPage = new ItineraryPage();
	    browserHelper = new BrowserHelper();
	    CommonUtils utils = new CommonUtils();
	    SoftAssert verify = new SoftAssert();
	    
	    String location = "Manali";
	    int checkInFromCurrentDate = utils.getRandomNumber(15, 30);
	    int checkOutFromCurrentDate = checkInFromCurrentDate + 1;
	    int roomIndex = 1;
	    
	    homePage.doSearch(location, checkInFromCurrentDate, checkOutFromCurrentDate, roomIndex);
	    hotelResultsPage.verifyHotelResultsPagePresence();
	    String searchPageUrl = browserHelper.getCurrentUrl();
	    String[] hotelNames = hotelResultsPage.getDisplayedHotelNames();
	    
	    int i = 1;
	    for (String hotelName : hotelNames){
	    	System.out.println("Currently checking for "+ i + " st/th "+" Hotel: " + hotelName);
	    	hotelResultsPage.clickOnHotelByName(hotelName);
	    	hotelDetailsPage.verifyHotelDetailsPagePresence();
	    	verify.assertEquals(hotelDetailsPage.getHotelNameInHotelDetailsPage().toLowerCase(),hotelName.toLowerCase());
	    	browserHelper.openUrl(searchPageUrl);
	    	i = i + 1;
	    }
	    verify.assertAll();
	}
	
	@Test(groups = { "ProdSmoke","ProdSanity" })
	public void testAllHotelHDPageLoadsAgra(){
	    homePage = new HomePage();
	    hotelResultsPage = new HotelResultsPage();
	    hotelDetailsPage = new HotelDetailsPage();
	    itineraryPage = new ItineraryPage();
	    browserHelper = new BrowserHelper();
	    CommonUtils utils = new CommonUtils();
	    SoftAssert verify = new SoftAssert();
	    
	    String location = "Agra";
	    int checkInFromCurrentDate = utils.getRandomNumber(15, 30);
	    int checkOutFromCurrentDate = checkInFromCurrentDate + 1;
	    int roomIndex = 1;
	    
	    homePage.doSearch(location, checkInFromCurrentDate, checkOutFromCurrentDate, roomIndex);
	    hotelResultsPage.verifyHotelResultsPagePresence();
	    String searchPageUrl = browserHelper.getCurrentUrl();
	    String[] hotelNames = hotelResultsPage.getDisplayedHotelNames();
	    
	    int i = 1;
	    for (String hotelName : hotelNames){
	    	System.out.println("Currently checking for "+ i + " st/th "+" Hotel: " + hotelName);
	    	hotelResultsPage.clickOnHotelByName(hotelName);
	    	hotelDetailsPage.verifyHotelDetailsPagePresence();
	    	verify.assertEquals(hotelDetailsPage.getHotelNameInHotelDetailsPage().toLowerCase(),hotelName.toLowerCase());
	    	browserHelper.openUrl(searchPageUrl);
	    	i = i + 1;
	    }
	    verify.assertAll();
	}
	
	@Test(groups = { "ProdSmoke","ProdSanity" })
	public void testAllHotelHDPageLoadsUdaipur(){
	    homePage = new HomePage();
	    hotelResultsPage = new HotelResultsPage();
	    hotelDetailsPage = new HotelDetailsPage();
	    itineraryPage = new ItineraryPage();
	    browserHelper = new BrowserHelper();
	    CommonUtils utils = new CommonUtils();
	    SoftAssert verify = new SoftAssert();
	    
	    String location = "Udaipur";
	    int checkInFromCurrentDate = utils.getRandomNumber(15, 30);
	    int checkOutFromCurrentDate = checkInFromCurrentDate + 1;
	    int roomIndex = 1;
	    
	    homePage.doSearch(location, checkInFromCurrentDate, checkOutFromCurrentDate, roomIndex);
	    hotelResultsPage.verifyHotelResultsPagePresence();
	    String searchPageUrl = browserHelper.getCurrentUrl();
	    String[] hotelNames = hotelResultsPage.getDisplayedHotelNames();
	    
	    int i = 1;
	    for (String hotelName : hotelNames){
	    	System.out.println("Currently checking for "+ i + " st/th "+" Hotel: " + hotelName);
	    	hotelResultsPage.clickOnHotelByName(hotelName);
	    	hotelDetailsPage.verifyHotelDetailsPagePresence();
	    	verify.assertEquals(hotelDetailsPage.getHotelNameInHotelDetailsPage().toLowerCase(),hotelName.toLowerCase());
	    	browserHelper.openUrl(searchPageUrl);
	    	i = i + 1;
	    }
	    verify.assertAll();
	}
	
	@Test(groups = { "ProdSmoke","ProdSanity" })
	public void testAllHotelHDPageLoadsKodaikanal(){
	    homePage = new HomePage();
	    hotelResultsPage = new HotelResultsPage();
	    hotelDetailsPage = new HotelDetailsPage();
	    itineraryPage = new ItineraryPage();
	    browserHelper = new BrowserHelper();
	    CommonUtils utils = new CommonUtils();
	    SoftAssert verify = new SoftAssert();
	    
	    String location = "Kodaikanal";
	    int checkInFromCurrentDate = utils.getRandomNumber(15, 30);
	    int checkOutFromCurrentDate = checkInFromCurrentDate + 1;
	    int roomIndex = 1;
	    
	    homePage.doSearch(location, checkInFromCurrentDate, checkOutFromCurrentDate, roomIndex);
	    hotelResultsPage.verifyHotelResultsPagePresence();
	    String searchPageUrl = browserHelper.getCurrentUrl();
	    String[] hotelNames = hotelResultsPage.getDisplayedHotelNames();
	    
	    int i = 1;
	    for (String hotelName : hotelNames){
	    	System.out.println("Currently checking for "+ i + " st/th "+" Hotel: " + hotelName);
	    	hotelResultsPage.clickOnHotelByName(hotelName);
	    	hotelDetailsPage.verifyHotelDetailsPagePresence();
	    	verify.assertEquals(hotelDetailsPage.getHotelNameInHotelDetailsPage().toLowerCase(),hotelName.toLowerCase());
	    	browserHelper.openUrl(searchPageUrl);
	    	i = i + 1;
	    }
	    verify.assertAll();
	}
	
	@Test(groups = { "ProdSmoke","ProdSanity" })
	public void testAllHotelHDPageLoadsAurangabad(){
	    homePage = new HomePage();
	    hotelResultsPage = new HotelResultsPage();
	    hotelDetailsPage = new HotelDetailsPage();
	    itineraryPage = new ItineraryPage();
	    browserHelper = new BrowserHelper();
	    CommonUtils utils = new CommonUtils();
	    SoftAssert verify = new SoftAssert();
	    
	    String location = "Aurangabad";
	    int checkInFromCurrentDate = utils.getRandomNumber(15, 30);
	    int checkOutFromCurrentDate = checkInFromCurrentDate + 1;
	    int roomIndex = 1;
	    
	    homePage.doSearch(location, checkInFromCurrentDate, checkOutFromCurrentDate, roomIndex);
	    hotelResultsPage.verifyHotelResultsPagePresence();
	    String searchPageUrl = browserHelper.getCurrentUrl();
	    String[] hotelNames = hotelResultsPage.getDisplayedHotelNames();
	    
	    int i = 1;
	    for (String hotelName : hotelNames){
	    	System.out.println("Currently checking for "+ i + " st/th "+" Hotel: " + hotelName);
	    	hotelResultsPage.clickOnHotelByName(hotelName);
	    	hotelDetailsPage.verifyHotelDetailsPagePresence();
	    	verify.assertEquals(hotelDetailsPage.getHotelNameInHotelDetailsPage().toLowerCase(),hotelName.toLowerCase());
	    	browserHelper.openUrl(searchPageUrl);
	    	i = i + 1;
	    }
	    verify.assertAll();
	}
	
	@Test(groups = { "ProdSmoke","ProdSanity" })
	public void testAllHotelHDPageLoadsNoida(){
	    homePage = new HomePage();
	    hotelResultsPage = new HotelResultsPage();
	    hotelDetailsPage = new HotelDetailsPage();
	    itineraryPage = new ItineraryPage();
	    browserHelper = new BrowserHelper();
	    CommonUtils utils = new CommonUtils();
	    SoftAssert verify = new SoftAssert();
	    
	    String location = "Noida";
	    int checkInFromCurrentDate = utils.getRandomNumber(15, 30);
	    int checkOutFromCurrentDate = checkInFromCurrentDate + 1;
	    int roomIndex = 1;
	    
	    homePage.doSearch(location, checkInFromCurrentDate, checkOutFromCurrentDate, roomIndex);
	    hotelResultsPage.verifyHotelResultsPagePresence();
	    String searchPageUrl = browserHelper.getCurrentUrl();
	    String[] hotelNames = hotelResultsPage.getDisplayedHotelNames();
	    
	    int i = 1;
	    for (String hotelName : hotelNames){
	    	System.out.println("Currently checking for "+ i + " st/th "+" Hotel: " + hotelName);
	    	hotelResultsPage.clickOnHotelByName(hotelName);
	    	hotelDetailsPage.verifyHotelDetailsPagePresence();
	    	verify.assertEquals(hotelDetailsPage.getHotelNameInHotelDetailsPage().toLowerCase(),hotelName.toLowerCase());
	    	browserHelper.openUrl(searchPageUrl);
	    	i = i + 1;
	    }
	    verify.assertAll();
	}
	
	@Test(groups = { "ProdSmoke","ProdSanity" })
	public void testAllHotelHDPageLoadsAhmedabad(){
	    homePage = new HomePage();
	    hotelResultsPage = new HotelResultsPage();
	    hotelDetailsPage = new HotelDetailsPage();
	    itineraryPage = new ItineraryPage();
	    browserHelper = new BrowserHelper();
	    CommonUtils utils = new CommonUtils();
	    SoftAssert verify = new SoftAssert();
	    
	    String location = "Ahmedabad";
	    int checkInFromCurrentDate = utils.getRandomNumber(15, 30);
	    int checkOutFromCurrentDate = checkInFromCurrentDate + 1;
	    int roomIndex = 1;
	    
	    homePage.doSearch(location, checkInFromCurrentDate, checkOutFromCurrentDate, roomIndex);
	    hotelResultsPage.verifyHotelResultsPagePresence();
	    String searchPageUrl = browserHelper.getCurrentUrl();
	    String[] hotelNames = hotelResultsPage.getDisplayedHotelNames();
	    
	    int i = 1;
	    for (String hotelName : hotelNames){
	    	System.out.println("Currently checking for "+ i + " st/th "+" Hotel: " + hotelName);
	    	hotelResultsPage.clickOnHotelByName(hotelName);
	    	hotelDetailsPage.verifyHotelDetailsPagePresence();
	    	verify.assertEquals(hotelDetailsPage.getHotelNameInHotelDetailsPage().toLowerCase(),hotelName.toLowerCase());
	    	browserHelper.openUrl(searchPageUrl);
	    	i = i + 1;
	    }
	    verify.assertAll();
	}
	
	@Test(groups = { "ProdSmoke","ProdSanity" })
	public void testAllHotelHDPageLoadsChandigarh(){
	    homePage = new HomePage();
	    hotelResultsPage = new HotelResultsPage();
	    hotelDetailsPage = new HotelDetailsPage();
	    itineraryPage = new ItineraryPage();
	    browserHelper = new BrowserHelper();
	    CommonUtils utils = new CommonUtils();
	    SoftAssert verify = new SoftAssert();
	    
	    String location = "Chandigarh";
	    int checkInFromCurrentDate = utils.getRandomNumber(15, 30);
	    int checkOutFromCurrentDate = checkInFromCurrentDate + 1;
	    int roomIndex = 1;
	    
	    homePage.doSearch(location, checkInFromCurrentDate, checkOutFromCurrentDate, roomIndex);
	    hotelResultsPage.verifyHotelResultsPagePresence();
	    String searchPageUrl = browserHelper.getCurrentUrl();
	    String[] hotelNames = hotelResultsPage.getDisplayedHotelNames();
	    
	    int i = 1;
	    for (String hotelName : hotelNames){
	    	System.out.println("Currently checking for "+ i + " st/th "+" Hotel: " + hotelName);
	    	hotelResultsPage.clickOnHotelByName(hotelName);
	    	hotelDetailsPage.verifyHotelDetailsPagePresence();
	    	verify.assertEquals(hotelDetailsPage.getHotelNameInHotelDetailsPage().toLowerCase(),hotelName.toLowerCase());
	    	browserHelper.openUrl(searchPageUrl);
	    	i = i + 1;
	    }
	    verify.assertAll();
	}
	
	@Test(groups = { "ProdSmoke","ProdSanity" })
	public void testAllHotelHDPageLoadsCoimbatore(){
	    homePage = new HomePage();
	    hotelResultsPage = new HotelResultsPage();
	    hotelDetailsPage = new HotelDetailsPage();
	    itineraryPage = new ItineraryPage();
	    browserHelper = new BrowserHelper();
	    CommonUtils utils = new CommonUtils();
	    SoftAssert verify = new SoftAssert();
	    
	    String location = "Coimbatore";
	    int checkInFromCurrentDate = utils.getRandomNumber(15, 30);
	    int checkOutFromCurrentDate = checkInFromCurrentDate + 1;
	    int roomIndex = 1;
	    
	    homePage.doSearch(location, checkInFromCurrentDate, checkOutFromCurrentDate, roomIndex);
	    hotelResultsPage.verifyHotelResultsPagePresence();
	    String searchPageUrl = browserHelper.getCurrentUrl();
	    String[] hotelNames = hotelResultsPage.getDisplayedHotelNames();
	    
	    int i = 1;
	    for (String hotelName : hotelNames){
	    	System.out.println("Currently checking for "+ i + " st/th "+" Hotel: " + hotelName);
	    	hotelResultsPage.clickOnHotelByName(hotelName);
	    	hotelDetailsPage.verifyHotelDetailsPagePresence();
	    	verify.assertEquals(hotelDetailsPage.getHotelNameInHotelDetailsPage().toLowerCase(),hotelName.toLowerCase());
	    	browserHelper.openUrl(searchPageUrl);
	    	i = i + 1;
	    }
	    verify.assertAll();
	}
	
	@Test(groups = { "ProdSmoke","ProdSanity" })
	public void testAllHotelHDPageLoadsIndore(){
	    homePage = new HomePage();
	    hotelResultsPage = new HotelResultsPage();
	    hotelDetailsPage = new HotelDetailsPage();
	    itineraryPage = new ItineraryPage();
	    browserHelper = new BrowserHelper();
	    CommonUtils utils = new CommonUtils();
	    SoftAssert verify = new SoftAssert();
	    
	    String location = "Indore";
	    int checkInFromCurrentDate = utils.getRandomNumber(15, 30);
	    int checkOutFromCurrentDate = checkInFromCurrentDate + 1;
	    int roomIndex = 1;
	    
	    homePage.doSearch(location, checkInFromCurrentDate, checkOutFromCurrentDate, roomIndex);
	    hotelResultsPage.verifyHotelResultsPagePresence();
	    String searchPageUrl = browserHelper.getCurrentUrl();
	    String[] hotelNames = hotelResultsPage.getDisplayedHotelNames();
	    
	    int i = 1;
	    for (String hotelName : hotelNames){
	    	System.out.println("Currently checking for "+ i + " st/th "+" Hotel: " + hotelName);
	    	hotelResultsPage.clickOnHotelByName(hotelName);
	    	hotelDetailsPage.verifyHotelDetailsPagePresence();
	    	verify.assertEquals(hotelDetailsPage.getHotelNameInHotelDetailsPage().toLowerCase(),hotelName.toLowerCase());
	    	browserHelper.openUrl(searchPageUrl);
	    	i = i + 1;
	    }
	    verify.assertAll();
	}
	
	@Test(groups = { "ProdSmoke","ProdSanity" })
	public void testAllHotelHDPageLoadsPondicherry(){
	    homePage = new HomePage();
	    hotelResultsPage = new HotelResultsPage();
	    hotelDetailsPage = new HotelDetailsPage();
	    itineraryPage = new ItineraryPage();
	    browserHelper = new BrowserHelper();
	    CommonUtils utils = new CommonUtils();
	    SoftAssert verify = new SoftAssert();
	    
	    String location = "Pondicherry";
	    int checkInFromCurrentDate = utils.getRandomNumber(15, 30);
	    int checkOutFromCurrentDate = checkInFromCurrentDate + 1;
	    int roomIndex = 1;
	    
	    homePage.doSearch(location, checkInFromCurrentDate, checkOutFromCurrentDate, roomIndex);
	    hotelResultsPage.verifyHotelResultsPagePresence();
	    String searchPageUrl = browserHelper.getCurrentUrl();
	    String[] hotelNames = hotelResultsPage.getDisplayedHotelNames();
	    
	    int i = 1;
	    for (String hotelName : hotelNames){
	    	System.out.println("Currently checking for "+ i + " st/th "+" Hotel: " + hotelName);
	    	hotelResultsPage.clickOnHotelByName(hotelName);
	    	hotelDetailsPage.verifyHotelDetailsPagePresence();
	    	verify.assertEquals(hotelDetailsPage.getHotelNameInHotelDetailsPage().toLowerCase(),hotelName.toLowerCase());
	    	browserHelper.openUrl(searchPageUrl);
	    	i = i + 1;
	    }
	    verify.assertAll();
	}
	
	@Test(groups = { "ProdSmoke","ProdSanity" })
	public void testAllHotelHDPageLoadsNainital(){
	    homePage = new HomePage();
	    hotelResultsPage = new HotelResultsPage();
	    hotelDetailsPage = new HotelDetailsPage();
	    itineraryPage = new ItineraryPage();
	    browserHelper = new BrowserHelper();
	    CommonUtils utils = new CommonUtils();
	    SoftAssert verify = new SoftAssert();
	    
	    String location = "Nainital";
	    int checkInFromCurrentDate = utils.getRandomNumber(15, 30);
	    int checkOutFromCurrentDate = checkInFromCurrentDate + 1;
	    int roomIndex = 1;
	    
	    homePage.doSearch(location, checkInFromCurrentDate, checkOutFromCurrentDate, roomIndex);
	    hotelResultsPage.verifyHotelResultsPagePresence();
	    String searchPageUrl = browserHelper.getCurrentUrl();
	    String[] hotelNames = hotelResultsPage.getDisplayedHotelNames();
	    
	    int i = 1;
	    for (String hotelName : hotelNames){
	    	System.out.println("Currently checking for "+ i + " st/th "+" Hotel: " + hotelName);
	    	hotelResultsPage.clickOnHotelByName(hotelName);
	    	hotelDetailsPage.verifyHotelDetailsPagePresence();
	    	verify.assertEquals(hotelDetailsPage.getHotelNameInHotelDetailsPage().toLowerCase(),hotelName.toLowerCase());
	    	browserHelper.openUrl(searchPageUrl);
	    	i = i + 1;
	    }
	    verify.assertAll();
	}
	
	@Test(groups = { "ProdSmoke","ProdSanity" })
	public void testAllHotelHDPageLoadsShirdi(){
	    homePage = new HomePage();
	    hotelResultsPage = new HotelResultsPage();
	    hotelDetailsPage = new HotelDetailsPage();
	    itineraryPage = new ItineraryPage();
	    browserHelper = new BrowserHelper();
	    CommonUtils utils = new CommonUtils();
	    SoftAssert verify = new SoftAssert();
	    
	    String location = "Shirdi";
	    int checkInFromCurrentDate = utils.getRandomNumber(15, 30);
	    int checkOutFromCurrentDate = checkInFromCurrentDate + 1;
	    int roomIndex = 1;
	    
	    homePage.doSearch(location, checkInFromCurrentDate, checkOutFromCurrentDate, roomIndex);
	    hotelResultsPage.verifyHotelResultsPagePresence();
	    String searchPageUrl = browserHelper.getCurrentUrl();
	    String[] hotelNames = hotelResultsPage.getDisplayedHotelNames();
	    
	    int i = 1;
	    for (String hotelName : hotelNames){
	    	System.out.println("Currently checking for "+ i + " st/th "+" Hotel: " + hotelName);
	    	hotelResultsPage.clickOnHotelByName(hotelName);
	    	hotelDetailsPage.verifyHotelDetailsPagePresence();
	    	verify.assertEquals(hotelDetailsPage.getHotelNameInHotelDetailsPage().toLowerCase(),hotelName.toLowerCase());
	    	browserHelper.openUrl(searchPageUrl);
	    	i = i + 1;
	    }
	    verify.assertAll();
	}
	
	@Test(groups = { "ProdSmoke","ProdSanity" })
	public void testAllHotelHDPageLoadsKochi(){
	    homePage = new HomePage();
	    hotelResultsPage = new HotelResultsPage();
	    hotelDetailsPage = new HotelDetailsPage();
	    itineraryPage = new ItineraryPage();
	    browserHelper = new BrowserHelper();
	    CommonUtils utils = new CommonUtils();
	    SoftAssert verify = new SoftAssert();
	    
	    String location = "Kochi";
	    int checkInFromCurrentDate = utils.getRandomNumber(15, 30);
	    int checkOutFromCurrentDate = checkInFromCurrentDate + 1;
	    int roomIndex = 1;
	    
	    homePage.doSearch(location, checkInFromCurrentDate, checkOutFromCurrentDate, roomIndex);
	    hotelResultsPage.verifyHotelResultsPagePresence();
	    String searchPageUrl = browserHelper.getCurrentUrl();
	    String[] hotelNames = hotelResultsPage.getDisplayedHotelNames();
	    
	    int i = 1;
	    for (String hotelName : hotelNames){
	    	System.out.println("Currently checking for "+ i + " st/th "+" Hotel: " + hotelName);
	    	hotelResultsPage.clickOnHotelByName(hotelName);
	    	hotelDetailsPage.verifyHotelDetailsPagePresence();
	    	verify.assertEquals(hotelDetailsPage.getHotelNameInHotelDetailsPage().toLowerCase(),hotelName.toLowerCase());
	    	browserHelper.openUrl(searchPageUrl);
	    	i = i + 1;
	    }
	    verify.assertAll();
	}
	
	@Test(groups = { "ProdSmoke","ProdSanity" })
	public void testAllHotelHDPageLoadsThiruvananthapuram(){
	    homePage = new HomePage();
	    hotelResultsPage = new HotelResultsPage();
	    hotelDetailsPage = new HotelDetailsPage();
	    itineraryPage = new ItineraryPage();
	    browserHelper = new BrowserHelper();
	    CommonUtils utils = new CommonUtils();
	    SoftAssert verify = new SoftAssert();
	    
	    String location = "Thiruvananthapuram";
	    int checkInFromCurrentDate = utils.getRandomNumber(15, 30);
	    int checkOutFromCurrentDate = checkInFromCurrentDate + 1;
	    int roomIndex = 1;
	    
	    homePage.doSearch(location, checkInFromCurrentDate, checkOutFromCurrentDate, roomIndex);
	    hotelResultsPage.verifyHotelResultsPagePresence();
	    String searchPageUrl = browserHelper.getCurrentUrl();
	    String[] hotelNames = hotelResultsPage.getDisplayedHotelNames();
	    
	    int i = 1;
	    for (String hotelName : hotelNames){
	    	System.out.println("Currently checking for "+ i + " st/th "+" Hotel: " + hotelName);
	    	hotelResultsPage.clickOnHotelByName(hotelName);
	    	hotelDetailsPage.verifyHotelDetailsPagePresence();
	    	verify.assertEquals(hotelDetailsPage.getHotelNameInHotelDetailsPage().toLowerCase(),hotelName.toLowerCase());
	    	browserHelper.openUrl(searchPageUrl);
	    	i = i + 1;
	    }
	    verify.assertAll();
	}
	
	@Test(groups = { "ProdSmoke","ProdSanity" })
	public void testAllHotelHDPageLoadsAmritsar(){
	    homePage = new HomePage();
	    hotelResultsPage = new HotelResultsPage();
	    hotelDetailsPage = new HotelDetailsPage();
	    itineraryPage = new ItineraryPage();
	    browserHelper = new BrowserHelper();
	    CommonUtils utils = new CommonUtils();
	    SoftAssert verify = new SoftAssert();
	    
	    String location = "Amritsar";
	    int checkInFromCurrentDate = utils.getRandomNumber(15, 30);
	    int checkOutFromCurrentDate = checkInFromCurrentDate + 1;
	    int roomIndex = 1;
	    
	    homePage.doSearch(location, checkInFromCurrentDate, checkOutFromCurrentDate, roomIndex);
	    hotelResultsPage.verifyHotelResultsPagePresence();
	    String searchPageUrl = browserHelper.getCurrentUrl();
	    String[] hotelNames = hotelResultsPage.getDisplayedHotelNames();
	    
	    int i = 1;
	    for (String hotelName : hotelNames){
	    	System.out.println("Currently checking for "+ i + " st/th "+" Hotel: " + hotelName);
	    	hotelResultsPage.clickOnHotelByName(hotelName);
	    	hotelDetailsPage.verifyHotelDetailsPagePresence();
	    	verify.assertEquals(hotelDetailsPage.getHotelNameInHotelDetailsPage().toLowerCase(),hotelName.toLowerCase());
	    	browserHelper.openUrl(searchPageUrl);
	    	i = i + 1;
	    }
	    verify.assertAll();
	}
	
//	@DataProvider(name = "cityNames",parallel=true)
//	public Object[][] cityName(){  
//		return new Object[][] {
//		      { new String("Bengaluru") },
//		      { new String("Hyderabad") },
//		      { new String("Mumbai") },
//		      { new String("Chennai") },
//		      { new String("Jaipur") },
//		      { new String("New Delhi") },
//		      { new String("Pune") },
//		      { new String("Goa") },
//		      { new String("Kolkata") },
//		      { new String("Mysore") },
//		      { new String("Gurgaon") },
//		      { new String("Ooty") },
//		      { new String("Manali") },
//		      { new String("Agra") },
//		      { new String("Udaipur") },
//		      { new String("Kodaikanal") },
//		      { new String("Aurangabad") },
//		};
//   } 
}